<div class="internas--format">
    <div class="columns">
        <div class="column is-one-fifth custom-side-width">
            __MENU__
        </div>
        <div class="column">
            <img class="fr-dii fr-fic" src="/public/ccc/internas/cabezotes/quienessomos-cabezote.jpg" />
        </div>
    </div>

    <div class="container container--margin-bottom-20">
        <div class="row">
            <div class="col-4" id="image--left">
                <img src="/public/ccc/internas/quienes-somos/secondary.jpg" />
            </div>
            <div class="col font--secundario">
                <p>
                    Somos una entidad privada de carácter corporativo, gremial y sin ánimo de lucro, que trabaja por una región más próspera que genere mejor calidad de vida para sus habitantes.
                </p>
            </div>
        </div>
    </div>

    <div class="columns">
        <div class="column columns is-multiline">

            <!--TABS-->
            <input class="tab" id="tab1" type="radio" name="tabs" checked>
            <label class="column" for="tab1">Qué hacemos</label>

            <input class="tab" id="tab2" type="radio" name="tabs">
            <label class="column" for="tab2">Nuestro Público</label>

            <input class="tab" id="tab3" type="radio" name="tabs">
            <label class="column" for="tab3">Nuestro Desafío</label>

            <input class="tab" id="tab4" type="radio" name="tabs">
            <label class="column" for="tab4">Valores Institucionales</label>

            <div class="container--content-tabs column is-12 font--secundario">

                <!--SEPARADOR CONTENIDO TABS-->
                <section id="content1">
                    <div class="container container--margin-bottom-20">
                        <div class="row">
                            <div class="col-2 text-center column-image-custom" id="border--right">
                                <img src="/public/ccc/internas/quienes-somos/que-hacemos.png" />
                            </div>
                            <div class="col column-text-custom font--secundario">
                                <p>
                                    <strong>En la Cámara de Comercio de Cali cumplimos la función jurídica que nos delegó el Estado de registrar las empresas existentes en la ciudad de Cali</strong> y en los municipios de Dagua, Jamundí, La Cumbre, Vijes y Yumbo.
                                </p>
                                <p>
                                    También llevamos los registros de las empresas y profesionales independientes interesados en contratar con el Estado (Registro de Proponentes); el de las entidades privadas sin ánimo de lucro y el de las empresas del sector turismo (Registro Nacional de Turismo). Los registros dan fe de la existencia de las empresas que impulsan la prosperidad de la región.
                                </p>
                            </div>
                        </div>
                    </div>
                </section><!--content1-->

                <section id="content2">
                    <div class="container container--margin-bottom-20">
                        <div class="row">
                            <div class="col-2 text-center column-image-custom" id="border--right">
                                <img src="/public/ccc/internas/quienes-somos/nuestro-publico.png" />
                            </div>
                            <div class="col column-text-custom font--secundario">
                                <p>
                                    <strong>Nuestras acciones están enfocadas principalmente hacia los empresarios matriculados en nuestra jurisdicción.</strong> Ellos son personas naturales o jurídicas, quienes para ejercer en forma profesional y permanente actividades empresariales, se matriculan en el Registro Mercantil.
                                </p>

                                <p>
                                    También orientamos servicios y programas específicos a los afiliados, que son aquellos empresarios, que además de tener matriculada su empresa o establecimiento comercial en el Registro Mercantil, voluntariamente y cumpliendo con los lineamientos de la Ley 1727 de 2014, adquieren una membresía que los acredita como tales.
                                </p>
                            </div>
                        </div>
                    </div>
                </section><!--content2-->

                <section id="content3">
                    <div class="container container--margin-bottom-20">
                        <div class="row">
                            <div class="col-2 text-center column-image-custom" id="border--right">
                                <img src="/public/ccc/internas/quienes-somos/nuestro-desafio.png" />
                            </div>
                            <div class="col column-text-custom font--secundario">
                                <p>Acompañamos a los empresarios a crecer rentable y sosteniblemente y a competir con éxito en una economía global para construir una región más próspera.</p>

                                <p>Creemos en que si las empresas crecen, la región crece y esto se reflejará en prosperidad y calidad de vida para sus ciudadanos.</p>

                                <h4 id="fucsia">Por eso</h4>

                                <ul id="arrow">
                                    <li><i class="fas fa-long-arrow-alt-right"></i>
                                        Conocemos las dinámicas económicas y las necesidades de las empresas.</li>
                                    <li><i class="fas fa-long-arrow-alt-right"></i>
                                        Ofrecemos formación práctica y a la medida de sus necesidades.</li>
                                    <li><i class="fas fa-long-arrow-alt-right"></i>
                                        Entregamos información de valor para tomar decisiones que hagan crecer su negocio.</li>
                                    <li><i class="fas fa-long-arrow-alt-right"></i>
                                        Apoyamos la consolidación del ecosistema para microempresas a través de alianzas estratégicas y proyectos de impacto.</li>
                                    <li><i class="fas fa-long-arrow-alt-right"></i>
                                        Generamos espacios de atención y orientación para los empresarios y les ayudamos a resolver de manera pertinente sus conflictos empresariales.</li>
                                    <li><i class="fas fa-long-arrow-alt-right"></i>
                                        Identificamos emprendimientos extraordinarios y les brindamos las herramientas para innovar.</li>
                                    <li><i class="fas fa-long-arrow-alt-right"></i>
                                        Detonamos el crecimiento de emprendimientos con alto potencial en etapa temprana y fortalecemos modelos de negocio extraordinarios, conectando potencial con estrategia y experiencia para generar crecimiento rápido, rentable y sostenido.</li>
                                    <li><i class="fas fa-long-arrow-alt-right"></i>
                                        Conectamos a  emprendedores de alto impacto con conocimiento y experiencia a través de nuestra Red de Mentores.</li>
                                    <li><i class="fas fa-long-arrow-alt-right"></i>
                                        Visibilizamos a los empresarios y sus historias extraordinarias.</li>
                                    <li><i class="fas fa-long-arrow-alt-right"></i>
                                        Afianzamos el compromiso de las empresas para invertir en innovación.</li>
                                    <li><i class="fas fa-long-arrow-alt-right"></i>
                                        Impulsamos proyectos de Región que impacten en la competitividad.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section><!--content2-->

                <section id="content4">

                    <div class="format--modal">

                        <div class="container">
                            <div class="row" id="modal--valores">
                                <div class="col-4 text-center">
                                    <label class="valores--1" for="modal-1"><img src="/public/ccc/internas/valores/pasion-empresario-g.png" alt="Mas"></label>
                                </div>
                                <div class="col-4 text-center">
                                    <label class="valores--2" for="modal-2"><img src="/public/ccc/internas/valores/valoramos-diferencia-g.png" alt=""></label>
                                </div>
                                <div class="col-4 text-center">
                                    <label class="valores--3" for="modal-3"><img src="/public/ccc/internas/valores/somos-confiables-g.png" alt=""></label>
                                </div>
                                <div class="col-6 first-valores-6 text-center">
                                    <label class="valores--4" for="modal-4"><img src="/public/ccc/internas/valores/somos-extraordinaros-g.png" alt=""></label>
                                </div>
                                <div class="col-6 text-center">
                                    <label class="valores--5" for="modal-5"><img src="/public/ccc/internas/valores/mentalidad-desafiante-g.png" alt=""></label>
                                </div>

                                <!--CONTENIDO MODAL-->
                                <input class="modal-state" id="modal-1" type="checkbox" />
                                <div class="modal-agro">
                                    <label class="modal__bg" for="modal-1"></label>
                                    <div class="modal__inner inner--valores inner-color1">
                                        <label class="modal__close" for="modal-1"></label>
                                        <div class="container container--margin-bottom-20">
                                            <div class="row">
                                                <div class="col-2 text-center column-image-custom" id="border--right">
                                                    <img src="/public/ccc/internas/valores/pasion-empresario-p.png" />
                                                </div>
                                                <div class="col column-text-custom font--secundario">
                                                    <h2>Sentimos Pasión por el Empresario</h2>
                                                    <p>
                                                        Nuestro compromiso es entender las dinámicas económicas de la región y las necesidades de los  empresarios para ofrecerles servicios y productos relevantes para su crecimiento.
                                                    </p>

                                                    <h4>Conductas observables</h4>

                                                    <ul id="arrow">
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Brindamos información de valor para los empresarios en cualquier escenario.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Investigamos las necesidades y realidades de las empresas para ofrecerles soluciones acordes con sus expectativas.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Damos visibilidad a los empresarios, sus historias, logros y casos de éxito.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Generamos conversaciones sobre la agenda empresarial y el desarrollo regional.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Conectamos a los empresarios con redes de aprendizaje, comercio e inversión.
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <input class="modal-state" id="modal-2" type="checkbox" />
                                <div class="modal-agro">
                                    <label class="modal__bg" for="modal-2"></label>
                                    <div class="modal__inner inner--valores inner-color2">
                                        <label class="modal__close" for="modal-2"></label>

                                        <div class="container container--margin-bottom-20">
                                            <div class="row">
                                                <div class="col-2 text-center column-image-custom" id="border--right">
                                                    <img src="/public/ccc/internas/valores/valoramos-diferencia-p.png" />
                                                </div>
                                                <div class="col column-text-custom font--secundario">
                                                    <h2>Valoramos la Diferencia</h2>
                                                    <p>
                                                        Reconocemos que la diversidad aumenta las posibilidades de construir valor para el crecimiento, por eso respetamos y capitalizamos la diferencia en nuestros grupos de interés.
                                                    </p>

                                                    <h4>Conductas observables</h4>

                                                    <ul id="arrow">
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Reconocemos las diferentes dinámicas empresariales y ofrecemos programas y servicios a la medida de las necesidades de las empresas, sin importar su tamaño o sector.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Somos incluyentes con la opinión de los empresarios para la construcción de nuestros programas y servicios.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Valoramos la diversidad de pensamiento y contribuciones individuales de nuestros colaboradores para construir equipos efectivos y flexibles.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Tratamos a todas las personas con respeto y dignidad, entendiendo las diferencias socioeconómicas, educativas y culturales.
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <input class="modal-state" id="modal-3" type="checkbox" />
                                <div class="modal-agro">
                                    <label class="modal__bg" for="modal-3"></label>
                                    <div class="modal__inner inner--valores inner-color3">
                                        <label class="modal__close" for="modal-3"></label>

                                        <div class="container container--margin-bottom-20">
                                            <div class="row">
                                                <div class="col-2 text-center column-image-custom" id="border--right">
                                                    <img src="/public/ccc/internas/valores/somos-confiables-p.png" />
                                                </div>
                                                <div class="col column-text-custom font--secundario">
                                                    <h2>Somos Confiables</h2>
                                                    <p>
                                                        Tomamos decisiones objetivas y autónomas en beneficio de los colaboradores, los empresarios y la región.
                                                    </p>

                                                    <h4>Conductas observables</h4>

                                                    <ul id="arrow">
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Tomamos decisiones equilibrando la norma, los procedimientos y el mérito.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Somos capaces de renunciar a actividades y/o proyectos que no son coherentes con nuestra estrategia.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Cumplimos rigurosa y oportunamente con nuestros compromisos.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Empoderamos a nuestros equipos de trabajo a través de la información oportuna, el entrenamiento y la capacitación.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Somos coherentes entre lo que pensamos, decimos y hacemos, todos los días y en cualquier situación.
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <input class="modal-state" id="modal-4" type="checkbox" />
                                <div class="modal-agro">
                                    <label class="modal__bg" for="modal-4"></label>
                                    <div class="modal__inner inner--valores inner-color4">
                                        <label class="modal__close" for="modal-4"></label>


                                        <div class="container container--margin-bottom-20">
                                            <div class="row">
                                                <div class="col-2 text-center column-image-custom" id="border--right">
                                                    <img src="/public/ccc/internas/valores/somos-extraordinaros-p.png" />
                                                </div>
                                                <div class="col column-text-custom font--secundario">
                                                    <h2>Somos Extraordinarios en el Ser y el Hacer</h2>
                                                    <p>
                                                        Estamos comprometidos con la excelencia personal y profesional, por eso promovemos el  pensamiento estratégico, el conocimiento y el desarrollo integral.
                                                    </p>

                                                    <h4>Conductas observables</h4>

                                                    <ul id="arrow">
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Establecemos una agenda permanente de actividades orientadas a promover el bienestar integral de nuestros colaboradores.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Generamos espacios de diálogo permanentes entre líderes y colaboradores para hablar sobre la estrategia y los planes de acción de cada área.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Promovemos el plan carrera de los colaboradores y privilegiamos las convocatorias internas.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Nos preocupamos por visibilizar los logros profesionales, personales y laborales de los colaboradores a través de los canales internos de comunicación.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Ofrecemos una atención y trato extraordinario a los colaboradores de las distintas áreas, sin importar su rango.

                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Exploramos permanentemente estrategias y acciones para garantizar un servicio extraordinario al empresario.
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <input class="modal-state" id="modal-5" type="checkbox" />
                                <div class="modal-agro">
                                    <label class="modal__bg" for="modal-5"></label>
                                    <div class="modal__inner inner--valores inner-color5">
                                        <label class="modal__close" for="modal-5"></label>

                                        <div class="container container--margin-bottom-20">
                                            <div class="row">
                                                <div class="col-2 text-center column-image-custom" id="border--right">
                                                    <img src="/public/ccc/internas/valores/mentalidad-desafiante-p.png" />
                                                </div>
                                                <div class="col column-text-custom font--secundario">
                                                    <h2>Tenemos Mentalidad Desafiante e Innovadora</h2>
                                                    <p>
                                                        Creemos que el crecimiento se genera permanentemente, por eso desafiamos paradigmas, barreras y realidades para transformarlos en oportunidades.
                                                    </p>

                                                    <h4>Conductas observables</h4>

                                                    <ul id="arrow">
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Proponemos iniciativas innovadoras para transformar nuestros programas y servicios en beneficio de nuestros empresarios.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Generamos espacios para resolver preguntas e inquietudes a nivel interno y externo.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>

                                                            Desafiamos la manera tradicional de hacer las cosas, buscando alternativas diferentes y más eficientes para realizar nuestro trabajo.
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-long-arrow-alt-right"></i>
                                                            Convertimos las situaciones difíciles en oportunidades de crecimiento, y documentamos y compartimos las lecciones aprendidas para beneficio de todos.
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>

                    </div><!--format--modal-->

                </section><!--content4-->

            </div><!--.container--content-tabs-->

        </div>
    </div>
</div><!--internas--quienes-somos-->
