<div class="wrp-content-unidadades-corporativas-ccc">

    <div class="columns">
        <div class="column custom-side-width is-one-fifth">__MENU__</div>
        
        <div class="column">
            <div class="columns">
                <div class="column">
                    <img class="fr-dii fr-fic img-full" src="/public/ccc/internas/cabezotes/entorno-cabezote.jpg">
                </div>
            </div>
            <div class="separator-25"></div>                    
        </div>
    </div>
    <div class="columns">
        <div class="column custom-side-width is-one-fifth">  
            <div class="card shadow bg-g-white">
                <div class="card-image" id="image-width">
                    <img src="/public/ccc/internas/presidencia/cesar-garcia.jpg" alt="Cesar García" class="img-center">
                    <div class="shadow-bottom"></div>
                </div>
                <div class="card-content shadow-top">
                    <div class="content">
                            <h1>Cesar</h1>
                            <p class="subtitle is-3">Garcia Castaño</p>
                            <span class="tag tag-blue">Director</span>
                    </div>
                </div>
            </div>   
        </div>
        <div class="column wrp-text font--secundario">
            <h1 class="title-divider text-blue">
                ¿Cómo aportamos a la estrategia?
            </h1>
            <p>
                Visibilizamos y viabilizamos planes, programas y proyectos que transforman la región. Así mismo, generamos alianzas estratégicas que contribuyan a la competitividad y promuevan el desarrollo de la misma.
            </p>
        </div>
    </div>
    <div class="columns">
        <div class="column">
            <h1 class="title-divider text-blue">
                Áreas que lo conforman
            </h1>
            <div class="align-center">
                <img src="/public/ccc/internas/presidencia/areas-gerencia-entorno.png" alt="Diagrama" class="img-center" width="376">
            </div>
        </div>
    </div>
</div>
