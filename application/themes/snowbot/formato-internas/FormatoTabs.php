<div class="columns">
    <div class="column is-one-fifth custom-side-width">
        __MENU__
    </div>
    <div class="column">
        <img class="fr-dii fr-fic" src="/public/ccc/internas/banners/banner--nuestra-historia.jpg" />
    </div>
</div>

<div class="columns">
    <div class="column columns is-multiline">

        <!--TABS-->
        <input class="tab" id="tab1" type="radio" name="tabs" checked>
        <label class="column" for="tab1">1910 - 1916</label>

        <input class="tab" id="tab2" type="radio" name="tabs">
        <label class="column" for="tab2">1929 - 1931</label>

        <input class="tab" id="tab3" type="radio" name="tabs">
        <label class="column" for="tab3">1950 - 1976</label>

        <input class="tab" id="tab4" type="radio" name="tabs">
        <label class="column" for="tab4">1983 - 1993</label>

        <input class="tab" id="tab5" type="radio" name="tabs">
        <label class="column" for="tab5">1997 - 2014</label>


        <div class="container--content-tabs column is-12">

            <!--SEPARADOR CONTENIDO TABS-->
            <section id="content1">

            </section><!--content1-->

            <section id="content2">

            </section><!--content2-->

            <section id="content3">

            </section><!--content2-->

            <section id="content4">

            </section><!--content2-->

            <section id="content5">

            </section><!--content5-->

        </div><!--.container--content-tabs-->

    </div>
</div>