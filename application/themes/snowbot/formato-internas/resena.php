<div class="columns">
    <div class="column is-one-fifth custom-side-width">
        __MENU__
    </div>
    <div class="column">
        <img class="fr-dii fr-fic" src="/public/ccc/internas/cabezotes/resenahistorica-cabezote.jpg" />
    </div>
</div>

<div class="columns">
    <div class="column columns is-multiline">

        <!--TABS-->
        <input class="tab" id="tab1" type="radio" name="tabs" checked>
        <label class="column" for="tab1">1910 - 1916</label>

        <input class="tab" id="tab2" type="radio" name="tabs">
        <label class="column" for="tab2">1929 - 1931</label>

        <input class="tab" id="tab3" type="radio" name="tabs">
        <label class="column" for="tab3">1950 - 1976</label>

        <input class="tab" id="tab4" type="radio" name="tabs">
        <label class="column" for="tab4">1983 - 1993</label>

        <input class="tab" id="tab5" type="radio" name="tabs">
        <label class="column" for="tab5">1997 - 2014</label>


        <div class="container--content-tabs column is-12">

            <!--SEPARADOR CONTENIDO TABS-->
            <section id="content1">

                <div class="contenedor-historia-ccc" id="historia--1910-1916">

                    <div class="item-historia">
                        <h4>Decreto 669</h4>
                        <span>3 Agosto, 1910</span>
                        <p>Se expidió el Decreto 669 por el cual se crea la Cámara de Comercio de Cali.</p>
                    </div>

                    <div class="item-historia">
                        <h4>Primera Junta Directiva</h4>
                        <span>1 Septiembre, 1910</span>
                        <p>30 comerciantes en el despacho del gobernador Pablo Borrero Ayerbe, eligen la primera Junta Directiva, conformada por 9 integrantes.</p>
                    </div>

                    <div class="item-historia">
                        <h4>Decreto 1807</h4>
                        <span>29 Octubre, 1915</span>
                        <p>Se volvió a constituir la Cámara de Comercio de Cali con base en el Decreto 1807. Editó el primer número del boletín mensual de información comercial y agrícola.</p>
                    </div>

                    <div class="item-historia">
                        <h4>Nuevas Construcciones</h4>
                        <span>4 Abril, 1916</span>
                        <p>Promovió la construcción del Ferrocarril del Pacífico, de un edificio para la oficina de correos y telégrafos, la apertura en el Colegio de Santa Librada de una Escuela de Comercio y Formación.</p>
                    </div>

                </div><!--contenedor-historia-ccc-->

            </section><!--content1-->

            <section id="content2">

                <div class="contenedor-historia-ccc" id="historia--1929-1931">

                    <div class="item-historia">
                        <h4>Firma de Contratos</h4>
                        <span>1 Enero, 1929</span>
                        <p>Promovió el contrato para la conducción de correos y pasajeros por aeroplano entre la Costa Atlántica, Medellín, Manizales, Bogotá y Cali y la conformación de una comisión para estudiar en los Estados Unidos lo relacionado con la maquinaria para pozos artesianos.</p>
                    </div>

                    <div class="item-historia">
                        <h4>Ley 28 de 1931</h4>
                        <span>18 Febrero, 1931</span>
                        <p>Se expide la Ley 28 de 1931 que establece el Registro Público de Comercio y se delega su administración a las otras cámaras.</p>
                    </div>

                </div><!--contenedor-historia-ccc-->

            </section><!--content2-->

            <section id="content3">

                <div class="contenedor-historia-ccc" id="historia--1950-1976">

                    <div class="item-historia">
                        <h4>Crece número de Afiliados</h4>
                        <span>1 Enero, 1950</span>
                        <p>La Cámara de Comercio de Cali alcanza los 187 afiliados y ya tenía jurisdicción en Cali, Dagua, Jamundí, la Cumbre, Restrepo, Vijes y Yumbo.</p>
                    </div>

                    <div class="item-historia">
                        <h4>Sede Propia</h4>
                        <span>1 Enero, 1971</span>
                        <p>
                            La CCC decide construir su sede propia, en la Calle 8 con Carrera 3 y de la que apenas se tenían los planos. El trabajo en equipo de los empresarios fue determinante.
                        </p>
                    </div>

                    <div class="item-historia">
                        <h4>Comité Empresarial del Valle del Cauca</h4>
                        <span>10 Mayo, 1975</span>
                        <p>
                            Fomentó la creación del Comité Empresarial del Valle del Cauca.
                        </p>
                    </div>

                    <div class="item-historia">
                        <h4>Apoyo en Obras Ciudadanas</h4>
                        <span></span>
                        <p>
                            Trabajó para concluir los trabajos del Aeropuerto Alfonso Bonilla Aragón, asegurar el mantenimiento de la Carretera al Mar y continuar con las obras viales del Departamento.
                        </p>
                    </div>

                </div><!--contenedor-historia-ccc-->

            </section><!--content2-->

            <section id="content4">

                <div class="contenedor-historia-ccc" id="historia--1983-1993">

                    <div class="item-historia">
                        <h4>Inmersión en Proyectos para el Desarrollo</h4>
                        <span>28 Enero, 1983</span>
                        <p>Inició la concertación para identificar los grandes proyectos necesarios para el desarrollo del Suroccidente, mediante programas como “El Valle 2000”, “El Valle del Futuro” y “El Cali que Queremos”.</p>
                    </div>

                    <div class="item-historia">
                        <h4>Trabajos Sociales</h4>
                        <span>14 Enero, 1989</span>
                        <p>
                            Trabajó en el desarrollo social mediante comités cívicos de vivienda, alimentos, reciclaje, tránsito, seguridad y medio ambiente.
                        </p>
                    </div>

                    <div class="item-historia">
                        <h4>Proyectos de Infraestructura para la Región</h4>
                        <span>15 Julio, 1993</span>
                        <p>
                            Continuó con la promoción de los proyectos de infraestructura identificados como prioritarios para la región.
                        </p>
                    </div>

                    <div class="item-historia">
                        <h4>Búsqueda de Ventajas Competitivas</h4>
                        <span></span>
                        <p>
                            Trabajó en la búsqueda de las ventajas competitivas del Departamento, de acuerdo con el nuevo modelo de internacionalización de la economía.
                        </p>
                    </div>

                </div><!--contenedor-historia-ccc-->

            </section><!--content2-->

            <section id="content5">
                <div class="contenedor-historia-ccc" id="historia--1997-2014">

                    <!--item historia-->
                    <div class="item-historia">
                        <h4>Privatización Puerto Buenaventura</h4>
                        <span>28 Septiembre, 1997</span>
                        <p>Impulsó la privatización del Puerto de Buenaventura, que en pocos años mejoró rápidamente su eficiencia en general.</p>
                    </div>
                    <!--item historia-->


                    <!--item historia-->
                    <div class="item-historia">
                        <h4>Monumento a la Solidaridad</h4>
                        <span>28 Noviembre, 1998</span>
                        <p>Donó a la ciudad el Monumento a la Solidaridad.</p>
                    </div>
                    <!--item historia-->

                    <!--item historia-->
                    <div class="item-historia">
                        <h4>Logros de la CCC</h4>
                        <span>1 Enero, 2000</span>
                        <p>En la primera década del nuevo siglo, la Entidad desarrolló su Plan Estratégico “Unión, Compromiso y Acción”. Fue la primera Cámara del país en obtener la certificación de su Sistema de Gestión de Calidad y puso en funcionamiento los servicios renovación en línea y certificación digital.</p>
                    </div>
                    <!--item historia-->


                    <!--item historia-->
                    <div class="item-historia">
                        <h4>Apoyo al Estado</h4>
                        <span>10 Febrero, 2010</span>
                        <p>Trabajó en la recuperación de los espacios públicos, acompañó la recuperación de Emcali y coordinó las veedurías ciudadanas del MIO.</p>
                    </div>
                    <!--item historia-->


                    <!--item historia-->
                    <div class="item-historia">
                        <h4>Impulso al Desarrollo del Departamento</h4>
                        <span>28 Enero, 2012</span>
                        <p>Impulsó la agenda de desarrollo para el Valle, conformó el Bloque Regional y de Congresistas del Valle y en un ejercicio de liderazgo colectivo promovió la construcción del Centro de Eventos Valle del Pacífico.
                        </p>
                    </div>
                    <!--item historia-->

                    <!--item historia-->
                    <div class="item-historia">
                        <h4>Nuevos productos y servicios</h4>
                        <span>2014</span>
                        <p>La Cámara de Comercio de Cali asumió el desafío de reinventarse para crear nuevos productos y servicios encaminados a acompañar a las empresas a crecer rentable y sosteniblemente. En este marco lanzó la plataforma cluster para visibilizar las dinámicas empresariales de la región, creó un nuevo portafolio de servicios para los negocios, la plataforma Registr@YA para facilitar la realización de los registros públicos en Línea y la Unidad de Emprendimiento e Innovación para apoyar a las empresas con gran potencial de crecimiento.
                        </p>
                    </div>
                    <!--item historia-->

                </div>
            </section><!--content5-->

            <div></div></div>

    </div>
</div>