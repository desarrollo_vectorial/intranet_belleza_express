<div class="wrp-content-unidadades-corporativas-ccc">

    <div class="columns">
        <div class="column custom-side-width is-one-fifth">__MENU__</div>
        
        <div class="column">
            <div class="columns">
                <div class="column">
                    <img class="fr-dii fr-fic img-full" src="/public/ccc/internas/cabezotes/presidente-cabezote.jpg">
                </div>
            </div>
            <div class="separator-25"></div>                    
        </div>
    </div>
    <div class="columns">
        <div class="column custom-side-width is-one-fifth">  
            <div class="card shadow bg-g-white">
                <div class="card-image" id="image-width">
                    <img src="public/ccc/internas/presidencia/esteban-piedrahita.jpg" alt="Esteban" class="img-center">
                </div>
                <div class="card-content shadow-top">
                    <div class="content">
                            <h1>Esteban</h1>
                            <p class="subtitle is-3 color-fucsia">Piedrahíta Uribe</p>
                    </div>
                </div>
            </div>   
        </div>
        <div class="column wrp-text font--secundario">
            <p><strong>Economista de la Universidad de Harvard,</strong> egresado con altos honores, cuenta con Maestría en Filosofía e Historia de la Ciencia del London School of Economics and Political Science, se desempeñó como editor económico de la Revista Semana y trabajó en el Banco Interamericano de Desarrollo (BID), donde se desempeñaba como especialista senior, así mismo, trabajó en Salomon Brothers y fue director de Planeación Nacional.</p>

            <p>Esteban es un apasionado por viajar y por aprender sobre otras culturas.</p>
        </div>
    </div>
</div>
