<div class="wrp-content-unidadades-corporativas-ccc">

    <div class="columns">
        <div class="column custom-side-width is-one-fifth">__MENU__</div>
        
        <div class="column">
            <div class="columns">
                <div class="column">
                    <img class="fr-dii fr-fic img-full" src="/public/ccc/internas/cabezotes/aseguramiento-cabezote.jpg">
                </div>
            </div>
            <div class="separator-25"></div>                    
        </div>
    </div>
    <div class="columns">
        <div class="column custom-side-width is-one-fifth">  
            <div class="card shadow bg-g-white">
                <div class="card-image" id="image-width">
                    <img src="/public/ccc/internas/presidencia/carlos-alarcon.jpg" alt="Carlos Eduardo" class="img-center">
                    <div class="shadow-bottom"></div>
                </div>
                <div class="card-content shadow-top">
                    <div class="content">
                            <h1>Carlos Eduardo</h1>
                            <p class="subtitle is-3">Alarcon</p>
                            <span class="tag tag-blue">Director</span>
                    </div>
                </div>
            </div>   
        </div>
        <div class="column wrp-text font--secundario">
            <h1 class="title-divider text-blue">
                ¿Cómo aportamos a la estrategia?
            </h1>
            <p>
                Velamos por el cumplimiento de los procesos, procedimiento y protocolos de la institución.
            </p>
        </div>
    </div>
    <div class="columns">
        <div class="column">
            <h1 class="title-divider text-blue">
                Áreas que lo conforman
            </h1>
            <div class="align-center">
                <img src="/public/ccc/internas/presidencia/areas-gerencia-aseguramiento.png" alt="Diagrama" class="img-center" width="370">
            </div>
        </div>
    </div>
</div>
