<div class="internas--format">
    <div class="columns">
        <div class="column is-one-fifth custom-side-width">
            __MENU__
        </div>
        <div class="column">
            <img class="fr-dii fr-fic" src="/public/ccc/internas/cabezotes/estructura-cabezote.jpg" />
        </div>
    </div>

    <div class="container container--margin-bottom-20">
        <div class="row">
            <div class="col">
                <div class="format--modal">
                    <div class="nuestra-camara--estructura">
                        <!--PINES-->
                        <label class="pin in1" for="modal-1"><img src="/public/ccc/internas/valores/pasion-empresario-g.png" alt="Mas"></label>
                        <label class="pin in2" for="modal-2"><img src="/public/ccc/internas/valores/valoramos-diferencia-g.png" alt=""></label>
                        <label class="pin in3" for="modal-3"><img src="/public/ccc/internas/valores/somos-confiables-g.png" alt=""></label>
                        <label class="pin in4" for="modal-4"><img src="/public/ccc/internas/valores/somos-extraordinaros-g.png" alt=""></label>
                        <label class="pin in5" for="modal-5"><img src="/public/ccc/internas/valores/mentalidad-desafiante-g.png" alt=""></label>

                        <!--CONTENIDO MODAL-->
                        <input class="modal-state" id="modal-1" type="checkbox" />
                        <div class="modal-agro">
                            <label class="modal__bg" for="modal-1"></label>
                            <div class="modal__inner">
                                <label class="modal__close" for="modal-1"></label>
                                <p> <img src="/public/ccc/internas/popup1.png" alt=""></p>
                            </div>
                        </div>

                        <input class="modal-state" id="modal-2" type="checkbox" />
                        <div class="modal-agro">
                            <label class="modal__bg" for="modal-2"></label>
                            <div class="modal__inner">
                                <label class="modal__close" for="modal-2"></label>
                                <p> <img src="/public/ccc/internas/popup2.png" alt=""></p>
                            </div>
                        </div>

                        <input class="modal-state" id="modal-3" type="checkbox" />
                        <div class="modal-agro">
                            <label class="modal__bg" for="modal-3"></label>
                            <div class="modal__inner">
                                <label class="modal__close" for="modal-3"></label>
                                <p> <img src="/public/ccc/internas/popup3.png" alt=""></p>
                            </div>
                        </div>

                        <input class="modal-state" id="modal-4" type="checkbox" />
                        <div class="modal-agro">
                            <label class="modal__bg" for="modal-4"></label>
                            <div class="modal__inner">
                                <label class="modal__close" for="modal-4"></label>
                                <p> <img src="/public/ccc/internas/popup4.png" alt=""></p>
                            </div>
                        </div>

                        <input class="modal-state" id="modal-5" type="checkbox" />
                        <div class="modal-agro">
                            <label class="modal__bg" for="modal-5"></label>
                            <div class="modal__inner">
                                <label class="modal__close" for="modal-5"></label>
                                <p> <img src="/public/ccc/internas/popup5.png" alt=""></p>
                            </div>
                        </div>

                        <!--END CONTENIDO MODAL-->

                    </div><!--estructura-incauca-->
                </div><!--format--modal-->
            </div>
        </div>
    </div>
</div><!--internas--quienes-somos-->
