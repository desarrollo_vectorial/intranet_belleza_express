<?php 

defined('_ENGINE') or die('Access Denied'); return array (
  'adapter' => 'mysqli',
  'params'  =>
    array (
      'host'     => DB_HOST,
      'username' => DB_USER,
      'password' => DB_PASSWORD,
      'dbname'   => DB_NAME,
      'charset'  => 'UTF8',
      'adapterNamespace' => 'Zend_Db_Adapter',
    ),
  'isDefaultTableAdapter' => true,
  'tablePrefix'           => 'engine4_',
  'tableAdapterClass'     => 'Engine_Db_Table',
);

/*
defined('_ENGINE') or die('Access Denied'); return array (
  'adapter' => 'mysqli',
  'params'  =>
    array (
      'host'     => 'localhost',
      'username' => 'root',
      'password' => 'root',
      'dbname'   => 'dev_centelsa_intranet',
      'charset'  => 'UTF8',
      'adapterNamespace' => 'Zend_Db_Adapter',
    ),
  'isDefaultTableAdapter' => true,
  'tablePrefix'           => 'engine4_',
  'tableAdapterClass'     => 'Engine_Db_Table',
); */
?>