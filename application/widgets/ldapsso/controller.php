<?php

class Widget_LdapssoController extends Engine_Content_Widget_Abstract
{
    protected $ldapcon;
    protected $ldapserver;
    protected $bind;
    protected $username;
    protected $password;
    protected $DC;
    protected $search;
    protected $DCstring;

    public function __construct(){
        $this->ldapcon     = null;
        $this->ldapserver  = '172.26.6.103';
        $this->bind        = null;
        $this->username    = 'vectorial';
        $this->password    = 'Debian2017*';
        $this->DC          = array('owa', 'viakable', 'com');
        $this->search      = null;
        $this->DCstring    = '';
        $this->new_user_id = null;
    }

    public function indexAction()
    {
        // Verificar el entorno
        if (HTTP_HOST == LOCAL_DOMAIN || HTTP_HOST == DEV_DOMAIN) {
            return;
        }

        // Verificar si el usuario existe
        $viewer = Engine_Api::_()->user()->getViewer();
        if (isset($viewer->user_id)) {
            return;
        }

        /*
         * DELETE FROM `engine4_user_fields_values` WHERE `field_id` IN (6,33) AND `value`='';
        */
        /*
        //drop TRIGGER limpiarFechasErroneas;
        DELETE FROM engine4_user_fields_values WHERE field_id IN (6,33) AND value='';
        DELIMITER |
        CREATE TRIGGER limpiarFechasErroneas BEFORE INSERT ON engine4_user_fields_values
        FOR EACH ROW
        |
        DELIMITER ;
        */

        // Obtener el usuario y la contrase�a capturada por el navegador
        if (isset($_SERVER['AUTH_USER'])) {
            $ws = 'IIS';
            $usuario = $_SERVER['AUTH_USER'];
            $password = $_SERVER['AUTH_PASSWORD'];
            echo Zend_Registry::get('Zend_Translate')->_('Get local user ...').'<br />';
        }else if (isset($_SERVER['PHP_AUTH_USER'])) {
            $ws = 'Apache';
            $usuario = $_SERVER['PHP_AUTH_USER'];
            $password = $_SERVER['PHP_AUTH_PWD'];
            echo Zend_Registry::get('Zend_Translate')->_('Get local user ...').'<br />';
        }


        $usuarioar = explode('\\', $usuario);
        $usuario = utf8_encode(trim(end($usuarioar)));

        //$usuario = "jgarciac";
        //$usuario = "hmarturo";
        echo $usuario;
        $this->ldapBind();
        $this->ldapSearch($usuario);
        echo Zend_Registry::get('Zend_Translate')->_('Search user ...').'<br />';

        // Verificar si el usuario existe en el servidor ldap
        if ($this->search) {

            // Leer m�ltiples entradas de los resultados porporcionados.
            $info = ldap_get_entries($this->ldapcon, $this->search);
            // Desenlazar de directorio LDAP
            ldap_close($this->ldapcon);

            $ii = 0;
            $user_info = array();
            $data = array();

            // Extraer los datos de las entradas
            for ($i = 0; $ii < $info[$i]['count']; ++$ii) {
                $data = $info[$i][$ii];
                $user_info[$data] = $info[$i][$data][0];
            }

            // Verificar si la entrada tiene email
            if (isset($user_info['mail']) && !is_null($user_info['mail'])) {
                echo Zend_Registry::get('Zend_Translate')->_('User found ...').'<br />';
                // Verificar el usuario en db
                if (!$this->verifyUser($usuario)) {

                    // Crear usuario en db
                    $this->registrar($usuario, $user_info);
                    $this->seguirTodosLosAdministradores();
                } else {

                    // Actualizar el cin del usuario en db
                    $this->actualizarCinUsuario($usuario, $user_info);
                }

                $redes = array();
                // crear red y user_fields_values
                $redes = $this->crearPersonalizacion($usuario, $user_info);
                // actualizar contrase�a y datos basicos del usuario
                $this->actualizarContrasena($usuario, $user_info);

                if (is_array($redes)) {
                    $this->vincularARed($user_info, $usuario, $redes);
                    $this->autenticar($usuario, $user_info);

                    //$tiempo_fin = $this->microtime_float();
                    //$time_total = $tiempo_fin - $tiempo_inicio;
                    //exit('busca usuario :'.$time_total);
                    //header('Location: members/home/');
                    header('Location: http://'.HTTP_HOST.$_SERVER['REQUEST_URI']);

                    //return $this ->_redirect('members/home/');
                    exit;
                } else {
                    echo '<center>';
                    echo Zend_Registry::get('Zend_Translate')->_('Error: Se requiere actuailzar tus datos en el Directorio Activo<br />');
                    echo '</center>';
                }
            } else {
                echo Zend_Registry::get('Zend_Translate')->_('User not found ...').'<br />';
            }
        }
    }

    /**
     * Actualizar el cin del usuario.
     *
     * @param string $username  Usuario obtenido por el navegador
     * @param array  $user_info Informacion del usuario extraida del ldap
     */
    protected function actualizarCinUsuario($username, $user_info)
    {
        try {
            echo Zend_Registry::get('Zend_Translate')->_('Actualizando Usuario ...').'<br />';
            $db = Zend_Db_Table_Abstract::getDefaultAdapter();
            // Obtener datos de usuario en db
            $stmt = $db->query('SELECT engine4_users.* FROM engine4_users WHERE engine4_users.username=\''.$username.'\'');
            $rows = $stmt->fetchAll();
            $uid = $rows[0]['user_id'];
            $db->query('DELETE FROM engine4_user_fields_values WHERE item_id = '.$uid.' AND field_id = 83;');
            $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 83, \''.$user_info['postalcode'].'\');');
            $db->commit();
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    /**
     * Crear red y agregar datos a la tabla user_fields_values del usuario del servidor ldap.
     *
     * @param string $username  Usuario obtenido por el navegador
     * @param array  $user_info Informacion del usuario extraida del ldap
     *
     * @return array $redesPaises Retorna las redes por pais
     */
    protected function crearPersonalizacion($username, $user_info)
    {
        $sql = '';
        $arbol = split(',', $user_info['distinguishedname']);

        if (strcmp($arbol[count($arbol) - 4], 'OU=Internacional') == 0) {
            $pais = str_replace('OU=', '', $arbol[count($arbol) - 5]);
        } else {
            $pais = str_replace('OU=', '', $arbol[count($arbol) - 4]);
        }

        try {
            echo Zend_Registry::get('Zend_Translate')->_('Actualizando Usuario ...').'<br />';
            $db = Zend_Db_Table_Abstract::getDefaultAdapter();
            // Obtener datos de usuario en db
            $stmt = $db->query('SELECT engine4_users.* FROM engine4_users WHERE engine4_users.username=\''.$username.'\'');
            $rows = $stmt->fetchAll();
            $uid = $rows[0]['user_id'];
            // Insertar la red si no existe ya en la Red del pais
            $db->query("INSERT INTO engine4_network_networks (title,description,field_id,member_count,hide,assignment )
                SELECT a.*
                FROM(
                    Select  '".trim($pais)."' as title,
                    '".trim($pais)."' as descripcion,
                    0 as field_id,
                    0 as member_count,
                    0 as hide,
                    0 as assignment
                ) as a
                WHERE NOT EXISTS
                (
                    SELECT *
                    FROM engine4_network_networks
                    WHERE title = '".trim($pais)."'
                ); ");
            // Obtener red por pais
            $stmt = $db->query('SELECT * FROM engine4_network_networks WHERE title=\''.trim($pais).'\'');

            /*
            DELETE  FROM `engine4_authorization_allow` WHERE `resource_type` LIKE 'user' AND `action` LIKE 'view' AND `role` LIKE 'member';
            INSERT INTO `engine4_authorization_allow` (`resource_type`, `resource_id`, `action`, `role`, `role_id`, `value`, `params`)
            SELECT
            'user', user_id, 'view', 'member', 0, 1, NULL
            FROM
            engine4_users;
            SELECT *  FROM `engine4_authorization_allow` WHERE `resource_type` LIKE 'user' AND `action` LIKE 'view' AND `role` LIKE 'registered';
            */

            /*$db->query("  DELETE FROM engine4_activity_notificationsettings;
            INSERT INTO engine4_activity_notificationsettings
            (
            user_id,
            type,
            email
            )
            SELECT
            b.user_id,
            a.type,
            0
            FROM
            engine4_activity_notificationtypes as a,
            engine4_users as b;");

            */

            /*
            engine4_wall_usersettings
            DELETE FROM engine4_wall_usersettings;
            INSERT INTO engine4_wall_usersettings
            (
            user_id,
            type,
            email
            )
            SELECT
            b.user_id,
            a.type,
            0
            FROM
            engine4_activity_notificationtypes as a,
            engine4_users as b;");
            */

            $redesPaises = $stmt->fetchAll();

            if ($redesPaises) {
                $db->query('DELETE FROM engine4_user_fields_values WHERE item_id = '.$uid.' AND field_id IN(1,3,4,74,79,81,83);');
                // $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 1, \'4\');');
                $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 3, \''.$user_info['givenname'].'\');');
                $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 4, \''.$user_info['sn'].'\');');
                // $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 80, \''.$user_info['physicaldeliveryofficename'].'\');');
                $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 79, \''.$user_info['title'].'\');');
                $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 81, \''.$user_info['telephonenumber'].'\');');
                $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 83, \''.$user_info['postalcode'].'\');');
                $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 74, \''.$user_info['department'].'\');');
                $db->commit();
            }
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }

        return $redesPaises;
    }

    /**
     * Funcion para vincular el usuario a una red.
     *
     * @param string $username  Usuario obtenido por el navegador
     * @param array  $user_info Informacion del usuario extraida del ldap
     * @param array  $redes     Redes por pais
     */
    protected function vincularARed($user_info, $username, $redes)
    {

        /*$country = $this -> country;*/

        try {
            echo Zend_Registry::get('Zend_Translate')->_('Join to network country ...').'<br />';
            $db = Zend_Db_Table_Abstract::getDefaultAdapter();
            $stmt = $db->query('SELECT engine4_users.* FROM engine4_users WHERE engine4_users.username=\''.$username.'\'');
            $rows = $stmt->fetchAll();
            $uid = $rows[0]['user_id'];

            if ($rows[0]['level_id'] >= 4) {
                $db->query('DELETE FROM engine4_network_membership WHERE user_id = '.$uid.';');
                $db->query('INSERT INTO engine4_network_membership (resource_id, user_id, active, resource_approved, user_approved) VALUES('.$redes[0]['network_id'].', '.$uid.', 1, 1, 1);');
                $db->query('INSERT INTO engine4_network_membership (resource_id, user_id, active, resource_approved, user_approved) VALUES(8, '.$uid.', 1, 1, 1);');
                $db->query('UPDATE engine4_network_networks AS a,
                        (
                            SELECT
                            a.resource_id,
                            COUNT( * ) AS cantidad
                            FROM engine4_network_membership AS a
                            JOIN engine4_users AS b ON ( a.user_id = b.user_id )
                            WHERE TRUE
                            GROUP BY a.resource_id
                        ) AS b
                        SET
                        a.member_count = b.cantidad
                        WHERE
                        a.network_id = b.resource_id;');
                $db->commit();
            }
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    }

    protected function crearRed($user_info)
    {
        $treea = explode(',', $user_info['distinguishedname']);

        $tree = array();
        foreach ($treea as $item) {
            $node = explode('=', $item);
            if (isset($node[0]) && ($node[0] == 'OU')) {
                $tree[] = $node[1];
            }
        }

        $this->country = $country = end($tree);
        try {
            echo Zend_Registry::get('Zend_Translate')->_('Create network country ...').'<br />';
            $db = Zend_Db_Table_Abstract::getDefaultAdapter();

            $stmt = $db->query("SELECT engine4_network_networks.* FROM engine4_network_networks WHERE engine4_network_networks.title='".$country."'");

            $rows = $stmt->fetchAll();

            if (is_array($rows) && (count($rows) == 0)) {
                $db->query("INSERT INTO engine4_network_networks (title, description) values('".$country."', '');");
            }
            $db->commit();
        } catch (Exception $e) {
        }
    }

    /**
     * Funcion para autenticar al usuario obtenido del servidor ldap.
     *
     * @param string $username  Usuario obtenido por el navegador
     * @param array  $user_info Informacion del usuario extraida del ldap
     */
    protected function autenticar($username, $user_info)
    {
        try {
            echo Zend_Registry::get('Zend_Translate')->_('make the auth proccess ...').'<br />';
            $db = Engine_Db_Table::getDefaultAdapter();
            $ipObj = new Engine_IP();
            $ipExpr = new Zend_Db_Expr($db->quoteInto('UNHEX(?)', bin2hex($ipObj->toBinary())));
            $user_table = Engine_Api::_()->getDbtable('users', 'user');
            $user_select = $user_table->select()->where('email = ?', $user_info['mail']);
            // If post exists
            $user = $user_table->fetchRow($user_select);
            $authResult = Engine_Api::_()->user()->authenticate($user_info['mail'], $user_info['whencreated']);
            $authCode = $authResult->getCode();
            Engine_Api::_()->user()->setViewer();
            $loginTable = Engine_Api::_()->getDbtable('logins', 'user');
            $loginTable->insert(array('user_id' => $user->getIdentity(), 'email' => $user_info['mail'], 'ip' => $ipExpr, 'timestamp' => new Zend_Db_Expr('NOW()'), 'state' => 'success', 'active' => true));
            $_SESSION['login_id'] = $login_id = $loginTable->getAdapter()->lastInsertId();
            Engine_Api::_()->getDbtable('statistics', 'core')->increment('user.logins');
            // Test activity @todo remove
            $viewer = Engine_Api::_()->user()->getViewer();

            if ($viewer->getIdentity()) {
                $viewer->lastlogin_date = date('Y-m-d H:i:s');

                if ('cli' !== PHP_SAPI) {
                    $viewer->lastlogin_ip = $ipExpr;
                }

                $viewer->save();
                Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($viewer, $viewer, 'login');
            }
        } catch (Exception $e) {
            print_r($e->getMessage().' a');
        }
    }

    /**
     * Funcion para Actualizar contrase�a e informacion basica de la tabla usuers.
     *
     * @param string $username  Usuario obtenido por el navegador
     * @param array  $user_info Informacion del usuario extraida del ldap
     */
    protected function actualizarContrasena($username, $user_info)
    {
        echo Zend_Registry::get('Zend_Translate')->_('Update user ...').'<br />';
        $userTable = Engine_Api::_()->getDbtable('users', 'user');
        $db = $userTable->getAdapter();
        $db->beginTransaction();

        try {
            $staticSalt = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.secret', 'staticSalt');
            $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            $password = '';
            $length = 8;
            $size = strlen($chars);

            /*for( $i = 0; $i < $length; $i++ ) {
            $password .= $chars[ rand( 0, $size - 1 ) ];
            }*/

            $password = $user_info['whencreated'];
            $salt = rand(1000000, 9999999);
            $db = Engine_Db_Table::getDefaultAdapter();
            $ipObj = new Engine_IP();
            $ipExpr = new Zend_Db_Expr($db->quoteInto('UNHEX(?)', bin2hex($ipObj->toBinary())));
            $data = array('username' => $username, 'displayname' => $user_info['displayname'], 'email' => $user_info['mail'], 'password' => md5($staticSalt.$password.$salt), 'salt' => $salt, 'enabled' => 1);
            $where = $userTable->getAdapter()->quoteInto("( LOWER( RTRIM(email ) ) = LOWER( RTRIM('".$user_info['mail']."') ) )");
            $userTable->update($data, $where);
            $db->commit();
        } catch (Exception $e) {
            print_r($e->getMessage().' b');
        }
    }

    /**
     * Insertar en la tabla users el usuario obtenido del ldap.
     *
     * @param string $username  Usuario obtenido por el navegador
     * @param array  $user_info Informacion del usuario extraida del ldap
     */
    protected function registrar($username, $user_info)
    {
        echo Zend_Registry::get('Zend_Translate')->_('Create user ...').'<br />';
        $userTable = Engine_Api::_()->getDbtable('users', 'user');
        $db = $userTable->getAdapter();
        $db->beginTransaction();

        try {
            $staticSalt = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.secret', 'staticSalt');
            $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            $password = '';
            $length = 8;
            $size = strlen($chars);

            /*for( $i = 0; $i < $length; $i++ ) {
            $password .= $chars[ rand( 0, $size - 1 ) ];
            }*/

            $password = $user_info['whencreated'];
            $salt = rand(1000000, 9999999);
            $db = Engine_Db_Table::getDefaultAdapter();

            /*$db_1 = Zend_Db_Table_Abstract::getDefaultAdapter();*/
            $ipObj = new Engine_IP();
            $ipExpr = new Zend_Db_Expr($db->quoteInto('UNHEX(?)', bin2hex($ipObj->toBinary())));

            $userTable->insert(
                array(
                    'email' => $user_info['mail'],
                    'username' => $username,
                    'displayname' => $user_info['name'],
                    'photo_id' => 0,
                    'password' => md5($staticSalt.$password.$salt),
                    'salt' => $salt,
                    'language' => 'es',
                    'level_id' => 4,
                    'creation_date' => date('Y-m-d H:i:s'),
                    'modified_date' => date('Y-m-d H:i:s'),
                    'creation_ip' => $ipExpr, 'enabled' => 1,
                    'verified' => 1,
                    'approved' => 1,
                    )
                );

            $this->new_user_id = $userTable->getAdapter()->lastInsertId();

            /*Delete From engine4_activity_notificationsettings*/

            /*$db_1->query("    INSERT INTO engine4_activity_notificationsettings
            (
            user_id,
            type,
            email
            )
            select
            user_id,
            b.type,
            0
            from
            engine4_activity_notificationtypes as b;");*/

            /*
            $db_1->query("INSERT INTO engine4_activity_notificationsettings (user_id, type, email) VALUES (".$uid.",'commented_commented','0');");
            $db_1->query("INSERT INTO engine4_activity_notificationsettings (user_id, type, email) VALUES (".$uid.",'event_discussion_reply','0');");
            $db_1->query("INSERT INTO engine4_activity_notificationsettings (user_id, type, email) VALUES (".$uid.",'friend_accepted','0');");
            $db_1->query("INSERT INTO engine4_activity_notificationsettings (user_id, type, email) VALUES (".$uid.",'friend_request','0');");
            $db_1->query("INSERT INTO engine4_activity_notificationsettings (user_id, type, email) VALUES (".$uid.",'group_discussion_reply','0');");
            $db_1->query("INSERT INTO engine4_activity_notificationsettings (user_id, type, email) VALUES (".$uid.",'liked','0');");
            $db_1->query("INSERT INTO engine4_activity_notificationsettings (user_id, type, email) VALUES (".$uid.",'liked_commented','0');");
            $db_1->query("INSERT INTO engine4_activity_notificationsettings (user_id, type, email) VALUES (".$uid.",'tagged','0');");*/

            $db->commit();

            /*$db_1->commit();*/
        } catch (Exception $e) {
            print_r($e->getMessage().' b');
        }
    }

    /**
     * Esta funcion se utiliza para que los administradores puedan seguir a todos los usuarios y viceversa.
     */
    protected function seguirTodosLosAdministradores()
    {
        if ($this->new_user_id == null) {
            return;
        }

        $userTable = Engine_Api::_()->getDbtable('users', 'user');
        $db = $userTable->getAdapter();
        $db->beginTransaction();
        $adminUsers = $userTable->fetchAll();
        $membership = Engine_Api::_()->getDbtable('users', 'user');

        foreach ($adminUsers as $key => $adminUser) {
            if ($adminUser->isAdmin()) {
                $admin_id = $adminUser['user_id'];
                $u_id = $this->new_user_id;

                $dbQuery = Zend_Db_Table_Abstract::getDefaultAdapter();
                $membership = $dbQuery->query('SELECT * FROM engine4_user_membership WHERE resource_id = '.$admin_id.' AND user_id = '.$u_id.';');
                $rows = $membership->fetchAll();

                if (count($rows) == 0) {
                    $dbQueryInsert = Zend_Db_Table_Abstract::getDefaultAdapter();
                    $dbQueryInsert->query('INSERT INTO engine4_user_membership (resource_id, user_id, active, resource_approved, user_approved) VALUES('.$admin_id.', '.$u_id.', 1, 1, 1);');
                    $dbQueryInsert->commit();
                }

                $dbQuery = Zend_Db_Table_Abstract::getDefaultAdapter();
                $membership = $dbQuery->query('SELECT * FROM engine4_user_membership WHERE resource_id = '.$u_id.' AND user_id = '.$admin_id.';');
                $rows = $membership->fetchAll();

                if (count($rows) == 0) {
                    $dbQueryInsert = Zend_Db_Table_Abstract::getDefaultAdapter();
                    $dbQueryInsert->query('INSERT INTO engine4_user_membership (resource_id, user_id, active, resource_approved, user_approved) VALUES('.$u_id.', '.$admin_id.', 1, 1, 1);');
                    $dbQueryInsert->commit();
                }
            }
        }
    }

    /**
     * Verificar el usuario en db por username.
     *
     * @param string $username Usuario obtenido por el navegador
     *
     * @return bool
     */
    protected function verifyUser($username)
    {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $stmt = $db->query('SELECT engine4_users.* FROM engine4_users WHERE engine4_users.username=\''.$username.'\'');
        $rows = $stmt->fetchAll();

        if (is_array($rows) && (count($rows) > 0)) {
            return true;
        }

        return false;
    }

    /**
     * Establece una conexi�n con un servidor LDAP en un determinado hostname.
     */
    protected function ldapConnect()
    {
        if (is_null($this->ldapcon)) {
            $this->ldapcon = ldap_connect($this->ldapserver) or die('no se encuentra el servidor');
        }
    }

    /**
     * Establece el valor de la opci�n especificada.
     */
    protected function ldapInitOptions()
    {
        $this->ldapConnect();
        ldap_set_option($this->ldapcon, LDAP_OPT_REFERRALS, 0);
        //ldap_set_option(NULL,LDAP_SCOPE_SUBTREE,3);
        ldap_set_option($this->ldapcon, LDAP_OPT_PROTOCOL_VERSION, 3);
    }

    /**
     * Autentica contra un servidor LDAP tomando un RDN (usuario) y contrase�a especificados.
     */
    protected function ldapBind()
    {
        if (is_null($this->bind)) {
            $this->ldapInitOptions();
            $this->bind = ldap_bind($this->ldapcon, $this->username, $this->password) or die('no puedo hacer bind');
        }
    }

    /**
     * Realiza la b�squeda de un filtro espec�fico sobre el directorio.
     *
     * @param string $username Usuario obtenido por el navegador
     */
    protected function ldapSearch($username){
        $dominio = trim($username).'@centelsa.com.co';
        $filter  = "(mail=$dominio)";
        $this->search  = ldap_search($this->ldapcon,"dc=centelsa,dc=cables,dc=xignux,dc=com", $filter);
    }
}
