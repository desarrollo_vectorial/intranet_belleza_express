<?php return array (
  'package' => 
  array (
    'type' => 'widget',
    'name' => 'ldapsso',
    'version' => '4.1.8p1',
    'path' => 'application/widgets/ldapsso',
    'title' => 'Ldap Single Sign On',
    'description' => 'Modulo, para ejecutar el single sign on contra ldap',
    'author' => 'm4user@gmail.com',
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'remove',
    ),
    'directories' => 
    array (
      0 => 'application/widgets/ldapsso',
    ),
  ),
  'type' => 'widget',
  'name' => 'ldapsso',
  'version' => '4.1.8p1',
  'title' => 'Ldap Single Sign On',
  'description' => 'Modulo, para ejecutar el single sign on contra ldap',
  'category' => 'Widgets',
); ?>