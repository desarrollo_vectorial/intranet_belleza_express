<?php return array (
  'package' => 
  array (
    'type' => 'widget',
    'name' => 'list-articles',
    'version' => '4.0.0',
    'path' => 'application/widgets/list-articles',
    'title' => 'List Articles',
    'description' => 'Show a list of articles',
    'author' => 'jrodriguez@vectorial.co',
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'remove',
    ),
    'directories' => 
    array (
      0 => 'application/widgets/list-articles',
    ),
  ),
  'type' => 'widget',
  'name' => 'list-articles',
  'version' => '4.0.0',
  'title' => 'List Articles',
  'description' => 'Show a list of articles',
  'category' => 'Widgets',
); ?>