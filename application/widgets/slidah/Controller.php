<?php

class Widget_SlidahController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
  	$url = $this->view->url();

  	switch ($url) {
  		case '/pagina/view/60':
  			$data = array(
		  		'url' => $url,
		  		'numberOfSlides' => 77,
		  		'pathToGetSlides' => 'sliderImages/pagina/informe2009/',
          'show' => true,
          'height' => 880
		  	);
        $this->view->data = $data;
  			break;

      case '/pagina/view/61':
        $data = array(
          'url' => $url,
          'numberOfSlides' => 109,
          'pathToGetSlides' => 'sliderImages/pagina/informe2010/',
          'show' => true,
          'height' => 880
        );
        $this->view->data = $data;
        break;
      
      case '/pagina/view/62':
        $data = array(
          'url' => $url,
          'numberOfSlides' => 64,
          'pathToGetSlides' => 'sliderImages/pagina/informe2011/',
          'show' => true,
          'height' => 370
        );
        $this->view->data = $data;
        break;

      case '/pagina/view/63':
        $data = array(
          'url' => $url,
          'numberOfSlides' => 88,
          'pathToGetSlides' => 'sliderImages/pagina/informe2012/',
          'show' => true,
          'height' => 880
        );
        $this->view->data = $data;
        break;

      default:
        return;
        break;
  	}
  }
}