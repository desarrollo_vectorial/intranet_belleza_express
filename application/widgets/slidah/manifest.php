<?php return array (
  'package' => 
  array (
    'type' => 'widget',
    'name' => 'slidah',
    'version' => '1.0.0',
    'path' => 'application/widgets/slidah',
    'title' => 'slidah',
    'description' => 'Slider de jquery cycle malsup',
    'author' => 'Andres Ramirez',
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'remove',
    ),
    'directories' => 
    array (
      0 => 'application/widgets/slidah',
    ),
  ),
  'type' => 'widget',
  'name' => 'slidah',
  'version' => '1.0.0',
  'title' => 'slidah',
  'description' => 'Slider de jquery cycle malsup',
  'category' => 'Widgets',
); ?>