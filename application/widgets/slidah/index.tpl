<?php

if(isset($this->data['show'])):

$numberOfSlides = $this->data['numberOfSlides'];
$pathToGetSlides = $this->data['pathToGetSlides'];
$height = $this->data['height'];

?>

<!--

width:740px
height:392px

-->
<style type="text/css">
	.center{
		text-align: center;
		margin-top: 15px;
	}

	.center a{
		padding: 5px 15px;
		background: #00A2FF;
		color: #FFFFFF;
		text-decoration: none;
		position: relative;
		top: -5px;
	}
</style>

<div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="0" data-cycle-prev="#prev" data-cycle-next="#next">

	<?php for($i = 1; $i <= $numberOfSlides; $i++): ?>
		<img src="<?php echo $pathToGetSlides . $i . '.jpg'; ?>" width="740" height="<?php echo $height; ?>" />
	<?php endfor; ?>

<div>

<div class="center">
	<a href="#" id="st-slidah">&laquo;&laquo;</a>
	<a href="#" id="prev">&laquo;</a> 
	<a href="#" id="next">&raquo;</a>
	<a href="#" id="ls-slidah">&raquo;&raquo;</a>
</div>

<script src="externals/cycle2/jquery.cycle2.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){

		var n = jQuery('.cycle-slideshow img').length-1;

		jQuery('#st-slidah').click(function(e){
			e.preventDefault();
			jQuery('.cycle-slideshow').cycle('goto', 0);
		});

		jQuery('#ls-slidah').click(function(e){
			e.preventDefault();
			jQuery('.cycle-slideshow').cycle('goto', n);
		});

	});
</script>

<?php endif; ?>