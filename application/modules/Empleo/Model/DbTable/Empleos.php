<?php
class Empleo_Model_DbTable_Empleos extends Engine_Db_Table
{
	protected $_rowClass = "Empleo_Model_Empleo";
	
	public function getEmpleosPaginator($params = array(), $customParams = array())
	{
    	$paginator = Zend_Paginator::factory($this->getEmpleosSelect($params, $customParams));
		if( !empty($params['page']) ) {
			$paginator->setCurrentPageNumber($params['page']);
		}
		if( !empty($params['limit']) ) {
			$paginator->setItemCountPerPage($params['limit']);
		}
		return $paginator;
	}
  
  public function getEmpleosSelect($params = array(), $customParams = array())
  {
	  $aspirantes = Engine_Api::_()->getDbtable('aspirantes', 'empleo');
	  /*Obtengo el total de Aspirantes por Oferta*/
	  $queryA = $aspirantes -> select()
	  						->from($aspirantes->info('name'),array('total'=>'count(*)','empleo_id'))
	  						->setIntegrityCheck(false)
	  						->group('empleo_id');
	  						
	  /*Obtengo si el usuario especifico ha aplicado a una oferta*/						
	  $queryB = $aspirantes -> select()
	  						->from($aspirantes->info('name'),array('aplicado'=>'count(*)','empleo_id'))
	  						->setIntegrityCheck(false)
	  						->where("aspirante_id = ?",$params['user_id'])
	  						->group('empleo_id');
	  
	  /*Obtengo el total de Preseleccionados por Oferta*/
	  $queryC = $aspirantes -> select()
	  						->from($aspirantes->info('name'),array('total_preseleccionado'=>'count(*)','empleo_id'))
	  						->setIntegrityCheck(false)
	  						->where("estado = ?",1)
	  						->group('empleo_id');
	  
	  						
	  $usuario = Engine_Api::_()->getItemTable('user');
	  
	  $tableName = $this->info('name');
	  $select = $this->select()
	  	->setIntegrityCheck(false)
	  	->from(array("a" => $tableName),array("a.*","created"=>"DATE_FORMAT(a.created,'%Y-%m-%d')","vigencia_oferta"=>"DATE_FORMAT(a.vigencia_oferta,'%Y-%m-%d')"))
	  	->joinLeft(array('b'=>$queryA),"a.empleo_id = b.empleo_id",array("total"=>"b.total"))
	  	->join(array("c"=>$usuario->info('name')),"a.user_id = c.user_id",array("displayname"=>"c.displayname"))
	  	->joinLeft(array('d'=>$queryB),"a.empleo_id = d.empleo_id",array("aplicado"=>"d.aplicado"))
	  	->joinLeft(array('e'=>$queryC),"a.empleo_id = e.empleo_id",array("preseleccionados"=>"e.total_preseleccionado"))
	  	->where("a.estado IN(".$params['estado'].")")
	  	->order('a.created DESC');
	  	
	  	if($params['empleo_id'])	
	  		$select ->where("a.empleo_id = ?",$params['empleo_id']);
	  		
	  	/*Buscador*/
	  	if($customParams['palabras_clave'])
	  		{
		  		$select ->where("a.descripcion LIKE '%".$customParams['palabras_clave']."%'");
		  		$select->orWhere("a.competencias LIKE '%".$customParams['palabras_clave']."%'");
	  		}
	  		
	  	if($customParams['cargo'])
	  		$select ->where("a.cargo LIKE '%".$customParams['cargo']."%'");
	  	if($customParams['sede'])
	  		$select ->where("a.ubicacion LIKE '%".$customParams['sede']."%'");
	  	if($customParams['fecha_inicial'])
	  		$select ->where("DATE_FORMAT(a.created,'%Y-%m-%d') >= DATE_FORMAT('".$customParams['fecha_inicial']."','%Y-%m-%d')");
	  	if($customParams['fecha_final'])
	  		$select ->where("DATE_FORMAT(a.created,'%Y-%m-%d') <= DATE_FORMAT('".$customParams['fecha_final']."','%Y-%m-%d')");
	  	if($customParams['fecha_inicial'])
	  		$select ->where("DATE_FORMAT(a.vigencia_oferta,'%Y-%m-%d') >= DATE_FORMAT('".$customParams['fecha_vigencia']."','%Y-%m-%d')");
	  	/*echo $select;*/
	  	/*exit;*/
	return $select;
  }
  
  public function delEmpleos($where)
	 {
		 $this->delete($where);
	 }
	 
	public function cambiarEstados()
	 {
		$fechaActual = date("Y-m-d");
		
		$data = array("estado"=>'0');
		$where = array();
		$where[] = "estado = 1";
		$where[] = "DATE_FORMAT('".$fechaActual."','%Y-%m-%d') > DATE_FORMAT(vigencia_oferta,'%Y-%m-%d') ";
		
		$empleos = Engine_Api::_()->getDbtable('empleos', 'empleo');
		$empleos -> update($data,$where);
	 }
}

?>