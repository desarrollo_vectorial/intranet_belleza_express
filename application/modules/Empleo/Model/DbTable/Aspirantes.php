<?php

class Empleo_Model_DbTable_Aspirantes extends Engine_Db_Table
{
	protected $_rowClass = "Empleo_Model_Aspirante";
	
	public function getAspirantes($params =array())
	{
	  $usuarios = Engine_Api::_()->getItemTable('user');
	  $aspirantes = Engine_Api::_()->getDbtable('aspirantes', 'empleo');
	  $query = $aspirantes -> select()
	  						->setIntegrityCheck(false)
	  						->from(array("a"=>$aspirantes->info('name')),array("a.*","created"=>"DATE_FORMAT(a.created,'%Y-%m-%d')"))
	  						->join(array("b"=>$usuarios->info('name')),"a.aspirante_id = b.user_id");
	  			if($params['empleo_id'])	
	  					$query ->where("a.empleo_id = ?",$params['empleo_id']);
	  			if($params['user_id'])
	  				$query ->where("a.aspirante_id = ?",$params['user_id']);
	  $rst = $query->query();
	  return $rst->fetchAll();
	 }
	 
	 public function setSeleccion($data,$where)
	 {
		 $aspirantes = Engine_Api::_()->getDbtable('aspirantes', 'empleo');
		 $aspirantes -> update($data,$where);
	 }
	 
	 public function delAspirantes($where)
	 {
		 $aspirantes = Engine_Api::_()->getDbtable('aspirantes', 'empleo');
		 $aspirantes -> delete($where);
	 }
	 
}
?>