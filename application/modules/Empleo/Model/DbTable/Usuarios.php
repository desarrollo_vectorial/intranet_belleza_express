<?php
class Empleo_Model_DbTable_Usuarios extends Engine_Db_Table
{
	protected $_rowClass = "Empleo_Model_Usuario";
	public function permisosModulo($viewer)
	{
		$usuarios = Engine_Api::_()->getDbtable('usuarios', 'empleo');
		$query = $usuarios -> select()
								->setIntegrityCheck(false)
								->where("user_id =?",$viewer->user_id);
		$rst = $query->query();
		$usuariosPermisos = $rst->fetchAll();
		//print_r($usuariosPermisos);
		if($usuariosPermisos)
			return $usuariosPermisos[0];
		
		return false;
	}
	
}

?>