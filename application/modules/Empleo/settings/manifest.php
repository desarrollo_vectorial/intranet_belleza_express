<?php return array (
  'package' => array (
    'type' 				=> 'module',
    'name' 				=> 'empleo',
    'version' 		=> '4.0.0',
    'path' 				=> 'application/modules/Empleo',
    'title' 			=> 'Modulo de Empleos',
    'description' => 'Modulo de Empleos',
    'author' 			=> 'mmedina@vectorial.co',
    'callback' 		=> array (
				'class' => 'Engine_Package_Installer_Module',
		),
		'actions' =>array (
				0 => 'install',
				1 => 'upgrade',
				2 => 'refresh',
				3 => 'enable',
				4 => 'disable',
		),
		'directories' => array (
			0 => 'application/modules/Empleo',
		),
		'files' => array (
			0 => 'application/languages/en/empleo.csv',
		),
	),
  'routes' => array(		
		'empleo_index_index' => array(
			'route' 	 => 'empleo/*',
			'defaults' => array(
				'module' 		 => 'empleo',
				'controller' => 'index',
				'action' 		 => 'listado',
			),
		),
		'empleo_index_create' => array(
			'route' 	 => 'empleo/create/*',
			'defaults' => array(
				'module' 		 => 'empleo',
				'controller' => 'index',
				'action' 		 => 'create',
			),
		),
		'empleo_index_edit' => array(
			'route' 	 => 'empleo/edit/:empleo_id',
			'defaults' => array(
				'module' 		 => 'empleo',
				'controller' => 'index',
				'action' 	 	 => 'create',
			),
		),
		'empleo_index_listado' => array(
			'route' 	 => 'empleo/listado/*',
			'defaults' => array(
				'module' 		 => 'empleo',
				'controller' => 'index',
				'action' 		 => 'listado',
			),
		),
		'empleo_index_proceso' => array(
			'route' 	 => 'empleo/proceso/:empleo_id',
			'defaults' => array(
				'module' 		 => 'empleo',
				'controller' => 'index',
				'action' 		 => 'proceso',
			),
		),
		'empleo_index_seleccion' => array(
			'route' 	 => 'empleo/seleccion/:estado/:empleo_id/:aspirante_id',
			'defaults' => array(
				'module' 		 => 'empleo',
				'controller' => 'index',
				'action' 	   => 'seleccion',
			),
		),
		'empleo_index_accion' => array(
			'route' 	 => 'empleo/accion/*',
			'defaults' => array(
				'module' 		 => 'empleo',
				'controller' => 'index',
				'action' 		 => 'accion',
			),
		),
		'empleo_index_aplicar' => array(
			'route' 	 => 'empleo/aplicar/*',
			'defaults' => array(
				'module' 		 => 'empleo',
				'controller' => 'index',
				'action' 		 => 'aplicar',
			),
		),
		'empleo_index_hojadevida' => array(
			'route'    => 'empleo/hojadevida/:id/*',
			'defaults' => array(
				'module' 		 => 'empleo',
				'controller' => 'index',
				'action' 		 => 'hojadevida',
			),
		),
		'empleo_index_reportes' => array(
			'route' 	 => 'empleo/reportes/*',
			'defaults' => array(
				'module' 		 => 'empleo',
				'controller' => 'index',
				'action' 		 => 'reportes',
			),
		),
		'empleo_index_reportesexcel' => array(
			'route' 	 => 'empleo/reportesexcel/*',
			'defaults' => array(
				'module' 		 => 'empleo',
				'controller' => 'index',
				'action' 		 => 'reportesexcel',
			),
		),
		'empleo_index_borrar' => array
		(
			'route' => 'empleo/borrar/:empleo_id',
			'defaults' => array
			(
				'module' => 'empleo',
				'controller' => 'index',
				'action' => 'borrar',
			),
		),



		//Rutas para las funciones de la sede
      'empleo_index_sede'                       => array(
        'route'    => 'empleo/admin-sede/*',
        'defaults' => array(
          'module'     => 'empleo',
          'controller' => 'index',
          'action'     => 'sedes',
        ),
      ),
      'empleo_index_createsede'                     => array(
        'route'    => 'empleo/create-sede/*',
        'defaults' => array(
          'module'     => 'empleo',
          'controller' => 'index',
          'action'     => 'createsede',
        ),
      ),
      'empleo_index_edit'                       => array(
        'route'    => 'empleo/edit-sede/:sede_id',
        'defaults' => array(
          'module'     => 'empleo',
          'controller' => 'index',
          'action'     => 'createsede',
        ),
      ),

      'empleo_index_delete'                     => array(
        'route'    => 'empleo/delete-sede/:id',
        'defaults' => array(
          'module'     => 'empleo',
          'controller' => 'index',
          'action'     => 'deletesede',
        ),
      ),




	),
); ?>