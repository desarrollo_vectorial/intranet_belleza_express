<?php
	return array(
	array(
		'title' => 'BUSCADOR EMPLEO',
		'description' => 'Permite la Busqueda de Vacantes en la Intranet',
		'category' => 'EMPLEO',
		'type' => 'widget',
		'name' => 'empleo.buscador',
		'defaultParams' => array('title' => 'Buscar empleo')
	),
	array(
		'title' => 'MENU EMPLEO',
		'description' => 'Menu del Modulo de Empleos',
		'category' => 'EMPLEO',
		'type' => 'widget',
		'name' => 'empleo.menu'
	),

	// ------- where I am
	array(
		'title'       => 'Donde estoy - EMPLEO',
		'description' => 'Muestra en que modulo se encuestra actualmente',
		'category'    => 'EMPLEO',
		'type'        => 'widget',
		'name'        => 'empleo.where-i-am'
	),

	// ------- where I am
	array(
		'title'       => 'Boton Administrador Sedes - EMPLEO',
		'description' => 'Boton para el administrador de crear, modificar, activar e inactivar sedes del modulo',
		'category'    => 'EMPLEO',
		'type'        => 'widget',
		'name'        => 'empleo.crear-sede-job'
	),
);