 <style>
.menuEmpleo { box-shadow: 0 0 5px #D1CFC8; border: 1px solid #D1CFC8; }
.menuEmpleo li { background: #ab1607; height: 60px; border-top: 0.001em solid white; padding-top: 10px; padding-left:20px; }
.menuEmpleo li a { display: block; padding: 15px 0 15px 25%; width: 80%; color: #FFF; text-decoration: none; font-size:16px; font-weight: normal; }
.menuEmpleo li.one a { background: url(../application/modules/Empleo/externals/images/0.png) no-repeat 7px center; }
.menuEmpleo li.two a { background: url(../application/modules/Empleo/externals/images/1.png) no-repeat 7px center; }
.menuEmpleo li.three a { background: url(../application/modules/Empleo/externals/images/2.png) no-repeat 7px center; }
.menuEmpleo li.four a { background: url(../application/modules/Empleo/externals/images/3.png) no-repeat 7px center; }
.menuEmpleo li.active { background: #780e04; border-bottom: none; }
.menuEmpleo li.active a { color: #FFF; }
 </style>
<ul class="menuEmpleo">
	
	<?php if($this->permisos['crear']) { ?>
		<li class="one <?php echo ((strcmp($this->baseUrl()."/empleo/create", $this->url())==0)? "active":"");?>">
			<a href="empleo/create"><?php echo $this->translate('Nuevo empleo'); ?></a>
		</li>
	<?php } ?>

	<li class="two <?php echo ((strcmp($this->baseUrl()."/empleo", $this->url())==0)? "active":"");?>">
		<a href="empleo"><?php echo $this->translate('Ofertas'); ?></a>
	</li>

	<li class="three <?php echo ((strcmp($this->baseUrl()."/empleo/profile", $this->url())==0)? "active":"");?> ">
		<a href="members/edit/profile" target="NEW"><?php echo $this->translate('Hoja de vida'); ?></a>
	</li>

	<?php if($this->permisos['exportar']){ ?>
		<li class="four <?php echo ((strcmp($this->baseUrl()."/empleo/reportes", $this->url())==0)? "active":"");?> ">
			<a href="empleo/reportes"><?php echo $this->translate('Reportes'); ?></a>
		</li>
	<?php } ?>

</ul>