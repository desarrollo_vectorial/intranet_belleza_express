<?php
	class Empleo_Widget_MenuController extends Engine_Content_Widget_Abstract
	{
		
		public function indexAction()
		{
			/*Zend_Debug::dump($this->view->url());
			Zend_Debug::dump($this->view->baseUrl());*/
			$viewer = Engine_Api::_()->user()->getViewer();
			$permisos=null;
			$permisos = Engine_Api::_()->getDbtable('usuarios', 'empleo')->permisosModulo($viewer);
			$this->view->permisos = $permisos;
		}
	}
?>