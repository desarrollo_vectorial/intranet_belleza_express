<?php

class Article_Widget_BotonDestacadosController extends Engine_Content_Widget_Abstract{

    public function indexAction(){
        $viewer = Engine_Api::_()->user()->getViewer();

        if ($viewer->isModSuperAdmin() == 0) {
            $this->view->showButtons = false;
        }else{
            $this->view->showButtons = true;
        }
    }
}
