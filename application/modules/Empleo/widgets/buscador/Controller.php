<?php
	class Empleo_Widget_BuscadorController extends Engine_Content_Widget_Abstract
	{
		
		public function indexAction()
		{
			$buscador = $_GET['buscador'];
			$this->view->palabras_clave = $buscador['palabras_clave'];
			$this->view->cargo = $buscador['cargo'];
			$this->view->sede = $buscador['sede'];
		}
	}
?>