<?php

class Empleo_Form_Accion extends Engine_Form
{
	public $_error = array();

	public function init()
	{
		/*Incializa Formulario*/
		$this->setAttrib('name', 'crear_accion');
		/*->setAttrib('onSubmit', "alert('ok'); return false;")*/
		 
		
		 $this->addElement('Button', 'submit', array(
		'label' => 'Aceptar',
		'type' => 'submit',
		'ignore' => true,
		'decorators' => array('ViewHelper')
		));
		
		$this->addElement('Cancel', 'cancel', array(
		'label' => 'Cancelar',
		'link' => true,
		'href' => 'javascript:void(0);',
		'onclick' => 'parent.Smoothbox.close();',
		'prependText' => ' or ',
		'decorators' => array('ViewHelper')
		));
	}
}

?>