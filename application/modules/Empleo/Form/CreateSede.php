<?php

Class Empleo_Form_CreateSede extends Engine_Form {

	public function init() {

		$this->setTitle('Crear Sede');
		

		$this->addElement('text', 'sedes_name', array(
			'label' 		=> 	'Nombre de la sede:',
			'allowEmpty'	=>	false,
			'required'		=> 	true
		));

		$this->addElement('Button', 'guardar', array(
			'label'  => 'Guardar',
			'type'   => 'submit',
			'ignore' => true,
			'decorators' => array('ViewHelper')
		));

	}
}