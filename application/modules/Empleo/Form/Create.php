<?php

class Empleo_Form_Create extends Engine_Form
{
	public $_error = array();

	public function init()
	{
		/*Incializa Formulario*/
		$this->setTitle('Crear Nueva Vacante')
		->setDescription('Pagina para la creación de Vacantes')
		->setAttrib('name', 'crear_empleo');
		/*->setAttrib('onSubmit', "alert('ok'); return false;")*/
		 
		$this->addElement('Text', 'vigencia_oferta', array(
		'label' => 'Vigencia de la oferta',
		'allowEmpty' => false,
		'required' => true,
		'readOnly' => true,
		'maxlength ' => 20		
		));
		
		 
		$this->addElement('Text', 'responsable', array(
		'label' => 'Responsable del proceso de selección',
		'allowEmpty' => false,
		'required' => true,
		'maxlength ' => 300		
		));
		
		$this->addElement('Text', 'cargo', array(
		'label' => 'Nombre del Cargo',
		'allowEmpty' => false,
		'required' => true,
		'maxlength ' => 300		
		));
		
		$this->addElement('Textarea', 'formacion_academica', array(
		'label' => 'Formación Académica',
		'allowEmpty' => false,
		'required' => true	
		));
		
		$this->addElement('Textarea', 'descripcion', array(
		'label' => 'Descripción',
		'allowEmpty' => false,
		'required' => true	
		));
		
		$this->addElement('Textarea', 'experiencia', array(
		'label' => 'Experiencia Requerida',
		'allowEmpty' => false,
		'required' => true	
		));	
		
		
		$this->addElement('Textarea', 'competencias', array(
		'label' => 'Competencias',
		'allowEmpty' => false,
		'required' => true		
		));	
		
		$this->addElement('Textarea', 'conocimientos', array(
		'label' => 'Conocimientos',
		'allowEmpty' => false,
		'required' => true		
		));		
		
		
		/*this->addElement('radio', 'test', array(
	    'label'=>'Test Thing',
	    'multiOptions'=>array(
	        'male' => 'Male',
	        'female' => 'Female',
							),
		));*/
		
		
		$this->addElement('Text', 'area', array(
		'label' => 'Área',
		'allowEmpty' => false,
		'required' => true,
		'maxlength ' => 300		
		));
		
		/*$this->addElement('Text', 'ubicacion', array(
		'label' => 'Ubicación',
		'allowEmpty' => false,
		'required' => true,
		'maxlength ' => 300		
		));*/
		$this->addElement('select', 'ubicacion', array(
			'label' 					=> 	'Sede:',
			'RegisterInArrayValidator' 	=> false,
			'allowEmpty' 				=> false,
			'required' 					=> true,
			'multiOptions'				=>	array(''=>'Seleccione la sede')
		));
		
		 $this->addElement('Button', 'submit', array(
		'label' => 'Archivar Empleo',
		'type' => 'submit',
		'ignore' => true,
		'decorators' => array('ViewHelper')
		));
		
		$this->addElement('Cancel', 'cancel', array(
		'label' => 'Cancelar',
		'link' => true,
		'prependText' => ' or ',
		'decorators' => array('ViewHelper')
		));
	}
}

?>