<?php
/*$this->headLink()
		->appendStylesheet($this->baseUrl().'/externals/jquery_ui/css/ui-lightness/jquery-ui-1.10.3.custom.min.css')
		->appendStylesheet($this->baseUrl().'/externals/jquery_ui/css/timepicker.css');
$this->headScript()->appendFile($this->baseUrl().'/externals/jquery_ui/js/jquery-ui-1.10.3.custom.js');
?>
<style> .mi-contenedor{ width: 100% !important; } </style>

<div class="mi-contenedor">  

<div class="reportModal" style="width: 700px; height: 300px;">
	<form method="post">
		<table  width="700px">
			<tr>
				<td colspan="4" style="text-align: center;">
					<h1>GENERAR REPORTE</h1>
				</td>
			</tr>
			<tr>
				<td><label>Fecha Inicial</label></td>
				<td><input type="text" name="fecha_inicial" name="fecha_inicial"></td>

				<td><label>Fecha Final</label></td>
				<td><input type="text" name="fecha_final" name="fecha_final"></td>
			</tr>
			<tr>
				<td><label>Ofertas</label></td>
				<td colspan="3">
					<p><input type="radio" name="buscador[ofertas]" value="all" checked> Todas</p>
					<p><input type="radio" name="buscador[ofertas]" value="1"> Activas</p>
					<p><input type="radio" name="buscador[ofertas]" value="2"> Archivadas</p>
					<p><input type="radio" name="buscador[ofertas]" value="3"> Canceladas</p>
					<p><input type="radio" name="buscador[ofertas]" value="0"> Finalizadas</p>
				</td>
			</tr>
			<tr>
				<td colspan="4" style="text-align: center;"><input type="submit" value="Generar reporte"></td>
			</tr>
		</table>
	</form>
</div >
<script>
    jQuery(function() {

        jQuery("#fecha_inicial").datepicker({
            maxDate   : 0,
            dateFormat: "yy-mm-dd",
            showOn    : "both"
        });

        jQuery("#fecha_final").datepicker({
            maxDate   : 0,                
            dateFormat: "yy-mm-dd",
            showOn    : "both"
        });
    });    
</script>

<?php */
$this->headLink()
     //->appendStylesheet($this->baseUrl().'/application/modules/Encuestas/externals/styles/materialize.min.css')
     ->appendStylesheet($this->baseUrl().'/externals/jquery_ui/css/ui-lightness/jquery-ui-1.10.3.custom.min.css')
     ->appendStylesheet($this->baseUrl().'/externals/jquery_ui/css/timepicker.css');

$this->headScript()->appendFile($this->baseUrl().'/externals/jquery_ui/js/jquery-ui-1.10.3.custom.js');
?>
<style> .mi-contenedor{ width: 100% !important; } </style>

<div class="mi-contenedor">     
    
    <h4 class="red-text text-darken-3">Generar Reporte de las interacciones de los usuarios</h4>
    <form id="exportar_csv" enctype="multipart/form-data" class="global_form" action="/estadistica" method="post">
        
        <div class="form-elements">
            <div id="fecha_inicio-wrapper" class="form-wrapper" style="display: inline-block;">
                <div id="fecha_inicio-label" class="form-label" >
                    <label for="fecha_inicio" class="required">Fecha de Inicio</label>
                </div>
                <div id="fecha_inicio-element" class="form-element">
                    <input type="text" id="fecha_inicio" name="fecha_inicio">
                </div>
            </div>
            <div id="limite_inscripcion-wrapper" class="form-wrapper" style="display: inline-block; padding-left: 50px;">
                <div id="limite_inscripcion-label" class="form-label"
                    <label for="limite_inscripcion" class="optional">Fecha límite</label>
                </div>
                <div id="limite_inscripcion-element" class="form-element">
                    <input type="text" id="limite_inscripcion" name="limite_inscripcion">
                </div>
            </div>

            <div class="input-field col s12">
                <button name="submit" id="submit" type="submit" class="waves-effect waves-light right btn red darken-4">EXPORTAR (.CSV)</button>
            </div>
        </div>

        
    </form>
    


    <!-- FLASH MESSAGES -->
    <script type="text/javascript">
        <?php if (!empty($this->popupMessage)): ?>
        <?php Engine_Api::_()->core()->unsetFlashMessage();?>
            var myPopUp = new PopUpWindow('<?php echo $this->popupMessage["title"]; ?>', <?php echo json_encode($this->popupMessage); ?>);
            myPopUp.open();
            setTimeout(function(){
                myPopUp.close();
            }, 5000);
        <?php endif;?>
    </script>
    <!-- FLASH MESSAGES -->
</div>


<script>
    jQuery(function() {

        jQuery("#fecha_inicio").datepicker({
            maxDate   : 0,
            dateFormat: "yy-mm-dd",
            showOn    : "both"
        });

        jQuery("#limite_inscripcion").datepicker({
            maxDate   : 0,                
            dateFormat: "yy-mm-dd",
            showOn    : "both"
        });
    });    
</script>