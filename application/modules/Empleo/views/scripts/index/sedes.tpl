<div style="float: right; display: inline-block; margin-top: 10px; margin-bottom: 10px;">
	<a href="/empleo/create-sede/"><h3 style="display: inline-block;">Crear Sede</h3></a> |
	<a href="/empleo/"><h3 style="display: inline-block;">Volver</h3></a>
</div>

<div style="display: inline-block; width: 100%;">
	<table width="100%" class="admin_table">
		<thead>
			<tr>
				<th>Sede</th>
				<th colspan="2">Accion</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			foreach($this->listaSedes as $sede){  ?>
				<tr>
					<td ><?php echo $sede['sedes_name']; ?></td>
					<td width="100px"> 
						<a href="empleo/edit-sede/<?php echo $sede['sede_id']; ?>">Modificar</a> 
					</td>
					<td width="100px"> 
						<a href="/empleo/delete-sede/<?php echo $sede['sede_id']; ?>"> 
							<?php if($sede['deleted'] == true){ echo 'Activar'; }else{ echo 'Eliminar'; } ?> 
						</a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>