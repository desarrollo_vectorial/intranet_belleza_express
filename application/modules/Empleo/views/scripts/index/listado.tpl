<?php
	$estado = array("0"=>"Cerrada", "1"=>"Abierta", "2"=>"Archivado", "3"=>"Cancelado", "4"=>"Aplazado"); 

	if(count($this->paginator)<1){ ?>
		<div class="tip">
			<span><?php echo $this->translate('No hay vacantes publicadas'); ?></span>
		</div>
	<?php } 

	foreach ($this->paginator as $key => $valor){ ?>
		<div class="columnasVacantes">
			<ul>
				<li class="columnaI">
					<?php echo $this->translate("Descripción del Cargo"); ?>
				</li>
				<li class="columnaII">
					<?php echo $this->translate("Perfil y funciones del aspirante"); ?>
				</li>
				<li class="columnaIII">
					<?php echo $this->translate("Vigencia de la oferta"); ?>
				</li>
				<li class="columnaIV">
					<?php echo $this->translate("Estado"); ?>
				</li>
			</ul>
		</div>
		<div class="infoVacante clearfix <?php echo $estado[$valor['estado']];?>">
			<ul class="columnaIVacante">
				<li class='cargo'>
					<?php echo $valor['cargo']; ?>
				</li>
				<li class='ubicacion'>
					<p><?php echo $valor['area']; ?></p>
					<?php echo $valor['ubicacion']; ?>
				</li>
				<li class='descripcion'>
					<?php echo $valor['descripcion']; ?>
				</li>
				<li class='responsable'>
					<p class="responsableVacante"><?php echo $this->translate("Responsable del proceso de selección: "); ?></p>
					<?php echo $valor['responsable']; ?>
				</li>
			</ul>
			<ul class="columnaIIVacante">
				<li>
					<p class="tituloVacante"><?php echo $this->translate("Formación académica :"); ?></p>
					<?php echo $valor['formacion_academica']; ?>
				</li>		
				<li>
					<p class="tituloVacante"><?php echo $this->translate("Experiencia Requerida:"); ?></p>
					<?php echo $valor['experiencia']; ?>
				</li>	
				<li>
					<p class="tituloVacante"><?php echo $this->translate("Conocimientos Específicos:"); ?></p>
					<?php echo $valor['conocimientos']; ?>
				</li>
				<li>
					<p class="tituloVacante"><?php echo $this->translate("Competencias:"); ?></p>
					<?php echo $valor['competencias']; ?>
				</li>
			</ul>		
			<ul class="columnaIIIVacante">
				<li>
					<p class="vigenciaVacante"><?php echo $valor['vigencia_oferta']; ?></p>
				</li>
			</ul>
			<ul class="columnaIVVacante">
				<?php if($valor['aplicado']){ ?>
					<li class="ofertaAplicada">¡Ya estás aplicando para este empleo!"</li>
				<?php }
				if($this->viewer->level_id < 5 || $this->viewer->level_id > 6)
				if(!$valor['aplicado'] && $valor['estado']=='1'){ ?>
					<li>
						<a href="empleo/aplicar/empleo_id/<?php echo $valor['empleo_id'];?>" class="smoothbox botonAplicar">Aplicar</a>
					</li>
				<?php }
				if($this->permisos['publicar'])
					if($valor['estado']=='2'){ ?>
						<li>
							<a href="empleo/accion/estado/1/empleo_id/<?php echo $valor['empleo_id'];?>" class="smoothbox botonPublicar">Publicar</a>
						</li>
				<?php }
				if($this->permisos['archivar'])
					if($valor['estado']=='1'){ ?>
						<li>
							<a href="empleo/accion/estado/2/empleo_id/<?php echo $valor['empleo_id'];?>" class="smoothbox botonArchivar">Archivar</a>
						</li>
				<?php }
				if($this->permisos['cerrar'])
					if($valor['estado']=='1' || $valor['estado']=='2'){ ?>
						<li>
							<a href="empleo/accion/estado/0/empleo_id/<?php echo $valor['empleo_id'];?>" class="smoothbox botonCerrar">Cerrar</a>
						</li>
				<?php }
				if($this->permisos['exportar']){ ?>
				<li>
					<a class="botonExcel botonExportar " href="empleo/reportesexcel/empleo_id/<?php echo $valor['empleo_id']?>" target="NEW">Exportar Excel</a>
				</li>
				<?php } ?>
			</ul>
		</div>
		<div class="footerVacantes">
			<ul>
				<?php if($this->permisos['editar']){ ?>
					<li class="columnaI">
						<i class="icon-editar"></i>
						<a href="empleo/edit/<?php echo $valor['empleo_id'];?>" class="botonEditar">Editar</a>
					</li>
				<?php }
				if($this->permisos['borrar']){ ?>
					<li class="columnaII">
						<i class="icon-delete"></i>
						<a href="empleo/borrar/<?php echo $valor['empleo_id'];?>" class="smoothbox botonBorrar">Borrar</a>
					</li>
				<?php } ?>
				<li class="columnaIII">
					<?php echo $this->translate("Publicado por:"); ?>
					<?php echo $valor['displayname']; ?>
				</li>
				<li class="columnaIV">
					<?php echo $this->translate("Fecha :"); ?>
					<?php echo $valor['created']; ?>
				</li>
				<?php if($this->permisos['preseleccionar']){ ?>
					<li class="columnaV">
						<a href="empleo/proceso/<?php echo $valor['empleo_id'];?>" id="aspirantesTotal">
							<?php echo intval($valor['total'])." &nbsp; "; ?>
							<?php echo $this->translate("Aspirantes"); ?>
						</a>
					</li>
				<?php } ?>
			</ul>
		</div>
		<div class='dotted'>&nbsp;</div>
	<?php } ?>

	<div><?php echo $this->paginationControl($this->paginator,null,null,array('pageAsQuery' => true,"query"=>array("buscador"=>$_GET['buscador']))); ?></div>

	<?php if($this->aspirantes){ ?>
		<div class="resumenAspirantes">
			<ul>
				<li id="totalAspirantes">
					<?php echo intval($valor['total']); ?>
					<p><?php echo $this->translate('Total Aspirantes'); ?></p>
				</li>
				<li id="totalPreseleccionados">
					<?php echo intval($valor['preseleccionados']); ?>
					<p><?php echo $this->translate('Preseleccionados'); ?></p>
				</li>
			</ul>
		</div>
		<br>
		<div class="aspirantes">
			<?php foreach($this->aspirantes  as $llave => $value){ ?>
				<div class="infoAspirante clearfix ">
					<ul class="fotoAspirante">
						<li>
							<?php echo $this->itemPhoto($this->user($value['aspirante_id']), 'thumb.profile'); ?>
						</li>
					</ul>
					<ul class="infoAspirantes">
						<li class="nombreAspirante">
							<?php echo $value['displayname']; ?>
						</li>
						<li class="fechaPostulacion">
							<?php echo $this->translate('Fecha en que se postuló este colaborador:'); ?>
							<p><?php echo $value['created']; ?></p>
						</li>
						<li class="opcionesAspirante">
							<ul>
								<li>
									<a href="mailto:<?php echo $value['email']."?subject=Vacante ".$valor['cargo']; ?>" target="NEW">
										<?php echo $this->translate('Contactar'); ?>
									</a>
								</li>
								<li>
									<a href="<?php echo $this->user($value['aspirante_id'])->getHref();?>" target="NEW">
										<?php echo $this->translate('Ver perfil'); ?>
									</a>
								</li>
								<li>
									<a href="empleo/hojadevida/<?php echo $value['aspirante_id'];?>" class="smoothbox">
										<?php echo $this->translate('Ver hoja de vida'); ?>
									</a>
								</li>
							</ul>
						</li>
					</ul>
					<ul class="opcionesProceso">
						<?php if($value['estado']==1){ ?>
							<li>
								<a id="botonPreseleccionado" class="smoothbox botonPreseleccionado">Preseleccionado</a>
							</li>
						<?php }
						if($value['estado']==2 || $value['estado']==0){ ?>
							<li>
								<a id="botonPreseleccionar" href="empleo/seleccion/1/<?php echo $valor['empleo_id']."/".$value['aspirante_id'];?>" class="smoothbox botonAplicar">Preseleccionar</a>
							</li>
						<?php }
						if($value['estado']==2 || $value['estado']==1){ ?>
							<li>
								<a id="botonDescartar" href="empleo/seleccion/0/<?php echo $valor['empleo_id']."/".$value['aspirante_id'];?>" class="smoothbox botonDescartado">Descartar</a>
							</li>
						<?php }
						if($value['estado']==0){ ?>
							<li>
								<a id="botonDescartado" class="smoothbox botonDescartado">Descartado</a>
							</li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
		</div>
	<?php } ?>