<div class="mi-contenedor">
	<?php $fc = Zend_Controller_Front::getInstance();
	$_baseUrl =  $fc->getBaseUrl(); ?>
	<!--<script type="text/javascript" src="<php echo $_baseUrl; ?>/externals/ckeditor_new/ckeditor.js"></script>
	$this->headLink()->appendStylesheet($this->baseUrl().'/externals/jquery_ui/css/ui-lightness/jquery-ui-1.10.3.custom.min.css');
	<script type="text/javascript" src="<php echo $_baseUrl; ?>/externals/jquery_ui/js/jquery-ui-1.10.3.custom.js"></script>-->
	<script>
		jQuery(document).ready(function() {
			jQuery("#vigencia_oferta").datepicker({
				dateFormat : "yy-mm-dd",
				changeMonth: true,
				changeYear : true,
				minDate    : "+1D"
			});
		});

		window.addEvents({
		// be sure to fire the event once the document is fully loaded
			load: function(){
				CKEDITOR.replace( 'competencias', {
					filebrowserBrowseUrl : '<?php echo $_baseUrl; ?>/externals/ckfinder/ckfinder.html',
					filebrowserImageBrowseUrl : '<?php echo $_baseUrl; ?>/externals/ckfinder/ckfinder.html?type=Images',
					filebrowserFlashBrowseUrl : '<?php echo $_baseUrl; ?>/externals/ckfinder/ckfinder.html?type=Flash',
					filebrowserUploadUrl : '<?php echo $_baseUrl; ?>/externals/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
					filebrowserImageUploadUrl : '<?php echo $_baseUrl; ?>/externals/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
					filebrowserFlashUploadUrl : '<?php echo $_baseUrl; ?>/externals/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
				});
			}
		}); 
    </script>
	<?php /*echo $this->form->setAttrib('class', 'global_form_popup')->render($this)*/ ?>
	<?php echo $this->form->render($this); ?>
</div>