<?php
	$Datfecha = date("Y-m-d");
	header("Content-Type: application/vnd.ms-excel;charset=iso-8859-1");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("content-disposition: attachment;filename=empleo_$Datfecha.xls");
?>
<HTML LANG="es">
<HEAD>
    <TITLE>::. Exportacion de Datos .::</TITLE>
</HEAD>
<body>
   <?php	/*(2)=> Archivado 1=>Publicado 3=> Cancelado 0=> Finalizado*/
   			/* 2=>Aplicante 0=>Descartado 1=>Seleccionado*/
		$estadosEmpleos = array(0=>"Cerrado",1=>"Publicado",2=>"Archivado");
		$estadosAspirantes = array(0=>"Descartado",1=>"Seleccionado",2=>"Aplicante");
		$html  = "	<table border=\"1\">";
		$html .= "		<tr>";
		$html .= "			<td>";
		$html .= "				empleo_id";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				responsable";
		$html .= "			</td>";	
		$html .= "			<td>";
		$html .= "				cargo";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				formacion_academica";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				descripcion";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				experiencia";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				competencias";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				conocimientos";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				area";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				ubicacion";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				estado";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				fecha_oferta";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				vigencia_oferta";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				total_aspirantes";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				publicado por";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				preseleccionados";
		$html .= "			</td>";
		$html .= "		</tr>";
		foreach($this->empleos as $key => $valor)
			{
				$paramsA = array("empleo_id" => $valor['empleo_id']);
				$aspirantes = Engine_Api::_()->getDbtable('aspirantes', 'empleo')->getAspirantes($paramsA);
		$html .= "		<tr>";
		$html .= "			<td>";
		$html .= "				".$valor['empleo_id'];
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".utf8_decode($valor['responsable']);
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".utf8_decode($valor['cargo']);
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".htmlentities(utf8_decode($valor['formacion_academica']));
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".htmlentities(utf8_decode($valor['descripcion']));
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".htmlentities(utf8_decode($valor['experiencia']));
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".$valor['competencias'];
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".htmlentities(utf8_decode($valor['conocimientos']));
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".utf8_decode($valor['area']);
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".utf8_decode($valor['ubicacion']);
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".$estadosEmpleos[$valor['estado']];
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".$valor['created'];
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".$valor['vigencia_oferta'];
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".intval($valor['total']);
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".utf8_decode($valor['displayname']);
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".intval($valor['preseleccionados']);
		$html .= "			</td>";
		$html .= "		</tr>";
		$html .= "			<td colspan=\"17\">";
				if($aspirantes)
					{
		$html .= "				<table border=\"1\">";
		$html .= "					<tr>";
		$html .= "						<td colspan=\"5\">";
		$html .= "							ASPIRANTES";
		$html .= "						</td>";
		$html .= "					</tr>";
		$html .= "					<tr>";
		$html .= "						<td>";
		$html .= "							nombre";
		$html .= "						</td>";
		$html .= "						<td>";
		$html .= "							email";
		$html .= "						</td>";
		$html .= "						<td>";
		$html .= "							fecha_postulacion";
		$html .= "						</td>";
		$html .= "						<td>";
		$html .= "							estado";
		$html .= "						</td>";
		$html .= "						<td>";
		$html .= "							enlace_perfil";
		$html .= "						</td>";
		$html .= "					</tr>";
						foreach($aspirantes as $llave => $value)
						{
		$html .= "					<tr>";
		$html .= "						<td>";
		$html .= "							".$value['displayname'];
		$html .= "						</td>";
		$html .= "						<td>";
		$html .= "							".$value['email'];
		$html .= "						</td>";
		$html .= "						<td>";
		$html .= "							".$value['created'];
		$html .= "						</td>";
		$html .= "						<td>";
		$html .= "							".$estadosAspirantes[$value['estado']];
		$html .= "						</td>";		
		$html .= "						<td>";
		$html .= "							".$this->user($value['aspirante_id'])->getHref();
		$html .= "						</td>";
		$html .= "					</tr>";
						}
		$html .= "				</table>";
					}	
		$html .= "			</td>";
		$html .= "		<tr>";
			}
		$html .= "	</table>";
		echo $html;
	?>
</body>
</html>