<?php

class Empleo_IndexController extends Core_Controller_Action_Standard{

	public function indexAction(){
		$this->view->someVar = 'someVal';
	}
	
	public function createAction(){
		$viewer    = Engine_Api::_()->user()->getViewer();
		$empleo_id = $this->_getParam('empleo_id');
		$permisos  = null;
		$permisos  = Engine_Api::_()->getDbtable('usuarios', 'empleo')->permisosModulo($viewer);
		if(!isset($viewer->user_id) || !$permisos['crear']){
				return $this ->_redirect('empleo/');
			}
		
		/*Pagina Actual*/
		$empleoTable = Engine_Api::_()->getDbtable('empleos', 'empleo');
		$empleoResultado = $empleoTable->find($empleo_id)->current();
		/*echo "<pre>".print_r($empleoResultado,true)."</pre>";*/
		
		
		
		$this->view->form = $form = new Empleo_Form_Create();
		$form->setAction($this->view->url());
		$this->_helper->content->setEnabled();
		
			$tablaSedes = $this->_helper->api()->getDbtable('sedes','empleo');
			$queryS = $tablaSedes->select()
							->order('sedes_name');
			$sedesR = $queryS->query();
			$sedesF = $sedesR->fetchAll();
			$sedes = array();
			foreach ($sedesF as $key => $value) 
			{
				$sedes[$value['sedes_name']] = $value['sedes_name'];
			}
			$form->ubicacion->addMultiOptions($sedes);
		
		
		if($this->getRequest()->isPost())
			if($form->isValid($this->getRequest()->getPost()))
			{
				$table = $this->_helper->api()->getDbtable('empleos', 'empleo');
				$db = $table->getAdapter();
				$db->beginTransaction();
				try
					{	
						if(!$empleoResultado)
							{
								$values = array_merge($form->getValues(), array('user_id' => $viewer->getIdentity(),'estado'=>'2','created'=>date('Y-m-d H:i:s')));
								$empleo = $table->createRow();
								$empleo->setFromArray($values);
								$empleo->save();
							}
							else
								{
									$values = array_merge($form->getValues(), array('estado'=>'2','modified'=>date('Y-m-d H:i:s')));
									$empleoResultado->setFromArray($values);
									$empleoResultado->save();
								}
						
						
					}
					catch( Exception $e )
						{
							$db->rollBack();
							throw $e;
						}
				$db->commit();
				return $this ->_redirect('empleo/');
			}
			else
				{
					return;
				}
			if($empleoResultado)
			{
				$form->vigencia_oferta->setValue($empleoResultado->vigencia_oferta);
				$form->responsable->setValue($empleoResultado->responsable);
				$form->cargo->setValue($empleoResultado->cargo);
				$form->formacion_academica->setValue($empleoResultado->formacion_academica);
				$form->descripcion->setValue($empleoResultado->descripcion);
				$form->experiencia->setValue($empleoResultado->experiencia);
				$form->competencias->setValue($empleoResultado->competencias);
				$form->conocimientos->setValue($empleoResultado->conocimientos);
				$form->area->setValue($empleoResultado->area);
				$form->ubicacion->setValue($empleoResultado->ubicacion);
			}
	}
	
	public function listadoAction(){
		$permisos = null;
		$viewer = Engine_Api::_()->user()->getViewer();
		Engine_Api::_()->getDbtable('empleos', 'empleo')->cambiarEstados();
		
		if(!isset($viewer->user_id))
				{
					return $this ->_redirect('members/home/');
				}
		$permisos = Engine_Api::_()->getDbtable('usuarios', 'empleo')->permisosModulo($viewer);
		$page=$this->_getParam('page',1);
		
		$buscador = $_GET['buscador'];
		

		
		$params = array('orderby' => 'created',
						"user_id"=>$viewer->getIdentity(),
						"estado" => "1"
						);
			$buscador['fecha_vigencia']=date("Y-m-d");
		if($permisos)
			{
				$params = array('orderby' => 'created',
						"user_id"=>$viewer->getIdentity(),
						"estado" => "1,2,3,0"
						);
				unset($buscador['fecha_vigencia']);
			}
		
		
		$this->view->permisos = $permisos;
		$this->view->viewer = $viewer;
		
		$this->view->paginator = Engine_Api::_()->getDbtable('empleos', 'empleo')->getEmpleosPaginator($params,$buscador);
		$this->view->paginator->setItemCountPerPage(5);
		$this->view->paginator->setCurrentPageNumber($page);
		$this->_helper->content->setEnabled(); 
	}
	
	public function procesoAction(){
		$viewer = Engine_Api::_()->user()->getViewer();
		$this->_helper->viewRenderer('index/listado', null, true);
		$empleo_id=$this->_getParam('empleo_id');
		$permisos=null;
		$permisos = Engine_Api::_()->getDbtable('usuarios', 'empleo')->permisosModulo($viewer);
		if(!isset($viewer->user_id) || !$permisos['preseleccionar'])
			{
				return $this ->_redirect('empleo/');
			}
			
		$permisos = 1;
		$page=$this->_getParam('page',1);
			
		$paramsE = array('orderby' => 'created',
				"user_id"=>$viewer->getIdentity(),
				"estado" => "1,2,3,0",
				"empleo_id" => $empleo_id);
		$paramsA = array("empleo_id" => $empleo_id);
		
		$this->view->permisos = $permisos;
		$this->view->paginator = Engine_Api::_()->getDbtable('empleos', 'empleo')->getEmpleosPaginator($paramsE);
		$this->view->aspirantes = Engine_Api::_()->getDbtable('aspirantes', 'empleo')->getAspirantes($paramsA);
		$this->view->paginator->setItemCountPerPage(5);
		$this->view->paginator->setCurrentPageNumber($page);
		
		$this->_helper->content->setEnabled(); 
	}
	
	public function seleccionAction(){
			$this->_helper->layout->setLayout('default-simple');
			$viewer = Engine_Api::_()->user()->getViewer();
			$estado=$this->_getParam('estado');
			$empleo_id=$this->_getParam('empleo_id');
			$aspirante_id=$this->_getParam('aspirante_id');
			$permisos=null;
			$permisos = Engine_Api::_()->getDbtable('usuarios', 'empleo')->permisosModulo($viewer);
			if(!isset($viewer->user_id) || !$permisos['preseleccionar'])
			{
				/*$this->_helper->layout->disableLayout();*/
				$this->_helper->viewRenderer->setNoRender(TRUE);
				echo "Acceso Denegado";
				return;
			}
			
			$data = array("estado"=>$estado);
			/*$this->getAdapter()->quoteInto('user_id = ?', $form['id']);*/
			$where = array();
			/*$where[] = $this->getAdapter()->quoteInto('empleo_id = ?', $empleo_id);
			$where[] = $this->getAdapter()->quoteInto('aspirante_id = ?', $aspirante_id);*/
			
			$where[] = "empleo_id = $empleo_id";
			$where[] = "aspirante_id = $aspirante_id";
							
			Engine_Api::_()->getDbtable('aspirantes', 'empleo')->setSeleccion($data,$where);
			
			
			return $this->_forward('success', 'utility', 'core', array(
						'smoothboxClose' => 1000,
						'parentRefresh' => 1000,
						'messages' => array('Acción Realizada')
						));
			
	}
	
	public function accionAction(){
		$this->_helper->layout->setLayout('default-simple');
		$viewer = Engine_Api::_()->user()->getViewer();
		$permisos=null;
		$permisos = Engine_Api::_()->getDbtable('usuarios', 'empleo')->permisosModulo($viewer);
		if(!isset($viewer->user_id) || !$permisos)
			{
				/*$this->_helper->layout->disableLayout();*/
				$this->_helper->viewRenderer->setNoRender(TRUE);
				echo "Acceso Denegado";
				return;
			}
			$estado=$this->_getParam('estado');
			$empleo_id=$this->_getParam('empleo_id');
			
			$empleoTable = Engine_Api::_()->getDbtable('empleos', 'empleo');
			$empleoResultado = $empleoTable->find($empleo_id)->current();
			
			$mensaje = array(1=>"PUBLICAR",2=>"ARCHIVAR",0=>"CERRAR");
		
			if (!$this->getRequest()->isPost()){      
				$this->view->form = $form = new Empleo_Form_Accion();
				$form->setTitle('Confirma '.$mensaje[$estado].' la vacante?');
				$form->setAction($this->view->url());
			}else{
				$table = $this->_helper->api()->getDbtable('empleos', 'empleo');
				$db = $table->getAdapter();
				$db->beginTransaction();
				try
					{
						$values = array('estado' => $estado,'modified'=>date('Y-m-d H:i:s'));
						$empleoResultado->setFromArray($values);
						$empleoResultado->save();
						$db->commit();
						
						return $this->_forward('success', 'utility', 'core', array(
						'smoothboxClose' => 2000,
						'parentRefresh' => 2000,
						'messages' => array('Acción Realizada')
						));
					}
					catch( Exception $e )
						{
							$db->rollBack();
							throw $e;
						}
			}
			return;
	}
	
	public function aplicarAction() {
			$this->_helper->layout->setLayout('default-simple');
			$viewer = Engine_Api::_()->user()->getViewer();
			if(!isset($viewer->user_id))
				{
					/*$this->_helper->layout->disableLayout();*/
					$this->_helper->viewRenderer->setNoRender(TRUE);
					echo "Acceso Denegado";
					return;
				}
			$empleo_id=$this->_getParam('empleo_id');
			$this->view->mensaje = $mensaje[$estado];
			$params = array("empleo_id"=>$empleo_id,"user_id"=>$viewer->getIdentity());
			$aspirante=Engine_Api::_()->getDbtable('aspirantes', 'empleo')->getAspirantes($params);
		
			if($aspirante){
				$this->render('aplicarF');
				return;
			}
		
			if (!$this->getRequest()->isPost())
			{      
				$this->view->form = $form = new Empleo_Form_Aplicar();
				$form->setTitle('Aplicando al empleo deseado');
				$form->setAction($this->view->url());
			}else{
				$table = $this->_helper->api()->getDbtable('aspirantes', 'empleo');
				$db = $table->getAdapter();
				$db->beginTransaction();
				try
				{
					$values = array(	'aspirante_id' => $viewer->getIdentity(),
										'empleo_id'=>$empleo_id,
										'estado'=>'2',
										'created'=>date('Y-m-d H:i:s'));
					$empleo = $table->createRow();
					$empleo->setFromArray($values);
					$empleo->save();
					$db->commit();
					
					return $this->_forward('success', 'utility', 'core', array(
					'smoothboxClose' => 5000,
					'parentRefresh' => 5000,
					'messages' => array('Ha aplicado exitosamente a la oferta de empleo. A partir de este momento la información sobre el proceso de selección será enviada a su correo electrónico.')
					));
				}
				catch( Exception $e )
					{
						$db->rollBack();
						throw $e;
					}
				}
			return;	
	}
		
	public function hojadevidaAction(){
	  $this->_helper->layout->setLayout('default-simple');
	  $id = $this->_getParam('id');
	  $subject = Engine_Api::_()->user()->getUser($id);
	  Engine_Api::_()->core()->setSubject($subject);
	  echo $this->view->content()->renderWidget("user.profile-fields");	  
	}
  
	public function reportesAction(){
		$this->_helper->content->setEnabled();

		$viewer   = Engine_Api::_()->user()->getViewer();
		$permisos = null;
		$permisos = Engine_Api::_()->getDbtable('usuarios', 'empleo')->permisosModulo($viewer);
		
		if(!isset($viewer->user_id) || !$permisos['exportar']){
			echo "Acceso Denegado";
			return;
		}

		$empleo_id = $this->_getParam('empleo_id');
		if($_POST || $empleo_id) {
			$this->_helper->layout->disableLayout();
			$buscador = $_POST['buscador'];
			$estado   = $buscador['ofertas'];
			if($buscador['ofertas']=='all' || $buscador['ofertas']==''){
				$estado = "1,2,3,0";
			}

			$params = array(
				"orderby"   => "created",
				"user_id"   => $viewer->getIdentity(),
				"estado"    => $estado,
				"empleo_id" => $empleo_id
			);

			$query 				 = Engine_Api::_()->getDbtable('empleos', 'empleo')->getEmpleosSelect($params,$buscador);
			$rst 				 = $query->query();
			$empleos 			 = $rst->fetchAll();
			$this->view->empleos = $empleos;
			$this->renderScript('index/excel.tpl');
		}	
	}


	public function reportesexcelAction(){
		$this->_helper->layout->setLayout('default-simple');
		$viewer = Engine_Api::_()->user()->getViewer();
		$permisos=null;
		$permisos = Engine_Api::_()->getDbtable('usuarios', 'empleo')->permisosModulo($viewer);
		if(!isset($viewer->user_id) || !$permisos['exportar']){
			/*$this->_helper->layout->disableLayout();*/
			$this->_helper->viewRenderer->setNoRender(TRUE);
			echo "Acceso Denegado";
			return;
		}
		
		$empleo_id = $this->_getParam('empleo_id');
		if($_POST || $empleo_id) {
			$this->_helper->layout->disableLayout();
			$buscador = $_POST['buscador'];
			$estado = $buscador['ofertas'];
			if($buscador['ofertas']=='all' || $buscador['ofertas']=='')
				{
					$estado = "1,2,3,0";
				}
			
			$params = array('orderby' => 'created',
					"user_id"=>$viewer->getIdentity(),
					"estado" => $estado,
					"empleo_id" => $empleo_id
					);
					
			$query = Engine_Api::_()->getDbtable('empleos', 'empleo')->getEmpleosSelect($params,$buscador);
			/*echo $query;*/
			$rst = $query->query();
			$empleos = $rst->fetchAll();
			$this->view->empleos = $empleos;
			$this->renderScript('index/excel.tpl');
		}	
  	}





	public function borrarAction(){
		$this->_helper->layout->setLayout('default-simple');
		$viewer = Engine_Api::_()->user()->getViewer();
		$permisos=null;
		$permisos = Engine_Api::_()->getDbtable('usuarios', 'empleo')->permisosModulo($viewer);
		if(!isset($viewer->user_id) || !$permisos['borrar']) {
		/*$this->_helper->layout->disableLayout();*/
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo "Acceso Denegado";
		return;
		}
		$empleo_id=$this->_getParam('empleo_id');

		if (!$this->getRequest()->isPost()){      
		$this->view->form = $form = new Empleo_Form_Accion();
		$form->setTitle('Confirma eliminar la oferta de empleo?(Ésta acción no se puede des-hacer)');
		$form->setAction($this->view->url());
		}else{		
		$where = "empleo_id = $empleo_id";
		Engine_Api::_()->getDbtable('empleos', 'empleo')->delEmpleos($where);
		Engine_Api::_()->getDbtable('aspirantes', 'empleo')->delAspirantes($where);

		return $this->_forward('success', 'utility', 'core', array(
		'smoothboxClose' => 1500,
		'parentRefresh' => 1500,
		'messages' => array('Acción Realizada')
		));
		}
	}

	//Bloque de funciones para las sedes
	//Index
	public function sedesAction(){
		$table                  = Engine_Api::_()->getDbtable('sedes', 'empleo');
		$this->view->listaSedes = $table->fetchAll();
	}

	//Crear sede
	public function createsedeAction(){
		$this->view->form = new Empleo_Form_CreateSede();

		$sede_id   = $this->_getParam('sede_id');
		$sedeTable = Engine_Api::_()->getDbtable('sedes', 'empleo');
		$sede      = $sedeTable->find($sede_id)->current();

		// Para guardar o actualizar una sede
		if ($this->getRequest()->isPost()) {

		if ($this->view->form->isValid($this->getRequest()->getPost())) {

		$db = $sedeTable->getAdapter();
		$db->beginTransaction();

		try {
		$values = $this->view->form->getValues();

		if (!$sede) {
		$sede = $sedeTable->createRow();
		$sede->setFromArray($values);
		$sede->save();

		} else {
		$sede->setFromArray($values);
		$sede->save();
		}

		} catch (Exception $e) {
		$db->rollBack();
		throw $e;
		}

		$db->commit();
		return $this->_redirect("/empleo/admin-sede");
		}
		}

		// Para editar una sede
		if ($sede) {
		$this->view->form->populate($sede->toArray());
		}
	}

	//Elimimar sedes
	public function deletesedeAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$id     = $this->_getParam('id');
		$table  = Engine_Api::_()->getDbtable('sedes', 'empleo');
		$select = $table->select()->where("sede_id = ? ", $id);
		$sede   = $table->fetchRow($select);
		$sede->deleted = !$sede->deleted;
		$sede->save();
		$this->_redirect("/empleo/admin-sede");
		}
	}