<div id="custom_indicators">
	<div class="title">INDICADORES ECONÓMICOS</div>
	
	<div class="indicators-list">
		<?php foreach($this->indicators as $indicator): ?>
			<?php
				$indicator_class = '';
				$indicator_image = '';
				if($indicator['value_status'] == 1) {
					$indicator_class = 'indicator-up';
					$indicator_image = '<img src="application/modules/Indicators/externals/images/arrow-up.png" />';
				} elseif($indicator['value_status'] == 2) {
					$indicator_class = 'indicator-down';
					$indicator_image = '<img src="application/modules/Indicators/externals/images/arrow-down.png" />';
				}
			?>
			<div>
				<div><?php echo $indicator['name']; ?></div>
				<div class="<?php echo $indicator_class; ?>"><?php echo $indicator['value']; ?></div>
				<div><?php echo $indicator_image; ?></div>
			</div>
		<?php endforeach; ?>
	</div>
</div>