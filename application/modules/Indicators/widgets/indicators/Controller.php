<?php
class Indicators_Widget_IndicatorsController extends Engine_Content_Widget_Abstract
{
	public function indexAction(){
		$table = Engine_Api::_()->getDbtable('lists', 'indicators');
		$select = $table->select()->order('orden');
		$this->view->indicators = $table->fetchAll($select)->toArray();
   }
}