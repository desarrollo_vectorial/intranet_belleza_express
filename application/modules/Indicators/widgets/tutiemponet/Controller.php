<?php
class Indicators_Widget_TutiemponetController extends Engine_Content_Widget_Abstract
{
	public function indexAction(){
		$table = Engine_Api::_()->getDbtable('tutiemponets', 'indicators');
		$select = $table->select()->order('orden');
		$this->view->tutiempo_widgets = $table->fetchAll($select)->toArray();
   }
}