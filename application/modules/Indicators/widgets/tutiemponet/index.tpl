<div id="weather-widget">
	<div class="title">CLIMA</div>
	<select onchange="showWidgetTime(this.value)" class="nice-select">
		<?php foreach($this->tutiempo_widgets as $weather_widget): ?>
			<option value="<?php echo $weather_widget['tutiemponet_id']; ?>"><?php echo $weather_widget['city']; ?></option>
		<?php endforeach; ?>
	</select>
	<div id="list-weather-widgets">
		<?php foreach($this->tutiempo_widgets as $index => $weather_widget): ?>
			<div id="weather_wdg_<?php echo $weather_widget['tutiemponet_id']; ?>" <?php echo $index > 0 ? 'style="display:none"' : ''; ?> class="weather-box">
				<?php echo $weather_widget['data']; ?>
			</div>
		<?php endforeach; ?>
	</div>
</div>

<script>
	function showWidgetTime(id) {
		
		<?php foreach($this->tutiempo_widgets as $weather_widget): ?>
			jQuery('#weather_wdg_<?php echo $weather_widget['tutiemponet_id']; ?>').hide();
		<?php endforeach; ?>
		
		jQuery('#weather_wdg_'+id).show();
	}	
</script>