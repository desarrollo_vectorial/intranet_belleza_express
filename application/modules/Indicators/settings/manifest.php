<?php

return array(
	'package' => array(
		'type' 		  => 'module',
		'name' 		  => 'indicators',
		'version' 	  => '1.0.0',
		'path' 		  => 'application/modules/Indicators',
		'title' 	  => 'Módulo indicadores',
		'description' => 'Módulos indicadores',
		'author' 	  => 'vectorial@vectorial.co',
		'callback' 	  => array(
			'class'   => 'Engine_Package_Installer_Module',
		),
		'actions' => array(
			0 => 'install',
			1 => 'upgrade',
			2 => 'refresh',
			3 => 'enable',
			4 => 'disable',
		),
		'directories' => array(
			0 => 'application/modules/Indicators',
		),
	),
	'routes' => array(
        'indicators_admin' => array(
            'route'    => 'indicators/admin/:action',
            'defaults' => array(
                'module' 	 => 'indicators',
                'controller' => 'admin-settings',
                'action' 	 => 'index',
            ),
            'reqs' => array('action' => '(index|newindicator)'),
        ),	        
        'weather_admin' => array(
            'route'    => 'weather/admin/:action',
            'defaults' => array(
                'module' 	 => 'indicators',
                'controller' => 'admin-weather',
                'action' 	 => 'index',
            ),
            'reqs' => array('action' => '(index|newindicator)'),
        ),	        
	),	
	'items' => array('indicators_lists','indicators_tutiemponets'),
);