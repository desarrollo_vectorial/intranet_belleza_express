<?php

return array(
    array(
        'title'       => 'Indicadores',
        'description' => 'Indicadores',
        'category'    => 'Indicadores',
        'type'        => 'widget',
        'name'        => 'indicators.indicators',
    ),
    array(
        'title'       => 'TuTiempo.Net',
        'description' => 'Estado del clima',
        'category'    => 'Indicadores',
        'type'        => 'widget',
        'name'        => 'indicators.tutiemponet',
    )
);