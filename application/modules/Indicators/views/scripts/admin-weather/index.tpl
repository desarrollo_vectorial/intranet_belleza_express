<div id="user-admin-box" class="">
	<div class="admin-title">Indicador del clima</div>
	
	<div class="layout_left">
	    <div class="side-left-box">
	        <div class="side-left-box--titulo">
	            <span><strong>Nueva</strong> ciudad</span>
	        </div>
	        
	        <?php echo $this->formNewIndicator->render($this) ?>
	
	    </div>
	</div>
	
	<?php if ($this->form): ?>
		<div class="table-form">
			<table>
				<tr>
					<th>Ciudad</th>
					<th>Código (Tutiempo.net)</th>
					<th>Orden</th>
				</tr>
			</table>
		    <?php echo $this->form->render($this) ?>
	    </div>
	<?php endif ?>
</div>