<div id="user-admin-box" class="">
	<div class="admin-title">Indicadores</div>
	
	<div class="layout_left">
	    <div class="side-left-box">
	        <div class="side-left-box--titulo">
	            <span><strong>Nuevo</strong> indicador</span>
	        </div>
	        
	        <?php echo $this->formNewIndicator->render($this) ?>
	
	    </div>
	</div>
	
	<?php if ($this->form): ?>
		<div class="table-form">
			<table>
				<tr>
					<th>Indicador</th>
					<th>Valor</th>
					<th>Estado</th>
					<th>Orden</th>
				</tr>
			</table>
		    <?php echo $this->form->render($this) ?>
	    </div>
	<?php endif ?>
</div>