<?php
class Indicators_Form_Newindicator extends Engine_Form
{
    public $_error = array();
	private $indicator_value_status = array();
	
	public function __construct($indicator_value_status) {
	    Engine_FOrm::enableForm($this);
	    self::enableForm($this);

		$this->indicator_value_status = $indicator_value_status;
	
	    parent::__construct();
	}

    public function init()
    {
        $this
            ->setAttribs(array(
                'class' => 'side-form',
        ));
    	
        $this->addElement('Text', 'name', array(
            'label' => 'Indicador',
            'placeholder' => 'Indicador',
            'allowEmpty' => false,
            'required' => true,
        ));
        $this->addElement('Text', 'value', array(
            'label' => 'Valor',
            'placeholder' => 'Valor',
            'allowEmpty' => false,
            'required' => true,
        ));
        $this->addElement('Select', 'value_status', array(
            'label' => 'Estado',
            'multiOptions' => $this->indicator_value_status,
            'allowEmpty' => false,
            'required' => true,
        ));
         $this->addElement('Button', 'submit', array(
            'label' => 'Guardar',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array('ViewHelper'),
        ));
    }
}