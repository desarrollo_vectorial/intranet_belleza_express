<?php
class Indicators_Form_Weather_Newindicator extends Engine_Form
{
    public $_error = array();
	
    public function init()
    {
        $this
            ->setAttribs(array(
                'class' => 'side-form',
        ));
    	
        $this->addElement('Text', 'city', array(
            'label' => 'Ciudad',
            'placeholder' => 'Ciudad',
            'allowEmpty' => false,
            'required' => true,
        ));
        $this->addElement('Text', 'data', array(
            'label' => 'Código',
            'placeholder' => 'Código',
            'allowEmpty' => false,
            'required' => true,
        ));
         $this->addElement('Button', 'submit', array(
            'label' => 'Guardar',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array('ViewHelper'),
        ));
    }
}