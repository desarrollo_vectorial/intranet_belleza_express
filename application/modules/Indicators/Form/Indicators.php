<?php
class Indicators_Form_Indicators extends Engine_Form
{
    public $_error = array();
	private $indicators = array();
	
	public function __construct($indicators) {
	    Engine_FOrm::enableForm($this);
	    self::enableForm($this);

		$this->indicators = $indicators;
	
	    parent::__construct();
	}

    public function init()
    {
        $this->setTitle('')
            ->setDescription('')
            ->setAttrib('name', 'indicators');

	    $this
	      ->setAttribs(array(
	        'class' => 'global-form',
	      ))
	       ->setMethod('POST')
	            ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()));

       $this->setDecorators(array(
                    'FormElements',
                    array('HtmlTag',array('tag'=>'table', 'width'=>'100%', 'cellspacing' => '0', 'cellpadding'=>'0')),
                    'Form'
                ));


		$subForm = new Zend_Form_SubForm();

		foreach($this->indicators as $rownum => $row){
		  $id = $row['list_id'];
		  $rowForm = new Zend_Form_SubForm();
		  foreach($row as $key => $value){
		    if($key == 'list_id' || $key == 'modified_date') continue;
			 
			if($key == 'value_status') {
			    $rowForm->addElement(
			      'select',
			      $key,
			      array(
			        'value' => $value,
			      )
			    );								
			} else {
			    $rowForm->addElement(
			      'text',
			      $key,
			      array(
			        'value' => $value,
			      )
			    );				
			}
		  }
		  
		  $rowForm->value_status->setMultiOptions( array(0=>'Igual', 1 => 'Subió', 2 => 'Bajó') );
		  
		  $rowForm->setElementDecorators(array(
		    'ViewHelper',
		    'Errors',
		    array('HtmlTag', array('tag' => 'td')),
		  ));
		
		  $subForm->addSubForm($rowForm, $id);
		}
		
		$subForm->setSubFormDecorators(array(
		  'FormElements',
		  array('HtmlTag', array('tag'=>'tr')),
		));
		 
		$this->addSubForm($subForm, 'indicators');
		
		$this->setSubFormDecorators(array(
		  'FormElements',
		  array('HtmlTag', array('tag' => 'tbody')),
		));
		 
		$this->setDecorators(array(
		    'FormElements',
		    array('HtmlTag', array('tag' => 'table')),
		    'Form'
		));
 
		$this->addElement(
		  'submit', 'submit', array('label' => 'Actulizar'));
		
		$this->submit->setDecorators(array(
		    array(
		        'decorator' => 'ViewHelper',
		        'options' => array('helper' => 'formSubmit')),
		    array(
		        'decorator' => array('td' => 'HtmlTag'),
		        'options' => array('tag' => 'td', 'colspan' => 4)
		        ),
		    array(
		        'decorator' => array('tr' => 'HtmlTag'),
		        'options' => array('tag' => 'tr')),
		));

    }
}