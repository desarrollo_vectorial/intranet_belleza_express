<?php

 /**
* SocialEngine
*
* @category   Application_Extensions
* @package    Birthday
* @copyright  Copyright 2009-2010 BigStep Technologies Pvt. Ltd.
* @license    http://www.socialengineaddons.com/license/
* @version    $Id: AdminSettingsController.php 6590 2010-17-11 9:40:21Z SocialEngineAddOns $
* @author     SocialEngineAddOns
*/
class Indicators_AdminSettingsController extends Core_Controller_Action_Standard
{
  private $indicator_value_status = array(0 => 'Igual', 1 => 'Subió', 2 => 'Bajó');
  
  public function indexAction()
  {
		$table = Engine_Api::_()->getDbtable('lists', 'indicators');
		$select = $table->select()->order('orden');
		//Traer listado de indicadores
		$indicators = $table->fetchAll($select)->toArray();
  	
		//Construir formulario de indicadores
		$this->view->form = $form = new Indicators_Form_Indicators($indicators, $this->indicator_value_status);

		//Construir formulario de nuevo indicador
		$this->newindicatorAction();

		//Post actions
        if (! $this->getRequest()->isPost()) {
            return;
        }
        if (! $form->isValid($this->getRequest()
            ->getPost())) {
            return;
        }
		
    	$indicator_list = $form->getValues();
		
		foreach($indicator_list['indicators'] as $indicator_id => $indicator_values) {
			
			$indicator = Engine_Api::_()->getItem('indicators_lists', $indicator_id);
			$indicator->setFromArray($indicator_values);
            $indicator->modified_date = date('Y-m-d H:i:s');
            $indicator->save();			
		}

        return $this->_helper->redirector->gotoRoute(array(
            'action' => 'index'
        ));		
  }

	public function newindicatorAction() {
		$this->view->formNewIndicator = $formNewIndicator = new Indicators_Form_Newindicator($this->indicator_value_status);
		$formNewIndicator->setAction('/indicators/admin/newindicator');
		
		//Post actions
        if (! $this->getRequest()->isPost()) {
            return;
        }
        if (! $formNewIndicator->isValid($this->getRequest()
            ->getPost())) {
            return;
        }
			
		$table = Engine_Api::_()->getDbtable('lists', 'indicators');
		
		// Obtener el máximo orden
		$select = $table -> select()->from($table->info('name'), array(new Zend_Db_Expr('max(orden) as maxOrder')));
		if($res = $table->fetchAll($select)->toArray()) {
			$nextOrder = $res[0]['maxOrder'] + 1;
		}
		
		$data = $table->createRow();
		$data->setFromArray(array_merge(array('orden' => $nextOrder), $formNewIndicator->getValues()));
		$data->save();

        return $this->_helper->redirector->gotoRoute(array(
            'action' => 'index'
        ));		
				
	}


}
