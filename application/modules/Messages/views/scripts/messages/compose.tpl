<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Messages
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: compose.tpl 10224 2014-05-15 18:45:45Z lucas $
 * @author     John
 */
?>

<style>
    #toValues-label{
        display: none;
    }
    #global_page_messages-messages-compose .form-elements{
        margin-top: 0px !important;
    }
</style>

<?php
/*if (APPLICATION_ENV == 'production')*/
$this->headScript()
     ->appendFile( $this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.min.js' );
/*else
  $this->headScript()
	->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Observer.js')
	->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.js')
	->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Local.js')
	->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Request.js');*/
?>

    <script type="text/javascript">

        // Populate data
        var maxRecipients = <?php echo sprintf( "%d", $this->maxRecipients ) ?> ||
        10;
        var to = {
            id: false,
            type: false,
            guid: false,
            title: false
        };
        var isPopulated = false;

		<?php if( ! empty( $this->isPopulated ) && ! empty( $this->toObject ) ): ?>
        isPopulated = true;
        to = {
            id: <?php echo sprintf( "%d", $this->toObject->getIdentity() ) ?>,
            type: '<?php echo $this->toObject->getType() ?>',
            guid: '<?php echo $this->toObject->getGuid() ?>',
            title: '<?php echo $this->string()->escapeJavascript( $this->toObject->getTitle() ) ?>'
        };
		<?php endif; ?>



        function removeToValue(id, toValueArray) {
            for (var i = 0; i < toValueArray.length; i++) {
                if (toValueArray[i] == id) toValueIndex = i;
            }

            toValueArray.splice(toValueIndex, 1);
            $('toValues').value = toValueArray.join();
        }

        en4.core.runonce.add(function () {
            if (!isPopulated) { // NOT POPULATED
                new Autocompleter.Request.JSON('to', '<?php echo $this->url( array( 'module'     => 'user',
				                                                                    'controller' => 'friends',
				                                                                    'action'     => 'suggest',
				                                                                    'message'    => true
				), 'default', true ) ?>', {
                    'minLength': 1,
                    'delay': 250,
                    'selectMode': 'pick',
                    'autocompleteType': 'message',
                    'multiple': false,
                    'className': 'message-autosuggest',
                    'filterSubset': true,
                    'tokenFormat': 'object',
                    'tokenValueKey': 'label',
                    'injectChoice': function (token) {
                        if (token.type == 'user') {
                            var choice = new Element('li', {
                                'class': 'autocompleter-choices',
                                'html': token.photo,
                                'id': token.label
                            });
                            new Element('div', {
                                'html': this.markQueryValue(token.label),
                                'class': 'autocompleter-choice'
                            }).inject(choice);
                            this.addChoiceEvents(choice).inject(this.choices);
                            choice.store('autocompleteChoice', token);
                        }
                        else {
                            var choice = new Element('li', {
                                'class': 'autocompleter-choices friendlist',
                                'id': token.label
                            });
                            new Element('div', {
                                'html': this.markQueryValue(token.label),
                                'class': 'autocompleter-choice'
                            }).inject(choice);
                            this.addChoiceEvents(choice).inject(this.choices);
                            choice.store('autocompleteChoice', token);
                        }

                    },
                    onPush: function () {
                        if ($('toValues').value.split(',').length >= maxRecipients) {
                            $('to').disabled = true;
                        }
                    }
                });

                new Composer.OverText($('to'), {
                    'textOverride': '<?php echo $this->translate( 'Start typing...' ) ?>',
                    'element': 'label',
                    'isPlainText': true,
                    'positionOptions': {
                        position: (en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft'),
                        edge: (en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft'),
                        offset: {
                            x: (en4.orientation == 'rtl' ? -4 : 4),
                            y: 2
                        }
                    }
                });

            } else { // POPULATED

                var myElement = new Element("span", {
                    'id': 'tospan' + to.id,
                    'class': 'tag tag_' + to.type,
                    'html': to.title /* + ' <a href="javascript:void(0);" ' +
                  'onclick="this.parentNode.destroy();removeFromToValue("' + toID + '");">x</a>"' */
                });
                $('to-element').appendChild(myElement);
                $('to-wrapper').setStyle('height', 'auto');

                // Hide to input?
                $('to').setStyle('display', 'none');
                $('toValues-wrapper').setStyle('display', 'none');
            }
        });
    </script>

<?php
/*$this->headScript()
     ->appendFile( $this->layout()->staticBaseUrl . 'externals/mdetect/mdetect' . ( APPLICATION_ENV != 'development' ? '.min' : '' ) . '.js' )
     ->appendFile( $this->layout()->staticBaseUrl . 'application/modules/Core/externals/scripts/composer.js' );
*/?><!--

    <script type="text/javascript">
        var composeInstance;
        en4.core.runonce.add(function () {
            var tel = new Element('div', {
                'id': 'compose-tray',
                'styles': {
                    'display': 'none'
                }
            }).inject($('submit'), 'before');

            var mel = new Element('div', {
                'id': 'compose-menu'
            }).inject($('submit'), 'after');

            // @todo integrate this into the composer
            if ('<?php
/*					$id = Engine_Api::_()->user()->getViewer()->level_id;
					echo Engine_Api::_()->getDbtable( 'permissions', 'authorization' )->getAllowed( 'messages', $id, 'editor' );
					*/?>' == 'plaintext') {
                if (!Browser.Engine.trident && !DetectMobileQuick() && !DetectIpad()) {
                    composeInstance = new Composer('body', {
                        overText: false,
                        menuElement: mel,
                        trayElement: tel,
                        baseHref: '<?php /*echo $this->baseUrl() */?>',
                        hideSubmitOnBlur: false,
                        allowEmptyWithAttachment: false,
                        submitElement: 'submit',
                        type: 'message'
                    });
                }
            }
        });
    </script>
-->

<?php foreach ( $this->composePartials as $partial ): ?>
	<?php echo $this->partial( $partial[0], $partial[1] ) ?>
<?php endforeach; ?>

    <div class="form-general">
        <form enctype="multipart/form-data" class="global_form" action="<?php echo $this->form->getAction(); ?>" method="<?php echo $this->form->getMethod(); ?>">
        <div>
            <div>
                <h3><?php echo $this->translate($this->form->getTitle()); ?></h3>
                <p class="form-description">
	                <?php echo $this->translate($this->form->getDescription()); ?>
                </p>

                <div class="form-elements">
                    <!--//Para-->
	                <?php echo $this->form->to ?>
	                <?php $toMessage = $this->form->getMessages('to'); ?>

	                <?php foreach( $toMessage as $to ): ?>
		                <?php echo $to; ?>
	                <?php endforeach; ?>

                    <?php echo $this->form->toValues ?>
	                <?php $toValuesMessage = $this->form->getMessages('toValues'); ?>

	                <?php foreach( $toValuesMessage as $tovalues ): ?>
		                <?php echo $tovalues; ?>
	                <?php endforeach; ?>



                    <!--//Asunto-->
	                <?php echo $this->form->title ?>
	                <?php $titleMessage = $this->form->getMessages('title'); ?>

	                <?php foreach( $titleMessage as $title ): ?>
		                <?php echo $title; ?>
	                <?php endforeach; ?>

                    <!--//Mensaje-->
	                <?php echo $this->form->body ?>

	                <?php $bodyMessage = $this->form->getMessages('body'); ?>

	                <?php foreach( $bodyMessage as $message ): ?>
		                <?php echo $message; ?>
	                <?php endforeach; ?>


                    <!--//Enviar-->
	                <?php echo $this->form->submit ?>
                </div>
            </div>
        </div>

	    <?php /*echo $this->form->render($this) */?>

        </form>

    </div>


<?php echo $this->partial( '/application/modules/Core/views/scripts/_richeditor.tpl', array() ); ?>