<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Messages
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John Boehr <j@webligo.com>
 */
?>
<div class="generic--breadcrumbs">
    <a href="/">Inicio > </a>
    <a href="/classifieds" class="activo"> Mis Mensajes </a>
</div>

<div class="menu-tabs-generic" id="num-tabs3">
	<?php if( count($this->navigation) > 0 ): ?>
		<?php
		// Render the menu
		echo $this->navigation()
		          ->menu()
		          ->setContainer($this->navigation)
		          ->render();
		?>
	<?php endif; ?>
</div>



