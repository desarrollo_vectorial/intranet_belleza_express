<?php
$this->headLink()
    ->appendStylesheet($this->baseUrl().'/externals/datetimepicker/jquery.datetimepicker.css');

$this ->headScript()
    ->appendFile($this->baseUrl().'/externals/datetimepicker/php-date-formatter.min.js')
    ->appendFile($this->baseUrl().'/externals/datetimepicker/jquery.mousewheel.js')
    ->appendFile($this->baseUrl().'/externals/datetimepicker/jquery.datetimepicker.js');
?>

<script type="text/javascript">
  var myCalStart = false;
  var myCalEnd = false;

  en4.core.runonce.add(function init() 
  {
    monthList = [];
    myCal = new Calendar({ 'starttime[date]': 'M d Y', 'endtime[date]' : 'M d Y' }, {
      classes: ['event_calendar'],
      pad: 0,
      direction: 0
    });
});
</script>

<div class="form-general">
    <?php echo $this->form->render($this) ?>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $.datetimepicker.setLocale('es');

        $('.fecha').datetimepicker({
            lang: 'es',
            format: 'Y-m-d H:i:s',
            formatDate: 'Y-m-d',
            mask: '9999-19-39 29:59'
        });
    });
</script>