<?php
$this->headLink()
     ->appendStylesheet($this->baseUrl().'/externals/datetimepicker/jquery.datetimepicker.css');

$this ->headScript()
      ->appendFile($this->baseUrl().'/externals/datetimepicker/php-date-formatter.min.js')
      ->appendFile($this->baseUrl().'/externals/datetimepicker/jquery.mousewheel.js')
      ->appendFile($this->baseUrl().'/externals/datetimepicker/jquery.datetimepicker.js');
?>

<div class="form-general">
  <?php if( $this->parent_type == 'group' ) { ?>
    <h2>
      <?php echo $this->group->__toString() ?>
      <?php echo '&#187; '.$this->translate('Events');?>
    </h2>
  <?php } ?>
  <?php echo $this->form->render() ?>
</div>

<script type="text/javascript">
    $$('.core_main_event').getParent().addClass('active');
</script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $.datetimepicker.setLocale('es');

        $('.fecha').datetimepicker({
            lang: 'es',
            format: 'Y-m-d H:i:s',
            formatDate: 'Y-m-d',
            mask: '9999-19-39 29:59'
        });
    });
</script>