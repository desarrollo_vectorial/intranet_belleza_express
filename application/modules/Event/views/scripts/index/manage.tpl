<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Event
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: manage.tpl 9989 2013-03-20 01:13:58Z john $
 * @author     Sami
 */
?>
<div class="container-list">
  <?php if( count($this->paginator) > 0 ): ?>

      <ul class='format--list'>
        <?php foreach( $this->paginator as $event ): ?>
          <li>
              <div class="format--image-left">
                  <a class="image-left--enlace" href="<?php echo $event->getHref(); ?>">
                      <?php if ($event->getPhotoUrl('thumb') != null): ?>
                          <span style="background-image: url('<?php echo $event->getPhotoUrl('thumb.normal'); ?>');"></span>
                      <?php else: ?>
                          <span style="background-image: url('<?php echo $this->baseUrl(); ?>/application/themes/snowbot/images/events.jpg');"></span>
                      <?php endif; ?>
                  </a>
              </div>

              <div class="format--info">
                  <div class="info--content">
                      <div class="format--title">
                          <h3><?php echo $this->htmlLink($event->getHref(), $event->getTitle()) ?></h3>
                      </div>
                      <div class="format--date-start">
	                      <?php
	                      $lenguage = 'es_ES.UTF-8';
	                      putenv("LANG=$lenguage");
	                      setlocale(LC_ALL, $lenguage);
	                      /*setlocale(LC_ALL, 'es_ES');*/
	                      // En windows
	                     /* setlocale(LC_ALL, 'spanish');*/
	                      ?>
                          <?php echo $fecha = strftime("%A, %d de %B del %Y a las", strtotime($event->starttime)); ?>
                          <?php  echo $hora= Date("g:i: a", strtotime($event->starttime)); ?>
                      </div>
                      <div class="format--members">
                          <?php echo $this->translate(array('%s guest', '%s guests', $event->membership()->getMemberCount()), $this->locale()->toNumber($event->membership()->getMemberCount())) ?>
                          <?php echo $this->translate('led by') ?>
                          <?php echo $this->htmlLink($event->getOwner()->getHref(), $event->getOwner()->getTitle()) ?>
                      </div>
                      <div class="format--description">
                          <?php echo $event->getDescription() ?>
                      </div>
                  </div>

                  <div class="format--options">
                      <?php if( $this->viewer() && $event->isOwner($this->viewer()) ): ?>
                          <?php echo $this->htmlLink(array('route' => 'default', 'module' => 'event', 'controller' => 'event', 'action' => 'delete', 'event_id' => $event->getIdentity(), 'format' => 'smoothbox'), $this->translate('Delete Event'), array(
                              'class' => 'smoothbox options--eliminar'
                          )); ?>

                          <?php echo $this->htmlLink(array('route' => 'event_specific', 'action' => 'edit', 'event_id' => $event->getIdentity()), $this->translate('Edit Event'), array(
                              'class' => 'options--editar'
                          )) ?>
                      <?php endif; ?>

                      <?php if( $this->viewer() && !$event->membership()->isMember($this->viewer(), null) ): ?>
                          <?php echo $this->htmlLink(array('route' => 'event_extended', 'controller'=>'member', 'action' => 'join', 'event_id' => $event->getIdentity()), $this->translate('Únete al evento'), array(
                              'class' => 'buttonlink smoothbox icon_event_join'
                          )) ?>
                      <?php elseif( $this->viewer() && $event->membership()->isMember($this->viewer()) && !$event->isOwner($this->viewer()) ): ?>
                          <?php echo $this->htmlLink(array('route' => 'event_extended', 'controller'=>'member', 'action' => 'leave', 'event_id' => $event->getIdentity()), $this->translate('Abandonar el evento'), array(
                              'class' => 'buttonlink smoothbox icon_event_leave'
                          )) ?>
                      <?php endif; ?>

                  </div>

              </div>

          </li>
        <?php endforeach; ?>
      </ul>

      <?php if( $this->paginator->count() > 1 ): ?>
        <?php echo $this->paginationControl($this->paginator, null, null, array(
          'query' => array('view'=>$this->view, 'text'=>$this->text)
        )); ?>
      <?php endif; ?>

  <?php else: ?>
    <div class="tip">
      <span>
          <?php echo $this->translate('No has ingresado en ningún evento todavía.') ?>
          <?php if( $this->canCreate): ?>
            <?php echo $this->translate('¿Por qué no %1$screas uno%2$s?',
              '<a href="'.$this->url(array('action' => 'create'), 'event_general').'">', '</a>') ?>
          <?php endif; ?>
      </span>
    </div>
  <?php endif; ?>
  <script type="text/javascript">
    $$('.core_main_event').getParent().addClass('active');
  </script>
</div>