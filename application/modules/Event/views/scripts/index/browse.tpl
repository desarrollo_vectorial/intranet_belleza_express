<?php if (count($this->paginator) > 0): ?>

<div class="container-list">
    <ul class='format--list'>
        <?php foreach ($this->paginator as $event): ?>
            <li>
                <div class="format--image-left">
                    <a class="image-left--enlace" href="<?php echo $event->getHref(); ?>">
                        <?php if ($event->getPhotoUrl('thumb') != null): ?>
                           <span style="background-image: url('<?php echo $event->getPhotoUrl('thumb'); ?>');"></span>
                        <?php else: ?>
                            <span style="background-image: url('<?php echo $this->baseUrl(); ?>/application/themes/snowbot/images/events.jpg');"></span>
                        <?php endif; ?>
                    </a>
                </div>
                <div class="format--info">
                    <div class="info--content">
                        <div class="format--title">
                            <h3><?php echo $this->htmlLink($event->getHref(), $event->getTitle()) ?></h3>
                        </div>
                        <div class="format--date-start">

                            <?php
                            $lenguage = 'es_ES.UTF-8';
                            putenv("LANG=$lenguage");
                            setlocale(LC_ALL, $lenguage);
                            /*setlocale(LC_ALL, 'es_ES');*/
                            // En windows
                            /*setlocale(LC_ALL, 'spanish');*/
                            ?>
                            <?php echo $fecha = strftime("%A, %d de %B del %Y a las", strtotime($event->starttime)); ?>
                            <?php  echo $hora= Date("g:i: a", strtotime($event->starttime)); ?>
                        </div>
                        <div class="format--members">
                            <?php echo $this->translate(array('%s guest', '%s guests', $event->membership()->getMemberCount()), $this->locale()->toNumber($event->membership()->getMemberCount())) ?>
                            <?php echo $this->translate('led by') ?>
                            <?php echo $this->htmlLink($event->getOwner()->getHref(), $event->getOwner()->getTitle()) ?>
                        </div>
                        <div class="format--description">
                            <?php echo $event->getDescription() ?>
                        </div>
                    </div>

                    <div class="format--options">
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

    <?php if ($this->paginator->count() > 1): ?>
        <?php echo $this->paginationControl($this->paginator, null, null, array(
            'query' => $this->formValues,
        )); ?>
    <?php endif; ?>


<?php elseif (preg_match("/category_id=/", $_SERVER['REQUEST_URI'])): ?>
    <div class="tip">
      <span>
      <?php echo $this->translate('_events_Nobody has created an event with that criteria.');?>
      <?php if( $this->canCreate ): ?>
        <?php echo $this->translate('_events_Be the first to %1$screate%2$s one!', '<a href="'.$this->url(array('action'=>'create'), 'event_general').'">', '</a>'); ?>
      <?php endif; ?>
      </span>
    </div>


<?php else: ?>
    <div class="tip">
      <span>
      <?php if( $this->filter != "past" ): ?>
        <?php echo $this->translate('_events_Nobody has created an event yet.') ?>
        <?php if( $this->canCreate ): ?>
          <?php echo $this->translate('_events_Be the first to %1$screate%2$s one!', '<a href="'.$this->url(array('action'=>'create'), 'event_general').'">', '</a>'); ?>
        <?php endif; ?>
      <?php else: ?>
        <?php echo $this->translate('_events_There are no past events yet.') ?>
      <?php endif; ?>
      </span>
    </div>

<?php endif; ?>

<script type="text/javascript">
    $$('.core_main_event').getParent().addClass('active');
</script>