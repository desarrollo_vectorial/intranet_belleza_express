<div class="calendar--container">
  <?php
  $events = array();
  foreach ($this->allEvents as $key => $event) {
    array_push($events, $event->toArray());
  } ?>

  <div id="events_callendar"></div>
  <script type="text/javascript">
    jQuery.noConflict();
    (function($) { // START NAMESPACE

      $(document).ready(function(){

        var today = new Date();
        today.format()
        var eventsDB = <?php echo json_encode($events); ?>;
        var events = [];
        for ( var i = 0; i < eventsDB.length; i++ ){
          events.push({
            id   : eventsDB[i].event_id,
            title: eventsDB[i].description,
            start: eventsDB[i].starttime,
            end  : eventsDB[i].endtime,
          });
        }

  
        $('#events_callendar').fullCalendar({
          //height: 'auto',
          header: {
            left  : 'today prev,next',
            center: 'title',
            right : 'month,basicWeek,basicDay'
          },
          defaultDate: today,
          dayClick: function(date, jsEvent, view) {
            var fecha = date.format();
            alert(fecha);
          },

          
          eventMouseover: function(calEvent, jsEvent) { 
            var tooltip = '<div class="tooltipevent" style="background: #1f589a;\n' +
                '  color: white;\n' +
                '  height: auto;\n' +
                '  padding: 10px;\n' +
                '  position: absolute;\n' +
                '  width: 400px;\n' +
                '  z-index: 10001;"> <b>Evento:</b><br> ' + calEvent.title + '</div>'; var $tool = $(tooltip).appendTo('body');
            $(this).mouseover(function(e) {
                $(this).css('z-index', 10000);
                $tool.fadeIn('500');
                $tool.fadeTo('10', 1.9);
            }).mousemove(function(e) {
                $tool.css('top', e.pageY + 10);
                $tool.css('left', e.pageX + 20);
            });
          },
          eventMouseout: function(calEvent, jsEvent) {
            $(this).css('z-index', 8);
            $('.tooltipevent').remove();
          },

          eventClick: function(calEvent, jsEvent, view, event) {
            location.href = "/event/" + calEvent.id;
          },
          events: events
        });

      });

    })(jQuery);

  </script>
</div>