<div class="menu-tabs-generic" id="num-tabs4">
    <?php if( count($this->navigation) > 0 ): ?>
        <?php
            // Render the menu
            echo $this->navigation()
                ->menu()
                ->setContainer($this->navigation)
                ->render();
        ?>
    <?php endif; ?>
</div>