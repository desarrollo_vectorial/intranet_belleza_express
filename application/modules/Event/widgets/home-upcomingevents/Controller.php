<?php

class Event_Widget_HomeUpcomingeventsController extends Engine_Content_Widget_Abstract {
	public function indexAction() {
		$eventTable     = Engine_Api::_()->getItemTable( 'event' );
		$eventTableName = $eventTable->info( 'name' );

		// Show all upcoming events
		$select = $eventTable->select()->where( 'search = ?', 1 );
		$select->order( "starttime ASC" );
		$this->view->allEvents = $eventTable->fetchAll( $select );


		/*/ Do not render if nothing to show and not viewer
		if( count($this->view->allEvents) == 0 ) {
		  return $this->setNoRender();
		}*/


		$this->view->paginator = array();

		foreach ( $this->view->allEvents as $key => $event ) {
			array_push( $this->view->paginator, $event->toArray() );
		}

	}
}
