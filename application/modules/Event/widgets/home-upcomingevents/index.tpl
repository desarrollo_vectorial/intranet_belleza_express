<?php $this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Event/externals/scripts/home-upcomingevents.js'); ?>

<div style="clear: both;"></div>

<div ng-app="modulo-eventos">

    <div ng-controller="widget_home_events_ctrl" ng-init='init(<?php echo json_encode($this->paginator); ?>)'>

        <div class="title">
            <h4>Calendario de eventos</h4>
        </div>
        <div class="instructions">
            <p>Para ver un evento seleccione un día</p>
        </div>

        <div class="select_month">
            <div class="left">
                <div class="arrow" ng-click="changeMonth('minus')"><i class="material-icons">navigate_before</i></div>
                <div class="month_name">{{ months[ selectedMonth.getMonth() ] }}</div>
                <div class="arrow" ng-click="changeMonth('plus')"><i class="material-icons">navigate_next</i></div>
            </div>
            <div class="right">
                <div class="today_circle"></div>
                <div class="today_circle_text">Hoy</div>
                <div class="events_circle"></div>
                <div class="events_circle_text">Eventos</div>
            </div>
        </div>

        <div class="wrapper__list-events">
            <div ng-repeat=" event in events | filter:{ month: selectedMonth } " class="event_row"
                 ng-click=" goToEvent(event.event_id) ">

                <div class="event_date">
                    <div class="date_day">{{ event.starttime | date : 'd' }}</div>
                    <div class="date_month">{{ event.starttime | date : 'MMMM' }}</div>
                </div>
                <div class="event_info">
                    <div class="event_title">{{ event.title }}</div>
                    <div class="event_time">{{ event.starttime | date : 'hh:mm a' }} - {{ event.endtime | date : 'hh:mm a'
                        }}
                    </div>
                </div>

            </div>
        </div>



        <!-- Calendario -->
        <div id="calendario_eventos_home"></div>
        <!-- Calendario -->

        <div class="link_all_events">
            <a href="/events/calendar">Ver todos los eventos</a>
        </div>

    </div>

</div>


<script type="text/javascript">

    jQuery.noConflict();

    (function ($) { // START NAMESPACE

        $(document).ready(function () {

            var eventosDB = <?php echo json_encode($this->paginator); ?>;
            var events = [];
            for (var i = 0; i < eventosDB.length; i++) {
                events.push({
                    title: eventosDB[i].title,
                    url: '/event/' + eventosDB[i].event_id,
                    start: eventosDB[i].starttime,
                    end: eventosDB[i].endtime,
                });
            }

            $('#calendario_eventos_home').fullCalendar({
                height: 'auto',
                header: {
                    left: false,
                    center: 'title',
                    right: false
                },
                defaultView: 'month',
                events: events,
                dayNamesShort: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                eventClick: function (calEvent, jsEvent, view) {
                    window.location = calEvent.url;
                }
            });

        });

    })(jQuery);

</script>
