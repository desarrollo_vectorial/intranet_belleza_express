<div class="events-widget-profile-photo">
    <?php if ($this->itemPhoto($this->subject(), 'thumb.profile') != null): ?>
        <?php echo $this->itemPhoto($this->subject(), 'thumb.profile'); ?>
    <?php else: ?>
        <img src="<?php echo $this->baseUrl(); ?>/application/themes/snowbot/images/events.jpg" alt="" class="thumb_profile item_photo_event item_nophoto ">
    <?php endif; ?>
</div>