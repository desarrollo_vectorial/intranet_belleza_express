var app = angular.module('modulo-eventos', []);

app.controller("widget_home_events_ctrl", function($scope) {

    $scope.init = function(events) {

        console.log(events);

        $scope.events = events;
        for ( var i = 0; i < events.length; i++ ){
          var date = new Date($scope.events[i].starttime);
          $scope.events[i].month = new Date(date.getFullYear(), date.getMonth(), "1");
          $scope.events[i].starttime = new Date($scope.events[i].starttime);
          $scope.events[i].endtime = new Date($scope.events[i].endtime);
        }
    };

    // Meses
    $scope.months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    var today = new Date();
    $scope.selectedMonth = new Date(today.getFullYear(), today.getMonth(), "1");

    // Cambiar mes
    $scope.changeMonth = function(add){

      // Elemento Calendario
      var target = angular.element('#calendario_eventos_home');

      switch (add) {
        case 'plus':
          $scope.selectedMonth = new Date($scope.selectedMonth.getFullYear(), $scope.selectedMonth.getMonth() + 1, "1");
          target.fullCalendar('next');
          break;
        case 'minus':
          $scope.selectedMonth = new Date($scope.selectedMonth.getFullYear(), $scope.selectedMonth.getMonth() - 1, "1");
          target.fullCalendar('prev');
          break;
      }
    };

    // Al dar click sobre un evento se debe redireccionar a la página del evento
    $scope.goToEvent = function(event_id){
      window.location = '/event/' + event_id;
    };

});
