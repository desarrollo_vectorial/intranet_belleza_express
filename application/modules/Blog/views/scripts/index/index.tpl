<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Blog
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */
?>
<div class="container-list">
    <?php if (count($this->paginator) > 0): ?>
        <ul class="format--list">
            <?php foreach ($this->paginator as $item): ?>

                <?php echo $this->partial('blogBox.tpl', array('item' => $item, 'showOwnerOptions' => false)); ?>

            <?php endforeach; ?>
        </ul>

    <?php elseif ($this->category || $this->show == 2 || $this->search): ?>
        <div class="tip">
      <span>

        <?php echo $this->translate('_blog_Nobody has written a blog entry with that criteria.');?>
        <?php if (TRUE): // @todo check if user is allowed to create a poll ?>
          <?php echo $this->translate('_blog_Be the first to %1$swrite%2$s one!', '<a href="'.$this->url(array('action' => 'create'), 'blog_general').'">', '</a>'); ?>
        <?php endif; ?>

      </span>

        </div>

    <?php else: ?>
        <div class="tip">
      <span>
        <?php echo $this->translate('_blog_Nobody has written a blog entry yet.'); ?>
        <?php if( $this->canCreate ): ?>
          <?php echo $this->translate('_blog_Be the first to %1$swrite%2$s one!', '<a href="'.$this->url(array('action' => 'create'), 'blog_general').'">', '</a>'); ?>
        <?php endif; ?>
      </span>
        </div>
    <?php endif; ?>
    <?php echo $this->paginationControl($this->paginator, null, null, array(
        'pageAsQuery' => true,
        'query' => $this->formValues,
        //'params' => $this->formValues,
    )); ?>
</div>