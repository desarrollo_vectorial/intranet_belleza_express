<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Blog
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: manage.tpl 9987 2013-03-20 00:58:10Z john $
 * @author     Jung
 */
?>

<div class="container-list">
    <?php if (count($this->paginator) > 0): ?>
        <ul class="format--list">
            <?php foreach ($this->paginator as $item): ?>

                <?php echo $this->partial('blogBox.tpl', array('item' => $item, 'showOwnerOptions' => true)); ?>

            <?php endforeach; ?>
        </ul>

    <?php elseif ($this->search): ?>
        <div class="tip">
        <span>
          <?php echo $this->translate('_blog_You do not have any blog entries that match your search criteria.');?>
        </span>
        </div>
    <?php else: ?>
        <div class="tip">
        <span>
          <?php echo $this->translate('_blog_You do not have any blog entries.');?>
          <?php if( $this->canCreate ): ?>
            <?php echo $this->translate('_blog_Get started by %1$swriting%2$s a new entry.', '<a href="'.$this->url(array('action' => 'create'), 'blog_general').'">', '</a>'); ?>
          <?php endif; ?>
        </span>
        </div>
    <?php endif; ?>

    <?php echo $this->paginationControl($this->paginator, null, null, array(
        'pageAsQuery' => true,
        'query' => $this->formValues,
        //'params' => $this->formValues,
    )); ?>


    <script type="text/javascript">
        $$('.core_main_blog').getParent().addClass('active');
    </script>
    <script type="text/javascript">
        var pageAction = function (page) {
            $('page').value = page;
            $('filter_form').submit();
        }
    </script>
</div>