<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Blog
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: list.tpl 9987 2013-03-20 00:58:10Z john $
 * @author     Jung
 */
?>

<style>
    .mi-contenedor { background-color: #f3f3f3 !important; margin-top: 5px;}
  .mi-contenedor h2{ color: #ab1607; }
  ul.blogs_entrylist, ul.classifieds_entrylist{ border: none !important; }
  .mi-contenedor .blogs_entrylist li{ min-height: 150px;  background-color: white!important;}
  .mi-contenedor .blogs_entrylist li span{ padding-left: 20px; padding-right:20px; display:block; position:relative; }
  .mi-contenedor .blogs_entrylist li h3 a{ color: #4c8ac5; font-size:22px; }
  .mi-contenedor .blogs_entrylist .blog_entrylist_entry_date span{ position:relative; display:inline-block; }
  .mi-contenedor .blogs_entrylist .blog_entrylist_entry_date .icon-publicada{
    background-image: url(public/admin/split-articulo.png);
    display: inline-block;
    background-size: cover; 
    width : 20px;
    height: 20px;
  }
  .mi-contenedor .blogs_entrylist .blog_entrylist_entry_date .img1{ background-position: 0 0; }
  .mi-contenedor .blogs_entrylist .blog_entrylist_entry_date .img2{ background-position: 100px 0; }
  .mi-contenedor .blogs_entrylist .blog_entrylist_entry_date .img3{ background-position: 80px 0; }
  .mi-contenedor .blogs_entrylist .blog_entrylist_entry_date .img4{ background-position: 60px 0; }
    .mi-contenedor .blogs_entrylist .blogs_browse_photo {
        width: 50px;
        height: 100px;
    }
    ul.blogs_entrylist > li .blogs_browse_photo {
         float: left;
         overflow: hidden;
         margin-right: 8px;
     }
</style>


<div class="mi-contenedor all-width">
  <!--<h2><?php //echo $this->translate('Recent Entries')?></h2>-->

  <?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
    <ul class='blogs_entrylist'>
    <?php foreach ($this->paginator as $item): ?>
      <li>
          <div class='blogs_browse_photo'>
              <?php echo $this->htmlLink($item->getOwner()->getHref(), $this->itemPhoto($item->getOwner(), 'thumb.icon')); ?>
          </div>
        <span>
          <h3><?php echo $this->htmlLink($item->getHref(), $item->getTitle());?></h3>

          <div class="blog_entrylist_entry_body">
            <?php echo $this->string()->truncate($this->string()->stripTags($item->body), 300) ?>
          </div>
            <div class="blog_entrylist_entry_date">
<!--            <i class="icon-publicada img1"></i>-->
                <!--            --><?php //echo $this->timestamp(strtotime($item->creation_date)) ?>
                <!---->
                <!--            <i class="icon-publicada img2"></i>-->
                <!--            --><?php //echo $this->htmlLink($item->getOwner()->getHref(), $item->getOwner()->getTitle()) ?>

                <i class="icon-publicada img3"></i>
                <?php echo $this->translate('Comments');?>
                <span style="display: inline-block; position: relative; padding: 5px; width: 10px;background: #a7a5a5; color: #fff; border-radius: 60px; vertical-align: top; text-align: center;font-weight: 400;"><b><?php echo $item->getCommentsCount(); ?></b></span>
          </div>
          <div class="ver-mas"><?php echo $this->htmlLink($item->getHref(), 'Ver más');?></div>
          
          <?php /*if ($item->comment_count > 0) :?>          
            <?php echo $this->htmlLink($item->getHref(), $this->translate(array('%s comment', '%s comments', $this->comment_count), $this->locale()->toNumber($this->comment_count)) , array('class' => 'buttonlink icon_comments')) ?>
          <?php endif;*/ ?>
        </span>
      </li>
    <?php endforeach; ?>
    </ul>

  <?php elseif( $this->category || $this->tag ): ?>
    <div class="tip">
      <span>
        <?php echo $this->translate('%1$s has not published a blog entry with that criteria.', $this->owner->getTitle()); ?>
      </span>
    </div>

  <?php else: ?>
    <div class="tip">
      <span>
        <?php echo $this->translate('%1$s has not written a blog entry yet.', $this->owner->getTitle()); ?>
      </span>
    </div>
  <?php endif; ?>

  <?php echo $this->paginationControl($this->paginator, null, null, array(
    'pageAsQuery' => true,
    'query' => $this->formValues,
    //'params' => $this->formValues,
  )); ?>

  <script type="text/javascript">
    $$('.core_main_blog').getParent().addClass('active');
  </script>

</div>