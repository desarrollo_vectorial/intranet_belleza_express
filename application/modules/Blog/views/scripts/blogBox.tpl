
<li>
    <div class="format--image-left">
        <div class="image-left--enlace">
            <?php if ($this->item->getPhotoUrl("thumb.feature") != null): ?>
                <span style="background-image: url('<?php echo $this->item->getPhotoUrl("thumb"); ?>');"></span>
            <?php else: ?>
                <span style="background-image: url('<?php echo $this->baseUrl(); ?>/application/modules/Advgroup/externals/images/nophoto_group_thumb_feature.png');"></span>
            <?php endif; ?>
        </div>
    </div>

    <div class="format--info">
        <div class="info--content">
            <div class="format--title">
                <h3>
                    <?php echo $this->htmlLink($this->item->getHref(), $this->item->getTitle()) ?>
                </h3>
            </div>
            <div class="format--date-start">
                <?php echo $this->timestamp(strtotime($this->item->creation_date)) ?>
            </div>
            <div class="format--members">

            </div>
            <div class="format--description">
                <?php echo $this->string()->truncate($this->string()->stripTags($this->item->body), 300) ?>

            </div>
        </div>

        <div class="format--options">
            <?php if ($this->showOwnerOptions): ?>
                <div class="bottons--action">
                    <?php echo $this->htmlLink(array(
                        'action' => 'edit',
                        'blog_id' => $this->item->getIdentity(),
                        'route' => 'blog_specific',
                        'reset' => true,

                    ), 'Editar'); ?>

                    <?php echo $this->htmlLink(array(
                        'route' => 'default',
                        'module' => 'blog',
                        'controller' => 'index',
                        'action' => 'delete',
                        'blog_id' => $this->item->getIdentity(),
                        'format' => 'smoothbox'

                    ), 'Borrar',array(
                        'class' => 'buttonlink smoothbox'
                    )); ?>
                </div>

            <?php else: ?>
                <div class="botton--group">
                    <?php echo $this->htmlLink($this->item->getHref(), 'Ver Blog'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</li>