<div class="mi-contenedor-options">
  <?php
     //Render the menu
    echo $this->navigation()
      ->menu()
      ->setContainer($this->gutterNavigation)
      ->setUlClass('navigation blogs_gutter_options')
      ->render();
  ?>

</div>
<script type="text/javascript">
    var pageAction     = function(page){
      $('page').value = page;
      $('filter_form').submit();
    }
    var categoryAction = function(category){
      $('page').value = 1;
      $('blog_search_field').value = '';
      $('category').value = category;
      $('tag').value = '';
      $('start_date').value = '';
      $('end_date').value = '';
      $('filter_form').submit();
    }
    var tagAction      = function(tag){
      $('page').value = 1;
      $('blog_search_field').value = '';
      $('tag').value = tag;
      $('category').value = '';
      $('start_date').value = '';
      $('end_date').value = '';
      $('filter_form').submit();
    }
    
    var dateAction     = function(start_date, end_date){
      $('page').value = 1;
      $('blog_search_field').value = '';
      $('start_date').value = start_date;
      $('end_date').value = end_date;
      $('tag').value = '';
      $('category').value = '';
      $('filter_form').submit();
    }
    en4.core.runonce.add(function(){
      new OverText($('blog_search_field'), {
        poll: true,
        pollInterval: 500,
        positionOptions: {
          position: ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
          edge: ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
          offset: {
            x: ( en4.orientation == 'rtl' ? -4 : 4 ),
            y: 2
          }
        }
      });
    });
  </script>

  <?php $searchUrl = $this->url(array('user_id' => $this->owner->getIdentity()), 'blog_view', true); ?>

  <form id='filter_form' class="blog_search_form" method='GET' action="<?php echo $this->escape($searchUrl) ?>">

    <input type='hidden'   id='blog_search_field' name='search' class='text suggested form_titulo' size='20' maxlength='100' alt='<?php echo $this->translate('Search Blogs') ?>' value="<?php if( $this->search ) echo $this->search; ?>" />
    <input type="hidden" id="tag"               name="tag"        value="<?php if( $this->tag ) echo $this->tag; ?>"/>
    <input type="hidden" id="category"          name="category"   value="<?php if( $this->category ) echo $this->category; ?>"/>
    <input type="hidden" id="page"              name="page"       value="<?php if( $this->page ) echo $this->page; ?>"/>
    <input type="hidden" id="start_date"        name="start_date" value="<?php if( $this->start_date) echo $this->start_date; ?>"/>
    <input type="hidden" id="end_date"          name="end_date"   value="<?php if( $this->end_date) echo $this->end_date; ?>"/>

  </form>