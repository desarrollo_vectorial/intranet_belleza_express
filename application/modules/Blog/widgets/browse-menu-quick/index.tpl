<?php if( count($this->quickNavigation) > 0 ): ?>
    <div class="quick-links--button">
        <?php echo $this->navigation()->menu()->setContainer($this->quickNavigation)->render(); ?>
    </div>
<?php endif; ?>