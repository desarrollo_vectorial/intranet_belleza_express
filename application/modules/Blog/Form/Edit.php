<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Blog
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Edit.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Blog
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Blog_Form_Edit extends Blog_Form_Create
{
    public function init()
    {
        parent::init();
        $this->setTitle('')
            ->setDescription('');

        /*Validaciones*/
        $user = Engine_Api::_()->user()->getViewer();
        $user_level = Engine_Api::_()->user()->getViewer()->level_id;

        $allowed_html = Engine_Api::_()->authorization()->getPermission($user_level, 'blog', 'auth_html');
        $upload_url = "";

        if (Engine_Api::_()->authorization()->isAllowed('album', $user, 'create')) {
            $upload_url = Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'upload-photo'), 'blog_general', true);
        }

        $editorOptions = array(
            'upload_url' => $upload_url,
            'html' => (bool)$allowed_html,
        );


        if (!empty($upload_url)) {
            $editorOptions['plugins'] = array(
                'table', 'fullscreen', 'media', 'preview', 'paste',
                'code', 'image', 'textcolor', 'jbimages', 'link'
            );

            $editorOptions['toolbar1'] = array(
                'undo', 'redo', 'removeformat', 'pastetext', '|', 'code',
                'media', 'image', 'jbimages', 'link', 'fullscreen',
                'preview'
            );
        }

        /*FORM SET FORMAT*/
        $this->addElement('textarea', 'body', array(
                'label' => 'Contenido',
                'required' => true,
                'allowEmpty' => false,
                'class' => 'richtext',
                'editorOptions' => $editorOptions,
            )
        );


        $this->submit->setLabel('Save Changes');
    }
}