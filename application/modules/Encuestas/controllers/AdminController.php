<?php
date_default_timezone_set('America/Bogota');

class Encuestas_AdminController extends Core_Controller_Action_Standard{
  
  public function init(){
    // Validar que el usuario debe estar logueado y ser administrador para poder entrar al administrador del módulo
    // Aplica para todas las vistas de este controlador
    /*$viewer = Engine_Api::_()->user()->getViewer();
    if ($viewer->isModSuperAdmin() == 0) {
      $this->_redirect('/');
    }*/
  }

  /*/ Funcion que modifica la clave, salt de los usuarios
    public function pruebaAction(){
      $this->_helper->content->setEnabled();

      $db         = Zend_Db_Table_Abstract::getDefaultAdapter();
      $staticSalt = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.secret', 'staticSalt');
      
      $tableUsers = Engine_Api::_()->getDbtable('users', 'user');
      $selUsers   = $tableUsers->select()->where('level_id <> 1')->where('fecha_nacimiento is not NULL');
      $usuarios   = $tableUsers->fetchAll($selUsers);
    

      /* Se recorren los usuarios para modificarles el salt y el password /
      foreach ($usuarios as $usuario) {
        $fecha = date('Y', strtotime($usuario['fecha_nacimiento'])).date('m', strtotime($usuario['fecha_nacimiento'])).date('d', strtotime($usuario['fecha_nacimiento']));
        $salt  = mt_rand(1000000, 9999999);
        $clave = md5($staticSalt.$fecha.$salt);
        $hoy   = date("Y-m-d H:i:s");
        $db->query(
          "UPDATE engine4_users 
              SET salt = '".$salt."', password = '".$clave."', locale = 'es', language = 'es', timezone = 'US/Pacific', search = 1, enabled = 1, verified = 1, approved = 1, creation_date = '".$hoy."', level_id = 4
            WHERE user_id = ".$usuario['user_id']
        );
      }

      exit('ok');
    }
    */


  /*/ Funcion que hace amigos todos con todos /
  public function pruebaAction(){
    $this->_helper->layout->disableLayout();
    $this->_helper->content->setEnabled();

    $db          = Zend_Db_Table_Abstract::getDefaultAdapter();
    $api         = Engine_Api::_()->encuestas();

    $viewer      = $api->getViewer();
    $tableUsers  = Engine_Api::_()->getDbtable('users', 'user');
    $usuarios    = $tableUsers->fetchAll();
    
    $tableMember = Engine_Api::_()->getDbtable('membership', 'user');   
    $db_nofriend = $tableMember->getAdapter();
    $db_nofriend->beginTransaction();

    foreach ($usuarios as $key => $usuario) {
      $selMember   = new Zend_Db_Select($db);
      $selMember->from('engine4_user_membership', array('user_id'))
                ->where('resource_id = ?', $usuario['user_id'] ); 
      $allMember   = $db->fetchAll($selMember); 
      $members     = array();

      foreach ($allMember as $key => $member) {
        array_push($members, $member['user_id']);
      }    

      $memberUsers = implode(",", $members);

      if(count($allMember) > 0)
        $selUserNoFriends = $tableUsers->select()->from('engine4_users', array('user_id'))->where('user_id NOT IN ('.$memberUsers.')')->where( 'user_id <> ?', $usuario['user_id'] );
      else
        $selUserNoFriends = $tableUsers->select()->where( 'user_id <> ?', $usuario['user_id'] );
      
      $usuariosNoFriends = $tableUsers->fetchAll($selUserNoFriends);
            
      foreach ($usuariosNoFriends as $key => $noFriend) {
        
        $db_nofriend->beginTransaction();
        try {

          //Se crea el amigo envio
          $friend_row   = $tableMember->createRow();
          $valuesFriend = array(
            'resource_id'       => $usuario['user_id'],
            'user_id'           => $noFriend['user_id'],
            'active'            => 1, 
            'resource_approved' => 1,
            'user_approved'     => 1
          );
          $friend_row->setFromArray($valuesFriend);
          $friend_row->save();


          //Se crea el amigo devuelta
          $friend_row   = $tableMember->createRow();
          $valuesFriend = array(
            'resource_id'       => $noFriend['user_id'],
            'user_id'           => $usuario['user_id'],
            'active'            => 1, 
            'resource_approved' => 1,
            'user_approved'     => 1
          );
          $friend_row->setFromArray($valuesFriend);
          $friend_row->save();

          $count++;
        
        } catch (Exception $e) {
          $db_nofriend->rollBack();
        }
        $db_nofriend->commit();
      }
    }
    
    exit('Se hicieron amigos todos con todos.');
  }*/


  public function pruebaAction(){
    Engine_Api::_()->core()->PHPMailer();
    Engine_Api::_()->core()->PHPMailerSendEmail("Usuario Intranet", "stephanie.sanchez@centelsa.com.co", "Mensaje prueba desde PHPMailerSendEmail", "Prueba AltBody");
  }




  //FUNCIONES PARA CREAR, MODIFICAR, ELIMINAR LAS ENCUESTAS

    //FUNCION DE LA PAGINA INDEX DEL ADMINISTRADOR
    public function indexAction(){

      /*
        $dominio = trim($username).'@centelsa.com.co';
        $filter  = "(mail=$dominio)";
        $this->search = ldap_search($this->ldapcon,"dc=centelsa,dc=cables,dc=xignux,dc=com", $filter);
        exit();
      /*

      // Obtener el usuario y la contrase�a capturada por el navegador
      if (isset($_SERVER['PHP_AUTH_USER'])) {
        $ws = 'Apache';
        $usuario = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PWD'];
        echo Zend_Registry::get('Zend_Translate')->_('Get local user 1...').'<br />Usuario: '.$usuario.'<br>Pass:'.$password;
      } elseif (isset($_SERVER['AUTH_USER'])) {
        $ws = 'IIS';
        $usuario = $_SERVER['AUTH_USER'];
        $password = $_SERVER['AUTH_PASSWORD'];
        echo Zend_Registry::get('Zend_Translate')->_('Get local user 2...').'<br />';
      }
      exit;
      */
      $this->_helper->content->setEnabled();
      $api                   = Engine_Api::_()->encuestas();
      $this->view->encuestas = $api->getListaEncuestas();
    }

    //FUNCION DE LA PAGINA CREAR UNA ENCUESTA
    public function crearAction(){
      $this->_helper->content->setEnabled();
      $api     = Engine_Api::_()->encuestas();
      $viewer  = $api->getViewer();
      $request = $this->getRequest();

      // Para editar una encuesta
      $survey_id = $request->getParam('survey_id', null);
      if ($survey_id) {
        $this->view->encuesta = $api->getFullEncuesta($survey_id);
      }

      $this->view->tipos_preguntas = $api->getTiposPreguntas();
      $this->view->allUsersList    = $api->getAllUsersList();
      $this->view->countUsers      = count($this->view->allUsersList);
    }

    //FUNCION DE LA PAGINA DUPLICAR UNA ENCUESTA
    public function duplicarAction(){
      $api     = Engine_Api::_()->encuestas();
      $viewer  = $api->getViewer();
      $request = $this->getRequest();

      // Para editar una encuesta
      $survey_id = $request->getParam('survey_id', null);
      if ($survey_id) {
        $this->view->encuesta = $api->getFullEncuesta($survey_id);
      }

      $this->view->tipos_preguntas = $api->getTiposPreguntas();
      $this->view->allUsersList    = $api->getAllUsersList();
      $this->view->countUsers      = count($this->view->allUsersList);
    }

    //FUNCION QUE GUARDA LOS DATOS GENERALES DE UNA ENCUESTA
    public function savegeneralAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      // Solo aceptar request POST
      if (!$this->getRequest()->isPost()) {
        return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
      }

      $api   = Engine_Api::_()->encuestas();
      $datos = $api->getRequest();

      $tableEncuestas = $api->getTableEncuestas();
      $db_survey      = $tableEncuestas->getAdapter();
      $db_survey->beginTransaction();

      try {
        // Crear una encuesta nueva si el campo survey_id NO está en el request,
        // De lo contrario actualizar la encuesta existente
        if (isset($datos['survey_id'])) {
          $encuesta     = $tableEncuestas->findRow($datos['survey_id']);
          $cantPaginas  = $encuesta['pages'];
          $cantUsers    = count($datos['users']);
          $cantQuestion = count($datos['questions']);
        } else {
          $encuesta            = $tableEncuestas->createRow();
          $datos['created_at'] = date('Y-m-d H:i:s');
          $cantPaginas         = 1;
          $cantUsers           = 0;
          $cantQuestion        = 0;
        }

        // Guardar la encuesta
        $valuesSurvey = array_merge($datos, array(
            'pages'           => $cantPaginas,
            'total_users'     => $cantUsers,
            'total_questions' => $cantQuestion,
            'updated_at'      => date('Y-m-d H:i:s'),
          )
        );

        $encuesta->setFromArray($valuesSurvey);
        $encuesta->save();   

        // Commit a la encuesta
        $db_survey->commit();

        $api->sendSuccessResponse('Se guardaron los datos exitosamente.', $encuesta['survey_id']);
      } catch (Exception $e) {
        $db_survey->rollBack();
        $api->sendErrorResponse($e->getMessage(), null);
      }
    }
    
    //FUNCION QUE ACTUALIZA LA CANTIDAD DE PAGINAS DE LA ENCUESTA
    public function actualizapaginaAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      // Solo aceptar request POST
      if (!$this->getRequest()->isPost()) {
        return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
      }

      $api   = Engine_Api::_()->encuestas();
      $datos = $api->getRequest();

      $tableEncuestas = $api->getTableEncuestas();
      $db_survey      = $tableEncuestas->getAdapter();
      $db_survey->beginTransaction();

      try {
        $encuesta = $tableEncuestas->findRow($datos['survey_id']);

        // Guardar la encuesta
        $valuesSurvey = array('pages' => $datos['pages']);

        $encuesta->setFromArray($valuesSurvey);
        $encuesta->save();   

        // Commit a la encuesta
        $db_survey->commit();
      } catch (Exception $e) {
        $db_survey->rollBack();
        $api->sendErrorResponse($e->getMessage(), null);
      }
    }

    //FUNCION QUE ACTUALIZA LA CANTIDAD DE PAGINAS DE LA ENCUESTA
    public function eliminapaginaAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      // Solo aceptar request POST
      if (!$this->getRequest()->isPost()) {
        return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
      }

      $api    = Engine_Api::_()->encuestas();
      $datos  = $api->getRequest();
      $laPag  = $datos['pages'];

      //Modificacion a las preguntas
      $tabQuestions   = Engine_Api::_()->getDbtable('questions', 'encuestas');
      $selQuestions   = $tabQuestions->select()->where("survey_id = ? ", $datos['survey_id'])->where("page >= ? ", $laPag);
      $listaQuestions = $tabQuestions->fetchAll($selQuestions);

      foreach ($listaQuestions as $key => $question) {
        $pagina = $question->page - 1;
        $tabQuestions->update(array('page' => $pagina), array('question_id = ?' => $question->question_id)); 
      }

        
      $tableEncuestas = $api->getTableEncuestas();
      $db_survey      = $tableEncuestas->getAdapter();
      $db_survey->beginTransaction();

      // Guardar la encuesta
      try {
        $encuesta = $tableEncuestas->findRow($datos['survey_id']);      
        $valuesSurvey = array('pages' => $datos['total']);
        $encuesta->setFromArray($valuesSurvey);
        $encuesta->save();
        $db_survey->commit();
      } catch (Exception $e) {
        $db_survey->rollBack();
        $api->sendErrorResponse($e->getMessage(), null);
      }
    }

    //FUNCION QUE GUARDA LOS DATOS DE UNA PREGUNTA DE LA ENCUESTA
    public function savepreguntaindAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      // Solo aceptar request POST
      if (!$this->getRequest()->isPost()) {
        return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
      }

      $api            = Engine_Api::_()->encuestas();
      $datos          = $api->getRequest();
      $survey_id      = $datos[0];
      $question       = $datos[1];
      $tableEncuestas = $api->getTableEncuestas();
      $db_survey      = $tableEncuestas->getAdapter();
      $db_survey->beginTransaction();

      try {
        $encuesta    = $tableEncuestas->findRow($survey_id);
        $question_id = $api->GuardarPregunta($question, $encuesta);
        $api->sendSuccessResponse('Se guardo los datos de la pregunta exitosamente.', $question_id);
      } catch (Exception $e) {
        $db_survey->rollBack();
        $api->sendErrorResponse($e->getMessage(), null);
      }
    }

    //FUNCION QUE GUARDA LAS PREGUNTAS DE UNA ENCUESTA
    public function savepreguntasAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      // Solo aceptar request POST
      if (!$this->getRequest()->isPost()) {
        return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
      }

      $api = Engine_Api::_()->encuestas();
      $datos = $api->getRequest();

      $tableEncuestas = $api->getTableEncuestas();
      $db_survey      = $tableEncuestas->getAdapter();
      $db_survey->beginTransaction();

      try {

        // Crear una encuesta nueva si el campo survey_id NO está en el request,
        // De lo contrario actualizar la encuesta existente
        if (isset($datos['survey_id'])) {
          $encuesta = $tableEncuestas->findRow($datos['survey_id']);
          // Validar si la encuesta ya ha sido enviada no se puede editar.
          if ($encuesta['is_sent']) {
            return $api->sendErrorResponse("La encuesta ya ha sido enviada a los invitados y no puede ser editada.", null);
          }
          // Si está en modo de edición entonces se deben eliminar todas las preguntas
          //$api->EliminarTodasLasPreguntas($encuesta['survey_id']);
        } else {
          $encuesta            = $tableEncuestas->createRow();
          $datos['created_at'] = date('Y-m-d H:i:s');
        }

        // Guardar la encuesta
        $valuesSurvey = array_merge($datos, array(
          'total_users'     => count($datos['users']),
          'total_questions' => count($datos['questions']),
          'updated_at'      => date('Y-m-d H:i:s'),
        )
        );

        $encuesta->setFromArray($valuesSurvey);
        $encuesta->save();

        // Guardar las preguntas de una encuesta
        $tableQuestions = $api->getTableQuestions();
        $db_qestion     = $tableQuestions->getAdapter();
        $db_qestion->beginTransaction();
        foreach ($datos['questions'] as $key => $question) {
          // Guardar cada una de las preguntas de la encuesta
          $api->GuardarPregunta($tableQuestions, $db_qestion, $question, $encuesta);
        }

        // Commit a la encuesta
        $db_survey->commit();

        $api->sendSuccessResponse('Se guardaron las preguntas exitosamente.', $encuesta['survey_id']);
      } catch (Exception $e) {
        $db_survey->rollBack();
        $api->sendErrorResponse($e->getMessage(), null);
      }
    }

    //FUNCION QUE GUARDA LOS USUARIOS INVITADOS A UNA ENCUESTA
    public function saveinvitadosAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      // Solo aceptar request POST
      if (!$this->getRequest()->isPost()) {
        return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
      }

      try {
        $api            = Engine_Api::_()->encuestas();
        $datos          = $api->getRequest();
        $tableEncuestas = $api->getTableEncuestas();
        $db_survey      = $tableEncuestas->getAdapter();
        $db_survey->beginTransaction();

        // Crear una encuesta nueva si el campo survey_id NO está en el request,
        // De lo contrario actualizar la encuesta existente
        if (isset($datos['survey_id'])) {
          $encuesta = $tableEncuestas->findRow($datos['survey_id']);
          // Validar si la encuesta ya ha sido enviada no se puede editar.
          if ($encuesta['is_sent']) {
            return $api->sendErrorResponse("La encuesta ya ha sido enviada a los invitados y no puede ser editada.", null);
          }
        } else {
          $encuesta            = $tableEncuestas->createRow();
          $datos['created_at'] = date('Y-m-d H:i:s');
        }

        // Guardar la encuesta
        $valuesSurvey = array_merge($datos, array(
            'total_users'     => count($datos['users']),
            'updated_at'      => date('Y-m-d H:i:s'),
          )
        );

        $encuesta->setFromArray($valuesSurvey);
        $encuesta->save();

        //Eliminar usuarios invitados
        Engine_Api::_()->getDbtable('users', 'encuestas')->delete( array('survey_id = ?' => $datos['survey_id']) );

        
        // Guardar los usuarios a quienes va dirigida la encuesta
        $api->GuardarUsuariosEncuesta($datos['users'], $encuesta);

        // Commit a la encuesta
        $db_survey->commit();

        $api->sendSuccessResponse('Se guardaron los usuarios invitados exitosamente.');
      } catch (Exception $e) {
        $db_survey->rollBack();
        $api->sendErrorResponse($e->getMessage(), null);
      }
    }

    //FUNCION QUE ENVIA LA ENCUESTA A LOS USUARIOS INVITADOS
    public function sendAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      // Solo aceptar request POST
      if (!$this->getRequest()->isPost()) {
        return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
      }

      $api   = Engine_Api::_()->encuestas();
      $datos = $api->getRequest();

      $tableEncuestas = $api->getTableEncuestas();
      $db_survey      = $tableEncuestas->getAdapter();
      $db_survey->beginTransaction();

      try {

        $encuesta   = $tableEncuestas->findRow($datos['survey_id']);
        $tableUser  = Engine_Api::_()->getDbtable('users', 'encuestas');
        $tableUsua  = Engine_Api::_()->getDbtable('users', 'user');
        $selectUser = $tableUser
                      ->select()
                      ->setIntegrityCheck(false)
                      ->from( array("u1"  => $tableUser->info('name')), array("u2.user_id", "u2.displayname", "u2.email") )
                      ->join( array("u2"  => $tableUsua->info('name')), 'u1.item_id = u2.user_id', array() )
                      ->where("u1.survey_id = ? ", $encuesta['survey_id']);
        $usuariosEnv = $tableUser->fetchAll($selectUser);


        // TODO
        // Enviar email a todos los usuariosEnv invitados a la encuesta
        $viewer      = Engine_Api::_()->user()->getViewer();
        $loggedId    = $viewer->user_id;
        $loggedEmail = $viewer->email;
        $loggedName  = $viewer->displayname;
        $fecha       = date("Y-m-d H:i:s");

        
        foreach ($usuariosEnv as $key => $usuario) {

          $receiver  = $usuario['email'];
          $emailHtml = '<!DOCTYPE html>
                        <html>
                        <head>
                          <title>Mailing Encuestas</title>
                        </head>
                        <body>

                          <table width="750px">
                            <tbody>
                              <tr>
                                <td>
                                  <img src="http://vectorial-portal.vectorial.co/centelsa/mailing-encabezado.jpg" alt="" height="167px" width="750px">
                                </td>
                              </tr>

                              <tr>
                                <td align="justify" style="padding: 40px;">

                                  <h1>Hola <b>'.$usuario['displayname'].',</b><br><br></h1>

                                  <h2>Has sido invitado a responder la siguiente encuesta: <span style="color:#5ca9f4">'.$encuesta['title'].'</span></h2>

                                  <center>
                                    <a href="http://www.conexioncentelsa.com" >
                                      <img src="http://vectorial-portal.vectorial.co/centelsa/mailing-boton.png" alt="" height="90px" width="350px">
                                    </a>
                                  </center>
                                </td>
                              </tr>

                              <tr>
                                <td>
                                  <img src="http://vectorial-portal.vectorial.co/centelsa/mailing-pie.jpg" alt="" height="77" width="750px">
                                </td>
                              </tr>
                            </tbody>
                          </table>

                        </body>
                        </html>';

          /*/Enviando el correo al reconocido
          $mail = new Zend_Mail();
          $mail->setBodyHtml($emailHtml);
          $mail->setFrom($loggedEmail, $loggedName);
          $mail->addTo($receiver);
          $mail->setSubject('ENCUESTAS');
          $mail->send();*/

          Engine_Api::_()->core()->PHPMailerSendEmail("Usuario Intranet", $receiver, $emailHtml, "CENTELSA INTRANET ENCUESTAS");

        }

        unset($datos['pages']);

        // Guardar la encuesta
        $valuesSurvey = array_merge($datos, array('is_active' => 1, 'is_sent' => 1, 'updated_at' => date('Y-m-d H:i:s')));

        $encuesta->setFromArray($valuesSurvey);
        $encuesta->save();

        // Commit a la encuesta
        $db_survey->commit();


        $api->sendSuccessResponse('Se envio la invitacion a los usuarios exitosamente.', $encuesta['survey_id']);
      } catch (Exception $e) {
        $db_survey->rollBack();
        $api->sendErrorResponse($e->getMessage(), null);
      }
    }
    
    //FUNCION QUE ACTIVA O INACTIVA UNA ENCUESTA
    public function setactiveAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      $api = Engine_Api::_()->encuestas();

      // Solo aceptar request POST
      if (!$this->getRequest()->isPost()) {
        return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
      }

      $datos = $api->getRequest();

      // Validaciones de los parámetros
      if (!isset($datos['survey_id'])) {
        return $api->sendErrorResponse('El parámetro ( survey_id ) no está presente en el request.', null);
      }
      if (!isset($datos['is_active'])) {
        return $api->sendErrorResponse('El parámetro ( is_active ) no está presente en el request.', null);
      }

      $tableEncuestas = $api->getTableEncuestas();
      $db_survey      = $tableEncuestas->getAdapter();
      $db_survey->beginTransaction();

      try {
        $encuesta = $tableEncuestas->findRow($datos['survey_id']);

        unset($datos['pages'], $datos['is_sent'], $datos['is_anonymous'], $datos['total_users'], $datos['total_answers'], $datos['total_questions']);

        // Guardar la encuesta
        $valuesSurvey = array_merge($datos, array('updated_at' => date('Y-m-d H:i:s')));

        $encuesta->setFromArray($valuesSurvey);
        $encuesta->save();

        // Commit a la encuesta
        $db_survey->commit();

        $api->sendSuccessResponse('La encuesta se activo exitosamente.', $encuesta['is_active']);
      } catch (Exception $e) {
        $db_survey->rollBack();
        $api->sendErrorResponse($e->getMessage(), null);
      }
    }

    //FUNCION QUE ENVIA UNA ENCUESTA A LOS USUARIOS INVITADOS
    public function sendsurveyAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      $api = Engine_Api::_()->encuestas();

      // Solo aceptar request POST
      if (!$this->getRequest()->isPost()) {
        return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
      }

      $datos = $api->getRequest();

      // Validaciones de los parámetros
      if (!isset($datos['survey_id'])) {
        return $api->sendErrorResponse('El parámetro ( survey_id ) no está presente en el request.', null);
      }

      $tableEncuestas = $api->getTableEncuestas();
      $db_survey      = $tableEncuestas->getAdapter();
      $db_survey->beginTransaction();


      $tableUser  = Engine_Api::_()->getDbtable('users', 'encuestas');
      $tableUsua  = Engine_Api::_()->getDbtable('users', 'user');
      $selectUser = $tableUser
                    ->select()
                    ->setIntegrityCheck(false)
                    ->from( array("u1"  => $tableUser->info('name')), array("u2.user_id", "u2.displayname", "u2.email") )
                    ->join( array("u2"  => $tableUsua->info('name')), 'u1.item_id = u2.user_id', array() )
                    ->where("u1.survey_id = ? ", $datos['survey_id']);
      $usuariosEnv   = $tableUser->fetchAll($selectUser);

      try {
        $encuesta = $tableEncuestas->findRow($datos['survey_id']);
        if ($encuesta['is_sent']) {
          // Prevenir que no se envíen correos a encuestas ya enviadas anteriormente
          return $api->sendErrorResponse("Encuesta ya ha sido enviada anteriormente.", null);
        }

        if ($encuesta['is_active'] == 0) {
          // Prevenir que envíe la encuesta sin estar activa
          return $api->sendErrorResponse("Para enviar tu encuesta, primero debes hacer clic en el botón ‘Activar’.", null);
        }

        // Validar que la encuesta tenga todos los campos requeridos para poder enviarse a los invitados
        // Debe tener una fecha límite, descripción, instrucciones, mínimo un usuario invitado y al menos una pregunta.
        if ($encuesta['limit_date'] <= Date('Y-m-d')) {
          return $api->sendErrorResponse('La encuesta debe tener una fecha límite válida.', null);
        }
        
        //if ($encuesta['total_questions'] == 0) 
        if ($api->getTotalQuestions($encuesta['survey_id']) == 0) {
          return $api->sendErrorResponse('Debe crear al menos una pregunta antes de enviar esta encuesta.', null);
        }
        if ($encuesta['total_users'] == 0) {
          return $api->sendErrorResponse('Debe invitar al menos un usuario antes de enviar esta encuesta.', null);
        }

        // TODO
        // Enviar email a todos los usuariosEnv invitados a la encuesta
        $viewer      = Engine_Api::_()->user()->getViewer();
        $loggedId    = $viewer->user_id;
        $loggedEmail = $viewer->email;
        $loggedName  = $viewer->displayname;
        $fecha       = date("Y-m-d H:i:s");

        
        foreach ($usuariosEnv as $key => $usuario) {

          $receiver  = $usuario['email'];
          $emailHtml = '<!DOCTYPE html>
                        <html>
                        <head>
                          <title>Mailing Encuestas</title>
                        </head>
                        <body>

                          <table width="750px">
                            <tbody>
                              <tr>
                                <td>
                                  <img src="http://vectorial-portal.vectorial.co/centelsa/mailing-encabezado.jpg" alt="" height="167px" width="750px">
                                </td>
                              </tr>

                              <tr>
                                <td align="justify" style="padding: 40px;">

                                  <h1>Hola <b>'.$usuario['displayname'].',</b><br><br></h1>

                                  <h2>Has sido invitado a responder la siguiente encuesta: <span style="color:#5ca9f4">'.$encuesta['title'].'</span></h2>

                                  <center>
                                    <a href="http://www.conexioncentelsa.com/encuestas/index/contestar/survey_id/'.$encuesta['survey_id'].'/user_id/'.$usuario['user_id'].'" >
                                      <img src="http://vectorial-portal.vectorial.co/centelsa/mailing-boton.png" alt="" height="90px" width="350px">
                                    </a>
                                  </center>
                                </td>
                              </tr>

                              <tr>
                                <td>
                                  <img src="http://vectorial-portal.vectorial.co/centelsa/mailing-pie.jpg" alt="" height="77" width="750px">
                                </td>
                              </tr>
                            </tbody>
                          </table>

                        </body>
                        </html>';

          /*/Enviando el correo al reconocido
          $mail = new Zend_Mail();
          $mail->setBodyHtml($emailHtml);
          $mail->setFrom($loggedEmail, $loggedName);
          $mail->addTo($receiver);
          $mail->setSubject('ENCUESTA');
          $mail->send();*/

          Engine_Api::_()->core()->PHPMailerSendEmail("Usuario Intranet", $receiver, $emailHtml, "CENTELSA INTRANET ENCUESTAS");
        }


        unset($datos['pages'], $datos['is_sent'], $datos['is_anonymous'], $datos['total_users'], $datos['total_answers'], $datos['total_questions']);

        // Guardar la encuesta
        $valuesSurvey = array_merge($datos, array('is_sent' => 1, 'updated_at' => date('Y-m-d H:i:s')));

        $encuesta->setFromArray($valuesSurvey);
        $encuesta->save();

        // Commit a la encuesta
        $db_survey->commit();

        $api->sendSuccessResponse('Encuesta enviada exitosamente.', $encuesta['is_sent']);
      } catch (Exception $e) {
        $db_survey->rollBack();
        $api->sendErrorResponse($e->getMessage(), null);
      }
    }
  
    //FUNCION QUE ACTIVA O INACTIVA UNA ENCUESTA
    public function deletequestionAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      // Solo aceptar request POST
      if (!$this->getRequest()->isPost()) {
        return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
      }

      $api   = Engine_Api::_()->encuestas();
      $datos = $api->getRequest();
      
      try {
        //Eliminando las opciones de matriz, si tiene
        Engine_Api::_()->getDbtable('matrizoptions', 'encuestas')->delete( array('question_id = ?' => $datos['question_id']) );

        //Eliminando las preguntas de matriz, si tiene
        Engine_Api::_()->getDbtable('matrizquestions', 'encuestas')->delete( array('question_id = ?' => $datos['question_id']) );

        //Eliminando las opciones de tipo multiple
        Engine_Api::_()->getDbtable('questioncerradas', 'encuestas')->delete( array('question_id = ?' => $datos['question_id']) );

        //Eliminando la pregunta
        Engine_Api::_()->getDbtable('questions', 'encuestas')->delete( array('question_id = ?' => $datos['question_id']) );

      } catch (Exception $e) {
        $api->sendErrorResponse($e->getMessage(), null);
      }
    }

    //Funcion que muestra los reportes de un usuario y una encuesta especifica
    public function vistapreviaAction(){
      $this->_helper->content->setEnabled();
      
      $api       = Engine_Api::_()->encuestas();
      $viewer    = $api->getViewer();
      $request   = $this->getRequest();    
      $survey_id = $request->getParam('survey_id', null); 
      $this->view->encuesta  = $api->getFullEncuesta($survey_id);
      $this->view->survey_id = $survey_id;
    }

    public function mailAction(){
    }

  //FUNCIONES PARA GENERAR REPORTES

    //FUNCIONES DE LA VISTA DE REPORTE INDIVIDUAL
    public function reporteindividualAction(){
      $this->_helper->content->setEnabled();

      $api     = Engine_Api::_()->encuestas();
      $request = $this->getRequest();    

      $survey_id = $survey_id = $request->getParam('survey_id', null);
      if (is_null($survey_id)) {
        $this->_redirect('/encuestas/admin');
      }

      $tableEncuestas = $api->getTableEncuestas();
      $db_survey      = $tableEncuestas->getAdapter();
      $db_survey->beginTransaction();
      $encuesta = $tableEncuestas->findRow($survey_id)->toArray();
      $this->view->survey_id = $survey_id;
      $this->view->users     = $api->getSurveyUsers($survey_id);
      $this->view->encuesta  = $encuesta;
    }

    //Funcion que muestra los reportes de un usuario y una encuesta especifica
    public function reporteindividualuserAction(){
      //$this->_helper->content->setEnabled();

      $api     = Engine_Api::_()->encuestas();
      $viewer  = $api->getViewer();
      $request = $this->getRequest();

      $this->view->survey_id = $survey_id = $request->getParam('survey_id', null);
      $this->view->user_id   = $user_id   = $request->getParam('user_id', null);

      $encuesta   = $api->getFullEncuesta($survey_id);
      $respuestas = $api->getSurveyAnswers($encuesta, $user_id);
      $this->view->encuesta = $respuestas;
    } 

    //FUNCION DE LA VISTA DE GENERAR REPORTES
    public function reportesAction(){
      $this->_helper->content->setEnabled();

      $api     = Engine_Api::_()->encuestas();
      $viewer  = $api->getViewer();

      $this->view->listadoEncuestas = $api->getListaEncuestas();
    }  

    //FUNCION QUE GENERA EL REPORTES DETALLADO
    public function genreporteAction(){
      header('Content-type: application/vnd.ms-excel');
      header("Content-Disposition: attachment; filename=reporte_encuesta.xls");
      header("Expires: 0");

      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $apiEncues = Engine_Api::_()->encuestas();

      $request   = $this->getRequest();
      $survey_id = $request->getParam('survey_id', null);
      $fecha_ini = $request->getParam('fecha_ini', null) . ' 00:00:00';
      $fecha_fin = $request->getParam('fecha_fin', null) . ' 23:59:59';
      $totales   = array();

      if ($survey_id) {
        $encuesta = $apiEncues->getFullEncuesta($survey_id);
      }

      //Pintando las preguntas generales
      $html  = '<h2 style="color:#2B70B5;">'. $encuesta['title'] .'</h2>';
      $html .= '<table style="font-size:14px; font-family: Arial;">
                      <thead>
                        <tr>
                          <th colspan="4" style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;"></th>';
      foreach ($encuesta['questions'] as $key => $questions1) {
        $colspan = 0;
        if ($questions1['type'] == 'abierta') {
          $colspan = 1;
        }elseif($questions1['type'] == 'cerrada'){
          $colspan = count($questions1['cerrada']['options']);
          if ($questions1['has_another'] == 1) $colspan++;
        }elseif($questions1['type'] == 'matriz'){
          $colspan  = count($questions1['matriz']['questions']) * count($questions1['matriz']['options']);
        }
        $html .= '<th colspan="'.$colspan.'" style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center">'.$questions1['title'].'</th>';
      }

      //Pintando las opciones
      $html .= '        </tr>
                        <tr>
                          <td style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center" colspan="4"></td>';   
      foreach ($encuesta['questions'] as $key => $questions2) {
        $colspan = 0;

        if ($questions2['type'] == 'abierta') {
          $html .= '<td style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;"></td>';

        }elseif($questions2['type'] == 'cerrada'){

          $col1 = count($questions2['cerrada']['options']);
          if ($questions2['has_another'] == 1) $col1++;

          $html .= '<td style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" colspan="'.$col1.'"></td>';

        }elseif($questions2['type'] == 'matriz'){
          $col0 = count($questions2['matriz']['options']);        
          foreach ($questions2['matriz']['questions'] as $key => $questionm) {
            $html .= '<td style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center" colspan="'.$col0.'">'.$questionm['value'].'</td>';
          }
        }
      }

      $html .= '        </tr>
                        <tr>
                          <td style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center">No</td>
                          <td style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center">Usuario Invitado</td>
                          <td style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center">Correo del usuario</td>
                          <td style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center">Estado</td>';

      $totales = array();


      foreach ($encuesta['questions'] as $key => $questions2) {
        $totales[$questions2['question_id']] = array(
          'type'        => $questions2['type'],
          'options'     => array(),
          'has_another' => 0,
          'ans_another' => 0
        );

        if ($questions2['type'] == 'abierta') {
          $html .= '<td style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;"></td>';

        }elseif($questions2['type'] == 'cerrada'){
          
          foreach ($questions2['cerrada']['options'] as $key => $option) {
            $html .= '<td style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center">'.$option['value'].'</td>';
            $totales[$questions2['question_id']]['options'][$option['questioncerrada_id']] = 0;
          }

          if ($questions2['has_another'] == 1) {
            $html .= '<td style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center">'.$questions2['name_another'].'</td>';
            $totales[$questions2['question_id']]['has_another'] = 1;
            $totales[$questions2['question_id']]['ans_another'] = 0;
          }

        }elseif($questions2['type'] == 'matriz'){        
          foreach ($questions2['matriz']['questions'] as $key => $questionm) {  

            $totales[$questions2['question_id']]['preguntas'][$questionm['matrizquestion_id']]['options'] = array();
           
            foreach ($questions2['matriz']['options'] as $key => $optionm) {
              $html .= '<td style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center">'.$optionm['value'].'</td>';
              $totales[$questions2['question_id']]['preguntas'][$questionm['matrizquestion_id']]['options'][$optionm['matrizoption_id']] = 0; 

            }
          }
        }
      }
      $html .= '        </tr>
                      </thead>
                      <tbody>';


      //Sacando los resultados de los usuarios
      for ($i=0; $i < count($encuesta['users']); $i++) { 
        $user       = $encuesta['users'][$i]; 
        $questions3 = array();
        $answers    = $apiEncues->getSurveyAnswers($encuesta, $user['item_id']);
        $cont++;

        if($cont % 2 == 1)
          $color = "#DBDBDB";
        else
          $color = "";

        if ( $answers['is_finalized'] == 1 )
          $estado = "Finalizado";
        else
          $estado = "Pendiente";


        $num     = $i + 1;    
        $html   .= '<tr>
                      <td align="center" style="background-color: '.$color.'; border: 1px solid black;">'.$num.'</td>
                      <td align="center" style="background-color: '.$color.'; border: 1px solid black;">'.$user['displayname'].'</td>
                      <td align="center" style="background-color: '.$color.'; border: 1px solid black;">'.$user['email'].'</td>
                      <td align="center" style="background-color: '.$color.'; border: 1px solid black;">'.$estado.'</td>';

        for ($j=0; $j < count($encuesta['questions']); $j++) {
          
          $questions3  = $answers['questions'][$j];

          if($questions3['type'] == 'cerrada'){

            if ($questions3['cerrada']['value'] == 'unica') {
              
              foreach ($questions3['cerrada']['options'] as $key => $option) {
                if ($option['questioncerrada_id'] == $questions3['answer']){
                  $html .= '<td align="center" style="background-color: '.$color.'; border: 1px solid black;" align="center">x</td>';
                  $totales[$questions3['question_id']]['options'][$option['questioncerrada_id']] += 1;
                }else{
                  $html .= '<td align="center" style="background-color: '.$color.'; border: 1px solid black;" align="center"></td>';
                }             
              }
              
            }else{

              foreach ($questions3['cerrada']['options'] as $key => $option) {
                $esta = 0;

                foreach ($questions3['answer'] as $key => $answer) {
                  if ($answer == $option['questioncerrada_id']) {
                    $esta = 1;
                    $totales[$questions3['question_id']]['options'][$option['questioncerrada_id']] += 1;
                  }
                }

                if ($esta == 1){
                  $html .= '<td align="center" style="background-color: '.$color.'; border: 1px solid black;" align="center">x</td>';
                }else{
                  $html .= '<td align="center" style="background-color: '.$color.'; border: 1px solid black;" align="center"></td>';
                }              
              }            
            }
            
            if ($questions3['has_another'] == 1) {
              $html .= '<td align="center" style="background-color: '.$color.'; border: 1px solid black;"  align="center">'.$questions3['another_answer'].'</td>';
              
              if ($questions3['another_answer'] != '') {
                $totales[$questions3['question_id']]['ans_another'] += 1;
              }            
            }
          
          }elseif($questions3['type'] == 'matriz'){         
            foreach ($questions3['matriz']['questions'] as $key => $questionm) {
              $laOpcion = "";
              foreach ($questions3['answer'] as $key => $answer) {
                if ($answer['answ_mtr_question_id'] == $questionm['matrizquestion_id']) {
                  
                  foreach ($questions3['matriz']['options'] as $key => $optionm) {
                    if ($optionm['matrizoption_id'] == $answer['answ_mtr_option_id']) {
                      $laOpcion = $optionm['value'];
                      $html .= '<td align="center" style="background-color: '.$color.'; border: 1px solid black;" align="center">x</td>';
                      $totales[$questions3['question_id']]['preguntas'][$questionm['matrizquestion_id']]['options'][$optionm['matrizoption_id']] += 1;
                    }else{
                      $laOpcion = $optionm['value'];
                      $html .= '<td align="center" style="background-color: '.$color.'; border: 1px solid black;" align="center"></td>';
                    }                
                  }
                }
              }
              //$html .= '<td style="background-color: '.$color.'; padding:5px; font-family: verdana;  align="center">'.$laOpcion.'</td>';
            }
          
          }else{
            $html .= '<td align="center" style="background-color: '.$color.'; border: 1px solid black;" >'.$questions3['answer'].'</td>';
          }
        }
        $html   .= '</tr>';    
      }

      //Sacando los totales
      $html .= '<tr><td colspan="4" style="background-color: #adadad; border: 1px solid black; padding:5px; font-family: verdana; font-weight:bold;" align="right">Totales:</td>';
      foreach ($totales as $key => $total) {
        if ($total['type'] == 'abierta') {
          $html .= '<td style="background-color: #adadad; border: 1px solid black; padding:5px; font-family: verdana; font-weight:bold;"></td>';

        }elseif($total['type'] == 'cerrada'){

          foreach ($total['options'] as $key => $option) {
            $html .= '<td style="background-color: #adadad; border: 1px solid black; padding:5px; font-family: verdana; font-weight:bold; text-align:center">'.$option.'</td>';
          }

          if ($total['has_another'] == 1) {
            $html .= '<td style="background-color: #adadad; border: 1px solid black; padding:5px; font-family: verdana; font-weight:bold; text-align:center">'.$total['ans_another'].'</td>';
          }

        }elseif($total['type'] == 'matriz'){         
          foreach ($total['preguntas'] as $key => $subpreg) {

            foreach ($subpreg['options'] as $key => $option) {
              $html .= '<td style="background-color: #adadad; border: 1px solid black; padding:5px; font-family: verdana; font-weight:bold; text-align:center">'.$option.'</td>';
            }
          }
        }
      }
      $html .= '</tr>';
      $html .= '      </body>
                    </html>';
      
      echo $html; 
      exit;
    }
    
    //FUNCION QUE GENERA EL REPORTES DETALLADO
    public function genreporteconsolidadoAction(){
      header('Content-type: application/vnd.ms-excel');
      header("Content-Disposition: attachment; filename=reporte_encuesta.xls");
      header("Expires: 0");

      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $apiEncues = Engine_Api::_()->encuestas();

      $request   = $this->getRequest();
      $survey_id = $request->getParam('survey_id', null);
      $fecha_ini = $request->getParam('fecha_ini', null) . ' 00:00:00';
      $fecha_fin = $request->getParam('fecha_fin', null) . ' 23:59:59';
      $totales   = array();

      if ($survey_id) {
        $encuesta = $apiEncues->getFullEncuesta($survey_id);
      }

      //Pintando las preguntas generales
      $html  = '<h2 style="color:#2B70B5;">'. $encuesta['title'] .'</h2>';


      //Sacando los totales
      $totales = array();

      //Creando el arreglo de los totales
      foreach ($encuesta['questions'] as $key => $questions2) {
        $totales[$questions2['question_id']] = array(
          'type'        => $questions2['type'],
          'options'     => array(),
          'has_another' => 0,
          'ans_another' => 0
        );
        if($questions2['type'] == 'cerrada'){        
          foreach ($questions2['cerrada']['options'] as $key => $option) {
            $totales[$questions2['question_id']]['options'][$option['questioncerrada_id']] = 0;
          }
          if ($questions2['has_another'] == 1) {
            $totales[$questions2['question_id']]['has_another'] = 1;
            $totales[$questions2['question_id']]['ans_another'] = 0;
          }
        }elseif($questions2['type'] == 'matriz'){        
          foreach ($questions2['matriz']['questions'] as $key => $questionm) {  
            $totales[$questions2['question_id']]['preguntas'][$questionm['matrizquestion_id']]['options'] = array();
            foreach ($questions2['matriz']['options'] as $key => $optionm) {
              $totales[$questions2['question_id']]['preguntas'][$questionm['matrizquestion_id']]['options'][$optionm['matrizoption_id']] = 0; 
            }
          }
        }
      }

      //Sacando los resultados de los usuarios
      for ($i=0; $i < count($encuesta['users']); $i++) { 
        $user       = $encuesta['users'][$i]; 
        $questions3 = array();
        $answers    = $apiEncues->getSurveyAnswers($encuesta, $user['item_id']);
    
        for ($j=0; $j < count($encuesta['questions']); $j++) {        
          $questions3  = $answers['questions'][$j];
          if($questions3['type'] == 'cerrada'){
            if ($questions3['cerrada']['value'] == 'unica') {            
              foreach ($questions3['cerrada']['options'] as $key => $option) {
                if ($option['questioncerrada_id'] == $questions3['answer']){
                  $totales[$questions3['question_id']]['options'][$option['questioncerrada_id']] += 1;
                }            
              }            
            }else{
              foreach ($questions3['cerrada']['options'] as $key => $option) {
                foreach ($questions3['answer'] as $key => $answer) {
                  if ($answer == $option['questioncerrada_id']) {
                    $totales[$questions3['question_id']]['options'][$option['questioncerrada_id']] += 1;
                  }
                }            
              }            
            }
            if ($questions3['has_another'] == 1) {            
              if ($questions3['another_answer'] != '') {
                $totales[$questions3['question_id']]['ans_another'] += 1;
              }            
            }        
          }elseif($questions3['type'] == 'matriz'){         
            foreach ($questions3['matriz']['questions'] as $key => $questionm) {
              foreach ($questions3['answer'] as $key => $answer) {
                if ($answer['answ_mtr_question_id'] == $questionm['matrizquestion_id']) {                
                  foreach ($questions3['matriz']['options'] as $key => $optionm) {
                    if ($optionm['matrizoption_id'] == $answer['answ_mtr_option_id']) {
                      $totales[$questions3['question_id']]['preguntas'][$questionm['matrizquestion_id']]['options'][$optionm['matrizoption_id']] += 1;
                    }              
                  }
                }
              }
            }        
          }
        }
      }

      $html .= '<table style="font-size:14px; font-family: Arial;">
                  <thead>
                    <tr>
                      <th style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center" width="400px">Pregunta</th>
                      <th style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center" width="150px">Totales</th>
                    </tr>
                  </thead>
                  ';
                  
                      foreach ($encuesta['questions'] as $key => $questions1) {
                        
                        //PINTANDO LAS PREGUNTAS CERRADAS (SELECCION MULTIPLE)
                        if($questions1['type'] == 'cerrada'){
                          $html .= '<tr><td colspan="2" style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;">'.$questions1['title'].'</td><tr>';
                          
                          foreach ($questions1['cerrada']['options'] as $key => $option) {
                            $html .= '<tr>
                                        <td style="padding:5px; border: 1px solid black; font-family: verdana;">'.$option['value'].'</td>
                                        <td style="padding:5px; border: 1px solid black; font-family: verdana;">'.$totales[$questions1['question_id']]['options'][$option['questioncerrada_id']].'</td>
                                      </tr>';
                          }
                          
                          if ($questions1['has_another'] == 1) {
                            $html .= '<tr>
                                        <td style="padding:5px; border: 1px solid black; font-family: verdana;">'.$questions1['name_another'].'</td>
                                        <td style="padding:5px; border: 1px solid black; font-family: verdana;">'.$totales[$questions1['question_id']]['ans_another'].'</td>
                                      </tr>';
                          }                
                  
                        }elseif($questions1['type'] == 'matriz'){
                          $html .= '<tr><td colspan="2" style="background-color: #adadad; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;" align="center">'.$questions1['title'].'</td></tr>';
                          foreach ($questions2['matriz']['questions'] as $key => $questionm) {    
                            $html .= '<tr><td colspan="2" style="background-color: #E8E8E8; padding:5px; border: 1px solid black; font-family: verdana; font-weight:bold;">'.$questionm['value'].'</td></tr>';
                            foreach ($questions2['matriz']['options'] as $key => $optionm) {
                              $html .= '<tr>
                                          <td style="padding:5px; border: 1px solid black; font-family: verdana;">'.$optionm['value'].'</td>
                                          <td style="padding:5px; border: 1px solid black; font-family: verdana;">'.$totales [$questions1['question_id']] ['preguntas'] [$questionm['matrizquestion_id']] ['options'] [$optionm['matrizoption_id']].'</td>
                                        </tr>';
                            }
                          }                
                        }      
                      }

                      $html .= '</table>';    
      echo $html;
    }

    //FUNCION QUE GENERA EL REPORTES DETALLADO
    public function genreportedemograficoAction(){
      header('Content-type: application/vnd.ms-excel');
      header("Content-Disposition: attachment; filename=reporte_encuesta_demografica.xls");
      header("Expires: 0");

      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $apiEncues = Engine_Api::_()->encuestas();

      $request   = $this->getRequest();
      $survey_id = $request->getParam('survey_id', null);
      $fecha_ini = $request->getParam('fecha_ini', null) . ' 00:00:00';
      $fecha_fin = $request->getParam('fecha_fin', null) . ' 23:59:59';

      //Obteniendo las tablas
      $tabSurvey    = Engine_Api::_()->getDbtable('surveys',          'encuestas');
      $tabQuestion  = Engine_Api::_()->getDbtable('questions',        'encuestas');
      $tabOption    = Engine_Api::_()->getDbtable('questioncerradas', 'encuestas');
      $tabMatrizQue = Engine_Api::_()->getDbtable('matrizquestions',  'encuestas');
      $tabMatrizOpt = Engine_Api::_()->getDbtable('matrizoptions',    'encuestas');
      $tabAnswers   = Engine_Api::_()->getDbtable('answers',          'encuestas');
      $tabUsuarios  = Engine_Api::_()->getDbtable('users',            'encuestas');
      $encuesta     = $tabSurvey->findRow($survey_id);
      

      /* ///////////////////////////////////////////////// SACANDO LAS PREGUNTAS DEMOGRAFICAS ///////////////////////////////////////////////////////////////// */
      //Sacando las preguntas tipo demograficas
      $questionsDemo = array();
      $selQuestDemo = $tabQuestion
                      ->select()
                      ->setIntegrityCheck(false)
                      ->from(array("Q" => $tabQuestion->info('name')), array("Q.*"))
                      ->where("Q.is_demographic = ? ", 1)
                      ->where("Q.type = ? ", "cerrada")
                      ->where("Q.survey_id = ? ", $survey_id);
      $lisQuestDemo  = $tabQuestion->fetchAll($selQuestDemo);

      //Sacando las preguntas de tipo demograficas
      foreach ($lisQuestDemo as $key => $questionDemo) {

        //Sacando las opciones 
        $sqlOptDemo = $tabOption
                      ->select()
                      ->setIntegrityCheck(false)
                      ->from(array("O" => $tabOption->info('name')), array('O.*'))
                      ->where("O.question_id = ? ", $questionDemo['question_id']);
        $lisOptDemo = $tabOption->fetchAll($sqlOptDemo);
        $opciones   = array();
        $usuarios   = array();
        foreach ($lisOptDemo as $key => $optionDemo) {

          //Sacando los ID de los usuarios que seleccionaron esta opcion
          $sqlUsuarios =  $tabUsuarios
                          ->select()
                          ->setIntegrityCheck(false)
                          ->from(array("U" => $tabUsuarios->info('name')), array("U.item_id"))
                          ->join(array("A"  => $tabAnswers->info('name')), 'U.item_id = A.user_id', array())
                          ->where("A.question_id = ? ",    $questionDemo['question_id'])
                          ->where("A.answ_option_id = ? ", $optionDemo['questioncerrada_id'])
                          ->where("U.survey_id = ? ",      $survey_id)
                          ->where("U.is_finalized = ? ",   1);
          $lisUsuarios = $tabUsuarios->fetchAll($sqlUsuarios);
          $usuariosListIds = $lisUsuarios->toArray();
          $idsUsuariosComa = array();
          array_walk_recursive($usuariosListIds, function($item, $key) use(&$idsUsuariosComa) {
            $idsUsuariosComa[] = $item;
          });

          $idsUsuariosComa = implode(",", $idsUsuariosComa);
          //$idsUsuariosComa = "IN (".$idsUsuariosComa.")";

          array_push($opciones, array(
              'option_id' => $optionDemo['questioncerrada_id'],
              'option'    => $optionDemo['value'],
              'id_users'  => $idsUsuariosComa
            )
          );
        }

        $questionsDemo[$questionDemo['question_id']] =  array(
          'pregunta'    => $questionDemo['title'],
          'options'     => $opciones
        );
      }


      /* //////////////////////////////////////////////////// SACANDO LAS PREGUNTAS A CONTABILIZAR ///////////////////////////////////////////////////////////// */
      //Sacando las preguntas tipo cerrada, seleccion multiple
      $selQuestCer  = $tabQuestion
                      ->select()
                      ->setIntegrityCheck(false)
                      ->from(array("Q"  => $tabQuestion  ->info('name')),  array("Q.question_id", "Q.title AS question", "Q.type", "Q.has_another", "Q.name_another"))
                      ->joinLeft(array("O"  => $tabOption    ->info('name')), 'Q.question_id = O.question_id', array("O.questioncerrada_id AS option_id", "O.value AS opcion"))
                      ->where("Q.is_demographic = ? ", 0)
                      ->where("Q.survey_id = ? ", $survey_id)
                      ->where("Q.type = ? ", "cerrada")
                      ->order("O.questioncerrada_id ASC");
      $lisQuestCer  = $tabQuestion->fetchAll($selQuestCer);
      $questions = array();

      $auxiliar = 0;
      foreach ($lisQuestCer as $key => $questionCer) {
          if ($auxiliar != $questionCer['question_id']) {
            $questions[ $questionCer['question_id'] ] = array(
              "title" => $questionCer['question'],
              "type"  => $questionCer['type'],
              "other" => $questionCer['has_another'],
              "name"  => $questionCer['name_another'],
              "tot"   => 0,
              "options"     => array(
                array(
                  "option_id" => $questionCer['option_id'],
                  "opcion"    => $questionCer['opcion']
                )
              )          
            );
            $auxiliar = $questionCer['question_id'];
          }else{
            array_push($questions[$questionCer['question_id']]['options'], 
              array(
                "option_id" => $questionCer['option_id'],
                "opcion"    => $questionCer['opcion']
              )
            );
          }      
      }
     
      //Sacando las preguntas tipo matriz
      $selQuestMat  = $tabQuestion
                      ->select()
                      ->setIntegrityCheck(false)
                      ->from(array("Q"  => $tabQuestion  ->info('name')),  array("Q.question_id", "Q.title AS question", "Q.type"))
                      ->where("Q.is_demographic = ? ", 0)
                      ->where("Q.survey_id = ? ", $survey_id)
                      ->where("Q.type = ? ", "matriz")
                      ->order("Q.question_id ASC");
      $lisQuestMat  = $tabQuestion->fetchAll($selQuestMat);
      foreach ($lisQuestMat as $key => $questionMat) {
        $questions[ $questionMat['question_id'] ] = array(
          "title"     => $questionMat['question'],
          "type"      => $questionMat['type'],
          "questions" => array(),
          "options"   => array()
        );

        //Sacando las subpreguntas
        $selQuestMatQ  = $tabMatrizQue
                        ->select()
                        ->setIntegrityCheck(false)
                        ->from(array("Q"  => $tabMatrizQue->info('name')),  array("Q.matrizquestion_id AS id", "Q.value AS question"))
                        ->where("Q.question_id = ? ", $questionMat['question_id'])
                        ->order("Q.matrizquestion_id ASC");
        $lisQuestMatQ = $tabMatrizQue->fetchAll($selQuestMatQ);
        foreach ($lisQuestMatQ as $key => $questMatQ) {
          array_push($questions[$questionMat['question_id']]["questions"] , array(
              "id"       => $questMatQ['id'],
              "question" => $questMatQ['question']
            )
          );
        }

        //Sacando las opciones
        $selQuestMatO  = $tabMatrizOpt
                        ->select()
                        ->setIntegrityCheck(false)
                        ->from(array("Q"  => $tabMatrizOpt->info('name')),  array("Q.matrizoption_id AS id", "Q.value AS option"))
                        ->where("Q.question_id = ? ", $questionMat['question_id'])
                        ->order("Q.matrizoption_id ASC");
        $lisQuestMatO = $tabMatrizOpt->fetchAll($selQuestMatO);
        foreach ($lisQuestMatO as $key => $questMatO) {
          array_push($questions[$questionMat['question_id']]["options"] , array(
              "id"     => $questMatO['id'],
              "option" => $questMatO['option']
            )
          );
        }
      }

      /*
      echo '<pre>';
      print_r($questions);
      echo '</pre>';

      echo '<pre>';
      print_r($questionsDemo);
      echo '</pre>';

      exit;
      */

      $contGen = 1;
      $auxCon  = 0;


      //Pintando las preguntas generales
      $html  = '<h2 style="color:#2B70B5;">'.$encuesta['title'].'</h2>';
      $html .= '<table border="1" cellspacing="0" cellpadding="5">';

      //Pintado las preguntas
      foreach ($questions as $question_id => $question) {      
          

        //Pintando las preguntas tipo cerradas
          if($question['type'] == 'cerrada'){  

            $html .= '<tr>
                      <td align="center" style="background-color:#2B70B5; color:white; font-size:18px;"><b>'.$question['title'].'</b></td>';      

            //Pintando las preguntas demograficas
            foreach ($questionsDemo as $demos_id => $demogra) {
              $html .= '<td align="center" style="background-color:#2B70B5; color:white; font-size:18px;" colspan="'.count($demogra['options']).'">
                          <b>'.$demogra['pregunta'].'</b>
                        </td>';
            }
            //Pintando las preguntas demograficas


            //Pintando las opciones de las preguntas demograficas
            $html .= '</tr>
                      <tr>
                        <td style="background-color:#71AEE8; color:white; font-size:14px;"></td>';
            foreach ($questionsDemo as $demos_id => $demogra) {          
              foreach ($demogra['options'] as $optDemo_id => $optDem) {
                if ($auxCon == 0)
                  $contGen++;
                
                $html .= '<td align="center" style="background-color:#71AEE8; color:white; font-size:14px;">
                            '.$optDem['option'].'
                          </td>';
              }
            }
            $html .= '</tr>';
            //Pintando las opciones de las preguntas demograficas

            $auxCon++;


            
            //Sacando las opciones si tipo cerrada
            foreach ($question['options'] as $options_id => $option) {
              $html .= '<tr>
                          <td align="left"><b>'.$option['opcion'].'</b></td>';

              foreach ($questionsDemo as $demos_id => $demogra) {
                foreach ($demogra['options'] as $optDemo_id => $optDem) {
                  $idUsers = explode(",", $optDem['id_users']);
                  $sqlAns = $tabAnswers
                            ->select()
                            ->setIntegrityCheck(false)
                            ->from(array("A"  => $tabAnswers ->info('name')),  array("COUNT(*) AS cantidad"))
                            ->join(array("U"  => $tabUsuarios->info('name')), 'A.user_id = U.item_id', array())
                            ->where("U.survey_id    =  ?  ", $survey_id)
                            ->where("A.question_id    =  ?  ", $question_id)
                            ->where("A.answ_option_id =  ?  ", $option['option_id'])
                            ->where("U.item_id       IN (?) ", $idUsers);
                  $lisAns = $tabAnswers->fetchRow($sqlAns);
                  $html .= '<td align="center">'.$lisAns['cantidad'].'</td>';
                }
              }
              $html .= '</tr>';
            }

            $html .= '<tr><td colspan="'.$contGen.'"><br></td></tr>';
          }
        //Pintando las preguntas tipo cerradas



        //Pintando las preguntas tipo matriz
          if($question['type'] == 'matriz'){

            $html .= '<tr>
                      <td align="center" style="background-color:#2B70B5; color:white; font-size:18px;"><b>'.$question['title'].'</b></td>'; 

            //Pintando las preguntas demograficas
            foreach ($questionsDemo as $demos_id => $demogra) {
              $html .= '<td align="center" style="background-color:#2B70B5; color:white; font-size:18px;" colspan="'.count($demogra['options']).'">
                          <b>'.$demogra['pregunta'].'</b>
                        </td>';
            }
            //Pintando las preguntas demograficas
            $html .= '</tr>'; 


            //Pintando las opciones de las preguntas demograficas
            $html .= '</tr>
                      <tr>
                        <td style="background-color:#71AEE8; color:white; font-size:14px;"></td>';
            foreach ($questionsDemo as $demos_id => $demogra) {          
              foreach ($demogra['options'] as $optDemo_id => $optDem) {
                $html .= '<td align="center" style="background-color:#71AEE8; color:white; font-size:14px;">
                            '.$optDem['option'].'
                          </td>';
              }
            }
            $html .= '</tr>';
            //Pintando las opciones de las preguntas demograficas


            foreach ($question['questions'] as $quest_id => $quest) {
              $html .= '<tr><td align="left" colspan="'.$contGen.'" style="background-color:#ABC3DB; font-size:18px;">'.$quest['question'].'</b></td></tr>';


              foreach ($question['options'] as $opcion_id => $opcion) {
                $html .= '<tr><td align="left" style="font-size:14px; text-align:right">'.$opcion['option'].'</b></td>';


                foreach ($questionsDemo as $demos_id => $demogra) {
                  foreach ($demogra['options'] as $optDemo_id => $optDem) {
                    $idUsers = explode(",", $optDem['id_users']);
                    $sqlAns = $tabAnswers
                              ->select()
                              ->setIntegrityCheck(false)
                              ->from(array("A"  => $tabAnswers ->info('name')),  array("COUNT(*) AS cantidad"))
                              ->join(array("U"  => $tabUsuarios->info('name')), 'A.user_id = U.item_id', array())
                              ->where("U.survey_id    =  ?  "       , $survey_id)
                              ->where("A.question_id    =  ?  "     , $question_id)
                              ->where("A.answ_mtr_question_id =  ? ", $quest['id'])                              
                              ->where("A.answ_mtr_option_id =  ? "  , $opcion['id'])
                              ->where("U.item_id       IN (?) "     , $idUsers);
                    $lisAns = $tabAnswers->fetchRow($sqlAns);
                    $html .= '<td align="center">'.$lisAns['cantidad'].'</td>';
                  }
                }

                $html .= '</tr>';
              }
            }



            $html .= '<tr><td colspan="'.$contGen.'"><br></td></tr>';

          }
        //Pintando las preguntas tipo matriz

      }

      echo $html;
      exit;
    }


    //FUNCION QUE DUPLICA UNA ENCUESTA
    public function duplicarencuestaAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      
      $api = Engine_Api::_()->encuestas();
      $tableSurvey      = $api->getTableEncuestas();
      $tableQuestion    = $api->getTableQuestions();
      $tableOption      = $api->getTableQuestioncerrada();
      $tableMatQuestion = $api->getTableMatrizquestions();
      $tableMatOption   = $api->getTableMatrizoptions();

      $datos     = $this->getRequest()->getParams();
      $survey_id = $datos['survey_id'];
      

      //Preparando las tablas para duplicar los datos
      $db_survey  = $tableSurvey->getAdapter();
      $db_survey->beginTransaction();

      $db_qestion = $tableQuestion->getAdapter();
      $db_qestion->beginTransaction();

      $db_options = $tableOption->getAdapter();
      $db_options->beginTransaction();

      $db_matques = $tableMatQuestion->getAdapter();
      $db_matques->beginTransaction();

      $db_matopts = $tableMatOption->getAdapter();
      $db_matopts->beginTransaction();


      try {

        //Creando la nueva encuesta
        $selSurvey = $tableSurvey->select()->where('survey_id = ?', $survey_id);
        $survey    = $tableSurvey->fetchRow($selSurvey);
        $values_survey = array(
          'title'           => $survey['title'],
          'instructions'    => $survey['instructions'],
          'description'     => $survey['description'],
          'limit_date'      => $survey['limit_date'],
          'pages'           => $survey['pages'],
          'is_active'       => 1,
          'is_sent'         => 0,
          'is_anonymous'    => $survey['is_anonymous'],
          'is_deleted'      => 0,
          'total_users'     => 0,
          'total_questions' => $survey['total_questions'],
          'total_answers'   => 0,
          'updated_at'      => date("Y-m-d H.i:s"),
          'created_at'      => date("Y-m-d H.i:s")
        );

        $surveyNew = $tableSurvey->createRow();   
        $surveyNew->setFromArray($values_survey);
        $surveyNew->save();

        //Se buscan las preguntas de la encuesta
        $selQuests = $tableQuestion->select()->where('survey_id = ?', $survey_id);
        $questions = $tableQuestion->fetchAll($selQuests);

        foreach ($questions as $key => $question) {          
          $values_question = array(
            'survey_id'       => $surveyNew['survey_id'],
            'type'            => $question['type'],
            'type_value'      => $question['type_value'],
            'title'           => $question['title'],
            'page'            => $question['page'],
            'is_required'     => $question['is_required'],
            'is_demographic'  => $question['is_demographic'],
            'has_another'     => $question['has_another'],
            'name_another'    => $question['name_another']
          );

          //Se crea la pregunta
          $questionNew = $tableQuestion->createRow();  
          $questionNew->setFromArray($values_question);
          $questionNew->save();

          //Si la pregunta es de tipo cerrada de seleccion multiple
          if ($question['type'] == 'cerrada') {
            $selOptions = $tableOption->select()->where('question_id = ?', $question['question_id']);
            $options    = $tableOption->fetchAll($selOptions);

            foreach ($options as $key => $option) {

              $values_option = array(
                'question_id' => $questionNew['question_id'],
                'value'       => $option['value']
              );

              //Se crean las opciones de la pregunta
              $optionNew = $tableOption->createRow();  
              $optionNew->setFromArray($values_option);
              $optionNew->save();
            }

          }

          //Si la pregunta es de tipo matriz
          elseif($question['type'] == 'matriz'){

            //Duplicando las preguntas 
            $selMatQuestion = $tableMatQuestion->select()->where('question_id = ?', $question['question_id']);
            $matQuestions   = $tableMatQuestion->fetchAll($selMatQuestion);

            foreach ($matQuestions as $key => $matQuestion) {
              $values_matQuestion = array(
                'question_id' => $questionNew['question_id'],
                'value'       => $matQuestion['value']
              );

              //Se crean las opciones de la pregunta
              $matQuestionNew = $tableMatQuestion->createRow();  
              $matQuestionNew->setFromArray($values_matQuestion);
              $matQuestionNew->save();
            }


            //Duplicando las opciones 
            $selMatOption = $tableMatOption->select()->where('question_id = ?', $question['question_id']);
            $matOptions   = $tableMatOption->fetchAll($selMatOption);

            foreach ($matOptions as $key => $matOption) {
              $values_matOption = array(
                'question_id' => $questionNew['question_id'],
                'value'       => $matOption['value']
              );

              //Se crean las opciones de la pregunta
              $matOptionNew = $tableMatOption->createRow();  
              $matOptionNew->setFromArray($values_matOption);
              $matOptionNew->save();
            }
          }
        }



        // Commit a la encuesta
        $db_survey ->commit();
        $db_qestion->commit();
        $db_options->commit();
        $db_matques->commit();
        $db_matopts->commit();

        $this->_redirect("/encuestas/admin/editar/".$surveyNew['survey_id']);

      } catch (Exception $e) {
        $db_survey ->rollBack();
        $db_qestion->rollBack();
        $db_options->rollBack();
        $db_matques->rollBack();
        $db_matopts->rollBack();
      }


      
      exit;
    }

    /**
     * Actualiza la fecha limite de una encuensta
     */
    public function updatefieldAction( ){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      $api = Engine_Api::_()->encuestas();

      // Solo aceptar request POST
      if (!$this->getRequest()->isPost()) {
        return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
      }

      $datos = $api->getRequest();

       // Validaciones de los parámetros
       if (!isset($datos['survey_id'])) {
        return $api->sendErrorResponse('El parámetro ( survey_id ) no está presente en el request.', null);
      }

      $survey_id = $datos["survey_id"];
      
      $tableEncuestas = $api->getTableEncuestas();
      $db_survey      = $tableEncuestas->getAdapter();
      $db_survey->beginTransaction();

      
      try {
  
        $encuesta = $tableEncuestas->findRow($datos['survey_id']);
  
        $valuesSurvey = array_merge($datos, array('updated_at' => date('Y-m-d H:i:s'), $datos["field"] => $datos["value"]));
        
        $encuesta->setFromArray($valuesSurvey);
        $encuesta->save();

        // Commit a la encuesta
        $db_survey->commit();

        $api->sendSuccessResponse('La encuesta se activo exitosamente', null);

      } catch (Exception $e) {

        $db_survey->rollBack();
        $api->sendErrorResponse($e->getMessage(), null);

      }

    }

    /**
     * Elimina una encuesta:
     * Relaciona las tablas: 
     *    encuestas_surveys, encuestas_users, encuestas_questions, encuestas_questioncerradas, encuestas_matrizquestions, encuestas_matrizoptions, encuestas_answers
     */
    public function removencuestaAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      $api = Engine_Api::_()->encuestas();

      // Solo aceptar request POST
      if (!$this->getRequest()->isPost()) {
        return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
      }

      try {

        //Recibe datos post
        $datos = $api->getRequest();

        //Prepara el adapter db
        $table_questionscerradas = $api->getTableQuestioncerrada();
        
        $db = $table_questionscerradas->getAdapter();

        $db->beginTransaction();

        try{

          if( !isset( $datos["survey_id"] ) )
            throw "No existe el ID survey";

          $surveyId = $datos["survey_id"];
            
          //Eliminacion de questioncerradas
          $query = $db->query("DELETE FROM engine4_encuestas_questioncerradas WHERE question_id 
                                IN( SELECT question_id FROM engine4_encuestas_questions WHERE survey_id = $surveyId )");
                                
          //Eliminacion de matrizoptions
          $query = $db->query("DELETE FROM engine4_encuestas_matrizoptions WHERE question_id 
                      IN( SELECT question_id FROM engine4_encuestas_questions WHERE survey_id = $surveyId )");

          //Eliminacion de matrizquestions
          $query = $db->query("DELETE FROM engine4_encuestas_matrizquestions WHERE question_id 
                      IN( SELECT question_id FROM engine4_encuestas_questions WHERE survey_id = $surveyId )");  

          //Eliminacion de questions
          $query = $db->query("DELETE FROM engine4_encuestas_questions WHERE survey_id = $surveyId"); 

          //Eliminacion de usuarios de survey
          $query = $db->query("DELETE FROM engine4_encuestas_users WHERE survey_id = $surveyId"); 

          //Eliminacion de las answers
          $query = $db->query("DELETE FROM engine4_encuestas_answers WHERE survey_id = $surveyId");

          //Eliminacion de survey
          $query = $db->query("DELETE FROM engine4_encuestas_surveys WHERE survey_id = $surveyId"); 


          $db->commit();

          $encuestas_refreshed = $api->getListaEncuestas();

          $api->sendSuccessResponse('La encuesta se activo exitosamente', null);
          
        } catch (Exception $e) {
          $db->rollBack();
          //throw "Se presentó un error al eliminar la encuesta";
          throw $e;
        }
        
      } catch (Exception $e) {

        //$db_survey->rollBack();
        $api->sendErrorResponse($e->getMessage(), null);

      }


    }


}