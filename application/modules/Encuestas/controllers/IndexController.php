<?php
date_default_timezone_set('America/Bogota');

class Encuestas_IndexController extends Core_Controller_Action_Standard{

  //FUNCION INICIAL
  public function init(){    
    $viewer = Engine_Api::_()->user()->getViewer();
    $user   = $this->_user;
    $idUsu  = $this->_getParam('user_id');

    // No user?
    if ($viewer->getIdentity() == 0 && $idUsu != '') {
      $user = Engine_Api::_()->getItem('user', $idUsu);      
      // Login
      Zend_Auth::getInstance()->getStorage()->write($user->getIdentity());
      header("Refresh:0");
      exit;
    }else if($viewer->getIdentity() == 0){

    }
  }

  //FUNCION DE LA PAGINA INCIAL QUE REDIRECCIONA A LAS ENCUESTAS PROGRAMADAS PARA EL USUARIO LOGUEADO
  public function indexAction(){
    $this->_redirect("/encuestas/index/misencuestas/");
  }

  //FUNCION QUE ENVIA LOS DATOS DE LA ENCUESTA DEL USUARIO LOGUEADO
  public function misencuestasAction(){
    $this->_helper->content->setEnabled();

    $api                      = Engine_Api::_()->encuestas();
    $viewer                   = $api->getViewer();
    $tableSurveys             = $api->getTableEncuestas();
    $this->view->misencuestas = $api->getListaMisEncuestas($viewer->getIdentity());
    $this->view->showButtons  = true;

    $viewer2 = Engine_Api::_()->user()->getViewer();
    if ($viewer2->isModSuperAdmin() == 0) {
      $this->view->showButtons = false;
    }

    // Revisar encuesta por encuesta el porcentaje de respuestas que tiene el usuario logueado
    foreach ($this->view->misencuestas as $key => $encuesta) {
      $total_answers   = $api->getTotalCantidadRespuestas($encuesta['survey_id'], $viewer->getIdentity());
      $total_questions = $api->getTotalQuestions($encuesta['survey_id']);
      $this->view->misencuestas[$key]['total_percentage'] = round(($total_answers * 100) / $total_questions, 2);
      $this->view->misencuestas[$key]['user_id'] = $viewer->getIdentity();

      if (strtotime($encuesta['limit_date']) < strtotime(date("Y-m-d"))) {
        $this->view->misencuestas[$key]['vencio']  = 1;
      }else{
        $this->view->misencuestas[$key]['vencio']  = 0;
      }
    }

  }

  //FUNCION DE LA PAGINA CONTESTAR ENCUESTA
  public function contestarAction(){
    $this->_helper->content->setEnabled();

    $api       = Engine_Api::_()->encuestas();
    $viewer    = $api->getViewer();
    $request   = $this->getRequest();
    $survey_id = $request->getParam('survey_id', null);
    $user_id = $request->getParam('user_id', null);

    // Validar el parametro survey_id
    if (!isset($survey_id)) {
      $mensaje = 'Falta el parámetro ( survey_id )';
      $this->_redirect("/encuestas/index/error/mensaje/".$mensaje);
      exit;
    }

    $my_survey = $api->getMySurvey($viewer->getIdentity(), $survey_id);

    if (!$my_survey) {
      $mensaje = 'Encuesta no encontrada.';
      $this->_redirect("/encuestas/index/error/mensaje/".$mensaje);
      exit;
    }

    if ($my_survey['limit_date'] < Date('Y-m-d')) {
      $mensaje = 'Encuesta pasó su fecha límite.';
      $this->_redirect("/encuestas/index/error/mensaje/".$mensaje);
      exit;
    }

    // Validar si la encuesta ya ha sido finalizada
    if ($api->isFinalizedAnswer($survey_id, $viewer->getIdentity())) {
      $mensaje = 'Encuesta ya ha sido finalizada.';
      $this->_redirect("/encuestas/index/error/mensaje/".$mensaje);
      exit;
    }

    $this->view->my_survey = $api->getFullEncuesta($survey_id);
    $this->view->my_survey = $api->getSurveyAnswers($this->view->my_survey, $viewer->getIdentity());
  }

  //FUNCION QUE GUARDA LAS RESPUESTAS
  public function saveanswersAction(){
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    $api    = Engine_Api::_()->encuestas();
    $viewer = $api->getViewer();

    // Solo aceptar request POST
    if (!$this->getRequest()->isPost()) {
      return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
    }

    $datos = $api->getRequest();
    $survey = $api->getMySurvey($viewer->getIdentity(), $datos['survey_id']);

    if (!$survey) {
      return $api->sendErrorResponse('Encuesta no encontrada.', null);
    }




    //Eliminando las respuestas
    Engine_Api::_()->getDbtable('answers', 'encuestas')->delete(array('survey_id = ?' => $datos['survey_id'], 'user_id = ?' => $viewer->getIdentity())); 

    $tableAnswers = $api->getTableAnswers();

    foreach ($datos['questions'] as $key => $question) {
      $answer       = $api->getUserAnswerByQuestionId($datos['survey_id'], $question['question_id'], $viewer->getIdentity());
      $valuesAnswer = array(
        "survey_id"  => $datos['survey_id'],
        "question_id"=> $question['question_id'],
        "user_id"    => $viewer->getIdentity(),
      );

      switch ($question['type']) {
        case 'abierta':
          if ($question['answer'] == '') {
            // Saltar esta pregunta si la respuesta está vacía
            continue;
          }

          if ($question['abierta']['value'] == 'text') {
            $valuesAnswer = array_merge($valuesAnswer, array(
              'answ_text' => $question['answer'],
            ));
          } elseif ($question['abierta']['value'] == 'textarea') {
            $valuesAnswer = array_merge($valuesAnswer, array(
              'answ_textarea' => $question['answer'],
            ));
          }
          $answer->setFromArray($valuesAnswer);
          $answer->save();
          break;


        case 'cerrada':          
          //Cerrada de respuesta unica
          if ($question['cerrada']['value'] == 'unica') {
            $valuesAnswer = array_merge($valuesAnswer, array(
              'answ_option_id' => $question['answer'],
              'answ_another'   => $question['another_answer'],
            ));
            $answer->setFromArray($valuesAnswer);
            $answer->save();           

          //Cerrada de respuesta multiple
          } elseif ($question['cerrada']['value'] == 'multiple') {
            $elDato = array( 
                            "survey_id = ? "   => $datos['survey_id'],
                            "question_id = ? " => $question['question_id'], 
                            "user_id = ? "     => $viewer->getIdentity()
                            );

            $api->deleteAnswersCerradaMultiple( $elDato );            
            if ($question['has_another'] == true && $question['another_answer'] != '') {
              $answer              = $tableAnswers->createRow();
              $valuesAnswerAnother = array_merge($valuesAnswer, array(
                'answ_another' => $question['another_answer'],
              ));
              $answer->setFromArray($valuesAnswerAnother);
              $answer->save();
            }           
            
            foreach ($question['answer'] as $key => $answer_id) {
              $answer       = $tableAnswers->createRow();
              $valuesAnswer = array_merge($valuesAnswer, array(
                'answ_option_id' => $answer_id,
              ));
              $answer->setFromArray($valuesAnswer);
              $answer->save();
            }
          }
          break;


        case 'matriz':
          if (count($question['answer']) == 0) {
            // Saltar esta pregunta si la respuesta está vacía
            continue;
          }
          foreach ($question['answer'] as $mtr_answer) {
            if (is_null($mtr_answer)) {
              continue;
            }
            $answer       = $api->getUserAnswerByQuestionId($datos['survey_id'], $question['question_id'], $viewer->getIdentity(), false, $mtr_answer['answ_mtr_question_id']);
            $valuesAnswer = array_merge($valuesAnswer, array(
              'answ_mtr_question_id' => $mtr_answer['answ_mtr_question_id'],
              'answ_mtr_option_id'   => $mtr_answer['answ_mtr_option_id'],
            ));
            $answer->setFromArray($valuesAnswer);
            $answer->save();
          }
          break;
      }
    }

    $api->getTotalSurveyAnswers($datos['survey_id']);

    return $api->sendSuccessResponse('Respuestas guardadas exitosamente.', null);
  }

  public function finalizesurveyAction(){
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    $api    = Engine_Api::_()->encuestas();
    $viewer = $api->getViewer();

    // Solo aceptar request POST
    if (!$this->getRequest()->isPost()) {
      return $api->sendErrorResponse('Solo se permiten request tipo POST.', null);
    }

    $datos = $api->getRequest();

    $tableSurveys = $api->getTableEncuestas();
    $encuesta     = $tableSurveys->findRow($datos['survey_id']);
    if (!$encuesta) {
      return $api->sendErrorResponse('Encuesta no encontrada.', null);
    }

    $userTable = $api->getTableUsers();
    $select    = $userTable->select()
      ->where('survey_id = ?', $datos['survey_id'])
      ->where('item_id = ?', $viewer->getIdentity());
    $userSurvey = $userTable->fetchRow($select);

    $userSurvey['is_finalized'] = 1;
    $userSurvey->save();

    return $api->sendSuccessResponse('Encuesta finalizada exitosamente.', null);
  }


  //FUNCION DE LA PAGINA DE ERROR
  public function errorAction(){
    $request = $this->getRequest(); 
    $this->view->mensaje = $request->getParam('mensaje', null);
  }
}
