app.controller("form_survey_ctrl", function($scope, $http) {

    $scope.count_users = 0;
    $scope.init = function(tipos_preguntas, encuesta, all_users, count_users) {
        // Lista de usuarios disponibles por defecto abierta
        $scope.show_available  = true;
        // Lista de todos los usuarios de la intranet
        $scope.all_users       = all_users;
        $scope.count_users     = count_users;
        $scope.tipos_preguntas = tipos_preguntas;

        // Para editar una encuesta
        if (encuesta != null) {
            $scope.survey = encuesta;
            // Si se va a editar una encuesta entonces se deben seleccionar los usuarios que la encuesta ya tiene invitados
            // poner el atributo selected true ó false si ese usuario no está invitado a la encuesta
            function userExists(item_id) {
                return $scope.survey.users.some(function(el) {
                    return el.item_id === item_id;
                });
            }
            for (var i = 0; i < $scope.all_users.length; i++) {
                $scope.all_users[i].selected = userExists($scope.all_users[i].item_id);
            }


        } else {
            $scope.survey = {
                title       : '',
                instructions: '',
                description : '',
                limit_date  : '',
                is_active   : false,
                is_anonymous: false,
                is_deleted  : false,
                questions   : [],
                pages       : [{}],
                users       : []
            };
        };
    };

    $scope.show_modal = false;
    $scope.openModal = function() {
        $scope.editMode = false;
        resetQuestion();
        $scope.show_modal = true;
    };

    $scope.closeModal = function() {
        $scope.show_modal = false;
    };

    $scope.question = {};
    var resetQuestion = function() {
        $scope.question = {
            question_id : '',
            type        : '',
            title       : '',
            page        : "0",
            is_required   : false,
            is_demographic: false,
            has_another   : false,
            name_another  : '',
            abierta: {
                value: ''
            },
            cerrada: {
                value  : '',
                options: []
            },
            matriz: {
                questions: [],
                options  : []
            },
        };
    };
    resetQuestion();
    // Validaciones para agregar una pregunta a la encuesta
    $scope.addQuestion = function() {
        // Se hacen las validaciones según el tipo de pregunta
        if ($scope.question.type == '') {
            alert('Debe seleccionar un tipo de pregunta.');
            return;
        }
        if ($scope.question.title == '') {
            alert('El campo (pregunta) es requerido.');
            return;
        }
        switch ($scope.question.type) {
            case 'abierta':
                if ($scope.question.abierta.value == '') {
                    alert('Debe seleccionar un tipo de respuesta, (corta ó larga)');
                    return;
                }
                break;
            case 'cerrada':
                if ($scope.question.cerrada.value == '') {
                    alert('Debe seleccionar un tipo de respuesta, (única ó múltiple)');
                    return;
                }
                if ($scope.question.cerrada.options.length == 0) {
                    alert('Debe crear al menos una opción de respuesta.');
                    return;
                } else {
                    for (var i = 0; i < $scope.question.cerrada.options.length; i++) {
                        var option = $scope.question.cerrada.options[i];
                        if (option.value == '') {
                            alert('El campo (opción) no debe estar vacío.');
                            return;
                        }
                    }
                }
                break;
            case 'matriz':
                if ($scope.question.matriz.questions.length == 0) {
                    alert('Debe crear al menos una pregunta.');
                    return;
                } else {
                    for (var i = 0; i < $scope.question.matriz.questions.length; i++) {
                        var option = $scope.question.matriz.questions[i];
                        if (option.value == '') {
                            alert('El campo (pregunta) no debe estar vacío.');
                            return;
                        }
                    }
                }
                if ($scope.question.matriz.options.length == 0) {
                    alert('Debe crear al menos una opción de respuesta.');
                    return;
                } else {
                    for (var i = 0; i < $scope.question.matriz.options.length; i++) {
                        var option = $scope.question.matriz.options[i];
                        if (option.value == '') {
                            alert('El campo (opción) no debe estar vacío.');
                            return;
                        }
                    }
                }
                break;
        }

        // Pasaron todas las validaciones
        if ($scope.editMode == false) {
            // Agregar una pregunta nueva cuando no está en modo de edición
            $scope.survey.questions.push($scope.question);
        }

        $scope.savepreguntaindSurvey($scope.question);
        //$scope.survey.questions.push($scope.question);

        

        resetQuestion();
        $scope.show_modal = false;

    };
    $scope.editMode = false;
    $scope.editQuestion = function(question) {
        $scope.editMode = true;
        resetQuestion();
        $scope.question = question;
        $scope.show_modal = true;        
    };


    $scope.deleteQuestion = function(question) {

        var del = $scope.survey.questions.indexOf(question);
        $scope.survey.questions.splice(del);

        // Guardar la encuesta en el backend
        $http.post("/encuestas/admin/deletequestion", question, {
            header: {
                contentType: "application/json; charset=utf-8"
            }
        }).then(function(response) {
            
            alert('Se elimino la pregunta.');
            //$scope.survey.questions.splice($index, 1);
        }, function(response) {
        
        });
        
    };



    // Validaciones para agregar una pagina a la encuesta
    $scope.agregarPagina = function() {
        $scope.survey.pages.push({});

        var datos = {
            survey_id: $scope.survey.survey_id,
            pages    : $scope.survey.pages.length
        }

        // Guardar la encuesta en el backend
        $http.post("/encuestas/admin/actualizapagina", datos, {
            header: {
                contentType: "application/json; charset=utf-8"
            }
        }).then(function(response) {
        }, function(response) {
        });
    };

    $scope.eliminarPagina = function(index) {
        // Recorrer todas las preguntas que coincidan con ésta página y reasignarlas a la primer página
        for (var i = 0; i < $scope.survey.questions.length; i++) {
            var question = $scope.survey.questions[i];
            if (question.page == index) {
                question.page = 0;
            }
        }
        $scope.survey.pages.splice(index, 1);

        var datos = {
            survey_id: $scope.survey.survey_id,
            pages    : index,
            total    : $scope.survey.pages.length
        }

        // Guardar la encuesta en el backend
        $http.post("/encuestas/admin/eliminapagina", datos, {
            header: {
                contentType: "application/json; charset=utf-8"
            }
        }).then(function(response) {
        }, function(response) {
        });
    };

    //FUNCION QUE GUARDA LOS DATOS GENERALES DE LA ENCUESTA
    $scope.savegeneralSurvey = function() {
        // Guardar la encuesta en el backend
        $http.post("/encuestas/admin/savegeneral", $scope.survey, {
            header: {
                contentType: "application/json; charset=utf-8"
            }
        }).then(function(response) {
            var result = response.data;

            //return;
            if (response.status == 200) {
                $scope.survey.survey_id = result.data;
            } else {
                // TODO
                // Mostrar mensaje de error cuando la encuesta no se pueda guardar en el servidor
                alert(result.message);
            }
            //location.href = "/encuestas/admin/";
        }, function(response) {
            // TODO
            // Mostrar mensaje de error en caso de que la encuesta no se pueda guardar bien en el backend
        });
    };

    //FUNCION QUE GUARDA LAS PREGUNTA DE LA ENCUESTA
    $scope.savepreguntaindSurvey = function( pregunta ) {
        // Guardar la encuesta en el backend
        $http.post("/encuestas/admin/savepreguntaind", [$scope.survey.survey_id, pregunta],  {
            header: {
                contentType: "application/json; charset=utf-8"
            }
        }).then(function(response) {
            var result = response.data;
            //return;
            if (response.status == 200) {
                if (result.status == true) {
                    pregunta.question_id = result.data;
                }
            } else {
            }
        }, function(response) {
        });
    };

    //FUNCION QUE GUARDA LAS PREGUNTAS DE LA ENCUESTA
    $scope.savepreguntasSurvey = function() {
        // Guardar la encuesta en el backend
        $http.post("/encuestas/admin/savepreguntas", $scope.survey, {
            header: {
                contentType: "application/json; charset=utf-8"
            }
        }).then(function(response) {
            var result = response.data;
            //return;
            if (response.status == 200) {
                if (result.status == true) {
                    // TODO
                    // Mostrar mensaje de confirmación cuando se cree la encuesta correctamente
                    // TODO
                    // Mostrar mensaje de confirmación cuando se actualice la encuesta correctamente
                    alert(result.message);
                    $scope.survey.survey_id = result.data;
                } else {
                    // TODO
                    // Mostrar mensaje de error cuando la encuesta no se pueda guardar en el servidor
                    alert(result.message);
                }
            } else {
                // TODO
                // Mostrar mensaje de error cuando la encuesta no se pueda guardar en el servidor
                alert(result.message);
            }
            //location.href = "/encuestas/admin/";
        }, function(response) {
            // TODO
            // Mostrar mensaje de error en caso de que la encuesta no se pueda guardar bien en el backend
        });
    };

    //FUNCION QUE GUARDA LOS USUARIOS INVITADOS DE LA ENCUESTA
    $scope.saveinvitadosSurvey = function() {
       
        var datos = {
            survey_id: $scope.survey.survey_id,
            users    : $scope.survey.users
        }

        // Guardar la encuesta en el backend
        $http.post("/encuestas/admin/saveinvitados", datos, {
            header: {
                contentType: "application/json; charset=utf-8"
            }
        }).then(function(response) {
            var result = response.data;
        }, function(response) {
        });
    };

    $scope.isReserved = function(value) {
        return $scope.reserved.indexOf(value) > -1;
    };

    //FUNCION QUE ENVIA LA ENCUESTA A LOS USUARIOS INVITADOS
    $scope.sendSurvey = function() {
        alert("Se esta enviando la encuesta");
        document.getElementById('save_survey').setAttribute('disabled', 'disabled');

        var survey_id;
        // Guardar la encuesta en el backend
        $http.post("/encuestas/admin/send", $scope.survey, {
            header: {
                contentType: "application/json; charset=utf-8"
            }
        }).then(function(response) {
            var result = response.data;
            //return;
            if (response.status == 200) {
                if (result.status == true) {
                    // TODO
                    // Mostrar mensaje de confirmación cuando se cree la encuesta correctamente
                    // TODO
                    // Mostrar mensaje de confirmación cuando se actualice la encuesta correctamente
                    //alert(" Algo 1: "+result.message);
                } else {
                    // TODO
                    // Mostrar mensaje de error cuando la encuesta no se pueda guardar en el servidor
                    //alert(" Algo 2: "+result.message);
                }
            } else {
                // TODO
                // Mostrar mensaje de error cuando la encuesta no se pueda guardar en el servidor
                //alert(" Algo 3: "+result.message);
            }
            location.href = "/encuestas/admin/";
        }, function(response) {
            //alert(response);
            // TODO
            // Mostrar mensaje de error en caso de que la encuesta no se pueda guardar bien en el backend
        });

        document.getElementById('save_survey').removeAttribute('disabled');
    };


    

    $scope.AllUsers_selectAll = false;
    $scope.selectAllUsers = function() {
            $scope.AllUsers_selectAll = !$scope.AllUsers_selectAll;
            for (var i = 0; i < $scope.all_users.length; i++) {
                $scope.all_users[i].selected = $scope.AllUsers_selectAll;
            }
            // Agregar a la encuesta los usuarios seleccionados
            $scope.addSelectedUsers();
        }
        // Paginación tabla de todos los usuarios
    $scope.paginationAllUsers = {
        itemsPerPage: 10,
        currentPage: 1,
        q: {}
    };
    // Paginación tabla de usuarios invitados
    $scope.paginationInvitedUsers = {
        itemsPerPage: 10,
        currentPage: 1,
        q: {
            selected: true
        }
    };
    // Validaciones del Wizard
    $scope.wizardForm = {
        stepOne: {
            title: true,
            instructions: true,
            description: true,
            limit_date: true
        }
    };
    $scope.wizardValidations = {
            validateStepOne: function() {
                $scope.wizardForm.stepOne.title        = !($scope.survey.title == '');
                $scope.wizardForm.stepOne.limit_date   = ! ($scope.survey.limit_date == '');
                

                if (!$scope.wizardForm.stepOne.title) return false;
                /*
                
                $scope.wizardForm.stepOne.description  = ! ($scope.survey.description == '');
                $scope.wizardForm.stepOne.instructions = ! ($scope.survey.instructions == '');

                if ( ! $scope.wizardForm.stepOne.instructions )
                    return false;
                
                if ( ! $scope.wizardForm.stepOne.description )
                    return false;*/
                
                if ( !$scope.wizardForm.stepOne.limit_date) return false;

                return true;
            },
            validateStepTwo: function() {
                if ( $scope.survey.questions.length == 0 ) {
                    alert('Para continuar, debes crear al menos una pregunta en una página.');
                    return false;
                };
                return true;
            },
            validateStepThree: function() {
                if ( $scope.survey.users.length == 0 ){
                    alert('Haz clic en la casilla de verificación para seleccionar o deseleccionar todos los usuarios. También puedes buscarlos individualmente por su nombre de usuario o correo electrónico.');
                    return false;
                }
                return true;
            },
            validateStepFour: function() {
                return true;
            },
            validateStepFive: function() {
                return true;
            },
        }
        // FUNCIONES
    $scope.addSelectedUsers = function() {
            $scope.survey.users = [];
            for (var i = 0; i < $scope.all_users.length; i++) {
                if ($scope.all_users[i].selected) {
                    $scope.survey.users.push($scope.all_users[i]);
                }
            }
        }
        // FUNCIONES

    $scope.wizardPage = 1;
    $scope.wizardNext = function(step){
      switch(step){
        case 1:
          if ($scope.wizardValidations.validateStepOne() ){
            $scope.savegeneralSurvey();
            $scope.wizardPage = 2;
          }
        break;
        case 2:
          if ($scope.wizardValidations.validateStepTwo() ){
            $scope.wizardPage = 3;
          }
        break;
        case 3:
          if ($scope.wizardValidations.validateStepThree() ){
            $scope.saveinvitadosSurvey();
            $scope.wizardPage = 4;
          }
        break;
        case 4:
          if ($scope.wizardValidations.validateStepFour() ){
            $scope.wizardPage  = 5;
          }
        break;
        case 5:
          if ($scope.wizardValidations.validateStepFive() ){
            $scope.wizardPage = 6;
          }
        break;
      }
    }
    $scope.wizardGoto = function(page){
      $scope.wizardPage = page;
    }

    /*
        $scope.saveSurvey = function() {
            // Guardar la encuesta en el backend
            $http.post("/encuestas/admin/save", $scope.survey, {
                header: {
                    contentType: "application/json; charset=utf-8"
                }
            }).then(function(response) {
                var result = response.data;
                //return;
                if (response.status == 200) {
                    if (result.status == true) {
                        // TODO
                        // Mostrar mensaje de confirmación cuando se cree la encuesta correctamente
                        // TODO
                        // Mostrar mensaje de confirmación cuando se actualice la encuesta correctamente
                        alert(result.message);
                        $scope.survey.survey_id = result.data;
                    } else {
                        // TODO
                        // Mostrar mensaje de error cuando la encuesta no se pueda guardar en el servidor
                        alert(result.message);
                    }
                } else {
                    // TODO
                    // Mostrar mensaje de error cuando la encuesta no se pueda guardar en el servidor
                    alert(result.message);
                }
                location.href = "/encuestas/admin/";
            }, function(response) {
                // TODO
                // Mostrar mensaje de error en caso de que la encuesta no se pueda guardar bien en el backend
            });
        };

        $scope.saveSendSurvey = function() {
            var survey_id;
            // Guardar la encuesta en el backend
            $http.post("/encuestas/admin/savesend", $scope.survey, {
                header: {
                    contentType: "application/json; charset=utf-8"
                }
            }).then(function(response) {
                var result = response.data;
                //return;
                if (response.status == 200) {
                    if (result.status == true) {
                        // TODO
                        // Mostrar mensaje de confirmación cuando se cree la encuesta correctamente
                        // TODO
                        // Mostrar mensaje de confirmación cuando se actualice la encuesta correctamente
                        alert(result.message);
                        $scope.survey.survey_id = result.data;
                        survey_id = result.data;
                    } else {
                        // TODO
                        // Mostrar mensaje de error cuando la encuesta no se pueda guardar en el servidor
                        alert(result.message);
                    }
                } else {
                    // TODO
                    // Mostrar mensaje de error cuando la encuesta no se pueda guardar en el servidor
                    alert(result.message);
                }
                location.href = "/encuestas/admin/";
            }, function(response) {
                // TODO
                // Mostrar mensaje de error en caso de que la encuesta no se pueda guardar bien en el backend
            });
        };

        $scope.saveDuplicateSurvey = function() {
            // Guardar la encuesta en el backend
            $http.post("/encuestas/admin/saveduplicate", $scope.survey, {
                header: {
                    contentType: "application/json; charset=utf-8"
                }
            }).then(function(response) {
                var result = response.data;
                //return;
                if (response.status == 200) {
                    if (result.status == true) {
                        // TODO
                        // Mostrar mensaje de confirmación cuando se cree la encuesta correctamente
                        // TODO
                        // Mostrar mensaje de confirmación cuando se actualice la encuesta correctamente
                        alert(result.message);
                        $scope.survey.survey_id = result.data;
                    } else {
                        // TODO
                        // Mostrar mensaje de error cuando la encuesta no se pueda guardar en el servidor
                        alert(result.message);
                    }
                } else {
                    // TODO
                    // Mostrar mensaje de error cuando la encuesta no se pueda guardar en el servidor
                    alert(result.message);
                }
                location.href = "/encuestas/admin/";
            }, function(response) {
                // TODO
                // Mostrar mensaje de error en caso de que la encuesta no se pueda guardar bien en el backend
            });
        };
    */
});