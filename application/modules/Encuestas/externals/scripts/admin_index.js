jQuery(document).ready(function() {
    jQuery(".datepicker").datepicker({
            changeYear : true,
            minDate    : "+1D"
        });
});

app.controller("admin_index_ctrl", function($scope, $http) {

    /** ========== VARIABLES ========== */

    $scope.encuestas = [];

    // Paginación tabla de encuestas
    $scope.paginationSurveys = {
        itemsPerPage: 10,
        currentPage: 1,
        q: ''
    };

    $scope.encuesta_selected = {};

    /** ========== PRIVATE FUNCTIONS ========== */
    //Oculta y/o muestra un elemento jQuery
    function toggle_element ( tagElement ){
        if( jQuery( tagElement ).hasClass("hidden") )
            jQuery( tagElement ).removeClass( "hidden" );
        else
            jQuery( tagElement ).addClass( "hidden" );
    }

    /**
     * @param {object DOM} encuesta 
     * @param {number} $index
     * @description Lanza la modal de confirmacion de eliminar encuesta
     */
    function delete_encuesta( encuesta, $index ){

        var text = "¿Desea eliminar la encuesta: <i>"+encuesta.title+"</i>?";

        //Inicialmente el html esta oculto, si lo esta, entonces muestre la modal
        if( jQuery( "#delete-confirm" ).hasClass("hidden") )
            jQuery( "#delete-confirm" ).removeClass("hidden");

        //Selecciona el div para mostrar mensaje dentro de la modal
        jQuery("#delete-confirm #confirm_text").html(text);

        //Utiliza ui jquery modal
        jQuery( "#delete-confirm" ).dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            dialogClass: "no-close",
            buttons: [
              {
                text: "Borrar",
                class: "btn_delete", //refencia para resetear los estilos del boton de borrar de la modal
                click: function(){
                    $scope.encuesta_selected = encuesta;

                    //Se pasa la modal y la posicion del elemento
                    confirm_delete_encuesta( this, $index );
                }
              },
              {
                text: "Cancelar",
                class:"btn-transparent",
                click: function() {
                    jQuery( this ).dialog( "close" );
                  }
              },
              
            ],
            open: function() {                         
                //Oculta boton de cerrar de la modal
                $(this).parent().find(".ui-dialog-titlebar-close").hide();                       
                //Cambia el color de la cabecera de la modal
                $(this).parent().find(".ui-dialog-titlebar").css({'background': '#ab1607'});
                //Cambia el color del boton de borrar de la modal
                $(this).parent().find(".btn_delete").css({'background': '#ab1607'});
            }
          });
    }

    /**
     * @param {object DOM} modal 
     * @param {number} $index 
     * @description Callback despues de confirmar el eliminado de la encuesta
     */
    function confirm_delete_encuesta( modal, $index ){
        var encuesta = $scope.encuesta_selected;

        if( encuesta ){
            //Elemento indicador de carga
            toggle_element( ".loading_confirm" );
    
            //elimina las clases de la caja de mensajes de la modal
            if( jQuery( ".message_confirm" ).hasClass("alert-success") || jQuery( ".message_confirm" ).hasClass("alert-danger") )
                jQuery( ".message_confirm" ).removeClass("alert-success").removeClass("alert-danger")

            var obj = {
                survey_id: encuesta.survey_id
            }
            
            $http.post("/encuestas/admin/removencuesta", obj).then(function( rs ){
    
                if( rs.data ){
                    //Ocultael indicador de carga
                    toggle_element( ".loading_confirm" );
                    toggle_element( ".message_confirm" );
        
                    if( rs.data.status ){
        
                        jQuery( ".message_confirm" ).html(rs.data.message).addClass( "alert-success" );

                        //Elimina la encuesta del arreglo de encuestas. Nota: Al retornar el listado de encuestas, no permite re dibujar el listado porque 
                        //el framework preconfigura el datatable desde el servidor
                        $scope.encuestas.splice($index, 1);
                        jQuery( "#row_encuesta_"+$index ).remove();
                                    
                        setTimeout(() => {
                            jQuery( modal ).dialog( "close" );
                            jQuery( ".message_confirm" ).html("").addClass( "hidden" );
                        }, 1000);
                    }else{
                        jQuery( ".message_confirm" ).html(rs.data.message).addClass( "alert-danger" );
                    }
                }
    
            }, function( er ){
                //Oculta indicador de carga
                if( er.data ){
                    toggle_element( ".loading_confirm" );
                    toggle_element( ".message_confirm" );
                    jQuery( ".message_confirm" ).html(rs.data.message).addClass( "alert-danger" );
                }
            });
        }
                
    }

    /**
     * ========== PUBLIC FUNCTIONS ==========
     */
    
    $scope.init = function(encuestas) {
        $scope.encuestas = encuestas;
    };

    // Activar o inactivar una encuesta
    $scope.activar_encuesta = function(encuesta) {
        var datos = {
            survey_id: encuesta.survey_id,
            is_active: ( ! encuesta.is_active )
        }
        $http.post("/encuestas/admin/setactive", datos, {
                header: {
                    contentType: "application/json; charset=utf-8"
                }
            })
            .then(function(response) {
                var result = response.data;
                if (response.status == 200) {
                    if ( result.status == true ){
                        // TODO
                        // Mostrar mensaje de confirmación cuando se active la encuesta correctamente
                        // TODO
                        // Mostrar mensaje de confirmación cuando se inactive la encuesta correctamente
                        alert(result.message);
                        // Actualizar el valor del campo is_active en el front
                        encuesta.is_active = result.data;
                    }else{
                        // TODO
                        // Mostrar mensaje de error cuando no se pueda actualizar la encuesta en el servidor
                        alert(result.message);
                    }

                } else {
                    // TODO
                    // Mostrar mensaje de error cuando no se pueda actualizar la encuesta en el servidor
                    alert(result.message);
                }
            }, function(response) {
                // TODO
                // Mostrar mensaje de error en caso de que la encuesta no se pueda actualizar bien en el backend
            });
    };

    // Enviar una encuesta a los usuarios invitados a responderla
    $scope.enviar_encuesta = function(encuesta) {
        if ( encuesta.is_sent == 1 ){
            return false;
        }
        
        var datos = { survey_id: encuesta.survey_id }
        $http.post("/encuestas/admin/sendsurvey", datos, {
                header: {
                    contentType: "application/json; charset=utf-8"
                }
            })
            .then(function(response) {
                var result = response.data;
                if (response.status == 200) {
                    if ( result.status == true ){
                        // TODO
                        // Mostrar mensaje de confirmación cuando se active la encuesta correctamente
                        // TODO
                        // Mostrar mensaje de confirmación cuando se inactive la encuesta correctamente
                        //alert(result.message);
                        // Actualizar el valor del campo is_active en el front
                        encuesta.is_sent = result.data;
                    }else{
                        // TODO
                        // Mostrar mensaje de error cuando no se pueda actualizar la encuesta en el servidor
                        //alert(result.message);
                    }

                } else {
                    // TODO
                    // Mostrar mensaje de error cuando no se pueda actualizar la encuesta en el servidor
                    //alert(result.message);
                }
            }, function(response) {
                // TODO
                // Mostrar mensaje de error en caso de que la encuesta no se pueda actualizar bien en el backend
            });
    };

    //Muestra el datapicker de fecha limite - es llamado en el boton con imagen de lapiz
    $scope.show_data_picker = function( index ){
        var tagElement = ".wrp_picker_"+index;
        toggle_element( tagElement );
    }
    //Oculta el datapicker de fecha limite 
    $scope.cancel_data_picker = function( index ){
        var tagElement = ".wrp_picker_"+index;
        toggle_element( tagElement );       
    }

    $scope.save_data_picker = function( index, survey_id ){
        
        if( survey_id && !jQuery( ".btn_save_"+index ).data("sending") ){
            //Construye etiqueta
            var tagElement = ".picker_"+index;
            //Captura el datapicker
            var str_date = jQuery( tagElement ).datepicker( "getDate" );
            
            
            var month_of_date = ( (str_date.getMonth()+1) < 10 )? "0"+String((str_date.getMonth()+1)) : String((str_date.getMonth()+1)) ;
            
            var day_of_date = ( (str_date.getDate()) < 10 )? "0"+String(str_date.getDate()) : String((str_date.getDate())) ;
            
            var date = String(str_date.getFullYear()) + "-" + month_of_date + "-" + day_of_date;
            
            //Si el boton tiene el atributo sendig
            jQuery( ".btn_save_"+index ).data("sending", true);
            //Muestra el indicador de carga
            toggle_element( ".wrp_loading_picker_"+index );
            
            var obj = {
                "survey_id": survey_id,
                "field": "limit_date",
                "value": date
            }

            if( jQuery( ".wrp_message_picker_"+index ).hasClass("alert-success") || jQuery( ".wrp_message_picker_"+index ).hasClass("alert-danger") )
                jQuery( ".wrp_message_picker_"+index ).removeClass("alert-success").removeClass("alert-danger")

            $http.post("/encuestas/admin/updatefield", obj).then(function( rs ){

                //Ocultael indicador de carga
                toggle_element( ".wrp_loading_picker_"+index );
                jQuery( ".btn_save_"+index ).data("sending", false);

                if( rs.data ){
                    
                    //Escribe la fecha guardada en el html
                    jQuery( ".label_limit_date_"+index ).html( date );

                    //Caja de mensajes
                    jQuery( ".wrp_message_picker_"+index ).html(rs.data.message).addClass( "alert-success" );
                    toggle_element( ".wrp_message_picker_"+index );
                    
                    setTimeout(() => {
                        toggle_element( ".wrp_message_picker_"+index );
                    }, 5000);
                }

            }, function( er ){
                //Oculta indicador de carga
                toggle_element( ".wrp_loading_picker_"+index );
                
                if( er.data ){
                    toggle_element( ".wrp_message_picker_"+index );
                    jQuery( ".wrp_message_picker_"+index ).html(rs.data.message).addClass( "alert-danger" );
                }
            });

        }

    }

    $scope.delete_encuesta = delete_encuesta;

});
