app.controller("my_survey_answer_ctrl", function($scope, $http, WizardHandler) {

    $scope.init = function(miencuesta) {
        $scope.my_survey = miencuesta;
    };

    $scope.my_survey = [];

    // Guardar las respuestas de esta encuesta
    $scope.saveSurveyAnswers = function() {
        // Guardar las respuestas para esta encuesta en el backend
        $http.post("/encuestas/index/saveanswers", $scope.my_survey, { header: { contentType: "application/json; charset=utf-8" } })
            .then(function(response) {
                var result = response.data;

                if ( response.status == 200 ){

                    if ( result.status == true ){
                        // TODO
                        // Mostrar mensaje de confirmación cuando se responda total o parcialmente la encuesta correctamente
                        alert(result.message);
                    }else{
                        // TODO
                        // Mostrar mensaje de error cuando las respuestas de la encuesta no se puedan guardar en el servidor
                        alert(result.message);
                    }

                }else{
                    // TODO
                    // Mostrar mensaje de error cuando las respuestas de la encuesta no se puedan guardar en el servidor
                    alert(result.message);
                }
            },function(response){

                // TODO
                // Mostrar mensaje de error en caso de que las respuestas para esta encuesta no se pueda guardar bien en el backend
            });
    };


    // Guardar las respuestas de esta encuesta
    $scope.saveTempSurveyAnswers = function() {
        // Guardar las respuestas para esta encuesta en el backend
        $http.post("/encuestas/index/saveanswers", $scope.my_survey, { header: { contentType: "application/json; charset=utf-8" } })
            .then(function(response) {
                var result = response.data;

                if ( response.status == 200 ){

                    if ( result.status == true ){
                        // TODO
                        // Mostrar mensaje de confirmación cuando se responda total o parcialmente la encuesta correctamente
                        alert(result.message);
                    }else{
                        // TODO
                        // Mostrar mensaje de error cuando las respuestas de la encuesta no se puedan guardar en el servidor
                        alert(result.message);
                    }

                }else{
                    // TODO
                    // Mostrar mensaje de error cuando las respuestas de la encuesta no se puedan guardar en el servidor
                    alert(result.message);
                }
            },function(response){

                // TODO
                // Mostrar mensaje de error en caso de que las respuestas para esta encuesta no se pueda guardar bien en el backend
            });
    };

    // Seleccionar opción cerrada multiple
    $scope.selectQuestionCerradaMultiple = function(question, questioncerrada_id) {
        var idx = question.answer.indexOf(questioncerrada_id);
        if (idx > -1) { // is currently selected
            question.answer.splice(idx, 1);
        } else { // is newly selected
            question.answer.push(questioncerrada_id);
        }
    };

    // Seleccionar opción de una matriz
    $scope.selectQuestionMatriz = function(question, matrizquestion_id, matrizoption_id) {
        question.answer[matrizquestion_id] = {
            answ_mtr_question_id: matrizquestion_id,
            answ_mtr_option_id: matrizoption_id
        };
    };

    // Validar si puede pasar a la siguiente página de la encuesta
    $scope.validarPreguntasRequeridas = function(pagina){
        //var pagina = WizardHandler.wizard().currentStepNumber() - 1;
        return validarPaso(pagina);
    };

    var validarPaso = function(pagina){
        for ( var i = 0; i < $scope.my_survey.questions.length; i++ ){
            var question = $scope.my_survey.questions[i]; 

            if ( question.page <= pagina ){ 
                // Validaciones por tipo de pregunta Texto.
                if ( question.type == 'abierta' && question.is_required && ( question.answer == null || question.answer == '' || question.answer.length == 0 ) ){
                    alert("\n La pregunta: (" + question.title + "), es requerida.");
                    return false;
                }

                
                // Validaciones generales de las respuestas requeridas
                if (question.type == 'cerrada' && question.is_required && (question.answer == null || question.answer == '' || question.answer.length == 0 ) ){
                        
                    if (question.has_another == 1 && (question.another_answer == null || question.another_answer == '' || question.another_answer.length == 0 ) ) {
                        alert("\n La pregunta: (" + question.title + "), es requerida.");
                        return false;
                    }else if(question.has_another == 0){
                        alert("\n La pregunta: (" + question.title + "), es requerida.");
                        return false;
                    }

                }

                // Validaciones por tipo de pregunta Matriz.
                if ( question.type == 'matriz' && question.is_required){
                    var size = Object.keys(question.answer).length; // Tamaño de la lista de preguntas respondidas
                    if ( size != question.matriz.questions.length   ) { // Tamaño de la lista de preguntas
                        var laPage = question.page + 1;
                        alert("\n Debe responder todas las opciones de la pregunta: (" + question.title + "), ya que es requerida.");
                        return false;
                    }
                }

            }
        }
        return true;
    };

    //Ir a la siguiente pagina
    $scope.nextPage = function(){
        var pasoAct = WizardHandler.wizard().currentStepNumber();
        var pasoNex = pasoAct;
        var pasoAct = pasoAct - 1;

        if ( validarPaso(pasoAct) ){ 
           // Guardar las respuestas para esta encuesta en el backend
            $http.post("/encuestas/index/saveanswers", $scope.my_survey, { header: { contentType: "application/json; charset=utf-8" } })
                .then(function(response) {
                    var result = response.data;

                    if ( response.status == 200 ){

                        if ( result.status == true ){
                            // TODO
                            // Mostrar mensaje de confirmación cuando se responda total o parcialmente la encuesta correctamente
                            //alert(result.message);
                        }else{
                            // TODO
                            // Mostrar mensaje de error cuando las respuestas de la encuesta no se puedan guardar en el servidor
                            //alert(result.message);
                        }

                    }else{
                        // TODO
                        // Mostrar mensaje de error cuando las respuestas de la encuesta no se puedan guardar en el servidor
                        //alert(result.message);
                    }
                },function(response){

                    // TODO
                    // Mostrar mensaje de error en caso de que las respuestas para esta encuesta no se pueda guardar bien en el backend
                });
            
            //Ir a la siguiente pagina
            WizardHandler.wizard().goTo(pasoNex);
        }

    };


    // Ultima validación para finalizar encuesta
    $scope.FinalizarEncuesta = function(){
        var pasoAct = WizardHandler.wizard().currentStepNumber();
        var pasoAct = pasoAct - 1;

        if (validarPaso(pasoAct)){         
            $scope.saveSurveyAnswers();
            finalizarEncuesta();                      
        }        
    };

    var finalizarEncuesta = function(){

        var data = {
            survey_id: $scope.my_survey.survey_id
        };

        //Finalizando la encuesta
        $http.post("/encuestas/index/finalizesurvey", data, { header: { contentType: "application/json; charset=utf-8" } })
            .then(function(response) {
                var result = response.data;
                if ( response.status == 200 ){

                    if ( result.status == true ){
                        alert(result.message);
                    }else{
                        alert(result.message);
                    }

                }else{
                    alert(result.message);
                }
                
                location.href  = "/encuestas/index/";
                
            },function(response){
            });
    }

});
