app.controller("admin_reporteind_ctrl", function($scope, $http) {

    $scope.init = function(users, encuesta) {
        $scope.users    = users;
        $scope.encuesta = encuesta;        
    };

    // Paginación tabla de encuestas
    $scope.paginationUsers = {
        itemsPerPage: 10,
        currentPage: 1,
        q: ''
    };

});

app.controller("admin_reporteinduser_ctrl", function($scope, $http) {
    $scope.init = function(my_survey) {
        $scope.my_survey = my_survey;
    };
});

app.controller("admin_reporte_ctrl", function($scope, $http) {
    $scope.init = function(surveys) {
        $scope.surveys = surveys;
    };

    $scope.reporte = {
        survey_id : '',
        fecha_ini : '',
        fecha_fin : ''
    };

    // Validar si puede pasar a la siguiente página de la encuesta
    $scope.validarFormulario = function(){
        
        if ($scope.reporte.survey_id == "") {
            alert("Seleccione la encuesta.");
        }else if($scope.reporte.fecha_ini.length == 0) {
            alert("Seleccione la fecha de inicio del reporte.");
        }else if($scope.reporte.fecha_fin.length == 0) {
            alert("Seleccione la fecha de finalizacion del reporte.");
        }else{
            var datos = $scope.reporte.survey_id + "/" + $scope.reporte.fecha_ini + "/" + $scope.reporte.fecha_fin;
            location.href = "/encuestas/admin/genreporte/"+ datos;
        }

    };


    // Validar si puede pasar a la siguiente página de la encuesta
    $scope.validarFormularioConsolidado = function(){
        
        if ($scope.reporte.survey_id == "") {
            alert("Seleccione la encuesta.");
        }else if($scope.reporte.fecha_ini.length == 0) {
            alert("Seleccione la fecha de inicio del reporte.");
        }else if($scope.reporte.fecha_fin.length == 0) {
            alert("Seleccione la fecha de finalizacion del reporte.");
        }else{
            var datos = $scope.reporte.survey_id + "/" + $scope.reporte.fecha_ini + "/" + $scope.reporte.fecha_fin;
            location.href = "/encuestas/admin/genreporteconsolidado/"+ datos;
        }

    };


    // Validar si puede pasar a la siguiente página de la encuesta
    $scope.validarFormularioDemografico = function(){
        
        if ($scope.reporte.survey_id == "") {
            alert("Seleccione la encuesta.");
        }else if($scope.reporte.fecha_ini.length == 0) {
            alert("Seleccione la fecha de inicio del reporte.");
        }else if($scope.reporte.fecha_fin.length == 0) {
            alert("Seleccione la fecha de finalizacion del reporte.");
        }else{
            var datos = $scope.reporte.survey_id + "/" + $scope.reporte.fecha_ini + "/" + $scope.reporte.fecha_fin;
            location.href = "/encuestas/admin/genreportedemografico/"+ datos;
        }

    };

});

app.controller("admin_vistaprevia_ctrl", function($scope, $http) {
    $scope.init = function(survey) {
        $scope.survey = survey;
    };
});
