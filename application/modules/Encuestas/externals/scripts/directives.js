// Directiva para actualizar el campo ( fecha límite de la encuesta ) integración con jQuery UI
app.directive('jqdatepicker', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            element.datepicker({
                minDate: 1,
                defaultDate: +1,
                dateFormat: 'yy-mm-dd',
                onSelect: function(date) {
                    scope.survey.limit_date = date;
                    scope.$apply();
                }
            });
        }
    };
});


// Directiva para actualizar el campo ( fecha inicio del reporte de la encuesta ) integración con jQuery UI
app.directive('jqdatepicker2', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            element.datepicker({
                defaultDate: +1,
                dateFormat: 'yy-mm-dd',
                onSelect: function(date) {
                    scope.reporte.fecha_ini = date;
                    scope.$apply();
                }
            });
        }
    };
});

// Directiva para actualizar el campo ( fecha fin del reporte de la encuesta ) integración con jQuery UI
app.directive('jqdatepicker3', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            element.datepicker({
                defaultDate: +1,
                dateFormat: 'yy-mm-dd',
                onSelect: function(date) {
                    scope.reporte.fecha_fin = date;
                    scope.$apply();
                }
            });
        }
    };
});

app.directive('ngConfirmClick', [
    function() {
        return {
            priority: -1,
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('click', function(e) {
                    var message = attrs.ngConfirmClick;
                    // confirm() requires jQuery
                    if (message && !confirm(message)) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                    }
                });
            }
        }
    }
]);

app.directive('fileReader', function() {
    return {
        scope: {
            fileReader: "="
        },
        link: function(scope, element) {
            element.on('change', function(changeEvent) {
                var files = changeEvent.target.files;
                if (files.length) {
                    var r = new FileReader();
                    r.onload = function(e) {
                        var contents = e.target.result;
                        scope.$apply(function() {
                            scope.fileReader = contents;
                        });
                    };

                    r.readAsText(files[0]);
                }
            });
        }
    };
});
