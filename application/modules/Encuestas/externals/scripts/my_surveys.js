app.controller("my_surveys_ctrl", function($scope, $http) {

    $scope.init = function(misencuestas) {
        $scope.my_surveys = misencuestas;
        $scope.date      = new Date();
    };

    $scope.my_surveys = [];

    // Paginación tabla de mis encuestas
    $scope.paginationMySurveys = {
        itemsPerPage: 10,
        currentPage: 1,
        q: ''
    };

});
