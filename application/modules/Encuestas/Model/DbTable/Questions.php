<?php

class Encuestas_Model_DbTable_Questions extends Engine_Db_Table {

    public function getTotalQuestionsBySurveyId($survey_id)
    {
        $select = $this->select();
        $select->from($this, array('count(*) as count'));
        $select->where('survey_id = ?', $survey_id);
        $rows = $this->fetchAll($select);
        return ($rows[0]->count);
    }

}

?>
