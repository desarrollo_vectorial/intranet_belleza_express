<?php

class Encuestas_Api_Core extends Core_Api_Abstract{
  public $tipos_preguntas = array(
    array(
      'name'  => 'Abierta',
      'title' => 'abierta',
      'types' => array('text', 'textarea'),
    ),
    array(
      'name'  => 'Cerrada',
      'title' => 'cerrada',
      'types' => array(
        array(
          'name'  => 'Respuesta única',
          'title' => 'unica',
          'types' => array('radio', 'text'),
        ),
        array(
          'name'  => 'Respuesta múltiple',
          'title' => 'multiple',
          'types' => array('checkbox', 'text'),
        ),
      ),
    ),
    array(
      'name'  => 'Matriz',
      'title' => 'matriz',
      'types' => array('radio'),
    ),
  );

  public function getTiposPreguntas(){
    return $this->tipos_preguntas;
  }

  public function getRequest(){
    return json_decode(file_get_contents('php://input'), true);
  }

  public function sendSuccessResponse($message, $data = null){
    echo json_encode(array('status' => true, 'message' => $message, 'data' => $data));
  }

  public function sendErrorResponse($message, $data = null){
    echo json_encode(array('status' => false, 'message' => $message, 'data' => $data));
  }

  public function getViewer(){
    return Engine_Api::_()->user()->getViewer();
  }

  public function formatFecha($fecha){
    $meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
    $date  = new DateTime($fecha);
    return $date->format('d') . ' de ' . $meses[$date->format('n') - 1] . ' de ' . $date->format('Y');
  }

  public function formatMoney($valor){
    return '$ ' . number_format($valor, 0, ',', '.');
  }

  public function formatNumber($valor){
    return number_format($valor, 0, ',', '.');
  }

  public function getTableEncuestas(){
    return Engine_Api::_()->getDbtable('surveys', 'encuestas');
  }

  public function getTableQuestions(){
    return Engine_Api::_()->getDbtable('questions', 'encuestas');
  }

  public function getTableMatrizquestions(){
    return Engine_Api::_()->getDbtable('matrizquestions', 'encuestas');
  }

  public function getTableMatrizoptions(){
    return Engine_Api::_()->getDbtable('matrizoptions', 'encuestas');
  }

  public function getTableQuestioncerrada(){
    return Engine_Api::_()->getDbtable('questioncerradas', 'encuestas');
  }

  public function getTableUsers(){
    return Engine_Api::_()->getDbtable('users', 'encuestas');
  }

  public function getTableAnswers(){
    return Engine_Api::_()->getDbtable('answers', 'encuestas');
  }

  /*
   * Obtiene una encuesta completa con todas sus preguntas y opciones de respuesta,
   * para después ser convertida en un objeto JSON y ser pasada al controlador de angular.js en el frontend
   */
  public function getFullEncuesta($survey_id){
    $tableEncuestas       = $this->getTableEncuestas();
    $tablePreguntas       = $this->getTableQuestions();
    $tableUsuarios        = $this->getTableUsers();
    $tableQuestioncerrada = $this->getTableQuestioncerrada();
    $tableMatrizQuestions = $this->getTableMatrizquestions();
    $tableMatrizOptions   = $this->getTableMatrizoptions();

    $encuesta  = $tableEncuestas->findRow($survey_id);
    $preguntas = $tablePreguntas->fetchAll($tablePreguntas->select()->where('survey_id = ?', $encuesta['survey_id'])->order("page ASC"));
    $usuarios  = $tableUsuarios->fetchAll($tableUsuarios->select()->where('survey_id = ?', $encuesta['survey_id']));

    $respObj = array(
      'survey_id'    => $encuesta['survey_id'],
      'title'        => $encuesta['title'],
      'instructions' => $encuesta['instructions'],
      'description'  => $encuesta['description'],
      'limit_date'   => $encuesta['limit_date'],
      'pages'        => array(),
      'is_active'    => $encuesta['is_active'],
      'is_sent'      => $encuesta['is_sent'],
      'is_anonymous' => $encuesta['is_anonymous'],
      'is_deleted'   => $encuesta['is_deleted'],
      'users'        => array(),
      'questions'    => array(),
    );

    // PREGUNTAS
    foreach ($preguntas as $key => $pregunta) {
      $question = array(
        'question_id'    => $pregunta['question_id'],
        'survey_id'      => $pregunta['survey_id'],
        'type'           => $pregunta['type'],
        //'type_value' => $pregunta['type_value'],
        'title'          => $pregunta['title'],
        'page'           => $pregunta['page'],
        'answer'         => '',
        'another_answer' => '',
        'is_required'    => $pregunta['is_required'],
        'is_demographic' => $pregunta['is_demographic'],
        'has_another'    => $pregunta['has_another'],
        'name_another'   => $pregunta['name_another'],
        'abierta'        => array('value' => ''),
        'cerrada'        => array('value' => ''),
        'matriz'         => array('questions' => array(), 'options' => array()),
      );

      switch ($pregunta['type']) {
        case 'abierta':
          $question['abierta']['value'] = $pregunta['type_value'];
          break;
        case 'cerrada':
          $question['cerrada']['value'] = $pregunta['type_value'];

          if ($pregunta['type_value'] == 'multiple') {
            $question['answer'] = array();
          }

          $qst_cerrada_options = $tableQuestioncerrada->fetchAll($tableQuestioncerrada->select()->where('question_id = ?', $pregunta['question_id']));
          foreach ($qst_cerrada_options as $key => $qst_cerrada_option) {
            $question['cerrada']['options'][] = array(
              'questioncerrada_id' => $qst_cerrada_option['questioncerrada_id'],
              'question_id'        => $qst_cerrada_option['question_id'],
              'value'              => $qst_cerrada_option['value'],
            );
          }
          break;
        case 'matriz':

          $question['answer'] = array();

          $mtr_questions = $tableMatrizQuestions->fetchAll($tableMatrizQuestions->select()->where('question_id = ?', $pregunta['question_id']));
          foreach ($mtr_questions as $key => $mtr_question) {
            $question['matriz']['questions'][] = array(
              'matrizquestion_id' => $mtr_question['matrizquestion_id'],
              'question_id'       => $mtr_question['question_id'],
              'value'             => $mtr_question['value'],
            );
          }
          $mtr_options = $tableMatrizOptions->fetchAll($tableMatrizOptions->select()->where('question_id = ?', $pregunta['question_id']));
          foreach ($mtr_options as $key => $mtr_option) {
            $question['matriz']['options'][] = array(
              'matrizoption_id' => $mtr_option['matrizoption_id'],
              'question_id'     => $mtr_option['question_id'],
              'value'           => $mtr_option['value'],
            );
          }
          break;
      }

      $respObj['questions'][] = $question;
    }

    // USUARIOS
    $tableUsers = Engine_Api::_()->getDbtable('users', 'user');
    foreach ($usuarios as $key => $usuario) {
      $select             = $tableUsers->select()->where('user_id = ?', $usuario['item_id']);
      $user               = $tableUsers->fetchRow($select);
      $respObj['users'][] = array(

        'user_id'     => $usuario['user_id'],
        'item_id'     => $usuario['item_id'],
        'displayname' => $user['displayname'],
        'email'       => $user['email'],
        'sede'        => $user['email'],
        'area'        => $user['email'],
        'selected'    => true,
      );
    }

    // PÁGINAS
    for ($i = 0; $i < $encuesta['pages']; ++$i) {
      $respObj['pages'][] = array();
    }

    return $respObj;
  }

  /*
   * Obtiene las respuestas de un usuario específico para un encuesta específica
   * para después ser convertida a formato JSON y ser pasada a angular.js
   */
  public function getSurveyAnswers($survey, $user_id){
    unset($survey['users']);
    $tableUsuarios = $this->getTableUsers();
    $usuario       = $tableUsuarios->fetchRow($tableUsuarios->select()->where('survey_id = ?', $survey['survey_id'])->where('item_id = ?', $user_id));
    $survey["is_finalized"] = $usuario['is_finalized'];
   
    foreach ($survey['questions'] as $key => $question) {
      $answer = $this->getUserAnswerByQuestionId($survey['survey_id'], $question['question_id'], $user_id);

     
      switch ($question['type']) {
        // Dependiendo del tipo de pregunta se guardan las opciones de respuesta
        case 'abierta':
          if ($question['abierta']['value'] == 'text') {
            $survey['questions'][$key]['answer'] = $answer['answ_text'];
          } else if ($question['abierta']['value'] == 'textarea') {
            $survey['questions'][$key]['answer'] = $answer['answ_textarea'];
          }
          break;
        case 'cerrada':
          if ($question['cerrada']['value'] == 'unica') {
            $survey['questions'][$key]['answer']         = $answer['answ_option_id'];
            $survey['questions'][$key]['another_answer'] = $answer['answ_another'];
          } else if ($question['cerrada']['value'] == 'multiple') {
            $survey['questions'][$key]['answer'] = array();
            $answers                             = $this->getUserAnswerByQuestionId($survey['survey_id'], $question['question_id'], $user_id, true);
            foreach ($answers as $key_ans => $answer) {
              if ($answer['answ_option_id'] == null) {
                $survey['questions'][$key]['another_answer'] = $answer['answ_another'];
              } else {
                $survey['questions'][$key]['answer'][] = $answer['answ_option_id'];
              }
            }
          }
          break;
        case 'matriz':
          $survey['questions'][$key]['answer'] = array();
          $answers                             = $this->getUserAnswerByQuestionId($survey['survey_id'], $question['question_id'], $user_id, true);
          foreach ($answers as $key_ans => $answer) {
            $survey['questions'][$key]['answer'][$answer['answ_mtr_question_id']] = array(
              'answ_mtr_question_id' => $answer['answ_mtr_question_id'],
              'answ_mtr_option_id'   => $answer['answ_mtr_option_id'],
            );
          }
          break;
      }
    }
    return $survey;
  }

  /*
   * Al crear o editar una encuesta, se guarda cada una de sus preguntas
   * esta función guarda una pregunta específica para una encuesta
   */
  public function GuardarPregunta($question, $encuesta){
    // Guardar las preguntas de una encuesta

    $tableQuestions = Engine_Api::_()->getDbtable('questions', 'encuestas');
    $db_qestion     = $tableQuestions->getAdapter();
    $db_qestion->beginTransaction();
    
    try {
      // Crear una encuesta nueva si el campo survey_id NO está en el request,
      // De lo contrario actualizar la encuesta existente
      if ($question['question_id'] != '') {
        $question_row = $tableQuestions->findRow($question['question_id']);
      } else {
        $question_row = $tableQuestions->createRow();
        unset($question['question_id']);
      }

      $valuesQuestion = array_merge($question, array('survey_id' => $encuesta['survey_id']));
      $question_row->setFromArray($valuesQuestion);
      $question_row->save();

      switch ($question['type']) {
        // Dependiendo del tipo de pregunta se guardan las opciones de respuesta
        case 'abierta':
          $question_row['type_value'] = $question['abierta']['value'];
          break;
        case 'cerrada':
          $question_row['type_value'] = $question['cerrada']['value'];
          $this->GuardarOpcionesPreguntaCerrada($question, $question_row); // Se guardan las opciones de respuesta para una pregunta tipo cerrada
          break;
        case 'matriz':
          $this->GuardarPreguntasMatriz($question, $question_row); // MATRIZ PREGUNTAS
          $this->GuardarOpcionesMatriz($question, $question_row); // MATRIZ RESPUESTAS
          break;
      }

      $question_row->save();
      $db_qestion->commit();
      return $question_row['question_id'];
    } catch (Exception $e) {
      $db_qestion->rollBack();
    }
  }

  /*
   * Al guardar una encuesta, se guardan una por una sus preguntas,
   * cuando la pregunta es tipo cerrada entonces se guardan sus opciones en esta función
   */
  public function GuardarOpcionesPreguntaCerrada($question, $question_row){
    $tableQuestioncerrada = $this->getTableQuestioncerrada();
    $db_qc                = $tableQuestioncerrada->getAdapter();
    $db_qc->beginTransaction();

    try {
      //Eliminar las opciones de la pregunta cerrada
      Engine_Api::_()->getDbtable('questioncerradas', 'encuestas')->delete(array('question_id = ?' => $question_row['question_id']));

      foreach ($question['cerrada']['options'] as $key => $option) {
        $q_opt_row = $tableQuestioncerrada->createRow();
        $q_opt_row->setFromArray(array_merge($option, array('question_id' => $question_row['question_id'])));
        $q_opt_row->save();
      }

      $db_qc->commit();
    } catch (Exception $e) {
      $db_qc->rollBack();

      return $this->sendErrorResponse('Error al crear esta encuesta.', $e->getMessage());
    }
  }


  /*
   * Al guardar una encuesta, se guardan una por una sus preguntas,
   * cuando la pregunta es tipo matriz entonces se guardan sus preguntas en esta función
   */
  public function GuardarPreguntasMatriz($question, $question_row){
    $tableMatrizquestions = $this->getTableMatrizquestions(); // MATRIZ PREGUNTAS
    $db_mq                = $tableMatrizquestions->getAdapter();
    $db_mq->beginTransaction();

    try {
      //Eliminar las preguntas de la matriz
      Engine_Api::_()->getDbtable('matrizquestions', 'encuestas')->delete(array('question_id = ?' => $question_row['question_id']));

      foreach ($question['matriz']['questions'] as $key => $m_question) {        
        $matrizQuestionRow = $tableMatrizquestions->createRow();
        $matrizQuestionRow->setFromArray(array_merge($m_question, array('question_id' => $question_row['question_id'])));
        $matrizQuestionRow->save();
        $db_mq->commit();
      }
    } catch (Exception $e) {
      $db_mq->rollBack();
      return $this->sendErrorResponse('Error al crear esta encuesta.', $e->getMessage());
    }
  }

  /*
   * Al guardar una encuesta, se guardan una por una sus preguntas,
   * cuando la pregunta es tipo matriz entonces se guardan sus opciones en esta función
   */
  public function GuardarOpcionesMatriz($question, $question_row){
    $tableMatrizoptions = $this->getTableMatrizoptions(); // MATRIZ RESPUESTAS
    $db_mopt            = $tableMatrizoptions->getAdapter();
    $db_mopt->beginTransaction();

    try {
      //Eliminar las opciones de la matriz
      Engine_Api::_()->getDbtable('matrizoptions', 'encuestas')->delete(array('question_id = ?' => $question_row['question_id']));

      foreach ($question['matriz']['options'] as $key => $option) {
        //Si esta la opcion de la matriz la busca de lo contrario la crea
        $matrizOptionRow = $tableMatrizoptions->createRow();
        $matrizOptionRow->setFromArray(array_merge($option, array('question_id' => $question_row['question_id'])));
        $matrizOptionRow->save();
        $db_mopt->commit();
      }
    } catch (Exception $e) {
      $db_mopt->rollBack();

      return $this->sendErrorResponse('Error al crear esta encuesta.', $e->getMessage());
    }
  }

  /*
   * Al guardar una encuesta, se guardan uno por uno sus usuarios invitados,
   *
   */
  public function GuardarUsuariosEncuesta($users, $encuesta){
    //$tableUsers = Engine_Api::_()->getDbtable('users', 'encuestas');
    $tableUsers = $this->getTableUsers();
    $db = $tableUsers->getAdapter();
    $db->beginTransaction();
    try {
      foreach ($users as $key => $user) {

        $surveyUser = $tableUsers->createRow();
        $surveyUser->setFromArray( array('item_id' => $user['item_id'], 'survey_id' => $encuesta['survey_id']) );
        $surveyUser->save();
        $db->commit();
      }
    } catch (Exception $e) {
      $db->rollBack();
      return $this->sendErrorResponse('Error al crear un usuario para la encuesta.', $e->getMessage());
    }
  }

  /*
   * Al editar una encuesta se eliminan primero todas las preguntas para
   * después volverlas a crear desde cero
   */
  public function EliminarTodasLasPreguntas($survey_id){
    $usersTable           = $this->getTableUsers();
    $questionsTable       = $this->getTableQuestions();
    $optionsMatrizTable   = $this->getTableMatrizoptions();
    $questionsMatrizTable = $this->getTableMatrizquestions();
    $questionCerradaTable = $this->getTableQuestioncerrada();

    $questions = $questionsTable->fetchAll(√);
    foreach ($questions as $key => $question) {
      $questionsCerradas = $questionCerradaTable->fetchAll($questionCerradaTable->select()->where('question_id = ?', $question['question_id']));
      foreach ($questionsCerradas as $key => $q_cerrada) { $q_cerrada->delete(); }
      $questionsMatriz = $questionsMatrizTable->fetchAll($questionsMatrizTable->select()->where('question_id = ?', $question['question_id']));
      foreach ($questionsMatriz as $key => $q_matriz) { $q_matriz->delete(); }
      $optionsMatriz = $optionsMatrizTable->fetchAll($optionsMatrizTable->select()->where('question_id = ?', $question['question_id']));
      foreach ($optionsMatriz as $key => $o_matriz) { $o_matriz->delete(); }
      // Eliminar la pregunta
      $question->delete();
    }

    // Eliminar los usuarios de una encuesta
    $users = $usersTable->fetchAll($usersTable->select()->where('survey_id = ?', $survey_id));
    foreach ($users as $key => $user) { $user->delete(); }
  }




  /*
   * Obtiene la lista de todas las encuestas en formato ARRAY
   *
   */
  public function getListaEncuestas(){
    $db     = Zend_Db_Table::getDefaultAdapter();
    $select = new Zend_Db_Select($db);
    $select->from('engine4_encuestas_surveys', array('*'))->order("created_at DESC");
    return $db->fetchAll($select);
  }


  /*
   * Obtiene la cantidad de preguntas de una encuesta
   *
   */
  public function getTotalQuestions($survey_id){
    $tabQuestions = Engine_Api::_()->getDbtable('questions', 'encuestas');
    $selQuestions = $tabQuestions
                    ->select()
                    ->from(array("r" => $tabQuestions->info('name')), array("count(question_id) as count"))
                    ->where("r.survey_id = ? ", $survey_id);
    $listaCounts  = $tabQuestions->fetchRow($selQuestions);
    return $listaCounts['count'];
  }


  /*
   * Obtiene la cantidad de preguntas de una encuesta
   *
   */
  public function getTotalUserSend($survey_id){
    $tabQuestions = Engine_Api::_()->getDbtable('users', 'encuestas');
    $selQuestions = $tabQuestions
                    ->select()
                    ->from(array("r" => $tabQuestions->info('name')), array("count(user_id) as count"))
                    ->where("r.survey_id = ? ", $survey_id);
    $listaCounts  = $tabQuestions->fetchRow($selQuestions);
    return $listaCounts['count'];
  }


  /*
   * Obtiene la lista de encuestas asignadas a un usuario específico
   * la encuesta tiene que estar activa, haber sido enviada y el usuario debe haber sido invitado
   */
  public function getListaMisEncuestas($user_id = null){
    $db     = Zend_Db_Table::getDefaultAdapter();
    $select = new Zend_Db_Select($db);
    $select->from('engine4_encuestas_surveys', array('*'))
      ->joinInner('engine4_encuestas_users', 'engine4_encuestas_surveys.survey_id = engine4_encuestas_users.survey_id', array("is_finalized"))
      ->where('engine4_encuestas_surveys.is_active = ?', 1)
      ->where('engine4_encuestas_surveys.is_sent = ?', 1)
      ->where('engine4_encuestas_users.item_id = ?', $user_id)
      ->order('engine4_encuestas_surveys.survey_id DESC');

    return $db->fetchAll($select);
  }

  /*
   * Obtiene la información de una encuesta específica asignada a un usuario específico
   *
   */
  public function getMySurvey($user_id = null, $survey_id = null){
    $db     = Zend_Db_Table::getDefaultAdapter();
    $select = new Zend_Db_Select($db);
    $select->from('engine4_encuestas_surveys', array('*'))
      ->joinInner('engine4_encuestas_users', 'engine4_encuestas_surveys.survey_id = engine4_encuestas_users.survey_id', array())
      ->where('engine4_encuestas_surveys.is_active = ?', 1)
      ->where('engine4_encuestas_surveys.is_sent = ?', 1)
      ->where('engine4_encuestas_surveys.survey_id = ?', $survey_id)
      ->where('engine4_encuestas_users.item_id = ?', $user_id);
    //$this->debug($select->__toString());
    return $db->fetchRow($select);
  }

  /*
   * Obtiene la lista de todos los usuarios de la intranet
   *
   */
  public function getAllUsersList(){
    $respObj  = array();
    $usuarios = Engine_Api::_()->user()->getActiveUsers(array("user_id", "displayname", "email"))->toArray();

    foreach ($usuarios as $usuario) {
      $respObj[] = array(
        'item_id'     => $usuario['user_id'],
        'displayname' => str_replace("'", "", $usuario['displayname']),
        'email'       => $usuario['email'],        
        //'sede'        => $usuario->getCargo(),
        'selected'    => true
      );
      
    }


    /*
    foreach ($users as $key => $user) {
      $respObj[] = array(
        'user_id'     => $user['user_id'],
        'displayname' => str_replace("'", "", $user['displayname']),
        'email'       => $user['email'],
        'sede'        => $user['email'],
        'area'        => $user['email'],
        'selected'    => false,
      );
    }*/

    return $respObj;
  }

  /*
   * Función reutilizable para eliminar registros de una tabla específica con unas
   * condiciones específicas en la variable $params
   */
  public function deleteFromTable($table, $params){
    $db = $table->getAdapter();
    $db->beginTransaction();
    $table->delete($params);
    $db->getProfiler()->setEnabled(true);
    $profiler = $db->getProfiler();
    $query    = $profiler->getLastQueryProfile();
    $params   = $query->getQueryParams();
    $querystr = $query->getQuery();
    foreach ($params as $par) {
      $querystr = preg_replace('/\\?/', "'" . $par . "'", $querystr, 1);
    }
    //echo $querystr;
    //$db->commit();
    return $db->commit();
  }

  public function debug($data){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    exit;
  }

  /*
   * Obtiene una respuesta específica de una pregunta específica para un usuario
   *
   */
  public function getUserAnswerByQuestionId($survey_id = null, $question_id = null, $user_id = null, $all = false, $matrizquestion_id = null ){
    $tableAnswers = $this->getTableAnswers();
    $select       = $tableAnswers->select()
      ->where('survey_id   = ?', $survey_id)
      ->where('question_id = ?', $question_id)
      ->where('user_id     = ?', $user_id);

    if (!is_null($matrizquestion_id)) {
      $select->where('answ_mtr_question_id = ?', $matrizquestion_id);
    }

    if ($all == true) {
      return $tableAnswers->fetchAll($select);
    } else {
      $answer = $tableAnswers->fetchRow($select);
      if (is_null($answer)) {
        $answer = $tableAnswers->createRow();
      }

      return $answer;
    }
  }

  /*
   * Borra las respuestas de tipo Cerrada Multiple con unos parametros específicos
   * user_id, question_id
   */
  public function deleteAnswersCerradaMultiple($params){
    $tablaAns = Engine_Api::_()->getDbtable('answers', 'encuestas');
    $selecAns = $tablaAns->select();
    foreach ($params as $key => $param) {
      $selecAns->where($key, $param);
    }    
    $answers = $tablaAns->fetchRow($selecAns);

    if (!empty($answers)) {
      $answers->delete();
    }
  }

  /*
   * Obtiene la cantidad de preguntas de una encuesta específica
   *
   */
  public function getTotalCantidadPreguntas($survey_id = null){
    $tableQuestions = $this->getTableQuestions();
    return $tableQuestions->getTotalQuestionsBySurveyId($survey_id);
  }

  /*
   * Obtiene la cantidad de respuestas de un usuario en una encuesta específica
   *
   */
  public function getTotalCantidadRespuestas($survey_id = null, $user_id = null){


    $tabAnswers   = Engine_Api::_()->getDbtable('answers', 'encuestas');
    $tabQuestions = Engine_Api::_()->getDbtable('questions', 'encuestas');
    $selQuestions = $tabQuestions
                    ->select()
                    ->setIntegrityCheck(false)
                    ->from(array("r" => $tabAnswers->info('name')), array("count(answer_id) as count"))
                    ->join(array("t" => $tabQuestions->info('name')), 't.survey_id = r.survey_id AND t.question_id = r.question_id', array())
                    ->where("r.survey_id = ? ", $survey_id)
                    ->where("r.user_id = ? ", $user_id)
                    ->group('r.question_id');
    $listaCounts  = $tabQuestions->fetchAll($selQuestions);
    return count($listaCounts);
  }

  /*
   * Revisa la cantidad de usuarios que han respondido la encuesta y la actualiza en la BD
   *
   */
  public function getTotalSurveyAnswers($survey_id){
    $tableEncuestas = $this->getTableEncuestas();
    $survey         = $tableEncuestas->findRow($survey_id);
    $tableUsers     = $this->getTableUsers();
    $users          = $tableUsers->fetchAll($tableUsers->select()->where('survey_id = ?', $survey_id));
    $tableQuestions = $this->getTableQuestions();
    $questions      = $tableQuestions->fetchAll($tableQuestions->select()->where('survey_id = ?', $survey_id));

    $totalAnswers = 0;
    foreach ($users as $key => $user) {
      $totalUserAnswers = $this->getTotalCantidadRespuestas($survey_id, $user['item_id']);
      if ($totalUserAnswers == count($questions)) {
        $totalAnswers++;
      }
    }
    $survey['total_answers'] = $totalAnswers;
    $survey->save();
  }

  /*
   * Devuelve un boolean para saber si un usuario ha finalizado o no una encuesta
   *
   */
  public function isFinalizedAnswer($survey_id, $user_id){
    $userTable = $this->getTableUsers();
    $select    = $userTable->select()
      ->where('survey_id = ?', $survey_id)
      ->where('item_id = ?', $user_id);
    $userSurvey = $userTable->fetchRow($select);
    return $userSurvey['is_finalized'];
  }

  /*
   * Obtiene una lista de todos los usuarios que están invitados a una encuesta
   *
   */
  public function getSurveyUsers($survey_id){
    $db     = Zend_Db_Table::getDefaultAdapter();
    $select = new Zend_Db_Select($db);
    $select->from('engine4_users AS u', array('u.user_id', 'u.email', 'u.displayname', 'ue.*'))
      ->joinInner('engine4_encuestas_users AS ue', 'ue.item_id = u.user_id', array())
      ->where('ue.survey_id = ?', $survey_id);

    return $db->fetchAll($select);
  }
}
