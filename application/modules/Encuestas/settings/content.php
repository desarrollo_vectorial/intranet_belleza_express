<?php
return array(
	array(
			'title' 	  => 'Encuestas',
			'description' => 'Descripcion de Widget encuestas',
			'category' 	  => 'Encuestas',
			'type' 		  => 'widget',
			'name' 		  => 'encuestas'
		),

	// ------- where I am
	array(
		'title'       => 'Donde estoy - Encuestas',
		'description' => 'Muestra en que modulo se encuestra actualmente',
		'category'    => 'Encuestas',
		'type'        => 'widget',
		'name'        => 'encuestas.where-i-am'
	),
);