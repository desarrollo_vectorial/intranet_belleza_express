<?php 

return array(
  'package' => array(
    'type'        => 'module',
    'name'        => 'encuestas',
    'version'     => '1.0.0',
    'path'        => 'application/modules/Encuestas',
    'title'       => 'Encuestas',
    'description' => 'Módulo de encuestas',
    'author'      => 'Vectorial Studios Ltda',
    'callback'    => array(
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => array(
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => array(
      0 => 'application/modules/Encuestas',
    ),
    'files' => array(
      0 => 'application/languages/en/encuestas.csv',
    ),
  ),
  'routes' => array(
    // User - General
    'crear_encuesta' => array(
      'route'        => 'encuestas/admin/editar/:survey_id',
      'defaults'     => array(
        'module'     => 'encuestas',
        'controller' => 'admin',
        'action'     => 'crear'
      )
    ),

    // User - General
    'duplicar_encuesta' => array(
      'route'        => 'encuestas/admin/duplicar/:survey_id',
      'defaults'     => array(
        'module'     => 'encuestas',
        'controller' => 'admin',
        'action'     => 'duplicar'
      )
    ),

    // User - General
    'duplicarencuesta_encuesta' => array(
      'route'        => 'encuestas/admin/duplicarencuesta/:survey_id',
      'defaults'     => array(
        'module'     => 'encuestas',
        'controller' => 'admin',
        'action'     => 'duplicarencuesta'
      )
    ),

    // User - General
    'vistaprevia_encuesta' => array(
      'route'        => 'encuestas/admin/vistaprevia/:survey_id',
      'defaults'     => array(
        'module'     => 'encuestas',
        'controller' => 'admin',
        'action'     => 'vistaprevia'
      )
    ),

    // User - General
    'genreporte_encuesta' => array(
      'route'        => 'encuestas/admin/genreporte/:survey_id/:fecha_ini/:fecha_fin',
      'defaults'     => array(
        'module'     => 'encuestas',
        'controller' => 'admin',
        'action'     => 'genreporte'
      )
    ),

    // User - General
    'genreporteconsolidado_encuesta' => array(
      'route'        => 'encuestas/admin/genreporteconsolidado/:survey_id/:fecha_ini/:fecha_fin',
      'defaults'     => array(
        'module'     => 'encuestas',
        'controller' => 'admin',
        'action'     => 'genreporteconsolidado'
      )
    ),

    // User - General
    'genreportedemografico_encuesta' => array(
      'route'        => 'encuestas/admin/genreportedemografico/:survey_id/:fecha_ini/:fecha_fin',
      'defaults'     => array(
        'module'     => 'encuestas',
        'controller' => 'admin',
        'action'     => 'genreportedemografico'
      )
    ),

    // User - General
    'contestar_encuesta' => array(
      'route'        => 'encuestas/index/contestar/:survey_id/:user_id',
      'defaults'     => array(
        'module'     => 'encuestas',
        'controller' => 'index',
        'action'     => 'contestar'
      )
    ),

    // User - General
    'error_encuesta' => array(
      'route'        => 'encuestas/index/error/:mensaje/',
      'defaults'     => array(
        'module'     => 'encuestas',
        'controller' => 'index',
        'action'     => 'error'
      )
    ),  


    // User - General
    'sendsurvey_encuesta' => array(
      'route'        => 'encuestas/admin/sendsurvey/:survey_id',
      'defaults'     => array(
        'module'     => 'encuestas',
        'controller' => 'admin',
        'action'     => 'sendsurvey'
      )
    ),






    // User - General
    'prueba_encuesta' => array(
      'route'        => 'encuestas/admin/prueba',
      'defaults'     => array(
        'module'     => 'encuestas',
        'controller' => 'admin',
        'action'     => 'prueba'
      )
    ),



  ),
); ?>
