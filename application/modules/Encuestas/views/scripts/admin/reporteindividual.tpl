<div class="container-fluid encuestas--container">
	<?php echo $this->partial('_scripts.tpl'); ?>
	<div class="container-app" ng-app="app-encuestas">
        <div class="container-nginit" ng-controller="admin_reporteind_ctrl" ng-init='init(<?php echo json_encode($this->users); ?>, <?php echo json_encode($this->encuesta); ?>)'>

            <div class="row align-items-center">
                <div class="col-sm-6 text-left">
                    <h2>Reporte individual de encuestas</h2>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="/encuestas/admin/" class="btn btn-outline-danger">Salir</a>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th data-field="title">Encuesta</th>
                            <th class="center-align" data-field="title" width="30%">Usuario Invitado</th>
                            <th class="center-align" data-field="creation_date" width="15%">Estado</th>
                            <th class="center-align" data-field="options" width="15%">Ver informe individual</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr dir-paginate=" user in users | filter: paginationUsers.q | itemsPerPage: paginationUsers.itemsPerPage" current-page="paginationUsers.currentPage">

                            <td>{{ encuesta.title }}</td>
                            <td class="center-align">{{ user.displayname }}</td>
                            <td class="center-align">{{ ( user.is_finalized ) ? 'Finalizado' : 'Pendiente' }} </td>
                            <td class="center-align">
                                <a href="/encuestas/admin/reporteindividualuser/survey_id/<?php echo $this->survey_id; ?>/user_id/{{ user.item_id }}" class="btn btn-dark btn-sm" ng-if=" user.is_finalized == 1">Ver</a>
                                <span class="btn btn-dark btn-sm" ng-if="user.is_finalized == 0">No hay reporte</span>
                            </td>
                        </tr>

                        </tbody>

                    </table>
                </div>
                <div class="col-sm-12">
                    <nav aria-label="Page navigation">
                        <dir-pagination-controls boundary-links="true"></dir-pagination-controls>
                    </nav>
                </div>
            </div>

        </div>
	</div>
</div>