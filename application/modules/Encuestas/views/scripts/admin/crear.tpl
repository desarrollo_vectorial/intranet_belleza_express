<div class="container-fluid encuestas--container">
    <?php echo $this->partial('_scripts.tpl'); ?>

    <div class="row">
        <div class="col-sm-12 text-right">
            <a href="/encuestas/admin/index" class="btn btn-outline-danger">Salir</a>
        </div>
    </div>


    <div class="row" ng-app="app-encuestas">
        <div class="col-12 col-sm-12" id="form_create_survey" ng-controller="form_survey_ctrl"
        ng-init='init(<?php echo json_encode($this->tipos_preguntas); ?>, <?php echo json_encode($this->encuesta); ?>, <?php echo json_encode($this->allUsersList); ?>, <?php echo $this->countUsers; ?>)'>

            <div ng-if=" ! survey.is_sent ">            
                <?php echo $this->partial('admin/wizard_steps/step_one.tpl'); ?>
                <?php echo $this->partial('admin/wizard_steps/step_two.tpl'); ?>
                <?php echo $this->partial('admin/wizard_steps/step_three.tpl'); ?>
                <?php echo $this->partial('admin/wizard_steps/step_four.tpl'); ?>
                <?php echo $this->partial('admin/wizard_steps/step_five.tpl'); ?>
            </div>

            <div on-finish="finishedWizard()" ng-if=" survey.is_sent " class="valign-wrapper">
                <h4 class="deep-orange-text text-darken-4 center-align"><br><br>
                    Esta encuesta no se puede editar debido a que ya ha sido enviada una notificación por correo a los usuarios invitados.
                </h4>
            </div>
        </div>
    </div>
    
</div>