<div class="container-fluid encuestas--container">
    <?php echo $this->partial('_scripts.tpl'); ?>
    <div class="row align-items-center">
        <div class="col-sm-6 text-left">
            <h2>Generar Reporte Detallado</h2>
        </div>
        <div class="col-sm-6 text-right">
            <a href="/encuestas/admin/" class="btn btn-outline-danger">Salir</a>
        </div>
    </div>
    <hr>
    <div class="row" ng-app="app-encuestas">
        <div class="col-12 col-sm-12" id="form_generate" ng-controller="admin_reporte_ctrl" ng-init='init(
				<?php echo json_encode($this->listadoEncuestas); ?>)'>
            <form action="/encuestas/admin/genreporte/" method="get" accept-charset="utf-8" name="formGen" id="formGen">
                <div class="row">
                    <div class="input-field col-4 col-sm-4">
                        <div class="form-group">
                            <label for="survey_limit_date">Seleccione la encuesta</label>
                            <select class="form-control browser-default validate" id="survey_limit_date" ng-model="reporte.survey_id">
                                <option ng-repeat="survey in surveys" value="{{ survey.survey_id }}">{{ survey.title }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-field col-4 col-sm-4" ng-class=" (wizardForm.stepOne.limit_date == false) ? 'errorValidation' : '' ">
                        <div class="form-group">
                            <label for="survey_limit_date">Fecha Inicio</label>
                            <input placeholder="Fecha inicio reporte" type="text" class="form-control" ng-model="reporte.fecha_ini" jqdatepicker2 />
                        </div>
                    </div>
                    <div class="input-field col-4 col-sm-4" ng-class=" (wizardForm.stepOne.limit_date == false) ? 'errorValidation' : '' ">
                        <div class="form-group">
                            <label for="survey_limit_date">Fecha Final</label>
                            <input placeholder="Fecha fin reporte" type="text" class="form-control" ng-model="reporte.fecha_fin" jqdatepicker3 />
                        </div>
                    </div>
                    <div class="input-field col-12 col-sm-12" style="text-align:center">
                        <input type="button" name="btnGenerar" ng-click="validarFormulario()" value="Generar Detallado" class="btn btn-secondary">
                        <input type="button" name="btnGenerarCon" ng-click="validarFormularioConsolidado()" value="Generar Consolidado" class="btn btn-secondary">
                        <input type="button" name="btnGenerarCon" ng-click="validarFormularioDemografico()" value="Generar Demografico" class="btn btn-secondary">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>