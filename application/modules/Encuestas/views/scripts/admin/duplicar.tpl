<?php echo $this->partial('_scripts.tpl'); ?>
<div class="row" ng-app="app-encuestas">
    <div class="col s12" id="form_create_survey" ng-controller="form_survey_ctrl" ng-init='init(<?php echo json_encode($this->tipos_preguntas); ?>, <?php echo json_encode($this->encuesta); ?>, <?php echo json_encode($this->allUsersList); ?>, <?php echo $this->countUsers; ?>)'>
        <a href="/encuestas/admin/index" class="waves-effect waves-light right btn red darken-4" style="color:white; font-weight: normal;">
            <!--<i class="material-icons left">chevron_left</i>--> <small>Salir</small>
        </a>
        <h1 class="cyan-text text-accent-4">
            Duplicar Encuesta
        </h1>

        <div>
            <?php echo $this->partial('admin/wizard_steps/step_one.tpl'); ?>
            <?php echo $this->partial('admin/wizard_steps/step_two.tpl'); ?>
            <?php echo $this->partial('admin/wizard_steps/step_three.tpl'); ?>
            <?php echo $this->partial('admin/wizard_steps/step_four.tpl'); ?>
            <?php echo $this->partial('admin/wizard_steps/step_five_duplicar.tpl'); ?>
        </div>
    </div>
</div>