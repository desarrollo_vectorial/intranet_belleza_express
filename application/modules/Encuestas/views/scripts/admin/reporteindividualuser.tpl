<?php
$this->headScript()->appendFile($this->baseUrl() . '/application/modules/Encuestas/externals/scripts/angular-wizard.min.js');
?>
<div class="container-fluid encuestas--container">
    <?php echo $this->partial('_scripts.tpl'); ?>
    <div class="container-app"
         ng-app="app-encuestas">

        <div class="container-nginit"
             ng-controller="admin_reporteinduser_ctrl"
             ng-init='init(<?php echo json_encode($this->encuesta); ?>)'>

            <div class="row">
                <div class="col-sm-12 text-right">
                    <a href="/encuestas/admin/reporteindividual/survey_id/<?php echo $this->survey_id; ?>" class="btn btn-outline-danger">Salir</a>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <h2 class="blue-text text-darken-2">
                        {{
                        my_survey.title
                        }}
                    </h2>
                </div>
                <div class="col-12">
                    <h6 class="blue-grey-text text-darken-1">
                        {{
                        my_survey.description
                        }}
                    </h6>
                </div>

                <div class="col-12">
                    <p class="blue-text text-darken-4">
                        {{
                        my_survey.instructions
                        }}
                    </p>
                </div>
            </div>

            <hr>

            <wizard on-finish="FinalizarEncuesta()">
                <!--<wz-step ng-repeat=" page in my_survey.pages " wz-title="Página - {{ $index + 1 }}" canexit=" validarPreguntasRequeridas ">-->
                <div class="row container--inputs-encuesta">

                    <div class="col-6 col-sm-6" ng-repeat=" question in my_survey.questions | filter:{ page: $index } ">

                        <div class="col-12 col-sm-12">
                            <h5 class="blue-grey-text text-darken-1">
                                <span ng-if=" question.is_required "
                                      class="red-text text-accent-4">* </span>
                                {{
                                question.title
                                }}
                            </h5>
                        </div>

                        <!-- PREGUNTA ABIERTA DE TIPO TEXT -->
                        <div class="question_abierta_tex row"
                             ng-if=" question.type == 'abierta' && question.abierta.value == 'text' ">
                            <div class="input-field col-6 col-sm-6">
                                <input disabled
                                       placeholder="{{ question.title }}"
                                       id="qst_{{ question.question_id }}"
                                       type="text"
                                       class="validate"
                                       ng-model=" question.answer ">
                                <!--<label for="qst_{{ question.question_id }}">Respuesta</label>-->
                            </div>
                        </div>
                        <!-- PREGUNTA ABIERTA DE TIPO TEXT -->

                        <!-- PREGUNTA ABIERTA DE TIPO TEXTAREA -->
                        <div class="question_abierta_textarea row"
                             ng-if=" question.type == 'abierta' && question.abierta.value == 'textarea' ">
                            <div class="input-field col-6 col-sm-6">
                                <textarea
                                        disabled
                                        placeholder="{{ question.title }}"
                                        id="qst_{{ question.question_id }}"
                                        class="materialize-textarea"
                                        ng-model=" question.answer "></textarea>
                                <!--<label for="qst_{{ question.question_id }}">Respuesta</label>-->
                            </div>
                        </div>
                        <!-- PREGUNTA ABIERTA DE TIPO TEXTAREA -->

                        <!-- PREGUNTA CERRADA DE TIPO UNICA -->
                        <div class="question_cerrada_unica row"
                             ng-if=" question.type == 'cerrada' && question.cerrada.value == 'unica' ">
                            <div class="input-field col-12 col-sm-12">
                                <p ng-repeat=" option in question.cerrada.options ">
                                    <input disabled
                                           name="question_cerrada_unica_{{ question.question_id }}"
                                           type="radio"
                                           id="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}"
                                           ng-value="{{ option.questioncerrada_id }}"
                                           ng-model=" question.answer "/>
                                    <label for="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}">{{
                                        option.value
                                        }}</label>
                                </p>
                                <p ng-if=" question.has_another ">
                                    <input disabled
                                           name="question_cerrada_unica_{{ question.question_id }}"
                                           type="radio"
                                           id="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}"
                                           ng-value="{{ question.another_answer }}"
                                           ng-model=" question.answer "/>
                                    <label for="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}">{{
                                        question.name_another
                                        }}</label>
                                </p>
                                <div class="input-field col-12 col-sm-12"
                                     ng-if=" question.has_another ">
                                    <input disabled
                                           placeholder="{{ question.name_another }}"
                                           id="qst_{{ question.question_id }}"
                                           type="text"
                                           class="validate"
                                           ng-model=" question.another_answer ">
                                    <!--<label for="qst_{{ question.question_id }}">{{ question.name_another }}</label>-->
                                </div>
                            </div>
                        </div>
                        <!-- PREGUNTA CERRADA DE TIPO UNICA -->

                        <!-- PREGUNTA CERRADA DE TIPO MULTIPLE -->
                        <div class="question_cerrada_multiple row"
                             ng-if=" question.type == 'cerrada' && question.cerrada.value == 'multiple' ">
                            <div class="input-field col-12 col-sm-12">
                                <p ng-repeat=" option in question.cerrada.options ">
                                    <input disabled
                                           name="question_cerrada_multiple_{{ question.question_id }}[]"
                                           type="checkbox"
                                           id="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}"
                                           ng-value="{{ option.questioncerrada_id }}"
                                           ng-checked="question.answer.indexOf(option.questioncerrada_id) != -1"/>
                                    <label for="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}">{{
                                        option.value
                                        }}</label>
                                </p>
                                <p ng-if=" question.has_another ">
                                    <input disabled
                                           name="question_cerrada_multiple_{{ question.question_id }}[]"
                                           type="checkbox"
                                           id="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}"
                                           value=""
                                           ng-checked=" question.another_answer "/>
                                    <label for="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}">{{
                                        question.name_another
                                        }}</label>
                                </p>
                                <div class="input-field col-12 col-sm-12"
                                     ng-if=" question.has_another ">
                                    <input disabled
                                           placeholder="{{ question.name_another }}"
                                           id="qst_{{ question.question_id }}"
                                           type="text"
                                           class="validate"
                                           ng-model=" question.another_answer ">
                                </div>
                            </div>
                        </div>
                        <!-- PREGUNTA CERRADA DE TIPO MULTIPLE -->

                        <!-- PREGUNTA MATRIZ -->
                        <div class="question_matriz row"
                             ng-if=" question.type == 'matriz' ">
                            <table class="striped bordered centered">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th ng-repeat=" option in question.matriz.options ">
                                        {{
                                        option.value
                                        }}
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr ng-repeat=" question_mtr in question.matriz.questions ">
                                    <td>
                                        {{
                                        question_mtr.value
                                        }}
                                    </td>
                                    <td ng-repeat=" option_mtr in question.matriz.options ">
                                        <p>
                                            <input
                                                    disabled
                                                    name="qst_mtr_{{ question.question_id }}_{{ question_mtr.matrizquestion_id }}"
                                                    type="radio"
                                                    id="qst_{{ question.question_id }}_{{ question_mtr.matrizquestion_id }}_{{ option_mtr.matrizoption_id }}"
                                                    ng-click=" selectQuestionMatriz(question, question_mtr.matrizquestion_id, option_mtr.matrizoption_id ) "
                                                    ng-checked=" question.answer[ question_mtr.matrizquestion_id ]['answ_mtr_option_id'] == option_mtr.matrizoption_id  "
                                            />
                                            <label for="qst_{{ question.question_id }}_{{ question_mtr.matrizquestion_id }}_{{ option_mtr.matrizoption_id }}"> </label>
                                        </p>
                                    </td>

                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <!-- PREGUNTA MATRIZ -->

                    </div>

                </div>

                <div class="row">
                    <div class="input-field col-12 col-sm-12">
                        <a class="waves-effect waves-light left btn blue darken-4"
                           ng-if=" $index > 0 "
                           wz-previous>
                            <!--<i class="material-icons left">chevron_left</i>-->
                            Atrás
                        </a>

                        <a class="waves-effect waves-light right btn blue darken-4"
                           ng-if=" $index < survey.pages.length - 1"
                           wz-next>
                            <!--<i class="material-icons right">chevron_right</i>-->
                            Siguiente
                        </a>
                    </div>
                </div>

                <!--</wz-step>-->

            </wizard>


        </div>
    </div>
</div>