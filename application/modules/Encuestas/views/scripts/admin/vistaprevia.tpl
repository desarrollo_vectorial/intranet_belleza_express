<?php
$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/angular-wizard.min.js');
?>
<div class="container-fluid encuestas--container">
    <?php echo $this->partial('_scripts.tpl'); ?>
    <div class="container-app" ng-app="app-encuestas">
        <div class="container-nginit" ng-controller="admin_vistaprevia_ctrl" ng-init='init(
			<?php echo json_encode($this->encuesta); ?>)'>
            <div class="row align-items-center text-left">
                <div class="col-6 col-sm-6">
                    <h2 class="blue-text text-darken-2">{{ survey.title }}</h2>
                    <h4 class="blue-grey-text text-darken-1">{{ survey.description }}</h4>
                    <p class="blue-text text-darken-4">{{ survey.instructions }}</p>
                </div>
                <div class="col-6 col-sm-6 text-right">
                    <a href="/encuestas/admin/editar/{{ survey.survey_id }}" class="btn btn-outline-danger">Salir</a>
                </div>
            </div>
            <hr>
            <div class="row" id="container--preview">

                <wizard class="col-sm-12" on-finish="FinalizarEncuesta()">
                    <wz-step ng-repeat=" page in survey.pages " wz-title="Página - {{ $index + 1 }}" canexit=" validarPreguntasRequeridas ">

                        <div class="col-sm-12" ng-repeat=" question in survey.questions | filter:{ page: $index } ">

                            <div class="col-12 col-sm-12">
                                <h5 class="blue-grey-text text-darken-1">
                                    <span ng-if=" question.is_required " class="red-text text-accent-4">* </span> {{ question.title }}
                                </h5>
                            </div>

                            <!-- PREGUNTA ABIERTA DE TIPO TEXT -->
                            <div class="col-12 col-sm-12 question_abierta_tex" ng-if=" question.type == 'abierta' && question.abierta.value == 'text' ">
                                <div class="input-field">
                                    <input disabled placeholder="{{ question.title }}" id="qst_{{ question.question_id }}" type="text" class="validate" ng-model=" question.answer ">
                                    <!--<label for="qst_{{ question.question_id }}">Respuesta</label>-->
                                </div>
                            </div><!-- PREGUNTA ABIERTA DE TIPO TEXT -->

                            <!-- PREGUNTA ABIERTA DE TIPO TEXTAREA -->
                            <div class="col-12 col-sm-12 question_abierta_textarea" ng-if=" question.type == 'abierta' && question.abierta.value == 'textarea' ">
                                <div class="input-field">
                                    <textarea disabled placeholder="{{ question.title }}" id="qst_{{ question.question_id }}" rows="3" class="form-control" ng-model=" question.answer "></textarea>
                                    <!--<label for="qst_{{ question.question_id }}">Respuesta</label>-->
                                </div>
                            </div><!-- PREGUNTA ABIERTA DE TIPO TEXTAREA -->

                            <!-- PREGUNTA CERRADA DE TIPO UNICA -->
                            <div class="col-12 col-sm-12 question_cerrada_unica" ng-if=" question.type == 'cerrada' && question.cerrada.value == 'unica' ">
                                <div class="input-field">
                                    <p ng-repeat=" option in question.cerrada.options ">
                                        <input disabled name="question_cerrada_unica_{{ question.question_id }}" type="radio" id="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}" ng-value="{{ option.questioncerrada_id }}" ng-model=" question.answer "/>
                                        <label for="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}">{{ option.value }}</label>
                                    </p>
                                    <p ng-if=" question.has_another ">
                                        <input disabled name="question_cerrada_unica_{{ question.question_id }}" type="radio" id="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}" ng-value="{{ question.another_answer }}" ng-model=" question.answer "/>
                                        <label for="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}">{{ question.name_another }}</label>
                                    </p>
                                    <div class="input-field" ng-if=" question.has_another ">
                                        <input disabled placeholder="{{ question.name_another }}" id="qst_{{ question.question_id }}" type="text" class="validate" ng-model=" question.another_answer ">
                                        <!--<label for="qst_{{ question.question_id }}">{{ question.name_another }}</label>-->
                                    </div>
                                </div>
                            </div><!-- PREGUNTA CERRADA DE TIPO UNICA -->

                            <!-- PREGUNTA CERRADA DE TIPO MULTIPLE -->
                            <div class="col-12 col-sm-12 question_cerrada_multiple" ng-if=" question.type == 'cerrada' && question.cerrada.value == 'multiple' ">
                                <div class="input-field">
                                    <p ng-repeat=" option in question.cerrada.options ">
                                        <input
                                                disabled
                                                name="question_cerrada_multiple_{{ question.question_id }}[]"
                                                type="checkbox" id="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}"
                                                ng-value="{{ option.questioncerrada_id }}"
                                                ng-checked="question.answer.indexOf(option.questioncerrada_id) != -1"
                                                ng-click=" selectQuestionCerradaMultiple(question, option.questioncerrada_id) "
                                        />
                                        <label for="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}">{{ option.value }}</label>
                                    </p>
                                    <p ng-if=" question.has_another ">
                                        <input
                                                disabled
                                                name="question_cerrada_multiple_{{ question.question_id }}[]"
                                                type="checkbox" id="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}"
                                                value=""
                                                ng-checked=" question.another_answer "
                                                ng-click=""
                                        />
                                        <label for="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}">{{ question.name_another }}</label>
                                    </p>
                                    <div class="input-field" ng-if=" question.has_another ">
                                        <input disabled placeholder="{{ question.name_another }}" id="qst_{{ question.question_id }}" type="text" class="validate" ng-model=" question.another_answer ">
                                        <!--<label for="qst_{{ question.question_id }}">Otro</label>-->
                                    </div>
                                </div>
                            </div><!-- PREGUNTA CERRADA DE TIPO MULTIPLE -->

                            <!-- PREGUNTA MATRIZ -->
                            <div class="col-12 col-sm-12 question_matriz" ng-if=" question.type == 'matriz' ">
                                <table class="striped bordered centered">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th ng-repeat=" option in question.matriz.options ">{{ option.value }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat=" question_mtr in question.matriz.questions ">
                                        <td>{{ question_mtr.value }}</td>
                                        <td ng-repeat=" option_mtr in question.matriz.options ">
                                            <p>
                                                <input
                                                        disabled
                                                        name="qst_mtr_{{ question.question_id }}_{{ question_mtr.matrizquestion_id }}"
                                                        type="radio"
                                                        id="qst_{{ question.question_id }}_{{ question_mtr.matrizquestion_id }}_{{ option_mtr.matrizoption_id }}"
                                                        ng-click=" selectQuestionMatriz(question, question_mtr.matrizquestion_id, option_mtr.matrizoption_id ) "
                                                        ng-checked=" question.answer[ question_mtr.matrizquestion_id ]['answ_mtr_option_id'] == option_mtr.matrizoption_id "
                                                />
                                                <label for="qst_{{ question.question_id }}_{{ question_mtr.matrizquestion_id }}_{{ option_mtr.matrizoption_id }}"></label>
                                            </p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div><!-- PREGUNTA MATRIZ -->

                        </div><!--ng repeat-->


                        <div class="row" style="margin-top: 30px; padding: 20px;">
                            <div class="col-6 col-sm-6 text-left">
                                <a class="btn btn-danger" ng-if=" $index > 0 " wz-previous>Atrás</a>
                            </div>
                            <div class="col-6 col-sm-6 text-right">
                                <a class="btn btn-danger" ng-if=" $index < survey.pages.length - 1" wz-next>Siguiente</a>
                            </div>
                        </div>

                    </wz-step>
                </wizard>

            </div><!--row-->
        </div>
    </div>
</div>