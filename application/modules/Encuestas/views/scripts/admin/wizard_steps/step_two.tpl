<div wz-title="Agregar preguntas" canenter="wizardValidations.validateStepTwo" canexit="wizardValidations.validateStepTwo" ng-if="wizardPage == 2" class="card-panel">
    <div class="row">
        <div class="col-sm-12">
            <!-- INFORMACIÓN DE LA ENCUESTA -->
            <h4 class="blue-text text-darken-2">Paso 2: Agregar preguntas.</h4>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-12">
            <a href="/encuestas/admin/vistaprevia/survey_id/{{ survey.survey_id }}" target="_blank" class="btn btn-secondary btn-sm">Vista Previa</a>
            <a ng-click="agregarPagina()" class="btn btn-secondary btn-sm">Agregar página</a>
            <a id="create-question" ng-click="openModal()" class="btn btn-secondary btn-sm">Agregar pregunta</a>
        </div>
    </div>
    <!-- INFORMACIÓN DE LAS PREGUNTAS -->
    <div class="row">
        <div class="survey_pages_container">
            <div ng-repeat=" page in survey.pages " class="survey_page card-panel white">
                <div class="col-sm-12">
                    <h3 class="blue-text text-darken-2">
                        Página {{ $index + 1 }}

                        <!--<a ng-if="$index > 0" ng-click="eliminarPagina($index)" class="btn-floating right orange darken-4"><i class="material-icons left">delete</i></a>-->
                        <a ng-if="$index > 0" ng-click="eliminarPagina($index)" class="btn btn-outline-danger btn-sm" style="font-size: 10px !important;">Eliminar</a>
                    </h3>
                </div>
                <hr>
                <div class="page_questions_container">
                    <div ng-repeat=" question in survey.questions | filter:{ page: $index } " class="card-panel blue-grey lighten-5">
                        <div class="col-sm-12">
                            <h4>{{ question.title }}</h4>
                        </div>
                        <div class="col-sm-12">
                            <h6 ng-show=" question.is_required ">Requerida</h6>
                        </div>
                        <div class="col-sm-12">
                            <!-- Tipo de pregunta Abierta -->
                            <div ng-show=" question.type == 'abierta' ">
                                <p>Pregunta Abierta de opción {{ ( question.abierta.value == 'text' ) ? 'Texto corto' : 'Texto largo' }} </p>
                            </div>
                            <!-- Tipo de pregunta Abierta -->
                            <!-- Tipo de pregunta Cerrada -->
                            <div ng-show=" question.type == 'cerrada' ">
                                <p>Pregunta Cerrada de opción {{ ( question.cerrada.value == 'unica' ) ? 'Única' : 'Múltiple' }} </p>
                            </div>
                            <!-- Tipo de pregunta Cerrada -->
                            <!-- Tipo de pregunta Matriz -->
                            <div ng-show=" question.type == 'matriz' ">
                                <p>Pregunta tipo Matriz</p>
                            </div>
                            <!-- Tipo de pregunta Matriz -->
                        </div>
                        <div class="col-sm-12" style="padding: 10px 15px">
                            <a style="font-size: 10px !important;" class="btn btn-outline-success btn-sm" ng-click=" editQuestion(question) " >Editar</a>
                            <a style="font-size: 10px !important;" class="btn btn-outline-danger btn-sm" ng-click=" deleteQuestion(question) " ng-confirm-click="Está seguro de eliminar ésta pregunta?">Eliminar</a>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
    <!-- INFORMACIÓN DE LAS PREGUNTAS -->
    <!-- FORMULARIO PARA AGREGAR NUEVAS PREGUNTAS -->
    <div id="create-question-dialog" ng-show="show_modal">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h5 class="red-text text-darken-2">*Todos los campos son requeridos.</h5>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">

                    <form>
                        <div class="row">
                            <div class="col-6 col-sm-6">
                                <!-- Tipo de pregunta -->
                                <label for="question_type">Tipo de pregunta</label>
                                <select ng-model="question.type" class="browser-default">
                                    <option disabled selected value="">-- Seleccione un tipo de pregunta --</option>
                                    <option ng-repeat="tipos_pregunta in tipos_preguntas" value="{{tipos_pregunta.title}}">{{ tipos_pregunta.name }}</option>
                                </select>
                            </div>
                            <div class="col-3 col-sm-3">
                                <label for="question_type">Asignar a la página</label>
                                <select ng-model=" question.page " class="browser-default">
                                    <option ng-repeat="page in survey.pages" value="{{ $index }}">{{ $index + 1 }}</option>
                                </select>
                            </div>
                            <div class="col-3 col-sm-3 align-self-center" style="padding-top: 30px;">
                                <div class="form-check">
                                    <input type="checkbox" ng-checked="survey.is_anonymous" ng-model="question.is_required" class="form-check-input" id="is_required">
                                    <label class="form-check-label" for="is_required">Obligatoria</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <!-- Información básica de la pregunta -->
                                <div class="form-group">
                                    <label for="question_title">Pregunta</label>
                                    <input type="text" class="form-control" ng-model="question.title" id="question_title" placeholder="Pregunta">
                                </div>
                            </div>
                        </div>
                        <!-- Contenedor de las opciones de una pregunta -->
                        <!-- Pregunta de tipo Abierta : Tipo texto corto ó tipo texto largo -->
                        <div id="options_abierta_container" ng-show=' question.type == "abierta" '>
                            <p>
                                <input name="options_abierta_type" type="radio" id="options_abierta_type_text" value="text" ng-model=" question.abierta.value " />
                                <label for="options_abierta_type_text">Respuesta corta
                                    <small class="blue-text text-darken-2">(Texto de respuesta corta 255 caracteres)</small>
                                </label>
                            </p>
                            <p>
                                <input name="options_abierta_type" type="radio" id="options_abierta_type_textarea" value="textarea" ng-model=" question.abierta.value " />
                                <label for="options_abierta_type_textarea">Párrafo
                                    <small class="blue-text text-darken-2">(Texto de respuesta larga)</small>
                                </label>
                            </p>
                        </div>


                        <!-- Pregunta de tipo Cerrada : Tipo respuesta única ó tipo respuesta múltiple -->
                        <div id="options_cerrada_container" ng-show=' question.type == "cerrada" '>
                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="form-check">
                                        <input class="form-check-input" name="options_cerrada_type" type="radio" id="options_cerrada_type_unica" value="unica" ng-model=" question.cerrada.value">
                                        <label class="form-check-label" for="options_cerrada_type_unica">
                                            Selección múltiple, única respuesta.
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" name="options_cerrada_type" type="radio" id="options_cerrada_type_multiple" value="multiple" ng-model=" question.cerrada.value">
                                        <label class="form-check-label" for="options_cerrada_type_multiple">
                                            Selección múltiple, múltiple respuesta.
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="options_cerrada_add_another" value="another" ng-model=" question.has_another ">
                                        <label class="form-check-label" for="options_cerrada_add_another">
                                            Añadir respuesta Jutificación
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name_another" ng-model="question.name_another" placeholder="Texto que va a aparecer en la justificación. Ej: Otros, ¿Por qué?" />
                                    </div>

                                </div>

                                <div class="col-sm-12">
                                    <div id="options_cerrada_items" ng-repeat="option in question.cerrada.options ">
                                        <div class="row">
                                            <div class="col-10 col-sm-10">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" ng-model="option.value" placeholder="Opción" />
                                                </div>
                                            </div>
                                            <div class="col-2 col-sm-2">
                                                <a class="btn btn-outline-danger btn-sm" ng-click=" question.cerrada.options.splice($index, 1) ">X</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button id="options_cerrada_add_item" ng-click="question.cerrada.options.push({value: ''}); " class="btn btn-secondary">Agregar opción de respuesta</button>
                                </div>
                            </div>
                        </div>


                        <!-- Pregunta de tipo Matriz -->
                        <div id="options_matriz_container" ng-show=' question.type == "matriz" '>
                            <div class="row">

                                <div class="col-6 col-sm-6" style="border-right: 1px solid #e9e9ea;">
                                    <h4 class="indigo-text text-lighten-1">Preguntas</h4>
                                    <hr>

                                    <div id="options_matriz_questions_container" ng-repeat=" question_item in question.matriz.questions ">
                                        <div class="row">
                                            <div class="col-10 col-sm-10">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" ng-model="question_item.value" placeholder="Pregunta" />
                                                </div>
                                            </div>
                                            <div class="col-2 col-sm-2">
                                                <a class="btn btn-outline-danger btn-sm" ng-click=" question.matriz.questions.splice($index, 1)">X</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <a id="options_matriz_add_question" ng-click=" question.matriz.questions.push({value: ''})" class="btn btn-secondary btn-sm">Agregar pregunta</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-6 col-sm-6">
                                    <h4 class="indigo-text text-lighten-1">Opciones</h4>
                                    <hr>

                                    <div id="options_matriz_options_container" ng-repeat=" option_item in question.matriz.options ">
                                        <div class="row">
                                            <div class="col-10 col-sm-10">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" ng-model="option_item.value" placeholder="Opción" />
                                                </div>
                                            </div>
                                            <div class="col-2 col-sm-2">
                                                <a class="btn btn-outline-danger btn-sm" ng-click="question.matriz.options.splice($index, 1)">X</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <a id="options_matriz_add_option" ng-click="question.matriz.options.push({value: ''}) " class="btn btn-secondary btn-sm">Agregar opción</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 40px;">
                            <div class="col-6 col-sm-6 text-center">
                                <button ng-click="addQuestion()" class="btn btn-danger">
                                    Guardar
                                </button>
                            </div>
                            <div class="col-6 col-sm-6 text-center">
                                <button ng-click="closeModal()" class="btn btn-danger">
                                    Cancelar
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div><!--end modal-->
    <div class="row">
        <div class="input-field col-6 col-sm-6 text-center">
            <a class="btn btn-danger" ng-click="wizardGoto(1)">Atrás</a>
        </div>
        <div class="input-field col-6 col-sm-6 text-center">
            <a class="btn btn-danger" ng-click="wizardNext(2)">Siguiente</a>
        </div>
    </div>
    <!-- FORMULARIO PARA AGREGAR NUEVAS PREGUNTAS -->
</div ng-if="wizardPage == 2">
