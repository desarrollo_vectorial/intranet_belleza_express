<div wz-title="Invitar contactos" canenter="wizardValidations.validateStepThree" canexit="wizardValidations.validateStepThree" ng-if="wizardPage == 3" class="card-panel">

    <div class="row">
        <div class="col-sm-12">
            <!-- INFORMACIÓN DE LA ENCUESTA -->
            <h4 class="blue-text text-darken-2">Paso 3: Invitar contactos.</h4>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-12">
            <button ng-click="show_available = true" class="btn btn-secondary btn-sm">Usuarios disponibles</button>
            <button ng-click=" show_available = false " class="btn btn-secondary btn-sm">Usuarios Invitados</button>
        </div>
    </div>

    <!-- A Quienes va dirigida la encuesta -->
    <div class="row">
        <div class="col-sm-12" id="lista_usuarios_disponibles" ng-if=" show_available ">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col" data-field="all">
                            <div class="form-check" style="padding-bottom: 25px;">
                                <input class="form-check-input filled-in" type="checkbox" value="" id="selected_allusers_checkbox" ng-model="AllUsers_selectAll" ng-change=" selectAllUsers() ">
                                <label class="form-check-label" for="selected_allusers_checkbox">
                                    Todos
                                </label>
                            </div>
                        </th>
                        <th scope="col" data-field="displayname">
                            <div class="form-group">
                                <label for="searchname">Nombre</label>
                                <input type="text" class="form-control" id="searchname" placeholder="Escribir nombre ..." ng-model="paginationAllUsers.q.displayname">
                            </div>
                        </th>
                        <th scope="col" data-field="email">
                            <div class="form-group">
                                <label for="searchemail">E-mail</label>
                                <input type="text" class="form-control" id="searchemail" placeholder="Escribir correo ..." ng-model="paginationAllUsers.q.email">
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr dir-paginate=" user in all_users | filter: paginationAllUsers.q | itemsPerPage: paginationAllUsers.itemsPerPage" current-page="paginationAllUsers.currentPage">
                        <td>
                            <p class="left-align">
                                <input type="checkbox" class="filled-in" id="select_user_checkbox_{{ user.item_id }}" ng-checked="user.selected" ng-model="user.selected" ng-change=" addSelectedUsers() " />
                                <label for="select_user_checkbox_{{ user.item_id }}" class="margin-clear"></label>
                            </p>
                        </td>
                        <td>{{ user.displayname }}</td>
                        <td>{{ user.email }}</td>
                    </tr>
                </tbody>
            </table>

            <div class="row">
                <div class="col-sm-12 text-center">
                    <dir-pagination-controls boundary-links="true"></dir-pagination-controls>
                </div>
            </div>
        </div>

        <div class="col-sm-12" id="lista_usuarios_invitados" ng-if=" ! show_available ">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th data-field="all">Invitados</th>
                        <th data-field="displayname">
                            Nombre
                            <br>
                            <input type="text" name="" ng-model="paginationInvitedUsers.q.displayname">
                        </th>
                        <th data-field="email">
                            Email
                            <br>
                            <input type="text" name="" ng-model="paginationInvitedUsers.q.email">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr dir-paginate=" user in all_users | filter: paginationInvitedUsers.q | itemsPerPage: paginationInvitedUsers.itemsPerPage" current-page="paginationInvitedUsers.currentPage">
                        <td>
                            <p>
                                <input type="checkbox" class="filled-in" id="selected_user_checkbox_{{ user.item_id }}" ng-checked="user.selected" ng-model="user.selected" ng-change=" addSelectedUsers() " />
                                <label for="selected_user_checkbox_{{ user.item_id }}"></label>
                            </p>
                        </td>
                        <td>{{ user.displayname }}</td>
                        <td>{{ user.email }}</td>
                    </tr>
                </tbody>
            </table>

            <div class="row">
                <div class="col-sm-12 text-center">
                    <dir-pagination-controls boundary-links="true"></dir-pagination-controls>
                </div>
            </div>

        </div>

    </div>

    <div class="row" style="margin-top: 40px;">
        <div class="input-field col-6 col-sm-6 text-center">
            <a class="btn btn-danger" ng-click="wizardGoto(2)">Atrás</a>
        </div>
        <div class="input-field col-6 col-sm-6 text-center">
            <a class="btn btn-danger" ng-click="wizardNext(3)">Siguiente</a>
        </div>
    </div>
    <!-- A Quienes va dirigida la encuesta -->
</div>
