<div wz-title="Crear encuesta" canenter="wizardValidations.validateStepOne" canexit="wizardValidations.validateStepOne" ng-if="wizardPage == 1" class="card-panel">
    <div class="row">
        <div class="col-sm-12">
            <!-- INFORMACIÓN DE LA ENCUESTA -->
            <h4 class="blue-text text-darken-2">Paso 1: Crear la encuesta.</h4>
        </div>
    </div>

    <hr>

    <div class="row">        
        <div class="input-field col-12 col-sm-12" ng-class=" (wizardForm.stepOne.title == false) ? 'errorValidation' : '' ">
            <div class="form-group">
                <label for="survey_title">Título de la encuesta</label>
                <input type="text" class="form-control validate" ng-model="survey.title" id="survey_title" placeholder="Título de la encuesta">
            </div>
        </div>

        <div class="input-field col-12 col-sm-12" ng-class=" (wizardForm.stepOne.instructions == false) ? 'errorValidation' : '' ">
            <div class="form-group">
                <label for="survey_instructions">Instrucciones</label>
                <textarea class="form-control" ng-model="survey.instructions" id="survey_instructions" rows="3"></textarea>
            </div>
        </div>

        <div class="input-field col-12 col-sm-12" ng-class=" (wizardForm.stepOne.description == false) ? 'errorValidation' : '' ">
            <div class="form-group">
                <label for="survey_description">Mensaje para invitar a los usuarios a responder la encuesta</label>
                <small>(Este es un mensaje de invitación predeterminado. Si desea modificarlo reemplace el texto.)</small>
                <textarea class="form-control" ng-model="survey.description" id="survey_description" rows="3"></textarea>
            </div>
        </div>

        <div class="input-field col-12 col-sm-6" ng-class=" (wizardForm.stepOne.limit_date == false) ? 'errorValidation' : '' ">
            <div class="form-group">
                <label for="survey_limit_date">Fecha límite</label>
                <input placeholder="Fecha límite para responder la encuesta" id="survey_limit_date" type="text" class="validate form-control" ng-model="survey.limit_date" jqdatepicker />
            </div>
        </div>

        <div class="input-field col-12 col-sm-6 align-self-center">
            <div class="form-check">
                <input type="checkbox" ng-checked="survey.is_anonymous" ng-model="survey.is_anonymous" class="form-check-input" id="input_is_anonima">
                <label class="form-check-label" for="input_is_anonima">Encuesta anónima</label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-sm-12 text-center">
            <a class="btn btn-danger" ng-click="wizardNext(1)">Siguiente</a>
        </div>
    </div>
    <!-- INFORMACIÓN DE LA ENCUESTA -->
</div>

<script>
    /* (function(){

        'use strict';
        document.getElementById('survey_title').focus();

    })(); */

    jQuery(document).on('ready', function (){
        window.setTimeout(function(){
            //Materialize.updateTextFields();
            jQuery('#survey_title').focus();
        }, 50);
    });  
</script>
