<div wz-title="Finalizar" canenter="wizardValidations.validateStepFive" canexit="wizardValidations.validateStepFive" ng-if="wizardPage == 5" class="card-panel">
    <div class="row">
        <div class="col-sm-12">
            <!-- INFORMACIÓN DE LA ENCUESTA -->
            <h4 class="blue-text text-darken-2">Paso 5: Envio</h4>
        </div>
        <hr>
        <div class="col-sm-12">
            <p>Su encuesta ha sido creada con exito,  para terminar el proceso solo falta enviarla a los usuarios.</p>
        </div>
    </div>

    <div class="row" style="margin-top: 40px;">
        <div class="input-field col-6 col-sm-6 text-center">
            <a class="btn btn-danger" ng-click="wizardGoto(4)">Atrás</a>
        </div>
        <div class="input-field col-6 col-sm-6 text-center">
            <a id="save_survey" ng-disabled="isReserved(1)" ng-click="sendSurvey()" class="btn btn-success">ENVIAR</a>
        </div>
    </div>
</div>
