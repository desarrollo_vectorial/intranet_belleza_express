<div wz-title="Verificar encuesta" canenter="wizardValidations.validateStepFour" canexit="wizardValidations.validateStepFour" ng-if="wizardPage == 4" class="card-panel">

    <div class="row">
        <div class="col-sm-12">
            <!-- INFORMACIÓN DE LA ENCUESTA -->
            <h2 class="blue-text text-darken-2">Paso 4: Verificar encuesta.</h2>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-12 text-right">
            <a href="/encuestas/admin/vistaprevia/survey_id/{{ survey.survey_id }}" target="_blank" class="btn btn-secondary btn-sm">Vista Previa</a>
        </div>
    </div>

	<div class="row">

        <div class="col-sm-12">
            <p>La encuesta sobre ( {{ survey.title }} ), se va a realizar a {{ survey.users.length }} usuario(s), de {{ count_users }} posibles.</p>
        </div>

        <div class="col-sm-12">
            <h2 class="light-blue-text text-accent-4">{{ survey.title }}</h2>
        </div>

        <div class="col-sm-12">

            <div ng-repeat="page in survey.pages">

                <hr>

                <div class="row" style="margin-top: 20px;">
                    <div class="col-sm-12">
                        <h3 class="page_indicator_title">Página número: {{ $index + 1 }}</h3>
                    </div>
                </div>

                <hr>

                <div class="row" ng-repeat=" question in survey.questions | filter:{ page: $index }">
                    <div class="col-sm-12">
                        <h4>{{ $index + 1 }} - {{ question.title }}</h4>
                    </div>
                    <div class="col-sm-12" ng-show="question.is_required">
                        <h4>Requerida</h4>
                    </div>
                    <div class="col-sm-12" ng-show=" question.is_demographic">
                        <h4>Demográfica</h4>
                    </div>
                    <div class="col-sm-12" ng-show="question.type == 'abierta'">
                        <p>Pregunta Abierta de opción {{ ( question.abierta.value == 'text' ) ? 'Texto corto' : 'Texto largo' }} </p>
                    </div>
                    <div class="col-sm-12" ng-show="question.type == 'cerrada'">
                        <p>Pregunta Cerrada de opción {{ ( question.cerrada.value == 'unica' ) ? 'Única' : 'Múltiple' }} </p>
                    </div>
                    <div class="col-sm-12" ng-show="question.type == 'matriz'">
                        <p>Pregunta tipo Matriz</p>
                    </div>
                </div>
            </div>
        </div>

	</div>


    <div class="row" style="margin-top: 40px;">
        <div class="input-field col-6 col-sm-6 text-center">
            <a class="btn btn-danger" ng-click="wizardGoto(3)">Atrás</a>
        </div>
        <div class="input-field col-6 col-sm-6 text-center">
            <a class="btn btn-danger" ng-click="wizardNext(4)">Siguiente</a>
        </div>
    </div>
</div>
