<!DOCTYPE html>
<html>
<head>
	<title>Mailing Encuestas</title>
</head>
<body>

	<table width="750px" border="1px">
		<tbody>
			<tr>
				<td>
					<img src="http://vectorial-portal.vectorial.co/colombina/mailing-encabezado.jpg" alt="" height="167px" width="750px">
				</td>
			</tr>

			<tr>
				<td align="justify" style="padding: 40px;">

					<h1>Hola <b>'.$usuario['displayname'].',</b><br><br></h1>

					<h2>Has sido invitado a responder la siguiente encuesta: <span style="color:#5ca9f4">'.$encuesta['title'].'</span></h2>

					<center>
						<a href="http://colombinet.intranet.col/encuestas/index/contestar/survey_id/'.$encuesta['survey_id'].'/user_id/'.$usuario['user_id'].'" >
							<img src="http://vectorial-portal.vectorial.co/colombina/mailing-boton.png" alt="" height="90px" width="350px">
						</a>
					</center>
				</td>
			</tr>

			<tr>
				<td>
					<img src="http://vectorial-portal.vectorial.co/colombina/mailing-pie.jpg" alt="" height="77" width="750px">
				</td>
			</tr>
		</tbody>
	</table>

</body>
</html>