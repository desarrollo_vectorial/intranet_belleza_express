<div class="container-fluid encuestas--container">

	<?php echo $this->partial('_scripts.tpl'); ?>

	<div class="container-app index-admin" ng-app="app-encuestas">

        <div class="container-nginit" ng-controller="admin_index_ctrl" ng-init='init(<?php echo json_encode($this->encuestas); ?>)'>

            <div class="row align-items-center text-left">
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="encuesta-buscar">Buscar encuesta:</label>
                        <input type="text" ng-model="paginationSurveys.q" class="form-control campoBusca" id="encuesta-buscar" aria-describedby="encuesta-buscar" placeholder="Buscar">
                    </div>
                </div>

                <div class="col-12 col-sm-6 text-right">
                    <a href="/encuestas/index/" class="btn btn-outline-danger">Salir</a>

                    <a href="/encuestas/admin/reportes" class="btn btn-outline-secondary">
                        Reportes
                    </a>
                    <a href="/encuestas/admin/crear" class="btn btn-outline-secondary">
                        Crear Encuesta
                    </a>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-12 col-sm-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th data-field="title" width="30%">Encuesta</th>
                                <th class="center-align" width="15%" data-field="limit_date">Fecha límite</th>
                                <th class="center-align" width="15%" data-field="users">Encuestados</th>
                                <th class="center-align" width="2%" data-field="users">Estado</th>
                                <th class="center-align" width="38%" data-field="options">Opciones de administración</th>
                            </tr>
                        </thead>

                        <tbody>

                            <tr dir-paginate=" encuesta in encuestas | filter: paginationSurveys.q | itemsPerPage: paginationSurveys.itemsPerPage" current-page="paginationSurveys.currentPage">

                                <td>{{ encuesta.title }}</td>
                                <td class="center-align">
                                    <!-- caja texto fecha limite -->
                                    <span class="label_limit_date_{{$index}}">
									{{ encuesta.limit_date }}
								</span>
                                    <!-- boton mostrar calendario - datapicker -->
                                    <a href="javascript:;" title="Editar" style="margin-left: 8px" ng-click="show_data_picker( $index )">
                                        <img src="application/modules/Encuestas/externals/images/pencil.png" alt="Editar" width="16">
                                    </a>
                                    <!-- caja datapicker -->
                                    <div class="wrp_picker_{{ $index }} hidden">
                                        <!-- calendario - datapicker -->
                                        <div class="datepicker picker_{{$index}}"></div>
                                        <!-- boton guardar -->
                                        <a href="javascript:;" ng-click="save_data_picker( $index, encuesta.survey_id )" data-sending="false" class="btn-green btn_save_{{$index}}">Guardar </a>
                                        <!-- boton cerrar -->
                                        <a href="javascript:;" ng-click="cancel_data_picker( $index )" class="blue-grey-text text-darken-3" style="margin-left: 10px;">Cerrar </a>

                                        <!-- indicador loading -->
                                        <span style="float:right;" class="wrp_loading_picker_{{$index}} hidden">
										<img src="application/modules/Encuestas/externals/images/loading.gif" alt="Editar" width="20" style="margin-bottom: -5px"></a> Guardando
									</span>
                                        <!-- caja de mensajes -->
                                        <div class="alert wrp_message_picker_{{$index}} hidden"></div>

                                    </div>
                                </td>
                                <td class="center-align">{{ encuesta.total_users }} / {{ encuesta.total_answers }}</td>
                                <td class="center-align">
                                    <a ng-click="enviar_encuesta(encuesta)" class="btn btn-sm btn-outline-secondary" ng-if=" encuesta.is_sent == 0">
                                        <img src="application/modules/Encuestas/externals/images/red-point.png" alt="">
                                        Enviar
                                    </a>
                                    <span ng-if="encuesta.is_sent == 1" class="btn btn-sm btn-outline-secondary">
                                        <img src="application/modules/Encuestas/externals/images/blue-light-point.png" alt="">
                                         Enviada
                                     </span>
                                </td>
                                <td class="text-right">
                                    <a ng-click=" activar_encuesta(encuesta) ">
                                        <span ng-if="encuesta.is_active == 1" class="btn btn-secondary btn-sm">Inactivar</span>
                                        <span ng-if="encuesta.is_active == 0" class="btn btn-danger btn-sm">Activar</span>
                                    </a>
                                    <a href="/encuestas/admin/editar/{{ encuesta.survey_id }}" class="btn btn-primary btn-sm" ng-if="encuesta.is_sent == 0">
                                        Editar
                                    </a>
                                    <span class="btn btn-secondary btn-sm" ng-if="encuesta.is_sent == 1">Editar</span>
                                    <a href="/encuestas/admin/duplicarencuesta/{{ encuesta.survey_id }}" class="btn btn-dark btn-sm" style="font-weight: normal;">
                                        Duplicar
                                    </a>
                                    <a href="/encuestas/admin/reporteindividual/survey_id/{{ encuesta.survey_id }}" class="btn btn-dark btn-sm">
                                        Reporte
                                    </a>
                                    <a class="btn btn-light btn-sm" href="javascript:;" ng-click="delete_encuesta( encuesta, $index )">
                                        <img src="/application/modules/Encuestas/externals/images/tash.png" alt="Borrar" width="16">
                                    </a>

                                </td>

                            </tr>

                        </tbody>
                    </table>
                    <dir-pagination-controls boundary-links="true"></dir-pagination-controls>
                </div>
            </div>
        </div>
	</div>

</div>

<div id="delete-confirm" title="Confirmacion" class="hidden">
    <p align="center">
        <span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
        <span id="confirm_text"></span>
        <!-- indicador loading -->
        <span style="float:right;" class="loading_confirm hidden">
			<img src="application/modules/Encuestas/externals/images/loading.gif" alt="Editar" width="20" style="margin-bottom: -5px"></a> Procesando
		</span>
        <!-- caja de mensajes -->
    <div class="message_confirm alert hidden"></div>

    </p>
</div>