<?php
$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/angular-wizard.min.js');
?>
<div class="container-fluid encuestas--container">
    <?php echo $this->partial('_scripts.tpl'); ?>
    <div class="container-app" ng-app="app-encuestas">
        <div class="container-nginit" ng-controller="my_survey_answer_ctrl" ng-init='init(
			<?php echo json_encode($this->my_survey); ?>)'>
            <div class="row align-items-center text-left">
                <div class="col-8 col-sm-8">
                    <h2 class="blue-text text-darken-2">{{ my_survey.title }}</h2>
                    <h4 class="blue-grey-text text-darken-1">{{ my_survey.description }}</h4>
                    <p class="blue-text text-darken-4">{{ my_survey.instructions }}</p>
                </div>
                <div class="col-4 col-sm-4 text-right">
                    <a href="/encuestas/index" class="btn btn-outline-danger">Salir</a>
                    <a id="save_survey_answers" ng-click="saveSurveyAnswers()" class="btn btn-outline-primary">GUARDAR RESPUESTAS</a>
                </div>
            </div>
            <hr>

            <div class="row" id="container--contestar">

                <wizard class="col-sm-12" on-finish="FinalizarEncuesta()">
                    <wz-step ng-repeat=" page in my_survey.pages " wz-title="Página {{ $index + 1 }}" class="card-panel">

                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <h2 class="blue-text text-darken-2">Página {{ $index + 1 }}</h2>
                            </div>
                        </div>

                        <hr>

                        <div class="row" ng-repeat=" question in my_survey.questions | filter:{ page: $index } ">

                            <div class="col s12">
                                <h5 class="blue-grey-text text-darken-1">
                                    <span ng-if=" question.is_required " class="red-text text-accent-4">* </span> {{ question.title }}
                                </h5>
                            </div>

                            <!-- PREGUNTA ABIERTA DE TIPO TEXT -->
                            <div class="col-12 col-sm-12 question_abierta_tex row" ng-if=" question.type == 'abierta' && question.abierta.value == 'text' ">
                                <div class="input-field">
                                    <input placeholder="Respuesta, 200 caracteres máximo." id="qst_{{ question.question_id }}" type="text" class="validate" ng-model="question.answer" maxlength="200" >
                                </div>
                            </div><!-- PREGUNTA ABIERTA DE TIPO TEXT -->

                            <!-- PREGUNTA ABIERTA DE TIPO TEXTAREA -->
                            <div class="col-12 col-sm-12 question_abierta_textarea row" ng-if=" question.type == 'abierta' && question.abierta.value == 'textarea' ">
                                <div class="input-field col s12">
                                    <!--<label for="qst_{{ question.question_id }}">Respuesta</label>-->
                                    <textarea
                                            placeholder="{{ question.title }}"
                                            id="qst_{{ question.question_id }}"
                                            rows="3" class="form-control validate"
                                            ng-model=" question.answer "></textarea>
                                </div>
                            </div><!-- PREGUNTA ABIERTA DE TIPO TEXTAREA -->

                            <!-- PREGUNTA CERRADA DE TIPO UNICA -->
                            <div class="col-12 col-sm-12 question_cerrada_unica" ng-if=" question.type == 'cerrada' && question.cerrada.value == 'unica' ">
                                <div class="input-field">
                                    <p ng-repeat=" option in question.cerrada.options ">
                                        <input
                                                name       = "question_cerrada_unica_{{ question.question_id }}"
                                                type       = "radio"
                                                id         = "qst_{{ question.question_id }}_{{ option.questioncerrada_id }}"
                                                ng-value   = "{{ option.questioncerrada_id }}"
                                                ng-model   = "question.answer" />
                                        <label for = "qst_{{ question.question_id }}_{{ option.questioncerrada_id }}">{{ option.value }}</label>
                                    </p>
                                    <p ng-if=" question.has_another ">
                                        <input
                                                name       = "question_cerrada_unica_{{ question.question_id }}"
                                                type       = "radio" id="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}"
                                                ng-value   = "{{ question.another_answer }}"
                                                ng-model   = " question.answer " />
                                        <label for = "qst_{{ question.question_id }}_{{ option.questioncerrada_id }}">{{ question.name_another }}</label>
                                    </p>
                                    <div class="input-field col s12" ng-if=" question.has_another ">
                                        <input
                                                placeholder = "{{ question.name_another }}"
                                                id          = "qst_{{ question.question_id }}"
                                                type        = "text"
                                                class       = "validate"
                                                ng-model    = " question.another_answer "
                                                ng-disabled = "question.answer" />
                                    </div>
                                </div>
                            </div><!-- PREGUNTA CERRADA DE TIPO UNICA -->

                            <!-- PREGUNTA CERRADA DE TIPO MULTIPLE -->
                            <div class="col-12 col-sm-12 question_cerrada_multiple" ng-if=" question.type == 'cerrada' && question.cerrada.value == 'multiple' ">
                                <div class="input-field">
                                    <p ng-repeat=" option in question.cerrada.options " style="">
                                        <input
                                                name       = "question_cerrada_multiple_{{ question.question_id }}[]"
                                                type       = "checkbox" id="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}"
                                                ng-value   = "{{ option.questioncerrada_id }}"
                                                ng-checked = "question.answer.indexOf(option.questioncerrada_id) != -1"
                                                ng-click   = " selectQuestionCerradaMultiple(question, option.questioncerrada_id) "
                                        />
                                        <label for="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}">{{ option.value }}</label>
                                    </p>
                                    <p ng-if=" question.has_another ">
                                        <input
                                                name = "question_cerrada_multiple_{{ question.question_id }}"
                                                type = "checkbox"
                                                id   = "qst_{{ question.question_id }}_{{ option.questioncerrada_id }}"
                                        />
                                        <label for="qst_{{ question.question_id }}_{{ option.questioncerrada_id }}">{{ question.name_another }}</label>
                                    </p>
                                    <div class="input-field col s12" ng-if="question.has_another">
                                        <input
                                                placeholder = "{{ question.name_another }}"
                                                id          = "qst_{{ question.question_id }}"
                                                type        = "text"
                                                class       = "validate"
                                                ng-model    = "question.another_answer"  />
                                    </div>
                                </div>
                            </div><!-- PREGUNTA CERRADA DE TIPO MULTIPLE -->

                            <!-- PREGUNTA MATRIZ -->
                            <div class="col-12 col-sm-12 question_matriz" ng-if=" question.type == 'matriz' ">
                                <table class="striped bordered centered">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th ng-repeat=" option in question.matriz.options ">{{ option.value }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat=" question_mtr in question.matriz.questions ">
                                        <td>{{ question_mtr.value }}</td>
                                        <td ng-repeat=" option_mtr in question.matriz.options ">
                                            <p>
                                                <input
                                                        name       = "qst_mtr_{{ question.question_id }}_{{ question_mtr.matrizquestion_id }}"
                                                        type       = "radio"
                                                        id         = "qst_{{ question.question_id }}_{{ question_mtr.matrizquestion_id }}_{{ option_mtr.matrizoption_id }}"
                                                        ng-click   = " selectQuestionMatriz(question, question_mtr.matrizquestion_id, option_mtr.matrizoption_id ) "
                                                        ng-checked = " question.answer[ question_mtr.matrizquestion_id ]['answ_mtr_option_id'] == option_mtr.matrizoption_id "
                                                />
                                                <label for="qst_{{ question.question_id }}_{{ question_mtr.matrizquestion_id }}_{{ option_mtr.matrizoption_id }}"></label>
                                            </p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- PREGUNTA MATRIZ -->
                        </div>

                        <div class="row" style="margin-top: 30px; padding: 20px;">
                            <div class="col-6 col-sm-6 text-left" ng-if=" $index > 0 " wz-previous>
                                <a class="btn btn-danger">Atrás</a>
                            </div>
                            <div class="col-6 col-sm-6 text-right" ng-if=" $index < my_survey.pages.length - 1" ng-click="nextPage()">
                                <a class="btn btn-danger">Siguiente</a>
                            </div>
                            <div class="col-6 col-sm-6 text-right" ng-if=" $index == my_survey.pages.length - 1" wz-next>
                                <a class="btn btn-danger">Finalizar</a>
                            </div>
                        </div>

                    </wz-step>
                </wizard>
            </div>
        </div>
    </div>
</div>