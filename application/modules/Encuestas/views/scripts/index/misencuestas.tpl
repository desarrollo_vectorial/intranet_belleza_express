<div class="container-fluid encuestas--container">
    <?php echo $this->partial('_scripts.tpl'); ?>

    <div class="container-app" ng-app="app-encuestas">
        <div class="container-nginit" id="all_my_surveys" ng-controller="my_surveys_ctrl" ng-init='init(<?php echo json_encode($this->misencuestas); ?>)'>
            <div class="row align-items-center">
                <div class="col-6 col-sm-6 text-left">
                    <div class="form-group">
                        <label for="encuesta-buscar">Buscar encuesta:</label>
                        <input type="text" ng-model="paginationMySurveys.q" class="form-control campoBusca" id="encuesta-buscar" aria-describedby="encuesta-buscar" placeholder="Nombre de la encuesta">
                    </div>
                </div>

                <div class="col-6 col-sm-6 text-right">
                    <a href="../" class="btn btn-outline-danger">Salir</a>
                    <?php if ($this->showButtons == true) { ?>
                        <a href="/encuestas/admin/" class="btn btn-outline-secondary">Administrar Encuestas</a>
                    <?php } ?>
                </div>

            </div>

            <hr>

            <div class="row">
                <div class="col-12 col-sm-12">
                    <h2>Mis encuestas</h2>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th data-field="title">
                                Encuesta
                            </th>
                            <th data-field="creation_date" width="10%" class="center-align">
                                Fecha de creación
                            </th>
                            <th data-field="limit_date" width="10%" class="center-align">
                                Fecha límite
                            </th>
                            <th data-field="average_answers" width="15%" class="center-align">
                                Porcentaje respondido
                            </th>
                            <th data-field="options" width="10%" class="center-align">
                                Opciones
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        <tr dir-paginate="encuesta in my_surveys | filter: paginationMySurveys.q | itemsPerPage: paginationMySurveys.itemsPerPage" current-page="paginationMySurveys.currentPage">
                            <td>
                                {{ encuesta.title }}
                            </td>
                            <td class="center-align">
                                {{ encuesta.created_at }}
                            </td>
                            <td class="center-align">
                                {{ encuesta.limit_date }}
                            </td>
                            <td class="center-align">
                                {{ encuesta.total_percentage + " %" }}
                            </td>
                            <td class="center-align">
                                <a href="/encuestas/index/contestar/survey_id/{{ encuesta.survey_id }}/user_id/{{ encuesta.user_id }}" class="btn btn-secondary btn-sm" ng-if=" encuesta.is_finalized  == 0 && encuesta.vencio == 0">
                                    Contestar
                                </a>

                                <h6 class="btn btn-secondary btn-sm" ng-if="encuesta.is_finalized  > 0">
                                    Finalizado
                                </h6>

                                <h6 class="btn btn-secondary btn-sm" ng-if="encuesta.vencio == 1 && encuesta.is_finalized  == 0">
                                    Encuesta vencida
                                </h6>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                    <dir-pagination-controls boundary-links="true"></dir-pagination-controls>
                </div>
            </div>

        </div>
    </div>
</div>