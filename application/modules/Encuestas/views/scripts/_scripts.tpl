<?php 

$this->headLink()->appendStylesheet($this->baseUrl().'/application/modules/Encuestas/externals/styles/jquery-ui.css');
//$this->headLink()->appendStylesheet('http://fonts.googleapis.com/icon?family=Material+Icons');
$this->headLink()->appendStylesheet($this->baseUrl().'/application/modules/Encuestas/externals/styles/crear_encuesta.css');
$this->headLink()->appendStylesheet($this->baseUrl().'/externals/jquery-fullcalendar/fullcalendar.css');
//$this->headLink()->appendStylesheet($this->baseUrl().'/application/modules/Encuestas/externals/styles/materialize.min.css');
$this->headLink()->appendStylesheet($this->baseUrl().'/application/modules/Encuestas/externals/styles/angular-wizard.min.css');


//$this->headScript()->appendFile('https://code.jquery.com/jquery-2.1.1.min.js');
//$this->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');

$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/jquery-2.1.1.min.js');
$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/jquery-ui.min.js');

$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/selectivizr.js');


//$this->headScript()->appendFile($this->baseUrl().'/externals/angular/angular-1-2-0.js');
//$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/angular-wizard.min.js');
$this->headScript()->appendFile($this->baseUrl().'/externals/angular-pagination/dirPagination.js');
$this->headScript()->appendFile($this->baseUrl().'/externals/angular-csv-reader/angular-csv-import.min.js');



$this->headScript()->appendFile($this->baseUrl().'/externals/jquery-fullcalendar/lib/moment.min.js');
//$this->headScript()->appendFile($this->baseUrl().'/externals/jquery-fullcalendar/lang/es.js');
$this->headScript()->appendFile($this->baseUrl().'/externals/jquery-fullcalendar/fullcalendar.min.js');


//$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/materialize.min.js');	
$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/app.js');
$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/directives.js');
$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/crear_encuesta.js');
$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/admin_index.js');
$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/my_surveys.js');
$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/my_survey_answer.js');
$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/admin_reporteinduser.js');
$this->headScript()->appendFile($this->baseUrl().'/application/modules/Encuestas/externals/scripts/modernizr-custom.js');
?>