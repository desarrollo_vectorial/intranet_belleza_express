# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.33)
# Base de datos: dev_centelsa_intranet
# Tiempo de Generación: 2016-11-22 22:06:10 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla engine4_encuestas_answers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine4_encuestas_answers`;

CREATE TABLE `engine4_encuestas_answers` (
  `answer_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answ_text` varchar(255) DEFAULT NULL,
  `answ_textarea` text,
  `answ_option_id` int(11) DEFAULT NULL,
  `answ_another` varchar(255) DEFAULT NULL,
  `answ_mtr_question_id` int(11) DEFAULT NULL,
  `answ_mtr_option_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla engine4_encuestas_matrizoptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine4_encuestas_matrizoptions`;

CREATE TABLE `engine4_encuestas_matrizoptions` (
  `matrizoption_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`matrizoption_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla engine4_encuestas_matrizquestions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine4_encuestas_matrizquestions`;

CREATE TABLE `engine4_encuestas_matrizquestions` (
  `matrizquestion_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`matrizquestion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla engine4_encuestas_questioncerradas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine4_encuestas_questioncerradas`;

CREATE TABLE `engine4_encuestas_questioncerradas` (
  `questioncerrada_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`questioncerrada_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla engine4_encuestas_questions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine4_encuestas_questions`;

CREATE TABLE `engine4_encuestas_questions` (
  `question_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT '',
  `type_value` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `page` int(11) NOT NULL DEFAULT '1',
  `is_required` tinyint(1) NOT NULL DEFAULT '0',
  `is_demographic` tinyint(1) NOT NULL DEFAULT '0',
  `has_another` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla engine4_encuestas_surveys
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine4_encuestas_surveys`;

CREATE TABLE `engine4_encuestas_surveys` (
  `survey_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `instructions` text NOT NULL,
  `description` text NOT NULL,
  `limit_date` date NOT NULL,
  `pages` int(11) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_anonymous` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `total_users` int(11) NOT NULL DEFAULT '0',
  `total_questions` int(11) NOT NULL DEFAULT '0',
  `total_answers` int(11) NOT NULL DEFAULT '0',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`survey_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla engine4_encuestas_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine4_encuestas_users`;

CREATE TABLE `engine4_encuestas_users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `is_finalized` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
