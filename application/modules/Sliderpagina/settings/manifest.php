<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'sliderpagina',
    'version' => '4.0.0',
    'path' => 'application/modules/Sliderpagina',
    'title' => 'Slider Paginas',
    'description' => 'Slider para Los modulos de Paginas de Colombina.',
    'author' => 'Mauricio Medina S.',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Sliderpagina',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/sliderpagina.csv',
    ),
  ),
  'routes' => array
	  (
		'sliderpagina_index_index' => array
		(
			'route' => 'sliderpagina/*',
			'defaults' => array
			(
				'module' => 'sliderpagina',
				'controller' => 'index',
				'action' => 'index',
			),
		),
  )
); ?>