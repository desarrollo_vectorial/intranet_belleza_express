	<style>
	.paginas_sliders thead td {
		border-bottom: 1px solid;
		font-size: 15px;
		font-weight: bold;
		text-align: center;
		color:#0383CD;
	}

	.paginas_sliders tr td {
		border-right: 1px solid;
		font-weight: bold;
		text-align: center;
	}

	.paginas_sliders tr td a {
		font-weight: bold;
		color:#0383CD;
	}
	</style>
<div class="mi-contenedor-2">

	<?php
		$html = "";
		$html .= "	<table width=\"100%\" class=\"paginas_sliders\">";
		$html .= "		<thead>";
		$html .= "			<tr>";
		$html .= "				<td>";
		$html .= "					MODULO";
		$html .= "				</td>";
		$html .= "				<td>";
		$html .= "					COD PAGINA";
		$html .= "				</td>";
		$html .= "				<td>";
		$html .= "					CARPETA";
		$html .= "				</td>";
		$html .= "				<td>";
		$html .= "					# FOTOS";
		$html .= "				</td>";
		$html .= "				<td>";
		$html .= "					";
		$html .= "				</td>";
		$html .= "			</tr>";
		$html .= "		</thead>";
		foreach($this->sliders as $key => $valor)
			{
		$html .= "		<tr>";
		$html .= "			<td>";
		$html .= "				".$valor['modulo']." (Gestión Humana)";
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".$valor['pagina_id'];
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".$valor['disenio'];
		$html .= "			</td>";
		$html .= "			<td>";
		$html .= "				".$valor['numero'];
		$html .= "			</td>";
		$html .= "			<td style=\"\">";
		$html .= "				<a href=\"sliderpagina/sliderpagina_id/".$valor['sliderpagina_id']."\">";
		$html .= "					editar";
		$html .= "				</a>";
		$html .= "			</td>";
		$html .= "		</tr>";
			}
		$html .= "	</table><br><br>";
		echo $html;
		echo $this->form->render($this);
	?>

</div>

<?php echo $this->partial('/application/modules/Core/views/scripts/_richeditor.tpl', array()); ?>