<?php

class Sliderpagina_IndexController extends Core_Controller_Action_Standard
{
	public function indexAction(){
		$this->_helper->content->setEnabled();
		$table = $this->_helper->api()->getDbtable('sliderpaginas', 'sliderpagina');
		
		$this->view->form = $form = new Sliderpagina_Form_Create();
		$form->setAction($this->view->url());
		
		if($this->getRequest()->isPost())
		if($form->isValid($this->getRequest()->getPost()))
		{
			$values = $form->getValues();

			$slider = $table->find($this->_getParam('sliderpagina_id'))->current();
						
			$db = $table->getAdapter();
			$db->beginTransaction();
			try
				{
					
					if(!$slider)
						{
							$sliderpagina = $table->createRow();
							$sliderpagina->setFromArray($values);
							$sliderpagina->save();
							return $this ->_redirect('sliderpagina/sliderpagina_id/'.$sliderpagina->sliderpagina_id);
						}
						else
							{
								$slider->setFromArray($values);
								$slider->save();
							}
				}
				catch( Exception $e )
					{
						$db->rollBack();
						throw $e;
					}
		}
		$query = $table->select()
			->setIntegrityCheck(false)
			->where("estado = ?", 1);

		$rstPS = $query->query();
		$sliders = $rstPS->fetchAll();
		
		$slider_ = $table->find($this->_getParam('sliderpagina_id'))->current();
		if($slider_)
			{
				/*$form->sliderpagina_id->setValue($slider_->sliderpagina_id);*/
				$form->disenio->setValue($slider_->disenio);
				$form->modulo->setValue($slider_->modulo);
				$form->pagina_id->setValue($slider_->pagina_id);
				$form->numero->setValue($slider_->numero);
				$form->extension->setValue($slider_->extension);
				$form->texto->setValue($slider_->texto);
				$form->estado->setValue($slider_->estado);
			}
				
		$this->view->sliders = $sliders;
		return;
	}
}
