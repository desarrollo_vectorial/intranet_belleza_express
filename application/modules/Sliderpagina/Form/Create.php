<?php

class Sliderpagina_Form_Create extends Engine_Form
{
	public $_error = array();

	public function init()
	{
		$this->setTitle('')
		->setDescription('')
		->setAttrib('name', 'denuncie');
		/*$this->addElement('hidden', 'sliderpagina_id');	*/
		$this->addElement('select', 'modulo', array(
			'label' => "Módulo (*)",
			'multiOptions' => array(""=>"Seleccione el Módulo / Pagina",
									"/pagina"=>"Nuestra Empresa",
									"/paginamodi"=>"Nuestra Gente",
									"/paginamodii"=>"Gestión Humana"
									),
			'allowEmpty' => false,
			'required' => true,
		));
				
		$this->addElement('Text', 'pagina_id', array(
			'label' => "Codigo De la Pagina (*)",
			'allowEmpty' => false,
			'required' => true,
		));
		
		$this->addElement('select', 'disenio', array(
			'label' => "Carpeta (*)",
			'multiOptions' => array(""=>"Seleccione el Diseño",
									"nacimientos" => "Nacimientos",
									"bodas" 			=> "Bodas",
									"grados" 			=> "Grados",
									"talentos" 		=> "Talentos",
									"albumes" 		=> "Albumes"
									),
			'allowEmpty' => false,
			'required' => true,
		));
		
		$this->addElement('Text', 'numero', array(
			'label' => "Número de Fotos (*)",
			'allowEmpty' => false,
			'required' => true,
		));
		
		$this->addElement('select', 'extension', array(
			'label' => "Extensión (*)",
			'multiOptions' => array(""=>"Seleccione la Extension del archivo",
									".jpg"=>"JPG",
									".png"=>"PNG"
									),
			'allowEmpty' => false,
			'required' => true,
		));
		
		$this->addElement('Textarea', 'texto', array(
		'label' => "Texto/Fotos (opc)",
		'allowEmpty' => true,
		'required' => false
		));
		
		$this->addElement('checkbox', 'estado', array(
		'label' => "Estado",
		'value' => true
		));
		
		
		$this->addElement('Button', 'submit', array(
		'label' => 'Enviar Formulario',
		'type' => 'submit',
		'ignore' => true,
		'decorators' => array('ViewHelper')
		));
		
		
	}
}
?>