<?php
/**
 * YouNet
 *
 * @category   Application_Extensions
 * @package    Wiki
 * @copyright  Copyright 2011 YouNet Developments
 * @license    http://www.modules2buy.com/
 * @version    $Id: Create.php
 * @author     Minh Nguyen
 */
class Ynwiki_Form_Create extends Engine_Form
{
  public $_error = array();

  public function init()
  {   
        $this
          ->setDescription("Compose your new page below, then click 'Save Page' to publish page.")
          ->setAttrib('name', 'ynwiki_create');
        $user = Engine_Api::_()->user()->getViewer();
        $user_level = Engine_Api::_()->user()->getViewer()->level_id;
        $translate = Zend_Registry::get('Zend_Translate');
        $parent = Zend_Controller_Front::getInstance()->getRequest()->getParam('fromPageId');
        // prepare page
        if(!$parent)
        {
             $this->setTitle('Añadir un nuevo espacio'); 
             $this->addElement('hidden','level', array(
                'value' => 0,
                'order' =>100
            ));  
        }
        else
        {
            $this->setTitle('Agregar una subpágina');
            $this->addElement('hidden','parent_page_id', array(
                'value' => $parent,
                'order' => 101
            ));
            $objParent = Engine_Api::_()->getItem('ynwiki_page',$parent);
            $this->addElement('hidden','level', array(
                'value' => $objParent->level + 1,
                'order' => 102
            ));
        }
        $this->addElement('Text', 'title', array(
          'label' => 'Title',
          'required' => true,
          'title' => $translate->translate('Titulo de la página'),             
          'autofocus' => 'autofocus',  
          'filters' => array(
            new Engine_Filter_Censor(),
            'StripTags',
            new Engine_Filter_StringLength(array('max' => '255'))
        )));
        
        // init to
        $this->addElement('Text', 'tags',array(
          'label'=>'Tags (Keywords)',
          'autocomplete' => 'off',
          'description' => 'Separa las etiquetas con comas.',
          'filters' => array(
            new Engine_Filter_Censor(),
          ),
        ));
       	$this->tags->getDecorator("Description")->setOption("placement", "append");
       
        $this->addElement('File', 'thumbnail', array(
	        'label' => 'Thumbnail',
	        'title' => $translate->translate('Imagen principal de la página'),    
	        'description' => 'Imagen principal de la página (jpg, png, gif, jpeg)',
        ));
        $this->thumbnail->getDecorator("Description")->setOption("placement", "append");
        $this->thumbnail->addValidator('Extension', false, 'jpg,png,gif,jpeg');
  		
  		
  		$upload_url = Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'upload-photo'), 'ynwiki_general', true);
		$allowed_html = Engine_Api::_()->authorization()->getPermission($user_level, 'ynwiki_page', 'auth_html');
		$this->addElement('Textarea', 'body', array(
          'label' => 'Content',
          'required' => true,
          'allowEmpty' => false,
          'class' => 'richtext'
          ))  ;

        $this->addElement('Button', 'submit', array(
	        'label' => 'Save',
	        'type' => 'submit',
	        'ignore' => true,
            'decorators'  => array('ViewHelper')
        ));
        // Element: cancel
        $this->addElement('Cancel', 'cancel', array(
          'label' => 'cancel',
          'link' => true,
          'prependText' => '',
          'href' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'browse'), 'ynwiki_general', true),
          'onclick' => '',
            'decorators'  => array('ViewHelper')
        ));
         // DisplayGroup: buttons
       $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
  }
}
