<?php
class Ynwiki_Form_Search extends Engine_Form
{
  public function init()
  {
    $this
      ->setAttribs(array(
        'id' => 'filter_form',
        'class' => 'global_form_box',
      ))
      ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()))
      ->setMethod('GET')
      ; 
    
    $this->addElement('Text', 'name', array(

      'placeholder' => 'Titulo',
      'class'       => 'form_titulo'
    ));
    $this->addElement('Text', 'owner', array(

      'placeholder' => 'Creado por',
      'class'       => 'form_titulo'
    ));
   
    $this->addElement('Select', 'orderby', array(
      'multiOptions' => array(
        'creation_date' => 'Most Recent',
        'view_count' => 'Most Viewed',
        'follow_count' => 'Most Followed',
        'favourite_count' => 'Most Favourite',
        'like_count' => 'Most Liked',
        'comment_count' => 'Most Commented',
        'rate_ave' => 'Most Rated',
      ),

      'class'    => 'form_select'
    ));

    $this->addElement('Hidden', 'page', array(
      'order' => 100
    ));

    $this->addElement('Hidden', 'tag', array(
      'order' => 101
    )); 

    $this->addElement('Button', 'submit', array(
      'order'  => 104,
      'label'  => 'Buscar',
      'type'   => 'submit',
      'class'  => 'form_boton',
      'ignore' => true,
    ));
  }
}