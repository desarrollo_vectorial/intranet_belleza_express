<?php 
return array(
	'package' => array(
		'type'         => 'module',
		'name'         => 'ynwiki',
		'version'      => '4.01p2',
		'path'         => 'application/modules/Ynwiki',
		'title'        => 'Social Wiki',
		'description'  => 'Wiki Plugin',
		'author'       => 'YouNet Company',
		'dependencies' => array( array(
				'type'       => 'module',
				'name'       => 'younet-core',
				'minVersion' => '4.02',
			), ),
		'callback' => array('class' => 'Engine_Package_Installer_Module', ),
		'actions'  => array(
			0 => 'install',
			1 => 'upgrade',
			2 => 'refresh',
			3 => 'enable',
			4 => 'disable',
		),
		'callback' => array(
			'path'  => 'application/modules/Ynwiki/settings/install.php',
			'class' => 'Ynwiki_Installer',
		),
		'directories' => array(0 => 'application/modules/Ynwiki', ),
		'files' => array(0 => 'application/languages/en/ynwiki.csv', ),
	),
	// Hooks ---------------------------------------------------------------------
	'hooks' => array( array(
			'event' => 'onStatistics',
			'resource' => 'Ynwiki_Plugin_Core'
		), ),
	// Items ---------------------------------------------------------------------
	'items' => array(
		'ynwiki_page',
		'ynwiki_revision',
		'ynwiki_follow',
		'ynwiki_favourite',
		'ynwiki_view',
		'ynwiki_edit',
		'ynwiki_attachment',
		'ynwiki_report',
		'ynwiki_recycle',
		'ynwiki_revision',
	),
	// Routes --------------------------------------------------------------------
	'routes' => array(
		'ynwiki_extended' => array(
			'route'        => 'wiki/:controller/:action/*',
			'defaults'     => array(
				'module'     => 'ynwiki',
				'controller' => 'index',
				'action'     => 'index',
			),
			'reqs' => array(
				'controller' => 'index|help|faqs',
				'action'     => '(index)',
			),
		),
		'ynwiki_general' => array(
			'route'        => 'wiki/:action/*',
			'defaults'     => array(
				'module'     => 'ynwiki',
				'controller' => 'index',
				'action'     => 'browse',
			),
			'reqs' => array('action' => '(browse|create|edit|delete|view|listing|upload-photo|set-permission|rate|history|preview-revision|restore-revision|download|print-view|follow|un-follow|un-follow-ajax|favourite|un-favourite|un-favourite-ajax|manage-follow|manage-favourite|more-space|view-user-rate|move-location|attach|suggest|delete-attach|compare-versions|report)', ),
		),
		'ynwiki_viewcount' => array(
			'route'        => 'wiki/getviewcount/:page_id',
			'defaults'     => array(
				'module'     => 'ynwiki',
				'controller' => 'index',
				'action'     => 'getviewcount'
			),
			'reqs' => array('page_id' => '\d+', )
		),
	)
); ?>
