<?php
class Ynwiki_FaqsController extends Core_Controller_Action_Standard{
	
	public function init() {
		$this->view->viewer_id = Engine_Api::_()->user()->getViewer()->getIdentity();
	}

	public function indexAction() {
		//$this->_helper->content->setNoRender()->setEnabled();
		$this->_helper->content->setEnabled();

		$Table  = new Ynwiki_Model_DbTable_Faqs;
		$select = $Table->select()->where('status=?','show')->order('ordering asc');	
		$items  = $Table->fetchAll($select);
		$this->view->items = $items;

	}
}



?>