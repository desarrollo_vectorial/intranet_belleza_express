<p class="title"><?php echo $this->htmlLink($this->page->getHref(), $this->page->title) ?></p>
<a name="ratepage"></a>

<?php
	if ($this->can_rate) {
		$divAction = 'onmouseout="rating_mouseout()"';
	} elseif ($this->viewer->getIdentity() == 0) {
		$divAction = 'onmouseover ="canNotRate(2);" onmouseout="canNotRate(0);"'; 
	} else {
		$divAction = 'onmouseover ="canNotRate(1);" onmouseout="canNotRate(0);"';   					
	}
	
?>

<!-- DIV to rate -->
<!--<div id="page_rate" <?php /*echo $divAction; */?>>
    <?php /*for($i = 1; $i <= 5; $i++): */?>
    	<?php
/*			if ($this->can_rate) {
				$imgStyle = 'style="cursor: pointer;"';
				$imgAction = 'onclick ="rate('.$i.');" onmouseover="rating_mousehover('.$i.');"';		  					
			} else {
				$imgStyle = $imgAction = '';
			}
			
			if ($i <= $this->page->rate_ave) {
				$imgStarName = 'star_full.png';
			} elseif( $i > $this->page->rate_ave &&  ($i-1) <  $this->page->rate_ave) {
				$imgStarName = 'star_part.png';
			} else {
				$imgStarName = 'star_none.png';
			}
    	
    	*/?>
      <img id="rate_<?php /*print $i;*/?>" <?php /*echo $imgStyle; */?> <?php /*echo $imgAction; */?> src="application/modules/Ynwiki/externals/images/<?php /*echo $imgStarName; */?>" />
    <?php /*endfor; */?>
    (<?php
/*    echo $this->translate(array('%s rating', '%s ratings', $this->page->rate_count), $this->locale()->toNumber($this->page->rate_count));
    */?>)
</div>-->
<!-- close DIV to rate -->

<div class="details">
	<span class="secondary-text">Creado por</span> <?php echo $this->htmlLink($this->owner->getHref(), $this->owner->getTitle()) ?> <span class="secondary-text"> <?php echo $this->timestamp($this->page->creation_date) ?></span>
 	<?php if($this->lastUpdate):  ?>
		<span class="secondary-text">| Última modificación por</span> <?php echo $this->htmlLink($this->lastUpdateOwner->getHref(), $this->lastUpdateOwner->getOwner()->getTitle(), array('target'=>'_top')); ?> 
		<span class="secondary-text"><?php echo $this->timestamp($this->lastUpdate->creation_date) ?>.</span>
		
		  <?php echo $this->htmlLink(array(
		          'action' => 'compare-versions',
		          'pageId' => $this->page->page_id,
		          'route' => 'ynwiki_general',
		          'reset' => true,
		        ), $this->translate("view change"), array(
		        )) ?>
   <?php endif;?>  
	<!--<div class="view-count"><?php /*echo $this->translate(array('%s view', '%s views', $this->page->view_count), $this->locale()->toNumber($this->page->view_count)); */?></div>-->
</div>

<div>
	<?php if($this->page->parent_type=='group'): ?>                       
        <?php echo $this->htmlLink($this->baseUrl().'/group/'.$this->page->parent_id.'/'.$this->page->getGroupSlug($this->page->getGroup($this->page->parent_id)),$this->page->getGroup($this->page->parent_id)).' &raquo;'; ?>
	<?php endif; ?>       
           
    <?php foreach($this->page->getBreadCrumNode() as $node): ?>
		<?php echo $this->htmlLink($node->getHref(), $this->string()->truncate($node->title,15) ) ?>
            &raquo;
	<?php endforeach; ?>
	
	<?php if(count($this->page->getBreadCrumNode()) >= 0):?>
		<?php echo $this->htmlLink($this->page->getHref(),$this->string()->truncate ($this->page->title,15) ); ?>
	<?php endif; ?>   

	<div> 
		 <div style="padding-left: 0px;">
		 <?php if(!$this->can_rate): ?>
		    <div style="color: #ccc;" id='mess_rate'></div>
		    <?php endif; ?>  
		 </div>
	</div>

</div>

 <script type="text/javascript">
    var img_star_full = "application/modules/Ynwiki/externals/images/star_full.png";
    var img_star_partial = "application/modules/Ynwiki/externals/images/star_part.png";
    var img_star_none = "application/modules/Ynwiki/externals/images/star_none.png";  
    
    function rating_mousehover(rating) {
        for(var x=1; x<=5; x++) {
          if(x <= rating) {
            $('rate_'+x).src = img_star_full;
          } else {
            $('rate_'+x).src = img_star_none;
          }
        }
    }
     function canNotRate(status) {
         if(status == 1)
         {
                //$('mess_rate').innerHTML = "<?php echo $this->translate('You have just rated this page or you are the creator of this page!') ?>";
         }
         else if(status == 2)
         {
                //$('mess_rate').innerHTML = "<?php echo $this->translate('You have to log in to rate this page!') ?>";
         }
         else
         {
             $('mess_rate').innerHTML = "";
         }
     }
    function rating_mouseout() {
        for(var x=1; x<=5; x++) {
          if(x <= <?php echo $this->page->rate_ave ?>) {
            $('rate_'+x).src = img_star_full;
          } else if(<?php echo $this->page->rate_ave ?> > (x-1) && x > <?php echo $this->page->rate_ave ?>) {
            $('rate_'+x).src = img_star_partial;
          } else {
            $('rate_'+x).src = img_star_none;
          }
        }
    }
    function rate(rates){
        $('page_rate').onmouseout = null;
        Smoothbox.open(en4.core.baseUrl + 'wiki/rate/pageId/<?php echo $this->page->getIdentity();?>/rates/'+rates);
      }
  
</script>
