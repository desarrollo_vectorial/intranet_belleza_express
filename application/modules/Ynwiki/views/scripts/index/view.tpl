<?php
	$attachment = $this->page->getAttachments();
	$viewer     = Engine_Api::_()->user()->getViewer();
	$child_wiki   = $this->partial('_childs.tpl', array('page'=>$this->page));
	$page_details = $this->partial('_page_detail.tpl', array('page' => $this->page,'owner' => $this->owner, 'viewer' => $this->viewer,'can_rate' => $this->can_rate));
?>

    
<div class="ynwiki-content">
    <?php echo $page_details; ?>

    <ul class='blogs_entrylist'>
        <div class="blog_entrylist_entry_short_description rich_content_body">
          <?php echo $this->page->description ?>
        </div>
        <div class="blog_entrylist_entry_body rich_content_body">
          <?php echo $this->page->body ?>
        </div>
      </li>
    </ul>

    <?php echo $this->translate("Tags: ");
    if(count($this->tags) > 0){
      foreach($this->tags as $tag){ ?>
        <a href='javascript:void(0);'onclick='javascript:tagAction(<?php echo $tag->tag_id; ?>);' ><?php echo $tag->text?></a>
      <?php }
    }else{
      echo $this->translate("None");
    } ?>

    <div style="padding-top: 5px;">
      <strong>
      <?php echo $this->translate(array('%s Attached File', '%s Attached Files', count($attachment)), $this->locale()->toNumber(count($attachment))) ;?>
      
      <?php if($this->page->authorization()->isAllowed($viewer,'edit') && $viewer->getIdentity() > 0){ ?>

        (<?php echo $this->htmlLink(array('action'=>'attach', 'pageId'=>$this->page->getIdentity(), 'route'=>'ynwiki_general', 'reset'=>true,), $this->translate('Attach File'), array()); ?>)
      <?php } ?>
      <br/>
      </strong>
    </div>

    <?php if(count($attachment) > 0){ ?>
      <div class="ynwiki_content">
        <table class="generic-table-a">
          <tr>
            <th><?php echo $this->translate("Name")?></th>
            <th><?php echo $this->translate("Size")?></th>
            <th><?php echo $this->translate("Creator")?></th>
            <th><?php echo $this->translate("Created Date")?></th>
            <th></th>
          </tr>
          <?php
          foreach($attachment as $attach){
            $user = Engine_Api::_()->getItem('user', $attach->user_id);
            $file =  Engine_Api::_()->getItem('storage_file', $attach->file_id); ?>
            <tr>
              <td><a href="./application/modules/Ynwiki/externals/scripts/download.php?f=<?php echo $file->storage_path ?>&fi=<?php echo $this->page->page_id?>&fc='<?php echo $attach->title ?>'"> <?php echo $attach->title ?> </a></td>
              <td>
                <?php if($file->size < 1024*1024){
                  echo round($file->size/(1024),2)." Kb";
                }else{
                  echo round($file->size/(1024*1024),2)." Mb";
                } ?>
              </td>
              <td><?php echo $user;?></td>
              <td><?php echo $attach->creation_date;?></td>
              <td>
                <?php if($this->page->authorization()->isAllowed($viewer,'edit') && $viewer->getIdentity() > 0){ ?>
                  <?php echo $this->htmlLink(array('action'=>'delete-attach', 'attachId'=>$attach->attachment_id, 'route'=>'ynwiki_general', 'reset'=>true,), $this->translate('delete'), array('class' => 'warn-link smoothbox')); ?>
                <?php } ?>
              </td>
            </tr>
          <?php } ?>
        </table>
      </div>
    <?php } ?>

    <?php echo $child_wiki; ?>
    
    <?php //echo $this->action("list", "comment", "core", array( "type" => "ynwiki_page", "id" => $this->page->getIdentity())) ?>


</div>

<script type="text/javascript">
  var tagAction = function(tag){
    window.location = en4.core.baseUrl + 'wiki/listing?tag=' + tag;
  }
</script>
