<?php
	$page_details = $this->partial('_page_detail.tpl', array('page' => $this->page,
															'owner' => $this->owner,
															'viewer' => $this->viewer,
															'lastUpdate' => $this->lastUpdate,
															'lastUpdateOwner' => $this->lastUpdateOwner,
															'can_rate' => $this->can_rate
															)
								);
?>

<div class="ynwiki-content">
	<?php echo $page_details; ?>

	<div>
		<?php echo $this->form->render($this) ?>
	</div>
</div>

<script type="text/javascript">
	function removeSubmit() {
		$('buttons-wrapper').hide();
	}
</script>
