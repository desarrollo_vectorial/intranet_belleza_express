 <?php
$page_details = $this->partial('_page_detail.tpl', array('page' => $this->page,
														'owner' => $this->owner,
														'viewer' => $this->viewer,
														'lastUpdate' => $this->lastUpdate,
														'lastUpdateOwner' => $this->lastUpdateOwner,
														'can_rate' => $this->can_rate
														)
							);

?>
<div class="ynwiki-content">
		<?php echo $page_details; ?>

		<h4 style="border: none;">  
		<?php echo $this->htmlLink(array(
		                  'action' => 'history',
		                  'pageId' => $this->page->getIdentity(),
		                  'route' => 'ynwiki_general',
		                  'reset' => true,
		                ), "&laquo; ".$this->translate('View Page History'), array(
		                )) ?>
		</h4>

		<?php echo $this->revision->body;?>
</div>