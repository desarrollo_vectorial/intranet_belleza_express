<script type="text/javascript">
	var pageAction = function(page) {
		$('page').value = page;
		$('filter_form').submit();
	}
	var tagAction = function(tag) {
		window.location = en4.core.baseUrl + 'wiki/listing?tag=' + tag;
	}
</script>
<div class='layout_middle'>
	<div class="middle-content-container">
		<?php if( $this->pages->getTotalItemCount() > 0 ): ?>
			<ul class="ynwiki_browse">
				<?php foreach( $this->pages as $item ): ?>
					<?php if(Engine_Api::_()->ynwiki()->checkparentallow($item, $viewer,'view') == true): ?>
		
						<?php echo $this->partial('_wikiListBox.tpl', array('item' => $item, 'show_options' => 'favorite')); ?>
		
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		<?php elseif($this->search): ?>
			<div class="tip">
				<span><?php echo $this->translate('_wiki_You do not have any pages that match your search criteria.');?></span>
			</div>
		<?php else: ?>
			<div class="tip">
				<span><?php echo $this->translate('_wiki_You do not have any pages.');?></span>
			</div>
		<?php endif; ?>

		<?php echo $this->paginationControl($this->pages, null, null, array('pageAsQuery' => true, 'query' => $this->formValues, )); ?>
	</div>
</div>

<script type="text/javascript">
	function showhide(id) {
		if (document.getElementById) {
			obj = document.getElementById(id);
			if (obj.style.display == "none") {
				obj.style.display = "";
				$('showlink').style.display = "none";
				$('hidelink').style.display = "";
			} else {
				obj.style.display = "none";
				$('showlink').style.display = "";
				$('hidelink').style.display = "none";
			}
		}
	}
</script>