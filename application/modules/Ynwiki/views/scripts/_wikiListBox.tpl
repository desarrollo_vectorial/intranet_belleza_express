  <li>
      <div class='ynwiki_browse_photo'>
        <?php echo $this->htmlLink($this->item->getHref(), $this->itemPhoto($this->item)) ?>
      </div>
      <div class='ynwiki_browse_info'>
        <p class='ynwiki_browse_info_title'>
          <?php echo $this->htmlLink($this->item->getHref(), $this->item->getTitle()) ?>
        </p>
        <?php //foreach($this->item->getBreadCrumNode() as $node): ?>
            <?php //echo $this->htmlLink($node->getHref(), $node->title) ?>
            <!-- &raquo; -->
            <?php //endforeach; ?>
            <?php //echo $this->htmlLink($this->item->getHref(), $this->item->title) ?>
      </div>
      <div class='ynwiki_browse_info_blurb'>
          <?php echo $this->string()->truncate($this->string()->stripTags($this->item->body), 300) ?>
      </div>          
      <div class='ynwiki_browse_info_date_created'>
		<div class="label"><?php echo $this->translate('Create by <b>%1$s</b> ', $this->htmlLink($this->item->getOwner()->getHref(), $this->item->getOwner()->getTitle(), array('target'=>'_top')));?></div>
      	<span class="date"><?php echo $this->timestamp($this->item->creation_date) ?></span>
        <!--<div class="view-count">
      		<?php /*echo $this->item->view_count; */?> VISITAS
        </div>    -->
      </div>
      
      <?php $revision = false; //$this->item->getLastUpdated(); ?>
      
      <?php if($revision): ?>
	      <div class='ynwiki_browse_info_date_modified'>	          
	          <?php if($revision):  $owner =  Engine_Api::_()->getItem('user', $revision->user_id);?>
	      		<div class="label"><?php echo $this->translate('Last updated by <b>%1$s</b> ',$this->htmlLink($owner->getHref(), $owner->getOwner()->getTitle(), array('target'=>'_top'))); ?></div>
	     		<span class="date"><?php echo $this->timestamp($revision->creation_date) ?></span>
		         <?php echo $this->htmlLink(array(
		                  'action' => 'compare-versions',
		                  'pageId' => $this->item->page_id,
		                  'route' => 'ynwiki_general',
		                  'reset' => true,
		                ), $this->translate("view change"), array(
		                )) ?>
	       	  <?php endif;?>  
	      </div>
      <?php endif; ?>
      
      <?php if(isset($this->show_options) && $this->show_options): ?>
	      <div class='ynwiki_browse_options'>
	      	
	      	  <?php if($this->show_options == 'follow'): ?>	
			      <?php echo $this->htmlLink(array(
			      'action' => 'un-follow',
			      'pageId' => $this->item->getIdentity(),
			      'route' => 'ynwiki_general',
			      'reset' => true,
			      ), $this->translate('Unfollow'), array(
			      'class' => 'buttonlink smoothbox icon_ynwiki_unfollow',
			      )) ?>
		      <?php endif; ?>
		      
	      	  <?php if($this->show_options == 'favorite'): ?>			      
			      <?php echo $this->htmlLink(array(
			      'action' => 'un-favourite',
			      'pageId' => $this->item->getIdentity(),
			      'route' => 'ynwiki_general',
			      'reset' => true,
			      ), $this->translate('Unfavourite'), array(
			      'class' => 'buttonlink smoothbox icon_ynwiki_unfavourite',
			      )) ?>	
		      <?php endif; ?>	      
	      </div>
      <?php endif; ?>
  </li>
