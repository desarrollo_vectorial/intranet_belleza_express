
<?php $childPages = $this->page->getChilds();
$viewer = Engine_Api::_()->user()->getViewer();?>
<div>
  <strong>
    <?php echo $this->translate(array('%s Child Page', '%s Child Pages', count($childPages)), $this->locale()->toNumber(count($childPages))) ;?>
    
    <?php if($this->page->authorization()->isAllowed($viewer,'edit') && $viewer->getIdentity() > 0 && Zend_Controller_Action_HelperBroker::getStaticHelper('requireAuth')->setAuthParams('ynwiki_page', null, 'create')->checkRequire()):?>
      (<?php echo $this->htmlLink(array( 'action'=>'create', 'fromPageId'=>$this->page->getIdentity(), 'route'=>'ynwiki_general', 'reset'=>true, ), $this->translate('Add A Child Page'), array()); ?>)
    <?php endif;?>

  </strong>
</div>

<ul class="ynwiki_browse">
  <?php foreach($childPages  as $child): ?>
    <li>
      <div class='ynwiki_browse_photo'>
        <?php echo $this->htmlLink($child->getHref(), $this->itemPhoto($child)) ?>
      </div>
      <div class='ynwiki_browse_info'>
        <p class='ynwiki_browse_info_title'>
          <?php echo $this->htmlLink($child->getHref(), $child->title) ?>
        </p>
      </div>
    </li>
  <?php endforeach;?>
</ul>
