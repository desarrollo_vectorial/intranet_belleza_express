<?php if( count($this->navigation) > 0 ): ?>
    <div class="menu-tabs-generic" id="num-tabs3">
        <?php
        // Render the menu
        echo $this->navigation()
            ->menu()
            ->setContainer($this->navigation)
            ->render();
        ?>
    </div>
<?php endif; ?>