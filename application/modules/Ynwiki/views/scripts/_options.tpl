<script type="text/javascript">
   function follow_page(){
       var request = new Request.JSON({
            'method' : 'post',
            'url' :  en4.core.baseUrl + 'wiki/follow',
            'data' : {
            	  'pageId' : <?php echo $this->page->page_id?>
            },'onComplete':function(responseObject){
                obj = document.getElementById('follow_id');
                obj.innerHTML = '<a href="javascript:;" onclick="unfollow_page()">' + '<?php echo $this->translate("Unfollow")?>' + '</a>';
            }
        });
        request.send();
   }
   function unfollow_page(){
       var request = new Request.JSON({
            'method' : 'post',
            'url' :  en4.core.baseUrl + 'wiki/un-follow-ajax',
            'data' : {
                'pageId' : <?php echo $this->page->page_id?>
            },'onComplete':function(responseObject){
                obj = document.getElementById('follow_id');
                obj.innerHTML = '<a href="javascript:;" onclick="follow_page()">' + '<?php echo $this->translate("Follow")?>' + '</a>';
            }
        });
        request.send();
   }
   function favourite_page(){
       var request = new Request.JSON({
            'method' : 'post',
            'url' :  en4.core.baseUrl + 'wiki/favourite',
            'data' : {
                'pageId' : <?php echo $this->page->getIdentity()?>
            },'onComplete':function(responseObject){
                obj = document.getElementById('favourite_id');
                obj.innerHTML = '<i class="botoneswiki unfavourite"></i><a href="javascript:;" onclick="unfavourite_page()">' + '<?php echo $this->translate("Quitar de favoritos")?>' + '</a>';
            }
        });
        request.send();
   }
   function unfavourite_page(){
       var request = new Request.JSON({
            'method' : 'post',
            'url' :  en4.core.baseUrl + 'wiki/un-favourite-ajax',
            'data' : {
                'pageId' : <?php echo $this->page->getIdentity()?>
            },'onComplete':function(responseObject){
                obj = document.getElementById('favourite_id');
                obj.innerHTML = '<i class="botoneswiki favourite"></i><a href="javascript:;" onclick="favourite_page()">' + '<?php echo $this->translate("Marcar como favorito")?>' + '</a>';
            }
        });
        request.send();
   }
   function confirmDelete(){
     <?php if(count($this->page->getChilds())>0): ?>
      return confirm("<?php echo $this->translate('Todas las subpáginas serán eliminadas. ¿Está seguro que desea eliminar esta página?'); ?>")
    <?php else:?>
   	  return confirm("<?php echo $this->translate('¿Está seguro que desea eliminar esta página?'); ?>")
    <?php endif; ?>
   }
 </script>

<?php $viewer = Engine_Api::_()->user()->getViewer(); ?>
<div class="main-picture"><?php echo $this->itemPhoto($this->page)?></div>

<div class="wiki-name"><?php echo $this->page->getTitle(); ?></div>

<div class="options-list">
  <ul>
    <?php /*if(!$this->page->isOwner($viewer) && $viewer->getIdentity() > 0):?>
      <?php if( !$this->page-> membership() -> isMember($viewer)):?>
        <li>
          <i class="botoneswiki join"></i>
          <?php echo $this->htmlLink(array( 'route' => 'ynwiki_general', 'action' => 'join', 'pageId' => $this->page->getIdentity(), 'fromPageId' => $this->page->parent_page_id, 'format' => 'smoothbox'), $this->translate('Join Wiki Space'), array()); ?>
        </li>
      <?php else: ?>
        <li>
          <i class="botoneswiki leave"></i>
          <?php echo $this->htmlLink(array('route' => 'ynwiki_general','action' => 'leave', 'pageId' => $this->page->getIdentity(),'fromPageId' => $this->page->parent_page_id,'format' => 'smoothbox'),$this->translate('Leave Wiki Space'), array()); ?>
        </li>
      <?php endif;?>
    <?php endif; */?>

    <?php if($this->page->authorization()->isAllowed($viewer,'edit') ):?>
      <li>
        <i class="botoneswiki edit"></i>
        <?php echo $this->htmlLink(array('action' => 'edit','pageId' => $this->page->getIdentity(),'fromPageId' => $this->page->parent_page_id,'route' => 'ynwiki_general','reset' => true,), $this->translate('Edit'), array()) ?>
      </li>
    <?php endif;?>

	<!--
    <?php if($this->page->authorization()->isAllowed($viewer,'view') && $viewer->getIdentity() > 0):?>
      <li id = "follow_id">
        <?php if($this->page->checkFollow()):  ?>
          <i class="botoneswiki follow"></i>
          <a href="javascript:;" onclick="follow_page()"><?php echo $this->translate('Follow')?></a>
        <?php else: ?>
          <i class="botoneswiki unfollow"></i>
          <a href="javascript:;" onclick="unfollow_page()"><?php echo $this->translate('Unfollow')?></a>
        <?php endif; ?>
      </li>
    <?php endif; ?>
    -->

    <?php if($this->page->authorization()->isAllowed($viewer,'view') && $viewer->getIdentity() > 0):?>
      <li id = "favourite_id">
        <?php if($this->page->checkFavourite()): ?>
          <i class="botoneswiki favourite"></i>
          <a href="javascript:;" onclick="favourite_page()"><?php echo $this->translate('Marcar como favorito')?></a>
        <?php else: ?>
          <i class="botoneswiki unfavourite"></i>
          <a href="javascript:;" onclick="unfavourite_page()"><?php echo $this->translate('Quitar de favoritos')?></a>
        <?php endif; ?>
      </li>
    <?php endif;?>

    <?php if($this->page->authorization()->isAllowed($viewer,'edit') && $viewer->getIdentity() > 0 && Zend_Controller_Action_HelperBroker::getStaticHelper('requireAuth')->setAuthParams('ynwiki_page', null, 'create')->checkRequire()):?>
      <li>
        <i class="botoneswiki add"></i>
        <?php echo $this->htmlLink(array('action' => 'create','fromPageId' => $this->page->getIdentity(),'route' => 'ynwiki_general','reset' => true,), $this->translate('Add A Child Page'), array()) ?>
      </li>
    <?php endif;?>


    <?php if($this->page->authorization()->isAllowed($viewer,'edit') && $viewer->getIdentity() > 0):?>
      <li>
        <i class="botoneswiki attach"></i>
        <?php echo $this->htmlLink(array('action' => 'attach','pageId' => $this->page->getIdentity(),'route' => 'ynwiki_general','reset' => true,), $this->translate('Attach File'), array()) ?>
      </li>
    <?php endif;?>

    <?php if($this->page->authorization()->isAllowed($viewer,'delete') && $viewer->getIdentity() > 0):?>
      <li>
        <i class="botoneswiki delete"></i>
        <?php echo $this->htmlLink(array('action' => 'delete','pageId' => $this->page->getIdentity(),'route' => 'ynwiki_general','reset' => true,), $this->translate('Delete'), array('onclick' => "return confirmDelete();")) ?>
      </li>
    <?php endif;?>

	<!--
    <?php if($this->page->authorization()->isAllowed($viewer,'edit') && $viewer->getIdentity() > 0): ?>
      <li>
        <i class="botoneswiki move"></i>
        <?php echo $this->htmlLink(array('action' => 'move-location', 'pageId' => $this->page->getIdentity(), 'route' => 'ynwiki_general', 'reset' => true,), $this->translate('Move Location'), array()) ?>
      </li>
    <?php endif;?>
   -->

    <?php if($this->page->authorization()->isAllowed($viewer,'restrict') && $viewer->getIdentity() > 0): ?>
      <li>
        <i class="botoneswiki restrict"></i>
        <?php echo $this->htmlLink(array('action' => 'set-permission', 'pageId' => $this->page->getIdentity(), 'route' => 'ynwiki_general', 'reset' => true,), $this->translate('Restrict'), array()) ?>
      </li>
    <?php endif;?>

    <?php if(Engine_Api::_()->getApi('settings', 'core')->getSetting('ynwiki.print', 0) == 1 || $viewer->getIdentity() > 0): ?>
      <li class="ynwiki_print">
        <i class="botoneswiki print"></i>
        <a href="javascript:;" onclick="window.open('<?php echo $this->page->getPrintHref();?>', 'mywindow', 'location=1,status=1,scrollbars=1,width=900,height=700')" > <?php echo $this->translate('Print');?> </a>
      </li>
    <?php endif;?>

    <?php if(Engine_Api::_()->getApi('settings', 'core')->getSetting('ynwiki.download', 0) == 1 || $viewer->getIdentity() > 0): ?>
      <li class="ynwiki_pdf">
        <i class="botoneswiki download"></i>
        <?php echo $this->htmlLink(array('action' => 'download','pageId' => $this->page->getIdentity(),'route' => 'ynwiki_general','reset' => true,), 
                   $this->translate('Download As PDF'), array()); ?>
      </li>
    <?php endif;?>

    <li>
      <i class="botoneswiki history"></i>
      <?php echo $this->htmlLink(array( 'action' => 'history', 'pageId' => $this->page->getIdentity(), 'route' => 'ynwiki_general', 'reset' => true, ), $this->translate('View History'), array()) ?></li>
    <li>
      <i class="botoneswiki report"></i>
      <?php echo $this->htmlLink(array('action' => 'report', 'pageId' => $this->page->getIdentity(), 'route' => 'ynwiki_general', 'reset' => true, ), $this->translate('Report'), array()); ?>
    </li>
    <!--<li><?php //echo $this->translate("PAGINA ID");  echo $this->page->getIdentity();?></li>-->
  </ul>
</div>
