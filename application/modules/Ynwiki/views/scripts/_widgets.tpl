<?php
$page_list = array();
if (isset($this->arr_pages)) {
	$page_list = $this->arr_pages;
} else if (isset($this->paginator)) {
	$page_list = $this->paginator;
}
if (count($page_list)<=0) { // no page
	if (isset($this->no_pages_message) && $this->no_pages_message) { ?>
		<div class="tip">
			<span><?php echo $this->no_pages_message;?></span>
		</div>
	<?php
	}
	return;
} ?>

<div class = '<?php echo $this->css ?>' style="margin-bottom: 15px; overflow: auto;">
	<ul class="ynwiki_browse">
		<?php foreach($page_list as $page): ?>			
			
			<?php echo $this->partial('_wikiListBox.tpl', array('item' => $page, 'show_options' => false)); ?>
			
		<?php endforeach;?>
	</ul>
</div>