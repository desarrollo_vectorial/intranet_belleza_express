<?php
/**
 * YouNet
 *
 * @category   Application_Extensions
 * @package    wiki
 * @copyright  Copyright 2011 YouNet Developments
 * @license    http://www.modules2buy.com/
 * @version    $Id: menu wikis
 * @author     Minh Nguyen
 */
class Ynwiki_Widget_MenuPagesController extends Engine_Content_Widget_Abstract
{
  protected $_navigation;
  
  public function indexAction(){

	//Get navigation
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('ynwiki_main');
    if( count($this->view->navigation) == 1 ) {
      $this->view->navigation = null;
    }	
  }
}
