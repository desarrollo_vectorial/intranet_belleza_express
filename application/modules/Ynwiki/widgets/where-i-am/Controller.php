<?php
class Ynwiki_Widget_WhereIAmController extends Engine_Content_Widget_Abstract{
    public function indexAction(){
		$this -> view -> header_image  = Engine_Api::_() -> getApi('settings', 'core') -> getSetting('ynwiki.header-image');
		$this -> view -> show_header_image = true;    	
    }
}