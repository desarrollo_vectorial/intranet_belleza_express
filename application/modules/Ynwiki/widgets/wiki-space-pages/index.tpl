<?php
  $page_list = array();
  if (isset($this->arr_pages)) {
    $page_list = $this->arr_pages;
  } else if (isset($this->paginator)) {
    $page_list = $this->paginator;
  }
  if (count($page_list) <= 0) { 
    if (isset($this->no_pages_message) && $this->no_pages_message) {  
        echo '<div class="tip"><span>'.$this->no_pages_message.'</span></div>';
    }
    return;
  }
?>

  <div class = '<?php echo $this->css ?>'>
    <ul class="ynwiki_browse">
      <?php $viewer = Engine_Api::_()->user()->getViewer(); ?>
      <?php foreach($page_list as $page): ?>
		<?php if(Engine_Api::_()->ynwiki()->checkparentallow($page, $viewer,'view') == true): ?>
        
        	<?php echo $this->partial('_wikiListBox.tpl', array('item' => $page, 'show_options' => false)); ?>

        <?php endif; ?>
      <?php endforeach;?>
    </ul>
  </div>

  <div class="list-more" >
    <?php echo $this->htmlLink(array( 'action'=> 'listing', 'route'=> 'ynwiki_general', 'reset'=> true ), $this->translate('More Wiki Spaces')." &raquo;", array( )); ?>
  </div>
