<style>
.layout_ynwiki_help_navigator .ynwiki_quicklinks_menu{ border: 1px solid #223777; padding-bottom: 5px; }
.layout_ynwiki_help_navigator .ynwiki_quicklinks_menu li{ border-bottom: 1px solid #a7a5a5; }
.layout_ynwiki_help_navigator .ynwiki_quicklinks_menu li>a{ color: #a7a5a5; }
.active { font-weight: bold; }
</style>


<ul class="ynwiki_quicklinks_menu">
    <?php foreach($this->items as $id=>$title): ?>
    <li>
        <a class = "<?php echo $id == $this->active?"active":""?>" href="<?php echo $this->url(array('controller'=>'help','action'=>'index','id'=>$id)) ?>"><?php echo $title ?></a>
    </li>
    <?php endforeach; ?>
</ul>