<div class="side-menu-box ynwiki-list-tags">

	<div class="title">
		<img src="../public/admin/icon_etiqueta_noticias.png" alt="">
		<span><strong>Etiquetas</strong></span>
	</div>

    <div class = "radcodes_popular_tags">
        <div id="script">
			<?php $index = 0; $flag = false; ?>
			<?php foreach($this->tags as $tag): $index ++;?>
				<?php if(trim($tag->text) != ""): ?>
	                            
	                <?php if($index > 25 && $flag == false): $flag = true;?>
	                    <p id="showlink" style="display: block; font-weight: bold">[<a id = 'title' href="javascript:;" onclick="showhide('hide'); return(false);">Ver todos</a>]</p>
	                    <span id="hide" style="display:none;font-size: 8pt;"></span>
	                <?php  endif;?>
                   
	                <span class="tag" style="<?php if($tag->count > 99 && $tag->count < 599): echo "font-size:".($tag->count/80 + 8)."pt"; elseif($tag->count > 599): echo "font-size: 14pt"; endif; ?>">
	                	<a  href='javascript:void(0);'onclick='javascript:tagAction(<?php echo $tag->tag_id; ?>);' ><?php echo $tag->text?></a>
	                </span>
                
				<?php endif; ?>
			<?php endforeach; ?>
                    
            <?php if($flag == true):?>
            	<p id="hidelink" style="display: none;font-weight: bold">[<a id = 'title' href="javascript:;" onclick="showhide('hide'); return(false);">Ocultar todos</a>]</p>
            <?php endif; ?>
        </div>
    </div>
    
    <script type="text/javascript">
    var tagAction =function(tag){
        window.location = en4.core.baseUrl + 'wiki/listing?tag=' + tag;
    };
    var showhide = function(id)
    {
        if (document.getElementById(id))
        {
            obj = document.getElementById(id);
            if (obj.style.display == "none")
            {
                obj.style.display = "";
                $('showlink').style.display = "none";
                $('hidelink').style.display = "";
            } else
            {
                obj.style.display = "none";
                $('showlink').style.display = "";
                $('hidelink').style.display = "none";
            }
        }
    };
    </script>


</div>