<style>
    .layout_ynwiki_listing_pages h3{
        display: none;
    }
    .ynwiki_browse{
        margin-top: 20px;
    }
    ul.ynwiki_browse li{
        margin-right: 14px !important;
    }
</style>
  <script type="text/javascript">
  var pageAction =function(page){
    $('page').value = page;
    $('filter_form').submit();
  }
  /*var tagAction =function(tag){
    $('tag').value = tag;
    $('filter_form').submit();
  }*/
</script>
<div class='layout_middle'> 

  <div class="middle-content-container">
  <?php if( $this->pages->getTotalItemCount() > 0 ): ?>
    <ul class="ynwiki_browse">
    	
      <?php $viewer = Engine_Api::_()->user()->getViewer(); ?>      	
		<?php foreach( $this->pages as $item ): ?>      
        	<?php if(Engine_Api::_()->ynwiki()->checkparentallow($item, $viewer,'view') == true): ?>
		
		      <?php echo $this->partial('_wikiListBox.tpl', array('item' => $item, 'show_options' => false)); ?>
       
            <?php endif; ?>
      <?php endforeach; ?>
    </ul>

  <?php elseif($this->search): ?>
    <div class="tip">
      <span><?php echo $this->translate('_wiki_You do not have any pages that match your search criteria.');?></span>
    </div>
  <?php else: ?>
    <div class="tip">
      <span><?php echo $this->translate('_wiki_You do not have any pages.');?></span>
    </div>
  <?php endif; ?>

  <?php echo $this->paginationControl($this->pages, null, null, array(
    'pageAsQuery' => true,
    'query' => $this->formValues,
  )); ?>


  </div>
</div>