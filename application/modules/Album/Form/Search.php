<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Search.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Album_Form_Search extends Engine_Form
{
  public function init()
  {
    $this
      ->setAttribs(array(
        'id' => 'filter_form',
        'class' => 'global_form_box form-elements',
      ))
      ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()));

    parent::init();
    
    $this->addElement('Text', 'search', array(
      'placeholder' => 'Título',
      'class'       => 'form_titulo'
    ));

    $this->addElement('Select', 'sort', array(
      //'label' => 'Browse By:',
      'multiOptions' => array(
        'recent' => 'Most Recent',
        'popular' => 'Most Popular',
      ),
      'class'    => 'form_select'
    ));
    
    // prepare categories
    $categories = Engine_Api::_()->getDbtable('categories', 'album')->getCategoriesAssoc();
    if( count($categories) > 0 ) {
      $this->addElement('Select', 'category_id', array(
        //'label' => 'Category',
        'multiOptions' => $categories,
        'class'    => 'form_select'
      ));
    }

    $this->addElement('Button', 'submit', array(
      'order'  => 104,
      'label'  => 'Buscar',
      'type'   => 'submit',
      'class'  => 'form_boton',
      'ignore' => true,
    ));
  }
}