<?php if( count($this->quickNavigation) > 0 ): ?>
  <div class="quick-links--new-photo">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->quickNavigation)
        ->render();
    ?>
  </div>
<?php endif; ?>