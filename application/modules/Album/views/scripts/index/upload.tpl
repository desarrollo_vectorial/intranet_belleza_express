<?php $this ->headScript()->appendFile($this->layout()->staticBaseUrl . 'externals/ckeditor_4/ckeditor.js'); ?>
<div class="form-general">
  <script type="text/javascript">
    var updateTextFields = function(){
      var fieldToggleGroup = ['#title-wrapper', '#category_id-wrapper', '#description-wrapper', '#search-wrapper',
                              '#auth_view-wrapper',  '#auth_comment-wrapper', '#auth_tag-wrapper'];
          fieldToggleGroup = $$(fieldToggleGroup.join(','))
      if ($('album').get('value') == 0) {
        fieldToggleGroup.show();
      } else {
        fieldToggleGroup.hide();
      }
    }
    en4.core.runonce.add(updateTextFields);
  </script>
  <?php echo $this->form->render($this) ?>
  <script type="text/javascript">
    $$('.core_main_album').getParent().addClass('active');
  </script>
</div>

<script>
    window.onload = function(){
      CKEDITOR.replace( 'description', {
        filebrowserBrowseUrl: '/externals/ckfinder_2/ckfinder.html',
        filebrowserUploadUrl: '/externals/ckfinder_2/core/connector/php/connector.php?command=QuickUpload&type=Files'
      });
    }
  </script>