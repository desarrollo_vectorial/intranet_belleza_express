<?php $this ->headScript()->appendFile($this->layout()->staticBaseUrl . 'externals/ckeditor_4/ckeditor.js'); ?>
<div class="form-general">
    <?php echo $this->form->render(); ?>

    <script type="text/javascript">
        $('.core_main_album').getParent().addClass('active');
    </script>
</div>


<script>
    window.onload = function(){
        CKEDITOR.replace( 'description', {
            filebrowserBrowseUrl: '/externals/ckfinder_2/ckfinder.html',
            filebrowserUploadUrl: '/externals/ckfinder_2/core/connector/php/connector.php?command=QuickUpload&type=Files'
        });
    }
</script>