<?php

class Article_IndexController extends Core_Controller_Action_Standard
{
    // Subject is set in the init function so as to be available easily
    // to all other methods in the same controller.
    public function init(){}

    public function indexAction(){}

    public function createAction(){
        if (! $this->_helper->requireUser()->isValid()) {
            return;
        }
        if (! $this->_helper->requireAuth()->setAuthParams('article', null, 'create')->isValid()) {
            return;
        }

        // Render
        $this->_helper->content->setEnabled();
        // $this->_helper->content->setNoRender()

        // Prepare form
        $this->view->form = $form = new Article_Form_Create();

        // If not post or form not valid, return
        if (! $this->getRequest()->isPost()) {
            return;
        }

        if (! $form->isValid($this->getRequest()
            ->getPost())) {
            return;
        }

        // Process
        $table = Engine_Api::_()->getItemTable('article_article');
        $db = $table->getAdapter();
        $db->beginTransaction();

        try {
            // Create article
            $viewer = Engine_Api::_()->user()->getViewer();
            $values = array_merge($form->getValues(), array(
                'owner_type' => $viewer->getType(),
                'owner_id' => $viewer->getIdentity()
            ));

            $article = $table->createRow();
            $article->setFromArray($values);
            $article->save();

            // Guardar imagen destacada
            if (!empty($values['photo'])) {
                $article->guardarFoto($form->photo);
            }

            // Guardar imagen destacada
            /*if (!empty($values['photoint'])) {
                $article->guardarFotoInt($form->photoint);
            }*/

            // Auth
            $auth = Engine_Api::_()->authorization()->context;
            $roles = array(
                'owner',
                'owner_member',
                'owner_member_member',
                'owner_network',
                'registered',
                'everyone'
            );

            if (empty($values['auth_view'])) {
                $values['auth_view'] = 'everyone';
            }
            if (empty($values['auth_comment'])) {
                $values['auth_comment'] = 'everyone';
            }
            $viewMax = array_search($values['auth_view'], $roles);
            $commentMax = array_search($values['auth_comment'], $roles);

            foreach ($roles as $i => $role) {
                $auth->setAllowed($article, $role, 'view', ($i <= $viewMax));
                $auth->setAllowed($article, $role, 'comment', ($i <= $commentMax));
            }
            // Add tags
            $tags = preg_split('/[,]+/', $values['tags']);
            $article->tags()->addTagMaps($viewer, $tags);

            // Add activity only if article is published
            if ($values['published'] == 1) {
                $action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($viewer, $article, 'article_new');

                // make sure action exists before attaching the article to the activity
                if ($action) {
                    Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $article);
                }
            }
            // Commit
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }
        return $this->_helper->redirector->gotoRoute(array(
            'action' => 'manage'
        ));
    }

    public function browseAction(){
        // Prepare data
        $viewer = Engine_Api::_()->user()->getViewer();

        // Permissions
        $this->view->canCreate = $this->_helper->requireAuth()
            ->setAuthParams('article', null, 'create')
            ->checkRequire();

        // Make form
        // Note: this code is duplicated in the article.browse-search widget
        $this->view->form = $form = new Article_Form_Search();

        $form->removeElement('draft');
        if (! $viewer->getIdentity()) {
            $form->removeElement('show');
        }

        // Populate form
        $categories = Engine_Api::_()->getDbtable('categories', 'article')->getCategoriesAssoc();
        if (! empty($categories) && is_array($categories) && $form->getElement('category')) {
            $form->getElement('category')->addMultiOptions($categories);
        }

        // Process form
        $defaultValues = $form->getValues();
        if ($form->isValid($this->_getAllParams())) {
            $values = $form->getValues();
        } else {
            $values = $defaultValues;
        }
        $values['published'] = null;

        $this->view->formValues = array_filter($values);
        $values['published'] = "1";

        // Do the show thingy
        if (@$values['show'] == 2) {
            // Get an array of friend ids
            $table = Engine_Api::_()->getItemTable('user');
            $select = $viewer->membership()->getMembersSelect('user_id');
            $friends = $table->fetchAll($select);
            // Get stuff
            $ids = array();
            foreach ($friends as $friend) {
                $ids[] = $friend->user_id;
            }
            // unset($values['show']);
            $values['users'] = $ids;
        }

        $this->view->assign($values);

        // Get articles
        $paginator = Engine_Api::_()->getItemTable('article')->getArticlesPaginator($values);

        $items_per_page = Engine_Api::_()->getApi('settings', 'core')->article_page;
        // $paginator->setItemCountPerPage($items_per_page);

        $this->view->paginator = $paginator->setCurrentPageNumber($values['page']);

        if (! empty($values['category'])) {
            $this->view->categoryObject = Engine_Api::_()->getDbtable('categories', 'article')
                ->find($values['category'])
                ->current();
        }

        // Render
        $this->_helper->content->setEnabled();
        // $this->_helper->content->setNoRender()
    }

    public function manageAction(){
        // Permissions
        if (! $this->_helper->requireUser()->isValid()) {
            return;
        }
        // Render
        $this->_helper->content->setEnabled();
        // $this->_helper->content->setNoRender()

        // Prepare data
        $viewer = Engine_Api::_()->user()->getViewer();
        $this->view->form = $form = new Article_Form_Search();
        $this->view->canCreate = $this->_helper->requireAuth()
            ->setAuthParams('article', null, 'create')
            ->checkRequire();
        $form->removeElement('show');

        // Populate form
        $categories = Engine_Api::_()->getDbtable('categories', 'article')->getCategoriesAssoc();
        if (! empty($categories) && is_array($categories) && $form->getElement('category')) {
            $form->getElement('category')->addMultiOptions($categories);
        }

        // Process form
        $defaultValues = $form->getValues();
        if ($form->isValid($this->_getAllParams())) {
            $values = $form->getValues();
        } else {
            $values = $defaultValues;
        }
        $this->view->formValues = array_filter($values);
        $values['user_id'] = $viewer->getIdentity();

        // Get paginator
        $this->view->paginator = $paginator = Engine_Api::_()->getItemTable('article')->getArticlesPaginator($values);
        $items_per_page = Engine_Api::_()->getApi('settings', 'core')->article_page;
        // $paginator->setItemCountPerPage($items_per_page);
        $this->view->paginator = $paginator->setCurrentPageNumber($values['page']);
    }

    public function listAction(){
        // Preload info
        $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
        $this->view->owner = $owner = Engine_Api::_()->getItem('user', $this->_getParam('user_id'));
        Engine_Api::_()->core()->setSubject($owner);

        if (! $this->_helper->requireSubject()->isValid()) {
            return;
        }

        // Make form
        $form = new Article_Form_Search();
        // Process form
        $defaultValues = $form->getValues();
        if ($form->isValid($this->getRequest()
            ->getParams())) {
            $values = $form->getValues();
        } else {
            $values = $defaultValues;
        }
        $this->view->formValues = array_filter($values);
        $values['user_id'] = $owner->getIdentity();

        // Prepare data
        $articleTable = Engine_Api::_()->getDbtable('articles', 'article');

        // Get paginator
        $this->view->paginator = $paginator = Engine_Api::_()->getItemTable('article')->getArticlesPaginator($values);
        $items_per_page = Engine_Api::_()->getApi('settings', 'core')->article_page;
        $paginator->setItemCountPerPage($items_per_page);
        $this->view->paginator = $paginator->setCurrentPageNumber($values['page']);

        // Render
        $this->_helper->content->setEnabled();
        // $this->_helper->content->->setNoRender()
    }

    public function viewAction(){
        // Check permission
        $viewer = Engine_Api::_()->user()->getViewer();
        $article = Engine_Api::_()->getItem('article', $this->_getParam('article_id'));
        if ($article) {
            Engine_Api::_()->core()->setSubject($article);
        }

        
        if (! $this->_helper->requireSubject()->isValid()) {
            return;
        }
        
        if (! $this->_helper->requireAuth()->setAuthParams($article, $viewer, 'view')->isValid()) {
            return;
        }

        
        if (! $article || ! $article->getIdentity() || ($article->published && ! $article->isOwner($viewer))) {
            //return $this->_helper->requireSubject->forward();
        }        

        // Prepare data
        $articleTable = Engine_Api::_()->getDbtable('articles', 'article');
        $this->view->article = $article;
        $this->view->owner   = $owner = $article->getOwner();
        $this->view->viewer  = $viewer;        

        if (! $article->isOwner($viewer)) {
            $articleTable->update(array(
                'view_count' => new Zend_Db_Expr('view_count + 1')
            ), array(
                'article_id = ?' => $article->getIdentity()
            ));
        }

        // Get tags
        $this->view->articleTags = $article->tags()->getTagMaps();

        // Get category
        if (! empty($article->category_id)) {
            $this->view->category = Engine_Api::_()->getDbtable('categories', 'article')
                ->find($article->category_id)
                ->current();
        }

        // Get styles
        $table = Engine_Api::_()->getDbtable('styles', 'core');
        $style = $table->select()
            ->from($table, 'style')
            ->where('type = ?', 'user_article')
            ->where('id = ?', $owner->getIdentity())
            ->limit(1)
            ->query()
            ->fetchColumn();
        if (! empty($style)) {
            try {
                $this->view->headStyle()->appendStyle($style);
            } catch (Exception $e) {
                // silence any exception, exceptin in development mode
                if (APPLICATION_ENV === 'development') {
                    throw $e;
                }
            }
        }

        // Render
        $this->_helper->content->setEnabled();
    }

    public function publishAction(){
        $viewer = Engine_Api::_()->user()->getViewer();
        $article = Engine_Api::_()->getItem('article', $this->_getParam('article_id'));

        if ($article) {
            Engine_Api::_()->core()->setSubject($article);
        }

        // only owner can publish
        if ($viewer->getIdentity() != $article->owner_id) {
            return $this->_forward('requireauth', 'error', 'core');
        }

        if ($article->isPublished()) {
            return $this->_helper->redirector->gotoRoute(array(), 'article_manage', true);
        }

        $table = $article->getTable();
        $db = $table->getAdapter();
        $db->beginTransaction();

        try {
            $article->published = 1;
            $article->save();

            // Add activity only if article is published
            if ($article->isPublished()) {
                $action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($viewer, $article, 'article_new');
                if ($action != null) {
                    Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $article);
                }
            }

            $db->commit();
        }

        catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        return $this->_redirectCustom($article);
    }

    public function editAction(){
        $this->_helper->content->setEnabled();

        if (! $this->_helper->requireUser()->isValid()) {
            return;
        }

        $viewer = Engine_Api::_()->user()->getViewer();
        $article = Engine_Api::_()->getItem('article', $this->_getParam('article_id'));

        if (! Engine_Api::_()->core()->hasSubject('article')) {
            Engine_Api::_()->core()->setSubject($article);
        }
        if (! $this->_helper->requireSubject()->isValid()) {
            return;
        }
        if (! $this->_helper->requireAuth()
            ->setAuthParams($article, $viewer, 'edit')
            ->isValid()) {
            return;
        }

        // Get navigation
        //$this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('article_main');
        //$this->view->navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('article_main')->toArray();

        // Prepare form
        $this->view->form = $form = new Article_Form_Edit(array(
            'item' => $article
        ));

        // Populate form
        $form->populate($article->toArray());

        $tagStr = '';
        foreach ($article->tags()->getTagMaps() as $tagMap) {
            $tag = $tagMap->getTag();
            if (! isset($tag->text))
                continue;
            if ('' !== $tagStr)
                $tagStr .= ', ';
            $tagStr .= $tag->text;
        }
        $form->populate(array(
            'tags' => $tagStr
        ));
        $this->view->tagNamePrepared = $tagStr;

        $auth = Engine_Api::_()->authorization()->context;
        $roles = array(
            'owner',
            'owner_member',
            'owner_member_member',
            'owner_network',
            'registered',
            'everyone'
        );

        foreach ($roles as $role) {
            if ($form->auth_view) {
                if ($auth->isAllowed($article, $role, 'view')) {
                    $form->auth_view->setValue($role);
                }
            }

            if ($form->auth_comment) {
                if ($auth->isAllowed($article, $role, 'comment')) {
                    $form->auth_comment->setValue($role);
                }
            }
        }

        // Check post/form
        if (! $this->getRequest()->isPost()) {
            return;
        }
        if (! $form->isValid($this->getRequest()
            ->getPost())) {
            return;
        }

        // Process
        $db = Engine_Db_Table::getDefaultAdapter();
        $db->beginTransaction();

        try {
            // $customfieldform = $form->getSubForm('customField');
            // $customfieldform->setItem($article);
            // $customfieldform->saveValues();
            $values = $form->getValues();

            $article->setFromArray($values);
            $article->modified_date = date('Y-m-d H:i:s');
            $article->save();

            // Guardar imagen destacada
            if (!empty($values['photo'])) {
                $article->guardarFoto($form->photo);
            }

            // Guardar imagen destacada
            /*if (!empty($values['photoint'])) {
                $article->guardarFotoInt($form->photoint);
            }*/

            // Auth
            if (empty($values['auth_view'])) {
                $values['auth_view'] = 'everyone';
            }

            if (empty($values['auth_comment'])) {
                $values['auth_comment'] = 'everyone';
            }

            $viewMax = array_search($values['auth_view'], $roles);
            $commentMax = array_search($values['auth_comment'], $roles);

            foreach ($roles as $i => $role) {
                $auth->setAllowed($article, $role, 'view', ($i <= $viewMax));
                $auth->setAllowed($article, $role, 'comment', ($i <= $commentMax));
            }

            // handle tags
            $tags = preg_split('/[,]+/', $values['tags']);
            $article->tags()->setTagMaps($viewer, $tags);

            // insert new activity if article is just getting published
            $action = Engine_Api::_()->getDbtable('actions', 'activity')->getActionsByObject($article);
            if (count($action->toArray()) <= 0 && $values['draft'] == '0') {
                $action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($viewer, $article, 'article_new');
                // make sure action exists before attaching the blog to the activity
                if ($action != null) {
                    Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $article);
                }
            }

            // Rebuild privacy
            $actionTable = Engine_Api::_()->getDbtable('actions', 'activity');
            foreach ($actionTable->getActionsByObject($article) as $action) {
                $actionTable->resetActivityBindings($action);
            }

            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        return $this->_helper->redirector->gotoRoute(array(
            'action' => 'manage'
        ));
    }

    public function deleteAction(){
        $viewer = Engine_Api::_()->user()->getViewer();
        $article = Engine_Api::_()->getItem('article', $this->getRequest()
            ->getParam('article_id'));
        if (! $this->_helper->requireAuth()
            ->setAuthParams($article, null, 'delete')
            ->isValid()) {
            return;
        }

        // In smoothbox
        $this->_helper->layout->setLayout('default-simple');

        $this->view->form = $form = new Article_Form_Delete();

        if (! $article) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_("Article entry doesn't exist or not authorized to delete");
            return;
        }

        if (! $this->getRequest()->isPost()) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
            return;
        }

        $db = $article->getTable()->getAdapter();
        $db->beginTransaction();

        try {
            $article->delete();

            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        $this->view->status = true;
        $this->view->message = Zend_Registry::get('Zend_Translate')->_('Your article entry has been deleted.');
        return $this->_forward('success', 'utility', 'core', array(
            'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()
                ->assemble(array(
                'action' => 'manage'
            ), 'article_general', true),
            'messages' => Array(
                $this->view->message
            )
        ));
    }

    public function homeAction(){
        // Render
        $this->_helper->content->setEnabled();
        // $this->_helper->content->->setNoRender()
    }
}
