<?php

class Article_Plugin_Menus
{

    public function canViewArticles()
    {
        $viewer = Engine_Api::_()->user()->getViewer();
        
        // Must be able to view articles
        if (! Engine_Api::_()->authorization()->isAllowed('article', $viewer, 'view')) {
            return false;
        }
        
        return true;
    }

    public function canCreateArticles()
    {
        $viewer = Engine_Api::_()->user()->getViewer();
        
        // Must be able to view articles
        if (! Engine_Api::_()->authorization()->isAllowed('article', $viewer, 'create')) {
            return false;
        }
        
        return true;
    }

    public function onMenuInitialize_ArticleGutterList($row)
    {
        if (! Engine_Api::_()->core()->hasSubject()) {
            return false;
        }
        
        $subject = Engine_Api::_()->core()->getSubject();
        if ($subject instanceof User_Model_User) {
            $user_id = $subject->getIdentity();
        } else 
            if ($subject instanceof Article_Model_Article) {
                $user_id = $subject->owner_id;
            } else {
                return false;
            }
        
        // Modify params
        $params = $row->params;
        $params['params']['user_id'] = $user_id;
        return $params;
    }

    public function onMenuInitialize_ArticleGutterCreate($row)
    {
        $viewer = Engine_Api::_()->user()->getViewer();
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $owner = Engine_Api::_()->getItem('user', $request->getParam('user_id'));
        
        if ($viewer->getIdentity() != $owner->getIdentity()) {
            return false;
        }
        
        if (! Engine_Api::_()->authorization()->isAllowed('article', $viewer, 'create')) {
            return false;
        }
        
        return true;
    }

    public function onMenuInitialize_ArticleGutterEdit($row)
    {
        if (! Engine_Api::_()->core()->hasSubject()) {
            return false;
        }
        
        $viewer = Engine_Api::_()->user()->getViewer();
        $article = Engine_Api::_()->core()->getSubject('article');
        
        if (! $article->authorization()->isAllowed($viewer, 'edit')) {
            return false;
        }
        
        // Modify params
        $params = $row->params;
        $params['params']['article_id'] = $article->getIdentity();
        return $params;
    }

    public function onMenuInitialize_ArticleGutterDelete($row)
    {
        if (! Engine_Api::_()->core()->hasSubject()) {
            return false;
        }
        
        $viewer = Engine_Api::_()->user()->getViewer();
        $article = Engine_Api::_()->core()->getSubject('article');
        
        if (! $article->authorization()->isAllowed($viewer, 'delete')) {
            return false;
        }
        
        // Modify params
        $params = $row->params;
        $params['params']['article_id'] = $article->getIdentity();
        return $params;
    }

    public function onMenuInitialize_ArticleGutterShare($row)
    {
        $viewer = Engine_Api::_()->user()->getViewer();
        if (! $viewer->getIdentity()) {
            return false;
        }
        
        if (! Engine_Api::_()->core()->hasSubject()) {
            return false;
        }
        
        $subject = Engine_Api::_()->core()->getSubject();
        if (! ($subject instanceof Article_Model_Article)) {
            return false;
        }
        
        // Modify params
        $params = $row->params;
        $params['params']['type'] = $subject->getType();
        $params['params']['id'] = $subject->getIdentity();
        $params['params']['format'] = 'smoothbox';
        return $params;
    }

    public function onMenuInitialize_ArticleGutterReport($row)
    {
        $viewer = Engine_Api::_()->user()->getViewer();
        if (! $viewer->getIdentity()) {
            return false;
        }
        
        if (! Engine_Api::_()->core()->hasSubject()) {
            return false;
        }
        
        $subject = Engine_Api::_()->core()->getSubject();
        if (($subject instanceof Article_Model_Article) && $subject->owner_id == $viewer->getIdentity()) {
            return false;
        } else 
            if ($subject instanceof User_Model_User && $subject->getIdentity() == $viewer->getIdentity()) {
                return false;
            }
        
        // Modify params
        $subject = Engine_Api::_()->core()->getSubject();
        $params = $row->params;
        $params['params']['subject'] = $subject->getGuid();
        return $params;
    }
}
