<style type="text/css" media="screen">
	.layout_core_content{
		background-color: white; padding: 30px; border: 0.01em solid #d1d1d1; margin-top: 20px; 
	}
    .layout_core_content .noticia-titulo{
      margin-top    :20px;
      font-size     :24px;
      color         :#9f291d;
      font-weight   :bold;
      text-transform:capitalize;
      margin-top: 20px;
    }

	div, td { color: #FFFFFF !important; }

    .layout_core_content .blogs_entrylist .blog_entrylist_entry_date{ color: #8b8b8a; font-weight: normal; }
    .layout_core_content .blogs_entrylist a{ color: #9f291d; font-weight: normal; text-transform: capitalize; }

    .layout_core_content .blogs_entrylist .buttonlink:before{
		content: ''; display: inline-block; position: relative; background-image: url(../public/admin/articulo-comentar-verde.png); 
		width: 25px; height: 25px; left: 0; top: 5px;
	}
	.layout_core_content .blogs_entrylist .sociales{ display:block; text-align:center; width: 100%; color: #FFFFFF; }
	.layout_core_content .blogs_entrylist .sociales .pequeno{ 
		background-color: #9f291d; position:relative; padding: 5px; width: 15px; text-align: center;
		-webkit-border-radius: 20px; -moz-border-radius: 20px; border-radius: 20px; display:inline-block; margin-right: 2px; margin-left: 2px;
	}
	.layout_core_content .blogs_entrylist .sociales .pequeno p { color: #fff; }
	.layout_core_content .blogs_entrylist .sociales .icon-publicada{ background-image: url(public/admin/icon-home.png); display: inline-block; width: 20px; height: 20px; }
    .layout_core_content .blogs_entrylist .sociales .img1{ background-position: -5px -27px; margin-left: 10px; }
    .layout_core_content .blogs_entrylist .sociales .img2{ background-position: 55px -27px; margin-left: 10px; }
    .layout_core_content .blogs_entrylist .sociales .img3{ background-position: 24px -27px; margin-left: 10px; }
	
	.layout_core_content .blogs_entrylist .ver-mas a{
		position:relative; float: right; top: -30px;
		background-color:#ab1607; display:inline-block; width:100px; color:white; font-size:16px; text-align:center;
		padding:5px; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px;
	}
</style>
<p class="noticia-titulo"><?php echo $this->translate('Recent Entries')?></p>
<?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
	<ul class='blogs_entrylist'>
		<?php foreach ($this->paginator as $item): ?>
			<li>
				<h3>
					<?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?>
				</h3>
				<div class="blog_entrylist_entry_date">
					<?php echo $this->translate('by');?>
					<?php echo $this->htmlLink($item->getParent(), $item->getParent()->getTitle()) ?>
					<?php echo $this->timestamp($item->creation_date) ?>
				</div>
				<div class="blog_entrylist_entry_body">
					<?php echo $this->string()->truncate($this->string()->stripTags($item->body), 300) ?>				
				</div>
				<div class="sociales">
					<i class="icon-publicada img1"></i> <div class="pequeno"><p><?php echo $item->getLikesCount(); ?></p></div>
					<i class="icon-publicada img2"></i> <div class="pequeno"><p><?php echo $item->getCommentsCount(); ?></p></div>
					<a href="/activity/index/share/type/article/id/<?php echo $item->article_id;?>/format/smoothbox" class="smoothbox">
						<i class="icon-publicada img3"></i>
					</a>
				</div>
				<div class="ver-mas"><?php echo $this->htmlLink($item->getHref(), 'Ver más'); ?></div>

				<?php /*if ($item->comment_count > 0) :?> 
					<?php echo $this->htmlLink($item->getHref(), $this->translate(array('%s comment', '%s comments', $this->comment_count), $this->locale()->toNumber($this->comment_count)) , array('class' => 'buttonlink')) ?>
				<?php endif;*/ ?>
			</li>
		<?php endforeach; ?>
	</ul>
<?php elseif( $this->category || $this->tag ): ?>
	<div class="tip">
		<span><?php echo $this->translate('%1$s has not published a article entry with that criteria.', $this->owner->getTitle()); ?></span>
	</div>
<?php else: ?>
	<div class="tip">
		<span><?php echo $this->translate('%1$s has not written a article entry yet.', $this->owner->getTitle()); ?></span>
	</div>
<?php endif; ?>

<?php echo $this->paginationControl($this->paginator, null, null, array('pageAsQuery' => true, 'query' => $this->formValues, /*'params' => $this->formValues, */)); ?>

<script type="text/javascript">
	$$('.core_main_blog').getParent().addClass('active');
</script>