<div class="mi-contenedor">
    <p class="noticia-titulo"><?php echo $this->article->getTitle(); ?></p>
    <div class="blog_entrylist_entry_date"><i
                class="icon-publicada img1"></i><?php echo $this->timestamp($this->article->creation_date) ?></div>

    <?php if( $this->article->getPhotoUrl() ){ echo "<div class='blogs_browse_photo'><img src='".$this->article->getPhotoUrl()."'></div>"; } ?>

    <ul class='blogs_entrylist'>
        <li>

            <!--
      <div class="blog_entrylist_entry_date">

        <i class="icon-publicada img1"></i>
        <?php echo $this->timestamp($this->article->creation_date) ?>

        <i class="icon-publicada img2"></i>
        <?php echo $this->htmlLink($this->owner->getHref(), $this->owner->getTitle()) ?>

        <i class="icon-publicada img5"></i>
        <?php echo $this->translate(array('%s view', '%s views', $this->article->view_count), $this->locale()->toNumber($this->article->view_count));

            /*if( $this->category ){
              echo $this->translate('Filed in'); ?>
              <a href='javascript:void(0);' onclick='javascript:categoryAction(<?php echo $this->category->category_id?>);'><?php echo $this->translate($this->category->category_name) ?></a>
            <?php } ?>

            <?php
            if (count($this->articleTags )){
              foreach ($this->articleTags as $tag){ ?>
                <a href='javascript:void(0);' onclick='javascript:tagAction(<?php echo $tag->getTag()->tag_id; ?>);'>#<?php echo $tag->getTag()->text?></a>&nbsp;
              <?php }
            }  */ ?>
      </div>
      -->

            <div class="blog_entrylist_entry_short_description rich_content_body">
                <?php echo $this->article->description ?>
            </div>

            <div class="blog_entrylist_entry_body rich_content_body">
                <?php echo $this->article->body ?>
            </div>

		<!--
            <div class="blogs_browse_info_date">
                <i class="icon-publicada img1"></i>
                <p><?php echo $this->article->getLikesCount(); ?></p>
                <i class="icon-publicada img2"></i>
                <p><?php echo $this->article->getCommentsCount(); ?></p>
            </div>
       -->

        </li>
    </ul>

    <script type="text/javascript">
        $$('.core_main_blog').getParent().addClass('active');
    </script>
</div>