<div class="blog-noticias">
  <?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
    <ul class="blogs_browse" >
      <?php foreach( $this->paginator as $item ): ?>
        <li>

          <div class='blogs_browse_photo'>
            <?php 
            if( $item->getPhotoUrl() ){
    			    echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, '', $item->getTitle()));
            }else{
  			      echo $this->htmlLink($item->getHref(), '<img src="application/modules/Video/externals/images/video.png" style="width:200px; height:120px;" />'); 
            } ?>
          </div>

          <div class='blogs_browse_info'>

            <div class='blogs_browse_info_category'><?php echo $this->translate($item->getCategoria());?></div>

            <div class='blogs_browse_info_title'>
              <?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?>
            </div>

            <div class='blogs_browse_info_blurb'>
              <?php echo $this->string()->truncate($this->string()->stripTags($item->description), 300) ?>
            </div>
            

            <div class="blogs_browse_info_date">
              
              <i class="icon-publicada img1"></i><p><?php echo $item->getLikesCount(); ?></p>
              <i class="icon-publicada img2"></i><p><?php echo $item->getCommentsCount(); ?></p>
              <a href="/activity/index/share/type/article/id/<?php echo $item->article_id;?>/format/smoothbox" class="smoothbox">
                  <i class="icon-publicada img3"></i>
              </a>

              <div class="ver-mas"><?php echo $this->htmlLink($item->getHref(), 'Ver más'); ?></div>
            </div>

            


          </div>
        </li>
      <?php endforeach; ?>
    </ul>

  <?php elseif( $this->category || $this->show == 2 || $this->search ): ?>
    <div class="tip">
      <span>
        <?php echo $this->translate('Nobody has written a article entry with that criterio.');?>
        <?php if (TRUE): // @todo check if user is allowed to create a poll ?>
          <?php echo $this->translate('Be the first to %1$swrite%2$s one!', '<a href="'.$this->url(array('action' => 'create'), 'article_general').'">', '</a>'); ?>
        <?php endif; ?>
      </span>
    </div>

  <?php else:?>
    <div class="tip">
      <span>
        <?php echo $this->translate('Nobody has written a article entry yet.'); ?>
        <?php if( $this->canCreate ): ?>
          <?php echo $this->translate('Be the first to %1$swrite%2$s one!', '<a href="'.$this->url(array('action' => 'create'), 'article_general').'">', '</a>'); ?>
        <?php endif; ?>
      </span>
    </div>
  <?php endif; ?>

  <?php echo $this->paginationControl($this->paginator, null, null, array(
    'pageAsQuery' => true,
    'query' => $this->formValues,
    //'params' => $this->formValues,
  )); ?>


</div>