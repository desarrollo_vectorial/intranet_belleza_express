<?php
/**
 * Radcodes - SocialEngine Module
 *
 * @category   Application_Extensions
 * @package    Article
 * @copyright  Copyright (c) 2009-2010 Radcodes LLC (http://www.radcodes.com)
 * @license    http://www.radcodes.com/license/
 * @version    $Id$
 * @author     Vincent Van <vincent@radcodes.com>
 */
?>

<style>
	.article_meta>ul{ margin-bottom: 7px; }
	.article_meta>ul>.article_meta_owner{ color: #636262 !important; }
	.article_meta>ul>.article_meta_owner>a{ color: #2A94CB !important; }
	
	li.article_meta_views { background: #6f6f6f; padding: 8px; color: #FFFFFF !important; border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px; }
	
	li.article_meta_likes:before { display: inline-block; width: 23px; height: 20px; content: ''; background-size: contain; background: url("application/modules/Activity/externals/images/activity/like_heart.png") no-repeat; margin-right: 3px; }
	li.article_meta_likes>span { color: #fff; font-size: 10px; top: -5px; display: inline-block; position: relative; font-weight: 300 !important; background: #3671a8; padding: 5px; padding-left: 8px; padding-right: 8px; border-radius: 24px; }

	li.article_meta_comments:before { display: inline-block; width: 23px; height: 20px; content: ''; background-size: contain; background: url("application/modules/Activity/externals/images/activity/comments_icon.png") no-repeat; margin-right: 3px; }
	li.article_meta_comments>span { color: #fff; font-size: 10px; top: -6px; display: inline-block; position: relative; font-weight: 300 !important; background: #a6a6a6; padding: 5px; padding-left: 8px; padding-right: 8px; border-radius: 24px; }
</style>

	<ul>
	  <?php if ($this->show_owner !== false): ?>
	    <li class="article_meta_owner"><?php echo $this->translate($this->display_style == 'narrow' ? 'by %s' : 'By %s', $this->article->getOwner()->toString()); ?></li>
	  <?php endif; ?>
	  <?php /*if ($this->show_date !== false): ?>
	    <li class="article_meta_date"><?php echo $this->timestamp($this->article->creation_date); ?></li>
	  <?php endif; */ ?>

		<?php if ($this->show_views != false): ?>
	    <li class="article_meta_views"><?php echo $this->translate(array('%1$s Visitas', '%1$s Visitas', $this->article->view_count), $this->locale()->toNumber($this->article->view_count)); ?></li>
	  <?php endif; ?>

	  

	  <?php if ($this->show_likes !== false): ?>
	    <li class="article_meta_likes"><span><?php echo $this->translate(array('%1$s', '%1$s', $this->article->like_count), $this->locale()->toNumber($this->article->like_count)); ?></span></li>
	  <?php endif; ?>

		<?php if ($this->show_comments !== false): ?>
	    <li class="article_meta_comments"><span><?php echo $this->translate(array("%s", "%s", $this->article->comment_count), $this->locale()->toNumber($this->article->comment_count)); ?></span></li>
	  <?php endif; ?>	  
	</ul>