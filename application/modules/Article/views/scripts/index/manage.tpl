<style type="text/css" media="screen">

    .icon_blog_delete
    {
        background-image: none !important;
    }



	.blog-noticias .pages{ display:flex; justify-content:left; }
	.blog-noticias .pages li a{ color: #ab1607; }

</style>

<script type="text/javascript">
	var pageAction = function(page) {
		$('page').value = page;
		$('filter_form').submit();
	}
</script>

<div class="blog-noticias">
	<?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
	<ul class="blogs_browse">
		<?php foreach( $this->paginator as $item ): ?>
		<li>
			<div class='blogs_browse_photo'>
				<?php 
				if( $item->getPhotoUrl() ){
					echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, '', $item->getTitle()));
				}else{
					echo $this->htmlLink($item->getHref(), '<img src="application/modules/Video/externals/images/video.png" style="width:200px; height:120px;" />'); 
				} ?>
			</div>

			<div class='blogs_browse_info'>
				<p class='blogs_browse_info_title'>
					<?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?>
				</p>
				<p class='blogs_browse_info_blurb'>
					<?php echo $this->string()->truncate($this->string()->stripTags($item->description), 300) ?>
				</p>
                <div class="blogs_browse_info_date">

                        <i class="icon-publicada img1"></i>
                        <p><?php echo $item->getLikesCount(); ?></p>
                        <i class="icon-publicada img2"></i>
                        <p><?php echo $item->getCommentsCount(); ?></p>

<!--                    <a href="/activity/index/share/type/article/id/--><?php //echo $item->article_id; ?><!--/format/smoothbox"-->
<!--                       class="smoothbox">-->
<!--                        <i class="icon-publicada img3"></i>-->
<!--                    </a>-->
                    <div class="editar-no">
                        <?php echo $this->htmlLink(array(
                            'action' => 'edit',
                            'article_id' => $item->getIdentity(),
                            'route' => 'article_specific',
                            'reset' => true,

                        ), 'Editar'); ?>
                    </div>
                    <div class="borrar-no">
                        <?php echo $this->htmlLink(array(
                            'route' => 'default',
                            'module' => 'article',
                            'controller' => 'index',
                            'action' => 'delete',
                            'article_id' => $item->getIdentity(),
                            'format' => 'smoothbox'

                        ), 'Borrar',array(
                            'class' => 'buttonlink smoothbox icon_blog_delete'
                        )); ?>
                    </div>
			</div>


            </div>




<!--            <div class='blogs_browse_options'>-->
<!--                --><?php //echo $this->htmlLink(array(
//                    'action' => 'edit',
//                    'article_id' => $item->getIdentity(),
//                    'route' => 'article_specific',
//                    'reset' => true,
//                ), $this->translate('Edit Entry'), array(
//                    'class' => 'buttonlink icon_blog_edit',
//                )) ?>
<!--                --><?php
//                echo $this->htmlLink(array(
//                    'route' => 'default',
//                    'module' => 'article',
//                    'controller' => 'index',
//                    'action' => 'delete',
//                    'article_id' => $item->getIdentity(),
//                    'format' => 'smoothbox'
//                ), $this->translate('Delete Entry'), array(
//                    'class' => 'buttonlink smoothbox icon_blog_delete'
//                ));
//                ?>
<!--                --><?php //if( !$item->isPublished() ): ?>
<!--                    --><?php
//                    echo $this->htmlLink(array(
//                        'route' => 'default',
//                        'module' => 'article',
//                        'controller' => 'index',
//                        'action' => 'publish',
//                        'article_id' => $item->getIdentity(),
//                    ), $this->translate('Publish Entry'), array(
//                        'class' => 'buttonlink icon_blog_publish'
//                    ));
//                    ?>
<!--                --><?php //endif; ?>
<!--            </div>-->
            
		</li>
		<?php endforeach; ?>
	</ul>

	<?php elseif($this->search): ?>
	<div class="tip">
		<span> <?php echo $this->translate('_article_You do not have any blog entries that match your search criteria.');?>
		</span>
	</div>
	<?php else: ?>
	<div class="tip">
		<span> <?php echo $this->translate('_article_You do not have any blog entries.');?>
			<?php if( $this->canCreate ): ?> <?php echo $this->translate('_article_Get started by %1$swriting%2$s a new entry.', '<a href="'.$this->url(array('action' => 'create'), 'article_general').'">', '</a>'); ?>
			<?php endif; ?>
		</span>
	</div>
	<?php endif; ?>
</div>
<br>
<?php echo $this->paginationControl($this->paginator, null, null, array('pageAsQuery' => true, 'query' => $this->formValues )); ?>	


<script type="text/javascript">
	$$('.core_main_blog').getParent().addClass('active');
</script>