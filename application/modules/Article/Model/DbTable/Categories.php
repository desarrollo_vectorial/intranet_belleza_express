<?php
 
 
 class Article_Model_DbTable_Categories extends Radcodes_Model_DbTable_Categories
 {
   protected $_rowClass = 'Article_Model_Category';

   public function getCategoriesAssoc()
   {
       $stmt = $this->select()
           ->from($this, array(
           'category_id',
           'category_name'
       ))
           ->order('category_name ASC')
           ->query();
       
       $data = array();
       foreach ($stmt->fetchAll() as $category) {
           $data[$category['category_id']] = $category['category_name'];
       }
       
       return $data;
   }

   public function getUserCategoriesAssoc($user)
   {
       if ($user instanceof User_Model_User) {
           $user = $user->getIdentity();
       } else 
           if (! is_numeric($user)) {
               return array();
           }
       
       $stmt = $this->getAdapter()
           ->select()
           ->from('engine4_article_categories', array(
           'category_id',
           'category_name'
       ))
           ->joinLeft('engine4_article_articles', "engine4_article_articles.category_id = engine4_article_categories.category_id")
           ->group("engine4_article_categories.category_id")
           ->where('engine4_article_articles.owner_id = ?', $user)
           ->where('engine4_article_articles.published = ?', "1")
           ->order('category_name ASC')
           ->query();
       
       $data = array();
       foreach ($stmt->fetchAll() as $category) {
           $data[$category['category_id']] = $category['category_name'];
       }
       
       return $data;
   }
   
 }