<?php

class Article_Model_DbTable_Articles extends Engine_Db_Table
{

    protected $_rowClass = 'Article_Model_Article';

    public function getArticlesSelect($params = array())
    {
        $table = Engine_Api::_()->getDbtable('articles', 'article');
        $rName = $table->info('name');
        $tmTable = Engine_Api::_()->getDbtable('TagMaps', 'core');
        $tmName = $tmTable->info('name');
        $select = $table->select()->order(! empty($params['orderby']) ? $params['orderby'] . ' DESC' : $rName . '.creation_date DESC');
        
        if (! empty($params['user_id']) && is_numeric($params['user_id'])) {
            $select->where($rName . '.owner_id = ?', $params['user_id']);
        }
        if (! empty($params['user']) && $params['user'] instanceof User_Model_User) {
            $select->where($rName . '.owner_id = ?', $params['user_id']->getIdentity());
        }
        if (! empty($params['users'])) {
            $str = (string) (is_array($params['users']) ? "'" . join("', '", $params['users']) . "'" : $params['users']);
            $select->where($rName . '.owner_id in (?)', new Zend_Db_Expr($str));
        }
        if (! empty($params['tag'])) {
            $select->setIntegrityCheck(false)
                ->from($rName)
                ->joinLeft($tmName, "$tmName.resource_id = $rName.article_id")
                ->where($tmName . '.resource_type = ?', 'article')
                ->where($tmName . '.tag_id = ?', $params['tag']);
        }
        if (! empty($params['category'])) {
            $select->where($rName . '.category_id = ?', $params['category']);
        }
        if (isset($params['published'])) {
            $select->where($rName . '.published = ?', $params['published']);
        }
        // Could we use the search indexer for this?
        if (! empty($params['search'])) {
            $select->where($rName . ".title LIKE ? OR " . $rName . ".body LIKE ?", '%' . $params['search'] . '%');
        }
        if (! empty($params['start_date'])) {
            $select->where($rName . ".creation_date > ?", date('Y-m-d', strtotime(str_replace('-', '/', $params['start_date']))));
        }
        if (! empty($params['end_date'])) {
            $select->where($rName . ".creation_date < ?", date('Y-m-d', strtotime(str_replace('-', '/', $params['end_date']))));
        }
        if (! empty($params['visible'])) {
            $select->where($rName . ".search = ?", $params['visible']);
        }
        return $select;
    }

    public function getArticlesList($params = array())
    {
        $table = Engine_Api::_()->getDbtable('articles', 'article');
        $select = $table->select($params)
            ->where('published = ? ', 1)
            ->order('creation_date DESC');
        
        if (! empty($params['featured']) && is_numeric($params['featured'])) {
            $select->where('featured = ?', $params['featured']);
        }
        return $table->fetchAll($select);
    }

    public function getPopularsTags()
    {
        $resource_type = 'article';
        
        $tag_table = Engine_Api::_()->getDbtable('tags', 'core');
        $tagmap_table = $tag_table->getMapTable();
        
        $tName = $tag_table->info('name');
        $tmName = $tagmap_table->info('name');
        
        if (isset($options['order'])) {
            $order = $options['order'];
        } else {
            $order = 'text';
        }
        
        if (isset($options['sort'])) {
            $sort = $options['sort'];
        } else {
            $sort = $order == 'total' ? SORT_DESC : SORT_ASC;
        }
        
        $limit = isset($options['limit']) ? $options['limit'] : 50;
        
        $select = $tag_table->select()
            ->setIntegrityCheck(false)
            ->from($tmName, array(
            'total' => "COUNT(*)"
        ))
            ->join($tName, "$tName.tag_id = $tmName.tag_id")
            ->where($tmName . '.resource_type = ?', $resource_type)
            ->where($tmName . '.tag_type = ?', 'core_tag')
            ->group("$tName.tag_id")
            ->order("total desc")
            ->limit("$limit");
        
        $params = array(
            'published' => 1,
            'search' => 1
        );
        $article_table = Engine_Api::_()->getDbtable('articles', 'article');
        $rName = $article_table->info('name');
        
        $select->setIntegrityCheck(false)->join($rName, "$tmName.resource_id = $rName.article_id");
        $select = $article_table->selectParamBuilder($params, $select);
        
        $tags = $tag_table->fetchAll($select);
        
        $records = array();
        
        $columns = array();
        if (! empty($tags)) {
            foreach ($tags as $k => $tag) {
                $records[$k] = $tag;
                $columns[$k] = $order == 'total' ? $tag->total : $tag->text;
            }
        }
        
        $tags = array();
        if (count($columns)) {
            if ($order == 'text') {
                natcasesort($columns);
            } else {
                arsort($columns);
            }
            
            foreach ($columns as $k => $name) {
                $tags[$k] = $records[$k];
            }
        }
        
        return $tags;
    }

    public function getArticlesPaginator($params = array())
    {
        $paginator = Zend_Paginator::factory($this->getArticlesSelect($params));
        
        if (! empty($params['page'])) {
            $paginator->setCurrentPageNumber($params['page']);
        }
        if (! empty($params['limit'])) {
            $paginator->setItemCountPerPage($params['limit']);
        }
        if (empty($params['limit'])) {
            $page = (int) Engine_Api::_()->getApi('settings', 'core')->getSetting('article.page', 10);
            $paginator->setItemCountPerPage($page);
        }
        return $paginator;
    }

    public function getArchiveList($spec)
    {
        if (! ($spec instanceof User_Model_User)) {
            return null;
        }
        
        $localeObject = Zend_Registry::get('Locale');
        if (! $localeObject) {
            $localeObject = new Zend_Locale();
        }
        
        $dates = $this->select()
            ->from($this, 'creation_date')
            ->where('owner_type = ?', 'user')
            ->where('owner_id = ?', $spec->getIdentity())
            ->where('published = ?', 1)
            ->order('article_id DESC')
            ->query()
            ->fetchAll(Zend_Db::FETCH_COLUMN);
        
        $time = time();
        
        $archive_list = array();
        foreach ($dates as $date) {
            
            $date = strtotime($date);
            $ltime = localtime($date, true);
            $ltime["tm_mon"] = $ltime["tm_mon"] + 1;
            $ltime["tm_year"] = $ltime["tm_year"] + 1900;
            
            // LESS THAN A YEAR AGO - MONTHS
            if ($date + 31536000 > $time) {
                $date_start = mktime(0, 0, 0, $ltime["tm_mon"], 1, $ltime["tm_year"]);
                $date_end = mktime(0, 0, 0, $ltime["tm_mon"] + 1, 1, $ltime["tm_year"]);
                $type = 'month';
                
                $dateObject = new Zend_Date($date);
                $format = $localeObject->getTranslation('yMMMM', 'dateitem', $localeObject);
                $label = $dateObject->toString($format, $localeObject);
            }  // MORE THAN A YEAR AGO - YEARS
else {
                $date_start = mktime(0, 0, 0, 1, 1, $ltime["tm_year"]);
                $date_end = mktime(0, 0, 0, 1, 1, $ltime["tm_year"] + 1);
                $type = 'year';
                
                $dateObject = new Zend_Date($date);
                $format = $localeObject->getTranslation('yyyy', 'dateitem', $localeObject);
                if (! $format) {
                    $format = $localeObject->getTranslation('y', 'dateitem', $localeObject);
                }
                $label = $dateObject->toString($format, $localeObject);
            }
            
            if (! isset($archive_list[$date_start])) {
                $archive_list[$date_start] = array(
                    'type' => $type,
                    'label' => $label,
                    'date' => $date,
                    'date_start' => $date_start,
                    'date_end' => $date_end,
                    'count' => 1
                );
            } else {
                $archive_list[$date_start]['count'] ++;
            }
        }
        
        return $archive_list;
    }

    public function selectParamBuilder($params = array(), $select = null)
    {
        $rName = $this->info('name');
        
        if ($select === null) {
            $select = $this->select();
        }
        
        if (isset($params['live']) && $params['live']) {
            $params['published'] = 1;
            unset($params['live']);
        }
        
        if (isset($params['user_id'])) {
            $params['user'] = $params['user_id'];
        }
        
        if (isset($params['user']) && $params['user']) {
            $user = Engine_Api::_()->user()->getUser($params['user']);
            $select->where($rName . '.owner_id = ?', $user->getIdentity());
        }
        
        if (! empty($params['users'])) {
            // $str = (string) ( is_array($params['users']) ? "'" . join("', '", $params['users']) . "'" : $params['users'] );
            // $select->where($rName.'.owner_id in (?)', new Zend_Db_Expr($str));
            $select->where($rName . '.owner_id in (?)', $params['users']);
        }
        
        if (isset($params['category']) && $params['category']) {
            $category_id = ($params['category'] instanceof Core_Model_Item_Abstract) ? $params['category']->getIdentity() : (int) $params['category'];
            
            $category_ids = array(
                $category_id
            );
            $categories = Engine_Api::_()->getItemTable('article_category')->getChildrenOfParent($category_id);
            foreach ($categories as $category) {
                $category_ids[] = $category->getIdentity();
            }
            
            $select->where($rName . '.category_id IN (?)', $category_ids);
        }
        
        foreach (array(
            'featured',
            'sponsored',
            'search',
            'published'
        ) as $field) {
            if (isset($params[$field])) {
                $select->where($rName . ".$field = ?", $params[$field] ? 1 : 0);
            }
        }
        
        if (! empty($params['keyword'])) {
            $select->where($rName . ".title LIKE ? OR " . $rName . ".description LIKE ? OR " . $rName . ".body LIKE ?", '%' . $params['keyword'] . '%');
        }
        
        if (! empty($params['start_date'])) {
            $select->where($rName . ".creation_date >= ?", date('Y-m-d', $params['start_date']));
        }
        
        if (! empty($params['end_date'])) {
            $select->where($rName . ".creation_date <= ?", date('Y-m-d', $params['end_date']));
        }
        
        if (isset($params['exclude_article_ids']) and ! empty($params['exclude_article_ids'])) {
            $select->where($rName . ".article_id NOT IN (?)", $params['exclude_article_ids']);
        }
        
        if (! empty($params['period'])) {
            $period_maps = array(
                '24hrs' => 1,
                'week' => 7,
                'month' => 30,
                'quarter' => 90,
                'year' => 365,
                
                7 => 7,
                30 => 30,
                90 => 90,
                180 => 180,
                365 => 365
            );
            if (isset($period_maps[$params['period']]) && $period_maps[$params['period']]) {
                $select->where($rName . ".creation_date >= ?", date('Y-m-d', time() - $period_maps[$params['period']] * 86400));
            }
        }
        
        if (isset($params['order'])) {
            switch ($params['order']) {
                case 'random':
                    $order_expr = new Zend_Db_Expr('RAND()');
                    break;
                case 'recent':
                    $order_expr = $rName . ".creation_date DESC";
                    break;
                case 'lastupdated':
                    $order_expr = $rName . ".modified_date DESC";
                    break;
                case 'mostcommented':
                    $order_expr = $rName . ".comment_count DESC";
                    break;
                case 'mostliked':
                    $order_expr = $rName . ".like_count DESC";
                    break;
                case 'mostviewed':
                    $order_expr = $rName . ".view_count DESC";
                    break;
                case 'alphabet':
                    $order_expr = $rName . ".title ASC";
                    break;
                
                default:
                    $order_expr = ! empty($params['order']) ? $params['order'] : $rName . '.creation_date DESC';
                    
                    if (! empty($params['order_direction'])) {
                        $order_expr .= " " . $params['order_direction'];
                    }
                    
                    if (! is_array($order_expr) && ! ($order_expr instanceof Zend_Db_Expr) and strpos($order_expr, '.') === false) {
                        $order_expr = $rName . "." . trim($order_expr);
                    }
                    break;
            }
            
            if (isset($params['preorder']) && $params['preorder']) {
                $pre_orders = array(
                    1 => array(
                        "{$rName}.sponsored DESC"
                    ), // Sponsored listings, then user preference",
                    2 => array(
                        "{$rName}.sponsored DESC",
                        "{$rName}.featured DESC"
                    ), // "Sponsored listings, featured listings, then user preference",
                    3 => array(
                        "{$rName}.featured DESC"
                    ), // "Featured listings, then user preference",
                    4 => array(
                        "{$rName}.featured DESC",
                        "{$rName}.sponsored DESC"
                    )
                ); // "Featured listings, sponsored listings, then user preference",
                
                if (array_key_exists($params['preorder'], $pre_orders)) {
                    $order_expr = array_merge($pre_orders[$params['preorder']], array(
                        $order_expr
                    ));
                }
            }
            
            $select->order($order_expr);
            unset($params['order']);
        }
        
        $viewer = Engine_Api::_()->user()->getViewer();
        if ($viewer->level_id > 3) {
            $select->where("article_id IN(
											SELECT b.article_id
													FROM engine4_network_membership AS a
											JOIN engine4_article_networks AS b ON ( a.resource_id = b.network_id )
												WHERE a.user_id = " . $viewer->user_id . "
												GROUP BY b.article_id
											)");
        }
        /*
         * INSERT INTO engine4_article_networks
         * (
         * article_id,
         * network_id
         * )
         * SELECT
         * a.article_id,
         * b.network_id
         * FROM
         * engine4_article_articles as a,
         * engine4_network_networks as b
         */
        
        // echo $select;
        return $select;
    }
}
?>