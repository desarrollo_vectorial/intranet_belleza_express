<?php

class Article_Model_Article extends Core_Model_Item_Abstract
{
    // protected $_owner_type = 'user';
    protected $_parent_type = 'user';

    protected $_searchTriggers = array(
        'title',
        'body',
        'search'
    );

    protected $_parent_is_owner = true;

    public function getHref($params = array())
    {
        $slug = $this->getSlug();

        $params = array_merge(array(
            'route' => 'article_entry_view',
            'reset' => true,
            'user_id' => $this->owner_id,
            'article_id' => $this->article_id,
            'slug' => $slug
        ), $params);
        $route = $params['route'];
        $reset = $params['reset'];
        unset($params['route']);
        unset($params['reset']);
        return Zend_Controller_Front::getInstance()->getRouter()->assemble($params, $route, $reset);
    }

    public function getDescription($length = 255, $field = 'description')
    {
        // @todo decide how we want to handle multibyte string functions
        $tmpBody = ($field == 'description') ? strip_tags($this->description) : strip_tags($this->body);
        return (Engine_String::strlen($tmpBody) > $length ? Engine_String::substr($tmpBody, 0, $length) . '...' : $tmpBody);
    }

    public function getShortTitle($length = 50)
    {
        return (Engine_String::strlen($this->title) > $length ? Engine_String::substr($this->title, 0, $length) . '...' : $this->title);
    }

    public function getKeywords($separator = ' ')
    {
        $keywords = array();
        foreach ($this->tags()->getTagMaps() as $tagmap) {
            $tag = $tagmap->getTag();
            $keywords[] = $tag->getTitle();
        }

        if (null === $separator) {
            return $keywords;
        }

        return join($separator, $keywords);
    }

    public function getCategoria(){
        //return $this->category_id;

        //$table = Engine_Api::_()->getDbtable('articles', 'article');
        return Engine_Api::_()->getDbtable('categories', 'article')->find($this->category_id)->current();


    }

    public function guardarFoto($photo)
    {
        if ($photo instanceof Zend_Form_Element_File) {
            $file = $photo->getFileName();
        } else {
            if (is_array($photo) && ! empty($photo['tmp_name'])) {
                $file = $photo['tmp_name'];
            } else {
                if (is_string($photo) && file_exists($photo)) {
                    $file = $photo;
                } else {
                    throw new Article_Model_Exception('invalid argument passed to guardarFoto');
                }
            }
        }

        $name = basename($file);
        $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary';
        $params = array(
            'parent_type' => 'article',
            'parent_id' => $this->article_id
        );

        if (! $fileName) {
            $fileName = basename($file);
        }
        $extension = ltrim(strrchr(basename($fileName), '.'), '.');

        // Save
        $filesTable = Engine_Api::_()->getItemTable('storage_file');

        // Resize image (main)
        $mainPath = $path . DIRECTORY_SEPARATOR . $base . '_m.' . $extension;
        $image = Engine_Image::factory();
        $image->open($file)
            ->resize(400, 400)
            ->write($mainPath)
            ->destroy();

        // Resize image (normal)
        $normalPath = $path . DIRECTORY_SEPARATOR . $base . '_in.' . $extension;
        $image = Engine_Image::factory();
        $image->open($file)
            ->resize(120, 100)
            ->write($normalPath)
            ->destroy();

        // Store
        $iMain = $filesTable->createFile($mainPath, $params);
        // $iIconNormal = $filesTable->createFile($normalPath, $params);

        // $iMain->bridge($iIconNormal, 'thumb.normal');

        // Remove temp files
        @unlink($mainPath);
        @unlink($normalPath);

        // Update row
        $this->photo_id = $iMain->file_id;
        $this->save();
    }

    public function guardarFotoInt($photo)
    {
        if ($photo instanceof Zend_Form_Element_File) {
            $file = $photo->getFileName();
        } else {
            if (is_array($photo) && ! empty($photo['tmp_name'])) {
                $file = $photo['tmp_name'];
            } else {
                if (is_string($photo) && file_exists($photo)) {
                    $file = $photo;
                } else {
                    throw new Article_Model_Exception('invalid argument passed to guardarFoto');
                }
            }
        }

        $name = basename($file);
        $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary';
        $params = array(
            'parent_type' => 'article',
            'parent_id' => $this->article_id
        );

        if (! $fileName) {
            $fileName = basename($file);
        }
        $extension = ltrim(strrchr(basename($fileName), '.'), '.');

        // Save
        $filesTable = Engine_Api::_()->getItemTable('storage_file');

        // Resize image (main)
        $mainPath = $path . DIRECTORY_SEPARATOR . $base . '_m.' . $extension;
        $image = Engine_Image::factory();
        $image->open($file)
            ->resize(400, 400)
            ->write($mainPath)
            ->destroy();

        // Resize image (normal)
        $normalPath = $path . DIRECTORY_SEPARATOR . $base . '_in.' . $extension;
        $image = Engine_Image::factory();
        $image->open($file)
            ->resize(120, 100)
            ->write($normalPath)
            ->destroy();

        // Store
        $iMain = $filesTable->createFile($mainPath, $params);
        // $iIconNormal = $filesTable->createFile($normalPath, $params);

        // $iMain->bridge($iIconNormal, 'thumb.normal');

        // Remove temp files
        @unlink($mainPath);
        @unlink($normalPath);

        // Update row
        $this->photoint_id = $iMain->file_id;
        $this->save();
    }

    public function isPublished()
    {
        return $this->published ? true : false;
    }

    public function getCommentsCount()
    {
        return $this->comment_count;
    }

    public function getLikesCount()
    {
        return $this->like_count;
    }

    public function getMainImage()
    {
        $table = Engine_Api::_()->getDbtable('files', 'storage');
        $select = $table->select()
            ->where('parent_type = ? ', 'article')
            ->where('parent_id = ? ', $this->article_id)
            ->limit(1);

        $img = $table->fetchAll($select);

        if (count($img)) {

            foreach ($img as $value) {
                return $value->storage_path;
            }
        }
        return false;
    }

    public function getViewsCount()
    {
        return $this->comment_count;
    }

    public function comments()
    {
        return new Engine_ProxyObject($this, Engine_Api::_()->getDbtable('comments', 'core'));
    }

    public function likes()
    {
        return new Engine_ProxyObject($this, Engine_Api::_()->getDbtable('likes', 'core'));
    }

    public function tags()
    {
        return new Engine_ProxyObject($this, Engine_Api::_()->getDbtable('tags', 'core'));
    }
}
?>
