<?php
return array(
    'package' => array(
        'type' => 'module',
        'name' => 'article',
        'version' => '4.0.0',
        'path' => 'application/modules/Article',
        'title' => 'Articles',
        'description' => 'manage the articles of the site',
        'author' => 'jrodriguez@vectorial.co',
        'callback' => array(
            'class' => 'Engine_Package_Installer_Module'
        ),
        'actions' => array(
            0 => 'install',
            1 => 'upgrade',
            2 => 'refresh',
            3 => 'enable',
            4 => 'disable'
        ),
        'directories' => array(
            0 => 'application/modules/Article'
        ),
        'files' => array(
            0 => 'application/languages/en/article.csv'
        )
    ),
    // Items ---------------------------------------------------------------------
    'items' => array(
        'article_article',
        'article_category',
        'article'
    ),
    // Routes --------------------------------------------------------------------
    'routes' => array(
        'article_general' => array(
            'route' => 'articles/:action/*',
            'defaults' => array(
                'module'     => 'article',
                'controller' => 'index',
                'action'     => 'browse'
            ),
            'reqs' => array(
                'action' => '(browse|create|list|manage)'
            )
        ),
       'article_index_edit' => array(
            'route' => 'articles/edit/:article_id/*',
            'defaults' => array(
                'module'     => 'article',
                'controller' => 'index',
                'action'     => 'edit'
            )
        ),
        
        'article_specific' => array(
            'route' => 'articles/:action/:article_id/*',
            'defaults' => array(
                'module' => 'article',
                'controller' => 'index',
                'action' => 'index'
            ),
            'reqs' => array(
                'article_id' => '\d+',
                //'action' => '(edit|delete)'
                'action' => '(delete)'
            )
        ),
        'article_extended' => array(
            'route' => 'article/:controller/:action/*',
            'defaults' => array(
                'module' => 'article',
                'controller' => 'index',
                'action' => 'index'
            ),
            'reqs' => array(
                'controller' => '\D+',
                'action' => '\D+'
            )
        ),
        'article_profile' => array(
            'route' => 'article/:id/*',
            'defaults' => array(
                'module' => 'article',
                'controller' => 'profile',
                'action' => 'index'
            ),
            'reqs' => array(
                'id' => '\d+'
            )
        ),
        'article_view' => array(
            'route' => 'article/:user_id/*',
            'defaults' => array(
                'module' => 'article',
                'controller' => 'index',
                'action' => 'list'
            ),
            'reqs' => array(
                'user_id' => '\d+'
            )
        ),
        'article_entry_view' => array(
            'route' => 'article/:user_id/:article_id/:slug',
            'defaults' => array(
                'module' => 'article',
                'controller' => 'index',
                'action' => 'view',
                'slug' => ''
            ),
            'reqs' => array(
                'user_id' => '\d+',
                'article_id' => '\d+'
            )
        )
    )
);
?>