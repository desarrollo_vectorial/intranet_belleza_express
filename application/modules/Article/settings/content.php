<?php
return array(
    array(
        'title' => 'Article Browse Search',
        'description' => 'Displays a search form in the article browse page.',
        'category' => 'Articles',
        'type' => 'widget',
        'name' => 'article.browse-search',
        'requirements' => array(
            'no-subject'
        )
    ),
    array(
        'title' => 'Article Browse Menu',
        'description' => 'Displays a menu in the article browse page.',
        'category' => 'Articles',
        'type' => 'widget',
        'name' => 'article.browse-menu',
        'requirements' => array(
            'no-subject'
        )
    ),
    array(
        'title' => 'Article Browse Quick Menu',
        'description' => 'Displays a small menu in the article browse page.',
        'category' => 'Articles',
        'type' => 'widget',
        'name' => 'article.browse-menu-quick',
        'requirements' => array(
            'no-subject'
        )
    ),
    // Gutter
    array(
        'title' => 'Article Gutter Menu',
        'description' => 'Displays a menu in the article gutter.',
        'category' => 'Articles',
        'type' => 'widget',
        'name' => 'article.gutter-menu'
    ),
    array(
        'title' => 'Article Gutter Photo',
        'description' => 'Displays the owners photo in the article gutter.',
        'category' => 'Articles',
        'type' => 'widget',
        'name' => 'article.gutter-photo'
    ),
    // Home list artilces
    array(
        'title' => 'Article List',
        'description' => 'Displays a list of posted articles with different filtering options.',
        'category' => 'Articles',
        'type' => 'widget',
        'name' => 'article.list-articles',
        'defaultParams' => array(
            'title' => 'Articles',
            'titleCount' => true,
            'max' => 5,
            'display_style' => 'wide',
            'order' => 'recent'
        )
    ),
    // Home list tags
    array(
        'title' => 'Article List Tags',
        'description' => 'Displays tag of article\'s',
        'category' => 'Articles',
        'type' => 'widget',
        'name' => 'article.list-tags'
    ),
    array(
        'title' => 'Slider Featured Articles',
        'description' => 'Show a slider with the articles featureds.',
        'category' => 'Articles',
        'type' => 'widget',
        'name' => 'article.slider-articles'
    ),
    // ------- top submitters
    array(
        'title' => 'Top Article Submitters',
        'description' => 'Displays list of top article\'s submitters',
        'category' => 'Articles',
        'type' => 'widget',
        'name' => 'article.top-submitters'
    ),
    // ------- most likes
    array(
        'title' => 'Top Article Likes',
        'description' => 'Displays list of top article\'s likes',
        'category' => 'Articles',
        'type' => 'widget',
        'name' => 'article.most-likes'
    ),
    // ------- most comments
    array(
        'title' => 'Top Article Comment',
        'description' => 'Displays list of top article\'s comments',
        'category' => 'Articles',
        'type' => 'widget',
        'name' => 'article.most-comments'
    ),

    // ------- where I am
    array(
        'title'       => 'Donde estoy - Noticias',
        'description' => 'Muestra en que modulo se encuestra actualmente',
        'category'    => 'Articles',
        'type'        => 'widget',
        'name'        => 'article.where-i-am'
    ),

    // ------- boton destacados
    array(
        'title'       => 'Boton Noticias Destacados',
        'description' => 'Boton que lleva al administrador de las noticias destacadas de la seccion',
        'category'    => 'Articles',
        'type'        => 'widget',
        'name'        => 'article.boton-destacados'
    ),

);
?>
