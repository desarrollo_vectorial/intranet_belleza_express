<?php

class Article_Widget_BrowseMenuQuickController extends Engine_Content_Widget_Abstract
{

    public function indexAction()
    {
        // Get navigation
        $this->view->quickNavigation = $quickNavigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('article_main');
        if (count($this->view->quickNavigation) == 1) {
            $this->view->quickNavigation = null;
        }
    }
}
