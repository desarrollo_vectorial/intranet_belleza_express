<div class="titulo">
	<!--<img src="../application/modules/Article/externals/images/etiqueta_noticias.png" alt="">-->
	<div class="tit">
		Etiquetas
		<div class="search"></div>
	</div>	

</div>
<div class="tags">
	<ul>
		<?php foreach ($this->tags as $k => $tag): ?>
		<li>
			<?php 
			echo $this->htmlLink(
				array('route' => 'article_entry_view', 'article_id' => $tag->article_id,
					'user_id' => $this->viewer->user_id
				),
				$tag->text, array()
			); ?>
		</li>
		<?php endforeach; ?>
	</ul>
</div>
