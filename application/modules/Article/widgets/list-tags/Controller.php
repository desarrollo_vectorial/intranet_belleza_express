<?php

class Article_Widget_ListTagsController extends Engine_Content_Widget_Abstract
{

    public function indexAction()
    {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', '1');
        
        $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
        $this->view->tags   = $tags = Engine_Api::_()->getItemTable('article')->getPopularsTags();
        
        if (empty($tags)) {
            return $this->setNoRender();
        }
    }
}