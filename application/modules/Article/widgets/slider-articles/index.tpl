
<style type="text/css">

	.layout_article_slider_home{
		float: left;
	}

	#articles_slider_home{
		width: 730px;
		height: 330px;
		margin: 0 auto;
	}

	#articles_slider_home .images_container{
		width: 480px;
		float: left;
	}

	.article_slider_item_img{
		width: 480px;
		height: 330px;
		display: none;
		margin-bottom: -6px;
	}

	.article_slider_item_img.active{
		display: block;
	}

	#articles_slider_home_selectors{
		width: 250px;
		height: 100%;
		overflow-y: scroll;
	}

	.article_slider_item {
		background-color: #f0efef;
		color: #777777;
		padding: 20px;
		width: 235px;
	}

	.article_slider_item:hover{
		cursor: pointer;
	}

	.article_slider_item p {
		font-weight: 200;
	}

	.article_slider_item.active{
		background-color: #777777;
		color: #FFF;
	}
	
</style>

<div id="articles_slider_home">
	
	<div class="images_container">

		<?php $index = 0; ?>
		<?php foreach ($this->articlesList as $key => $article) : ?>
				
			<a href="<?php echo $article->getHref(); ?>">
				<?php if( $article->photo_id ): ?>
					<img id="article_slider_img_<?php echo $article['article_id']; ?>" class="article_slider_item_img <?php echo (( $index == 0 ) ? 'active' : ''); ?>" src="<?php echo $article->getMainImage(); ?>">
				<?php else: ?>
					<img id="article_slider_img_<?php echo $article['article_id']; ?>" class="article_slider_item_img <?php echo (( $index == 0 ) ? 'active' : ''); ?>" src="application/modules/Video/externals/images/video.png">
				<?php endif; ?>
			</a>
			<?php $index++; ?>

		<?php endforeach; ?>

	</div>

	<div data-current="0" id="articles_slider_home_selectors">

		<?php $index = 0; ?>
		<?php foreach ($this->articlesList as $key => $article) : ?>

			<div data-item-id="<?php echo $article['article_id']; ?>" id="article_slider_item_<?php echo $index; ?>" class="article_slider_item <?php echo (( $index == 0 ) ? 'active' : ''); ?> ">
				<h5><?php echo $article->getShortTitle(13); ?></h5>
				<p><?php echo $article->getDescription(55); ?></p>
			</div>

			<?php $index++; ?>

		<?php endforeach; ?>

	</div>

</div>


<script type="text/javascript">

(function($) { // START NAMESPACE

	$(document).ready(function(){

		// Cambiar el slider
		$('.article_slider_item').click(function(){
			var items = $('.article_slider_item');
			// set active item
			$('.article_slider_item').removeClass('active');
			$(this).addClass('active');
			
			// show image item
			var id = $(this).data('item-id');
			$('.article_slider_item_img').hide();
			$('#article_slider_img_' + id).show();
			
			// set current item
			var current = $(this).attr('id');
			var cur_elm = current.substr(20);
			var curr = parseInt(cur_elm)+1;
			
			if ( curr == items.length ){
				curr = 0;
			}
			$('#articles_slider_home_selectors').data('current', curr);
		});

		// Activar el slider para que cambie cada cierta candidad de segundos
		setInterval(function(){
			var current = $('#articles_slider_home_selectors').data('current');
			var items = $('.article_slider_item');
			// Cambiar de slide simulando el click sobre el item
			var item = $('#article_slider_item_' + current);
			item.trigger('click');
		}, 5000);
		
	});

})(jQuery);
	
</script>