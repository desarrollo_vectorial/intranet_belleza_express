<?php

class Article_Widget_SliderArticlesController extends Engine_Content_Widget_Abstract
{

    public function indexAction()
    {
        $params['featured'] = 1;
        $this->view->articlesList = Engine_Api::_()->getItemTable('article')->getArticlesList($params);
    }
}
