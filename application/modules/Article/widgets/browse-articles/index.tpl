<?php
/**
 * Radcodes - SocialEngine Module
 *
 * @category   Application_Extensions
 * @package    Article
 * @copyright  Copyright (c) 2009-2010 Radcodes LLC (http://www.radcodes.com)
 * @license    http://www.radcodes.com/license/
 * @version    $Id$
 * @author     Vincent Van <vincent@radcodes.com>
 */
?>

<style>
  ul.articles_rows .article_photo{ width: auto !important; }
  ul.articles_rows .article_photo img { width: 90px !important; height: 60px !important; border: 4px solid white !important; }
  ul.articles_rows .article_title h3.article_sponsored_yes, div.article_profile_title .article_profile_title_sponsored_yes{ padding-left: 0 !important; }
  .generic_layout_container .articles_rows li .article_content .article_title h3 span a{ color: #2A94CB !important; }
  ul.articles_rows>li{ border-bottom: 1px solid #a6a6a6; }
  span.article-time { font-size: 12px; }
  a.ver-mas-news { display: inline-block; background: #bfbfc1; padding: 7px; padding-left: 13px; padding-right: 13px; border-radius: 3px; color: #fff; }
</style>

<?php if ($this->paginator->getTotalItemCount()): ?>

  <?php if( $this->tag || $this->user || $this->keyword):?>
    <div class="articles_result_filter_details">
      <?php echo $this->translate('Showing articles posted'); ?>
      <?php if ($this->user): ?>
        <?php echo $this->translate('by user %s', $this->htmlLink(
          array('route'=>'article_browse','user'=>$this->user),
          $this->userObject instanceof User_Model_User ? $this->userObject->getTitle() : '#'.$this->user
        ));?>
      <?php endif; ?>
      <?php if ($this->tag): ?>
        <?php echo $this->translate('using tag #%s', $this->htmlLink(
          array('route'=>'article_browse','tag'=>$this->tag),
          $this->tagObject ? $this->tagObject->text : $this->tag
        ));?>
      <?php endif; ?>
      <?php if ($this->keyword): ?>
        <?php echo $this->translate('with keyword %s', $this->htmlLink(
          $this->url(array('keyword'=>$this->keyword), 'article_browse', true),
          $this->keyword
        ));?>
      <?php endif; ?>
      <?php echo $this->htmlLink(array('route'=>'article_browse'), $this->translate('(x)'));?>
    </div>
  <?php endif; ?>

    <ul class='articles_rows'>
      <?php foreach ($this->paginator as $article): ?>
        <li>
          <?php if ($this->showphoto): ?>
            <div class="article_photo">
              <?php echo $this->htmlLink($article->getHref(), $this->itemPhoto($article, 'thumb.normal'));?>
            </div>
          <?php endif; ?>
          <div class="article_content">
            <div class="article_title">
              <?php echo $this->partial('index/_title.tpl', 'article', array('article' => $article))?>
            </div>

            <?php if ($this->show_date !== false): ?>
              <span class="article-time"><?php echo $this->timestamp($article->creation_date); ?></span>
            <?php endif; ?>

            <?php if ($this->showdescription && $article->getDescription()): ?>
              <div class="article_description">
                <?php echo $this->viewMore($article->getDescription()); ?>
              </div>
            <?php endif; ?>
			      <?php if ($this->showmeta): ?>
              <div class="article_meta">
                <?php $meta_options = array( 'article' => $article, 'show_views' => true); ?>
                <?php echo $this->partial('index/_meta.tpl', 'article', $meta_options)?>
              </div>
            <?php endif; ?>
          </div>
        </li>
      <?php endforeach; ?>
    </ul>	
    <?php echo $this->paginationControl($this->paginator, null, null, array(
      'query' => $this->formValues
    )); ?>
<?php elseif ( $this->tag || $this->keyword || $this->user || $this->category): ?>
  <div class="tip">
    <span>
      <?php echo $this->translate('Nobody has posted an article with that criteria.');?>
    </span>
  </div>
<?php else: ?>
  <div class="tip">
    <span>
      <?php echo $this->translate('Nobody has posted an article yet.');?>
    </span>
  </div>
<?php endif; ?>


<?php if ($this->paginator->getTotalItemCount()): ?>
  <center><a href="articles/browse" class="ver-mas-news"> VER HISTORICO DE NOTICIAS </a></center>
<?php endif; ?>