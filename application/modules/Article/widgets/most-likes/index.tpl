<?php if (count($this->mostviews)): ?>
<h5>Most Likes</h5>
<ul class='article_top_views'>
	<?php foreach( $this->mostviews as $item ): ?>
	<li>
		<div class='blogs_browse_photo'>
			<?php if( $item->getPhotoUrl() ): ?>
				<?php echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, 'thumb.icon', $item->getTitle())) ?>
			<?php else: ?>
				<?php echo $this->htmlLink($item->getHref(), '<img src="application/modules/Video/externals/images/video.png"/>') ?>
			<?php endif; ?>
		</div>
		<div class='blogs_browse_info'>
			<span class='blogs_browse_info_title'>
				<h6>
					<?php echo $this->htmlLink($item->getHref(), $this->string()->truncate($this->string()->stripTags($item->getTitle()), 50)) ?>
				</h6>
			</span>
			<p class='blogs_browse_info_date'>
				<?php echo $this->translate('Posted');?>
				<?php echo $this->timestamp(strtotime($item->creation_date)) ?>
				<?php echo $this->translate('by');?>
				<?php echo $this->htmlLink($item->getOwner()->getHref(), $item->getOwner()->getTitle()) ?>
				<?php echo $item->like_count; ?>
				<?php echo $this->translate('Likes');?>
			</p>
		</div>
	</li>
	<?php endforeach; ?>
</ul>
<?php else: ?>
<div class="tip">
	<span> <?php echo $this->translate('There are no most views yet.');?>
	</span>
</div>
<?php endif; ?>
