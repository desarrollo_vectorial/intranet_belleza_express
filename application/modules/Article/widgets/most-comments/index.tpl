<style type="text/css" media="screen">
	.generic_layout_container .layout_article_most_comments{
		background-color: #e2e2e2 !important;
		margin-bottom: 15px;
		margin-top: 10px;
	}

	.generic_layout_container .layout_article_most_comments .titulo{
		padding   : 20px;
		display   : block;
		position  : relative;
		text-align  : right;
	}

	.generic_layout_container .layout_article_most_comments .titulo .most{
		color 		: #8b8b8a;	
		display 	: inline-block;	
		top 		: 10px;
		font-size 	: 34px;
		line-height : 30px;
		width 		: 200px;
		vertical-align: middle;
	}

	.generic_layout_container .layout_article_most_comments .titulo .most:before{
		content 		: '';
		display 		: block;
		position 		: absolute;
		background-image: url(../application/modules/Article/externals/images/comment-noticias.png);
		width 			: 40px;
		height 			: 40px;
		left 			: 20px;
		top 			: 30px;
	}

	.generic_layout_container .layout_article_most_comments .article_top_views li{
		display: block;
		vertical-align: top;
		padding-left: 15px;
	}
	
	.generic_layout_container .layout_article_most_comments .article_top_views .blogs_browse_photo{	
		display: inline-block;
		margin-top: 10px;
	}

	.generic_layout_container .layout_article_most_comments .article_top_views .blogs_browse_photo img{
		width : 80px;
		height: 60px;
	}

	.generic_layout_container .layout_article_most_comments .article_top_views .blogs_browse_info{
		display: inline-block;
		margin-top: 20px;
		vertical-align: top;
		padding-left: 15px;
	}

	.generic_layout_container .layout_article_most_comments .article_top_views .blogs_browse_info .blogs_browse_info_title a{
		color: #8b8b8a;
		font-size: 18px;
		font-weight: normal;
		/*text-transform: capitalize;*/
		vertical-align: top
	}

	.generic_layout_container .layout_article_most_comments .article_top_views .blogs_browse_info .blogs_browse_info_date{
		color: #ab1607;
		font-size: 14px;
		text-transform: capitalize;
		display: block;
		vertical-align: top;
	}

</style>

<?php if (count($this->mostviews)): ?>

<div class="titulo">

	<div class="most">
		<p style="font-size: 30px !important;">Más</p>
		<b>Comentados</b>
	</div>
	
</div>

<ul class='article_top_views'>
	<?php foreach( $this->mostviews as $item ): ?>
	<li>
		<div class='blogs_browse_photo'>
			<?php if( $item->getPhotoUrl() ): ?>
				<?php echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, 'thumb.icon', $item->getTitle())) ?>
			<?php else: ?>
				<?php echo $this->htmlLink($item->getHref(), '<img src="application/modules/Video/externals/images/video.png" class="video" />') ?>
			<?php endif; ?>
		</div>

		<div class='blogs_browse_info'>
			<span class='blogs_browse_info_title'>
				<h6>
					<?php echo $this->htmlLink($item->getHref(), $this->string()->truncate($this->string()->stripTags($item->getTitle()), 50)) ?>
				</h6>
			</span>
			<p class='blogs_browse_info_date'>
				<!--<?php echo $this->translate('Posted');?>
				<?php echo $this->timestamp(strtotime($item->creation_date)) ?>
				<?php echo $this->translate('by');?>
				<?php echo $this->htmlLink($item->getOwner()->getHref(), $item->getOwner()->getTitle()) ?>-->
				<?php echo $item->comment_count; ?>
				<?php echo $this->translate('Comments');?>
			</p>
		</div>
	</li>
	<?php endforeach; ?>
</ul>
<?php else: ?>
<div class="tip">
	<span> <?php echo $this->translate('There are no most views yet.');?>
	</span>
</div>
<?php endif; ?>
