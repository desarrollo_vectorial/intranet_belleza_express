<?php

class Article_Widget_MostCommentsController extends Engine_Content_Widget_Abstract
{

    public function indexAction()
    {
        // Don't render this if not authorized
        $viewer = Engine_Api::_()->user()->getViewer();
        
        $params = array(
            'published' => 1,
            'search' => 1,
            'limit' => $this->_getParam('max', 5),
            'period' => $this->_getParam('period')
        );
        
        $this->view->mostviews = Engine_Api::_()->article()->getMostComments($params);
        
        if (empty($this->view->mostviews)) {
            return $this->setNoRender();
        }
    }
}