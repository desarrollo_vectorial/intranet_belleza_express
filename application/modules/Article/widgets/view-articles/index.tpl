<?php
/**
 * Widget most views articles
 * Get the most views articles with Google Analytics API
 *
 * @category Application_Extensions
 * @package Article
 * @author Jorge Rodriguez <[jrodriguez@vectorial.co]>
 * @since 1.0 [2016-03-18]
 */
?>
<?php if (count($this->articles) > 0) { ?>
  <div class="generic_layout_container layout_article_list_articles"><h3>Más Vistos</h3>
    <ul class="articles_list">
      <?php foreach ($this->views_articles as $key => $view) { ?>
        <?php foreach ($this->articles as $article) { ?>
          <?php if ($article->article_id == $key) { ?>
            <li>
              <div class="article_photo">
                <?php echo $this->htmlLink($article->getHref(), $this->itemPhoto($article, 'thumb.icon'));?>
              </div>
              <div class="article_content">
                <div class="article_title">
                  <?php echo $this->htmlLink($article->getHref(), $this->radcodes()->text()->truncate($article->getTitle(), 26)) ?>
                </div>
                <div class="article_meta">
                  <ul>
                    <li class="article_meta_views">
                      <?php echo (isset($this->views_articles[$article->article_id])) ? $this->views_articles[$article->article_id] : '0';?> visitas
                    </li>
                  </ul>
                </div>
              </div>
            </li>
          <?php } ?>
        <?php } ?>
      <?php } ?>
    </ul>
  </div>
<?php } else { ?>
  <div class="tip">
    <span>
      <?php echo $this->translate('There are no articles posted yet.');?>
    </span>
  </div>
<?php } ?>