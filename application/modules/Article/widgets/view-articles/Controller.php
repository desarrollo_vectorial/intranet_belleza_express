<?php
/**
 * Widget most views articles
 * Get the most views articles with Google Analytics API
 *
 * @category Application_Extensions
 * @package Article
 * @author Jorge Rodriguez <[jrodriguez@vectorial.co]>
 * @since 1.0 [2016-03-18]
 */
class Article_Widget_ViewArticlesController extends Engine_Content_Widget_Abstract {
	public function indexAction() {
    $page_path = 'articles/[0-9]/*';
    $max_results = 5;
		// Obtener los articulos mas vistos de Google Analytics
    $API_articles = Engine_Api::_()->article()->getMostViewsArticles($page_path, $max_results);
    // Obtener los id y vistas de los articulos de Google Analytics
    foreach ($API_articles as $key => $value) {
      $url_articles = explode('/', $value[0]);
      $id_articles[] = $url_articles[2];
      $views_articles[$url_articles[2]] = $value[1];
    }
    $this->view->views_articles = $views_articles;
    $this->view->id_articles = $id_articles;
    // Obtener los datos de los articulos por id
    $table = Engine_Api::_()->getDbtable('articles', 'article');
    $select = $table->select()->where('article_id IN (?)', $id_articles);
    $articles = $table->fetchAll($select);
    $this->view->articles = $articles;
	}
}