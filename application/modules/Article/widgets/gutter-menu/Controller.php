<?php

class Article_Widget_GutterMenuController extends Engine_Content_Widget_Abstract
{
    public function indexAction()
    {
        // Only article or user as subject
        if (Engine_Api::_()->core()->hasSubject('article')) {
            $this->view->article = $article = Engine_Api::_()->core()->getSubject('article');
            $this->view->owner = $owner = $article->getOwner();
        } else {
            if (Engine_Api::_()->core()->hasSubject('user')) {
                $this->view->article = null;
                $this->view->owner = $owner = Engine_Api::_()->core()->getSubject('user');
            } else {
                return $this->setNoRender();
            }
        }
        // Get navigation
        $this->view->gutterNavigation = $gutterNavigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('article_gutter');
        if (count($this->view->gutterNavigation) == 1) {
            $this->view->gutterNavigation = null;
        }
    }
}
