<style type="text/css" media="screen">
	.layout_article_gutter_photo { background-color: #a7a5a5 !important; display : block; position: relative; }
	.layout_article_gutter_photo .contenedor{ display: flex; padding: 10px; justify-content: left; }
	.layout_article_gutter_photo img{ width : 70px !important; height: 70px !important; display: inline-block; background-size: cover; }
	.layout_article_gutter_photo .span{ padding-top: 20px; padding-left: 15px; color: white !important; }
	.layout_article_gutter_photo .blogs_gutter_name{ color:white !important; }
</style>

<div class="contenedor">
	<?php echo $this->htmlLink($this->owner->getHref(), $this->itemPhoto($this->owner, 'thumb.icon'), array('class' => 'blogs_gutter_photo')); ?>
	
	<div class="span">
		<?php echo $this->htmlLink($this->owner->getHref(),  $this->owner->getTitle(), array('class' => 'blogs_gutter_name')); ?>
		<br>
		<?php echo $this->totOwn; ?> <?php echo $this->translate('Articles');?>
	</div>
</div>