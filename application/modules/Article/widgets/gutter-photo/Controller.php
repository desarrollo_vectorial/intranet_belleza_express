<?php

class Article_Widget_GutterPhotoController extends Engine_Content_Widget_Abstract
{

    public function indexAction()
    {
        // Only article or user as subject
        if (Engine_Api::_()->core()->hasSubject('article')) {
            $this->view->article = $article = Engine_Api::_()->core()->getSubject('article');
            $this->view->owner   = $owner   = $article->getOwner();
            $this->view->totOwn  = $totOwn  = Engine_Api::_()->article()->getTotByUser($owner->user_id);
        } else {
            if (Engine_Api::_()->core()->hasSubject('user')) {
                $this->view->article = null;
                $this->view->owner   = $owner  = Engine_Api::_()->core()->getSubject('user');
                $this->view->totOwn  = $totOwn  = Engine_Api::_()->article()->getTotByUser($owner->user_id);
            } else {
                return $this->setNoRender();
            }
        }
    }
}
