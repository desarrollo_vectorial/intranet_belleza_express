<?php

class Article_Widget_ListArticlesController extends Engine_Content_Widget_Abstract
{

    public function indexAction()
    {
        // Note: this code is duplicated in the article.browse-search widget
        $this->view->form = $form = new Article_Form_Search();
        
        // Populate form
        $categories = Engine_Api::_()->getDbtable('categories', 'article')->getCategoriesAssoc();
        if (! empty($categories) && is_array($categories) && $form->getElement('category')) {
            $form->getElement('category')->addMultiOptions($categories);
        }
        
        // Process form
        $defaultValues = $form->getValues();
        if ($form->isValid($this->_getAllParams())) {
            $values = $form->getValues();
        } else {
            $values = $defaultValues;
        }
        $values['page'] = $_GET['page'];
        $values['published'] = null;
        
        $this->view->formValues = array_filter($values);
        $values['published'] = "1";
        
        // Do the show thingy
        if (@$values['show'] == 2) {
            // Get an array of friend ids
            $table = Engine_Api::_()->getItemTable('user');
            $select = $viewer->membership()->getMembersSelect('user_id');
            $friends = $table->fetchAll($select);
            // Get stuff
            $ids = array();
            foreach ($friends as $friend) {
                $ids[] = $friend->user_id;
            }
            // unset($values['show']);
            $values['users'] = $ids;
        }
        
        $this->view->assign($values);
        
        // Get articles
        $paginator = Engine_Api::_()->getItemTable('article')->getArticlesPaginator($values);
        
        $items_per_page = Engine_Api::_()->getApi('settings', 'core')->article_page;
        // $paginator->setItemCountPerPage($items_per_page);
        
        $this->view->paginator = $paginator->setCurrentPageNumber($values['page']);
    }
}

