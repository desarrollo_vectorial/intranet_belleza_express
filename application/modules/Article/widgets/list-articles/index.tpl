<?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
  <ul class="blogs_browse">
    <?php foreach( $this->paginator as $item ): ?>
      <li>
        <div class='blogs_browse_photo'>
          <?php echo $this->htmlLink($item->getOwner()->getHref(), $this->itemPhoto($item->getOwner(), 'thumb.icon')) ?>
        </div>
        <div class='blogs_browse_info'>
          <span class='blogs_browse_info_title'>
            <h3><?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?></h3>
          </span>
          <p class='blogs_browse_info_date'>
            <?php echo $this->translate('Publicado');?>
            <?php echo $this->timestamp(strtotime($item->creation_date)) ?>
            <?php echo $this->translate('por');?>
            <?php echo $this->htmlLink($item->getOwner()->getHref(), $item->getOwner()->getTitle()) ?>
            <?php echo $item->getCommentsCount(); ?>
            <?php echo $this->translate('Comentarios');?>
            <?php echo $item->getLikesCount(); ?>
            <?php echo $this->translate('Likes');?>
            <?php echo $item->getViewsCount(); ?>
            <?php echo $this->translate('Visitas');?>
          </p>
          <p class='blogs_browse_info_blurb'>
            <?php echo $this->string()->truncate($this->string()->stripTags($item->description), 300) ?>
          </p>
        </div>
      </li>
    <?php endforeach; ?>
  </ul>

<?php elseif( $this->category || $this->show == 2 || $this->search ): ?>
  <div class="tip">
    <span>
      <?php echo $this->translate('Nobody has written a article entry with that criterio.');?>
      <?php if (TRUE): // @todo check if user is allowed to create a poll ?>
        <?php echo $this->translate('Be the first to %1$swrite%2$s one!', '<a href="'.$this->url(array('action' => 'create'), 'article_general').'">', '</a>'); ?>
      <?php endif; ?>
    </span>
  </div>

<?php else:?>
  <div class="tip">
    <span>
      <?php echo $this->translate('Nobody has written a article entry yet.'); ?>
      <?php if( $this->canCreate ): ?>
        <?php echo $this->translate('Be the first to %1$swrite%2$s one!', '<a href="'.$this->url(array('action' => 'create'), 'article_general').'">', '</a>'); ?>
      <?php endif; ?>
    </span>
  </div>
<?php endif; ?>

<?php echo $this->paginationControl($this->paginator, null, null, array(
  'pageAsQuery' => true,
  'query' => $this->formValues,
  //'params' => $this->formValues,
)); ?>