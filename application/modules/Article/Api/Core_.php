<?php

class Article_Api_Core extends Core_Api_Abstract
{

    public function getTopSubmitters($params = array()){
        $column = 'owner_id';
        
        $table = Engine_Api::_()->getDbtable('articles', 'article');
        $rName = $table->info('name');
        
        $select = new Zend_Db_Select($table->getAdapter());
        $select->from($table->info('name'), array(
            'user_id' => $column,
            'total' => new Zend_Db_Expr('COUNT(*)')
        ));
        $select->group($column);
        
        $select->order('total desc');
        
        if (isset($params['limit'])) {
            $select->limit($params['limit']);
            unset($params['limit']);
        }
        
        $select = $table->selectParamBuilder($params, $select);
        $rows = $select->query()->fetchAll();
        
        $result = array();
        foreach ($rows as $row) {
            $result[$row['user_id']] = $row;
        }
        
        return $result;
    }

    public function getMostViews($params = array())
    {
        $table = Engine_Api::_()->getDbtable('articles', 'article');
        $select = $table->select($params)
            ->where('published = ? ', 1)
            ->order('like_count desc')
            ->limit(5);
        
        return $table->fetchAll($select);
    }
    
    public function getMostComments($params = array())
    {
        $table = Engine_Api::_()->getDbtable('articles', 'article');
        $select = $table->select($params)
        ->where('published = ? ', 1)
        ->order('comment_count desc')
        ->limit(5);
    
        return $table->fetchAll($select);
    }

    public function getTotByUser($user_id){
        $tableNoticias = Engine_Api::_()->getDbtable('articles', 'article');
        $noticias = $tableNoticias->fetchAll($tableNoticias->select()->where('owner_id = ?', $user_id));
        return count($noticias);
    }

}
