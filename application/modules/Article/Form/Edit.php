<?php

class Article_Form_Edit extends Article_Form_Create
{

    protected $_item;

    public function getItem()
    {
        return $this->_item;
    }

    public function setItem(Core_Model_Item_Abstract $item)
    {
        $this->_item = $item;
        return $this;
    }

    public function init()
    {
        parent::init();
        $this->setTitle('')->setDescription('');
        $this->photo->setRequired(false);

        // Submit or succumb!
        $this->addElement('Button', 'submit', array(
            'label' => 'Guardar Cambios',
            'type' => 'submit',
            'setAttrib' => 'href', $this->_item->getHref(),
            'decorators' => array(
                'ViewHelper'
            )
        ));

        $this->addElement('Cancel', 'cancel', array(
            'label' => 'view',
            'link' => true,
            'decorators' => array(
                'ViewHelper'
            )
        ));

        $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');

    }
}