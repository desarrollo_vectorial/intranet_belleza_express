<?php

class Article_Form_Create extends Engine_Form
{

    public $_error = array();

    public function init(){
        $this->setTitle('')
             ->setDescription('')
             ->setAttrib(array(
                'name'  => 'article_create',
                'class' => 'form-crear',
            )
        );

        $user       = Engine_Api::_()->user()->getViewer();
        $user_level = Engine_Api::_()->user()->getViewer()->level_id;

        // Foto
        $this->addElement('File', 'photo', array(
            'label'       => 'Foto principal',
            'description' => 'Foto principal home (300 * 200 px).',
            'required'    => true,
            'class'       => 'form-picture'
        ));
        $this->photo->addValidator('Extension', false, 'jpg,png,gif');


        // Foto
        /*$this->addElement('File', 'photoint', array(
            'label'       => 'Foto interna',
            'description' => 'Foto interna (910px * alto deseado).',
            'required'    => true,
            'class'       => 'form-picture'
        ));
        $this->photoint->addValidator('Extension', false, 'jpg,png,gif');*/




        if (Engine_Api::_()->authorization()->isAllowed('album', $user, 'create')) {
            $upload_url = Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'upload-photo'), 'blog_general', true);
        }
        $allowed_html = Engine_Api::_()->authorization()->getPermission($user_level, 'article', 'auth_html');
        $upload_url   = "";


        

        
        //Titulo 
        $this->addElement('Text', 'title', array(
            'label'      => 'Title',
            'allowEmpty' => false,
            'required'   => true,
            'filters'    => array(
                new Engine_Filter_Censor(), 'StripTags',
                new Engine_Filter_StringLength(array(
                    'max' => '63'
                ))
            ),
            'autofocus'  => 'autofocus',
            'class'      => 'form-text'
        ));

        
        // Etiquetas
        $this->addElement('Text', 'tags', array(
            'label'        => 'Tags (Keywords)',
            'autocomplete' => 'off',
            'description'  => 'Separar los tags con comas.',
            'filters'      => array(
                new Engine_Filter_Censor()
            ),
            'class'    => 'form-text'
        ));
        $this->tags->getDecorator("Description")->setOption("placement", "append");

        
        // Categorias
        $categories = Engine_Api::_()->getDbtable('categories', 'article')->getCategoriesAssoc();
        if (count($categories) > 0) {
            $this->addElement('Select', 'category_id', array(
                'label'        => 'Category',
                'multiOptions' => $categories,
                'class'        => 'form-select'
            ));
        }


        // Opciones
        $availableLabels = array(
            'everyone'            => 'Everyone',
            'registered'          => 'All Registered Members',
            'owner_network'       => 'Friends and Networks',
            'owner_member_member' => 'Friends of Friends',
            'owner_member'        => 'Friends Only',
            'owner'               => 'Just Me'
        );
        
        // Element: auth_view
        $viewOptions = (array) Engine_Api::_()->authorization()->getAdapter('levels')->getAllowed('article', $user, 'auth_view');
        $viewOptions = array_intersect_key($availableLabels, array_flip($viewOptions));
        
        if (! empty($viewOptions) && count($viewOptions) >= 1) {
            // Make a hidden field
            if (count($viewOptions) == 1) {
                $this->addElement('hidden', 'auth_view', array('value' => key($viewOptions)));
                // Make select box
            } else {
                $this->addElement('Select', 'auth_view', array(
                    'label'        => 'Privacy',
                    'description'  => '¿Quién puede ver esta noticia?',
                    'multiOptions' => $viewOptions,
                    'value'        => key($viewOptions),
                    'class'        => 'form-select'
                ));
                $this->auth_view->getDecorator('Description')->setOption('placement', 'append');
            }
        }
        
        // Element: auth_comment
        $commentOptions = (array) Engine_Api::_()->authorization()->getAdapter('levels')->getAllowed('article', $user, 'auth_comment');
        $commentOptions = array_intersect_key($availableLabels, array_flip($commentOptions));
        
        if (! empty($commentOptions) && count($commentOptions) >= 1) {
            // Make a hidden field
            if (count($commentOptions) == 1) {
                $this->addElement('hidden', 'auth_comment', array('value' => key($commentOptions)));
                // Make select box
            } else {
                $this->addElement('Select', 'auth_comment', array(
                    'label'        => 'Comment Privacy',
                    'description'  => '¿Quién puede publicar comentarios sobre esta noticia?',
                    'multiOptions' => $commentOptions,
                    'value'        => key($commentOptions),
                    'class'        => 'form-select'
                ));
                $this->auth_comment->getDecorator('Description')->setOption('placement', 'append');
            }
        }


        // Estado publicacion
        $this->addElement('Select', 'published', array(
            'label'        => 'Status',
            'multiOptions' => array(
                "0"     => "Saved As Draft",
                "1"     => "Published"                
            ),
            'description' => 'If this entry is published, it cannot be switched back to draft mode.',
            'class'       => 'form-select'
        ));
        $this->published->getDecorator('Description')->setOption('placement', 'append');
        

        // Mostrar en busquedas
        $this->addElement('Checkbox', 'search', array(
            'label' => 'Mostrar esta noticias en los resultados de búsqueda.',
            'value' => 1
        ));
        

        // Destacada
        $this->addElement('Checkbox', 'featured', array(
            'label' => 'Esta noticia es Destacada.',
            'value' => 0
        ));


        // Descripcion
        $this->addElement('Textarea', 'description', array(
            'label' => 'Short Description',
            'description' => "You may optionally enter article's excerpt or summary below"
        ));
        $this->description->getDecorator("Description")->setOption("placement", "prepend");      
        
        
        
		$editorOptions = array();
        /*$editorOptions = array(
            'upload_url' => $upload_url,
            'html'       => (bool) $allowed_html
        );
        
        if (! empty($upload_url)) {
            $editorOptions['plugins']  = array('table', 'fullscreen', 'media', 'preview', 'paste', 'code', 'image', 'textcolor', 'jbimages', 'link');            
            $editorOptions['toolbar1'] = array('undo', 'redo', 'removeformat', 'pastetext', '|', 'code', 'media', 'image', 'jbimages', 'link', 'fullscreen', 'preview');
        }*/
        
        $this->addElement('Textarea', 'body', array(
            'label'         => 'Cuerpo de la Noticia',
            //'disableLoadDefaultDecorators' => true,
            'required'      => true,
            'allowEmpty'    => false,
            //'decorators'    => array('ViewHelper'),
            'editorOptions' => $editorOptions,
            'class' => 'richtext',
            /*'filters'       => array(
                new Engine_Filter_Censor(),
                new Engine_Filter_Html(array('AllowedTags' => $allowed_html))
            )*/
        ));


        // Element: submit
        $this->addElement('Button', 'submit', array(
            'label'      => 'Post Article',
            'type'       => 'submit',
            'ignore'     => true,
        ));
    }

    public function postEntry(){
        var_dump('postEntry');
        exit();
        $values = $this->getValues();
        
        $user        = Engine_Api::_()->user()->getViewer();
        $title       = $values['title'];
        $body        = $values['body'];
        $category_id = $values['category_id'];
        $tags        = preg_split('/[,]+/', $values['tags']);
        
        $db = Engine_Db_Table::getDefaultAdapter();
        $db->beginTransaction();
        try {
            // Transaction
            $table = Engine_Api::_()->getDbtable('articles', 'article');
            
            // insert the blog entry into the database
            $row = $table->createRow();
            $row->owner_id      = $user->getIdentity();
            $row->owner_type    = $user->getType();
            $row->category_id   = $category_id;
            $row->creation_date = date('Y-m-d H:i:s');
            $row->modified_date = date('Y-m-d H:i:s');
            $row->title         = $title;
            $row->body          = $body;
            $row->save();
            
            $article_id = $row->article_id;
            
            if ($tags) {
                $this->handleTags($article_id, $tags);
            }
            
            $attachment = Engine_Api::_()->getItem($row->getType(), $article_id);
            $action     = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($user, $row, 'article_new');
            Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $attachment);
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }
    }

    public function handleTags($blog_id, $tags){
        var_dump('handleTags');
        exit();
        $tagTable    = Engine_Api::_()->getDbtable('tags', 'blog');
        $tabMapTable = Engine_Api::_()->getDbtable('tagmaps', 'blog');
        $tagDup      = array();
        foreach ($tags as $tag) {
            
            $tag = htmlspecialchars((trim($tag)));
            if (! in_array($tag, $tagDup) && $tag != "" && strlen($tag) < 20) {
                $tag_id = $this->checkTag($tag);
                // check if it is new. if new, createnew tag. else, get the tag_id and insert
                if (! $tag_id) {
                    $tag_id = $this->createNewTag($tag, $blog_id, $tagTable);
                }
                
                $tabMapTable->insert(array(
                    'blog_id' => $blog_id,
                    'tag_id'  => $tag_id
                ));
                $tagDup[] = $tag;
            }
            if (strlen($tag) >= 20) {
                $this->_error[] = $tag;
            }
        }
    }

    public function checkTag($text){
        $table   = Engine_Api::_()->getDbtable('tags', 'blog');
        $select  = $table->select()->order('text ASC')->where('text = ?', $text);
        $results = $table->fetchRow($select);
        $tag_id  = "";
        if ($results)
            $tag_id = $results->tag_id;

        return $tag_id;
    }

    public function createNewTag($text, $blog_id, $tagTable){
        $row       = $tagTable->createRow();
        $row->text = $text;
        $row->save();
        $tag_id = $row->tag_id;
        
        return $tag_id;
    }

}