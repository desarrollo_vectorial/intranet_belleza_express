<?php

class Article_Form_Search extends Engine_Form
{

    public function init()
    {
        $this->setAttribs(array(
            'id'    => 'filter_form',
            'class' => 'global_form_box'
        ))
            ->setAction(Zend_Controller_Front::getInstance()->getRouter()
            ->assemble(array()))
            ->setMethod('GET');
        
        $this->addElement('Text', 'search', array(
            'onchange'    => 'this.form.submit();',
            'placeholder' => 'Titulo',
            'class'       => 'form_titulo'
        ));
        
        $this->addElement('Select', 'orderby', array(
            'multiOptions' => array(
                'creation_date' => 'Most Recent',
                'view_count'    => 'Most Viewed'
            ),
            'onchange' => 'this.form.submit();'
        ));
        
        $this->addElement('Select', 'draft', array(
            'multiOptions' => array(
                ''  => 'All Entries',
                '0' => 'Only Published Entries',
                '1' => 'Only Drafts'
            ),
            'onchange' => 'this.form.submit();'
        ));
        
        $this->addElement('Select', 'show', array(
            'multiOptions' => array(
                '1' => 'Todos los artículos',
                '2' => 'Solo los artículos de mis amigos'
            ),

            'class'    => 'form_select'
        ));
        
        $this->addElement('Select', 'category', array(
            'multiOptions' => array(
                '0' => 'All Categories'
            ),

            'class'    => 'form_select'
        ));
        
        $this->addElement('Hidden', 'page', array(
            'order' => 100
        ));
        
        $this->addElement('Hidden', 'tag', array(
            'order' => 101
        ));
        
        /*$this->addElement('Date', 'start_date', array(
            'order' => 102,
            'label' => 'Start Date',
            'min' => '2012-01-01',
            'max' => '2020-01-01',
        ));
        
        $this->addElement('Date', 'end_date', array(
            'order' => 103,
            'label' => 'End Date',
            'min' => '2012-01-01',
            'max' => '2020-01-01',
        ));*/
        
        $this->addElement('Button', 'submit', array(
            'order'  => 104,
            'label'  => 'Buscar',
            'type'   => 'submit',
            'class'  => 'form_boton',
            'ignore' => true,
        ));
    }
}