var $ = jQuery;

//Abre y cierra acordeón
function alternar(id) {
    var e = document.getElementById(id);
    if (e.style.display == 'block')
        e.style.display = 'none';
    else
        e.style.display = 'block';
}


$("div.acordeon-titulo").click(function () {
    $(this).children("div.acordeon-flecha").toggleClass("rotada");
});


var main = function () {

    var paused = false

    $('.arrowR').click(function () {
        paused = true;
        $('#slideshow > div:first')
            .fadeOut(1000)
            .next()
            .fadeIn(1000).attr("style", "display: block !important")
            .end()
            .appendTo('#slideshow');
    });

    $('.arrowL').click(function () {
        paused = true;
        $('#slideshow > div:last')
            .fadeIn(1000).attr("style", "display: block !important")
            .prependTo('#slideshow')
            .next()
            .fadeOut(1000)
            .end();
    });


    setInterval(function () {
        if (paused === false) {
            $('#slideshow > div:first')
                .fadeOut(1000)
                .next()
                .fadeIn(1000).attr("style", "display: block !important")
                .end()
                .appendTo('#slideshow');
        }
        ;
    }, 5000);


};

$(document).ready(main);