<?php

class Contents_IndexController extends Core_Controller_Action_Standard
{
    public function init() {
        $viewer = Engine_Api::_()->user()->getViewer();
        $this->view->identity = $viewer->getIdentity();
    }

	public function adminAction() {

		$viewer = Engine_Api::_()->user()->getViewer();		
		if(!$viewer->isAdmin()) {
			die('Acceso no autorizado');
		}
        
        $tabla   = Engine_Api::_()->getDbtable('pages', 'contents');
        $select  = $tabla->select()->order('page_section')->order('page_order');
        $this->view->pages = $pages = $tabla->fetchAll($select)->toArray();
	}

    public function createAction(){
        $this->_helper->content->setEnabled();
        $viewer = Engine_Api::_()->user()->getViewer();
		$api = Engine_Api::_()->contents();
		
		$page_section = $this->_getParam('page_section');

		if(!$viewer->isAdmin()) {
			die('Acceso no autorizado');
		}

        $this->view->form = $form = new Contents_Form_Create();
        $form->setAction($this->view->url());

        $form->page_parent_id->setMultiOptions($api->getSectionTree($page_section));//$this->_optionsArray);
        $total = $api->obtenerOrden();
        $next_order = $total[0]['max_order'] + 1;

        $ordenArray = array();
        $ordenArray[$next_order] = $next_order;

        $form->page_order->setMultiOptions($ordenArray);

        if ($this->getRequest()->isPost()) {

            if ($form->isValid($this->getRequest()->getPost())) {
            	
                $table = $this->_helper->api()->getDbtable('pages', 'contents');
                $values = array_merge($form->getValues(), array('page_state' => '1', 'page_section' => $page_section));
                $page = $table->createRow();
				$values['page_parent_id'] = $values['page_parent_id'] ? $values['page_parent_id'] : 0;

                $page->setFromArray($values);
                $page->save();
                if (!empty($values['page_banner'])) {
                    $page->guardarImagen($this->view->form->page_banner);
                }

                return $this->_redirect('content/edit/'.$page_section.'/'.$values['page_url']);
            }
        }
    }


    /*function general para eliminar paginas sin eliminar de la base de datos*/
    public function stateAction() {

        $this->_helper->content->setEnabled();
        $viewer = Engine_Api::_()->user()->getViewer();

        if(!$viewer->isAdmin()) {
            die('Acceso no autorizado');
        }

        $page_id = $this->_getParam('page_id');

        $pagesTable = Engine_Api::_()->getDbtable('pages', 'contents');
        $page = $pagesTable->find($page_id)->current();


        $datos = array();
        $datos['page_state'] = '0';
	    $datos['show_menu'] = '0';

        $db = $pagesTable->getAdapter();
        $db->beginTransaction();

        try	{
            $page->setFromArray($datos);
            $page->save();
        }catch( Exception $e ){
            $db->rollBack();
            throw $e;
            echo false;
        }

        $db->commit();

        return $this->_redirect('content/admin');
    }

    public function editAction(){
        $this->_helper->content->setEnabled();
        $viewer = Engine_Api::_()->user()->getViewer();

		if(!$viewer->isAdmin()) {
			die('Acceso no autorizado');
		}

		$page_url = $this->_getParam('page_url');
		$page_section = $this->_getParam('page_section');		
		
        $pagesTable = Engine_Api::_()->getDbtable('pages', 'contents');
		$select = $pagesTable->select()-> where( "page_url = '".$page_url."' AND page_section = '".$page_section."'");
		$res = $pagesTable->fetchAll($select);
		foreach($res as $row) {
			$page = $row;
		}
		
        $this->_page_id = $page_id = $page->page_id;
        $pageTable = Engine_Api::_()->getDbtable('pages', 'contents');

        if (!$page) {
            die('Página no encontrada');
        }
        $api = Engine_Api::_()->contents();

        // Formulario
        $this->view->form = $form = new Contents_Form_Create();

        $form->page_parent_id->setMultiOptions( array_replace(array(0 => '----------'),$api->getSectionTree($page_section)));
        $total = $api->obtenerOrden();

        $ordenArray = array();
        for ($i = 0;$i < $total[0]['max_order'];++$i) {
            $ordenArray[$i + 1] = $i + 1;
        }
        $form->page_order->setMultiOptions($ordenArray);

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $values = $form->getValues();

                if (!empty($values['page_banner'])) {
                    $page->guardarImagen($this->view->form->page_banner, $values['page_parent_id']);
                }
                $page->setFromArray($form->getValues());
                $page->save();
            }
        }

        $form->setTitle('Editar Pagina')
            ->setDescription('Edición de contenido')
            ->setAttrib('name', 'edit_page');

        $id = new Zend_Form_Element_Hidden('page_id');
        $form->addElement($id);
        $form->page_id->setValue($page->page_id);
        $form->page_url->setValue($page->page_url);
        $form->page_name->setValue($page->page_name);
        $form->page_parent_id->setValue($page->page_parent_id);
        $form->page_content->setValue($page->page_content);
        $form->page_order->setValue($page->page_order);
        $form->cancel->setAttrib('href', '/content/'.$page_section.'/'.$page_url);
	    $form->cancel->setAttrib('target', '_blank');
	    $form->cancel->setAttrib('class', 'btn custom-btn-ver');
        $form->cancel->setLabel('Ver página');
    }
	
	public function indexAction() {      
        $this->_helper->content->setEnabled();
        $page_id = 1;
        $tabla   = Engine_Api::_()->getDbtable('pages', 'contents');
        $select  = $tabla->select()->where('page_parent_id = ?', $page_id)->order('page_order');
        $this->view->pages = $pages = $tabla->fetchAll($select)->toArray();
	}

	public function stylesAction() {
		
		$this->view->form = $form = new Contents_Form_Styles();
		
		$stylesFilename = 'application/modules/Contents/externals/styles/main.css';
		$stylesContent = file_get_contents($stylesFilename, true);
		
		$form->styles->setValue($stylesContent);
		
        if ($this->getRequest()->isPost()) {

            if ($form->isValid($this->getRequest()->getPost())) {
            	
				$values = $form->getValues();
				file_put_contents($stylesFilename,$values['styles']);

                return $this->_redirect('content/styles');
            }
        }
	}

	public function viewAction() {
		
		$api = Engine_Api::_()->contents();
		
		$page_url = $this->_getParam('page_url');
		$page_section = $this->_getParam('page_section');
		
        $pagesTable = Engine_Api::_()->getDbtable('pages', 'contents');
		$select = $pagesTable->select()-> where( "page_url = '".$page_url."' AND page_section = '".$page_section."'");
		
		if($page = $pagesTable->fetchAll($select)->toArray()) {
			$page = $page[0];
		}
		$page_id = $page['page_id'];

        $migas = array();
        $migas = $api->menuMigas($page_id, 'current');
		
		$htmlMenu = $this->_getPageMenu($page_section, $page_id);
		$page['page_content'] = str_replace("__MENU__", $htmlMenu, $page['page_content']);

        $this->view->migas 		  = $migas;
        $this->view->page_name    = $page['page_name'];
        $this->view->page_content = $page['page_content'];
        $this->view->page_id      = $page_id;
        $this->view->page_section = $page['page_section'];
        $this->view->page_url     = $page['page_url'];
		
        $viewer = Engine_Api::_()->user()->getViewer();
        if ($viewer->isModSuperAdmin()) {
            $this->view->showButtons = true;
        }

        $this->_helper->content->setEnabled();
	}	

	private function _getPageMenu($page_section, $page_id){

		$viewer = Engine_Api::_()->user()->getViewer();

        $this->_page_id = $page_id;
        $api = Engine_Api::_()->contents();

        $table = Engine_Api::_()->getDbtable('pages', 'contents');
        $pages = $table->find($this->_page_id)->current();
		
		$elMenu = $api->section_menu($page_section);

        $html = '<ul>';
        foreach ($elMenu as $key => $item) {

            if($item['page_id'] == $this->_page_id)
                $clase = ' class="menu-activo-1"';
            else
                $clase = ' class=""';

            $html .= '<li '.$clase.'>';

            if(isset($item['children']) && count($item['children']) > 0){
                if($item['page_id'] == $this->_page_id)
                    $clase = ' id="menu-activo1"';
                else
                    $clase = ' id=""';
               
               	$clase = '';
				
                $html .= '<li '.$clase.'><label href="javascript:void();" class="mas" >'.$item['page_name'].'</label>';
                $html .= '<ul>';

                foreach ($item['children'] as $key2 => $item2){

					$visible1 = ($item2['show_menu']==1 ? '' : 'style="display:none"');

                    if($item2['page_id'] == $this->_page_id)
                        $clase = ' class="menu-activo"';
                    else
                        $clase = ' class=""';

                    $html .= '<li '.$clase.' '.$visible1.'>';

                    if(isset($item2['children']) && count($item2['children']) > 0){
                        $html .= '<label href="javascript:void();" class="mas">'.$item2['page_name'].'</label>';
                        $html .= '<ul>';

                        foreach ($item2['children'] as $key3 => $item3) {
                            if($item3['page_id'] == $this->_page_id)
                                $clase = ' class="menu-activo"';
                            else
                                $clase = ' class=""';
							
							$html .= '<li '.$clase.'>';
							
							
		                    if(isset($item3['children']) && count($item3['children']) > 0){
		                        $html .= '<label href="javascript:void();" class="mas">'.$item3['page_name'].'</label>';
		                        $html .= '<ul>';
		
		                        foreach ($item3['children'] as $key4 => $item4) {
		                            if($item4['page_id'] == $this->_page_id)
		                                $clase = ' class="menu-activo"';
		                            else
		                                $clase = ' class=""';
		
		                            $html .= '<li '.$clase.'><a href="/content/'.$item4['page_section'].'/'.$item4['page_url'].'">'.$item4['page_name'].'</a></li>';
		                        }
		
		
		                        $html .= '</ul></li>';
		
		
		                    }else{
		
		                        $html .= '<a href="/content/'.$item3['page_section'].'/'.$item3['page_url'].'">'.$item3['page_name'].'</a>';
		                        $html .="</li>";
		                    }							
                        }
                        $html .= '</ul></li>';
                    }else{

                        $html .= '<a href="/content/'.$item2['page_section'].'/'.$item2['page_url'].'">'.$item2['page_name'].'</a>';
                        $html .="</li>";
                    }
                }
                $html .= '</ul>';

            }else{
                $html .= '<a href="/content/'.$item['page_section'].'/'.$item['page_url'].'">'.$item['page_name'].'</a>';
                $html .= "</li>";
            }
        }
		
		return '<div class="menujl">'. $html .'</div>';
    }
}

