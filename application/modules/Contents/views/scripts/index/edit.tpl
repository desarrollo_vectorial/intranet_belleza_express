<div class="contents--admin">
	<div class="row">
		<div class="col">
			<h2>Editar página corporativa</h2>
			<p>
                En este espacio, podrás editar una página nueva.
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col">
            <?php echo $this->htmlLink(array('action'=>'admin', 'route'=>'contents_index_admin'), 'Ir al administrador', array('class' => 'btn custom-btn-secondary')); ?>
		</div>
	</div>
	<div class="row">
		<div class="col">
            <?php
            	echo $this->form->render($this);
            ?>
		</div>
	</div>
</div>

<?php echo $this->partial('/application/modules/Core/views/scripts/_richeditor.tpl', array()); ?>