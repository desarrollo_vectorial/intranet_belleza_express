<div class="contents--admin">
    <div class="row">
        <div class="col">
            <h2>Administración de páginas corporativas.</h2>
            <p>
                En este espacio, podrá editar las secciones corporativas, eliminarlas y/o crear una página nueva.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="new-section">Nueva sección</span>
                </div>
                <input type="text" class="form-control" aria-label="nueva_seccion" aria-describedby="new-section" id="nueva_seccion" placeholder="Nombre de la sección" />
            </div>
        </div>
        <div class="col-3 text-left">
            <a href="#" class="btn custom-btn-primary" onclick="return createSection()">Crear sección</a>
        </div>
        <div class="col-3 text-right">
            <?php echo $this->htmlLink(array('action'=>'styles', 'route'=>'contents_index_styles'), 'Actualizar estilos', array('target' => '_blank', 'class' => 'btn custom-btn-secondary')); ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <h4>Lista de secciones y páginas:</h4>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr class="thead-light">
                    <th scope="col">Sección</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">URL</th>
                    <th scope="col">Orden</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($this->pages as $row): ?>
                    <?php if ($row['page_state'] == '1'): ?>
                        <?php if($row['page_parent_id'] == 0): ?>
                            <tr class="table-active">
                        <?php else: ?>
                            <tr>
                        <?php endif; ?>

                            <td scope="row"><?php echo $row['page_section']; ?></td>
                            <td><?php echo $row['page_name']; ?></td>
                            <td><?php echo $row['page_url']; ?></td>
                            <td><?php echo $row['page_order']; ?></td>
                            <td>
                                <?php echo $this->htmlLink(array('action'=>'edit', 'page_section'=>$row['page_section'], 'page_url'=>$row['page_url'], 'route'=>'contents_index_edit'), $this->translate('edit'), array('class' => 'btn btn-info')); ?>
                            </td>
                            <td>
                                <?php echo $this->htmlLink(array('action'=>'state', 'page_id'=>$row['page_id'], 'route'=>'contents_index_state'), $this->translate('Eliminar'), array('class' => 'btn btn-danger')); ?>
                            </td>
                            <td>
                                <a class="btn btn-secondary" href="/content/<?php echo $row['page_section']; ?>/<?php echo $row['page_url']; ?>" target="_blank">Ver</a>
                            </td>
                            <td>
                                <?php if($row['page_parent_id'] == 0): ?>
                                    <?php echo $this->htmlLink(array('action'=>'create', 'page_section'=>$row['page_section'], 'route'=>'contents_index_create'), 'Nueva página', array('class' => 'btn custom-btn-primary')); ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div><!--.contents--admin-->

<script>
	function createSection() {
		window.location = window.location.href.replace("content/admin", "content/create/" + jQuery("#nueva_seccion").val());
		
		return false;
	}
</script>