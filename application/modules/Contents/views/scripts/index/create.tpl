<div class="contents--admin">
    <div class="row">
        <div class="col">
            <h2>Crear página corporativa</h2>
            <p>
                En este espacio, podrás crear una página nueva.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?php echo $this->htmlLink(array('action'=>'admin', 'route'=>'contents_index_admin'), 'Ir al administrador', array('class' => 'btn custom-btn-secondary')); ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?php
            echo $this->form->render($this);
            ?>
        </div>
    </div>
</div>

<script>
	function generate_friendly_url() {
		var keyword = jQuery('#page_name').val().replace(/[^A-Za-z0-9 ]/g,'');
	 
	    /* Replace multi spaces with a single space */
	    keyword = keyword.replace(/\s{2,}/g,' ');
	 
	    /* Replace space with a '-' symbol */
	    keyword = keyword.replace(/\s/g, "-");
	    
	    keyword.toLowerCase();
	    
	    jQuery('#page_url').val(keyword);		
	}

</script>

<?php echo $this->partial('/application/modules/Core/views/scripts/_richeditor.tpl', array()); ?>