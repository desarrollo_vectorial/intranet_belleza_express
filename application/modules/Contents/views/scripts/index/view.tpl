<?php
$this->headLink()->appendStylesheet($this->baseUrl() . '/application/modules/Contents/externals/styles/accordion.css');
/*$this->headLink()->appendStylesheet($this->baseUrl().'/application/modules/Contents/externals/styles/tabs.css');*/
$this->headLink()->appendStylesheet('https://fonts.googleapis.com/css?family=Roboto:300,400,700');
$this->headScript()->appendFile($this->BaseUrl() . '/externals/jQuery/jquery_1.11.3.js');
$this->headScript()->appendFile($this->BaseUrl() . '/application/modules/Contents/externals/scripts/funciones.js');
/*TABS RESPONSIVE AND PAGINATOR*/
$this->headScript()->appendFile($this->BaseUrl() . '/application/modules/Contents/externals/scripts/tabs-paginator/jquery.responsiveTabs.js');
$this->headScript()->appendFile($this->BaseUrl() . '/application/modules/Contents/externals/scripts/tabs-paginator/template.js');
$this->headScript()->appendFile($this->BaseUrl() . '/application/modules/Contents/externals/scripts/tabs-paginator/cPager.js');
?>

<div class="content-container content-<?php echo $this->page_section; ?>"
     id="content-container-<?php echo $this->page_id; ?>">
    <?php if ($this->migas): ?>
        <div class="generic--breadcrumbs">
            <a href="/">Inicio > </a>
            <?php echo $this->migas; ?>
        </div>
    <?php endif; ?>

    <?php echo $this->page_content; ?>
</div>


<?php if (isset($this->showButtons)) : ?>
    <div class="edit-page--content">
        <a href="/content/edit/<?php echo $this->page_section; ?>/<?php echo $this->page_url; ?>">Editar página</a>
    </div>
<?php endif; ?>

<!--TABS AND PAGINATOR SCRIPTS-->
<script type="text/javascript">
    var $ = jQuery;

    $(document).ready(function () {

        var tabs = $('#horizontalTab');
        tabs.responsiveTabs({
            rotate: false,
            startCollapsed: 'accordion',
            collapsible: 'accordion',
            setHash: true,
        });

        /*Paginador*/
        $(this).cPager({
            pageSize: 1,
            pageid: "pager",
            itemClass: "li-item"
        });

        $(this).cPager({
            pageSize: 1,
            pageid: "pager2",
            itemClass: "li-item2"
        });

        $(this).cPager({
            pageSize: 1,
            pageid: "pager3",
            itemClass: "li-item3"
        });

    });
</script>

<script>
    jQuery(document).ready(function(){

        jQuery('#menu-activo1').addClass('desplegable active');

        var currentParentElement = jQuery('.menu-activo').parent();

        if(currentParentElement.length > 0){
            while(currentParentElement.get(0).tagName == 'LI' || currentParentElement.get(0).tagName == 'UL') {
                if(currentParentElement.get(0).tagName == 'LI') {
                    currentParentElement.addClass('active');
                }
                currentParentElement.css('display','block');
                currentParentElement = currentParentElement.parent();
            }
        }

        jQuery('#menu-activo1 > li ').ready(function() {
            var comprobar = jQuery(this).next();
            jQuery('#menu-activo1 li').removeClass('active');
            jQuery(this).closest('li').addClass('active');
            jQuery('#menu-activo1 ul').css('display','block');
        });

        jQuery('.menujl > ul > li:has(ul)').addClass('desplegable');
        jQuery('.menujl > ul > li > label').click(function() {
            var comprobar = jQuery(this).next();
            jQuery('.menujl li').removeClass('active');
            jQuery(this).closest('li').addClass('active');
            if((comprobar.is('ul')) && (comprobar.is(':visible'))) {
                jQuery(this).closest('li').removeClass('active');
                comprobar.slideUp('normal');

            }
            if((comprobar.is('ul')) && (!comprobar.is(':visible'))) {
                jQuery('.menujl ul ul:visible').slideUp('normal');
                comprobar.slideDown('normal');

            }
        });
        jQuery('.menujl > ul > li > ul > li:has(ul)').addClass('desplegable');
        jQuery('.menujl > ul > li > ul > li > label').click(function() {
            var comprobar = jQuery(this).next();
            jQuery('.menujl ul ul li').removeClass('active');
            jQuery(this).closest('ul ul li').addClass('active');
            if((comprobar.is('ul ul')) && (comprobar.is(':visible'))) {
                jQuery(this).closest('ul ul li').removeClass('active');
                comprobar.slideUp('normal');
            }
            if((comprobar.is('ul ul')) && (!comprobar.is(':visible'))) {
                jQuery('.menujl ul ul ul:visible').slideUp('normal');
                comprobar.slideDown('normal');
            }
        });

        jQuery('.menujl > ul > li > ul > li > ul > li:has(ul)').addClass('desplegable');
        jQuery('.menujl > ul > li > ul > li > ul > li > label').click(function() {
            var comprobar = jQuery(this).next();
            jQuery('.menujl ul ul ul li').removeClass('active');
            jQuery(this).closest('ul ul ul li').addClass('active');
            if((comprobar.is('ul ul ul')) && (comprobar.is(':visible'))) {
                jQuery(this).closest('ul ul ul li').removeClass('active');
                comprobar.slideUp('normal');
            }
            if((comprobar.is('ul ul ul')) && (!comprobar.is(':visible'))) {
                jQuery('.menujl ul ul ul ul:visible').slideUp('normal');
                comprobar.slideDown('normal');
            }
        });

    });

</script>