<?php

return array(
	'package' => array(
		'type' 		  => 'module',
		'name' 		  => 'contents',
		'version' 	  => '1.0.0',
		'path' 		  => 'application/modules/Contents',
		'title' 	  => 'Módulo de contenidos',
		'description' => 'Contenidos',
		'author' 	  => 'vectorial@vectorial.co',
		'callback' 	  => array(
			'class'   => 'Engine_Package_Installer_Module',
		),
		'actions' => array(
			0 => 'install',
			1 => 'upgrade',
			2 => 'refresh',
			3 => 'enable',
			4 => 'disable',
		),
		'directories' => array(
			0 => 'application/modules/Contents',
		),
	),
	'routes' => array(
		'contents_index_view' => array(
			'route'    => 'content/:page_section/:page_url',
			'defaults' => array(
				'module' 	 => 'contents',
				'controller' => 'index',
				'action' 	 => 'view',
			),
		),
		'contents_index_admin' => array(
            'route'    => 'content/admin',
            'defaults' => array(
                'module' => 'contents',
                'controller' => 'index',
                'action' => 'admin',
            ),
        ),
		'contents_index_create' => array(
            'route'    => 'content/create/:page_section',
            'defaults' => array(
                'module' => 'contents',
                'controller' => 'index',
                'action' => 'create',
            ),
        ),        
        'contents_index_edit' => array(
            'route'    => 'content/edit/:page_section/:page_url',
            'defaults' => array(
                'module' 	 => 'contents',
                'controller' => 'index',
                'action' 	 => 'edit',
            ),
        ),
        'contents_index_state' => array(
            'route'    => 'content/state/:page_id',
            'defaults' => array(
                'module' 	 => 'contents',
                'controller' => 'index',
                'action' 	 => 'state',
            ),
        ),
		'contents_index_styles' => array(
            'route'    => 'content/styles',
            'defaults' => array(
                'module' => 'contents',
                'controller' => 'index',
                'action' => 'styles',
            ),
        ),        
	)
);