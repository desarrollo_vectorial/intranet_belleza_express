<?php
class Contents_Api_Core extends Core_Api_Abstract {
	
	private function buildTree(array $elements, $parentId = 0) {
	    $branch = array();
	
	    foreach ($elements as $element) {
	        if ($element['page_parent_id'] == $parentId) {
	            $children = $this->buildTree($elements, $element['page_id']);
	            if ($children) {
	                $element['children'] = $children;
	            }
	            $branch[] = $element;
	        }
	    }
	
	    return $branch;
	}

	public function createOptionsArray(&$options, $menu, $depth = 0) {
		foreach($menu as $row) {
			$options[$row['page_id']] = str_repeat("--", $depth).$row['page_name'];
			if(isset($row['children'])) {
				$this->createOptionsArray($options, $row['children'], $depth+1);
			}
		}	
	}
			
	public function getSectionTree($page_section) {
		$tabMenu = Engine_Api::_() -> getDbtable('pages', 'contents');
		$datMenu = $tabMenu->select()->from(array('a' => $tabMenu -> info('name')), array('a.page_id', 'a.page_name', 'a.page_parent_id', 'a.page_icon', 'a.page_order', 'a.page_url', 'a.page_section'))->where("page_section = '" . $page_section."'")->order('page_parent_id')->order('page_order ASC');
		if($resultados = $tabMenu->fetchAll($datMenu)->toArray()) {
			$menu = $this->buildTree($resultados, 0);
			
			$options = array();
			$this->createOptionsArray($options, $menu);
			
			return $options;
		} else {
			return array();
		}		
	}
		
	public function menuMigas($page_id, $current = '') {

		$pagesParentTable = Engine_Api::_() -> getDbtable('pages', 'contents');
		$query = $pagesParentTable -> select() -> setIntegrityCheck(false) -> from(array('a' => $pagesParentTable -> info('name')), array('a.page_id', 'a.page_name', 'a.page_parent_id', 'a.page_section', 'a.page_url')) -> where('page_id = ' . $page_id);
		$resultados = $query -> query();

		$pagesParent = $resultados -> fetchAll();

		$html = '';
		$migas = array();

		foreach ($pagesParent as $key => $valor) {

			if (!is_null($valor['page_parent_id']) && $valor['page_parent_id'] != 0) {
				$html .= $this -> menuMigas($valor['page_parent_id']);
			}
			$url = 'javascript:void(0)';
			$url = '/contents/' . $valor['page_section'] . '/' . $valor['page_url'] . '/';
			if($html) {
				$html .= " <span class='decorator'>></span> ";
			}
			$html .= '<a class="' . $current . '" href="' . $url . '">' . $valor['page_name'] . '</a>';
		}
		return $html;
	}

	public function obtenerOrden() {
		$pagesParentTable = Engine_Api::_() -> getDbtable('pages', 'contents');
		$query = $pagesParentTable -> select() -> setIntegrityCheck(false) -> from(array('a' => $pagesParentTable -> info('name')), array('MAX(a.page_order) as max_order')) -> where('page_state = 1');
		$query -> order('page_order ASC');
		$resultados = $query -> query();

		return $resultados -> fetchAll();
	}

	public function section_menu($page_section) {
		$tabMenu = Engine_Api::_() -> getDbtable('pages', 'contents');
		$datMenu = $tabMenu->select()->from(array('a' => $tabMenu -> info('name')), array('a.page_id', 'a.show_menu', 'a.page_name', 'a.page_parent_id', 'a.page_icon', 'a.page_order', 'a.page_url', 'a.page_section'))->where("a.page_section = ?", $page_section)->where("a.show_menu = ?", '1')->order('page_parent_id')->order('page_order ASC');

		if($resultados = $tabMenu->fetchAll($datMenu)->toArray()) {
			$menu = $this->buildTree($resultados, 0);
			return $menu[0]['children'];
		} else {
			return array();
		}
	}
		
}
