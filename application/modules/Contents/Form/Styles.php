<?php
class Contents_Form_Styles extends Engine_Form
{
    public $_error = array();

    public function init()
    {
        $this->setTitle('Estilos')
            ->setDescription('Estilos de las páginas de contenido')
            ->setAttrib('name', 'crear_page');

        $this->addElement('Textarea', 'styles', array(
            'label' => 'Estilos:',
            'allowEmpty' => false,
            'required' => true,
        ));

         $this->addElement('Button', 'submit', array(
            'label' => 'Guardar Pagina',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array('ViewHelper'),
        ));

        $this->addElement('Cancel', 'cancel', array(
            'label' => 'Cancelar',
            'link' => true,
            'prependText' => ' or ',
            'decorators' => array('ViewHelper'),
        ));
       
    }
}