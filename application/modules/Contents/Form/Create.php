<?php
class Contents_Form_Create extends Engine_Form
{
    public $_error = array();

    public function init()
    {
        $this->setTitle('Edición página')
            ->setDescription('pagina o sección')
            ->setAttrib('name', 'crear_page');


        $this->addElement('Text', 'page_name', array(
            'label' => 'Título de la página :',
            'allowEmpty' => false,
            'required' => true,
            'class' => 'form-control',
            'maxlength ' => 255,

        ));
        $this->addElement('Text', 'page_url', array(
            'label' => 'Friendly URL:',
            'allowEmpty' => false,
            'required' => true,
            'class' => 'form-control',
            'maxlength ' => 255,

        ));
        $this->addElement('Select', 'page_parent_id', array(
            'label' => 'Subcategorías de la página',
            'multiOptions' => array('' => 'NINGUNO'),
            'allowEmpty' => true,
            'required' => false,
            'class' => 'custom-select',
        ));

        $this->addElement('Textarea', 'page_content', array(
            'label' => 'Contenido de la página',
            'allowEmpty' => false,
            'required' => true,
            'class' => 'richtext form-control',
        ));

        $this->addElement('Select', 'page_order', array(
            'label' => 'Orden :',
            'class' => 'custom-select',
            'multiOptions' => array('' => 'NINGUNO'),
            'allowEmpty' => true,
            'required' => false,
        ));

         $this->addElement('Button', 'submit', array(
            'label' => 'Guardar página',
            'type' => 'submit',
            'ignore' => true,
        ));

        $this->removeDecorator('submit');

        $this->addElement('Cancel', 'cancel', array(
            'label' => 'Cancelar',
            'link' => true,
            'prependText' => '',
            'decorators' => array('ViewHelper'),
        ));


        $elementDecorators = array(
            'Label',

            array(
                array('openLabel' =>'HtmlTag'),
                array('tag' => 'span', 'class' => 'input-group-text', 'openOnly' => true)
            ),

            array(
                array('closeLabel' =>'HtmlTag'),
                array('tag' => 'span', 'closeOnly' => true)
            ),

            array(
                array('openDiv' =>'HtmlTag'),
                array('tag' => 'div', 'class' => 'input-group-prepend', 'openOnly' => true)
            ),

           array(
                array('closeDiv' =>'HtmlTag'),
                array('tag' => 'div', 'closeOnly' => true)
            ),

            'ViewHelper',

            array('HtmlTag', array( 'tag' => 'div', 'class'=>'input-group mb-3'))
        );

        $this->setElementDecorators( $elementDecorators);


        $this->getElement('submit')->setDecorators(array("ViewHelper", "HtmlTag"));
        $this->getElement('cancel')->setDecorators(array("ViewHelper", "HtmlTag"));

    }
}