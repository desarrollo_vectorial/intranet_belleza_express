<?php

class Bannernoticia_Model_Banner extends Core_Model_Item_Abstract {

    public function guardarFoto($file){
		if ($file instanceof Zend_Form_Element_File) {
			$file   = $file->getFileName();
		} else if ($file instanceof Core_Model_Item_Abstract && !empty($file->file_id)) {
			$tmpRow = Engine_Api::_()->getItem('storage_file', $file->file_id);
			$file   = $tmpRow->name;
		} else if (is_array($file) && !empty($file['tmp_name'])) {
			$file   = $file['tmp_name'];
		} else if (is_string($file) && file_exists($file)) {
			$file   = $file;
		} else {
			throw new Event_Model_Exception('invalid argument passed to guardarFoto');
		}

		$name = basename($file);
		$ext  = pathinfo($name, PATHINFO_EXTENSION);

		//$path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'temporary';
		$params = array(
			'parent_id'   => $this->banner_id,
			'parent_type' => 'banner_image'
		);

		// Save
		$storage = Engine_Api::_()->storage();

		// Store
		try {
			//$iMain = $storage->create($path . '/' . $name, $params);
			$iMain = $storage->create($file, $params);
			//$iMain->save($iIconNormal);
		} catch (Exception $e) {
			// Throw
			if ($e->getCode() == Storage_Model_DbTable_Files::SPACE_LIMIT_REACHED_CODE) {
				//throw new Album_Model_Exception($e->getMessage(), $e->getCode());
			} else {
				throw $e;
			}
		}

		// Update row
		$this->photo_id = $iMain->file_id;
		$this->save();

	}

    public function getPhotoPath($type = null){
        $file = Engine_Api::_()->getItemTable('storage_file')->getFile($this->photo_id, $type);
        if (!$file) {
            return;
        }
        return $file->map();
    }

    public function deletebanner(){
        $file = Engine_Api::_()->getItemTable('storage_file')->getFile($this->photo_id, null);
        $file->remove();
        return $this->delete();
    } 

}

?>
