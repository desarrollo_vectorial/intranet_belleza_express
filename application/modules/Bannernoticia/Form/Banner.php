<?php

class Bannernoticia_Form_Banner extends Engine_Form{
	public $_error = array();

	public function init(){
		$this->setTitle('<p>Noticia</p>')
		->setDescription('Creación o edición de un banner de noticias.
						  Imagen grande: 635px x 540px; Imagen pequeña: 310px x 265px')
		->setAttrib('name', 'crear_banner');


		$this->addElement('Hidden', 'banner_id', array(
			'label' => 'ID',
			'allowEmpty' => true,
			'required' => false
		));

		$this->addElement('select', 'type', array(
			'label' 					=> 	'Tipo:',
			'allowEmpty' 				=> false,
			'required' 					=> true,
			'multiOptions'				=>	array(
				'' => 'Seleccione el tipo.',
				'1' => 'Grande',
				'2' => 'Pequeño',
			)
		));

		$this->addElement('Text', 'title', array(
			'label' => 'Nombre de la imagen',
			'allowEmpty' => false,
			'required' => true,
			'maxlength' => 255
		));

		$this->addElement('Textarea', 'description', array(
		'label' => 'Descripción',
		'allowEmpty' => false,
		'required' => true
		));

		$this->addElement('Text', 'url', array(
			'label' => 'URL',
			'allowEmpty' => false,
			'required' => false,
			'maxlength' => 255
		));

		/*$this->addElement('Radio', 'estado', array(
			'label'        => 'Estado del banner',
			'allowEmpty'   => false,
			'required'     => true,
			'multiOptions' => array(
				1 => 'Activo',
				0 => 'Inactivo',
			),
		));*/

		$this->addElement('File', 'photo', array(
			'label' 	 => 'Seleccione una imagen',
			'allowEmpty' => true,
			'required' 	 => false
		));

		$this->addElement('Button', 'submit', array(
			'label' 	 => 'GUARDAR BANNER',
			'type' 		 => 'submit',
			'ignore' 	 => true,
			'decorators' => array('ViewHelper')
		));

	}
}

?>
