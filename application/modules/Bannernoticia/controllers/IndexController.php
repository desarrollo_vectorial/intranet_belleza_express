<?php

class Bannernoticia_IndexController extends Core_Controller_Action_Standard{
    public function indexAction(){
        $this->_helper->content->setEnabled();
        $bannerTable = Engine_Api::_()->getDbtable('banners', 'bannernoticia');
        $banners     = $bannerTable->fetchAll();
        $this->view->banners = array();

        foreach($banners AS $key => $banner){
            if($banner->type == 1)
                $type = 'Grande';
            else
                $type = 'Pequeño';

            if($banner->estado == 1)
                $estado = 'Activo';
            else
                $estado = 'Inactivo';
            
            array_push($this->view->banners, array(
                    "banner_id"   => $banner->banner_id,
                    "type"        => $type,
                    "title"       => $banner->title,
                    "description" => $banner->description,
                    "url"         => $banner->url,
                    "estado"      => $estado
                )
            );
        }


        $selGrande = $bannerTable->select()->where('type = ?', 1);
        $grande    = $bannerTable->fetchRow($selGrande);
        if($grande['banner_id'] != ''){
            $this->view->grande = array(
                "title"       => $grande->title,
                "description" => $grande->description,
                "path"        => $grande->getPhotoPath()
            );
        }


        $selPeque  = $bannerTable->select()->where('type = ?', 2)->limit(4);
        $peques    = $bannerTable->fetchAll($selPeque);
        $this->view->peques = array();
        foreach($peques AS $key => $peque){
            array_push($this->view->peques, array(
                    "title"       => $peque->title,
                    "description" => $peque->description,
                    "path"        => $peque->getPhotoPath()
                )
            );
        }

    }
  
    public function createAction(){
        $this->_helper->content->setEnabled();
        
        $banner_id  = $this->_getParam('banner_id', null);
        $bannerTable = Engine_Api::_()->getDbtable('banners', 'bannernoticia');
        $this->view->image = $banner = $bannerTable->find($banner_id)->current();
        $this->view->form = $form = new Bannernoticia_Form_Banner();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $db = $bannerTable->getAdapter();
                $db->beginTransaction();

                try {
                    $values = $form->getValues();

                    if (!$banner) {
                        if (empty($values['photo'])) {
                          return $form->addError('Debe seleccionar una imagen para guardarla.');
                        }
                        unset($values['banner_id']);
                        $values = array_merge($values, array('created_at' => date('Y-m-d H:i:s')), array('updated_at' => date('Y-m-d H:i:s')));
                        $banner = $bannerTable->createRow();
                        $banner->setFromArray($values);
                        $banner->save();
                    } else {
                        $values = array_merge($values, array('updated_at' => date('Y-m-d H:i:s')));
                        $banner->setFromArray($values);
                        $banner->save();
                    }

                    // Guardar la imagen
          			if (!empty($values['photo'])) {
                      $banner->guardarFoto($form->photo);
          			}

                    $form->addNotice('Ha actualizado la imagen ( ' . $banner->title . ' ) exitosamente.');
                    $db->commit();
                    $form->populate($banner->toArray());

                } catch (Exception $e) {
                    $db->rollBack();
                    throw $e;
                }
            }
        }

        if ($banner) {
            $form->populate($banner->toArray());
        }
    }

    public function deleteAction(){
      $banner_id = $this->_getParam('banner_id', null);
      try {
        $bannerTable = Engine_Api::_()->getDbtable('banners', 'bannernoticia');
        $banner      = $bannerTable->find($banner_id)->current();
        $banner->deletebanner();
        $this->_redirect('/bannernoticia/');
      } catch (Exception $e) {
        
      }
    }

}
