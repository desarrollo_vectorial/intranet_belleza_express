<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Bannernoticia_Widget_BannerNoticiasController extends Engine_Content_Widget_Abstract{

  public function indexAction(){
    $bannerTable = Engine_Api::_()->getDbtable('banners', 'bannernoticia');
    $selGrande   = $bannerTable->select()->where('type = ?', 1);
    $grande      = $bannerTable->fetchRow($selGrande);

    if($grande['banner_id'] != ''){
      $this->view->grande = array(
        "title"       => $grande->title,
        "description" => $grande->description,
        "url"         => $grande->url,
        "path"        => $grande->getPhotoPath()
      );
    }

    $selPeque  = $bannerTable->select()->where('type = ?', 2)->limit(4);
    $peques    = $bannerTable->fetchAll($selPeque);

    $this->view->peques = array();
    foreach($peques AS $key => $peque){
      array_push($this->view->peques, array(
        "title"       => $peque->title,
        "description" => $peque->description,
        "url"         => $peque->url,
        "path"        => $peque->getPhotoPath()
      ));
    }
  }

}
