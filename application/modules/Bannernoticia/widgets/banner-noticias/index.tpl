<style>
  .banner-noticia{ width:1280px; position:relative; display:block; margin: 0 auto; height:300px !important; margin-top: 10px; }
  .banner-noticia .grande{ height:540px; width:49%; position:relative; display:inline-block; }
  .banner-noticia .pequeno{ position:relative; display:inline-block; height:268px; width:49%; }
  .banner-noticia .texto{ display:block; position:absolute; background:rgba(0,0,0,0.9); width:93.5%; bottom:0px; padding:20px; }
  .banner-noticia .texto .titulo{ font-size: 22px; font-weight: normal; color: #FFF; border-bottom: 2px solid #c80e02; }
  .banner-noticia .texto .descripcion{ font-size: 14px; color: #FFF; margin-top: 10px; }

  .banner-noticia .texto-peque{ display:block; position:absolute; background:rgba(0,0,0,0.9); width:86.7%; bottom:0px; padding:20px; }
  .banner-noticia .texto-peque .titulo{ font-size: 16px; font-weight: normal; color: #FFF; border-bottom: 1px solid #c80e02; }
  .banner-noticia .texto-peque .descripcion{ font-size: 11px; color: #FFF; margin-top: 10px; }
</style>


<div class="banner-noticia" style="background: url(/public/img_general/banner_noticias.jpg) no-repeat; background-size: 100% auto;">

</div>