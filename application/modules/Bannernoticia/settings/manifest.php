<?php return array (
  'package' => 
  array (
    'type'        => 'module',
    'name'        => 'bannernoticia',
    'version'     => '1.0.0',
    'path'        => 'application/modules/Bannernoticia',
    'title'       => 'Banner Noticias',
    'description' => 'Administrador de Banner del Home de Noticias',
    'author'      => 'Vectorial Studios Ltda',
    'callback'    => 
    array (
      'class'     => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Bannernoticia',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/bannernoticia.csv',
    ),
  ),
); ?>