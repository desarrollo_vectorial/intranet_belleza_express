<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: content.php 10163 2014-04-11 19:50:10Z andres $
 * @author     John
 */
return array(
  // ------- where I am
	array(
		'title'       => 'Banner del Home de Noticias',
		'description' => 'Modulo del banner del home de noticias',
		'category'    => 'Bannernoticia',
		'type'        => 'widget',
		'name'        => 'bannernoticia.banner-noticias'
	),
) ?>
