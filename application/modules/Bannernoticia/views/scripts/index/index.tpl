<div class="mi-contenedor-2">
  <!--<a href="/bannernoticia/index/create"><button>Agregar nueva Noticia</button></a>
  <br><br>-->

  <table class="admin_table" style='width: 1200px;'>
    <thead>
      <tr>
        <th style='width: 15%; text-align:center'>Tipo</th>
        <th style='width: 30%; text-align:center'>Titulo</th>
        <th>Descripción</th>
        <th style='width: 20%; text-align:center'>URL</th>
        <th style='width: 20%; text-align:center'>Estado</th>
        <th style='width: 10%; text-align:center'>Acciones</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($this->banners as $key => $banner) : ?>
        <tr>
          <td><?php echo $banner['type']; ?></td>
          <td><?php echo $banner['title']; ?></td>
          <td><?php echo $banner['description']; ?></td>
          <td><?php echo $banner['url']; ?></td>
          <td><?php echo $banner['estado']; ?></td>
          <td>
            <a href="/bannernoticia/index/create/banner_id/<?php echo $banner['banner_id']; ?>"><button>Editar</button>
            <!--<a onclick="return confirm('Desea eliminar este Banner?');" href="/bannernoticia/index/delete/banner_id/<php echo $banner['banner_id']; ?>"><button>Eliminar</button></a>-->
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

  <div class="banner-noticia">
    <div class="grande" style="background: url(<?php echo $this->grande['path'];?>) no-repeat;">
      <div class="texto">
        <div class="titulo"><?php echo $this->grande['title'];?></div>
        <div class="descripcion"><?php echo $this->grande['description'];?></div>
      </div>
    </div>

    <div class="grande">
      <?php foreach($this->peques AS $key => $peque){ ?>
        <div class="pequeno" style="background: url(<?php echo $peque['path'];?>) no-repeat;">
          <div class="texto-peque">
            <div class="titulo"><?php echo $peque['title'];?></div>
            <div class="descripcion"><?php echo $peque['description'];?></div>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>

<style>
  .banner-noticia{ width:1250px; position:relative; display:block; }
  .banner-noticia .grande{ height:540px; width:49%; position:relative; display:inline-block; }
  .banner-noticia .pequeno{ position:relative; display:inline-block; height:268px; width:49%; }
  .banner-noticia .texto{ display:block; position:absolute; background:rgba(0,0,0,0.9); width:93.5%; bottom:0px; padding:20px; }
  .banner-noticia .texto .titulo{ font-size: 22px; font-weight: normal; color: #FFF; border-bottom: 2px solid #c80e02; }
  .banner-noticia .texto .descripcion{ font-size: 14px; color: #FFF; margin-top: 10px; }

  .banner-noticia .texto-peque{ display:block; position:absolute; background:rgba(0,0,0,0.9); width:86.7%; bottom:0px; padding:20px; }
  .banner-noticia .texto-peque .titulo{ font-size: 16px; font-weight: normal; color: #FFF; border-bottom: 1px solid #c80e02; }
  .banner-noticia .texto-peque .descripcion{ font-size: 11px; color: #FFF; margin-top: 10px; }
</style>