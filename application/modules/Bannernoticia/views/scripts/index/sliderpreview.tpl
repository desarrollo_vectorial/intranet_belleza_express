<div class="mi-contenedor-2">
  <a href="/slider/index"><button>Ver todos los Sliders</button></a>
  <a href="/slider/index/createslider/slider_id/<?php echo $this->slider['slider_id']; ?>"><button>Editar este slider</button></a>
  <a href="/slider/index/createimage/slider_id/<?php echo $this->slider['slider_id']; ?>"><button>Agregar nueva imagen</button></a>

  <br><br>

  <table class="admin_table">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Orden</th>
        <th>URL</th>
        <th>Abre nueva ventana</th>
        <th>Editar</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($this->images as $key => $image) : ?>
        <tr>
          <td><?php echo $image['title']; ?></td>
          <td><?php echo $image['description']; ?></td>
          <td><?php echo $image['order']; ?></td>
          <td><?php echo $image['url']; ?></td>
          <td><?php echo $image['new_tab']; ?></td>
          <td>
            <a href="/slider/index/createimage/slider_id/<?php echo $this->slider['slider_id']; ?>/image_id/<?php echo $image['image_id']; ?>"><button>Editar</button></a>
            <a onclick="return confirm('Desea eliminar esta imagen?');" href="/slider/index/deleteimage/slider_id/<?php echo $this->slider['slider_id']; ?>/image_id/<?php echo $image['image_id']; ?>"><button>Eliminar</button></a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <br><br>
  <?php echo $this->content()->renderWidget('slider.custom-slider', array('slider' => $this->slider['slider_id'])); ?>
</div>