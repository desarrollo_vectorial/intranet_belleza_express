<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Slider_Widget_CustomSliderController extends Engine_Content_Widget_Abstract
{
    public function indexAction()
    {
        $slider_id = $this->_getParam('slider');
        $sliderTable = Engine_Api::_()->getDbtable('sliders', 'slider');
        $this->view->slider = $sliderTable->find($slider_id)->current();

        $imageTable = Engine_Api::_()->getDbtable('images', 'slider');
        $select = $imageTable->select()
            ->where('slider_id = ?', $slider_id)
            ->order('order ASC');
        $images = $imageTable->fetchAll($select);
        $this->view->images = array();

        foreach ($images as $key => $image) {
            $imgObj = $image->toArray();
            $imgObj['path'] = $image->getPhotoPath();
            array_push($this->view->images, $imgObj);
        }
    }

}
