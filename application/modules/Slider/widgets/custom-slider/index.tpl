<!--SLIDER-->
<?php
/*SYLTES SLIDER*/
$this->headLink()->prependStylesheet($staticBaseUrl . 'externals/bpHS-slider/css/bpHS.css');
$this->headLink()->prependStylesheet('http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');

// SCRIPSTS SLIDER
$this->headScript()
    ->prependFile($staticBaseUrl . 'externals/bpHS-slider/js/bpHS.min.js')
    ->prependFile($staticBaseUrl . 'externals/bpHS-slider/js/jquery.touchSwipe.min.js');
?>

<?php

switch ($this->slider['css_set']) {
    case 'default':
        $path = 'widget_sets/default.tpl';
        break;

    case 'home_lateral_right':
        $path = 'widget_sets/home_lateral_right.tpl';
        break;

    case 'home_lateral_left':
        $path = 'widget_sets/home_lateral_left.tpl';
        break;

}

$datos = array('slider' => $this->slider, 'images' => $this->images);
echo $this->partial($path, 'slider', $datos);

?>