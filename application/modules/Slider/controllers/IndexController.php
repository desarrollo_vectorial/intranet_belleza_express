<?php

class Slider_IndexController extends Core_Controller_Action_Standard
{
    public function indexAction(){
        $this->_helper->content->setEnabled();

        $sliderTable = Engine_Api::_()->getDbtable('sliders', 'slider');
        $this->view->sliders = $sliderTable->fetchAll();
    }

    public function createsliderAction(){
        $this->_helper->content->setEnabled();

        $this->view->slider_id = $slider_id = $this->_getParam('slider_id', null);
        $sliderTable = Engine_Api::_()->getDbtable('sliders', 'slider');
        $slider = $sliderTable->find($slider_id)->current();

        $this->view->form = $form = new Slider_Form_Slider();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $db = $sliderTable->getAdapter();
                $db->beginTransaction();

                try {
                    $values = $form->getValues();

                    if (!$slider) {
                        unset($values['slider_id']);
                        $values = array_merge($values, array('created_at' => date('Y-m-d H:i:s')), array('updated_at' => date('Y-m-d H:i:s')));
                        $slider = $sliderTable->createRow();
                        $slider->setFromArray($values);
                        $slider->save();
                    } else {
                        $values = array_merge($values, array('updated_at' => date('Y-m-d H:i:s')));
                        $slider->setFromArray($values);
                        $slider->save();
                    }

                    $form->addNotice('Ha actualizado el slider ( ' . $slider->title . ' ) exitosamente.');

                    $db->commit();

                    $form->populate($slider->toArray());

                } catch (Exception $e) {
                    $db->rollBack();
                    throw $e;
                }
            }
        }

        if ($slider) {
            $form->populate($slider->toArray());
            // Buscar las imagenes del slider
        }
    }

    public function deletesliderAction(){
        $slider_id = $this->_getParam('slider_id', null);
        $sliderTable = Engine_Api::_()->getDbtable('sliders', 'slider');
        $slider = $sliderTable->find($slider_id)->current();

        $imageTable = Engine_Api::_()->getDbtable('images', 'slider');
        $select = $imageTable->select()
          ->where('slider_id = ?', $slider_id);
        $images = $imageTable->fetchAll($select);

        try {
          foreach ($images as $key => $image) {
            $image->deleteimage();
          }

          $slider->delete();
          $this->_redirect('/slider/index');
        } catch (Exception $e) {
          
        }
        
    }

    public function createimageAction(){
        $this->_helper->content->setEnabled();
        
        $image_id = $this->_getParam('image_id', null);
        $this->view->slider_id = $slider_id = $this->_getParam('slider_id', null);

        $imageTable = Engine_Api::_()->getDbtable('images', 'slider');
        $this->view->image = $image = $imageTable->find($image_id)->current();

        $this->view->form = $form = new Slider_Form_Image();
        $imageSelect = $form->getElement('slider_id');
        $imageSelect->setValue($slider_id);

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $db = $imageTable->getAdapter();
                $db->beginTransaction();

                try {
                    $values = $form->getValues();

                    if (!$image) {
                        if (empty($values['photo'])) {
                          return $form->addError('Debe seleccionar una imagen para guardarla.');
                        }
                        unset($values['image_id']);
                        $values = array_merge($values, array('created_at' => date('Y-m-d H:i:s')), array('updated_at' => date('Y-m-d H:i:s')));
                        $image = $imageTable->createRow();
                        $image->setFromArray($values);
                        $image->save();
                    } else {
                        $values = array_merge($values, array('updated_at' => date('Y-m-d H:i:s')));
                        $image->setFromArray($values);
                        $image->save();
                    }

                    // Guardar la imagen
          					if (!empty($values['photo'])) {
                      $image->guardarFoto($form->photo);
          					}

                    $form->addNotice('Ha actualizado la imagen ( ' . $image->title . ' ) exitosamente.');

                    $db->commit();

                    $form->populate($image->toArray());

                } catch (Exception $e) {
                    $db->rollBack();
                    throw $e;
                }
            }
        }

        if ($image) {
            $form->populate($image->toArray());
        }
    }

    public function deleteimageAction(){
      $image_id = $this->_getParam('image_id', null);
      $slider_id = $this->_getParam('slider_id', null);
      try {
        $imageTable = Engine_Api::_()->getDbtable('images', 'slider');
        $image = $imageTable->find($image_id)->current();
        $image->deleteimage();
        $this->_redirect('/slider/index/sliderpreview/slider_id/' . $slider_id);
      } catch (Exception $e) {
        
      }
    }

    public function sliderpreviewAction(){
        $this->_helper->content->setEnabled();
        $slider_id = $this->_getParam('slider_id', null);
        $sliderTable = Engine_Api::_()->getDbtable('sliders', 'slider');
        $this->view->slider = $sliderTable->find($slider_id)->current();

        $imageTable = Engine_Api::_()->getDbtable('images', 'slider');
        $select = $imageTable->select()
        ->where('slider_id = ?', $slider_id)
        ->order('order ASC');
        $images = $imageTable->fetchAll( $select );
        $this->view->images = array();

        foreach ($images as $key => $image) {
            $imgObj = $image->toArray();
            $imgObj['path'] = $image->getPhotoPath();
            array_push($this->view->images, $imgObj);
        }

    }

}
