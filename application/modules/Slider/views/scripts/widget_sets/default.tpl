<div class="container--slider">
    <div class="bp-hs" id="slider--home">
        <div class="bp-hs_inner">
            <?php foreach ($this->images as $key => $image) : ?>
                <?php /*prx($image); */?>
                <a href="<?php echo $image['url']; ?>" target="<?php echo ( ($image['new_tab'] == true) ? '_blank' : '' ); ?>" class="bp-hs_inner__item">
                    <img src="<?php echo $image['path']; ?>" alt="<?php echo $image['title'];?>"/>
                    <div class="slider--overlay">
                        <h3>
                            <?php echo $image['title'];?>
                        </h3>
                        <p><?php echo substr(strip_tags($image['description']), 0, 50);?></p>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<script>
    /*
    * Written for: Boompx - jQuery Hero Slider Plugin
    * Github     : https://github.com/boompx/bpHS
    */

    (function ($) {
        $(document).ready(function () {
            // fire default bpHS!
            $('#slider--home').bpHS({
                nextText: '<i class="fa fa-angle-right"></i>',
                prevText: '<i class="fa fa-angle-left"></i>',
                autoPlay: true,
                touchSwipe: false,
                duration: 10000
            });
        });
    })(jQuery);
</script>