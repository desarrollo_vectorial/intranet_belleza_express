<div id="flexslider_cont_<?php echo $this->slider['slider_id']; ?>" class="flexslider flexslider_<?php echo $this->slider['css_set']; ?>" style="width: <?php echo $this->slider['width']; ?>px !important; height: auto;">
    <div style="height: 2px">&nbsp;
        <div class="progressbar" style="height: 2px; background-color: #2299dd; width: 0%;"></div>
    </div>

    <ul class="slides">
        <?php foreach ($this->images as $key => $image){ ?>
            <li>
                <?php if($image['url'] != ''){ ?><a href="<?php echo $image['url']; ?>" target="<?php echo ( ($image['new_tab'] == true) ? '_blank' : '' ); ?>"><?php } ?>
                    <img src="<?php echo $image['path']; ?>" />
                <?php if($image['url'] != ''){ ?></a><?php } ?>
            </li>
        <?php } ?>
    </ul>
</div>
