<div class="mi-contenedor-2">
  <a href="/slider/index/createslider"><button>Agregar nuevo slider</button></a>

  <br><br>

  <table class="admin_table">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Estilo del slider</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($this->sliders as $key => $slider) : ?>
        <tr>
          <td><?php echo $slider['title']; ?></td>
          <td><?php echo $slider['description']; ?></td>
          <td><?php echo $slider['css_set']; ?></td>
          <td>
            <a href="/slider/index/createslider/slider_id/<?php echo $slider['slider_id']; ?>"><button>Editar</button>
            <a href="/slider/index/sliderpreview/slider_id/<?php echo $slider['slider_id']; ?>"><button>Vista previa</button></a>
            <a onclick="return confirm('Desea eliminar este Slider? se perderían todas las imagenes...');" href="/slider/index/deleteslider/slider_id/<?php echo $slider['slider_id']; ?>"><button>Eliminar</button></a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>