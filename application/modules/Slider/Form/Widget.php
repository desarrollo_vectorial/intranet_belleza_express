<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     Jung
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Slider_Form_Widget extends Core_Form_Admin_Widget_Standard
{
  public function init()
  {
    parent::init();

    // Set form attributes
    $this
      ->setTitle('Custom Slider')
      ->setDescription('Elige un Slider de imagenes previamente creado')
      ;

    // Get available files
    $sliderOptions = array('' => 'Seleccione un slider de la lista');
    $sliderTable = Engine_Api::_()->getDbtable('sliders', 'slider');
    $sliders = $sliderTable->fetchAll();
    foreach ($sliders as $key => $slider) {
      $sliderOptions[$slider['slider_id']] = $slider['title'];
    }

    $this->addElement('Select', 'slider', array(
      'label' => 'Custom Slider',
      'multiOptions' => $sliderOptions,
    ));

  }
}
