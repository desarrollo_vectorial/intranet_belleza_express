<?php

class Slider_Form_Slider extends Engine_Form
{
	public $_error = array();

	public function init()
	{
		$this->setTitle('<p>Slider</p>')
		->setDescription('Creación o edición de un Slider')
		->setAttrib('name', 'crear_slider');

		$this->addElement('Hidden', 'slider_id', array(
			'label' => 'ID',
			'allowEmpty' => true,
			'required' => false
		));

    $this->addElement('Text', 'title', array(
			'label' => 'Nombre del Slider',
			'allowEmpty' => false,
			'required' => true,
			'maxlength' => 255
		));

    $this->addElement('Textarea', 'description', array(
      'label' => 'Descripción',
      'allowEmpty' => false,
      'required' => true
    ));

    /*$this->addElement('Float', 'slideshow_speed', array(
			'label' => 'Duración cambio de slide (milisegundos)',
			'allowEmpty' => false,
			'required' => true,
		));*/

		/*$this->addElement('Float', 'animation_speed', array(
			'label' => 'Duración de la animación (milisegundos)',
			'allowEmpty' => false,
			'required' => true,
		));

		$this->addElement('select', 'animation_type', array(
			'label' => "Tipo de animación",
			'multiOptions' => array (
				'' => 'Seleccione un tipo de animación',
				'slide'   => 'Slide',
				'fade' 	  => 'Fade',
			),
			'allowEmpty' => false,
			'required' => true,
		));*/

		/*$this->addElement('select', 'direction', array(
			'label' => "Dirección del cambio de slide",
			'multiOptions' => array (
				'' => 'Seleccione una dirección para el cambio del slide',
				'horizontal' => 'Horizontal',
				'vertical' => 'Vertical',
			),
			'allowEmpty' => false,
			'required' => true,
		));*/

		$this->addElement('select', 'css_set', array(
			'label' => "Estilos del slider",
			'multiOptions' => array (
				'' => 'Seleccione un estilo CSS',
				'default' => 'Default',
				/*'loading_bar' => 'Loading Bar',*/
				/*'noticia' => 'Home - Tipo Noticia',*/
				'home_lateral_right' => 'Lateral derecho',
                'home_lateral_left' => 'Lateral izquierdo',
				/*'nuevo' => 'Inline - Sized',*/
			),
			'allowEmpty' => false,
			'required' => true,
		));

		/*$this->addElement('Float', 'width', array(
			'label' => 'Ancho de las imagenes (pixeles)',
			'allowEmpty' => false,
			'required' => true,
		));

		$this->addElement('Float', 'heigth', array(
			'label' => 'Alto de las imagenes (pixeles)',
			'allowEmpty' => false,
			'required' => true,
		));*/

		$this->addElement('Button', 'submit', array(
			'label' => 'GUARDAR SLIDER',
			'type' => 'submit',
			'ignore' => true,
			'decorators' => array('ViewHelper')
		));

	}
}

?>
