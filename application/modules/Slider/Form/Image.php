<?php

class Slider_Form_Image extends Engine_Form
{
	public $_error = array();

	public function init()
	{
		$this->setTitle('<p>Image</p>')
		->setDescription('Creación o edición de una imagen para un slider')
		->setAttrib('name', 'crear_image');

		$sliderTable = Engine_Api::_()->getDbtable('sliders', 'slider');
		$sliders = $sliderTable->fetchAll();
		$sliderOptions = array('' => 'Seleccione un Slider');
		foreach ($sliders as $key => $slider) {
			$sliderOptions[$slider['slider_id']] = $slider['title'];
		}
		$this->addElement('select', 'slider_id', array(
			'label' => 'Nombre del Slider',
			'multiOptions' => $sliderOptions,
			'allowEmpty' => false,
			'required' => true,
		));

		$this->addElement('Hidden', 'image_id', array(
			'label' => 'ID',
			'allowEmpty' => true,
			'required' => false
		));

		$this->addElement('Text', 'title', array(
			'label' => 'Nombre de la imagen',
			'allowEmpty' => false,
			'required' => true,
			'maxlength' => 255
		));

    $this->addElement('Textarea', 'description', array(
      'label' => 'Descripción',
      'allowEmpty' => false,
      'required' => true
    ));

    $this->addElement('Float', 'order', array(
			'label' => 'Orden',
			'allowEmpty' => false,
			'required' => true,
		));

		$this->addElement('Text', 'url', array(
			'label' => 'URL',
			'allowEmpty' => false,
			'required' => false,
			'maxlength' => 255
		));

		$this->addElement('Radio', 'new_tab', array(
			'label' => 'Abrir en nueva ventana',
			'allowEmpty' => false,
			'required' => false,
			'multiOptions' => array(
        1 => 'Si',
        0 => 'No',
      ),
		));

		$this->addElement('File', 'photo', array(
			'label' => 'Seleccione una imagen',
			'allowEmpty' => true,
			'required' => false
		));

		$this->addElement('Button', 'submit', array(
			'label' => 'GUARDAR IMAGEN',
			'type' => 'submit',
			'ignore' => true,
			'decorators' => array('ViewHelper')
		));

	}
}

?>
