<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'slider',
    'version' => '4.0.0',
    'path' => 'application/modules/Slider',
    'title' => 'Sliders',
    'description' => 'Administrador de Sliders de la intranet',
    'author' => 'Vectorial Studios Ltda',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Slider',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/slider.csv',
    ),
  ),
); ?>