<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: content.php 10163 2014-04-11 19:50:10Z andres $
 * @author     John
 */
return array(
    array(
        'title' => 'Custom Slider',
        'description' => 'Elige un Slider de imagenes previamente creado',
        'category' => 'Slider',
        'type' => 'widget',
        'name' => 'slider.custom-slider',
        'adminForm' => 'Slider_Form_Widget'
    ),
    array(
        'title' => 'Custom Slider Right',
        'description' => 'Elige un Slider de imagenes previamente creado',
        'category' => 'Slider',
        'type' => 'widget',
        'name' => 'slider.custom-slider-right',
        'adminForm' => 'Slider_Form_Widget'
    ),

    // ------- where I am
    array(
        'title' => 'Donde estoy - Slider',
        'description' => 'Muestra en que modulo se encuestra actualmente',
        'category' => 'Slider',
        'type' => 'widget',
        'name' => 'slider.where-i-am'
    ),
) ?>
