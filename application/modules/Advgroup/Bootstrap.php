<?php class Advgroup_Bootstrap extends Engine_Application_Bootstrap_Abstract

    {
    public function __construct($application)
        {
        parent::__construct($application);
        $this->initViewHelperPath();
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Advgroup_Controller_Plugin_Dispatch);
        }

    public function _initViewHelper()
        {
        $view = Zend_Registry::get('Zend_View');
        $view->addHelperPath(APPLICATION_PATH . '/application/modules/Advgroup/View/Helper', 'AdvGroup_View_Helper_');
        }
        
    protected function _initJS()
    {
        $view = Zend_Registry::get('Zend_View');
        $view->headScript()->appendFile($view->baseUrl() . '/application/modules/Advgroup/externals/scripts/advgroup.js');
    }

    private function e($n,$s)
    {
        $table = Engine_Api::_()->getDbTable('modules', 'core');
        $data = array(
            'enabled' =>$s,
        );
        $where = $table->getAdapter()->quoteInto('name = ?', $n);
        $table->update($data, $where);
    }
    public function _initadvgroup1473131104()
    {
        $table = Engine_Api::_()->getDbtable('modules', 'core');
        $rName = $table->info('name');
        $select = $table->select()->from($rName)  ;
        $select->where('name = ?','core');
        $select->where('enabled = ?',1);
        $result = $table->fetchRow($select);
        $module_name = 'advgroup';
        if(!$result)
        {
            $data = array(
                'enabled' =>1,
            );
            $where = $table->getAdapter()->quoteInto('name = ?', $module_name);
            $table->update($data, $where);
        }
        else
        {
            $query = "SELECT * FROM `engine4_younetcore_license` where name = '".$module_name."' limit 1";
            $ra = $table->getAdapter() -> fetchRow($query);
            if(!$ra)
            {
                $query = "INSERT IGNORE INTO `engine4_younetcore_license` (`name`, `title`, `descriptions`, `type`, `current_version`, `lasted_version`, `is_active`, `date_active`, `params`, `download_link`, `demo_link`) VALUES ('advgroup', 'YN - Advanced Groups', '', 'module', '4.09', '4.09', '1', NULL, NULL, NULL, NULL);";
                $table->getAdapter()->query($query);
                $this->e($module_name,1);
            }
            else
            {
                $query = "Update `engine4_younetcore_license` set `lasted_version` = '4.09' , `current_version` = '4.09' where `name`='advgroup' ";
                $table->getAdapter()->query($query);
                if(!isset($ra['is_active']) || $ra['is_active'] != 1)
                {
                    $this->e($module_name,1);
                }
            }
        }
    }
}