<div class="container-list" id="no--padding-top">

<?php if (count($this->groups) > 0): ?>

    <ul class="format--list">
        <?php
        $i = 0;
        foreach ($this->groups as $item):
            $session = new Zend_Session_Namespace('mobile');
            if ($session->mobile) {
                $title = $item->getTitle();
                $owner_name = $item->getOwner()->getTitle();
            } else {
                $title = $this->string()->truncate($item->getTitle(), 17);
                $owner_name = $this->string()->truncate($item->getOwner()->getTitle(), 13);
            }
            $owner = $item->getOwner();
            if ($i < $this->limit): $i++; ?>

                <?php echo $this->partial('groupBox.tpl', array('item' => $item, 'showOwnerOptions' => false)); ?>

            <?php endif;  ?>
        <?php endforeach; ?>
    </ul>

<?php else: ?>

    <div class="tip">
        <span><?php echo $this->translate('There is no featured group yet.'); ?></span>
    </div>

<?php endif; ?>

</div>
