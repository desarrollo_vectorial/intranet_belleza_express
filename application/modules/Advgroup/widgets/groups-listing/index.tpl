<div class="container-list">

    <?php if (count($this->paginator) > 0): ?>

        <ul class="format--list">
            <?php foreach ($this->paginator as $item): ?>

                <?php echo $this->partial('groupBox.tpl', array('item' => $item, 'showOwnerOptions' => false)); ?>

            <?php endforeach; ?>
        </ul>

    <?php else: ?>
        <div class="tip">
		<span>
			<?php echo $this->translate('_group_There are no groups yet.') ?>
			<?php if( $this->canCreate): ?>
            <?php echo $this->translate('_group_Why don\'t you %1$screate one%2$s?',
				'<a href="'.$this->url(array('action' => 'create'), 'group_general').'">', '</a>') ?>
			<?php endif; ?>
		</span>
        </div>
    <?php endif; ?>
    <div class="generic-paginator">
        <?php if ($this->paginator->count() > 1): ?>
            <?php echo $this->paginationControl($this->paginator, null, null, array(
                'pageAsQuery' => true,
                'query' => $this->formValues,
            )); ?>
        <?php endif; ?>
    </div>

</div>

<script type="text/javascript">
    var advgroup_view_map_time = function () {
        document.getElementById('advgroup_list_group').set('class', 'advgroup_map-view');
        var tab = 'group_listing';
        var html = '<?php echo $this->url(array('action' => 'display-map-view', 'ids' => $this->groupIds), 'group_general') ?>' + '/tab/' + tab;

        document.getElementById('list-most-time-iframe').dispose();

        var iframe = new IFrame({
            id: 'list-most-time-iframe',
            src: html,
            styles: {
                'height': '500px',
                'width': '100%'
            },
        });
        iframe.inject($('advgroup_list_group'));
        document.getElementById('list-most-time-iframe').style.display = 'block';
        //$$('.layout_advgroup_groups_listing .pages')[0].style.display = 'none';
        document.getElementById('list-most-time-iframe').style.display = 'block';
        document.getElementById('paginator').style.display = 'none';
        setCookie('view_mode', 'map');
    }
    var advgroup_view_grid_time = function () {
        document.getElementById('advgroup_list_group').set('class', 'advgroup_grid-view');
        //$$('.layout_advgroup_groups_listing .pages')[0].style.display = 'block';
        setCookie('view_mode', 'grid');
        document.getElementById('paginator').style.display = 'block';
    }
    var advgroup_view_list_time = function () {
        document.getElementById('advgroup_list_group').set('class', 'advgroup_list-view');
        //$$('.layout_advgroup_groups_listing .pages')[0].style.display = 'block';
        setCookie('view_mode', 'list');
        document.getElementById('paginator').style.display = 'block';
    }
    <?php if($this->view_mode == 'map'):?>
    {
        advgroup_view_map_time();
        document.getElementById('paginator').style.display = 'none';
    }
    <?php endif;?>
</script>

<script type="text/javascript">
    window.addEvent('domready', function () {

        if (getCookie('view_mode') != "") {
            document.getElementById('advgroup_list_group').set('class', "advgroup_" + getCookie('view_mode') + "-view");
            var map = getCookie('view_mode');
            if (map == 'map') {
                advgroup_view_map_time();
            }
        }
        else {
            document.getElementById('advgroup_list_group').set('class', "<?php echo $this->class_mode;?>");
        }
    });

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }
</script>
