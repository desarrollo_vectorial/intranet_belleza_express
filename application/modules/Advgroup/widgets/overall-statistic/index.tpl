<style type="text/css" media="screen">
	.layout_advgroup_overall_statistic{
		background-color: #ffffff !important;
		margin-bottom: 15px;
		margin-top: 10px;
	}

	.layout_advgroup_overall_statistic .titulo{
		padding   : 20px;
		display   : block;
		position  : relative;
		text-align  : right;
	}

	.layout_advgroup_overall_statistic .titulo .most{
		color 		: #8b8b8a;	
		display 	: inline-block;	
		top 		: 10px;
		font-size 	: 34px;
		line-height : 30px;
		width 		: 200px;
		vertical-align: middle;
	}

	.layout_advgroup_overall_statistic .titulo .most:before{
		content 		: '';
		display 		: block;
		position 		: absolute;
		background-image: url(../public/admin/articulo-estadisticas.png);
		width 			: 50px;
		height 			: 50px;
		left 			: 20px;
		top 			: 30px;
	}

	.layout_advgroup_overall_statistic .group_top_views li{
		display: block;
		vertical-align: top;
		padding-left: 15px;
	}
	
	.layout_advgroup_overall_statistic .group_top_views .blogs_browse_info{
		position: relative;
		display: inline-block;
		margin-bottom: 5px;
		padding-left: 15px;
		width: 90%;
	}

	.layout_advgroup_overall_statistic .group_top_views .blogs_browse_info .blogs_browse_info_title{
		position: relative;
		display:inline-block;
		color: #8b8b8a;
		font-size: 18px;
		font-weight: normal;
		/*text-transform: capitalize;*/
	}

	.layout_advgroup_overall_statistic .group_top_views .blogs_browse_info .blogs_browse_info_cont{
		position   : relative;
		display    : inline-block;
		color      : #8b8b8a;
		font-size  : 18px;
		font-weight: bold;
		float: right;
		
	}
    .generic_layout_container > h3{
        margin-top: 2px;
        font-size: 18px;
        color: #6b6e74;
        padding: 5px;
        margin-left: 11px;
    }
</style>

<div >


	
</div>  

<ul class = "global_form_box" style="margin-bottom: 15px; padding : 0px 15px 15px 15px;">
        <br/>
      <div class="statistic_info"> <?php echo $this->translate('Total Groups');?></div>
      <div class="statistic_count">  <?php echo $this->count_groups?> </div>
       <br/>
      <div class="statistic_info"> <?php echo $this->translate('Total Albums');?></div>
      <div class="statistic_count">  <?php echo $this->count_albums?> </div>
        <br/>
      <div class="statistic_info"> <?php echo $this->translate('Total Photos');?></div>
      <div class="statistic_count">  <?php echo $this->count_photos?> </div>
        <br/>
      <div class="statistic_info"> <?php echo $this->translate('Total Topics'); ?></div>
      <div class="statistic_count">  <?php echo $this->count_topics?> </div>
 </ul>
