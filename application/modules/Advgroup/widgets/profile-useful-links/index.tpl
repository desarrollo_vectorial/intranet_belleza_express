<style>
.generic_layout_container.layout_advgroup_profile_useful_links {
	padding: 20px !important;
  padding-top: 25px !important;
  background: #f3f3f3;
  margin-top: 6px !important;
}
</style>

<?php $group = $this->group;
      $viewer = $this->viewer; ?>
      
<?php if($this->helpfulLink ):?>
	<?php
      echo $this->htmlLink(array('route' => 'group_link', 'action' => 'create', 'subject' => $this->subject()->getGuid()), $this->translate('Añadir nuevo enlace'), array(
    'class' => 'buttonlink ',
          'style'=>'color:#4c8ac5 !important;'
  )) ?>
<?php endif;?>

<?php if(count($this->paginator)>0):?>
  <ul>
        <?php foreach($this->paginator as $item):?>
        <li style="padding: 10px 0px 10px 10px;border-bottom: 1px solid #EAEAEA ;">
          <h3 style="padding-left: 10px;"><?php echo $item->title;?></h3>
          <p style="padding-left: 10px;"><?php echo $item->description?></p>
          <p style="padding-left: 10px;"><a href="<?php echo $item->link_content;?>" target="_blank">
             <b><?php echo $item->link_content;?></b>
          </a></p>
        </li>
        <?php endforeach;?>
  </ul>
<?php else:?>
  <div class="tip">
    <span>
      <?php echo $this->translate('Aún no se ha agregado ningún enlace a este grupo.');?>
    </span>
  </div>
<?php endif;?>