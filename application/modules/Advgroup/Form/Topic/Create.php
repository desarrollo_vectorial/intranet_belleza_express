<?php

class Advgroup_Form_Topic_Create extends Engine_Form {
	public function init() {
		$this
			->setTitle( 'Post Discussion Topic' )
			->setAttrib( 'id', 'group_topic_create' );

		$this->addElement( 'Text', 'title', array(
			'label'      => 'Title',
			'allowEmpty' => false,
			'required'   => true,
			'filters'    => array(
				new Engine_Filter_Censor(),
				new Engine_Filter_HtmlSpecialChars(),
			),
			'validators' => array(
				array( 'StringLength', true, array( 1, 64 ) ),
			)
		) );

		$upload_url = Zend_Controller_Front::getInstance()->getRouter()->assemble( array( 'action' => 'upload-photo' ), 'group_general', true );

		$this->addElement( 'textarea', 'body', array(
				'label'      => 'Description',
				//'disableLoadDefaultDecorators' => true,
				'required'   => true,
				'allowEmpty' => false,
				'class'      => 'richtext',
				'filters'    => array(
					'StripTags',
					new Engine_Filter_Censor(),
					//new Engine_Filter_HtmlSpecialChars(),
					new Engine_Filter_EnableLinks(),
				),
			)
		);

		$this->addElement( 'Checkbox', 'watch', array(
			'label' => 'Send me notifications when other members reply to this topic.',
			'value' => true,
		) );

		$this->addElement( 'Button', 'submit', array(
			'label'      => 'Post New Topic',
			'ignore'     => true,
			'type'       => 'submit',
			'decorators' => array(
				'ViewHelper',
			),
		) );

		$this->addElement( 'Cancel', 'cancel', array(
			'label'       => 'cancel',
			'prependText' => '',
			'type'        => 'link',
			'link'        => true,
			'decorators'  => array(
				'ViewHelper',
			),
		) );

		$this->addDisplayGroup( array( 'submit', 'cancel' ), 'buttons' );
	}
}