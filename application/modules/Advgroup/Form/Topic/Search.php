<?php

class Advgroup_Form_Topic_Search extends Engine_Form {
	public function init() {
		$this
			->setAttribs( array(
				'id'    => 'filter_form',
				'class' => 'global_form_box',
			) )
			->setMethod( 'GET' )
			->setAction( Zend_Controller_Front::getInstance()->getRouter()->assemble( array( 'page' => null ) ) );

		$this->addElement( 'Hidden', 'page' );
		$this->addElement( 'Text', 'search', array(
			'label'       => 'Buscar debates',
			'placeholder' => 'Buscar debates',
		) );

		$this->addElement( 'Select', 'closed', array(
			'label'        => 'Status',
			'multiOptions' => array(
				''  => 'Todos los debates',
				'0' => 'Solo temas abiertos',
				'1' => 'Sólo temas cerrados',
			),
		) );

		$this->addElement( 'Select', 'view', array(
			'label'        => 'View',
			'multiOptions' => array(
				'0' => 'Debates de todos los miembros ',
				'1' => 'Sólo mis debates',
			),
		) );

		$this->addElement( 'Select', 'order', array(
			'label'        => 'Browse By',
			'multiOptions' => array(
				'recent'     => 'Mas recientes',
				'last_reply' => 'Compartidos recientemente',
				'view'       => 'Mas vistos',
				'reply'      => 'Mas compartidos',
				'no_reply'   => 'No compartidos',
			),
		) );

		// Buttons
		$this->addElement( 'Button', 'submit', array(
			'label'      => 'Search',
			'type'       => 'submit',
			'decorators' => array(
				'ViewHelper',
			),
		) );
	}
}