<?php
class Advgroup_Form_Poll_Search extends Engine_Form
{
  public function init()
  {
  	$translate = Zend_Registry::get("Zend_Translate");
   //Form Attribute and Method
    $this->setAttribs(array('id' => 'filter_form', 'class' => '',))
         ->setMethod('GET')
         ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array('page' => null)));


    $this->addElement('Hidden','page');
    //Search Text
    $this->addElement('Text', 'search', array(
      'label' => 'Buscar encuestas',
      'placeholder' => $translate->translate('Buscar encuestas'),
    ));

    //Closed
    $this->addElement('Select', 'closed', array(
      'label' => 'Status',
      'multiOptions' => array(
        '' => 'All Polls',
        '0' => 'Sólo encuestas abiertas',
        '1' => 'Sólo sondeos cerrados',
      ),
    ));

    //Order
    $this->addElement('Select', 'order', array(
      'label' => 'Browse By',
      'multiOptions' => array(
        'recent' => 'Más reciente',
        'popular' => 'Más popular',
      ),
    ));

    // Buttons
    $this->addElement('Button', 'submit', array(
      'label' => 'Search',
      'type' => 'submit',
      'decorators' => array(
        'ViewHelper',
      ),
    ));
  }
}