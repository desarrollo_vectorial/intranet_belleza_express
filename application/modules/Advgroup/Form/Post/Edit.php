<?php
class Advgroup_Form_Post_Edit extends Engine_Form
{
  public function init()
  {
    $this
      ->setTitle('Editar Debate')
      ->setDescription('En este espacio usted podrá editar la información del debate. ');

    $upload_url = Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'upload-photo'), 'group_general', true);

    $this->addElement('textarea', 'body', array(
        'disableLoadDefaultDecorators' => true,      
      'decorators' => array('ViewHelper'),
      'label' => 'Body',
      'allowEmpty' => false,
      'required' => true,
    ));
    
    $this->addElement('Button', 'submit', array(
      'label' => 'Editar Debate',
      'ignore' => true,
      'type' => 'submit',
      'decorators' => array(
        'ViewHelper',
      ),
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => ' or ',
      'decorators' => array(
        'ViewHelper',
      ),
    ));

    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
  }
}