<div class="group_discussions_options">
  <?php echo $this->htmlLink(array('route' => 'group_profile', 'id' => $this->group->getIdentity()), $this->translate('Back to Group'), array('class' => 'buttonlink icon_back')) ?>
</div>


<div class="settings">
  <?php echo $this->form->render($this) ?>
</div>
<script type="text/javascript">
  function removeSubmit(){
    $('buttons-wrapper').hide();
  }
</script>