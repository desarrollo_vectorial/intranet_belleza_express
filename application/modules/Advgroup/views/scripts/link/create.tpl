<div class="group_discussions_options">
  <?php echo $this->htmlLink(array('route' => 'group_profile', 'id' => $this->group->getIdentity()), $this->translate('Back to Group'), array('class' => 'buttonlink icon_back')) ?>
</div>

<div class="form-general">
    <?php echo $this->form->render(); ?>
</div>

<?php echo $this->partial('/application/modules/Core/views/scripts/_richeditor.tpl', array()); ?>
