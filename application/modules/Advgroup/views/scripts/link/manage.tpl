<style>	
.form-elements {
    border: 1px solid #a7a5a5;
    border-radius: 5px;
    background: #fff !important;
}
.layout_classified_browse_search div#filter_form, .filters, form#filter_form div.form-elements { background-color: #d8d8d8 !important; }
input[type=text], input[type=email], input[type=password], input.text, textarea{ width: 400px; }
select { width: 410px; height: 30px; }
.contenedor-blanco { background: #fff; }
</style>

<div class="group_discussions_options">
  <?php echo $this->htmlLink(array('route' => 'group_profile', 'id' => $this->group->getIdentity()), $this->translate('Back to Group'), array(
    'class' => 'buttonlink icon_back'
  )) ?>
  <?php
      echo $this->htmlLink(array('route' => 'group_link', 'action' => 'create', 'subject' => $this->group->getGuid()), $this->translate('Añadir nuevo enlace'), array(
    'class' => 'buttonlink icon_group_photo_new'
  )) ?>
</div>

<h2>
  <?php echo $this->group->__toString() ?>
  <?php echo $this->translate(' &#187; Administrar enlaces útiles');?>
</h2>

<div class="advgroup_list">
  <div class="layout_middle">

    <div class="contenedor-blanco">

      <?php if(count($this->paginator)>0):
      $session = new Zend_Session_Namespace('mobile');
      $class_smoothbox = 'smoothbox';
      if($session -> mobile)
      {
        $class_smoothbox = '';
      }
      ?>
      <ul>
        <?php foreach( $this->paginator as $link ):?>
              <li style="padding: 10px 0px 10px 0px;border-bottom: 1px solid #EAEAEA ;">
                <div class='link_edit_options'>
                  <?php echo $this->htmlLink(array(
                    'route' => 'group_link',
                    'action' => 'edit',
                    'link_id' => $link->getIdentity(),
                  ), $this->translate('Editar Enlace'), array(
                    'class' => $class_smoothbox. ' buttonlink icon_group_edit',
                  )) ?>
                  <br>
                  <?php echo $this->htmlLink(array(
                    'route' => 'group_link',
                    'action' => 'delete',
                    'link_id' => $link->getIdentity(),
                  ), $this->translate('Eliminar Enlace'), array(
                    'class' => 'smoothbox buttonlink icon_group_delete',
                  )) ?>
                </div>
                <div class="ymb_link_info">
                  <h3><?php echo $link->title;?></h3>
                  <p><?php echo $link->description?></p>
                  <p><a href="<?php echo $link->link_content;?>" target="_blank">
                      <?php echo $link->link_content;?>
                  </a></p>
              </div>
              </li>
        <?php endforeach; ?>
      </ul>

      <?php if( $this->paginator->count() > 1 ): ?>
        <div>
          <?php echo $this->paginationControl($this->paginator) ?>
        </div>
      <?php endif; ?>
    <?php else:?>
      <div class="tip">
        <span>
          <?php echo $this->translate('Aún no se ha agregado ningún enlace a este grupo.');?>
        </span>
      </div>
    <?php endif; ?>

  </div>
</div>
</div>