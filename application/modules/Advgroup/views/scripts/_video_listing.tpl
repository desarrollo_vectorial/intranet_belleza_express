<?php
/**
 * YouNet Company
 *
 * @category   Application_Extensions
 * @package    Ynvideo
 * @author     YouNet Company
 */
?>
<div class="ynvideo_thumb_wrapper video_thumb_wrapper">
    <?php if ($this->video->duration): ?>
        <?php echo $this->partial('_video_duration.tpl', 'advgroup', array('video' => $this->video)) ?>
    <?php endif ?>
    <?php
    if ($this->video->photo_id) {
        echo $this->htmlLink($this->video->getHref(), $this->itemPhoto($this->video, 'thumb.normal'));
    } else {
        echo '<img alt="" src="' . $this->escape($this->layout()->staticBaseUrl) . 'application/modules/video/externals/images/video_icon2.png">';
    }
    ?>
    <?php $ynvideo_enable = Engine_Api::_()->advgroup() -> checkYouNetPlugin('ynvideo') ?>
    <?php if($ynvideo_enable) :?>
	    <span class="video_button_add_to_area">
	        <button class="ynvideo_uix_button ynvideo_add_button" id="ynvideo_btn_video_<?php echo $this->video->getIdentity()?>" 
	            video-id="<?php echo $this->video->getIdentity()?>">	            
	           		 <div class="ynvideo_plus" />	           
	        </button>
	    </span>
    <?php endif ;?>
</div>
<?php 
    echo $this->htmlLink($this->video->getHref(), 
            $this->string()->truncate($this->video->getTitle(), 15), 
            array('class' => 'ynvideo_title', 'title' => $this->video->getTitle())) 
?>

<div class="video_author">
    <?php $user = $this->video->getOwner() ?>
    <?php if ($user) : ?>
        <?php echo $this->translate('By') ?>
        <?php echo $this->htmlLink($user->getHref(), htmlspecialchars ($this->string()->truncate($user->getTitle(), 15)), array('title' => $user->getTitle())) ?>
    <?php endif; ?>
    <?php 
    	$session = new Zend_Session_Namespace('mobile');
		 if(!$session -> mobile)
		 {
    ?>
    |
    <?php } ?>
    <span class="video_views">
        <?php if (!isset($this->infoCol) || ($this->infoCol == 'view')) : ?>
            <?php echo $this->translate(array('%1$s visita', '%1$s visitas', $this->video->view_count), $this->locale()->toNumber($this->video->view_count)) ?>
        <?php else : ?>
            <?php if ($this->infoCol == 'like') : ?>
                <?php
                    $likeCount = $this->video->likes()->getLikeCount();
                    echo $this->translate(array('%1$s me gusta', '%1$s me gustas', $likeCount), $this->locale()->toNumber($likeCount));
                ?>
            <?php elseif ($this->infoCol == 'comment') : ?>
                <?php
                    $commentCount = $this->video->comments()->getCommentCount();
                    echo $this->translate(array('%1$s comentario', '%1$s comentarios', $commentCount), $this->locale()->toNumber($commentCount));
                ?>
            <?php elseif ($this->infoCol == 'favorite') : ?>
            <?php
                echo $this->translate(array('%1$s favorito', '%1$s favoritos', $this->video->favorite_count), $this->locale()->toNumber($this->video->favorite_count));
            ?>
            <?php endif; ?>
        <?php endif; ?>
    </span>
</div>

    <?php 
        echo $this->partial('_video_rating_big.tpl', 'advgroup', array('video' => $this->video));
    ?>