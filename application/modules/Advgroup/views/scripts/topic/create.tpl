<!-- Menu Bar -->
<div class="group_discussions_options">
	<?php echo $this->htmlLink(array('route' => 'group_profile', 'id' => $this->group->getIdentity()), $this->translate('Back to Group'), array(
		'class' => 'buttonlink icon_back'
	)) ?>
</div>

<!-- Header -->
<div class="generic_layout_container layout_top">
    <div class="generic_layout_container layout_middle">
        <div class="generic_layout_container layout_classified_where_i_am"><div class="generic--breadcrumbs">
                <a href="/">Inicio &gt; </a>
				<?php echo $this->group->__toString();?>
            </div></div>
    </div>
</div>

<div class="form-general">
    <?php echo $this->form->render($this) ?>
</div>


<?php echo $this->partial('/application/modules/Core/views/scripts/_richeditor.tpl', array()); ?>