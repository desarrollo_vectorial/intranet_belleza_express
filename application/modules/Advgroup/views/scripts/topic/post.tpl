<!-- Header -->
<div class="generic_layout_container layout_top">
	<div class="generic_layout_container layout_middle">
		<h2>
		  <?php echo $this->group->__toString() ?>
		  <?php echo $this->translate('&#187; Discussions');?>
		</h2>
	</div>
</div>

<h3>
  <?php echo $this->topic->__toString() ?>
</h3>

<br />

<?php if( $this->message ) echo $this->message ?>

<?php if( $this->form ) echo $this->form->render($this) ?>


<script type="text/javascript" src="/externals/tinymce_new/tinymce.min.js"></script>
<script type="text/javascript" src="/externals/ckeditor_new/ckeditor.js"></script>
<script>
    window.addEvents({
        load: function(){
            CKEDITOR.replace('body',
            {
                filebrowserBrowseUrl : '/externals/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : '/externals/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : '/externals/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : '/externals/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : '/externals/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : '/externals/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            });
        }
    });
</script>