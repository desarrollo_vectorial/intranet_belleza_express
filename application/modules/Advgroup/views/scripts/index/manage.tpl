<script type="text/javascript">
    var pageAction = function (page) {
        $('page').value = page;
        $('filter_form').submit();
    }
</script>

<div class="container-list">

    <?php if (count($this->paginator) > 0): ?>
        <ul class="format--list">
            <?php foreach ($this->paginator as $item): ?>

                <?php echo $this->partial('groupBox.tpl', array('item' => $item, 'showOwnerOptions' => true)); ?>

            <?php endforeach; ?>
        </ul>
    <div class="generic-paginator">
        <?php if (count($this->paginator) > 1): ?>
            <?php echo $this->paginationControl($this->paginator, null, null, array(
                'pageAsQuery' => true,
                'query' => $this->formValues,
            )); ?>
        <?php endif; ?>
    </div>

    <?php else: ?>
        <div class="tip">
        <span>
        <?php echo $this->translate('_group_You have not joined any groups yet.') ?>
        <?php if ($this->canCreate): ?>
            <?php echo $this->translate('_group_Why don\'t you %1$screate one%2$s?',
                '<a href="' . $this->url(array('action' => 'create'), 'group_general') . '">', '</a>') ?>
        <?php endif; ?>
        </span>
        </div>
    <?php endif; ?>

</div>



