<style>	
.form-elements {
    border: 1px solid #a7a5a5;
    border-radius: 5px;
    background: #fff !important;
}
.layout_classified_browse_search div#filter_form, .filters, form#filter_form div.form-elements { background-color: #d8d8d8 !important; }
input[type=text], input[type=email], input[type=password], input.text, textarea{ width: 400px; }
select { width: 410px; height: 30px; }
.contenedor-blanco { background: #fff; padding:20px; }
</style>
<div class="group_discussions_options">
	<?php echo $this->htmlLink(array('route' => 'group_profile', 'id' => $this->group->getIdentity()), $this->translate('Back to Group'), array(
		'class' => 'buttonlink icon_back'
	)) ?>
	<?php echo $this->htmlLink(array('route' => 'group_extended', 'controller'=>'video','action'=>'list','subject' => $this->subject()->getGuid()), $this->translate('Browse Videos'), array(
		'class' => 'buttonlink icon_group_video active'
	)) ?>
	<?php
			$ynvideo_enable = Engine_Api::_() -> advgroup() ->checkYouNetPlugin('ynvideo');
		?>
	<?php if( $this->canCreate ): ?>
		<?php 
			if($ynvideo_enable){
				echo $this->htmlLink(array(
					'route' 	  => 'video_general',
					'action' 	  => 'create',
					'parent_type' =>'group',
					'subject_id'  =>  $this->group->group_id,
				), $this->translate('Crear Video'), array(
				'class' => 'buttonlink icon_group_video_new'
				)); 
			}else{
				echo $this->htmlLink(array(
					'route' 	  => 'video_general',
					'action' 	  => 'create',
					'parent_type' =>'group',
					'subject_id'  =>  $this->group->getGuid(),
				), $this->translate('Crear Video'), array(
				'class' => 'buttonlink icon_group_video_new'
				)) ;
			}
		?>
	<?php endif; ?>
</div>

<div class="generic_layout_container layout_top">
	<div class="generic_layout_container layout_middle">
		<h2>
			<?php echo $this->group->__toString();
				echo $this->translate(' &#187; Videos');
			?>
		</h2>
	</div>
</div>

<div class="generic_layout_container layout_main advgroup_list">
	
	<div class="generic_layout_container layout_middle">
		<?php echo $this->form->render($this);?>

		<div class="contenedor-blanco">
		
			<?php if ($this->paginator->getTotalItemCount() > 0): ?>
				<ul class="ynvideo_videos_manage videos_manage">
					<h3>
						<?php
						$totalVideo = $this->paginator->getTotalItemCount();
						echo $this->translate(array('%1$s video', '%1$s video', $totalVideo), $this->locale()->toNumber($totalVideo));
						?>
					</h3>
					<?php foreach ($this->paginator as $item): ?>
						<li>
							<div class="ynvideo_thumb_wrapper video_thumb_wrapper">
								<?php if ($item->duration): ?>
									<?php echo $this->partial('_video_duration.tpl','advgroup', array('video' => $item)) ?>
								<?php endif; ?>
								<?php
								if ($item->photo_id) {
									echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, 'thumb.normal'));
								} else {
									echo '<img alt="" src="' . $this->layout()->staticBaseUrl . 'application/modules/video/externals/images/video_icon2.png">';
								}
								?>
								<?php $ynvideo_enable = Engine_Api::_() -> advgroup() ->checkYouNetPlugin('ynvideo'); 
								if($ynvideo_enable) :?>
								<span class="video_button_add_to_area">
									<button class="ynvideo_uix_button ynvideo_add_button" video-id="<?php echo $item->getIdentity() ?>">
										<div class="ynvideo_plus" />
									</button>
								</span>
								<?php endif;?>
							</div>
							<div class='video_options'>
								<?php
								$ynvideo_enable = Engine_Api::_() -> advgroup() ->checkYouNetPlugin('ynvideo');
								if($ynvideo_enable)
								{
									echo $this->htmlLink(array(
										'route' => 'video_general',
										'action' => 'edit',
										'video_id' => $item->video_id,
										'group_id' => $this->group->group_id,
										'parent_type' => 'group',
										'id_subject' =>  $this->group->getIdentity(),
										), $this->translate('Edit Video'), array('class' => 'buttonlink icon_group_edit'));
								}
								else
								{
									echo $this->htmlLink(array(
										'route' => 'default',
										'module' => 'video',
										'controller' => 'index',
										'action' => 'edit',
										'video_id' => $item->video_id,
										'group_id' => $this->group->group_id,
										'parent_type' => 'group',
										'id_subject' =>  $this->group->getIdentity(),
										), $this->translate('Edit Video'), array('class' => 'buttonlink icon_group_edit'));
								}
								?>
								
								<?php
								if ($item->status != 2) {
									if($ynvideo_enable)
									{
										echo $this->htmlLink(array(
											'route' => 'video_general',
											'action' => 'delete',
											'video_id' => $item->video_id,
											'group_id' => $this->group->group_id,
											'parent_type' => 'group',
											'case' => 'video',
											'format' => 'smoothbox'
											), $this->translate('Delete Video'), array('class' => 'buttonlink smoothbox icon_video_delete'));
										}
									else
									{
										echo $this->htmlLink(array(
												'route' => 'default', 
												'module' => 'video', 
												'controller' => 'index', 
												'action' => 'delete', 
												'video_id' => $item->video_id, 
												'group_id' => $this->group->group_id,
												'parent_type' => 'group',
												'case' => 'video',
												'format' => 'smoothbox'), 
												$this->translate('Delete Video'), array('class' => 'buttonlink smoothbox icon_video_delete'
										));
									}
								}
								?>
								
								<?php
								$table = Engine_Api::_() -> getDbTable('highlights', 'advgroup');
								$select = $table -> select() -> where("group_id = ?", $this->group->group_id) -> where('item_id = ?', $item->getIdentity()) -> where("type = 'video'") -> limit(1);
								
								$row = $table -> fetchRow($select);
								if(!$row){
									$row = $table -> createRow();
									$row -> setFromArray(array(
															'group_id' => $this->group->group_id, 
															'item_id' => $item->getIdentity(), 
															'user_id' => $item->owner_id, 
															'type' => 'video', 
															'creation_date' => date('Y-m-d H:i:s'), 
															'modified_date' => date('Y-m-d H:i:s')));
									$row -> save();
								}
								
								
								if($row->highlight){
									echo $this->htmlLink(
										array(
											'route' => 'group_extended', 
											'controller'=>'video',
											'action'=>'highlight',
											'group_id' => $this->group->group_id,
											'video_id' => $item->getIdentity(),
											
										), 
										$this->translate('Sin destacar'), 
										array(
											'class' => 'smoothbox buttonlink icon_ynevent_unhighlight'
										)
									);
								}
								else {
									echo $this->htmlLink(
										array(
											'route' => 'group_extended', 
											'controller'=>'video',
											'action'=>'highlight',
											'group_id' => $this->group->group_id,
											'video_id' => $item->getIdentity(),
										), 
										$this->translate('Highlight'), 
										array(
											'class' => 'smoothbox buttonlink icon_ynevent_highlight'
										)
									);
								}
							?>
							</div>
							<div class="video_info video_info_in_list">
								<div class="ynvideo_title">
									<?php echo $this->htmlLink($item->getHref(), htmlspecialchars($item->getTitle())) ?>
									<?php if($row->highlight) :?>
											<strong style="color: red;"><?php echo " - " . $this->translate("Destacado"); ?></strong> 
									<?php endif;?>
								</div>
								<div class="video_stats">
									<?php echo $this->partial('_video_views_stat.tpl','advgroup', array('video' => $item)) ?>
									<div class="ynvideo_block">
										<?php echo $this->partial('_video_rating_big.tpl','advgroup', array('video' => $item)) ?>
									</div>
								</div>
								<div class="video_desc ynvideo_block">
										<?php echo $this->string()->truncate($this->string()->stripTags($item->description), 300) ?>
								</div>
								<?php if ($item->status == 0): ?>
								<div class="tip">
									<span>
										<?php echo $this->translate('Su video está en la cola para ser procesado; se le notificará cuando esté listo para ser visto.') ?>
									</span>
								</div>
								<?php elseif ($item->status == 2): ?>
								<div class="tip">
									<span>
										<?php echo $this->translate('El video se está procesando en ese momento; se le notificará cuando esté listo para verse.') ?>
									</span>
								</div>
								<?php elseif ($item->status == 3): ?>
								<div class="tip">
									<span>
										<?php echo $this->translate('Error de conversión de vídeo. Inténtalo %1$ssubiendo de nuevo%2$s.', '<a href="' . $this->url(array('action' => 'create', 'type' => 3)) . '">', '</a>'); ?>
									</span>
								</div>
								<?php elseif ($item->status == 4): ?>
								<div class="tip">
									<span>
										<?php echo $this->translate('Error de conversión de vídeo. El formato de video no es compatible con FFMPEG. Por favor, intente %1$sotra vez%2$s.', '<a href="' . $this->url(array('action' => 'create', 'type' => 3)) . '">', '</a>'); ?>
									</span>
								</div>
								<?php elseif ($item->status == 5): ?>
								<div class="tip">
									<span>
										<?php echo $this->translate('Error de conversión de vídeo. Los archivos de audio no son compatibles. Por favor, intente %1$sotra vez%2$s.', '<a href="' . $this->url(array('action' => 'create', 'type' => 3)) . '">', '</a>'); ?>
									</span>
								</div>
								<?php elseif ($item->status == 7): ?>
								<div class="tip">
									<span>
										<?php echo $this->translate('Error de conversión de vídeo. Puede estar sobre el límite de subida del sitio. Intente %1$ssubir%2$s un archivo más pequeño, o elimine algunos archivos para liberar espacio.', '<a href="' . $this->url(array('action' => 'create', 'type' => 3)) . '">', '</a>'); ?>
									</span>
								</div>
								<?php endif; ?>
							</div>
						</li>
					<?php endforeach; ?>
				</ul>
				<br/>
				<div class="ynvideo_pages">
					<?php echo $this->paginationControl($this->paginator, null, null, array(
						'pageAsQuery' => true,
						'query' => $this->formValues,
					)); ?>
				</div>
			<?php else: ?>
				<div class="tip">
					<span>
						<?php echo $this->translate('You do not have any videos.'); ?>
					</span>
				</div>
			<?php endif; ?> 		
		</div>
	</div>
</div>

<script type="text/javascript">
	en4.core.runonce.add(function(){
		if($('title')){
			new OverText($('title'), {
				poll		   : true,
				pollInterval   : 500,
				positionOptions: {
					position   : ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
					edge	   : ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
					offset	   : {
						x: ( en4.orientation == 'rtl' ? -4 : 4 ),
						y: 2
					}
				}
			});
		}
	});
</script>  