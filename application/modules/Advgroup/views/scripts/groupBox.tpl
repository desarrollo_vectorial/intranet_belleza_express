<li>
    <div class="format--image-left">
        <div class="image-left--enlace">
            <?php if ($this->item->getPhotoUrl("thumb") != null): ?>
                <span style="background-image: url('<?php echo $this->item->getPhotoUrl("thumb"); ?>');"></span>
            <?php else: ?>
                <span style="background-image: url('<?php echo $this->baseUrl(); ?>/application/themes/snowbot/images/grupos.png');"></span>
            <?php endif; ?>
        </div>
    </div>

    <div class="format--info">
        <div class="info--content">
            <div class="format--title">
                <h3>
                    <?php echo $this->htmlLink($this->item->getHref(), $this->item->getTitle()) ?>
                </h3>
            </div>
            <div class="format--date-start">
                <?php echo $this->timestamp(strtotime($this->item->creation_date)) ?>
            </div>
            <div class="format--members">
                <div class="time_active">
                    <i class="ynicon-time" title="Time create"></i>
                    <?php echo $this->item->getTimeAgo(); ?>
                </div>
                <div class="groups_members">
                    <i class="ynicon-person" title="Guests"></i>
                    <?php echo $this->translate(array("%s member", "%s member", $this->item->countGroupMembers()), $this->item->countGroupMembers()); ?>
                </div>
            </div>
            <div class="format--description">
                <?php if (strlen(strip_tags($this->item->description)) > 60)
                    echo $this->string()->truncate(strip_tags($this->item->description), 150);
                else
                    echo strip_tags($this->item->description);
                ?>
            </div>
        </div>

        <div class="format--options">
            <?php if ($this->showOwnerOptions): ?>
                <div class="bottons--action">
                    <?php echo $this->htmlLink(array(
                        'action' => 'edit',
                        'group_id' => $this->item->getIdentity(),
                        'route' => 'group_specific',
                        'reset' => true,

                    ), 'Editar'); ?>

                    <?php echo $this->htmlLink(array(
                        'route' => 'group_specific',
                        'module' => 'article',
                        'controller' => 'index',
                        'action' => 'delete',
                        'group_id' => $this->item->getIdentity(),
                        'format' => 'smoothbox'

                    ), 'Borrar', array(
                        'class' => 'buttonlink smoothbox '
                    )); ?>
                </div>

            <?php else: ?>
                <div class="botton--group">
                    <?php echo $this->htmlLink($this->item->getHref(), 'Ver grupo'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</li>