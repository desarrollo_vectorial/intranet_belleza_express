<style>
#main_tabs>li{ width: 120px !important; }
#main_tabs>li>a{ 
	position: relative;
    display: inline-block;
    margin: 0 auto;
    color: #a6a6a6;
    width: 120px;
    line-height: 1;
    text-align: center;
    padding: 10px;
    font-size: 16px;
    font-weight: normal;
	}
</style>
<script type="text/javascript">
  en4.core.runonce.add(function(){
      document.getElementById('selectall').addEvent('click', function(){
          check = this.checked;
          list = document.getElementsByName('users[]');
          for(var i=0;i<list.length;i++){
            list[i].checked = check;
          }
    })});
</script>

<?php echo $this->form->setAttrib('class', 'global_form_popup')->render($this) ?>