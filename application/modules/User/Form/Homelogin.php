<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Form_Homelogin extends Engine_Form{

  public function init(){
    $tabindex = 1;
    
    // Init form
    $this->setAttrib('id', 'user_form_homelogin');

    $this->addElement('Text', 'usuario_ldap', array(
      'label'      => 'Usuario:',
      'required'   => true,
      'allowEmpty' => false,
      'class'      => 'form_titulo',
      'tabindex'   => $tabindex++,
    ));

    $this->addElement('Password', 'password_ldap', array(
      'label'      => 'Clave:',
      'required'   => true,
      'allowEmpty' => false,
      'class'      => 'form_titulo',
      'tabindex'   => $tabindex++,
    ));

    // Init submit
    $this->addElement('Button', 'submit', array(
      'label'  => 'Ingresar',
      'type'   => 'submit',
      'ignore' => true,
    ));
    
  }
}
