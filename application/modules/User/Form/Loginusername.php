<?php
/**
 * SocialEngine
 */
class User_Form_Loginusername extends Engine_Form{
  public function init(){
    $tabindex = 1;
    
    // Init form
    $this->setAttrib('id', 'user_form_loginusername');

    $this->addElement('Text', 'usuario_login', array(
      'label'      => 'Usuario:',
      'required'   => true,
      'allowEmpty' => false,
      'class'      => 'form_titulo',
      'tabindex'   => $tabindex++,
    ));

    $this->addElement('Password', 'password_login', array(
      'label'      => 'Contraseña:',
      'required'   => true,
      'allowEmpty' => false,
      'class'      => 'form_titulo',
      'tabindex'   => $tabindex++,
    ));

    // Init submit
    $this->addElement('Button', 'submit', array(
      'label'     => 'Ingresar',
      'type'      => 'submit',
      'ignore'    => true,
      'tabindex'  => $tabindex++
    ));
    
  }

}
