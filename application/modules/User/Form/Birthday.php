<?php

class User_Form_Birthday extends Core_Form_Admin_Widget_Standard
{
  public function init()
  {
    parent::init();

    // Set form attributes
    $this
      ->setTitle('User Birthday')
      ->setDescription('Muestra los cumpleaños de los usuarios por día, semana ó mes.')
      ;

    $this->addElement('Select', 'range', array(
      'label' => 'Mostrar los cumpleaños del:',
      'multiOptions' => array(
        '' => '(Seleccione una opción)',
        'day' => 'Día',
        'week' => 'Semana',
        'month' => 'Mes',
      ),
      'allowEmpty' => false,
			'required' => true,
    ));

    $this->addElement('Select', 'display', array(
      'label' => 'Mostrar en formato:',
      'multiOptions' => array(
        '' => '(Seleccione una opción)',
        'calendar' => 'Calendario',
        'list' => 'Lista',
      ),
      'allowEmpty' => false,
			'required' => true,
    ));

  }
}
