<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: IndexController.php 10075 2013-07-30 21:51:18Z jung $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_IndexController extends Core_Controller_Action_Standard
{
  public function indexAction()
  {

  }

  public function homeAction()
  {
    // check public settings
    $require_check = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.general.portal', 1);
    if(!$require_check){
      if( !$this->_helper->requireUser()->isValid() ) return;
    }

    if( !Engine_Api::_()->user()->getViewer()->getIdentity() ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }

    // Render
    $this->_helper->content
        ->setNoRender()
        ->setEnabled()
        ;
  }

  public function browseAction(){
    $require_check = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.general.browse', 1);
    if(!$require_check){
      if( !$this->_helper->requireUser()->isValid() ) return;
    }
    if( !$this->_executeSearch() ) {
      // throw new Exception('error');
    }

    if( $this->_getParam('ajax') ) {
      $this->renderScript('_browseUsers.tpl');
    }

    if( !$this->_getParam('ajax') ) {
      $this->_helper->content->setEnabled();
    }
  }

  protected function _executeSearch()
  {
    // Check form
    $form = new User_Form_Search(array(
      'type' => 'user'
    ));

    if( !$form->isValid($this->_getAllParams()) ) {
      $this->view->error = true;
      $this->view->totalUsers = 0;
      $this->view->userCount = 0;
      $this->view->page = 1;
      return false;
    }

    $this->view->form = $form;

    // Get search params
    $page = (int)  $this->_getParam('page', 1);
    $ajax = (bool) $this->_getParam('ajax', false);
    $options = $form->getValues();

    // Process options
    $tmp = array();
    $originalOptions = $options;
    foreach( $options as $k => $v ) {
      if( null == $v || '' == $v || (is_array($v) && count(array_filter($v)) == 0) ) {
        continue;
      } else if( false !== strpos($k, '_field_') ) {
        list($null, $field) = explode('_field_', $k);
        $tmp['field_' . $field] = $v;
      } else if( false !== strpos($k, '_alias_') ) {
        list($null, $alias) = explode('_alias_', $k);
        $tmp[$alias] = $v;
      } else {
        $tmp[$k] = $v;
      }
    }
    $options = $tmp;

    // Get table info
    $table = Engine_Api::_()->getItemTable('user');
    $userTableName = $table->info('name');

    $searchTable = Engine_Api::_()->fields()->getTable('user', 'search');
    $searchTableName = $searchTable->info('name');

    //extract($options); // displayname
    $profile_type = @$options['profile_type'];
    $displayname = @$options['displayname'];
    if (!empty($options['extra'])) {
      extract($options['extra']); // is_online, has_photo, submit
    }

    // Contruct query
    $select = $table->select()
      ->setIntegrityCheck(false)
      ->from($userTableName)
      ->joinLeft($searchTableName, "`{$searchTableName}`.`item_id` = `{$userTableName}`.`user_id`", null)
      //->group("{$userTableName}.user_id")
      ->where("{$userTableName}.search = ?", 1)
      ->where("{$userTableName}.enabled = ?", 1);

    $searchDefault = true;

    // Build the photo and is online part of query
    if( isset($has_photo) && !empty($has_photo) ) {
      $select->where($userTableName.'.photo_id != ?', "0");
      $searchDefault = false;
    }

    if( isset($is_online) && !empty($is_online) ) {
      $select
        ->joinRight("engine4_user_online", "engine4_user_online.user_id = `{$userTableName}`.user_id", null)
        ->group("engine4_user_online.user_id")
        ->where($userTableName.'.user_id != ?', "0");
      $searchDefault = false;
    }
	
	/* Procesar extra fields a mostrar en el directorio */
	$this->view->extra_fields = $extra_fields = self::getFieldsDirectory();	
	foreach($extra_fields as $field_id => $field_data) {
		$field_name = $field_data['label'];
		$alias = "Fields__".$field_name;
		$select->joinLeft(array($alias => "engine4_user_fields_values"), $alias.".item_id = `{$userTableName}`.user_id AND ".$alias.".field_id = ".$field_id, $alias.'.value as '.$field_name);		
	}

	    // Add displayname
    if( !empty($displayname) ) {
      $select->where("(`{$userTableName}`.`username` LIKE ? || `{$userTableName}`.`displayname` LIKE ?)", "%{$displayname}%");
      $searchDefault = false;
    }

    // Build search part of query
    $searchParts = Engine_Api::_()->fields()->getSearchQuery('user', $options);
    foreach( $searchParts as $k => $v ) {
      $select->where("`{$searchTableName}`.{$k}", $v);

      if(isset($v) && $v != ""){
        $searchDefault = false;
      }
    }

    /*if($searchDefault){
      $select->order("{$userTableName}.lastlogin_date DESC");
    } else {
      $select->order("{$userTableName}.displayname ASC");
    }*/

    $select->order("{$userTableName}.displayname ASC");
    // Build paginator
    $paginator = Zend_Paginator::factory($select);
    $paginator->setItemCountPerPage(10);
    $paginator->setCurrentPageNumber($page);

    $this->view->page = $page;
    $this->view->ajax = $ajax;
    $this->view->users = $paginator;
    $this->view->totalUsers = $paginator->getTotalItemCount();
    $this->view->userCount = $paginator->getCurrentItemCount();
    $this->view->topLevelId = $form->getTopLevelId();
    $this->view->topLevelValue = $form->getTopLevelValue();
    $this->view->formValues = array_filter($originalOptions);

    return true;
  }

    public function followAction(){

        //prx("follow");
        set_time_limit(600);

        $tableUsers = Engine_Api::_()->getDbTable("users", "user");
        // $select =  $tableUsers->select()->where("user_id = ?", 187);
        $select =  $tableUsers->select();
        $users = $tableUsers->fetchAll( $select );
        $users = $users->toArray();

        foreach( $users as $user ){
            $this->seguirTodosLosAdministradores( $user );
        }

        prx( "finalizó" );

        $this->_helper->content
            ->setNoRender()
        ;
    }

    protected function seguirTodosLosAdministradores( $user )
    {

        $userTable = Engine_Api::_()->getDbtable('users', 'user');
        $db = $userTable->getAdapter();
        $db->beginTransaction();
        $adminUsers = $userTable->fetchAll();
        $membership = Engine_Api::_()->getDbtable('users', 'user');

        foreach ($adminUsers as $key => $adminUser) {
            if ($adminUser->isAdmin()) {
                $admin_id = $adminUser['user_id'];
                // $u_id = $this->new_user_id;
                $u_id = $user['user_id'];

                $dbQuery = Zend_Db_Table_Abstract::getDefaultAdapter();
                $membership = $dbQuery->query('SELECT * FROM engine4_user_membership WHERE resource_id = '.$admin_id.' AND user_id = '.$u_id.';');
                $rows = $membership->fetchAll();

                if (count($rows) == 0) {
                    $dbQueryInsert = Zend_Db_Table_Abstract::getDefaultAdapter();
                    $dbQueryInsert->query('INSERT INTO engine4_user_membership (resource_id, user_id, active, resource_approved, user_approved) VALUES('.$admin_id.', '.$u_id.', 1, 1, 1);');
                    $dbQueryInsert->commit();
                }

                $dbQuery = Zend_Db_Table_Abstract::getDefaultAdapter();
                $membership = $dbQuery->query('SELECT * FROM engine4_user_membership WHERE resource_id = '.$u_id.' AND user_id = '.$admin_id.';');
                $rows = $membership->fetchAll();

                if (count($rows) == 0) {
                    $dbQueryInsert = Zend_Db_Table_Abstract::getDefaultAdapter();
                    $dbQueryInsert->query('INSERT INTO engine4_user_membership (resource_id, user_id, active, resource_approved, user_approved) VALUES('.$u_id.', '.$admin_id.', 1, 1, 1);');
                    $dbQueryInsert->commit();
                }
            }
        }
    }

	private function getFieldsDirectory() {
		$structure = Engine_Api::_()->getApi('core', 'fields')->getFieldsStructureSearch('user');
		
		$extra_fields = array();// 14 => "Cargo", 19 => "Email", 18 => "Extension", 17 => "Empresa" );

	    foreach( $structure as $map ) {
	      $field = $map->getChild();
			
	      if( !$field->show_directory ) {
	        continue;
	      }
	      
	      $extra_fields[$field->field_id] = array('label' => $field->label, 'options' => array());
		  
		  $params = $field->getElementParams('user', array('required' => false));
		  
		  if(isset($params['options']['multiOptions'])) {
		  	$extra_fields[$field->field_id]['options'] = $params['options']['multiOptions'];
		  }	
		}	
		
		return $extra_fields;	
	}

  public function birthdaysAction(){
    $this->view->users = Engine_Api::_()->user()->getUsersBirthdate('month');
  }
}
