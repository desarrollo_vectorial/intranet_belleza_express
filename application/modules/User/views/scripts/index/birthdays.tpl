<?php foreach ($this->users as $key => $user): ?>

  <?php
    $userDB = Engine_Api::_()->user()->getUser($user['user_id']);
  ?>

  <div class="">
    <div class="">
      <?php echo $this->itemPhoto($userDB, 'thumb.icon'); ?>
    </div>
    <div class="">
      <div class=""><?php echo $user['displayname']; ?></div>
    </div>
  </div>

<?php endforeach; ?>
