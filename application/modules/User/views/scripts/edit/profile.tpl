<style>
  
</style>
<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: profile.tpl 9984 2013-03-20 00:00:04Z john $
 * @author     John
 */
?>

<div class="headline">
    <div class="row">
        <?php 
        $navegacion = $this->navigation->toArray();
        foreach ($navegacion as $key => $item) : 
            if ($item['action'] != '') { ?>
                <a href="members/edit/<?php echo $item['action']; ?>" <?php if($item['active']) echo 'class="activo"'; ?>>
                    <div class="col">
                        <span><?php echo $item['label']; ?></span>
                    </div>
                </a>

            <?php }else{ ?>
                <a href="members/edit/<?php echo $item['controller']; ?>" <?php if($item['active']) echo 'class="activo"'; ?>>
                    <div class="col">
                        <span><?php echo $item['label']; ?></span>
                    </div>
                </a>
            <?php } ?>
        <?php endforeach; ?>
    </div>
</div>


<!--
<div class="headline">
  <h2>
    <?php /*if ($this->viewer->isSelf($this->user)):?>
      <?php echo $this->translate('Edit My Profile');?>
    <?php else:?>
      <?php echo $this->translate('%1$s\'s Profile', $this->htmlLink($this->user->getHref(), $this->user->getTitle()));?>
    <?php endif;?>
  </h2>

  <div class="tabs">
    <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render(); */ ?>
  </div>
</div>
-->


<div class="mi-contenedor">
  	<h2 style="color:#9f291d;display:none">
        <?php if ($this->viewer->isSelf($this->user)):?>
            <?php echo $this->translate('Edit My Profile');?>
        <?php else:?>
            <?php echo $this->translate('%1$s\'s Profile', $this->htmlLink($this->user->getHref(), $this->user->getTitle()));?>
        <?php endif;?>
        <br><br>
    </h2>

    <?php
    /* Include the common user-end field switching javascript */
    echo $this->partial('_jsSwitch.tpl', 'fields', array(
        'topLevelId' => (int) @$this->topLevelId,
        'topLevelValue' => (int) @$this->topLevelValue
      )); ?>

    <?php
      $this->headTranslate(array(
        'Everyone', 'All Members', 'Friends', 'Only Me',
      ));
    ?>
    <script type="text/javascript">
      window.addEvent('domready', function() {
        en4.user.buildFieldPrivacySelector($$('.global_form *[data-field-id]'));
      });
    </script>

    <?php echo $this->form->render($this) ?>

</div>