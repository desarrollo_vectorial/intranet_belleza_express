<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: _browseUsers.tpl 9979 2013-03-19 22:07:33Z john $
 * @author     John
 */
?>
<h3>
  <?php echo $this->translate(array('%s member found.', '%s members found.', $this->totalUsers),$this->locale()->toNumber($this->totalUsers)) ?>
</h3>
<?php $viewer = Engine_Api::_()->user()->getViewer();?>

<?php if( count($this->users) ): ?>
<div class="container" id="list--directorio">
    <ul id="browsemembers_ul" class="row">
        <?php foreach( $this->users as $user ): ?>
            <?php
            $table = Engine_Api::_()->getDbtable('block', 'user');
            $select = $table->select()
                ->where('user_id = ?', $user->getIdentity())
                ->where('blocked_user_id = ?', $viewer->getIdentity())
                ->limit(1);
            $row = $table->fetchRow($select);
            ?>

            <li class="col-6 align-self-stretch">
                <div class="list-user--content">
                    <div class="row" id="actions-member">
                        <div class="col-8 align-self-center text-left">
                            <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon')) ?>
                        </div>
                        <div class="col-4 align-self-center text-right" id="action-delete">
                            <?php if( $row == NULL ): ?>
                                <?php if( $this->viewer()->getIdentity() ): ?>
                                    <?php echo $this->userFriendship($user) ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row" id="actions-member">
                        <div class="col-12 align-items-center">
                            <div class="item-user-list"><?php echo $this->htmlLink($user->getHref(), $user->getTitle()) ?></div>
                            <?php foreach($this->extra_fields as $field_data): ?>
                                <?php if($field_data['options']): ?>
                                    <div><strong><?php echo $field_data['label']; ?>:</strong> <?php echo $field_data['options'][$user->{$field_data['label']}]; ?></div>
                                <?php else: ?>
                                    <div><strong><?php echo $field_data['label']; ?>:</strong> <?php echo $user->{$field_data['label']}; ?></div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <?php if( $user->status != "" ): ?>
                        <div class="col-12" id="state">
                            <h4>Estado:</h4>
                            <div class='members--result-list'>
                                    <div class="list--user-state">
                                        <p><?php echo $user->status; ?></p>
                                        <div class="date-list-user"><?php echo $this->timestamp($user->status_date) ?></div>
                                    </div>

                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<?php endif ?>

<?php if( $this->users ):
    $pagination = $this->paginationControl($this->users, null, null, array(
      'pageAsQuery' => true,
      'query' => $this->formValues,
    ));
  ?>
  <?php if( trim($pagination) ): ?>
    <div class='generic-paginator'>
      <?php echo $pagination ?>
    </div>
  <?php endif ?>
<?php endif; ?>

<script type="text/javascript">
  page = '<?php echo sprintf('%d', $this->page) ?>';
  totalUsers = '<?php echo sprintf('%d', $this->totalUsers) ?>';
  userCount = '<?php echo sprintf('%d', $this->userCount) ?>';
</script>