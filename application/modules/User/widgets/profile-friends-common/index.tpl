<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<div class="profile--common-friends">
    <div class="profile-options--titulo">
        <span><strong>Amigos en </strong>Común</span>
    </div>

	<?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
        <ul class="row">
			<?php foreach( $this->paginator as $user ): ?>
                <li class="col-3"><?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon', $user->getTitle()), array('title'=>$user->getTitle())) ?></li>
			<?php endforeach; ?>
        </ul>
	<?php else:?>
        <div class="tip">
          <span>
            <?php echo $this->translate('_reservas_widget-mis-reservas');?>
          </span>
        </div>
	<?php endif; ?>
</div>
<!--<div class="generic-paginator">
    <?php /*echo $this->paginationControl($this->paginator, null, null, array(
        'pageAsQuery' => true,
        'query' => 'p',
    )); */?>
</div>-->