<style>
  .contenedor-azul{ background-color: #a7a5a5 !important; padding: 20px; color: white; -webkit-border-radius:none !important; -moz-border-radius:none !important; border-radius:none !important; }
</style>

<div class="contenedor-azul">
  <p><?php echo $this->count ?> Usuarios En Línea</p>

  <?php foreach( $this->paginator as $user ): ?>
    <div class='whosonline_thumb'>
      <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon', $user->getTitle()), array('title'=>$user->getTitle())) ?>
    </div>
  <?php endforeach; ?>
  
  <?php if( $this->guestCount ): ?>
    <div class="online_guests">
      <?php echo $this->translate(array('%s guest online', '%s guests online', $this->guestCount),
          $this->locale()->toNumber($this->guestCount)) ?>
    </div>
  <?php endif ?>
</div>