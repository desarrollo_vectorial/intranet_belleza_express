<style>
  .contenedor-sign{ background-color: #a7a5a5; padding: 10px; text-align:center; color:white; margin-top: 10px !important;; }
  .contenedor-sign .titulo{ color: #FFF; font-size: 20px !important; }
  .contenedor-sign .titulo a{ color: #ab1607; font-size: 20px !important; }
</style>
<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<div class="contenedor-sign">
  <?php if( !$this->noForm ): ?>

    <p class="titulo"><?php echo $this->translate('Sign In or %1$sJoin%2$s', '<a href="'.$this->url(array(), "user_signup").'">', '</a>'); ?></p>

    <?php echo $this->form->setAttrib('class', 'form-elements')->render($this) ?>

    <?php if( !empty($this->fbUrl) ): ?>

      <script type="text/javascript">
        var openFbLogin = function() {
          Smoothbox.open('<?php echo $this->fbUrl ?>');
        }
        var redirectPostFbLogin = function() {
          window.location.href = window.location;
          Smoothbox.close();
        }
      </script>

      <?php // <button class="user_facebook_connect" onclick="openFbLogin();"></button> ?>

    <?php endif; ?>

  <?php else: ?>
      
    <h3 style="margin-bottom: 0px;">
      <?php echo $this->htmlLink(array('route' => 'user_login'), $this->translate('Sign In')) ?>
      <?php echo $this->translate('or') ?>
      <?php echo $this->htmlLink(array('route' => 'user_signup'), $this->translate('Join')) ?>
    </h3>

    <?php echo $this->form->setAttrib('class', 'global_form_box no_form')->render($this) ?>
      
  <?php endif; ?>
</div>