<style>
  .contenedor-sign{ margin: 0 auto; background-color: #fff; padding: 30px; color:white; margin-top: 50px !important; color: #717171; width: 50%; }
  .contenedor-sign h2{ color: #003399; text-align:left; font-weight: 300; font-size: 28px; padding-bottom: 10px; margin-bottom: 30px; border-bottom: 1px solid #ebebeb;}
  .contenedor-sign .titulo{ font-size: 15px !important; }
  .form-elements { padding-top: 5px; }
  .form-wrapper{ margin: 5px; }
  .form-label{ font-size: 16px; color: #fff; }
  .form_titulo{ margin: 5px; width: 90%; }
  div#submit-label { display: none; }
  div#submit-element { text-align: right; }
  button#submit { margin-right:22px; width: 100px; margin-top: 15px; border-radius: 5px;}
  div#error-datos { background: #cc7f7f; border-radius: 5px; padding: 15px; margin-bottom: 10px; color: #fff; text-align: center; }
  .form-label { color: #717171; }
  .form-login{ width: 100%; margin: 0 auto; }
  
  #usuario_login-wrapper, #password_login-wrapper {
  	display:inline-block;
      width: 47%;
  }
  #usuario_login-wrapper label, #password_login-wrapper label {
  	font-weight: 600;
	color: #666666;
	padding-left:5px;
  }
  #usuario_login-wrapper input, #password_login-wrapper input {
	border: 0px;
	background: #ebebeb;
	border-radius: 5px;
	height: 25px;
      width: 100%;
  }
</style>
<div class="contenedor-sign">
  <?php if($this->error_datos == 1){ ?>
    <div id="error-datos">
      <h3>
        Datos ingresados incorrectos, verifique sus datos.
      </h3>
    </div>
  <?php } ?>

  <h2>Identificación de Usuario</h2>
  <p class="titulo">
      Ingresa con tu usuario de red y tu contraseña.<br /><br />
      Si no tienes cuenta, comunícate con el administrador del servicio a este correo: <span class="correo">dchoyos@bellezaexpress.com</span><br><br>
  </p>

  <div class="form-login">    
    <?php echo $this->form->setAttrib('class', 'form-elements')->render($this) ?>
    </div>
</div>