<style>
  .generic_layout_container>p#titlePopular { padding: 10px; text-align: center; }
  .list-usu-pop{ padding: 10px !important; background-color:transparent !important;  -moz-border-radius: 0 !important; -webkit-border-radius: 0 !important; border-radius: 0 !important; }
</style>
<p id="titlePopular">Usuarios Nuevos</p>

<ul class="list-usu-pop">  
  
  <?php foreach( $this->paginator as $user ): ?>
    <li>
      <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon', $user->getTitle()), array('class' => 'newestmembers_thumb')) ?>
      <div class='newestmembers_info'>
        <div class='newestmembers_name'>
          <?php echo $this->htmlLink($user->getHref(), $user->getTitle()) ?>
        </div>
        <div class='newestmembers_date'>
          <?php echo $this->timestamp($user->creation_date) ?>
        </div>
      </div>
    </li>
  <?php endforeach; ?>
</ul>