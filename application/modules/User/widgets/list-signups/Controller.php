<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_ListSignupsController extends Engine_Content_Widget_Abstract{
  public function indexAction(){
    $table  = Engine_Api::_()->getDbtable('users', 'user');
    $select = $table->select()
                    ->where('search = ?', 1)
                    ->where('enabled = ?', 1)
                    ->order('creation_date DESC')
                    ->limit(5);
    $rowset = $table->fetchAll($select);

    $this->view->paginator = $paginator = Zend_Paginator::factory($rowset);

    // Do not render if nothing to show
    if( $paginator->getTotalItemCount() <= 0 ) return $this->setNoRender();
  }

  public function getCacheKey(){
    $viewer = Engine_Api::_()->user()->getViewer();
    $translate = Zend_Registry::get('Zend_Translate');
	  $locale = Zend_Registry::get('Locale');
    return $viewer->getIdentity() . $translate->getLocale() . $locale->toString();
  }

  public function getCacheSpecificLifetime(){
    return 120;
  }
}