<style type="text/css" media="screen">
	.layout_user_home_photo { background-color: #717171 !important; display : block; position: relative; margin-top: 10px !important; margin: 0 auto; border: none !important; }
	.layout_user_home_photo p{ color: white; font-size: 14px; font-weight:normal; margin-top: 10px; margin-left: 20px; display: block; position: relative; }
	.layout_user_home_photo img{ padding-top: 10px; padding-bottom: 10px; width: 300px; border: none !important; max-width:250px !important; margin-left: 20px !important; }
</style>

<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>

<p>
  <?php echo $this->translate('Hi %1$s!', $this->viewer()->getTitle()); ?>
</p>

<?php echo $this->htmlLink($this->viewer()->getHref(), $this->itemPhoto($this->viewer(), 'thumb.profile')) ?>