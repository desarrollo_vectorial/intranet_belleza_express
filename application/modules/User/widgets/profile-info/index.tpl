<div class="profile-options--titulo">
    <span><strong>Información del </strong>Perfil</span>
</div>

<div class="user--perfil-info">
    <ul>
		<?php if( !empty($this->memberType) ): ?>
            <li style="display:none">
				<?php echo $this->translate('Member Type:') ?>
				<?php // @todo implement link ?>
				<?php echo $this->translate($this->memberType) ?>
            </li>
		<?php endif; ?>
        <li class="box-indicadores">
            <div class="indicador"><?php echo $this->locale()->toNumber($this->subject->view_count); ?></div>
            <div class="nombre-indicador"><?php echo $this->translate('views'); ?></div>
        </li>

		<?php if( !empty($this->networks) && count($this->networks) > 0 ): ?>
            <li class="fila-redes">
                <span class="nombre-indicador"><?php echo $this->translate('Networks:') ?></span>
				<?php echo $this->fluentList($this->networks, true) ?>
            </li>
		<?php endif; ?>
        <li class="user--indicador">
            <span class="nombre-indicador"><?php echo $this->translate('Last Update:'); ?></span>
			<?php
			if($this->subject->modified_date != "0000-00-00 00:00:00"){
				echo $this->timestamp($this->subject->modified_date);
			}
			else{
				echo $this->timestamp($this->subject->creation_date);
			}
			?>
        </li>
        <li>
            <span class="nombre-indicador"><?php echo $this->translate('Joined:') ?></span>
			<?php echo $this->timestamp($this->subject->creation_date) ?>
        </li>
		<?php if( !$this->subject->enabled && $this->viewer->isAdmin() ): ?>
            <li>
                <em>
					<?php echo $this->translate('Enabled:') ?>
					<?php echo $this->translate('No') ?>
                </em>
            </li>
		<?php endif; ?>
    </ul>
    <script type="text/javascript">
        $$('.core_main_user').getParent().addClass('active');
    </script>
</div>