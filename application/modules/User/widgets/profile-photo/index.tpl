<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<?php

?>
<div class="profile--photo">
    <?php if ($this->user->getPhotoUrl("thumb") != null): ?>
        <span style="background-image: url(<?php echo $this->user->getPhotoUrl("thumb"); ?>);"></span>
    <?php else: ?>
        <span style="background-image: url('<?php echo $this->baseUrl(); ?>/application/modules/User/externals/images/nophoto_user_thumb_profile.png');">
        </span>
    <?php endif; ?>

    <div class="user--name"><?php echo $this->subject()->getTitle() ?></div>
</div>