<?php

class User_Widget_UserBirthdayController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    $this->view->range = $range = $this->_getParam('range', null);
    $this->view->display = $display = $this->_getParam('display', null);
    $this->view->users = Engine_Api::_()->user()->getUsersBirthdate($range);

    if ( count($this->view->users) == 0 ){
      return $this->setNoRender();
    }
  }
}
