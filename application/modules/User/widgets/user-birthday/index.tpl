
<?php if ( $this->display == 'calendar' ): ?>

  <div id="birthdate_callendar_widget"></div>

  <script type="text/javascript">

    jQuery.noConflict();

    (function($) { // START NAMESPACE

      $(document).ready(function(){

        var today = new Date();

        var defaultView = <?php echo json_encode($this->range); ?>;
        switch (defaultView) {
          case 'day':
            defaultView = 'agendaDay';
            break;
          case 'week':
            defaultView = 'agendaWeek';
            break;
          case 'month':
            defaultView = 'month';
            break;
        }

        var users = <?php echo json_encode($this->users); ?>;
        var events = [];
        for ( var i = 0; i < users.length; i++ ){
          events.push({
  					title: users[i].displayname,
  					start: today.getFullYear() + "-" + users[i].birthdate,
  				});
        }

        $('#birthdate_callendar_widget').fullCalendar({
          height: 'auto',
          header: {
            left: false,
            right: false
    			},
          defaultView: defaultView,
          events: events
        });

      });

    })(jQuery);

  </script>

<?php elseif ( $this->display == 'list' ): ?>

  <style>
    .layout_user_user_birthday{
      margin: 20px 15px 20px 0px;
      box-shadow: 6px 6px 6px 0px rgba(0,0,0,0.22),0 25px 55px 0 rgba(0,0,0,0.21) !important;
    }
    .layout_user_user_birthday .list_container{
      background-color: #ab1607;
    }
    .layout_user_user_birthday .list_container .row{
      border-bottom: 1px solid #b3d47a;
      margin-bottom: 15px;
    }
    .layout_user_user_birthday .list_container .row_title{
      padding: 10px 0px;
      margin: 0px 15px;
      margin-right: 30px;
      display: inline-block;
    }
    .layout_user_user_birthday .list_container .title{
      font-size: 20px;
      color: #fff;
      font-weight: 500;
    }
    .layout_user_user_birthday .list_container .title_date{
      color: #ab1607;
      background-color: #fff;
      border-radius: 10px;
      padding: 0px 10px;
      display: inline-block;
    }
    .layout_user_user_birthday .list_container .users_list{
      margin: 10px 0px;
    }
    .layout_user_user_birthday .list_container .users_list .user_icon{
      display: inline-block;
      vertical-align: middle;
      margin: 0px 15px;
    }
    .layout_user_user_birthday .list_container .users_list .user_icon img{
      vertical-align: middle;
      border: 2px solid #fff;
      border-radius: 50%;
      width: 52px;
      height: auto;
    }
    .layout_user_user_birthday .list_container .users_list .user_name{
      display: inline-block;
      vertical-align: middle;
      width: 120px;
    }
    .layout_user_user_birthday .list_container .users_list .user_name .user_displayname{
      font-size: 14px;
      color: #fff;
      font-weight: 500;
    }
    .layout_user_user_birthday .list_container .users_list .user_name .user_cargo{
      font-size: 13px;
      color: #fff;
      font-weight: normal;
    }
    .layout_user_user_birthday .list_container .users_list .user_messages{
      display: inline-block;
      vertical-align: middle;
    }
    .layout_user_user_birthday .list_container .users_list .user_messages .messages_icon{
      background-image: url('/application/modules/User/externals/images/messages_icon.png');
      background-repeat: no-repeat;
      width: 20px;
      height: 18px;
      display: inline-block;
      vertical-align: middle;
      margin: 0px 5px;
    }
    .layout_user_user_birthday .list_container .users_list .user_messages .messages_count{
      width: 30px;
      height: 30px;
      background-image: url('/application/modules/User/externals/images/messages_count_bg.png');
      background-repeat: no-repeat;
      text-align: center;
      padding-top: 6px;
      color: #ab1607;
      font-size: 14px;
      font-weight: 500;
      display: inline-block;
      vertical-align: middle;
    }
    .layout_user_user_birthday .list_container .all_birthdays_link{
      padding-bottom: 10px;
      text-align: center;
    }
    .layout_user_user_birthday .list_container .all_birthdays_link a{
      color: #fff;
    }
    .layout_user_user_birthday .list_container .all_birthdays_link p{

    }
  </style>

  <div class="list_container">
    <div class="row">
      <div class="row_title">
        <h6 class="title">Cumpleaños</h6>
      </div>
      <div class="title_date">
        <?php echo Engine_Api::_()->core()->getDateFormat(Date('Y-m-d'), 'mmm-d'); ?>
      </div>
    </div>

    <?php foreach ($this->users as $key => $user): ?>

      <?php
        $userDB = Engine_Api::_()->user()->getUser($user['user_id']);
      ?>

      <div class="users_list">
        <div class="user_icon">
          <?php echo $this->itemPhoto($userDB, 'thumb.icon'); ?>
        </div>
        <div class="user_name">
          <div class="user_displayname"><?php echo $user['displayname']; ?></div>
          <div class="user_cargo"><?php echo $user['displayname']; ?></div>
        </div>
        <div class="user_messages">
          <a href="<?php echo $userDB->getHref(); ?>"><div class="messages_icon"></div></a>
          <div class="messages_count"><?php echo $userDB->getTodayCommentsCount(); ?></div>
        </div>
      </div>

    <?php endforeach; ?>

    <div class="all_birthdays_link">
      <a href="/members/index/birthdays"><p>Ver todos los cumpleaños del mes</p></a>
    </div>
  </div>

<?php endif; ?>
