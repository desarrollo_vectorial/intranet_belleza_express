<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<!--
<div class="quicklinks">
  <?php // This is rendered by application/modules/core/views/scripts/_navIcons.tpl
    /*echo $this->navigation()
      ->menu()
      ->setContainer($this->navigation)
      ->setPartial(array('_navIcons.tpl', 'core'))
      ->render(); */
  ?>
</div>-->


<div class="headline">
    <div class="row">
        <?php 
        $navegacion = $this->navigation->toArray();
        foreach ($navegacion as $key => $item) : 
            if ($item['action'] != '') { ?>
                <a href="members/edit/<?php echo $item['action']; ?>" <?php if($item['active']) echo 'class="activo"'; ?>>
                    <div class="col">
                        <span><?php echo $item['label']; ?></span>
                    </div>
                </a>

            <?php }else{ ?>
                <a href="members/edit/<?php echo $item['controller']; ?>" <?php if($item['active']) echo 'class="activo"'; ?>>
                    <div class="col">
                        <span><?php echo $item['label']; ?></span>
                    </div>
                </a>
            <?php } ?>
        <?php endforeach; ?>
    </div>
</div>