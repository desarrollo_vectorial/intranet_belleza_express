<style>
  .contenedor-sign{ background-color: #a7a5a5; padding: 10px; text-align:center; color:white; margin-top: 10px !important;; }
  .contenedor-sign .titulo{ color: #FFF; font-size: 20px !important; }
  .contenedor-sign .titulo a{ color: #ab1607; font-size: 20px !important; }
  .form-elements { padding-top: 5px; }
  .form-wrapper{ margin: 5px; }
  .form-label{ font-size: 16px; color: #fff; }
  .form_titulo{ margin: 5px; width: 90%; }
  div#submit-label { display: none; }
  div#submit-element { text-align: right; }
  button#submit { margin-right:10px; width: 100px; }
  div#error-datos { background: #cc7f7f; border-radius: 5px; padding: 15px; margin-bottom: 10px; color: #fff; text-align: center; }
</style>
<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<div class="contenedor-sign">

  <?php if($this->error_datos == 1){ ?>
    <div id="error-datos">
      <h3>
        Datos ingresados incorrectos, verifique sus datos.
      </h3>
    </div>
  <?php } ?>



  <?php if( !$this->noForm ): ?>
    <p class="titulo">Por favor ingresa sus datos</p>
    <?php echo $this->form->setAttrib('class', 'form-elements')->render($this) ?>
  <?php else: ?>
    <h3 style="margin-bottom: 0px;">
      <?php echo $this->htmlLink(array('route' => 'user_login'), $this->translate('Sign In')) ?>
      <?php echo $this->translate('or') ?>
      <?php echo $this->htmlLink(array('route' => 'user_signup'), $this->translate('Join')) ?>
    </h3>
    <?php echo $this->form->setAttrib('class', 'global_form_box no_form')->render($this) ?>

  <?php endif; ?>
</div>