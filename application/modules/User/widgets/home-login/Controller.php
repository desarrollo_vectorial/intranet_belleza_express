<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_HomeLoginController extends Engine_Content_Widget_Abstract{
  public function indexAction(){
    // Do not show if logged in
    if( Engine_Api::_()->user()->getViewer()->getIdentity() ) {
      $this->setNoRender();
      return;
    }

    // Display form
    $this->view->form = new User_Form_Homelogin();
    $this->view->error_datos = 0;
    

    if(isset($_POST['usuario_ldap'])){      
      $this->ldap_con      = ldap_connect("172.26.6.103");
      $this->userPost      = trim($_POST['usuario_ldap']);
      $this->ldap_usuario  = trim("CENTELSACO\ ").$this->userPost;
      $this->ldap_clave    = $_POST['password_ldap'];
      $this->usuario_sea   = $this->userPost.'@centelsa.com.co';
      $this->filtroldap    = "(mail=$this->usuario_sea)";
      
      ldap_set_option($this->ldap_con, LDAP_OPT_PROTOCOL_VERSION, 3);
      
      if(ldap_bind($this->ldap_con, $this->ldap_usuario, $this->ldap_clave)){
        $this->searchldap  = ldap_search($this->ldap_con, "dc=centelsa,dc=cables,dc=xignux,dc=com", $this->filtroldap);

        // Verificar si el usuario existe en el servidor ldap
        if ($this->searchldap) {
          
          // Leer multiples entradas de los resultados porporcionados.
          $info = ldap_get_entries($this->ldap_con, $this->searchldap);
          ldap_close($this->ldap_con);

          $ii        = 0;
          $user_info = array();
          $data      = array();

          // Extraer los datos de las entradas
          for ($i = 0; $ii < $info[$i]['count']; ++$ii) {
            $data             = $info[$i][$ii];
            $user_info[$data] = $info[$i][$data][0];
          }

          // Verificar si la entrada tiene email
          if (isset($user_info['mail']) && !is_null($user_info['mail'])) {

            // Verificar el usuario en db
            if (! $this->verifyUser($this->userPost)) {
              // Crear usuario en db
              $this->registrar($this->userPost, $user_info);
              $this->seguirTodosLosAdministradores();
            } else {
              // Actualizar el cin del usuario en db
              $this->actualizarCinUsuario($this->userPost, $user_info);
            }         

            // crear red y user_fields_values
            $redes = array();
            $redes = $this->crearPersonalizacion($this->userPost, $user_info);

            // actualizar contraseña y datos basicos del usuario
            $this->actualizarContrasena($this->userPost, $user_info);

            if (is_array($redes)) {
              $this->vincularARed($user_info, $this->userPost, $redes);
              $this->autenticar($this->userPost, $user_info);
              header('Location: http://'.HTTP_HOST.$_SERVER['REQUEST_URI']);
              exit;

            } else {
              echo Zend_Registry::get('Zend_Translate')->_('Error: Se requiere actuailzar tus datos en el Directorio Activo<br />');
            }
          }
        }

      }else{
        $this->view->error_datos = 1;
      }


    }    

  }


    protected function verifyUser($username){
      $db   = Zend_Db_Table_Abstract::getDefaultAdapter();
      $stmt = $db->query('SELECT * FROM engine4_users WHERE username="'.$username.'"');
      $rows = $stmt->fetchAll();
      if (is_array($rows) && (count($rows) > 0)) { return true; }
      return false;
    }  

    protected function actualizarContrasena($username, $user_info){
      $userTable = Engine_Api::_()->getDbtable('users', 'user');
      $db        = $userTable->getAdapter();
      $db->beginTransaction();

      try {
        $staticSalt = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.secret', 'staticSalt');
        $chars    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $password = '';
        $length   = 8;
        $size     = strlen($chars);

        $password = $user_info['whencreated'];
        $salt     = rand(1000000, 9999999);
        $db       = Engine_Db_Table::getDefaultAdapter();
        $ipObj    = new Engine_IP();
        $ipExpr   = new Zend_Db_Expr($db->quoteInto('UNHEX(?)', bin2hex($ipObj->toBinary())));
        $data     = array('username' => $username, 'displayname' => $user_info['displayname'], 'email' => $user_info['mail'], 'password' => md5($staticSalt.$password.$salt), 'salt' => $salt, 'enabled' => 1);
        $where    = $userTable->getAdapter()->quoteInto("( LOWER( RTRIM(email ) ) = LOWER( RTRIM('".$user_info['mail']."') ) )");
        $userTable->update($data, $where);
        $db->commit();
      } catch (Exception $e) {
        print_r($e->getMessage().' b');
      }
    }

    protected function registrar($username, $user_info){
      $userTable = Engine_Api::_()->getDbtable('users', 'user');
      $db        = $userTable->getAdapter();
      $db->beginTransaction();
      try {
        $staticSalt = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.secret', 'staticSalt');
        $chars      = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $password   = '';
        $length     = 8;
        $size       = strlen($chars);


        $password = $user_info['whencreated'];
        $salt     = rand(1000000, 9999999);
        $db       = Engine_Db_Table::getDefaultAdapter();

        $ipObj    = new Engine_IP();
        $ipExpr   = new Zend_Db_Expr($db->quoteInto('UNHEX(?)', bin2hex($ipObj->toBinary())));

        $userTable->insert(
          array(
            'email'         => $user_info['mail'],
            'username'      => $username,
            'displayname'   => $user_info['name'],
            'photo_id'      => 0,
            'password'      => md5($staticSalt.$password.$salt),
            'salt'          => $salt,
            'language'      => 'es',
            'level_id'      => 4,
            'creation_date' => date('Y-m-d H:i:s'),
            'modified_date' => date('Y-m-d H:i:s'),
            'creation_ip'   => $ipExpr, 
            'enabled'       => 1,
            'verified'      => 1,
            'approved'      => 1,
          )
        );

        $this->new_user_id = $userTable->getAdapter()->lastInsertId();
        $db->commit();
      } catch (Exception $e) {
        print_r($e->getMessage().' b');
      }
    }

    protected function seguirTodosLosAdministradores(){
        if ($this->new_user_id == null) {
          return;
        }

        $userTable  = Engine_Api::_()->getDbtable('users', 'user');
        $db         = $userTable->getAdapter();
        $db->beginTransaction();
        $adminUsers = $userTable->fetchAll();
        $membership = Engine_Api::_()->getDbtable('users', 'user');

        foreach ($adminUsers as $key => $adminUser) {
          if ($adminUser->isAdmin()) {
            $admin_id = $adminUser['user_id'];
            $u_id     = $this->new_user_id;

            $dbQuery    = Zend_Db_Table_Abstract::getDefaultAdapter();
            $membership = $dbQuery->query('SELECT * FROM engine4_user_membership WHERE resource_id = '.$admin_id.' AND user_id = '.$u_id.';');
            $rows       = $membership->fetchAll();

            if (count($rows) == 0) {
              $dbQueryInsert = Zend_Db_Table_Abstract::getDefaultAdapter();
              $dbQueryInsert->query('INSERT INTO engine4_user_membership (resource_id, user_id, active, resource_approved, user_approved) VALUES('.$admin_id.', '.$u_id.', 1, 1, 1);');
              $dbQueryInsert->commit();
            }

            $dbQuery    = Zend_Db_Table_Abstract::getDefaultAdapter();
            $membership = $dbQuery->query('SELECT * FROM engine4_user_membership WHERE resource_id = '.$u_id.' AND user_id = '.$admin_id.';');
            $rows       = $membership->fetchAll();

            if (count($rows) == 0) {
              $dbQueryInsert = Zend_Db_Table_Abstract::getDefaultAdapter();
              $dbQueryInsert->query('INSERT INTO engine4_user_membership (resource_id, user_id, active, resource_approved, user_approved) VALUES('.$u_id.', '.$admin_id.', 1, 1, 1);');
              $dbQueryInsert->commit();
            }
          }
        }
    }

    protected function actualizarCinUsuario($username, $user_info){
      try {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        // Obtener datos de usuario en db
        $stmt = $db->query('SELECT * FROM engine4_users WHERE username="'.$username.'"');
        $rows = $stmt->fetchAll();
        $uid  = $rows[0]['user_id'];
        $db->query('DELETE FROM engine4_user_fields_values WHERE item_id = '.$uid.' AND field_id = 83;');
        $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 83, \''.$user_info['postalcode'].'\');');
        $db->commit();
      } catch (Exception $e) {
        var_dump($e->getMessage());
      }
    }

    protected function crearPersonalizacion($username, $user_info){
      $sql   = '';
      $arbol = split(',', $user_info['distinguishedname']);

      if (strcmp($arbol[count($arbol) - 4], 'OU=Internacional') == 0) {
        $pais = str_replace('OU=', '', $arbol[count($arbol) - 5]);
      } else {
        $pais = str_replace('OU=', '', $arbol[count($arbol) - 4]);
      }

      try {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        // Obtener datos de usuario en db
        $stmt = $db->query('SELECT * FROM engine4_users WHERE username="'.$username.'"');
        $rows = $stmt->fetchAll();
        $uid  = $rows[0]['user_id'];

        // Insertar la red si no existe ya en la Red del pais
        $db->query("INSERT INTO engine4_network_networks (title,description,field_id,member_count,hide,assignment )
                    SELECT a.* FROM(SELECT '".trim($pais)."' as title, '".trim($pais)."' as descripcion, 0 as field_id, 0 as member_count, 0 as hide, 0 as assignment) as a
                    WHERE NOT EXISTS( SELECT * FROM engine4_network_networks WHERE title = '".trim($pais)."' ); ");

        // Obtener red por pais
        $stmt        = $db->query('SELECT * FROM engine4_network_networks WHERE title=\''.trim($pais).'\'');
        $redesPaises = $stmt->fetchAll();

        if ($redesPaises) {
          $db->query('DELETE FROM engine4_user_fields_values WHERE item_id = '.$uid.' AND field_id IN(1,3,4,74,79,81,83);');
          $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 3, \''.$user_info['givenname'].'\');');
          $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 4, \''.$user_info['sn'].'\');');
          $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 79, \''.$user_info['title'].'\');');
          $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 81, \''.$user_info['telephonenumber'].'\');');
          $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 83, \''.$user_info['postalcode'].'\');');
          $db->query('INSERT INTO engine4_user_fields_values (item_id, field_id, value) VALUES ('.$uid.', 74, \''.$user_info['department'].'\');');
          $db->commit();
        }
      } catch (Exception $e) {
        var_dump($e->getMessage());
      }

      return $redesPaises;
    }

    protected function vincularARed($user_info, $username, $redes){
      try {
        echo Zend_Registry::get('Zend_Translate')->_('Join to network country ...').'<br />';
        $db   = Zend_Db_Table_Abstract::getDefaultAdapter();
        $stmt = $db->query('SELECT * FROM engine4_users WHERE username =  "'.$username.'"');
        $rows = $stmt->fetchAll();
        $uid  = $rows[0]['user_id'];

        if ($rows[0]['level_id'] >= 4) {
          $db->query('DELETE FROM engine4_network_membership WHERE user_id = '.$uid.';');
          $db->query('INSERT INTO engine4_network_membership (resource_id, user_id, active, resource_approved, user_approved) VALUES('.$redes[0]['network_id'].', '.$uid.', 1, 1, 1);');
          $db->query('INSERT INTO engine4_network_membership (resource_id, user_id, active, resource_approved, user_approved) VALUES(8, '.$uid.', 1, 1, 1);');
          $db->query('UPDATE engine4_network_networks AS a,
                  (
                      SELECT a.resource_id, COUNT( * ) AS cantidad
                      FROM engine4_network_membership AS a
                      JOIN engine4_users AS b ON ( a.user_id = b.user_id )
                      WHERE TRUE
                      GROUP BY a.resource_id
                  ) AS b
                  SET
                  a.member_count = b.cantidad
                  WHERE
                  a.network_id = b.resource_id;');
          $db->commit();
        }
      } catch (Exception $e) {
        print_r($e->getMessage());
      }
    }

    protected function autenticar($username, $user_info){
      try {
        echo Zend_Registry::get('Zend_Translate')->_('make the auth proccess ...').'<br />';
        $db = Engine_Db_Table::getDefaultAdapter();
        $ipObj = new Engine_IP();
        $ipExpr = new Zend_Db_Expr($db->quoteInto('UNHEX(?)', bin2hex($ipObj->toBinary())));
        $user_table = Engine_Api::_()->getDbtable('users', 'user');
        $user_select = $user_table->select()->where('email = ?', $user_info['mail']);
        // If post exists
        $user = $user_table->fetchRow($user_select);
        $authResult = Engine_Api::_()->user()->authenticate($user_info['mail'], $user_info['whencreated']);
        $authCode = $authResult->getCode();
        Engine_Api::_()->user()->setViewer();
        $loginTable = Engine_Api::_()->getDbtable('logins', 'user');
        $loginTable->insert(array('user_id' => $user->getIdentity(), 'email' => $user_info['mail'], 'ip' => $ipExpr, 'timestamp' => new Zend_Db_Expr('NOW()'), 'state' => 'success', 'active' => true));
        $_SESSION['login_id'] = $login_id = $loginTable->getAdapter()->lastInsertId();
        Engine_Api::_()->getDbtable('statistics', 'core')->increment('user.logins');
        // Test activity @todo remove
        $viewer = Engine_Api::_()->user()->getViewer();

        if ($viewer->getIdentity()) {
          $viewer->lastlogin_date = date('Y-m-d H:i:s');

          if ('cli' !== PHP_SAPI) {
            $viewer->lastlogin_ip = $ipExpr;
          }

          $viewer->save();
          Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($viewer, $viewer, 'login');
        }
      } catch (Exception $e) {
        print_r($e->getMessage().' a');
      }
    }

}