<style>
  .generic_layout_container>p#titlePopular { padding: 10px; text-align: center; }
  .list-usu-pop{ padding: 10px !important; background-color:transparent !important;  -moz-border-radius: 0 !important; -webkit-border-radius: 0 !important; border-radius: 0 !important; }
</style>
<p id="titlePopular">Usuarios Populares</p>
<ul class="list-usu-pop">
  <?php foreach( $this->paginator as $user ): ?>
    <li>
      <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon', $user->getTitle()), array('class' => 'popularmembers_thumb')) ?>
      <div class='popularmembers_info'>
        <div class='popularmembers_name'>
          <?php echo $this->htmlLink($user->getHref(), $user->getTitle()) ?>
        </div>
        <div class='popularmembers_friends'>
          <?php echo $this->translate(array('%s friend', '%s friends', $user->member_count),$this->locale()->toNumber($user->member_count)) ?>
        </div>
      </div>
    </li>
  <?php endforeach; ?>
</ul>
