<?php
class Reserva_Model_DbTable_Reservas extends Engine_Db_Table{
	protected $_rowClass = "Reserva_Model_Reserva";

	/*Función general para cancelar reservas*/
	public function cancelarReserva($reserva_id, $user_id) {
        $datos = array();
        $reservaTable = Engine_Api::_()->getDbtable('reservas', 'reserva');
        $reserva = $reservaTable->find($reserva_id)->current();

        $datos['estado'] = '3';
        $datos['user_id'] = $user_id;
        $datos['modified']=date('Y-m-d H:i:s');

        $db = $reservaTable->getAdapter();
        $db->beginTransaction();
        try	{
            $reserva->setFromArray($datos);
            $reserva->save();
        }catch( Exception $e ){
            $db->rollBack();
            throw $e;
            echo false;
        }
        $db->commit();

        return true;
    }

	public function getReservasPaginator($params = array(), $customParams = array())
	{
    	$paginator = Zend_Paginator::factory($this->getReservasSelect($params, $customParams));
		if( !empty($params['page']) ) {
			$paginator->setCurrentPageNumber($params['page']);
		}
		if( !empty($params['limit']) ) {
			$paginator->setItemCountPerPage($params['limit']);
		}
		return $paginator;
	}
	
	public function getReservasSelect($params = array(), $customParams = array()){
		// echo "<pre>".print_r($customParams,true)."</pre>";
		$reserva = Engine_Api::_()->getDbtable('reservas', 'reserva');
		$itemTable = Engine_Api::_()->getDbtable('items', 'reserva');
		
		$query = $reserva->select()
					->setIntegrityCheck(false)
					->from(array("a" => $reserva->info('name')),array(	"id"=> "a.reserva_id",
																		"title"=>"CONCAT('',a.title,'<br>',c.displayname)",
																		"title_anex"=>"a.title",
																		"num_personas"=>"a.num_personas",
																		/*"responsable_reserva"=>"a.responsable_reserva",*/
																		"telefono" => "a.telefono",
																		"extension"=> "a.extension",
																		/*"responsable_correo" => "a.responsable_correo",*/
																		"start"=>"CONCAT(a.fecha_reserva,'T',a.hora_inicio)",
																		"end"=>"CONCAT(a.fecha_reserva,'T',a.hora_fin)",
																		"observaciones"=>"a.observaciones",
																		"observacion"=>"a.observacion",/*
																		"backgroundColor"=>"CONCAT('#',a.color)",*/
																		"hora_inicio" => "a.hora_inicio",
																		"hora_fin" => "a.hora_fin",
																		"fecha_reserva" => "a.fecha_reserva",
																		"estado" => "a.estado",
																		"className" => "CONCAT('','smoothbox')",
																		"url_"=>"CONCAT('','reserva/resumen/',a.reserva_id)",
																		"user_id" => "a.user_id"																			
																		))
					->joinLeft(array("b"=>"engine4_reserva_recursos"),"a.recurso_id = b.recurso_id",array("backgroundColor"=>"CONCAT('#',b.color)","recurso_fisico"=>"b.title"))
					->joinLeft(array("d"=>"engine4_user_fields_options"),"a.option_id = d.option_id",array("d.label"))
					->join(array("c"=>"engine4_users"),"a.user_id = c.user_id",array(
																						"responsable_reserva"=>"c.displayname",
																						"responsable_correo"=>"c.email"
																					))					->order('a.fecha_reserva');
		
		if($params['moderador_id'])
			$query->where("b.user_id =?",$params['moderador_id']);
				
		if($params['user_id'])
			$query->where("a.user_id =?",$params['user_id']);
		if($customParams['estado'])
			$query->where("a.estado =?",$customParams['estado']);
		if($customParams['option_id'])
				$query->where("a.option_id =?",$customParams['option_id']);
        if(isset($params['status']) && $params['status'])
            $query->where("a.estado =?",$params['status']);
		if($customParams['fecha_reserva'])
			$query->where("a.fecha_reserva =?",$customParams['fecha_reserva']);
			else
				$query->where("a.fecha_reserva >= '".$this->fechaNueva()."'");	
		return $query;
	}
	
	public function fechaNueva(){
		$fecha = date('Y-m-d');
		$nuevafecha = strtotime ( '-2 day' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'Y-m-d' , $nuevafecha );
		return $nuevafecha;
	}
	
	public function guardarReserva($datos){
		$nuevo = $this->createRow();
		unset($datos['reserva_id']);
		$nuevo->setFromArray($datos);
		$nuevo->save();
		return $nuevo->reserva_id;
	}
}

?>