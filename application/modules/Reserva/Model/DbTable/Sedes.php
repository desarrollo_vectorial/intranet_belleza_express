<?php
class Reserva_Model_DbTable_Sedes extends Engine_Db_Table{
	protected $_rowClass = "Reserva_Model_Sede";
	
	public function obtenerSedes($sede_id = null){
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		
		if($sede_id)
			$sql = " AND b.option_id = ".$sede_id;
		
		$stmt = $db->query('SELECT 
							b.option_id,
							b.label
							FROM 
							`engine4_reserva_sedes` as a
							JOIN `engine4_user_fields_options` as b ON(a.id = b.field_id)
							WHERE TRUE
							'.$sql.'
							ORDER BY label;');
				
		$rows = $stmt->fetchAll();
		
		/*echo "<pre>".print_r($rows,true)."</pre>";*/
		
		$sedes = array(''=>'Seleccionar sede/empresa');
		foreach($rows as $key => $valor){
			$sedes[$valor['option_id']]=$valor['label'];
		}
		return $sedes;
	}
}
?>