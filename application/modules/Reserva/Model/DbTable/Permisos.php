<?php
class Reserva_Model_DbTable_Permisos extends Engine_Db_Table{
	protected $_rowClass = "Reserva_Model_Permiso";
	
	public function usuariosModulos($opc = true){	
		$table = Engine_Api::_()->getItemTable('user');
		$query = $table->select()
						->setIntegrityCheck(false)
						->from(array("a" => $table->info('name')),array("a.*"))
						->order('a.displayname');
		if($opc)
			$query->where(new Zend_Db_Expr("a.user_id NOT IN(select user_id from engine4_reserva_permisos)"));
			else
				$query->where(new Zend_Db_Expr("a.user_id IN(select user_id from engine4_reserva_permisos)"));
		$rst = $query->query();			
		$usuarios = $rst->fetchAll();
		
		$resultados = array();
		foreach($usuarios as $key => $valor){
			$resultados[$valor['user_id']] = $valor['displayname'];
		}
		return $resultados;
	}
}

?>