<?php
class Reserva_Model_DbTable_Recursos extends Engine_Db_Table{
	protected $_rowClass = "Reserva_Model_Recurso";


	public function getRecursos($type = null, $fields = array()){
		$recursos = Engine_Api::_()->getDbtable('recursos', 'reserva');

		$query = $recursos->select()->order('title');
		
		if($fields) {
			$query->from($recursos->info('name'), $fields );
		}		
		
		if($type) {
			$query->where('tipo = '.$type);
		}
		
		
		return $query->query()->fetchAll();
	}

	public function getRecursoById( $recurso_id ){

        $select = $this->select()
            ->from( $this->info("name") )
            ->where("recurso_id =".$recurso_id);

        $recurso = $this->fetchAll( $select );
        $recurso = $recurso->toArray();
        return ($recurso[0])?$recurso[0]:[];
    }
		
}

?>