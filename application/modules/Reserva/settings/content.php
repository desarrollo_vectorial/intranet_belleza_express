<?php
return array
(
    Array
    (
        'title' => 'WIDGET SubMenu Reservas',
        'description' => 'Despliega el Submenú de Reservas',
        'category' => 'Reservas',
        'type' => 'widget',
        'name' => 'reserva.reserva-submenu'
    ),
    Array
    (
        'title' => 'Mis reservas',
        'description' => 'Despliega las reservas del usuario',
        'category' => 'Reservas',
        'type' => 'widget',
        'name' => 'reserva.misreservas'
    ),
    array(
        'title'       => 'Breadcrumbs - Reserva',
        'description' => 'Muestra en que modulo se encuestra actualmente',
        'category'    => 'Reservas',
        'type'        => 'widget',
        'name'        => 'reserva.breadcrumbs'
    ),
    array(
        'title'       => 'Banner Interna',
        'description' => 'Muestra un banner encima de las tabs.',
        'category'    => 'Reservas',
        'type'        => 'widget',
        'name'        => 'reserva.internalbanner'
    ),
);

