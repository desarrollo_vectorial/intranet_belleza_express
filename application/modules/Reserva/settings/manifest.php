<?php return array(
    'package' =>
        array(
            'type' => 'module',
            'name' => 'reserva',
            'version' => '4.0.0',
            'path' => 'application/modules/Reserva',
            'title' => 'Modulo de Reservas',
            'description' => 'Modulo de Reservas',
            'author' => 'mmedina@vectorial.co',
            'callback' =>
                array(
                    'class' => 'Engine_Package_Installer_Module',
                ),
            'actions' =>
                array(
                    0 => 'install',
                    1 => 'upgrade',
                    2 => 'refresh',
                    3 => 'enable',
                    4 => 'disable',
                ),
            'directories' =>
                array(
                    0 => 'application/modules/Reserva',
                ),
            'files' =>
                array(
                    0 => 'application/languages/en/reserva.csv',
                ),
        ),
    'routes' => array
    (
        'reserva_index_browse' => array
        (
            'route' => 'reserva/*',
            'defaults' => array
            (
                'module' => 'reserva',
                'controller' => 'index',
                'action' => 'browse'
            ),
        ),
        'reserva_index_calendariototal' => array
        (
            'route' => 'reserva/calendariototal/*',
            'defaults' => array
            (
                'module' => 'reserva',
                'controller' => 'index',
                'action' => 'calendariototal',
            ),
        ),
        'reserva_index_create' => array
        (
            'route' => 'reserva/create/*',
            'defaults' => array
            (
                'module' => 'reserva',
                'controller' => 'index',
                'action' => 'create',
            ),
        ),
        'reserva_index_viewsaladescription' => array
        (
            'route' => 'reserva/saladescription/:id_sala',
            'defaults' => array
            (
                'module' => 'reserva',
                'controller' => 'parametro',
                'action' => 'viewdededescription',
            ),
        ),
        'reserva_parametro_browse' => array
        (
            'route' => 'reserva/parametro/*',
            'defaults' => array
            (
                'module' => 'reserva',
                'controller' => 'parametro',
                'action' => 'browse',
            ),
        ),
        'reserva_parametro_create' => array
        (
            'route' => 'reserva/parametro/create/*',
            'defaults' => array
            (
                'module' => 'reserva',
                'controller' => 'parametro',
                'action' => 'create',
            ),
        ),
        'reserva_administrador_index' => array
        (
            'route' => 'reserva/admin/*',
            'defaults' => array
            (
                'module' => 'reserva',
                'controller' => 'administrador',
                'action' => 'index',
            ),
        ),
        'reserva_administrador_mreservas' => array
        (
            'route' => 'reserva/misreservas/*',
            'defaults' => array
            (
                'module' => 'reserva',
                'controller' => 'administrador',
                'action' => 'mreservas',
            ),
        ),
        'reserva_index_resumen' => array
        (
            'route' => 'reserva/resumen/:reserva_id/',
            'defaults' => array
            (
                'module' => 'reserva',
                'controller' => 'index',
                'action' => 'resumen',
            ),
        ),
        'reserva_administrador_usuarios' => array
        (
            'route' => 'reserva/usuarios/*',
            'defaults' => array
            (
                'module' => 'reserva',
                'controller' => 'administrador',
                'action' => 'usuarios',
            ),
        ),
        'reserva_administrador_permisosrecursos' => array
        (
            'route' => 'reserva/admin/permisosrecursos/*',
            'defaults' => array
            (
                'module' => 'reserva',
                'controller' => 'administrador',
                'action' => 'permisosrecursos',
            ),
        ),
    ),
    'items' => array('reserva_recursos', 'sede'),
); ?>