<?php
	class Reserva_Widget_ReservaSubmenuController extends Engine_Content_Widget_Abstract
	{		
		public function indexAction(){
			$api = Engine_Api::_()->reserva();
			/*$api = Engine_Api::_()->turno();
			if(!$api->permisos())
				return $this->setNoRender();*/

			/*prx(Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName());*/

            $this->view->class_item1 = $this->view->class_item2 = $this->view->class_item3 = $this->view->class_item4 = '';

			switch(Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName()) {
                case 'reserva_index_browse':
                    $this->view->class_item1 = 'active';
                    break;
                case 'reserva_parametro_create':
                    $this->view->class_item2 = 'active';
                    break;
                case 'reserva_administrador_index':
                    $this->view->class_item3 = 'active';
                    break;
                case 'reserva_administrador_usuarios':
                    $this->view->class_item4 = 'active';
                    break;
            }

			$this->view->permisos = $api->permisos();

		}
	}
?>