<div class="menu-tabs-generic no-left-margin" id="num-tabs4">
    <ul>
        <li class="<?php echo $this->class_item1 ? $this->class_item1 : ''; ?>"><a href="reserva/">Calendario</a></li>

        <?php if($this->permisos){ ?>
            <li class="<?php echo $this->class_item2 ? $this->class_item2 : ''; ?>"><a href="reserva/parametro/create/">Salas y Recursos</a></li>
        <?php } ?>

        <?php /*if($this->permisos){ */?>
            <li class="<?php echo $this->class_item3 ? $this->class_item3 : ''; ?>"><a href="reserva/admin/">Gestionar Reservas</a></li>
        <?php /*} */?>

        <?php if($this->permisos){ ?>
            <li class="<?php echo $this->class_item4 ? $this->class_item4 : ''; ?>"><a href="reserva/usuarios/">Moderadores</a></li>
        <?php } ?>

    </ul>
</div>
