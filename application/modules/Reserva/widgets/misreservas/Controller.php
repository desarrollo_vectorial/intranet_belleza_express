<?php
class Reserva_Widget_MisreservasController extends Engine_Content_Widget_Abstract
{
    public function indexAction(){
        $viewer = Engine_Api::_()->user()->getViewer();	//Obtener los datos del usuario en sesion
        $api = Engine_Api::_()->reserva(); //Obtener la api de reserva

        /*Verificacion Permisos*/
        if(!$api->permisosAcceso())
            $this ->_redirect('members/home/');
        /*Fin Verificacion Permisos*/


        $datos = array();
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $page = $request -> getParam('page', 1); //Obtener la página actual, si no hay, asinar page = 1 por defecto

        $paramsE = array();
        $paramsE = array("user_id" => $viewer->user_id); //$viewer->user_id tiene el ID del usuario en sesion
        $paramsE['status'] = 1; /*Variable para cargar enviar como parametro y mostrar solo las reservas con estado en reserva*/


        /*prx($this->view->reservas = $api->obtenerReservas());*/


        /*Ejecutar cancelar reserva*/
        if($request->isPost()){
            $reserva_id = $request->getPost('reserva_id');

            Engine_Api::_()->getDbtable('reservas', 'reserva')->cancelarReserva($reserva_id, $viewer->user_id);
        }


        //Obtener la tabla RESERVAS del modulo RESERVA
        $this->view->paginator = Engine_Api::_()->getDbtable('reservas', 'reserva')->getReservasPaginator($paramsE,$this->_getParam('buscador'));
        $this->view->paginator->setItemCountPerPage(3);
        $this->view->paginator->setCurrentPageNumber($page);
        $this->view->admin = false;
        $this->view->api = $api;
    }
}