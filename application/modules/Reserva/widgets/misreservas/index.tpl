<div class="container--my-reservations">
    <h2>Mis reservas</h2>
    <?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
    <ul>
        <?php foreach($this->paginator as $reserva): ?>
            <?php
                $hora_inicio = new DateTime($reserva->start);
                $hora_final = new DateTime($reserva->end);
            ?>
            <?php //prx($reserva->toArray()); ?>

                <li>
                    <h4><?php echo $reserva->title_anex; ?></h4><!--Título de la reserva-->

                    <div class="content--reservas">
                        <div class="columns is-multiline">
                            <div class="column">
                                <div><strong>Sala:</strong> <?php echo $reserva->recurso_fisico; ?></div><!--Fecha de la reserva-->
                            </div>
                        </div>

                        <div class="columns is-multiline">
                            <div class="column">
                                <div><strong>Fecha de reserva:</strong> <?php echo $reserva->fecha_reserva; ?></div><!--Fecha de la reserva-->
                            </div>
                            <div class="column">
                                <div><strong>Responsable:</strong> <?php echo $reserva['responsable_reserva']; ?></div><!--Responsable de la reserva-->
                            </div>
                        </div>

                        <div class="columns is-multiline">
                            <div class="column">
                                <div>
                                    <strong>Hora de la reserva:</strong>
                                    <?php echo $hora_inicio->format('g:i A'); ?> a <?php echo $hora_final->format('g:i A'); ?>
                                </div>
                            </div>
                            <div class="column">
                                <div>
                                    <strong>Asistentes:</strong> <?php echo $reserva->num_personas; ?>
                                </div>
                            </div>
                        </div>


                       <!-- <div class="columns is-multiline">
                            <div class="column">
                                <div>
                                    <strong>Obervación:</strong>
                                    <p><?php /*echo $reserva->observacion; */?></p>
                                </div>
                            </div>
                        </div>-->

                        <div class="columns is-multiline">
                            <div class="column">
                                <div class="btn--editar">
                                    <?php
                                    $id_reserva = $reserva->id;
                                    $fecha_reserva = $reserva->fecha_reserva;
                                    echo '<a href="reserva/create/fecha/'.$fecha_reserva.'/reserva_id/'.$id_reserva.'">Editar</a>';
                                    ?>
                                </div>
                            </div>
                            <div class="column">
                                <form method="post" name="admin" id="admin">
                                    <input type="hidden" name="reserva_id" value="<?php echo $reserva->id; ?>">
                                    <div class="btn--cancelar">
                                        <button name="option" value="3" onclick="accion=3;" type="submit">Cancelar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!--content--reservas-->
                </li>
        <?php endforeach; ?>
    </ul>

    <?php else:?>
        <div class="tip">
          <span>
            <?php echo $this->translate('_reservas_widget-mis-reservas');?>
          </span>
        </div>
    <?php endif; ?>
</div>

<div class="generic-paginator">
    <?php echo $this->paginationControl($this->paginator, null, null, array(
        'pageAsQuery' => true,
        'query' => 'p',
    )); ?>
</div>


<script lang="javascript">
    var accion=0;
    var estado = new Array("Pendiente", "Aprobar", "Rechazar","Cancelar");
    jQuery( "#admin" ).submit(function( event ) {

        if(confirm("Confirma "+estado[accion]+" la reserva seleccionada")){
            return true
        }else{
            event.preventDefault();
        }
    });
</script>