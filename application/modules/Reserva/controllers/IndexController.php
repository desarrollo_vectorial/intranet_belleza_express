<?php

class Reserva_IndexController extends Core_Controller_Action_Standard
{
    public function browseAction()
    {
        $this->_helper->content->setEnabled();
        $api = Engine_Api::_()->reserva();

        //$reservas = $api->obtenerReservas();
        $namespace = new Zend_Session_Namespace('sedeReserva');
        /*unset($namespace->option_id);*/
        /*Verificacion Permisos*/
        if (!$api->permisosAcceso()) {
            $this->_redirect('members/home/');
        }

        $datos = $this->_getParam('buscador');
        if (!empty($datos['option_id'])) {
            $namespace->option_id = $datos['option_id'];
        }

        if ($namespace->option_id) {
            $this->view->recursos = $api->obtenerRecursos(true, $namespace->option_id, true);
        }

        $this->view->option_id = $namespace->option_id;
        $sedes = Engine_Api::_()->getDbtable('sedes', 'reserva');
        $this->view->sedes = $sedes->obtenerSedes();
        $this->view->namespace = $namespace;
    }

    public function calendariototalAction()
    {
        $api = Engine_Api::_()->reserva();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $fechas = array();
        $option_id = $this->_getParam('option_id');
        $sala_id = $this->_getParam('sala_id');
        $start_view = $this->_getParam('start');
        $end_view = $this->_getParam('end');

        if (!$api->permisosAcceso()) {
            $this->_redirect('members/home/');
        }

        $reservas = $api->obtenerReservasCalendar('1', null, $option_id, $sala_id, $start_view, $end_view);
        echo json_encode($reservas);
    }

    public function createAction(){
        $this->_helper->content->setEnabled();
        $viewer         = Engine_Api::_()->user()->getViewer();
        $fecha          = $this->_getParam('fecha');
        $fechaFormato   = split('T', $fecha);
        $salaId         = $this->_getParam('salaId');
        $reserva_id     = $this->_getParam('reserva_id');
        $namespace      = new Zend_Session_Namespace('sedeReserva');
        $api            = Engine_Api::_()->reserva();
        $sedes          = Engine_Api::_()->getDbtable('sedes', 'reserva');
        $this->view->confirm     = $this->_getParam('data');
        $this->view->hora_inicio = $this->_getParam('inicio');
        $this->view->hora_fin    = $this->_getParam('fin');
        $dia_reserva             = split('-', $fechaFormato[0]);
        $this->view->dia_mes     = $dia_reserva;

        

        /*Verificacion Permisos*/
        if (!$api->permisosAcceso()) {
            $this->_redirect('members/home/');
        }
        /*Fin Verificacion Permisos*/

        $this->view->form = $form = new Reserva_Form_CreateReserva();
        $form->setAction($this->view->url());

        /**/
        if ($reserva_id) {
            $reservaTable = Engine_Api::_()->getDbtable('reservas', 'reserva');
            $reserva      = $reservaTable->find($reserva_id)->current();
            $recursos     = $api->obtenerRecursos(true, $reserva->option_id, false);
        } else {
            $recursos     = $api->obtenerRecursos(true, $namespace->option_id, false);
        }
        $option_id = '';

        if ($reserva_id) {
            $option_id = $reserva->option_id;
        } elseif ($namespace->option_id) {
            $option_id = $namespace->option_id;
        }else{
            return $this->_redirect('/reserva');
        }

        if ($option_id) {
            foreach ($sedes->obtenerSedes($option_id) as $key => $value) {
                if ($key ) {
                    $this->view->sede = $value;
                }
            }
        }
        $salas = array('' => 'Ninguno');

        for ($i = 0; $i < count($recursos[1]); ++$i) {
            $salas[$recursos[1][$i]['recurso_id']] = $recursos[1][$i]['title'];
        }
        /*Elimino recursos Físicos*/
        unset($recursos[1]);
        $audiovisual = array();

        foreach ($recursos as $k => $v) {
            for ($i = 0;$i < count($v);++$i) {
                $audiovisual[$v[$i]['recurso_id']] = $v[$i]['title'];
            }
        }

        $form->recursos->setMultiOptions($audiovisual);
        $form->recurso_id->setMultiOptions($salas);
        $form->recurso_id->setValue(array($salaId));
        $form->fecha_reserva->setValue($fechaFormato[0]);
        $form->hora_inicio->setValue($fechaFormato[1]);
        $form->hora_fin->setValue('19:00:00');        

        if ($reserva_id) {
            $this->view->itemsReserva = $items_reserva = $api->obtenerItems($reserva_id, 2);
            $items_selected = array();
            

            foreach ($items_reserva as $key => $item_reserva) {
                $items_selected[] = $item_reserva['recurso_id'];
            }
            $this->view->reserva = $reserva;
            $this->view->form->populate($reserva->toArray());
            $this->view->form->recursos->setValue($items_selected);

            if (($reserva['sabado'] == 1) && ($reserva['domingo'] == 1)) {
                $this->view->form->incluir_fds->setChecked(true);
            }
            // $this->view->form->recurso_id->setValue($reserva->recurso_id);
            $this->view->form->setTitle('Editar reserva');
            $array_dias = array();

            if ($reserva['lunes']) {
                $array_dias[] = '0';
            }
            if ($reserva['martes']) {
                $array_dias[] = '1';
            }
            if ($reserva['miercoles']) {
                $array_dias[] = '2';
            }
            if ($reserva['jueves']) {
                $array_dias[] = '3';
            }
            if ($reserva['viernes']) {
                $array_dias[] = '4';
            }
            if ($reserva['sabado']) {
                $array_dias[] = '5';
            }
            if ($reserva['domingo']) {
                $array_dias[] = '6';
            }
            $this->view->form->dia->setValue($array_dias);
        }

        if ($this->getRequest()->isPost()) {
            $datos = $this->getRequest()->getPost();
            

            if (intval($datos['recurrente'])) {
                $form->fecha_final->setRequired(true);
                $form->tipo_periodicidad->setRequired(true);

                if (intval($datos['tipo_periodicidad']) === 2) {
                    $form->dia->setRequired(true);
                }
            }
            if ($form->isValid($this->getRequest()->getPost())) {

                /* Validación para que la fecha de fin sea mayor que la inicial */
                $timeInicio = strtotime($datos['fecha_reserva'].' '.$datos['hora_inicio']);
                $timeFin    = strtotime($datos['fecha_reserva'].' '.$datos['hora_fin']);

                if ($timeInicio >= $timeFin) {
                    $form->hora_fin->setErrors(array('Recuerde que la hora de finalización debe ser superior a la hora de inicio del evento.'));

                    return false;
                }

                /* Validación no exista otra reserva de esa misma sala en la hora y fecha */
                if ($datos['recurso_id'] != '') {
                    $db         = Zend_Db_Table_Abstract::getDefaultAdapter();
                    $sql_query  = $db->query("SELECT engine4_reserva_reservas.* FROM engine4_reserva_reservas WHERE reserva_id != '".$datos['reserva_id']."' AND fecha_reserva = '".$datos['fecha_reserva']."' AND recurso_id = '".$datos['recurso_id']."' AND hora_inicio >= '".$datos['hora_inicio']."' AND hora_fin <= '".$datos['hora_fin']."' AND (estado != 2 AND estado != 3) ");
                    $rows       = $sql_query->fetchAll();

                    if (count($rows) > 0) {
                        $form->recurso_id->setErrors(array('La fecha o la hora en la que intenta hacer la reserva ya no se encuentra disponible. Por favor seleccione otras opciones.'));
                        return false;
                    }
                }

                
                $datos = $this->getRequest()->getPost();

                if ((empty($datos['recurso_id'])) && (empty($datos['recursos']))) {
                    $this->view->empty_all = 'empty';
                    
                } else {


                    
                    if (empty($datos['reserva_id'])) {
                        $db         = Zend_Db_Table_Abstract::getDefaultAdapter();
                        $sql_query  = $db->query("SELECT engine4_reserva_reservas.* FROM engine4_reserva_reservas WHERE fecha_reserva = '".$datos['fecha_reserva']."' AND recurso_id = '".$datos['recurso_id']."' AND estado IN('0','1') AND ( (hora_inicio >= '".$datos['hora_inicio']."' AND hora_inicio < '".$datos['hora_fin']."') OR (hora_fin > '".$datos['hora_inicio']."' AND hora_fin <= '".$datos['hora_fin']."') OR (hora_inicio < '".$datos['hora_inicio']."' AND hora_fin > '".$datos['hora_fin']."') OR (hora_inicio = '".$datos['hora_inicio']."' AND hora_fin = '".$datos['hora_fin']."') ) ;");
                        $rows       = $sql_query->fetchAll();
                        $rowCount   = count($rows);

                        

                        if (empty($datos['recurso_id'])) {
                            $rowCount = 0;
                        }

                        if ($rowCount > 0) {
                            foreach ($rows as $row) {
                                $this->view->confirm     = 'busy';
                                $this->view->fecha       = $row['fecha_reserva'];
                                $this->view->hora_inicio = $row['hora_inicio'];
                                $this->view->hora_fin    = $row['hora_fin'];
                            }
                        } else {
                            
                            $datos['user_id']   = $viewer->user_id;
                            $datos['estado']    = '1';
                            $datos['modified']  = date('Y-m-d H:i:s');
                            $datos['created']   = date('Y-m-d H:i:s');
                            $datos['option_id'] = $namespace->option_id;



                            if (intval($datos['recurrente'])) {
                                $repetidos = array();
                                $losDias   = implode(',', $datos['dia']);

                                if(count($losDias) > 0 && ! isset($losDias)){ $where = " AND WEEKDAY(fecha_reserva) IN (".$losDias.")"; }
                                $db        = Zend_Db_Table_Abstract::getDefaultAdapter();        
                                

                                $sqlQuery = " SELECT engine4_reserva_reservas.*
                                                FROM engine4_reserva_reservas
                                               WHERE fecha_reserva >= '".$datos['fecha_reserva']."'
                                                     AND fecha_reserva <= '".$datos['fecha_final']."'
                                                     AND recurso_id = '".$datos['recurso_id']."'
                                                     AND hora_inicio < '".$datos['hora_fin']."'
                                                     AND hora_fin > '".$datos['hora_inicio']."'
                                                     AND estado = 1 ".$where;
                                $sql_query = $db->query($sqlQuery);
                                $rows = $sql_query->fetchAll();
                                

                                if (count($rows) > 0) {
                                    $form->fecha_reserva->setErrors(array('La fecha y la hora en la que esta intentando hacer la reserva periodíca no se encuentra disponible, por favor seleccione otras opciones'));

                                    return false;
                                }
                                if (intval($datos['tipo_periodicidad']) === 1) {
                                    if (intval($datos['incluir_fds'])) {
                                        $datos['sabado'] = 1;
                                        $datos['domingo'] = 1;
                                    }
                                } elseif (intval($datos['tipo_periodicidad']) === 2) {
                                    foreach ($datos['dia'] as $key => $value) {
                                        if (intval($value) === 0) {
                                            $datos['lunes'] = 1;
                                        }
                                        if (intval($value) === 1) {
                                            $datos['martes'] = 1;
                                        }
                                        if (intval($value) === 2) {
                                            $datos['miercoles'] = 1;
                                        }
                                        if (intval($value) === 3) {
                                            $datos['jueves'] = 1;
                                        }
                                        if (intval($value) === 4) {
                                            $datos['viernes'] = 1;
                                        }
                                        if (intval($value) === 5) {
                                            $datos['sabado'] = 1;
                                        }
                                        if (intval($value) === 6) {
                                            $datos['domingo'] = 1;
                                        }
                                    }
                                } elseif (intval($datos['tipo_periodicidad']) === 3) {
                                    if (intval($datos['tipo_mensual']) === 1) {
                                        $datos['cada_mensual'] = $datos['cada_mensual_1'];
                                    } elseif (intval($datos['tipo_mensual']) === 2) {
                                        $datos['cada_mensual'] = $datos['cada_mensual_2'];
                                    }
                                    unset($datos['cada_mensual_1']);
                                    unset($datos['cada_mensual_2']);
                                }
                            }
                            

                            $id = Engine_Api::_()->getDbtable('reservas', 'reserva')->guardarReserva($datos);
                            if (intval($datos['recurrente'])) {
                                $this->periodicidad($id, $datos);
                            }
                            $this->guardaRecursos($id, $datos);
                            $this->generarICS($api->obtenerReservas(1, $id), $datos['tipo_mensual']);
                            sleep(1);
                            $this->_redirect('reserva/create/data/confirm');
                        }

                    } else {
                        $reserva_tb = Engine_Api::_()->getDbtable('reservas', 'reserva');
                        $reserva    = $reserva_tb->find($reserva_id)->current();

                        $db         = Zend_Db_Table_Abstract::getDefaultAdapter();
                        $sql_query  = $db->query("SELECT engine4_reserva_reservas.* 
                                                   FROM engine4_reserva_reservas WHERE reserva_id <> '".$datos['reserva_id']."' AND fecha_reserva = '".$datos['fecha_reserva']."' AND recurso_id = '".$datos['recurso_id']."' AND estado IN('0','1') AND ( (hora_inicio >= '".$datos['hora_inicio']."' AND hora_inicio < '".$datos['hora_fin']."') OR (hora_fin > '".$datos['hora_inicio']."' AND hora_fin <= '".$datos['hora_fin']."') OR (hora_inicio < '".$datos['hora_inicio']."' AND hora_fin > '".$datos['hora_fin']."') OR (hora_inicio = '".$datos['hora_inicio']."' AND hora_fin = '".$datos['hora_fin']."') ) ;");
                        $rows = $sql_query->fetchAll();

                        $rowCount = count($rows);

                        if (empty($datos['recurso_id'])) {
                            $rowCount = 0;
                        }

                        if ($rowCount > 0) {
                            foreach ($rows as $row) {
                                // echo $row["reserva_id"]." - ".$row["title"]."  ".$row["fecha_reserva"];
                                $this->view->confirm     = 'busy';
                                $this->view->fecha       = $row['fecha_reserva'];
                                $this->view->hora_inicio = $row['hora_inicio'];
                                $this->view->hora_fin    = $row['hora_fin'];
                            }
                        } else {
                            $datos['user_id']   = $viewer->user_id;
                            // $datos['estado'] = '1';
                            $datos['modified']  = date('Y-m-d H:i:s');
                            $datos['option_id'] = $reserva->option_id;

                            if (intval($datos['recurrente'])) {
                                if (intval($datos['tipo_periodicidad']) === 1) {
                                    if (intval($datos['incluir_fds'])) {
                                        $datos['sabado']  = 1;
                                        $datos['domingo'] = 1;
                                    } else {
                                        $datos['sabado']  = null;
                                        $datos['domingo'] = null;
                                        unset($datos['dia']);
                                    }
                                } elseif (intval($datos['tipo_periodicidad']) === 2) {
                                    foreach ($datos['dia'] as $key => $value) {
                                        if (intval($value) === 0) {
                                            $datos['lunes']  = 1;
                                        }
                                        if (intval($value) === 1) {
                                            $datos['martes'] = 1;
                                        }
                                        if (intval($value) === 2) {
                                            $datos['miercoles'] = 1;
                                        }
                                        if (intval($value) === 3) {
                                            $datos['jueves'] = 1;
                                        }
                                        if (intval($value) === 4) {
                                            $datos['viernes'] = 1;
                                        }
                                        if (intval($value) === 5) {
                                            $datos['sabado'] = 1;
                                        }
                                        if (intval($value) === 6) {
                                            $datos['domingo'] = 1;
                                        }
                                    }
                                } elseif (intval($datos['tipo_periodicidad']) === 3) {
                                    if (intval($datos['tipo_mensual']) === 1) {
                                        $datos['cada_mensual'] = $datos['cada_mensual_1'];
                                    } elseif (intval($datos['tipo_mensual']) === 2) {
                                        $datos['cada_mensual'] = $datos['cada_mensual_2'];
                                    }

                                    unset($datos['cada_mensual_1']);
                                    unset($datos['cada_mensual_2']);
                                }
                            }

                            $reserva->setFromArray($datos);
                            $reserva->save();

                            if (intval($datos['recurrente'])) {
                                // $current_db = Zend_Db_Table_Abstract::getDefaultAdapter();
                                $sql_children = $db->query("SELECT engine4_reserva_reservas.reserva_id FROM engine4_reserva_reservas WHERE parent_id = '".$reserva->reserva_id."';");

                                $rows_childs = $sql_children->fetchAll();

                                $rowChildsCount = count($rows_childs);
                                if ($rowChildsCount > 0) {
                                    foreach ($rows_childs as $child) {
                                        $db->query("DELETE FROM engine4_reserva_items WHERE reserva_id = '".$child->reserva_id."';");
                                        $db->commit();
                                    }
                                }

                                $db->query("DELETE FROM engine4_reserva_reservas WHERE parent_id = '".$reserva->reserva_id."';");
                                $db->commit();

                                $this->periodicidad($reserva->reserva_id, $datos);
                            }

                            $this->guardaRecursos($reserva->reserva_id, $datos);
                            $this->generarICS($api->obtenerReservas(1, $reserva->reserva_id), $datos['tipo_mensual']);
                            sleep(1);
                            $this->_redirect('reserva/create/data/confirm');
                        }

                    }
                }
            }
        }

        return true;
    }

    private function outputCSV($data) {
        $outputBuffer = fopen("php://output", 'w');
        foreach($data as $val) {
            foreach ($val as $key => $value) {
              fputcsv($outputBuffer, $value);
            }
        }
        fclose($outputBuffer);
    }

    private function periodicidad($reserva_id = null, $datos = array())
    {
        $api = Engine_Api::_()->reserva();
        $start = $datos['fecha_reserva'];
        $end = $datos['fecha_final'];
        $datos['estado'] = '1';
        $datos['parent_id'] = $reserva_id;

        $datos['recurrente'] = '0';

        $guardar_reserva = true;

        if (intval($datos['tipo_periodicidad']) === 1) {
            $num_dias = intval($datos['cada_dia']);
            for ($i = $num_dias;$i <= ($api->obtenerDiasEntreFechas($start, $end));$i += $num_dias) {
                unset($datos['dia']);
                unset($datos['cada_dia']);
                unset($datos['cada_semanal']);
                unset($datos['cada_mensual']);
                unset($datos['cada_anual']);

                unset($datos['tipo_mensual']);
                unset($datos['posicion_dia']);
                unset($datos['dia_mensual']);

                $datos['fecha_reserva'] = date('Y-m-d', strtotime($start.'+'.$i.' days'));
                $dia_semana = date('N', strtotime($start.'+'.$i.' days'));

                if (($dia_semana == 6) || ($dia_semana == 7)) {
                    $guardar_reserva = false;
                } else {
                    $guardar_reserva = true;
                }

                if (intval($datos['incluir_fds'])) {
                    $guardar_reserva = true;
                }

                if ($guardar_reserva) {
                    $new_data = $datos;
                    unset($new_data['tipo_periodicidad']);
                    unset($new_data['fecha_final']);
                    unset($new_data['lunes']);
                    unset($new_data['martes']);
                    unset($new_data['miercoles']);
                    unset($new_data['jueves']);
                    unset($new_data['viernes']);
                    unset($new_data['sabado']);
                    unset($new_data['domingo']);
                    $this->guardaRecursos(Engine_Api::_()->getDbtable('reservas', 'reserva')->guardarReserva($new_data), $new_data);
                }
            }
        }
        if (intval($datos['tipo_periodicidad']) === 2) {
            $num_semanas = intval($datos['cada_semanal']);
            for ($i = 0;$i <= ($api->obtenerSemanasEntreFechas($start, $end));$i += $num_semanas) {
                unset($datos['cada_dia']);
                unset($datos['cada_semanal']);
                unset($datos['cada_mensual']);
                unset($datos['cada_anual']);

                unset($datos['tipo_mensual']);
                unset($datos['posicion_dia']);
                unset($datos['dia_mensual']);

                // $fechaNueva = date('Y-m-d',strtotime($start . "+".$i." weeks"));
                foreach ($datos['dia'] as $key => $value) {
                    $fecha_reserva_generada = date('Y-m-d', strtotime($start.'+'.$i.' weeks '.$api->days[$value].' this week'));
                    if (($fecha_reserva_generada > $start) && ($fecha_reserva_generada <= $end)) {
                        $datos['fecha_reserva'] = $fecha_reserva_generada;

                        $new_data = $datos;
                        unset($new_data['tipo_periodicidad']);
                        unset($new_data['fecha_final']);
                        unset($new_data['lunes']);
                        unset($new_data['martes']);
                        unset($new_data['miercoles']);
                        unset($new_data['jueves']);
                        unset($new_data['viernes']);
                        unset($new_data['sabado']);
                        unset($new_data['domingo']);
                        $this->guardaRecursos(Engine_Api::_()->getDbtable('reservas', 'reserva')->guardarReserva($new_data), $new_data);
                    }
                }
            }
        }
        if (intval($datos['tipo_periodicidad']) === 3) {
            $num_meses = intval($datos['cada_mensual']);
            $primer_mes = true;
            $cant_meses = $api->obtenerMesesEntreFechas($start, $end);

            for ($i = $num_meses; $i <= $cant_meses; $i += $num_meses) {
                unset($datos['dia']);
                unset($datos['cada_dia']);
                unset($datos['cada_semanal']);
                unset($datos['cada_mensual']);
                unset($datos['cada_anual']);

                if (intval($datos['tipo_mensual']) === 1) {
                    $datos['fecha_reserva'] = date('Y-m-d', strtotime($start.'+'.$i.' month'));
                } elseif (intval($datos['tipo_mensual']) === 2) {
                    $next_mes = date('m', strtotime($start.'+'.$i.' month'));
                    $next_year = date('Y', strtotime($start.'+'.$i.' month'));

                    // echo "next_mes: ".$next_mes." - next_year: ".$next_year."<br />";

                    $num_semana = $datos['posicion_dia'];
                    $dia_mes = $datos['dia_mensual'];
                    // $num_dia_semana_start = date('N',strtotime($start));
                    // $dia_start = date('j',strtotime($start));
                    $ultimoDiaMes = $api->getUltimoDiaMes($next_year, $next_mes);

                    // $actualSemana=date("W",strtotime('2015-12-31'));

                    /*echo "num_semana: ".$num_semana."<br />";
                    echo "dia_mes: ".$dia_mes."<br />";
                    echo "ultimoDiaMes: ".$ultimoDiaMes."<br />";*/
                    $primer_semana = date('W', strtotime($next_year.'-'.$next_mes.'-01'));
                    $count_dia_semana = 0;

                    for ($j = 1; $j <= $ultimoDiaMes; ++$j) {
                        $fecha_ciclo = $next_year.'-'.$next_mes.'-'.$j;
                        $semana = date('W', strtotime($fecha_ciclo));

                        $semana_mes = $semana - ($primer_semana - 1);
                        $dia_semana_actual = date('N', strtotime($fecha_ciclo));

                        // echo "semana: ".$semana_mes."<br />";

                        if ($dia_semana_actual == $dia_mes) {
                            ++$count_dia_semana;
                            if ($count_dia_semana == $num_semana) {
                                /*echo "fecha_ciclo: ".$fecha_ciclo."<br />";
                                echo "semana_mes: ".$semana_mes."<br />";
                                echo "dia_semana_actual: ".$dia_semana_actual."<br />";*/
                                $datos['fecha_reserva'] = date('Y-m-d', strtotime($fecha_ciclo));
                            }
                        }
                    }
                }

                $new_data = $datos;
                unset($new_data['tipo_periodicidad']);

                unset($new_data['tipo_mensual']);
                unset($new_data['posicion_dia']);
                unset($new_data['dia_mensual']);

                unset($new_data['fecha_final']);
                unset($new_data['lunes']);
                unset($new_data['martes']);
                unset($new_data['miercoles']);
                unset($new_data['jueves']);
                unset($new_data['viernes']);
                unset($new_data['sabado']);
                unset($new_data['domingo']);
                if ($new_data['fecha_reserva'] < $end) {
                    $this->guardaRecursos(Engine_Api::_()->getDbtable('reservas', 'reserva')->guardarReserva($new_data), $new_data);
                }
            }
        }
        if (intval($datos['tipo_periodicidad']) === 4) {
            $num_anios = intval($datos['cada_anual']);
            for ($i = $num_anios;$i <= ($api->obtenerAniosEntreFechas($start, $end));$i += $num_anios) {
                unset($datos['dia']);
                unset($datos['cada_dia']);
                unset($datos['cada_semanal']);
                unset($datos['cada_mensual']);
                unset($datos['cada_anual']);
                $datos['fecha_reserva'] = date('Y-m-d', strtotime($start.'+'.intval($i).' years'));

                $new_data = $datos;
                unset($new_data['tipo_periodicidad']);
                unset($new_data['fecha_final']);
                unset($new_data['lunes']);
                unset($new_data['martes']);
                unset($new_data['miercoles']);
                unset($new_data['jueves']);
                unset($new_data['viernes']);
                unset($new_data['sabado']);
                unset($new_data['domingo']);
                $this->guardaRecursos(Engine_Api::_()->getDbtable('reservas', 'reserva')->guardarReserva($new_data), $new_data);
            }
        }
    }

    private function guardaRecursos($reserva_id, $datos)
    {
        /*echo "<pre>".print_r($datos,true)."</pre>";*/
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->query('DELETE FROM engine4_reserva_items WHERE reserva_id = '.$reserva_id.';');
        $db->commit();

        if ($datos['recursos']) {
            foreach ($datos['recursos'] as $key => $value) {
                if ($value) {
                    $db->query('INSERT INTO  engine4_reserva_items (reserva_id,recurso_id,tipo,cantidad) VALUES('.$reserva_id.','.$value.',2,0);');
                }
            }
        }

        $db->commit();

        return true;
    }

    public function generarICS($data = array(), $tipo_mensual = null){
        $api = Engine_Api::_()->reserva();

        $old = umask(0);
        mkdir(APPLICATION_PATH.'/public/reserva', 0777);
        umask($old);
        $fileTmp = rand(0, 1000).'.ics';
        $f = fopen(APPLICATION_PATH.'/public/reserva/'.$fileTmp, 'w');
        /*
        echo "DTEND:20091208T040000Z\n";
        echo "DTSTAMP:20091109T093305Z\n";
        echo "DTSTART:20091208T003000Z\n";

        */
        $msg = "BEGIN:VCALENDAR\n";
        $msg .= "PRODID:-//Google Inc//Google Calendar 70.9054//EN\n";

        $msg .= "PRODID:-//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN\n";
        $msg .= "VERSION:2.0\n";
        $msg .= "METHOD:PUBLISH\n";
        $msg .= "X-MS-OLK-FORCEINSPECTOROPEN:TRUE\n";

        $msg .= "VERSION:2.0\n";
        $msg .= "METHOD:REQUEST\n";
        $msg .= "BEGIN:VEVENT\n";

        $msg .= 'DTSTART:'.str_replace('-', '', str_replace(':', '', $data[0]['start']))."ET\n";
        $msg .= 'DTEND:'.str_replace('-', '', str_replace(':', '', $data[0]['end']))."ET\n";

        if (intval($data[0]['tipo_periodicidad']) === 1) {
            $msg .= 'RRULE:FREQ=DAILY;INTERVAL='.$data[0]['cada_dia'].';UNTIL='.str_replace('-', '', str_replace(':', '', $data[0]['fecha_final'])).'T133000ET'."\n";
        }
        if (intval($data[0]['tipo_periodicidad']) === 2) {
            /*SU,MO,TU,WE,TH,FR,SA*/
            $dias = '';
            if ($data[0]['lunes']) {
                $dias .= 'MO,';
            }
            if ($data[0]['martes']) {
                $dias .= 'TU,';
            }
            if ($data[0]['miercoles']) {
                $dias .= 'WE,';
            }
            if ($data[0]['jueves']) {
                $dias .= 'TH,';
            }
            if ($data[0]['viernes']) {
                $dias .= 'FR,';
            }
            if ($data[0]['sabado']) {
                $dias .= 'SA,';
            }
            if ($data[0]['domingo']) {
                $dias .= 'SU';
            }
            $msg .= 'RRULE:FREQ=WEEKLY;UNTIL='.str_replace('-', '', str_replace(':', '', $data[0]['fecha_final'])).'T143000ET;INTERVAL='.$data[0]['cada_semanal'].';BYDAY='.trim($dias, ',')."\n";
        }
        if (intval($data[0]['tipo_periodicidad']) === 3) {
            if (intval($tipo_mensual) === 1) {
                $msg .= 'RRULE:FREQ=MONTHLY;UNTIL='.str_replace('-', '', str_replace(':', '', $data[0]['fecha_final'])).'T153000ET;INTERVAL='.$data[0]['cada_mensual'].';BYMONTHDAY='.date('d', strtotime($data[0]['fecha_reserva']))."\n";
            } elseif (intval($tipo_mensual) === 2) {
                $num_semana = $data[0]['posicion_dia'];

                if ($num_semana == '5') {
                    $num_semana = '-1';
                }

                $dia = '';
                switch ($data[0]['dia_mensual']) {
                    case '1':
                        $dia = 'MO';
                        break;

                    case '2':
                        $dia = 'TU';
                        break;

                    case '3':
                        $dia = 'WE';
                        break;

                    case '4':
                        $dia = 'TH';
                        break;

                    case '5':
                        $dia = 'FR';
                        break;

                    case '6':
                        $dia = 'SA';
                        break;

                    case '7':
                        $dia = 'SU';
                        break;

                    default:
                        break;
            }

                $msg .= 'RRULE:FREQ=MONTHLY;UNTIL='.str_replace('-', '', str_replace(':', '', $data[0]['fecha_final'])).'T153000ET;INTERVAL='.$data[0]['cada_mensual'].';BYDAY='.$num_semana.''.$dia."\n";
            }
        }
        if (intval($data[0]['tipo_periodicidad']) === 4) {
            $msg .= 'RRULE:FREQ=YEARLY;UNTIL='.str_replace('-', '', str_replace(':', '', $data[0]['fecha_final'])).'T163000ET;INTERVAL='.$data[0]['cada_anual']."\n";
        }

        $msg .= 'ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP= TRUE;X-NUM-GUESTS=0:mailto:'.$data[0]['responsable_correo']."\n";
        $msg .= "X-MICROSOFT-CDO-BUSYSTATUS:BUSY\n";
        $msg .= "X-MICROSOFT-CDO-IMPORTANCE:1\n";
        $msg .= "X-MICROSOFT-DISALLOW-COUNTER:FALSE\n";
        $msg .= "X-MS-OLK-ALLOWEXTERNCHECK:TRUE\n";
        $msg .= "X-MS-OLK-AUTOFILLLOCATION:FALSE\n";
        $msg .= "X-MS-OLK-CONFTYPE:0\n";
        $msg .= 'SUMMARY: '.$data[0]['title_anex']."\n";
        $msg .= 'LOCATION:'.$data[0]['recurso_fisico']."\n";

        $msg .= "END:VEVENT\n";
        $msg .= "END:VCALENDAR\n";

        fwrite($f, $msg);
        fclose($f);

        //$mail = new Zend_Mail('utf-8');
        if (empty($data[0]['recurso_fisico'])) {
            $items_reserva = '';
            $reserva_items = $api->obtenerItems($data[0]['id']);

            foreach ($reserva_items as $key => $value) {
                for ($i = 0;$i < count($value);++$i) {
                    if ($i > 0) {
                        $items_reserva .= ', ';
                    }
                    $items_reserva .= trim($value[$i]['title']);
                }
            }

            $bodyHtml = 'Reserva de los siguientes recurso(s): '.$items_reserva.'<br />Evento: '.$data[0]['title_anex'];
        } else {
            $bodyHtml = 'Reserva de Sala: '.$data[0]['recurso_fisico'].'<br />Evento: '.$data[0]['title_anex'];
        }

        //$mail->setFrom('colombinet@colombina.com', 'Intranet');
        //$mail->addTo($data[0]['responsable_correo'], $data[0]['responsable_reserva']);
        //$mail->setSubject('Invitación');

        $ical   = $msg;
        $attach = new Zend_Mime_Part($ical);
        $attach->type        = 'text/calendar';
        $attach->disposition = Zend_Mime::DISPOSITION_INLINE;
        $attach->encoding    = Zend_Mime::ENCODING_8BIT;
        $attach->filename    = 'calendar.ics';

        //$mail->addAttachment($attach);

        $exito = Engine_Api::_()->core()->PHPMailerSendEmailAttach($data[0]['responsable_reserva'], $data[0]['responsable_correo'], $bodyHtml, "Invitación", $attach);
        return $exito;
    }

    public function enviarCorreo($reserva_id, $datos)
    {
        /*$api = Engine_Api::_()->reserva();
        $admins = $api->permisos(false);
        $viewer = Engine_Api::_()->user()->getViewer();

        $from = Engine_Api::_()->user()->getUser($viewer->user_id);
        $asunto = "Reservas - Clinica de Oftalmología de Cali";
        $msj  = "<p>Se ha creado una nueva reserva con la siguiente informaci&oacute;n:</p>";
        $msj .= "<br />";
        $msj .= "<p><strong>Nombre</strong>: ".$datos['title']."</p>";
        $msj .= "<p><strong>Fecha</strong>: ".$datos['fecha_reserva']."</p>";
        $msj .= "<p><strong>Hora</strong>: ".$datos['hora_inicio']." - ".$datos['hora_fin']."</p>";
        $msj .= "<hr />";
        $msj .= "<br />";
        $msj .= "<p><strong>Responsable</strong>: ".$from->displayname."</p>";
        $msj .= "<p><strong>Correo</strong>: ".$from->email."</p>";
        $msj .= "<p><strong>Tel&eacute;fono</strong>:  ".$datos['telefono']." ext  ".$datos['extension']."</p>";
        $msj .= "<p><strong>Observaci&oacute;n</strong>:  ".$datos['observacion']."</p>";
        $msj .= "<p>Para ver el listado de reservas: <a href=\"http://".$_SERVER['SERVER_NAME']."/".trim($_SERVER['PHP_SELF'],'/index.php')."/reserva/admin/\">Clic aqu&iacute;</a></p>";

        foreach($admins as $key => $value){
            $to = Engine_Api::_()->user()->getUser($value['user_id']);
            $api->sendConversation($asunto,$msj,$to,$from);
        }*/
        return true;
    }

    public function resumenAction()
    {
        /*$this->_helper->layout->setLayout('default-simple');*/
        $this->_helper->layout->disableLayout();
        $reserva_id = $this->_getParam('reserva_id');
        $api = Engine_Api::_()->reserva();

        /*Verificacion Permisos*/
        if (!$api->permisosAcceso()) {
        $this->_redirect('members/home/');
        }
        /*Fin Verificacion Permisos*/

        $this->view->reserva = $api->obtenerReservas(null, $reserva_id);
        $this->view->items = $api->obtenerItems($reserva_id);
    }
}
