<?php

class Reserva_AdministradorController extends Core_Controller_Action_Standard
{
  public function indexAction(){
  	$viewer = Engine_Api::_()->user()->getViewer();
    $this->_helper->content->setEnabled();
	$api = Engine_Api::_()->reserva();
	
	/*Verificacion Permisos*/
	if(!$api->permisosAcceso())
		$this ->_redirect('members/home/');
	if(!$api->permisos())
		$this ->_redirect('reserva/');
	/*Fin Verificacion Permisos*/
	
	$datos = array();
	if($this->getRequest()->isPost()){
		$request = $this->getRequest()->getPost();
		
		for($i=0;$i<count($request['reserva_opc']);$i++){
			$reservaTable = Engine_Api::_()->getDbtable('reservas', 'reserva');
			$reserva = $reservaTable->find($request['reserva_opc'][$i])->current();
			/*$this->enviarCorreo($reserva,$request);*/
			if($request['op']==='1')
				$datos['estado'] = '1';
			if($request['op']==='2')
				$datos['estado'] = '2';
			if($request['op']==='3')
				$datos['estado'] = '3';
				
			$datos['aprobador_id'] = $viewer->user_id;	
			$datos['modified']=date('Y-m-d H:i:s');
			$datos['approved']=date('Y-m-d H:i:s');

			$table = $this->_helper->api()->getDbtable('reservas', 'reserva');
			$db = $table->getAdapter();

			if(intval($reserva['recurrente'])){
				$db->query("UPDATE engine4_reserva_reservas SET estado = '".$datos['estado']."' WHERE parent_id = '".$reserva->reserva_id."';");	
				$db->commit();
			}

			$db->beginTransaction();

			try	{
  				$reserva->setFromArray($datos);
				$reserva->save();	
  			}catch( Exception $e ){
				$db->rollBack();
				throw $e;
				echo false;
			}

			$db->commit();
		}
	}

	$page=$this->_getParam('page',1);
	/*$this->view->reservas = $api->obtenerReservas();*/
	$paramsE = array();
	$paramsE['moderador_id'] = $viewer->user_id; 
	$this->view->paginator = Engine_Api::_()->getDbtable('reservas', 'reserva')->getReservasPaginator($paramsE,$this->_getParam('buscador'));
	$this->view->paginator->setItemCountPerPage(5);
	$this->view->paginator->setCurrentPageNumber($page);
	$sedes = Engine_Api::_()->getDbtable('sedes', 'reserva');
    $this->view->sedes = $sedes->obtenerSedes();
	$this->view->admin = true;
	$this->view->api = $api;
	
	return true;
  }

	public function permisosrecursosAction() {
		if($this->getRequest()->isPost()) {

			$recursos = Engine_Api::_()->getDbtable('recursos', 'reserva');
			$permiso = Engine_Api::_()->getDbtable('permisos', 'reserva');
			$lstRecursos = $recursos->getRecursos(1, array('title', 'recurso_id', 'user_id'));			
			$formPermisosRecursos = new Reserva_Form_Moderadores($lstRecursos, $permiso->usuariosModulos(false));
			
			if($formPermisosRecursos->isValid($this->getRequest()->getPost()))	{
					
				$recursos_list = $formPermisosRecursos->getValues();
				
				
				foreach($recursos_list['moderadors'] as $recurso_id => $permisos_values) {
					$recurso = Engine_Api::_()->getItem('reserva_recursos', $recurso_id);
					$recurso->setFromArray($permisos_values);
		            $recurso->save();			
				}	
			}
		}
		$this ->_redirect('reserva/usuarios/');
	}
  
  
  public function usuariosAction(){
	  	$viewer = Engine_Api::_()->user()->getViewer();
	    $this->_helper->content->setEnabled();
		$api = Engine_Api::_()->reserva();
		$permiso = Engine_Api::_()->getDbtable('permisos', 'reserva');
		$user_id = $this->_getParam('delete');
		
		
		$recursos = Engine_Api::_()->getDbtable('recursos', 'reserva');
		$lstRecursos = $recursos->getRecursos(1, array('title', 'recurso_id', 'user_id'));
		
		$this->view->form = $form = new Reserva_Form_FormUsuario();
		$form->setAction($this->view->url());
		$this->view->administradores = $permiso->usuariosModulos(false);
		$form->user_id->setMultiOptions($permiso->usuariosModulos(true));

		$this->view->formPermisosRecursos = $formPermisosRecursos = new Reserva_Form_Moderadores($lstRecursos, $permiso->usuariosModulos(false));
		$formPermisosRecursos->setAction('reserva/admin/permisosrecursos');
		

		if($user_id){
			$where = $permiso->getAdapter()->quoteInto('user_id = ?', $user_id);
			$permiso->delete($where);
			
			
			/* Retirar el usuario como administrador de las salas actuales y reemplazarlo por el administrador */
			$table = $this->_helper->api()->getDbtable('recursos', 'reserva');
			$db = $table->getAdapter();						
			$db->query("UPDATE engine4_reserva_recursos SET user_id = '1' WHERE user_id = '".$user_id."';");	
			$db->commit();
						
			$this ->_redirect('reserva/usuarios/');
		}
		
		
		if($this->getRequest()->isPost()) {
			if($form->isValid($this->getRequest()->getPost()))	{
				$table = $this->_helper->api()->getDbtable('permisos', 'reserva');
				$db = $table->getAdapter();
				$db->beginTransaction();
				try	{
						$values = $form->getValues();
						$values['created']=date('Y-m-d H:i:s');
						//echo "<pre>".print_r($values,true)."</pre>";
						$permisoTable = $table->createRow();
							
						$permisoTable->setFromArray($values);
						$permisoTable->save();	
					}
					catch( Exception $e ){
							$db->rollBack();
							throw $e;
						}
					$db->commit();
					$this ->_redirect('reserva/usuarios/');
			}
		}
	}
	
	
  public function enviarCorreo($datos,$request){
	$api    = Engine_Api::_()->reserva();
	$admins = $api->permisos(false);
	$viewer = Engine_Api::_()->user()->getViewer();
	$estado = array("1"=>"Aprobada","2"=>"Rechazada");
	    /*$asunto = "",$mensaje = "",$to=null,$from=null*/
    /*$asunto = "Notificacion del Modulo de Reservas";
    /*Para enviar Correos */
    /*$to = Engine_Api::_()->user()->getUser(1);
    $from = Engine_Api::_()->user()->getUser(274);
    $api->sendConversation($asunto,$mensaje,$to,$from);*/
	
	/*$asunto = "Reservas - Clinica de Oftalmología de Cali";
	$msj  = "<p>Su reserva</p>";
	$msj .= "<br />";
	$msj .= "<p><strong>Nombre</strong>: ".$datos['title']."</p>";
	$msj .= "<p><strong>Fecha</strong>: ".$datos['fecha_reserva']."</p>";
	$msj .= "<p><strong>Hora</strong>: ".$datos['hora_inicio']." - ".$datos['hora_fin']."</p>";
	$msj .= "<hr />";
	$msj .= "<br />";
	$msj .= "<p><strong>Responsable</strong>: ".$datos['responsable_reserva']."</p>";
	$msj .= "<p><strong>Correo</strong>: ".$datos['responsable_correo']."</p>";
	$msj .= "<p><strong>Tel&eacute;fono</strong>:  ".$datos['telefono']." ext  ".$datos['extension']."</p>";
	$msj .= "<p>Ha sido: <strong>".$estado[$request['op']]."</strong>";
	$msj .= "<p>Para ver el listado de reservas: <a href=\"http://".$_SERVER['SERVER_NAME']."/".trim($_SERVER['PHP_SELF'],'/index.php')."/reserva/misreservas/\">Clic aqu&iacute;</a></p>";

	
	$to = Engine_Api::_()->user()->getUser($datos['user_id']);
	$from = Engine_Api::_()->user()->getUser($viewer->user_id);
	if(intval($value['user_id']) != intval($viewer->user_id))
		$api->sendConversation($asunto,$msj,$to,$from);*/
	  return true;
  }
  
	public function mreservasAction(){
		$viewer = Engine_Api::_()->user()->getViewer();
		$this->_helper->content->setEnabled();
		
		$api = Engine_Api::_()->reserva();
		
		/*Verificacion Permisos*/
		if(!$api->permisosAcceso())
			$this ->_redirect('members/home/');
		/*Fin Verificacion Permisos*/
		
		$datos = array();
		if($this->getRequest()->isPost()){
			$request = $this->getRequest()->getPost();
			
			for($i=0;$i<count($request['reserva_opc']);$i++){
                Engine_Api::_()->getDbtable('reservas', 'reserva')->cancelarReserva($request['reserva_opc'][$i], $viewer->user_id);
			}
		}
		$page=$this->_getParam('page',1);
		$paramsE = array();
		$paramsE = array("user_id"=>$viewer->user_id);
		/*$this->view->reservas = $api->obtenerReservas();*/
		$this->view->paginator = Engine_Api::_()->getDbtable('reservas', 'reserva')->getReservasPaginator($paramsE,$this->_getParam('buscador'));
		$this->view->paginator->setItemCountPerPage(5);
		$this->view->paginator->setCurrentPageNumber($page);
		$this->view->admin = false;
		$this->view->api = $api;
		
		return $this->render('index');
	}
    
}