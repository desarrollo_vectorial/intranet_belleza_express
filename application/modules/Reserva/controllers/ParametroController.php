<?php

class Reserva_ParametroController extends Core_Controller_Action_Standard
{
  public function browseAction(){
    $this->view->someVar = 'someVal';
    $this->_helper->content->setEnabled();
	$api = Engine_Api::_()->reserva();
	/*Verificacion Permisos*/
	if(!$api->permisosAcceso())
		$this ->_redirect('members/home/');
	if(!$api->permisos())
		$this ->_redirect('reserva/');
	/*Fin Verificacion Permisos*/
	$recursosReservas = $api->obtenerRecursos();
	
	$this->view->recursos = $recursosReservas;
  }

  public function viewdededescriptionAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      $id_sala = $this->_getParam("id_sala");

      $recursoModel = Engine_Api::_()->getDbtable('recursos', 'reserva');

      $this->view->recurso = $recurso = $recursoModel->getRecursoById($id_sala);

      $this->render("viewdededescription");
  }
  
  public function createAction(){
		$namespace = new Zend_Session_Namespace('sedeReserva');
		$datos = $this->_getParam('buscador');
		
		$this->view->option_id = $datos['option_id'];
		$sedes = Engine_Api::_()->getDbtable('sedes', 'reserva');
		$this->view->sedes = $sedes->obtenerSedes();
		
		$api = Engine_Api::_()->reserva();
		/*Verificacion Permisos*/
		if(!$api->permisosAcceso())
			$this ->_redirect('members/home/');
		if(!$api->permisos())
			$this ->_redirect('reserva/');
		/*Fin Verificacion Permisos*/
  		
  		$this->_helper->content->setEnabled();
  		$viewer = Engine_Api::_()->user()->getViewer();
  		
  		$recurso_id = $this->_getParam('recurso_id');
		
		if($recurso_id){
	    	$recursoTable = Engine_Api::_()->getDbtable('recursos', 'reserva');
			$recurso = $recursoTable->find($recurso_id)->current();
	    }
  		
  		$this->view->form = $form = new Reserva_Form_CreateRecurso();
  		$form->setAction($this->view->url());
  		
  		$sedes = Engine_Api::_()->getDbtable('sedes', 'reserva');
  		$form->option_id->setMultiOptions($sedes->obtenerSedes());
  		
  		if($this->getRequest()->isPost())
		if( $form->isValid($this->getRequest()->getPost()))	{
			$table = $this->_helper->api()->getDbtable('recursos', 'reserva');
			$db = $table->getAdapter();
			$db->beginTransaction();
			try	{
					$values = $form->getValues();
					$values['user_id'] = $viewer->user_id;
					$values['modified']=date('Y-m-d H:i:s');

					if(!$recurso){
						$values['created']=date('Y-m-d H:i:s');
						$recurso = $table->createRow();
					}
						
						
					$recurso->setFromArray($values);
					$recurso->save();	
				}
				catch( Exception $e ){
						$db->rollBack();
						throw $e;
					}
				$db->commit();
				$this ->_redirect('reserva/parametro/create/');
		}
		if($recurso){
			$form->title->setValue($recurso->title);
			$form->tipo->setValue($recurso->tipo);
			$form->option_id->setValue($recurso->option_id);
			$form->color->setValue($recurso->color);
			$form->estado->setValue($recurso->estado);
			$form->observaciones->setValue($recurso->observaciones);
		}


		$this->view->recursos = $api->obtenerRecursos(false,$datos['option_id'],true);

		$this->view->recursos = $api->obtenerRecursos();
	 	return true;
  }
  
  private function subirArchivo($pre){
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		echo "<pre>".print_r($_FILES,true)."</pre><hr>";
		$aleatorio=rand(00000,99999);
		$tmp_filepath = "/tmp/".$_FILES['file']['name'];
		$old = umask(0);
		mkdir(APPLICATION_PATH.'/public/reserva', 0777);
		umask($old);
		$target = APPLICATION_PATH.'/public/reserva/'.$pre.$aleatorio.".".$extension;
		
		echo $tmp_filepath;
		echo "<br>";
		echo $target;
		echo "<br>";
		echo "<br>";
		
		if((($_FILES["file"]["type"] == "image/gif")||
			($_FILES["file"]["type"] == "image/jpeg")||
			($_FILES["file"]["type"] == "image/jpg")||
			($_FILES["file"]["type"] == "image/pjpeg")||
			($_FILES["file"]["type"] == "image/x-png")||
			($_FILES["file"]["type"] == "image/png"))
			&&	($_FILES["file"]["size"] < 33200000)&&in_array($extension, $allowedExts)) {
		  if($_FILES["file"]["error"] > 0) {
		  	echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
		  	die();
		  }else {
		    echo "Upload: " . $_FILES["file"]["name"] . "<br>";
		    echo "Type: " . $_FILES["file"]["type"] . "<br>";
		    echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
		    echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";
		    /*if(file_exists(APPLICATION_PATH.'/public/reserva/'. $_FILES["file"]["name"])) {*/
		    if(file_exists(APPLICATION_PATH.'/public/reserva/'.$pre.$aleatorio.".".$extension)) {
		      echo $_FILES["file"]["name"] . " already exists. ";
		      die();
		    } else {

		      if(copy( $tmp_filepath, $target)){
			      echo "Stored in: " . APPLICATION_PATH.'/public/reserva/'. $_FILES["file"]["name"];
			      return $pre.$aleatorio.".".$extension;
		      }
		      	else{
		      		echo "Error al subir";
			      	die();
		      	}
		      		
		      /*move_uploaded_file($_FILES["file"]["name"],
		      APPLICATION_PATH.'/public/turno/'. $sitio_id.$extension);*/
		      
		    }
		  }
		} else {
		  echo "Invalid file";
		}
	}
}