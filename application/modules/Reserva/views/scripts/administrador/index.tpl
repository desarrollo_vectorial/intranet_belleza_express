<style>
	.global_form>div{ width: 100% !important; }
	.global_form div.form-label{ width: 300px !important; }
	.title_reservas{ text-align: center; margin-bottom: 20px; }
	.subtitle_reservas ul>li{ display:inline-block; position:relative; }
	.chosen-container{ position: relative !important; }

	.line_form_buscador { text-align: center; }
	.line_form_buscador >div { display: inline-block; margin: 0 auto; position: relative; }
	div#content_btns_admin { display: table; position: relative; margin: 0 auto; }
</style>

<div class="gestor--reservas">
    <div class="reservas--container-body">

        <div class="form-general">
            <h2>Solicitudes de reserva</h2>

            <?php
            if($this->admin){
                ?>
                <form id="form_buscador_solicitudes" name="buscador" method="get">
                    <div class="line_form_buscador">
                        <div class="field_buscador">
                            <label for="fecha_reserva">Sede/Empresa </label>
                            <select id="selectSede" class="selectSede" name="buscador[option_id]">
                                <?php
                                foreach($this->sedes as $k => $v){
                                    echo "<option value=\"".$k."\" ".((isset($this->option_id) && $k==$this->option_id)? "selected":"").">".$v."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="field_buscador">
                            <label for="fecha_reserva">Fecha </label>
                            <input type="text" name="buscador[fecha_reserva]" id="fecha_reserva">
                        </div>

                        <button type="submit">&nbsp;</button>
                    </div>
                </form>
                <?php
            }else{
                ?>
                <form id="form_buscador_solicitudes" name="buscador" method="get">
                    <div class="line_form_buscador">
                        <div class="field_buscador">
                            <label for="fecha_reserva">Búsqueda por fecha</label>
                            <input type="text" name="buscador[fecha_reserva]" id="fecha_reserva">
                        </div>
                        <button type="submit">Buscar</button>
                    </div>
                </form>
                <?php
            }
            ?>
        </div>

    </div>
    <form method="post" name="admin" id="admin">
        <div id="content_btns_admin">
            <?php
            if($this->admin){ ?>
                <!--<button name="op" value="1" onclick="accion=1;" type="submit">Aprobar reservas seleccionadas</button>
                <button name="op" value="2" onclick="accion=2;" type="submit">Rechazar reservas seleccionadas</button>-->
                <button name="op" value="3" onclick="accion=3;" type="submit">Cancelar reservas seleccionadas</button>
                <?php
            }else{
                ?>
                <button name="op" value="3" onclick="accion=3;" type="submit">Cancelar reservas seleccionadas</button>
                <?php
            }
            ?>
        </div>
        <table class="tabla-reservas">
            <tbody>
            <?php foreach($this->paginator as $key => $value){ ?>
                <?php
                $class_estado = '';
                switch ($value['estado']) {
                    case '0':
                        $class_estado = 'pendiente';
                        break;

                    case '1':
                        $class_estado = 'Reservado';
                        break;

                    case '2':
                        $class_estado = 'rechazado';
                        break;

                    case '3':
                        $class_estado = 'Reserva Cancelada';
                        break;

                    default:
                        $class_estado = 'pendiente';
                        break;
                }
                ?>

                <tr>
                    <td style="width: 50px;" valign="middle">
                        <?php
                        if(
                        (($value['estado']=='1' && !$value['estado']=='0'))
                        ){
                            ?>
                            <input type="checkbox" name="reserva_opc[]" value="<?php echo $value['id']; ?>">
                            <?php
                        }
                        ?>
                        <div class="color-reserva" style="background:<?php echo $value['backgroundColor']; ?>">&nbsp;</div>
                    </td>
                    <td style="width: 169px;">
                        <div class="border">
                            <div class="title-reserva"><?php echo $value['recurso_fisico']; ?></div>
                            <div><?php echo $value['title_anex']; ?></div>
                          <!--  <div><?php /*echo $value['label']; */?></div>
                            <div><?php /*echo $value['fecha_reserva']; */?></div>
                            <div><?php /*echo $value['hora_inicio']." - ".$value['hora_fin']; */?></div>-->
                        </div>
                    </td>
                    <td style="width: 200px;">
                        <div class="border">
                            <div class="title-reserva"><?php echo $value['responsable_reserva']; ?></div>
                            <div><?php echo $value['responsable_correo']; ?></div>
                            <div><?php echo $value['telefono']; ?></div>
                        </div>
                    </td>
                    <td style="width: 306px;">
                        <div class="border">
                            <div class="title-reserva">Observaciones</div>
                            <div><?php echo $value['observacion']; ?></div>
                        </div>
                    </td>
                    <td style="width: 70px;">
                        <div class="border_estado">
                            <div class="<?php echo $class_estado; ?>"><?php echo $class_estado; ?></div>
                        </div>
                    </td>
                    <td style="width: 60px;">
                        <div style="text-align: center; padding: 0;">
                            <?php
                            if($value['estado'] !== '3'){
                                ?>
                                <a href="<?php echo 'reserva/create/fecha/'.$value['fecha_reserva'].'/reserva_id/'.$value['id']; ?>">Editar</a>
                            <?php } ?>
                        </div>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </form>
    <div class="generic-paginator">
        <?php
        echo $this->paginationControl($this->paginator,null,null,array('pageAsQuery' => true,"query"=>array("buscador"=>$_GET['buscador'])));
        ?>
    </div>
</div><!--gestor--reservas-->

<!--SCRIPTS-->
<script lang="javascript">
	var accion=0;
	var estado = new Array("Pendiente", "Aprobar", "Rechazar","Cancelar"); 
	jQuery( "#admin" ).submit(function( event ) {
		var total = jQuery('input:checkbox:checked').size();
		
		if(total == 0){
			alert("Debe Seleccionar al menos una reserva");
			return false;
		}
		 
		 
		if(confirm("Confirma "+estado[accion]+" las reservas seleccionadas")){
			return true
		}else{
			event.preventDefault();
		}
	});

	jQuery(document).ready(function() {
		jQuery("#fecha_reserva").datepicker({
			dateFormat: "yy-mm-dd",
		});
		/*jQuery('#selectSede').chosen({disable_search_threshold: 10});*/
	});
</script>