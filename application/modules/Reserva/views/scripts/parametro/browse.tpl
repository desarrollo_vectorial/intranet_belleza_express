<?php
	$api = Engine_Api::_()->reserva();
?>
	<table id="recursos" width="100%">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Icono</th>
				<th>Editar</th>
				<th>Estado</th>
			</tr>
		</thead>
	<?php
	foreach($this->recursos as $key => $valor){
	?>
		<tr class="line_categoria">
			<td colspan="4"><?php echo $key; ?></td>
		</tr>
		<?php
		foreach($valor as $k => $v){
		?>
		<tr>
			<td><?php echo $v['title']; ?></td>
			<td>
			<?php
				if($v['icono']){
					?>
					<img src="public/reserva/<?php echo $v['icono']; ?>" border="0"  style=
					"max-height:66px;max-width=49px;">
					<?php
				}
			?>
			</td>
			<td>
				<a href='reserva/parametro/create/recurso_id/<?php echo $v['recurso_id']; ?>'>
					<img src='application/modules/Core/externals/images/admin/editinfo.png' border='0'>
				</a>
			</td>
			<td><?php echo $api->estados[$v['estado']]; ?></td>
		</tr>
		<?php
		}
	}
	?>
	</table>