<?php
$this->headLink()->appendStylesheet($this->baseUrl() . '/externals/jquery_ui/css/ui-lightness/jquery-ui-1.10.3.custom.min.css');
$this->headScript()->appendFile($this->baseUrl() . '/externals/fullCalendar/lib/moment.min.js');

$this->headScript()->appendFile($this->baseUrl() . '/externals/jquery_ui/js/jquery-ui-1.10.3.custom.min.js');

$api = Engine_Api::_()->reserva();
?>

<div class="form-general no--padding-left">
    <div class="columns is-multiline">
        <div class="column is-7">
            <?php echo $this->form->render($this); ?>
        </div>
        <div class="column">
            <div class="salarecusros">
               <!-- <h2 class="title_reservas">Sede/Empresa</h2>
                <ul class="select-sede">
                    <li>
                        <form method="get" name="buscador" id="buscador-x-sede">
                            <select id="selectSede" class="selectSede" name="buscador[option_id]">
                                <?php /*foreach ($this->sedes as $k => $v) {
                                    echo "<option value=\"" . $k . "\" " . ((isset($this->option_id) && $k == $this->option_id) ? "selected" : "") . ">" . $v . "</option>";
                                } */?>
                            </select>
                        </form>
                    </li>
                </ul>-->

                <h2>Listado de Salas y Recursos</h2>

                <table id="recursos" width="100%" style="background:#fff;" cellpadding="0" cellspacing="0">
                    <?php foreach ($this->recursos as $key => $valor) { ?>
                        <tr class="line_categoria">
                            <td colspan="5" style="padding: 5px; text-align: center; background:#cecece;"><?php echo $key; ?></td>
                        </tr>
                        <?php foreach ($valor as $k => $v) { ?>
                            <tr>
                                <td style="padding:15px; border-bottom: 1px solid #f6f6f6; background:#<?php echo $v['color']; ?>; width:10px;">
                                    &nbsp;
                                </td>
                                <td style="padding:15px; border-bottom: 1px solid #f6f6f6;"><?php echo $v['title']; ?></td>
                                <td style="padding:15px; border-bottom: 1px solid #f6f6f6;"><?php echo $v['label']; ?></td>
                                <td style="padding:15px; border-bottom: 1px solid #f6f6f6;" width="70px"><a
                                            href='reserva/parametro/create/recurso_id/<?php echo $v['recurso_id']; ?>'>Editar</a></td>
                                <td style="padding:15px; border-bottom: 1px solid #f6f6f6;"
                                    width="70px"><?php echo $api->estados_recursos[$v['estado']]; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    jQuery(document).ready(function () {
        jQuery('#selectSede').change(function () {
            jQuery('#buscador-x-sede').submit();
        });
    });
</script>