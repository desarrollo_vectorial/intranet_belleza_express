<style>
.popup_reserva > .popup-body > #responsable .img img {
    height: 130px;
    width: 130px;
}
</style>
<div class="popup_reserva">
	<div class="popup-header" style="background-color:<?php echo $this->reserva[0]['backgroundColor']; ?>;">
		<div class="popup-title"><?php echo $this->reserva[0]['title_anex']; ?></div>
		<div class="popup-subtitle"><?php echo $this->reserva[0]['recurso_fisico']; ?></div>
	</div>
	<div class="popup-body" style="height: auto;">
		<div id="responsable">
			<div class="img">
				<?php echo $this->itemPhoto($this->user($this->reserva[0]['user_id']), 'thumb.profile'); ?>
			</div>
			<div class="data" style="width: 225px;">
				<h3>Reservado por: </h3>
				<ul>
					<li><?php echo $this->reserva[0]['responsable_reserva']; ?></li>
					<li><?php echo $this->reserva[0]['responsable_correo']; ?></li>
					<li><?php echo $this->reserva[0]['telefono'];?></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="popup-footer">
		<div id="info_reserva">
			<h3>Datos de reserva</h3>
			<ul>
                <li><span>Observación:</span> <?php echo $this->reserva[0]['observacion']; ?></li>
				<li><span>N&uacute;mero de personas:</span> <?php echo $this->reserva[0]['num_personas']; ?></li>
				<li><span>Fecha de reserva:</span> <?php echo $this->reserva[0]['fecha_reserva']; ?></li>
				<li><span>Hora de Inicio y Finalizaci&oacute;n:</span> <?php echo $this->reserva[0]['hora_inicio']." - ".$this->reserva[0]['hora_fin']; ?></li>
			</ul>
			<div>
				<ul>
					<?php
					foreach($this->items as $key => $value){
					?>
						<li>
							<span><?php echo $key; ?>: </span>
							<?php
							for($i=0;$i<count($value);$i++){
								if($i>0){
									echo ", ";
								}
								echo $value[$i]['title']." ".(($value[$i]['cantidad'])? "(".$value[$i]['cantidad'].")":""); 	
							}
							?>
						</li>
					<?php 
					}
					?>
				</ul>
			</div>
		</div>
</div>