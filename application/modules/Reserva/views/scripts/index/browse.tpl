<?php
$this->headLink()
    //->appendStylesheet($this->baseUrl().'/externals/chosen/chosen.css')
    ->appendStylesheet($this->baseUrl() . '/externals/fullCalendar/fullcalendar.css')
    ->appendStylesheet($this->baseUrl() . '/externals/fullCalendar/fullcalendar.print.css', 'print')
    ->appendStylesheet($this->baseUrl() . '/externals/jquery_ui/css/ui-lightness/jquery-ui-1.10.3.custom.min.css')
    ->appendStylesheet($this->baseUrl() . '/externals/fancyBox/source/jquery.fancybox.css');

$this->headScript()
    ->appendFile($this->baseUrl() . '/externals/moment/moment.js')
    ->appendFile($this->baseUrl() . '/externals/fullCalendar/fullcalendar.js')
    ->appendFile($this->baseUrl() . '/externals/fullCalendar/lang/es.js')
    ->appendFile($this->baseUrl() . '/externals/jquery_ui/js/jquery-ui-1.10.3.custom.min.js')
    ->appendFile($this->baseUrl() . '/externals/fancyBox/source/jquery.fancybox.js');
?>

<script>

    var salaId;

    jQuery(document).ready(function () {

        var option_id = parseInt("<?php echo $this->namespace->option_id; ?>");
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        today = yyyy + '-' + mm + '-' + dd;

        if (option_id) {
            jQuery('#calendar').fullCalendar({
                businessHours: {start: 6, end: 20, limitDisplay: true},
                lang: 'es',
                theme: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                defaultDate: today,
                defaultView: 'month',
                editable: false,
                allDaySlot: false,
                timeFormat: 'h:mm a',
                minTime: "06:00:00",
                maxTime: "21:00:00",
                weekMode: 'variable',
                dayClick: function (date, jsEvent, view) {
                    var fecha = date.format();

                    if (!restarFechas(today, fecha)) {
                        alert("Debes seleccionar una Fecha Mayor o igual a la actual.")
                        return false;
                    }
                    var fecha_split = fecha.split("T");
                    var mensaje = fechaStr(fecha_split[0]);
                    jQuery('#mensaje-modal').html(mensaje);
                    var href = "reserva/create/fecha/" + fecha + '/salaId/' + salaId;
                    jQuery("#ventana-crear-continuar").attr("href", href);
                    jQuery.fancybox('#dialog-confirm', {
                        maxWidth: 368,
                        maxHeight: 205,
                        padding: 10,
                        fitToView: false,
                        closeClick: false,
                        autoSize: false,
                        modal: true,
                        autoPlay: false,
                        helpers: {
                            title: {
                                type: 'float'
                            }
                        }
                    });
                },
                eventClick: function (calEvent, jsEvent, view) {
                    jQuery.ajax({
                        type: "GET",
                        url: calEvent.url_
                        /*,data: jQuery('#form-reserva').serialize()*/
                    })
                        .done(function (msg) {
                            jQuery.fancybox('#dialog-info', {
                                maxWidth: 408,
                                maxHeight: 400,
                                padding: 0,
                                autoSize: false,
                                helpers: {
                                    title: {
                                        type: 'float'
                                    }
                                }
                            });
                            jQuery("#dialog-info").html(msg);
                        });
                },
                viewRender: function (view, element) {
                    // console.log("Render View");
                    var sala = jQuery('#selectSala').val();
                    loadEvents(option_id, sala);
                },
                /*events: {
                    url: 'reserva/calendariototal/option_id/'+option_id,
                    error: function() {
                        jQuery('#script-warning').show();
                    }
                }*/
            });
            jQuery(".calendar-mesaje").detach();
        }
        /*jQuery('#selectSede').chosen({disable_search_threshold: 10});
        jQuery('#selectSala').chosen({disable_search_threshold: 10});*/

        jQuery('#selectSede').change(function () {
            jQuery('#buscador-x-sede').submit();
        });

        jQuery('#selectSala').change(function () {
            var sala = jQuery(this).val();
            salaId = sala;

            loadEvents(option_id, sala);
        });

        // loadEvents(option_id,'');
    });

    function loadEvents(option_id, sala) {
        var view_object = jQuery('#calendar').fullCalendar('getView');
        var start_fecha = moment(view_object.start).format('YYYY-MM-DD');
        var end_fecha = moment(view_object.end).format('YYYY-MM-DD');
        var eventos = [];

        /*eventos.push(
            {id: "7", name: "Douglas Adams", type: "comedy"}
        );
        eventos.push(
            {id: "1", name: "Yason James", type: "real"}
        );*/

        // console.log(eventos);

        var count_reservas = 0;

        jQuery.ajax({
            url: 'reserva/calendariototal/option_id/' + option_id + '/sala_id/' + sala + '/start/' + start_fecha + '/end/' + end_fecha,
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function () {
                //$("#respuesta").html('Buscando empleado...');
            },
            error: function () {
                jQuery('#script-warning').show();
            },
            success: function (respuesta) {
                jQuery('#calendar').fullCalendar('removeEvents');

                jQuery.each(respuesta, function (index, evento) {
                    count_reservas++;

                    var titulo = "";
                    if (evento.title == null) {
                        titulo = 'Recurso Audiovisual';
                    } else {
                        titulo = evento.title;
                    }

                    eventos.push(
                        {
                            id: evento.reserva_id,
                            title: titulo,
                            start: evento.start,
                            end: evento.end,
                            url_: evento.url_,
                            user_id: evento.user_id,
                            className: evento.className,
                            backgroundColor: evento.backgroundColor
                        }
                    );

                    /*jQuery('#calendar').fullCalendar('addEventSource', [{
                        id     	: evento.reserva_id,
                        title  	: titulo,
                        start  	: evento.start,
                        end    	: evento.end,
                        url_    : evento.url_,
                        user_id : evento.user_id,
                        className : evento.className,
                        backgroundColor : evento.backgroundColor
                    }]);*/
                });

                //console.log("Cantidad de reservas: "+count_reservas);

                jQuery('#calendar').fullCalendar('addEventSource', eventos);
            }
        });
    }

    function fechaStr(fecha) {
        var meses = new Array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        var diasSemana = new Array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        var f = new Date(fecha);

        var str = fecha.split("-");

        /*document.write(diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());*/
        return (diasSemana[f.getDay()] + ", " + str[2] + " de " + meses[parseInt(str[1])] + " de " + str[0]);
    }

    function restarFechas(fechaInicial, fechaFinal) {
        var fech1 = fechaInicial;
        var fech2 = fechaFinal;

        if ((Date.parse(fech1)) > (Date.parse(fech2))) {
            return false;
        }
        return true;
    }

    jQuery(document).on({
        ajaxStart: function () {
            jQuery("body").addClass("loading");
        },
        ajaxStop: function () {
            jQuery("body").removeClass("loading");
        }
    });

</script>

<div class="container--generic-calendar">
    <h1 class="title_reservas">Bienvenido a Calendario de reservas </h1>

    <div class="subtitle_reservas">
        <div class="columns">
            <div class="column">
                <form method="get" name="buscador" id="buscador-x-sede">
                    <div class="select-custom">
                        <select id="selectSede" class="selectSede" name="buscador[option_id]">
                            <?php foreach ($this->sedes as $k => $v) {
                                if (isset($this->option_id) && $k == $this->option_id)
                                    echo '<option value="' . $k . '" selected>' . $v . '</option>';
                                else
                                    echo '<option value="' . $k . '">' . $v . '</option>';
                            } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="column">
                <div class="select-custom">
                    <select id="selectSala" name="selectSala">
                        <option value="" selected="selected">Todas las salas</option>
                        <?php foreach ($this->recursos as $j => $val) {
                            foreach ($val as $k => $valor) {
                                if (intval($valor['tipo']) == 1) {
                                    echo "<option value='" . $valor['recurso_id'] . "' >" . $valor['title'] . "</option>";
                                }
                            }
                        } ?>
                    </select>
                </div>
            </div>
        </div>


        <!--<ul>
            <li><a href="reserva/misreservas/">Ver mis reservas</a></li>
            <li class="selSede">

            </li>
            <li class="selSala">

            </li>
        </ul>-->
    </div>

    <div id='calendar'>
        <div class="calendar-mesaje">Para visualizar el calendario, escoge la sede/empresa a la que perteneces.</div>
    </div>

    <div id="dialog-confirm" style="display:none;">
        <div class="dialog-confirm-header">Nueva <strong>Reserva</strong></div>
        <div class="dialog-confirm-body">
            <div>
                <div class="modal-fecha">Fecha :
                    <span id="mensaje-modal"></span>
                </div>
            </div>
        </div>
        <div class="dialog-confirm-footer">
            <a href="" id="ventana-crear-continuar" class="btn-reservas">Continuar</a>
            <a href="javascript:void(0);" id="ventana-crear-cancelar" onclick="jQuery.fancybox.close();"
               class="btn-reservas">Cancelar</a>
        </div>
    </div>

    <div id="dialog-info" style="display:none;"></div>

    <div class="modal"></div>
</div>