<?php
//$this->headScript()->appendFile($this->baseUrl().'/externals/fancyBox/lib/jquery-1.10.1.min.js');

$this->headLink()->appendStylesheet($this->baseUrl() . '/externals/chosen/chosen.css');
$this->headScript()->appendFile($this->baseUrl() . '/externals/chosen/chosen.jquery.js');
$this->headScript()->appendFile($this->baseUrl() . '/externals/chosen/docsupport/prism.js');

$this->headLink()->appendStylesheet($this->baseUrl() . '/externals/jquery_ui/css/ui-lightness/jquery-ui-1.10.3.custom.min.css');
$this->headScript()->appendFile($this->baseUrl() . '/externals/fullCalendar/lib/moment.min.js');

$this->headScript()->appendFile($this->baseUrl() . '/externals/jquery_ui/js/jquery-ui-1.10.3.custom.min.js');

$this->headLink()->appendStylesheet($this->baseUrl() . '/externals/fancyBox/source/jquery.fancybox.css');
$this->headScript()->appendFile($this->baseUrl() . '/externals/fancyBox/source/jquery.fancybox.js');

$dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
?>

<script type="text/javascript" src="externals/jquery_ui/js/jquery-ui-1.10.3.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="externals/jquery_ui/css/ui-lightness/jquery-ui-1.10.3.custom.min.css">

<script src="<?php echo $this->baseUrl(); ?>/externals/chosen/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo $this->baseUrl(); ?>/externals/chosen/docsupport/prism.js" type="text/javascript"
        charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl(); ?>/externals/chosen/chosen.css">

<script src="<?php echo $this->baseUrl(); ?>/externals/fancyBox/source/jquery.fancybox.js"></script>
<link media="all" type="text/css" rel="stylesheet"
      href="<?php echo $this->baseUrl(); ?>/externals/fancyBox/source/jquery.fancybox.css">
<style>
    .generic_layout_container.layout_core_content {
        margin-top: 20px;
        min-height: 600px;
    }

    .instrucciones {
        background: #fff;
        position: relative;
        padding: 15px;
        width: 100% !important;
    }

    .instrucciones h3 {
        margin-bottom: 10px;
    }

    .title_reservas {
        text-align: center;
    }

    .subtitle_reservas {
        text-align: right;
        position: relative;
        margin-top: 10px;
    }

    .subtitle_reservas ul > li {
        display: inline-block;
        position: relative;
    }

    .chosen-container {
        width: 500px !important;
    }

    .fc-header td {
        border: none !important;
         
    }

    .chosen-container chosen-container-multi {
        width: 400px;
    }

    .audiovisuales {
        width: 400px;
    }

    #content_mensual-label {
        visibility: hidden
    }

</style>

<!--<h1 class="title_reservas">Ingresa los datos de tu reserva</h1>
<div class="instrucciones">
    <h3>Para enviar la <strong>solicitud de tu reserva</strong>, diligencia el formulario con todos los campos. Debes
        seleccionar la sala y/o los recursos que deseas reservar. </h3>
</div>
<div>
    <div class="menu-ubicacion"><img src="application/modules/Reserva/externals/images/ubicacion.png"/>
        &nbsp; <?php /*echo $this->sede; */?></div>
</div>-->

<div class="form-general no--padding-left">
    <div class="row">
        <div class="col-8">
            <?php echo $this->form->render($this); ?>
        </div>
        <div class="col-4">
            <div class="sala--observaciones-container">
                <h4>Observaciones de la sala:</h4>
                <p id="sala--observaciones">Observación...</p>
            </div>
        </div>
    </div>
</div>


<div id="dialog-empty" class="create-reserva" style="display:none;">
    <div class="dialog-confirm-header">No es posible realizar la reserva</div>
    <div class="dialog-confirm-data-body">Para hacer efectiva esta reserva, debes seleccionar una sala o un recurso
    </div>
    <div class="dialog-confirm-footer">
        <button id="btn_cerrar_empty" class="btn-reservas">EDITAR RESERVA</button>
    </div>
</div>

<div id="dialog-confirm" class="create-reserva" style="display:none;">
    <div class="dialog-confirm-header">Se han enviado correctamente los datos de tu reserva.</div>
    <div class="dialog-confirm-data-body">Recibirás en tu correo electrónico la confirmación de solicitud y un archivo
        adjunto que debes descargar
        para que la reserva también quede incluida en tu calendario de outlook y desde allí invites a los asistentes.
    </div>
    <div class="dialog-confirm-data-body-gracias"><h3>Muchas gracias</h3></div>
    <div class="dialog-confirm-footer"><a href="reserva/" id="ventana-crear-continuar"
                                          class="btn-reservas">Finalizar</a></div>
</div>

<div id="dialog-busy" class="create-reserva" style="display:none;">
    <div class="dialog-confirm-header">No es posible realizar la reserva</div>
    <div class="dialog-confirm-data-body">Lo sentimos. La sala que seleccionó ya se encuentra ocupada
        el <?php echo $dias[date('w', strtotime($this->fecha))] . ' ' . date("d", strtotime($this->fecha)) . ' de ' . $meses[date('n', strtotime($this->fecha)) - 1] . ' de ' . date("Y", strtotime($this->fecha)) . ' de ' . date("g:i a", strtotime($this->hora_inicio)) . ' a ' . date("g:i a", strtotime($this->hora_fin)); ?>
        .
    </div>
    <div class="dialog-confirm-data-body-gracias"><h3>Por favor elija otra sala o cambie el horario</h3></div>
    <div class="dialog-confirm-footer">
        <button id="btn_cerrar_ocupada" class="btn-reservas">EDITAR RESERVA</button>
    </div>
</div>

<script>
    jQuery(function ($) {

        $( "#recurso_id-wrapper" ).append("<img class='hidden' src='/application/modules/Reserva/externals/images/spinner.gif' alt='carga' id='feedb' />" );

        $( "#recurso_id" ).change(function() {
            var recurso_id = $( "#recurso_id" ).val();

            $( "#feedb" ).removeClass("hidden");
            $.get( "/reserva/saladescription/" + recurso_id)
                .then(function( data ) {
                    console.log(data);
                    $( "#feedb" ).addClass("hidden");
                    $( "#sala--observaciones" ).html(data);
            });
        });
    })
</script>

<script>
    jQuery(document).ready(function () {

        jQuery("#btn_cerrar_ocupada, #btn_cerrar_empty").off("click");
        jQuery("#btn_cerrar_ocupada, #btn_cerrar_empty").on("click", function () {
            jQuery.fancybox.close();
        });

        jQuery("#content_mensual-wrapper .form-label").html('<ul>\
					<li><input type="radio" name="tipo_mensual" id="tipo_mensual-1" checked="checked" value="1"></li>\
					<li><input type="radio" name="tipo_mensual" id="tipo_mensual-2" value="2"></li>\
				</ul>');

        jQuery("#content_mensual-wrapper #dia_mes").val('<?php echo $this->dia_mes[2]; ?>');

        jQuery("#fecha_final").datepicker({
            dateFormat: "yy-mm-dd",
            minDate: 0,
            changeYear: true,
            changeMonth: true
        });

        var titulo = "<?php echo $this->form->getTitle(); ?>";

        if (titulo == "Editar reserva") {
            jQuery("#fecha_reserva").datepicker({
                dateFormat: "yy-mm-dd",
                minDate: 0,
                changeYear: true,
                changeMonth: true
            });
        }

        if (jQuery("#recurrente:checked").val()) {
            jQuery('#tipo_periodicidad-wrapper').attr("style", "display:block");
            jQuery('#fecha_final-wrapper').attr("style", "display:block");
            jQuery('#cada_mensual-wrapper').attr("style", "display:block");
        }

        if (jQuery("input[name=tipo_periodicidad]:checked").val() == 1) {
            jQuery('#cada_dia-wrapper').attr("style", "display:block");
            jQuery('#incluir_fds-wrapper').attr("style", "display:block");
            jQuery('#dia-wrapper').attr("style", "display:none");
            jQuery('#cada_semanal-wrapper').attr("style", "display:none");
            jQuery('#content_mensual-wrapper').attr("style", "display:none");
            jQuery('#cada_anual-wrapper').attr("style", "display:none");
        }
        if (jQuery("input[name=tipo_periodicidad]:checked").val() == 2) {
            jQuery('#cada_dia-wrapper').attr("style", "display:none");
            jQuery('#incluir_fds-wrapper').attr("style", "display:none");
            jQuery('#dia-wrapper').attr("style", "display:block");
            jQuery('#cada_semanal-wrapper').attr("style", "display:block");
            jQuery('#content_mensual-wrapper').attr("style", "display:none");
            jQuery('#cada_anual-wrapper').attr("style", "display:none");
        }
        if (jQuery("input[name=tipo_periodicidad]:checked").val() == 3) {
            jQuery('#cada_dia-wrapper').attr("style", "display:none");
            jQuery('#incluir_fds-wrapper').attr("style", "display:none");
            jQuery('#dia-wrapper').attr("style", "display:none");
            jQuery('#cada_semanal-wrapper').attr("style", "display:none");
            jQuery('#content_mensual-wrapper').attr("style", "display:block");
            jQuery('#cada_anual-wrapper').attr("style", "display:none");
        }
        if (jQuery("input[name=tipo_periodicidad]:checked").val() == 4) {
            jQuery('#cada_dia-wrapper').attr("style", "display:none");
            jQuery('#incluir_fds-wrapper').attr("style", "display:none");
            jQuery('#dia-wrapper').attr("style", "display:none");
            jQuery('#cada_semanal-wrapper').attr("style", "display:none");
            jQuery('#content_mensual-wrapper').attr("style", "display:none");
            jQuery('#cada_anual-wrapper').attr("style", "display:block");
        }

        jQuery('#recursos').chosen({disable_search_threshold: 10});

        <?php
        if (strcmp("empty", $this->empty_all) == 0) {
            echo "empty();";
        }

        if (strcmp("confirm", $this->confirm) == 0) {
            echo "confirmacion();";
        } else if (strcmp("busy", $this->confirm) == 0) {
            echo "console.log('Busy: jojojo " . $this->hora_inicio . " - " . $this->hora_fin . "');";
            echo "salaOcupada();";
        }
        ?>
    });
</script>
<script>
    jQuery("#recurrente").click(function () {
        if (jQuery("#recurrente:checked").val()) {
            jQuery('#tipo_periodicidad-wrapper').attr("style", "display:block");
            jQuery('#fecha_final-wrapper').attr("style", "display:block");
            jQuery('#cada_mensual-wrapper').attr("style", "display:block");
        } else {
            jQuery('#tipo_periodicidad-wrapper').attr("style", "display:none");
            jQuery('#fecha_final-wrapper').attr("style", "display:none");
            jQuery('#cada_mensual-wrapper').attr("style", "display:none");            
            jQuery('#cada_dia-wrapper').attr("style", "display:none");
            jQuery('#incluir_fds-wrapper').attr("style", "display:none");
            jQuery('#dia-wrapper').attr("style", "display:none");
            jQuery('#cada_semanal-wrapper').attr("style", "display:none");
            jQuery('#content_mensual-wrapper').attr("style", "display:none");
            jQuery('#cada_anual-wrapper').attr("style", "display:none");
        }
    });

    jQuery("input[name=tipo_periodicidad]:radio").click(function () {
        if (jQuery("input[name=tipo_periodicidad]:checked").val() == 1) {
            jQuery('#cada_dia-wrapper').attr("style", "display:block");
            jQuery('#incluir_fds-wrapper').attr("style", "display:block");
            jQuery('#dia-wrapper').attr("style", "display:none");
            jQuery('#cada_semanal-wrapper').attr("style", "display:none");
            jQuery('#content_mensual-wrapper').attr("style", "display:none");
            jQuery('#cada_anual-wrapper').attr("style", "display:none");
        }
        if (jQuery("input[name=tipo_periodicidad]:checked").val() == 2) {
            jQuery('#cada_dia-wrapper').attr("style", "display:none");
            jQuery('#incluir_fds-wrapper').attr("style", "display:none");
            jQuery('#dia-wrapper').attr("style", "display:block");
            jQuery('#cada_semanal-wrapper').attr("style", "display:block");
            jQuery('#content_mensual-wrapper').attr("style", "display:none");
            jQuery('#cada_anual-wrapper').attr("style", "display:none");
        }
        if (jQuery("input[name=tipo_periodicidad]:checked").val() == 3) {
            jQuery('#cada_dia-wrapper').attr("style", "display:none");
            jQuery('#incluir_fds-wrapper').attr("style", "display:none");
            jQuery('#dia-wrapper').attr("style", "display:none");
            jQuery('#cada_semanal-wrapper').attr("style", "display:none");
            jQuery('#content_mensual-wrapper').attr("style", "display:block");
            jQuery('#cada_anual-wrapper').attr("style", "display:none");
        }
        if (jQuery("input[name=tipo_periodicidad]:checked").val() == 4) {
            jQuery('#cada_dia-wrapper').attr("style", "display:none");
            jQuery('#incluir_fds-wrapper').attr("style", "display:none");
            jQuery('#dia-wrapper').attr("style", "display:none");
            jQuery('#cada_semanal-wrapper').attr("style", "display:none");
            jQuery('#content_mensual-wrapper').attr("style", "display:none");
            jQuery('#cada_anual-wrapper').attr("style", "display:block");
        }
    });

    function acceptNum(evt) {
        var nav4 = window.Event ? true : false;
        var key = nav4 ? evt.which : evt.keyCode;

        return (key <= 13 || (key >= 48 && key <= 57));
    }

    function empty() {
        jQuery.fancybox('#dialog-empty', {
            maxWidth: 700,
            maxHeight: 200,
            padding: 0,
            fitToView: false,
            closeClick: false,
            autoSize: false,
            modal: true,
            autoPlay: false,
            helpers: {
                title: {
                    type: 'float'
                }
            }
        });
    }

    function confirmacion() {
        jQuery.fancybox('#dialog-confirm', {
            maxWidth: 773,
            maxHeight: 247,
            padding: 0,
            fitToView: false,
            closeClick: false,
            autoSize: false,
            modal: true,
            autoPlay: false,
            helpers: {
                title: {
                    type: 'float'
                }
            }
        });
    }

    function salaOcupada() {
        jQuery.fancybox('#dialog-busy', {
            maxWidth: 773,
            maxHeight: 247,
            padding: 0,
            fitToView: false,
            closeClick: false,
            autoSize: false,
            modal: true,
            autoPlay: false,
            helpers: {
                title: {
                    type: 'float'
                }
            }
        });
    }
</script>

<!-- <script height="409px" width="641px" src="http://player.ooyala.com/iframe.js#pbid=Y2EwZTZjZWEzZTVjYTI5MzE5YWY2NTlh&ec=U2N2NqMjrfF4ZJYWzH7G1aZp1C28ic24"></script>-->