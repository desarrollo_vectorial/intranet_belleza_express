<?php
	class Reserva_Api_Core extends Core_Api_Abstract{
		
		public $tipos = array(1=>"Espacio Físico",
								2=>"Recursos Audiovisuales",
								3=>"Cafetería",
								4=>"Soporte");
								
		public $estados = array(0=>"Pendiente",
								1=>"Activo",
								2=>"Rechazado",
								3=>"Cancelado");

		public $estados_recursos = array(0 => "Inactivo", 1 => "Activo");
		public $days = array(
			0=>"Monday",
			1=>"Tuesday",
			2=>"Wednesday",
			3=>"Thursday",
			4=>"Friday",
			5=>"Saturday",
			6=>"Sunday"
		);
		
		public function obtenerRecursos($opc=false,$sede_id=null,$label=true){
			$recursoTable = Engine_Api::_()->getDbtable('recursos', 'reserva');
			$query = $recursoTable->select()
						->setIntegrityCheck(false)
						->from(array("a" => $recursoTable->info('name')),array("a.*"))
						->joinLeft(array("b"=>"engine4_user_fields_options"),"a.option_id = b.option_id",array("b.label"))
						->order('a.tipo')
						->order('b.label')
						->order('a.recurso_id')
						->order('a.title');
			if($opc){
				$query -> where("estado =?",1);
			}
			if($sede_id){
				$query -> where("a.option_id =?",$sede_id);
			}

			$rst = $query->query();
			$recursosReservas = $rst->fetchAll();
			
			$recursos = array();
			foreach($recursosReservas as $key => $valor){
				if($label){
					$recursos[$this->tipos[intval($valor['tipo'])]][]= $valor;
				} else {
					$recursos[$valor['tipo']][]= $valor;
				}
			}

			/*echo "<pre>".print_r($recursos,true)."</pre>";*/
			return $recursos;
		}
		
		public function obtenerItems($reserva_id=NULL,$opc=1){
			$recursoTable = Engine_Api::_()->getDbtable('recursos', 'reserva');
			$itemTable = Engine_Api::_()->getDbtable('items', 'reserva');
			
			$query = $itemTable->select()
						->setIntegrityCheck(false)
						->from(array("a" => $itemTable->info('name')),array("a.*"))
						->join(array("b"=>$recursoTable->info('name')),"a.recurso_id = b.recurso_id",array("b.*"))
						->where("a.reserva_id =?",$reserva_id)
						->order('b.tipo');
			$rst = $query->query();			
			$items = $rst->fetchAll();
			
			$recursos = array();
			
			if($opc === 1){
				foreach($items as $key => $valor){
					$recursos[$this->tipos[intval($valor['tipo'])]][]= $valor;	
				}
			}
			if($opc === 2){
				/*foreach($items as $key => $valor){
					$recursos[$valor['tipo']][$valor['recurso_id']]= $valor;
				}*/
				$recursos = $items;
			}
			/*echo "<pre>".print_r($recursos,true)."</pre>";*/
			return $recursos;
		}
		
		public function obtenerReservas($estado = null,$reserva_id = null,$sede_id=null,$sala_id=null){
			$reserva = Engine_Api::_()->getDbtable('reservas', 'reserva');
			$itemTable = Engine_Api::_()->getDbtable('items', 'reserva');
			
			$select = "(select
							a.reserva_id,
							CONCAT('#',b.color) as backgroundColor
						from
							engine4_reserva_items as a
							JOIN engine4_reserva_recursos as b ON(a.recurso_id = b.recurso_id)
						where
							b.tipo = 1
						)";
			
			$query = $reserva->select()
						->setIntegrityCheck(false)
						->from(array("a" => $reserva->info('name')),array(	"id"=> "a.reserva_id",
																			"title"=>"CONCAT('',b.title,'<br>',c.displayname)",
																			"title_anex"=>"a.title",
																			"num_personas"=>"a.num_personas",
																			/*"responsable_reserva"=>"a.responsable_reserva",*/
																			"telefono" => "a.telefono",
																			"extension"=> "a.extension",
																			/*"responsable_correo" => "a.responsable_correo",*/
																			"start"=>"CONCAT(a.fecha_reserva,'T',a.hora_inicio)",
																			"end"=>"CONCAT(a.fecha_reserva,'T',a.hora_fin)",
																			"observaciones"=>"a.observaciones",
																			"observacion"=>"a.observacion",/*
																			"backgroundColor"=>"CONCAT('#',a.color)",*/
																			"hora_inicio" => "a.hora_inicio",
																			"hora_fin" => "a.hora_fin",
																			"fecha_reserva" => "a.fecha_reserva",
																			"estado" => "a.estado",
																			"className" => "CONCAT('','smoothbox')",
																			"url_"=>"CONCAT('','reserva/resumen/',a.reserva_id)",
																			"user_id" => "a.user_id",
																			"recurrente",
																			"fecha_final",
																			"tipo_periodicidad",
																			"cada_dia",
																			"cada_semanal",
																			"cada_mensual",
																			"cada_anual",
																			"posicion_dia",
																			"dia_mensual",
																			"lunes",
																			"martes",
																			"miercoles",
																			"jueves",
																			"viernes",
																			"sabado",
																			"domingo"
																			))
						->joinLeft(array("b"=>"engine4_reserva_recursos"),"a.recurso_id = b.recurso_id",array("backgroundColor"=>"CONCAT('#',b.color)","recurso_fisico"=>"b.title"))
						->join(array("c"=>"engine4_users"),"a.user_id = c.user_id",array(
																							"responsable_reserva"=>"c.displayname",
																							"responsable_correo"=>"c.email"
																						))
						->order('a.fecha_reserva');
			if($estado)
				$query->where("a.estado =?",$estado);
			if($reserva_id)
				$query->where("a.reserva_id =?",$reserva_id);
			if($sede_id)
				$query->where("a.option_id =?",$sede_id);
			if($sala_id)
				$query->where("a.recurso_id =?",$sala_id);
			/*echo $query;*/
			$rst = $query->query();			
			$reservas = $rst->fetchAll();
			
			return $reservas;
		}

		public function obtenerReservasCalendar($estado = null,$reserva_id = null,$sede_id=null,$sala_id=null,$start_view=null,$end_view=null){
			$reserva = Engine_Api::_()->getDbtable('reservas', 'reserva');
			$itemTable = Engine_Api::_()->getDbtable('items', 'reserva');

			/*$month = date('m');
		    $year = date('Y');*/
		    /*$last_day = date("d", mktime(0,0,0, $month+1, 0, $year));
			
			$fecha_inicial_mes = date('Y-m-d', mktime(0,0,0, $month, 1, $year));
			$fecha_final_mes = date('Y-m-d', mktime(0,0,0, $month, $last_day, $year));*/

		    $fecha_inicial_mes = date('Y-m-d', strtotime($start_view));
			$fecha_final_mes = date('Y-m-d', strtotime($end_view." -1 day"));

			/*echo $fecha_inicial_mes."<br />";
			echo $fecha_final_mes."<br />";
			exit;*/
			
			$select = "(select
							a.reserva_id,
							CONCAT('#',b.color) as backgroundColor
						from
							engine4_reserva_items as a
							JOIN engine4_reserva_recursos as b ON(a.recurso_id = b.recurso_id)
						where
							b.tipo = 1
						)";
			
			$query = $reserva->select()
						->setIntegrityCheck(false)
						->from(array("a" => $reserva->info('name')),array(	"id"=> "a.reserva_id",
																			"title"=>"CONCAT('',b.title)",
																			"title_anex"=>"a.title",
																			"start"=>"CONCAT(a.fecha_reserva,'T',a.hora_inicio)",
																			"end"=>"CONCAT(a.fecha_reserva,'T',a.hora_fin)",
																			"className" => "CONCAT('','smoothbox')",
																			"url_"=>"CONCAT('','reserva/resumen/',a.reserva_id)",
																			"user_id" => "a.user_id"
																			))
						->joinLeft(array("b"=>"engine4_reserva_recursos"),"a.recurso_id = b.recurso_id",array("backgroundColor"=>"CONCAT('#',b.color)","recurso_fisico"=>"b.title"));

			$query->where("a.fecha_reserva >= ?",  $fecha_inicial_mes);
    		$query->where("a.fecha_reserva <= ?",  $fecha_final_mes);

			if($estado)
				$query->where("a.estado =?",$estado);
			if($reserva_id)
				$query->where("a.reserva_id =?",$reserva_id);
			if($sede_id)
				$query->where("a.option_id =?",$sede_id);
			if($sala_id)
				$query->where("a.recurso_id =?",$sala_id);
			/*echo $query;*/
			$rst = $query->query();			
			$reservas = $rst->fetchAll();
			
			return $reservas;
		}
		
		public function permisos($opc = true){
			$viewer = Engine_Api::_()->user()->getViewer();
			$table = Engine_Api::_()->getDbtable('permisos', 'reserva');
			$query = $table->select()
						->setIntegrityCheck(false)
						->from(array("a" => $table->info('name')),array("a.*"));
			if($opc)
				$query->where("a.user_id =?",$viewer->getIdentity());
			$rst = $query->query();			
			$permisos = $rst->fetchAll();
			
			if(!$opc){
				return $permisos;
			}
			
			if($permisos)
				return true;
			return false;	
		}
		
		public function permisosAcceso(){
			$viewer = Engine_Api::_()->user()->getViewer();
			
			if(!$viewer->getIdentity()){
				return false;
			}
			
			$table = Engine_Api::_()->getDbtable('configs', 'reserva');
			$query = $table->select()
						->setIntegrityCheck(false)
						->where("level_id =?",$viewer['level_id'])
						->where("prohibir =?",1);
			$rst = $query->query();			
			$permisos = $rst->fetchAll();
			
			if(count($permisos)>=1){
				return false;
			}
			
			return true;
		}
		/*
			`title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
			`user_id` int(11) unsigned NOT NULL,
			`recipients` int(11) unsigned NOT NULL,
			`modified` datetime NOT NULL,
			`locked` tinyint(1) NOT NULL DEFAULT '0',
			`resource_type` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '',
			`resource_id` int(11) unsigned NOT NULL DEFAULT '0',
			
	$conversation = Engine_Api::_()->getItemTable('messages_conversation')->send(
        $viewer,
        $recipients,
        $values['title'],
        $values['body'],
        $attachment
      );
			
			Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification(
              $user,
              $viewer,
              $conversation,
              'message_new'
            );
		*/
		public function sendConversation($asunto = "",$mensaje = "",$to=null,$from=null){
			/*$datos = array("title"=>$asunto,"user_id"=>$from,"recipients"=>$to,"modified"=>date('Y-m-d H:i:s'));*/
			/*$viewer = Engine_Api::_()->user()->getViewer();*/
			
			/*$conversation = Engine_Api::_()->getItemTable('messages_conversation')->send(
				$from,
				$to,
				$asunto,
				$mensaje,
				null
			);
			
			Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification(
				$to,
				$from,
				$conversation,
				'message_new'
			);*/
					$mail = new Zend_Mail('utf-8');
					$mail->setBodyHtml($mensaje);
					$mail->setFrom($from->email, $from->displayname);
					$mail->addTo($to->email, $to->displayname);
					$mail->setSubject($asunto);
					if($mail->send())
						return true;
						else
							return false;
				
		}
	
		public function obtenerDiasEntreFechas($fechaI = null,$fechaF = null){
			$start = strtotime($fechaI);
			$end = strtotime($fechaF);
			$dias	= ($start-$end)/86400;
			$dias 	= abs($dias); 
			$dias = floor($dias);
			return $dias;
		}
		
		public function obtenerSemanasEntreFechas($fechaI = null,$fechaF = null){
			$start = new DateTime($fechaI);
			$end = new DateTime($fechaF);
			$interval = $start->diff($end);
			$semanas = ceil(($interval->format('%a')/7));
			return $semanas;
		}
		public function obtenerMesesEntreFechas($fechaI = null,$fechaF = null){
			$start = new DateTime($fechaI);
			$end = new DateTime($fechaF);
			$interval = $start->diff($end);
			$meses = ($interval->y * 12) + $interval->m;
			echo $meses;
			return $meses;
		}
		public function obtenerAniosEntreFechas($fechaI = null,$fechaF = null){
			$start = new DateTime($fechaI);
			$end = new DateTime($fechaF);
			$interval = $start->diff($end);
			$anios = $interval->y;
			return $anios;
		}

		function getUltimoDiaMes($elAnio,$elMes) {
			return date("d",(mktime(0,0,0,$elMes+1,1,$elAnio)-1));
		}
		
	}
?>