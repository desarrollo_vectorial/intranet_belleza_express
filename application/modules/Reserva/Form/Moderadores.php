<?php
class Reserva_Form_Moderadores extends Engine_Form
{

	private $recursos = array();
	private $users = array();
	
	public function __construct($recursos, $users) {
	    Engine_FOrm::enableForm($this);
	    self::enableForm($this);

		$this->recursos = $recursos;
		$this->users = $users;

	    parent::__construct();
	}
	
    public function init()
    {
        $this->setTitle('Administradores de los recursos')
            ->setDescription('xxx')
            ->setAttrib('name', 'moderadores');

	    $this->setMethod('POST')
	            ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()));

       $this->setDecorators(
       						array(
                    				'FormElements',
                    				array('HtmlTag', array('tag'=>'table', 'width'=>'100%', 'cellspacing' => '0', 'cellpadding'=>'0')),
                    				'Form'
                				)
							);


		$subForm = new Zend_Form_SubForm();

		foreach($this->recursos as $row){
		  $id = $row['recurso_id'];
		  $rowForm = new Zend_Form_SubForm();
		  foreach($row as $key => $value){
		    if($key == 'recurso_id') continue;
			  
			  if($key == 'user_id') {
			    $rowForm->addElement(
			      'select',
			      $key,
			      array(
			        'value' => $value,
			      )
			    );
			  } else {
			    $rowForm->addElement(
			      'text',
			      $key,
			      array(
			        'value' => $value,
			      )
			    );							  	
			  }
			 
		  }

		  $rowForm->user_id->setMultiOptions( $this->users );
		  
		  $rowForm->setElementDecorators(array(
		    'ViewHelper',
		    'Errors',
		    array('HtmlTag', array('tag' => 'td', 'width' => '25%')),
		  ));
		
		  $subForm->addSubForm($rowForm, $id);
		}
		
		$subForm->setSubFormDecorators(array(
		  'FormElements',
		  array('HtmlTag', array('tag'=>'tr')),
		));
		 
		$this->addSubForm($subForm, 'moderadors');
		
		$this->setSubFormDecorators(array(
		  'FormElements',
		  array('HtmlTag', array('tag' => 'tbody')),
		));
		 
		$this->setDecorators(array(
		    'FormElements',
		    array('HtmlTag', array('tag' => 'table')),
		    'Form'
		));
 
		$this->addElement(
		  'submit', 'submit', array('label' => 'Actualizar'));
		
		$this->submit->setDecorators(array(
		    array(
		        'decorator' => 'ViewHelper',
		        'options' => array('helper' => 'formSubmit')),
		    array(
		        'decorator' => array('td' => 'HtmlTag'),
		        'options' => array('tag' => 'td', 'colspan' => 4)
		        ),
		    array(
		        'decorator' => array('tr' => 'HtmlTag'),
		        'options' => array('tag' => 'tr')),
		));

    }

}