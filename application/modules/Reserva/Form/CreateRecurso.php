<?php
class Reserva_Form_CreateRecurso extends Engine_Form
{
	public $_error = array();

	public function init()
	{
		$this->setTitle('Crear nuevo Recurso');
		$this->setDescription('Crear Recursos para utilizar en las reservas por parte del usuario.');
		$this->setAttrib('name', 'crear_reserva');
		/*		->setAttrib('onsubmit', 'return false;')*/
		
		$this->addElement('text', 'title', array(
		'label' => 'Nombre del recurso',
		'allowEmpty' => false,
		'required' => true,
		'maxlength ' => 200		
		));
		
		/*$this->addElement('text', 'color', array(
		'label' => 'Color',
		'allowEmpty' => false,
		'required' => true,
		'maxlength ' => 200		
		));*/		
		
		$this->addElement('Select', 'tipo', array(
		'label' => 'Tipo de recurso',
		'allowEmpty' => false,
		'required' => true,
		'multiOptions'=> array("1"=>"Físico","2"=>"Audiovisual","3"=>"Cafetería","4"=>"Soporte")
		));		       
        
        
        $this->addElement('Select', 'option_id', array(
			'label' => 'Sede/Empresa a la que pertenece el Recurso',
			'allowEmpty' => false,
			'required' => true
		));		
        
		/*$this->addElement('file', 'file', array(
		'label' => 'Icono',
		'allowEmpty' => true,
		'required' => false,
		'maxlength ' => 200		
		));*/
        
		$this->addElement('select', 'color', array(
		'label' => 'Color (*Aplicable solo al recurso físico)',
        'multiOptions'=> array(
            "#F44336"=>"Red",
            "#E91E63"=>"Pink",
            "#9C27B0"=>"Purple",
            "#673AB7"=>"Deep Purple",
            "#3F51B5"=>"Indigo",
            "#2196F3"=>"Blue",
            "#03A9F4"=>"Light Blue",
            "#00BCD4"=>"Cyan",
            "#009688"=>"Teal",
            "#4CAF50"=>"Green",
            "#8BC34A"=>"Light Green",
            "#CDDC39"=>"Lime",
            "#FFC107"=>"Amber",
            "#FF9800"=>"Orange",
            "#795548"=>"Brown",
            "#9E9E9E"=>"Grey",
            "#607D8B"=>"Blue Grey",
            "#000000"=>"Black",
            "#FFEB3B"=>"Yellow"
        ),
		'maxlength ' => 200		
		));

		$this->addElement('textarea', 'observaciones', array(
		'label' => 'Observaciones',
		'maxlength ' => 800		
		));
        
        $this->addElement('Select', 'estado', array(
		'label' => 'Estado',
		'allowEmpty' => false,
		'required' => true,
		'multiOptions'=> array("1"=>"Activo","0"=>"Inactivo")
		));	
        
		$this->addElement('Button', 'crear_turno', array(
		'label' => 'Crear/Actualizar recurso',
		'type' => 'submit',
		'ignore' => true,
        'decorators' => array('ViewHelper')
		));
		
		$this->addElement('Cancel', 'cancel', array(
		'label' => 'Cancelar',
		'link' => true,
		'decorators' => array('ViewHelper')
		));

        $this->addDisplayGroup(array('crear_turno', 'cancel'), 'buttons');

	}
}