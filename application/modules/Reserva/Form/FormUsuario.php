<?php
class Reserva_Form_FormUsuario extends Engine_Form
{
	public $_error = array();

	public function init()
	{
		$this->setTitle('Listado de Responsables');
		$this->setDescription('Todos los usuarios ingresados recibirán una notificación de cada solicitud de reserva.
        Busque y asigne los usuarios responsables de cada área, encargados de brindar soporte a las reservas aprobadas.');
		$this->setAttrib('name', 'reserva_permisos');


		$this->addElement('Select', 'user_id', array(
		'label' => 'Usuarios',
		));

         $this->addElement('Button', 'submit', array(
            'label' => 'Agregar usuario',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array('ViewHelper'),
        ));

	}
}