<?php
class Reserva_Form_CreateReserva extends Engine_Form
{
	public $_error = array();
	
	public $dias = array();
	public $semanas = array();
	public $meses 	= array();
	public $anios 	= array();	
	public $horas 	= array(
						"06:00:00"=>"06:00",
						"06:30:00"=>"06:30",
						"07:00:00"=>"07:00",
						"07:30:00"=>"07:30",
						"08:00:00"=>"08:00",
						"08:30:00"=>"08:30",
						"09:00:00"=>"09:00",
						"09:30:00"=>"09:30",
						"10:00:00"=>"10:00",
						"10:30:00"=>"10:30",
						"11:00:00"=>"11:00",
						"11:30:00"=>"11:30",
						"12:00:00"=>"12:00",
						"12:30:00"=>"12:30",
						"13:00:00"=>"13:00",
						"13:30:00"=>"13:30",
						"14:00:00"=>"14:00",
						"14:30:00"=>"14:30",
						"15:00:00"=>"15:00",
						"15:30:00"=>"15:30",
						"16:00:00"=>"16:00",
						"16:30:00"=>"16:30",
						"17:00:00"=>"17:00",
						"17:30:00"=>"17:30",
						"18:00:00"=>"18:00",
						"18:30:00"=>"18:30",
						"19:00:00"=>"19:00"
					);
	
	
	public function init()
	{
		$arreglo = array();
		for($i=1;$i<=365;$i++){
			$arreglo[$i] = $i." día(s)";
		}
		$this->dias = $arreglo;
		
		$arreglo = array();
		for($i=1;$i<=4;$i++){
			$arreglo[$i] = $i." semana(s)";
		}
		$this->semanas = $arreglo;
		
		$arreglo = array();
		for($i=1;$i<=12;$i++){
			$arreglo[$i] = $i." mes(es)";
		}
		$this->meses = $arreglo;
		
		$arreglo = array();
		for($i=1;$i<=3;$i++){
			$arreglo[$i] = $i." año(s)";
		}
		$this->anios = $arreglo;
		
		$this->setTitle('Nueva reserva');
		$this->setDescription('');
		$this->setAttrib('name', 'crear_reserva');
		/*		->setAttrib('onsubmit', 'return false;')*/

		$this->addElement('hidden', 'reserva_id');
		
		$this->addElement('text', 'title', array(
			'label' => 'Nombre de la reserva',
			'allowEmpty' => false,
			'required' => true,
			'maxlength ' => 200		
		));
		
		/*$this->addElement('text', 'num_personas', array(
			'label' => 'Nombre de la reserva',
			'allowEmpty' => false,
			'required' => true,
			'maxlength ' => 2		
		));*/
		
		$this->addElement('text', 'num_personas', array(
			'label' 	 => 'Número de personas',
			'allowEmpty' => false,
			'required' 	 => true,
			'maxlength ' => 2		
		));
		
		$this->addElement('text', 'telefono', array(
			'label' 	 => 'Teléfono y Extensión del responsable',
			'allowEmpty' => false,
			'required' 	 => true,
			'maxlength ' => 200		
		));
		
		$this->addElement('text', 'fecha_reserva', array(
			'label' 	 => 'Fecha de la reserva',
			'allowEmpty' => false,
			'required' 	 => true,
			'maxlength ' => 200,
			'readOnly'   => true	
		));
		
		$this->addElement('select', 'hora_inicio', array(
			'label' 	   => 'Hora de inicio',
			'allowEmpty'   => false,
			'required' 	   => true,
			'maxlength '   => 200,
			'multiOptions' => $this->horas,
		));	

		$this->addElement('select', 'hora_fin', array(
			'label' => 'Hora de finalización',
			'allowEmpty' => false,
			'required' => true,
			'maxlength ' => 200,
			'multiOptions' => $this->horas,
		));		
		
		$this->addElement('select', 'recurso_id', array(
			'label' 	   => 'Nombre de la sala',
			'allowEmpty'   => false,
			'required' 	   => true,
			'multiOptions' => array(""=>"Ninguno")
		));	
		
		/* $this->addElement('select', 'recursos', array(
			'label' => 'Recursos Audiovisuales',
			'allowEmpty' => true,
			'required' => false,
			'multiOptions'=> array(""=>"Ninguno"),
			'attributes' =>  array('multiple'=>true)	
		));	*/
		
		$multi = new Engine_Form_Element_Multiselect('recursos',array(
			'allowEmpty' 	   => true,
			'required' 		   => false,
			'data-placeholder' => "Seleccione el(los) Recurso(s) audiovisual(es) que requiere"
		));
		$multi->setLabel('Recursos Audiovisuales');
		$this->addElement($multi);
		
		$this->addElement('textarea', 'observacion', array(
			'label' 	 => 'Observaciones',
			'allowEmpty' => true,
			'required'   => false
		));	
        
        $this->addElement('checkbox', 'recurrente', array(
			'label' 	 => 'Cita Recurrente?',
			'allowEmpty' => true,
			'required'   => false
		));
			
		$this->addElement('radio', 'tipo_periodicidad', array(
			'label' => 'Periodicidad',
			/* 'allowEmpty' => false,
			'required' => true, */
			'multiOptions'=> array("1"=>"Diaria","2"=>"Semanal","3"=>"Mensual","4"=>"Anual")
		));
					
        $this->addElement('text', 'fecha_final', array(
			'label' 	   => 'Fecha final',
			/* 'allowEmpty' => false,
			'required' 	   => true, */
			'maxlength '   => 200,
			'readOnly'     => true	
		));		
		
		$this->addElement('select', 'cada_dia', array(
			'label' => 'Cada',
			/* 'allowEmpty' => true,
			'required' => false, */
			'multiOptions' => $this->dias,
		));

		$this->addElement('checkbox', 'incluir_fds', array(
			'label' => 'Incluir Fines de Semana',
			'allowEmpty' => true,
			'required' => false
		));
		
		
		$diasF = new Engine_Form_Element_MultiCheckbox('dia');
		$diasF->setMultiOptions(array(
									"0"=>"Lunes",
									"1"=>"Martes",
									"2"=>"Miércoles",
									"3"=>"Jueves",
									"4"=>"Viernes",
									"5"=>"Sábado",
									"6"=>"Domingo"
								)
							);

		$diasF->setLabel('Días');
		$this->addElement($diasF);
		
		$this->addElement('select', 'cada_semanal', array(
			'label' => 'Cada',
			/*'allowEmpty' => true,
			'required' => false,*/
			'multiOptions' => $this->semanas,
		));

		$this->addElement('select', 'cada_mensual', array(
			'label' => 'Cada',
			'multiOptions' => $this->meses,
		));

		$contenido = '<ul>
						<li>
						<div class="txt">El</div>
						<div class="input">
							<input type="text" name="dia_mes" id="dia_mes" value="" maxlength="2" disabled="disabled">
						</div>
						<div class="txt">Cada</div>
						<div class="input">
							<select name="cada_mensual_1" id="cada_mensual_1">';
								for ($i=1; $i < 13; $i++) { 
									$contenido .= "<option value='$i' label='$i mes(es)''>$i mes(es)</option>";
								}
		$contenido .=		'</select>
						</div>
					</li>
					<li>
						<div class="txt">El</div>
						<div class="input">
							<select name="posicion_dia" id="posicion_dia">
								<option value="1">Primer</option>
								<option value="2">Segundo</option>
								<option value="3">Tercer</option>
								<option value="4">Cuarto</option>
								<option value="5">Último</option>
							</select>
						</div>
						<div class="input">
							<select name="dia_mensual" id="dia_mensual">
								<option value="1">Lunes</option>
								<option value="2">Martes</option>
								<option value="3">Miércoles</option>
								<option value="4">Jueves</option>
								<option value="5">Viernes</option>
								<option value="6">Sábado</option>
								<option value="7">Domingo</option>
							</select>
						</div>
						<div class="txt"> de cada </div>
						<div class="input">
						    <div class="select-custom">
						        <select name="cada_mensual_2" id="cada_mensual_2">\';
								for ($i=1; $i < 13; $i++) { 
									$contenido .= "<option value=\'$i\' label=\'$i mes(es)\'\'>$i mes(es)</option>";
								}
		                        $contenido .=\'</select>
                            </div>
						</div>
					</li>
				</ul>';

		$this->addElement('Dummy', 'content_mensual', array(
			'content' => $contenido
		));

		?>
		<!-- <div id="content_mensual" class="form-wrapper">
			<div class="form-label">
				<ul>
					<li><input type="radio" name="tipo_mensual" id="tipo_mensual-1" value="1"></li>
					<li><input type="radio" name="tipo_mensual" id="tipo_mensual-2" value="2"></li>
				</ul>
			</div>
			<div class="form-element">
				
			</div>
		</div> -->
		<?php

		$this->addElement('select', 'cada_anual', array(
			'label' 	   => 'Cada',
			/*'allowEmpty' => true,
			'required' 	   => false,*/
			'multiOptions' => $this->anios,
		));
		
		$this->addElement('Button', 'crear_turno', array(
			'label' 	 => 'Confirma y envía tu solicitud',
			'type' 		 => 'submit',
			'ignore' 	 => true,
			'decorators' => array('ViewHelper')
		));
		
		$this->addElement('Cancel', 'cancel', array(
			'label' 	  => 'Cancelar',
			'link' 		  => true,
			'prependText' => '',
			'decorators'  => array('ViewHelper')
		));

        $this->addDisplayGroup(array('crear_turno', 'cancel'), 'buttons');

	}
}