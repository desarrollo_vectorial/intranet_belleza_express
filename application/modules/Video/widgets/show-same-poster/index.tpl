<style type="text/css" media="screen">
  .generic_layout_container .layout_video_show_same_poster{ background-color: #e9e9e9 !important; margin-bottom: 15px; margin-top: 10px; }
  .generic_layout_container .layout_video_show_same_poster h3{ display: none; }

  .generic_layout_container .layout_video_show_same_poster .generic_list_widget{ background-color: #e9e9e9 !important; }
  .generic_layout_container .layout_video_show_same_poster .generic_list_widget .photo,.info{ margin-left: 15px; }
  .generic_layout_container .layout_video_show_same_poster .icon-publicada{ background-image:url(public/admin/split-articulo.png); display:inline-block; background-size:cover; width:20px; height:20px; }
  .generic_layout_container .layout_video_show_same_poster .stats .img1{ background-position: 0 0; }
  .generic_layout_container .layout_video_show_same_poster .img2{ background-position: 100px 0; }
</style>


<ul class="generic_list_widget generic_list_widget_large_photo">
  <?php foreach( $this->paginator as $item ): ?>
    <li>
      <div class="photo">
        <?php //echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, 'thumb.normal'), array('class' => 'thumb')) ?>
        <?php
        if( $item->url ) {
            echo '<iframe src="'.$item->url.'" width="250px" height="160px" style="border:none;"></iframe>';
        } else {
            echo '<img alt="" src="' . $this->escape($this->layout()->staticBaseUrl) . 'application/modules/Video/externals/images/video.png">';
        } ?>
      </div>
      <div class="info">
        <div class="title">
          <?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?>
        </div>
        <div class="stats">
          <i class="icon-publicada img1"></i>
          <?php echo $this->timestamp($item->creation_date) ?>
        </div>
        <div class="owner">
          <i class="icon-publicada img2"></i>
          <?php
            $owner = $item->getOwner();
            echo $this->translate('%1$s', $this->htmlLink($owner->getHref(), $owner->getTitle()));
          ?>
        </div>
      </div>
    </li>
  <?php endforeach; ?>
</ul>