<style type="text/css" media="screen">
	.generic_layout_container .layout_video_estadisticas{
		background-color: #e9e9e9 !important;
		margin-bottom: 15px;
		margin-top: 10px;
	}

	.generic_layout_container .layout_video_estadisticas .titulo{
		padding   : 20px;
		display   : block;
		position  : relative;
		text-align  : right;
	}

	.generic_layout_container .layout_video_estadisticas .titulo .most{
		color 		: #8b8b8a;	
		display 	: inline-block;	
		top 		: 10px;
		font-size 	: 34px;
		line-height : 30px;
		width 		: 200px;
		vertical-align: middle;
	}

	.generic_layout_container .layout_video_estadisticas .titulo .most:before{
		content 		: '';
		display 		: block;
		position 		: absolute;
		background-image: url(../public/admin/articulo-estadisticas.png);
		width 			: 50px;
		height 			: 50px;
		left 			: 20px;
		top 			: 30px;
	}

	.generic_layout_container .layout_video_estadisticas .video_top_views li{
		display: block;
		vertical-align: top;
		padding-left: 15px;
	}
	
	.generic_layout_container .layout_video_estadisticas .video_top_views .blogs_browse_info{
		position: relative;
		display: inline-block;
		margin-bottom: 5px;
		padding-left: 15px;
		width: 90%;
	}

	.generic_layout_container .layout_video_estadisticas .video_top_views .blogs_browse_info .blogs_browse_info_title{
		position: relative;
		display:inline-block;
		color: #8b8b8a;
		font-size: 18px;
		font-weight: normal;
		/*text-transform: capitalize;*/
	}

	.generic_layout_container .layout_video_estadisticas .video_top_views .blogs_browse_info .blogs_browse_info_cont{
		position   : relative;
		display    : inline-block;
		color      : #8b8b8a;
		font-size  : 18px;
		font-weight: bold;
		float: right;
		
	}
</style>

<div class="titulo">

	<div class="most">
		<p style="font-size: 30px !important;">Estadísticas</p>
		<b>Generales</b>
	</div>
	
</div>

<ul class='video_top_views'>
	<?php foreach( $this->estadisticas as $estadistica ): ?>
	<li>
		<div class='blogs_browse_info'>
			<span class='blogs_browse_info_title'><?php echo $this->translate($estadistica['category_name']);?></span>
			<span class='blogs_browse_info_cont'><?php echo $estadistica['count_video'];?></span>
		</div>
	</li>
	<?php endforeach; ?>
</ul>




