<style type="text/css" media="screen">
  .generic_layout_container .layout_video_list_recent_videos{
    background-color: #e9e9e9 !important;
    margin-bottom: 15px;
    margin-top: 10px;
  }

  .generic_layout_container .layout_video_list_recent_videos .titulo{
    padding   : 20px;
    display   : block;
    position  : relative;
    text-align  : right;
  }

  .generic_layout_container .layout_video_list_recent_videos .titulo .most{
    color     : #8b8b8a;  
    display   : inline-block; 
    top     : 10px;
    font-size   : 34px;
    line-height : 30px;
    width     : 200px;
    vertical-align: middle;
  }

  .generic_layout_container .layout_video_list_recent_videos .titulo .most:before{
    content     : '';
    display     : block;
    position    : absolute;
    background-image: url(../public/admin/icon_videos.png);
    width       : 40px;
    height      : 40px;
    left        : 20px;
    top         : 30px;
  }
  .generic_layout_container .layout_video_list_recent_videos .generic_list_widget{ background-color: #e9e9e9 !important; }
  .generic_layout_container .layout_video_list_recent_videos .generic_list_widget .photo,.info{ margin-left: 15px; }
  .generic_layout_container .layout_video_list_recent_videos .icon-publicada{ background-image:url(public/admin/split-articulo.png); display:inline-block; background-size:cover; width:20px; height:20px; }
  .generic_layout_container .layout_video_list_recent_videos .stats .img1{ background-position: 0 0; }
  .generic_layout_container .layout_video_list_recent_videos .img2{ background-position: 100px 0; }
</style>


<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Video
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>

<div class="titulo">
  <div class="most">
    <p style="font-size: 30px !important;">Videos</p>
    <b>Recientes</b>
  </div>  
</div>


<ul class="generic_list_widget generic_list_widget_large_photo">
  <?php foreach( $this->paginator as $item ): ?>
    <li>
      <div class="photo">
        <?php
        if( $item->url ) {
            echo '<iframe src="'.$item->url.'" width="250px" height="160px" style="border:none;"></iframe>';
        } else {
            echo '<img alt="" src="' . $this->escape($this->layout()->staticBaseUrl) . 'application/modules/Video/externals/images/video.png">';
        } ?>
      </div>
      <div class="info">
        <div class="title">
          <?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?>
        </div>
        <div class="stats">
          <i class="icon-publicada img1"></i>
          <?php echo $this->timestamp($item->{$this->recentCol}) ?>
        </div>
        <div class="owner">
        <i class="icon-publicada img2"></i>
          <?php
            $owner = $item->getOwner();
            echo $this->translate('%1$s', $this->htmlLink($owner->getHref(), $owner->getTitle()));
          ?>
        </div>
      </div>
    </li>
  <?php endforeach; ?>
</ul>