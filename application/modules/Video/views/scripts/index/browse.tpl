<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Video
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: browse.tpl 9785 2012-09-25 08:34:18Z pamela $
 * @author     John Boehr <john@socialengine.com>
 */
?>
<div class="container--list-tumbs">
    <?php if ($this->tag): ?>
        <h3>
            <?php echo $this->translate('Videos using the tag') ?>
            #<?php echo $this->tag ?>
            <a href="<?php echo $this->url(array('module' => 'video', 'controller' => 'index', 'action' => 'browse'), 'default', true) ?>">(x)</a>
        </h3>
    <?php endif; ?>

    <?php if ($this->paginator->getTotalItemCount() > 0): ?>

        <ul class="format--list-tumbs">
            <?php foreach ($this->paginator as $item) { ?>

                <li>
                    <?php if($item->url): ?>
                        <a class="list-tumbs--image" href="<?php echo $item->getHref(); ?>">
                            <span style="background-image: url(<?php echo $item->getPhotoUrl('thumb'); ?>);"></span>
                        </a>
                    <?php else: ?>
                        <a class="list-tumbs--image" href="<?php echo $item->getHref(); ?>">
                            <span style="background-image: url(<?php echo $this->escape($this->layout()->staticBaseUrl) . 'application/modules/Video/externals/images/video.png'; ?>);"></span>
                        </a>
                    <?php endif; ?>

                    <p class="list-tumbs--info">
                        <span class="list-tumbs--title">
                            <?php echo $this->htmlLink($item->getHref(), $item->getTitle(), array('class' => 'video_titulo')) ?>
                        </span>
                        <span class="list-tumbs--autor">
                            Por: <?php echo $this->htmlLink($item->getOwner()->getHref(), $item->getOwner()->getTitle()) ?>
                        </span>
                        <span class="list-tumbs--counter">
                            <?php echo $this->translate(array('%1$s Visita', '%1$s Visitas', $item->view_count), $this->locale()->toNumber($item->view_count)) ?>
                        </span>

                        <span>
                            <?php if ($item->rating > 0): ?>
                                <?php for ($x = 1; $x <= $item->rating; $x++): ?>
                                    <span class="rating_star_generic rating_star"></span>
                                <?php endfor; ?>
                            <?php if ((round($item->rating) - $item->rating) > 0): ?>
                                    <span class="rating_star_generic rating_star_half"></span>
                                <?php endif; ?>
                            <?php endif; ?>
                        </span>
                    </p>
                </li>
            <?php } ?>
        </ul>

    <?php elseif ($this->category || $this->tag || $this->text): ?>
        <div class="tip">
            <span>
                <?php echo $this->translate('Nobody has posted a video with that criteria.');?>
                <?php if ($this->can_create):?>
                    <?php echo $this->translate('Be the first to %1$spost%2$s one!', '<a href="'.$this->url(array('action' => 'create'), "video_general").'">', '</a>'); ?>
                <?php endif; ?>
            </span>
        </div>

    <?php else: ?>
        <div class="tip">
            <span>
                <?php echo $this->translate('_videos_nocreate_video');?>
                <?php if ($this->can_create):?>
                    <?php echo $this->translate('_videos_link_create%1$spost%2$s one!', '<a href="'.$this->url(array('action' => 'create'), "video_general").'">', '</a>'); ?>
                <?php endif; ?>
            </span>
        </div>
    <?php endif; ?>

    <?php echo $this->paginationControl($this->paginator, null, null, array('query' => $this->formValues, 'pageAsQuery' => true,)); ?>
</div>
