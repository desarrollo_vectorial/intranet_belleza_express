<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Video
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: edit.tpl 9987 2013-03-20 00:58:10Z john $
 * @author     Jung
 */
?>

<div class="form-general">
  <?php echo $this->form->render(); ?>
</div>

<script type="text/javascript">
  $$('.core_main_video').getParent().addClass('active');
</script>
