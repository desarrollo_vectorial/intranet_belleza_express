<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Video
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: manage.tpl 9987 2013-03-20 00:58:10Z john $
 * @author     Jung
 */
?>

<div class="container-list">
    <?php if (($this->current_count >= $this->quota) && !empty($this->quota)): ?>
        <div class="tip">
      <span>
        <?php echo $this->translate('You have already created the maximum number of videos allowed. If you would like to post a new video, please delete an old one first.');?>
      </span>
        </div>
        <br/>
    <?php endif; ?>

    <?php if ($this->paginator->getTotalItemCount() > 0): ?>

        <ul class='format--list'>
            <?php foreach ($this->paginator as $item): ?>
                <li>
                    <div class="format--image-left">
                        <?php if($item->url): ?>
                            <a class="image-left--enlace" href="<?php echo $item->getHref(); ?>">
                                <span style="background-image: url(<?php echo $item->getPhotoUrl('thumb'); ?>);"></span>
                            </a>
                        <?php else: ?>
                            <a class="image-left--enlace" href="<?php echo $item->getHref(); ?>">
                                <span style="background-image: url(<?php echo $this->escape($this->layout()->staticBaseUrl) . 'application/modules/Video/externals/images/video.png'; ?>);"></span>
                            </a>
                        <?php endif; ?>
                    </div>

                    <div class="format--info">
                        <div class="info--content">
                            <div class="format--title">
                                <h3><?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?></h3>
                            </div>

                            <div class="video_stats">
                                <div class="video_views">
                                    <i class="icon-publicada img1"></i>
                                    <?php echo $this->timestamp(strtotime($item->creation_date)) ?>

                                    <i class="icon-publicada img2"></i>
                                    <span>
                                        <?php echo $this->translate(array('%s comment', '%s comments', $item->comments()->getCommentCount()), $this->locale()->toNumber($item->comments()->getCommentCount())) ?>
                                    </span>

                                    <i class="icon-publicada img3"></i>
                                    <span>
                                         <?php echo $this->translate(array('%s like', '%s likes', $item->likes()->getLikeCount()), $this->locale()->toNumber($item->likes()->getLikeCount())) ?>
                                    </span>

                                    <i class="icon-publicada img4"></i>
                                   <span>
                                       <?php echo $this->translate(array('%s view', '%s views', $item->view_count), $this->locale()->toNumber($item->view_count)) ?>
                                   </span>
                                </div>
                                <!--<span class="video_star"></span>
                                <span class="video_star"></span>
                                <span class="video_star"></span>
                                <span class="video_star"></span>
                                <span class="video_star_half"></span>-->
                            </div>

                            <div class="format--description">
                                <?php echo $this->string()->truncate($this->string()->stripTags($item->description), 300) ?>
                            </div>

                            <?php if ($item->status == 0): ?>
                                <div class="tip">
                                  <span>
                                    <?php echo $this->translate('Your video is in queue to be processed - you will be notified when it is ready to be viewed.')?>
                                  </span>
                                </div>
                            <?php elseif ($item->status == 2): ?>
                                <div class="tip">
                                  <span>
                                    <?php echo $this->translate('Your video is currently being processed - you will be notified when it is ready to be viewed.')?>
                                  </span>
                                </div>
                            <?php elseif ($item->status == 3): ?>
                                <div class="tip">
                                  <span>
                                   <?php echo $this->translate('Video conversion failed. Please try %1$suploading again%2$s.', '<a href="'.$this->url(array('action' => 'create', 'type'=>3)).'">', '</a>'); ?>
                                  </span>
                                </div>
                            <?php elseif ($item->status == 4): ?>
                                <div class="tip">
                                  <span>
                                   <?php echo $this->translate('Video conversion failed. Video format is not supported by FFMPEG. Please try %1$sagain%2$s.', '<a href="'.$this->url(array('action' => 'create', 'type'=>3)).'">', '</a>'); ?>
                                  </span>
                                </div>
                            <?php elseif ($item->status == 5): ?>
                                <div class="tip">
                                  <span>
                                   <?php echo $this->translate('Video conversion failed. Audio files are not supported. Please try %1$sagain%2$s.', '<a href="'.$this->url(array('action' => 'create', 'type'=>3)).'">', '</a>'); ?>

                                  </span>
                                </div>
                            <?php elseif ($item->status == 7): ?>
                                <div class="tip">
                                  <span>
                                   <?php echo $this->translate('Video conversion failed. You may be over the site upload limit.  Try %1$suploading%2$s a smaller file, or delete some files to free up space.', '<a href="'.$this->url(array('action' => 'create', 'type'=>3)).'">', '</a>'); ?>

                                  </span>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="format--options">
                            <?php
                            if ($item->status != 2) {
                                echo $this->htmlLink(array('route' => 'default', 'module' => 'video', 'controller' => 'index', 'action' => 'delete', 'video_id' => $item->video_id, 'format' => 'smoothbox'), $this->translate('Delete Video'), array(
                                    'class' => 'smoothbox options--eliminar'
                                ));
                            }
                            ?>
                            <?php echo $this->htmlLink(array(
                                'route' => 'default',
                                'module' => 'video',
                                'controller' => 'index',
                                'action' => 'edit',
                                'video_id' => $item->video_id
                            ), $this->translate('Edit Video'), array(
                                'class' => 'options--editar'
                            )) ?>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php else: ?>
        <div class="tip">
            <span>
                <?php echo $this->translate('_video_manage_firt_mensaje');?>
                <?php if ($this->can_create): ?>
                    <?php echo $this->translate('_video_manage_enlace %1$sposting%2$s.', '<a href="'.$this->url(array('action' => 'create')).'">', '</a>'); ?>
                <?php endif; ?>
            </span>
        </div>

    <?php endif; ?>
    <?php echo $this->paginationControl($this->paginator); ?>
</div>


<script type="text/javascript">
    $$('.core_main_video').getParent().addClass('active');
</script>