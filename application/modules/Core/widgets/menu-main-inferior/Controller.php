<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Core_Widget_MenuMainInferiorController extends Engine_Content_Widget_Abstract
{
    public function indexAction()
    {

        $this->view->ver = 0;
        $viewer = Engine_Api::_()->user()->getViewer();
        if ($viewer->getIdentity()) {
            $this->view->ver = 1;
        }

        $currentUrl = $this->view->url();
        if ($urlParts = explode("/", $currentUrl)) {
            if (isset($urlParts[1])) {
                $parentUrl = $urlParts[1];
            } else {
                $parentUrl = array();
            }
        } else {
            $parentUrl = array();
        }

        $this->view->cntrSubMenu = $cntrSubMenu = Engine_Api::_()->getApi('menus', 'core')->getNavigation('core_main_sub_principal', array(), false, $parentUrl);

    }

    public function getCacheKey()
    {
        //return Engine_Api::_()->user()->getViewer()->getIdentity();
    }
}