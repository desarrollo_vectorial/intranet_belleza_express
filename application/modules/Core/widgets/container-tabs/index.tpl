<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script type="text/javascript">
    en4.core.runonce.add(function () {
        var tabContainerSwitch = window.tabContainerSwitch = function (element) {

            if (element.tagName.toLowerCase() == 'a') {
                element = element.getParent('li');
            }

            var myContainer = element.getParent('.tabs_parent').getParent();

            myContainer.getChildren('div:not(.tabs_alt)').setStyle('display', 'none');
            myContainer.getElements('ul > li').removeClass('active');
            element.get('class').split(' ').each(function (className) {
                className = className.trim();
                if (className.match(/^tab_[0-9]+$/)) {
                    myContainer.getChildren('div.' + className).setStyle('display', null);
                    element.addClass('active');
                }
            });
        };

        var tabContainerSwitchBySelect = window.tabContainerSwitchBySelect = function (element, valueClass) {

            var myContainer = element.getParent('.tabs_parent').getParent();

            myContainer.getChildren('div:not(.tabs_alt)').setStyle('display', 'none');
            myContainer.getElements('ul > li').removeClass('active');
            valueClass.split(' ').each(function (className) {
                className = className.trim();
                if (className.match(/^tab_[0-9]+$/)) {
                    myContainer.getChildren('div.' + className).setStyle('display', null);
                    //element.addClass('active');
                }
            });
        };

        var moreTabSwitch = window.moreTabSwitch = function (el) {
            el.toggleClass('tab_open');
            el.toggleClass('tab_closed');
        };
    });
</script>

<div class='menu-tabs-generic tabs_alt tabs_parent' id="num-tabs<?php echo(count($this->tabs)); ?>">

    <?php if ($this->type == 'select'): ?>
        <select onchange="tabContainerSwitchBySelect(this, this.value);">
            <?php foreach ($this->tabs as $key => $tab): ?>
                <?php
                $class = array();
                $class[] = 'tab_' . $tab['id'];
                $class[] = 'tab_' . trim(str_replace('generic_layout_container', '', $tab['containerClass']));
                if ($this->activeTab == $tab['id'] || $this->activeTab == $tab['name'])
                    $class[] = 'active';
                $class = join(' ', $class);
                ?>
                <?php if ($key < $this->max): ?>
                    <option value="<?php echo $class; ?>"><?php echo $this->translate($tab['title']); ?></option>
                <?php endif; ?>
            <?php endforeach; ?>
        </select>
    <?php endif; ?>

    <ul>
        <?php if ($this->type == 'tab'): ?>
            <?php
            /*Realizamos el conteo para poner el id del numero de tabs*/
            $countTabs = 1;
            ?>
            <?php foreach ($this->tabs as $key => $tab): ?>
                <?php
                $class = array();
                $class[] = 'tab_' . $tab['id'];
                $class[] = 'tab_' . trim(str_replace('generic_layout_container', '', $tab['containerClass']));
                if ($this->activeTab == $tab['id'] || $this->activeTab == $tab['name'])
                    $class[] = 'active';
                $class = join(' ', $class);
                ?>
                <?php if ($key < $this->max): ?>
                    <li class="<?php echo $class ?>"><a href="javascript:void(0);"
                                                        onclick="tabContainerSwitch(document.getElementById('<?php echo $tab['containerClass'] ?>'), '<?php echo $tab['containerClass'] ?>');"
                                                        id="<?php echo $tab['containerClass'] ?>"><?php echo $this->translate($tab['title']) ?><?php if (!empty($tab['childCount'])): ?>
                                <span>(<?php echo $tab['childCount'] ?>)</span><?php endif; ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php echo $numbTabsFinal; ?>

        <?php if (count($this->tabs) > $this->max): ?>
            <li class="tab_closed more_tab" onclick="moreTabSwitch($(this));">
                <div class="tab_pulldown_contents_wrapper">
                    <div class="tab_pulldown_contents">
                        <ul>
                            <?php foreach ($this->tabs as $key => $tab): ?>
                                <?php
                                $class = array();
                                $class[] = 'tab_' . $tab['id'];
                                $class[] = 'tab_' . trim(str_replace('generic_layout_container', '', $tab['containerClass']));
                                if ($this->activeTab == $tab['id'] || $this->activeTab == $tab['name']) $class[] = 'active';
                                $class = join(' ', array_filter($class));
                                ?>
                                <?php if ($key >= $this->max): ?>
                                    <li class="<?php echo $class ?>"
                                        onclick="tabContainerSwitch($(this), '<?php echo $tab['containerClass'] ?>')"><?php echo $this->translate($tab['title']) ?><?php if (!empty($tab['childCount'])): ?>
                                            <span> (<?php echo $tab['childCount'] ?>)</span><?php endif; ?></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <a href="javascript:void(0);"><?php echo $this->translate('More +') ?><span></span></a>
            </li>
        <?php endif; ?>
    </ul>
</div>

<?php echo $this->childrenContent ?>