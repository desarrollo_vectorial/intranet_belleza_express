<?php

/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9972 2013-03-19 01:21:13Z john $
 * @author     John
 */

/**
 *
 * @category Application_Core
 * @package Core
 * @copyright Copyright 2006-2010 Webligo Developments
 * @license http://www.socialengine.com/license/
 */
class Core_Widget_CommentsController extends Engine_Content_Widget_Abstract
{

    public function indexAction()
    {
        // Get subject
        $subject = null;
        if (Engine_Api::_()->core()->hasSubject()) {
            $subject = Engine_Api::_()->core()->getSubject();
        } else 
            if (($subject = $this->_getParam('subject'))) {
                list ($type, $id) = explode('_', $subject);
                $subject = Engine_Api::_()->getItem($type, $id);
            } else 
                if (($type = $this->_getParam('type')) && ($id = $this->_getParam('id'))) {
                    $subject = Engine_Api::_()->getItem($type, $id);
                }
        
        if (! ($subject instanceof Core_Model_Item_Abstract) || ! $subject->getIdentity() || (! method_exists($subject, 'comments') && ! method_exists($subject, 'likes'))) {
            return $this->setNoRender();
        }
        
        // Perms
        $viewer = Engine_Api::_()->user()->getViewer();
        $this->view->canComment = $canComment = $subject->authorization()->isAllowed($viewer, 'comment');
        $this->view->canDelete = $canDelete = $subject->authorization()->isAllowed($viewer, 'edit');
        
        // Likes
        $this->view->viewAllLikes = $this->_getParam('viewAllLikes', false);
        $this->view->likes = $likes = $subject->likes()->getLikePaginator();
        
        // Comments
        
        // If has a page, display oldest to newest
        if (null !== ($pagina = $this->_getParam('page'))) {
            $comentarioselect = $subject->comments()->getCommentSelect();
            $comentarioselect->order('comment_id ASC');
            $comentarios = Zend_Paginator::factory($comentarioselect);
            $comentarios->setCurrentPageNumber($pagina);
            $comentarios->setItemCountPerPage(10);
            $this->view->comentarios = $comentarios;
            $this->view->pagina = $pagina;
        } else {
            // If not has a page, show the
            $comentarioselect = $subject->comments()->getCommentSelect();
            $comentarioselect->order('comment_id DESC');
            $comentarios = Zend_Paginator::factory($comentarioselect);
            $comentarios->setCurrentPageNumber(1);
            $comentarios->setItemCountPerPage(4);
            $this->view->comentarios = $comentarios;
            $this->view->pagina = $pagina;
        }
        
        if ($viewer->getIdentity() && $canComment) {
            $this->view->miForm = $miForm = new Core_Form_Comment_Create();
            // $miForm->setAction($this->view->url(array('action' => '')));
            $miForm->populate(array(
                'identity' => $subject->getIdentity(),
                'type' => $subject->getType()
            ));
        }
        
        // Hide if can't post and no comments
        if (! $canComment && ! $canDelete && count($comentarios) <= 0 && count($likes) <= 0) {
            $this->setNoRender();
        }
    }
}