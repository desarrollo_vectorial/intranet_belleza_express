<?php
/**
 * SocialEngine - Search Widget Smarty Template
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2012 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Matthew
 */
?>

<div class="side-search-box">
  <div class="side-search-box--titulo">
    <span><strong>Búsqueda</strong> general</span>
  </div>

  <?php if( $this->search_check ): ?>
  	<?php echo $this->form->render($this) ?>
  <?php endif; ?>
</div>