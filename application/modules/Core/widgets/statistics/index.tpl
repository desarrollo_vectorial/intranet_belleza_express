<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style type="text/css" media="screen">
  .layout_core_statistics{
    background-color: #e9e9e9 !important;
    margin-bottom: 15px;
    margin-top: 10px;
  }

  #tituloEs{
    padding   : 20px;
    display   : block;
    position  : relative;
    text-align  : right;
  }

  #tituloEs .most{
    color     : #8b8b8a;  
    display   : inline-block; 
    top     : 10px;
    font-size   : 34px;
    line-height : 30px;
    width     : 200px;
    vertical-align: middle;
  }

  #tituloEs .most:before{
    content     : '';
    display     : block;
    position    : absolute;
    background-image: url(../public/admin/articulo-estadisticas.png);
    width       : 50px;
    height      : 50px;
    left      : 20px;
    top       : 30px;
  }


  .layout_core_statistics #group_top_views li{
    display: block;
    vertical-align: top;
    padding-left: 15px;
  }
  
  .layout_core_statistics #group_top_views #blogs_browse_info{
    position: relative;
    display: inline-block;
    margin-bottom: 5px;
    padding-left: 15px;
    width: 90%;
  }
  #group_top_views{
    background-color: #e9e9e9 !important;;

  }

  .layout_core_statistics #group_top_views #blogs_browse_info #blogs_browse_info_title{
    position: relative;
    display:inline-block;
    color: #8b8b8a;
    font-size: 16px;
    font-weight: normal;
    /*text-transform: capitalize;*/
  }

  .layout_core_statistics #group_top_views #blogs_browse_info #blogs_browse_info_cont{
    position   : relative;
    display    : inline-block;
    color      : #8b8b8a;
    font-size  : 16px;
    font-weight: bold;
    float: right;
    
  }
</style>
<div id="tituloEs">
  <div class="most">
    <p style="font-size: 30px !important;">Estadísticas</p>
    <b>Generales</b>
  </div>  
</div>
<ul id='group_top_views'>
  <li>
    <div id='blogs_browse_info'>
      <span id='blogs_browse_info_title'><?php echo $this->translate(array('member', 'members', $this->member_count)) ?></span>
      <span id='blogs_browse_info_cont'><?php echo $this->locale()->toNumber($this->member_count); ?></span>
    </div>
  </li>
  <?php if ($this->friend_count > 0): ?>
    <li>
      <div id='blogs_browse_info'>
        <span id='blogs_browse_info_title'><?php echo $this->translate(array('friendship', 'friendships', $this->friend_count)) ?></span>
        <span id='blogs_browse_info_cont'><?php echo $this->locale()->toNumber($this->friend_count) ?></span>
      </div>
    </li>
  <?php endif; ?>
  <?php if ($this->post_count > 0): ?>
    <li>
      <div id='blogs_browse_info'>
        <span id='blogs_browse_info_title'><?php echo $this->translate(array('post', 'posts', $this->post_count)) ?></span>
        <span id='blogs_browse_info_cont'><?php echo $this->locale()->toNumber($this->post_count) ?></span>
      </div>
    </li>
  <?php endif; ?>
  <?php if ($this->comment_count > 0): ?>
    <li>
      <div id='blogs_browse_info'>
        <span id="blogs_browse_info_title"><?php echo $this->translate(array('comment', 'comments', $this->comment_count)) ?></span>
        <span id='blogs_browse_info_cont'><?php echo $this->locale()->toNumber($this->comment_count) ?></span>
      <div>
    </li>
  <?php endif; ?>

  <?php if (is_array($this->hooked_stats) && !empty($this->hooked_stats)): ?>
  <?php foreach ($this->hooked_stats as $key => $value): ?>
    <?php if ($value > 0): ?>
      <li>
         <div id='blogs_browse_info'>
          <span id="blogs_browse_info_title"><?php echo $this->translate(array($key, $key, $value)) ?></span>
          <span id="blogs_browse_info_cont"><?php echo $this->locale()->toNumber($value) ?></span>
        </div>
      </li>
    <?php endif; ?>
  <?php endforeach; endif; ?>

</ul>
