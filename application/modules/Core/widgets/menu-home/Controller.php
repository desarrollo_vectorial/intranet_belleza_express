<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Core_Widget_MenuHomeController extends Engine_Content_Widget_Abstract{
    public function indexAction(){
        $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('home_menu');
        $this->view->providencia = Engine_Api::_()->getApi('menus', 'core')->getNavigation('home_providencia');
        $this->view->sucroal = Engine_Api::_()->getApi('menus', 'core')->getNavigation('home_sucroal');
    }
}
