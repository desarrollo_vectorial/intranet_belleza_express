<style type="text/css">
     .tabulado1{ text-align:left; color: #717171;  margin-bottom: 20px; margin-top: 0px;}
     .tabulado1 a{ cursor:pointer; color: #717171; }
     .tabulado1 ul{ display:block; position:relative; margin: 0 auto; }
     .tabulado1 ul li{
        display: inline; position: relative; padding: 23px; padding-top: 10px; padding-bottom: 10px; margin: 0px;
        text-align: center; align-content: center; border-bottom: 1px solid #cfcfcf;
    }

     .tabulado1 ul li p:hover{ background: #4e4d4d; color:#FFF; }
      content_menu_bussunies .li{ border-bottom: 0.001em solid #a6a6a6; top: 0px; vertical-align: top; overflow:hidden; }
     .content_menu_bussunies span{ color:#FFF; position:relative; display:inline-block; width:15px; padding:3px; top:-7px; margin-left:5px; }
     /*.content_menu_bussunies .icon-publicada{ background-image: url(public/admin/icon-home.png); display: inline-block; width: 25px; height: 25px; }*/
    .content_menu_bussunies .img1{ background-position: 0 0; }
     .content_menu_bussunies .img2{ background-position: 60px 0; }
   .content_menu_bussunies .img3{ background-position: 30px 0; }

     .content_menu_bussunies .imagen{ display:inline-block; position: relative; width:60px; margin-top: 0px; }
     .content_menu_bussunies .info{ display:inline-block; position: relative; width:190px; margin-top: 0px; }
	.layout_core_menu_home{ margin-top:25px; display:inline-block; width:380px; margin-left: 25px; vertical-align:top; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
	.layout_core_menu_home .row{  }
	.layout_core_menu_home .row a{ width: 100px; margin-right: 10px; }
	.layout_core_menu_home .row .col{
		width:60px; height:60px; background-color:#ab1607; cursor:pointer; text-align:center; position:relative; background-repeat:no-repeat;
		margin:10px; margin-bottom: 30px; -webkit-border-radius: 50px; -moz-border-radius: 50px; border-radius: 50px;
	}
	.layout_core_menu_home .row .col img{ position:absolute; margin:auto; top:0px; top:15px; left:0px; right:0px; max-height: 35px; }
	.layout_core_menu_home .row .col:hover{ background-color:#4e4e4f; }
	.layout_core_menu_home .row .col span{
		position: absolute; top:65px; margin:auto; left:-10px; right:0px; color:#4e4e4f; width:90px; line-height:1;
		text-transform:uppercase; font-size:10px; font-weight:normal; text-align:center;
	}
	.menu-noticias { display: block; width: 100%; padding: 10px; text-align: center; background-color: #cfcfd1; }
	.menu-noticias img { margin-right: 15px; }
	.menu-noticias a { color: #6f6f70;  height:21px; width:100%; }

	.menu-bolita{ width:100%; display:flex; justify-content:left; flex-wrap:wrap; margin: 0px auto; }
	.menu-bolita .link-bolita{
        width: 49%;
        position: relative;
        font-size: 14px;
        margin: 0px;
        font-weight: 300;
        display: inline-block;
        margin-bottom: 2px;
    }
	.menu-bolita .link-bolita>a{ color:#4e4e4f; }
	.menu-bolita .link-bolita>a:hover{ color:#6f6f70; }
	.menu-bolita .link-bolita .img-bolita{
        margin-top: 10px;
        -webkit-border-radius:50px;
        -moz-border-radius:50px;
        border-radius:50px;
        display:inline-block;
    }
    #link-home-border-1{
        border-bottom: 1px solid #174087;
    }
     #link-home-border-2{
        border-bottom: 1px solid #369443;
    }#link-home-border-3{
        border-bottom: 1px solid #008000;
    }
     .tabulado1 ul li.active-cont-1{
         border-bottom: 2px solid #174087 !important;
     }
     .tabulado1 ul li.active-cont-2{
         border-bottom: 1px solid #369443 !important;
     }
     .tabulado1 ul li.active-cont-3{
         border-bottom: 1px solid #008000 !important;
     }

	.menu-bolita .link-bolita span{ display:inline-block; width:125px; position: relative; vertical-align: top; top: 18px; }
</style>

<!--<div class="tabulado1">
    <ul>
        <li  id="cont_tab_1" class="active-cont-1">
            <a href="javascript:void(0)" onclick="cargarMenu(1)" id="links-home1" class="home homer-2" >
                <label class="menu-1 homer-2" style="color: #174087 ;">INCAUCA</label>
            </a>
        </li>

        <li style="border-bottom: 1px solid #717171;" id="cont_tab_2">
            <a href="javascript:void(0)" onclick="cargarMenu(2)" id="links-home2" class="home homer-1" >
                <label class="menu-1 homer-1">PROVIDENCIA</label>
            </a>
        </li>

        <li style="border-bottom: 1px solid #717171;" id="cont_tab_3">
            <a href="javascript:void(0)" onclick="cargarMenu(3)" id="links-home3" class="home homer-3" >

                <label class="menu-1 homer-3">SUCROAL</label>
            </a>
        </li>
    </ul>
</div>-->
<div class="content_menu_bussunies">
    <div class="menu-bolita">
        <div id="menu_1">
            <?php foreach ($this->navigation as $key => $item): ?>
                <a href="<?php echo $item->getUri(); ?>" class="link-bolita" <?php echo $item->_target == '_blank' ? 'target="_blank"' : ''; ?>>
                    <img class="img-bolita" src="<?php echo $item->icon; ?>"/>
                    <span style="color:#4e4e4f; "><?php echo $item->label; ?></span>
                </a>
            <?php endforeach; ?>

        </div>
        <!--<div id="menu_2" style="display:none">
            <?php /*foreach ($this->providencia as $key => $item) : */?>
                <a href="<?php /*echo $item->getUri(); */?>" class="link-bolita" <?php /*echo $item->_target == '_blank' ? 'target="_blank"' : ''; */?>>
                    <img class="img-bolita" src="<?php /*echo $item->icon; */?>"/>
                    <span style="color:#369443; "><?php /*echo $item->label; */?></span>
                </a>
            <?php /*endforeach; */?>


        </div>
        <div id="menu_3" style="display:none">
            <?php /*foreach ($this->sucroal as $key => $item) : */?>
                <a href="<?php /*echo $item->getUri(); */?>" class="link-bolita" <?php /*echo $item->_target == '_blank' ? 'target="_blank"' : ''; */?>>
                    <img class="img-bolita" src="<?php /*echo $item->icon; */?>"/>
                    <span style="color:#008000; "><?php /*echo $item->label; */?></span>
                </a>
            <?php /*endforeach; */?>


        </div>-->
    </div>
</div>
<script>
    jQuery.noConflict();
    function cargarMenu(idTab){
        document.getElementById( "menu_1").style.display = "none";
        document.getElementById( "menu_2").style.display = "none";
        document.getElementById( "menu_3").style.display = "none";
        document.getElementById( "menu_" + idTab ).style.display = "block";
        //Cantidad
        var labelHome_1=document.getElementsByClassName('menu-1 homer-2');
        var labelHome_2=document.getElementsByClassName('menu-1 homer-1');
        var labelHome_3=document.getElementsByClassName('menu-1 homer-3');
        var tabsHome_1 = document.getElementsByClassName('home homer-2');
        var tabsHome_2 = document.getElementsByClassName('home homer-1');
        var tabsHome_3 = document.getElementsByClassName('home homer-3');
        tabsHome_1=tabsHome_1[0];

        jQuery("#cont_tab_1").removeClass('active-cont-1');
        jQuery("#cont_tab_2").removeClass('active-cont-2');
        jQuery("#cont_tab_3").removeClass('active-cont-3');
        jQuery("#cont_tab_"+idTab).addClass('active-cont-'+idTab);

       if(idTab==1){
           labelHome_1[0].style.color ='#174087';
           labelHome_2[0].style.color ='#717171';
           labelHome_3[0].style.color ='#717171';

//           tabsHome_1.css('border-bottom: 1px solid #174087');
//           tabsHome_2.css('border-bottom: 1px solid #717171');
//           tabsHome_3.css('border-bottom: 1px solid #717171');

       }else if(idTab==2){
           labelHome_1[0].style.color ='#717171';
           labelHome_2[0].style.color ='#369443';
           labelHome_3[0].style.color ='#717171';
//           tabsHome_1.css('border-bottom: 1px solid #717171');
//           tabsHome_2.css('border-bottom: 1px solid #369443');
//           tabsHome_3.css('border-bottom: 1px solid #717171');

       }else {
           labelHome_1[0].style.color ='#717171';
           labelHome_2[0].style.color ='#717171';
           labelHome_3[0].style.color ='#008000';
//           tabsHome_1.css('border-bottom: 1px solid #717171');
//           tabsHome_2.css('border-bottom: 1px solid #717171');
//           tabsHome_3.css('border-bottom: 1px solid #008000');
       }
//        for(var i=0; i < labelHome.length; i++) {
//            labelHome[i].style.color = '#717171';
//            //console.log(labelHome[i]);
//        }
//        for(var i=0; i < tabsHome.length; i++) {
//            //tabsHome[i].style.backgroundColor = '#717171';
//        }
//
//        document.getElementsByClassName('li-' + idTab)[0].style.backgroundColor = '#369443';
//        document.getElementsByClassName('link-' + idTab)[1].style.color = '#369443';
//        //console.log(document.getElementsByClassName('link-' + idTab)[1]);
//
//        //Border Bottom
//        var linksHome = document.getElementsByClassName('links-home');
//
//        for(var i=0; i < linksHome.length; i++) {
//            linksHome[i].style.borderBottom = 'none';
//        }
//        document.getElementsByClassName('link-' + idTab)[0].style.borderBottom = 'thin solid #74b71c';
    }
</script>
