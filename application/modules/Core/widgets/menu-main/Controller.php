<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Core_Widget_MenuMainController extends Engine_Content_Widget_Abstract
{
    public function indexAction()
    {

        $this->view->ver = 0;
        $viewer = Engine_Api::_()->user()->getViewer();
        if ($viewer->getIdentity()) {
            $this->view->ver = 1;
        }


        //prx($this->view->url());

        //prx( Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName() );
        $currentUrl = $this->view->url();
        if ($urlParts = explode("/", $currentUrl)) {
            if (isset($urlParts[1]) && isset($urlParts[2])) {
                $parentUrl = $urlParts[1] . "/" . $urlParts[2];
            } else {
                $parentUrl = array();
            }
        } else {
            $parentUrl = array();
        }

        $this->view->cntrSubMenu = $cntrSubMenu = Engine_Api::_()->getApi('menus', 'core')->getNavigation('core_main', array(), false, $parentUrl);

        //prx($cntrSubMenu->toArray());
        /*$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
        $require_check = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.general.browse', 1);
        if(!$require_check && !$viewer->getIdentity()){
          $navigation->removePage($navigation->findOneBy('route','user_general'));
        }*/
    }

    public function getCacheKey()
    {
        //return Engine_Api::_()->user()->getViewer()->getIdentity();
    }
}