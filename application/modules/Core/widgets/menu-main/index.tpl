<?php if($this->ver == 1){ ?>
    <div class="container menu-main">
        <div class="row no-gutters">
            <?php
            $elSubMenuSup = $this->cntrSubMenu->toArray();
            //echo "<pre>"; print_r($elSubMenuSup); echo "</pre>";
            foreach ($elSubMenuSup as $key => $linkElSubMenuSup) : ?>
                <?php $id = str_replace(" ", "_", $linkElSubMenuSup['class']); ?>
                <a class="col row no-gutters align-self-center menu-main--item <?php if($linkElSubMenuSup['active']) echo 'activo'; ?>" id="<?php echo $id; ?>" href="<?php echo $linkElSubMenuSup['uri']; ?>" >
                    <div class="col align-self-center menu-main--item-content">
                        <?php if($linkElSubMenuSup['icon'] != ''){ ?>
                            <img src="<?php echo $linkElSubMenuSup['icon']; ?>" />
                        <?php } ?>
                        <?php if($linkElSubMenuSup['icon'] == ''){ ?>
                            <span>
                            <?php echo $linkElSubMenuSup['label']; ?>
                        </span>
                        <?php } ?>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
<?php } ?>