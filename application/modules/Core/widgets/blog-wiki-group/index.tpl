<div class="tabulado">
    <ul class="columns is-gapless">
        <li class="column active" id="contenedor_tab_1">
            <a href="javascript:void(0)" onclick="cargarTab(1)" class="links-home link-1">
                <p class="tabs-home li-1"><?php echo $this->conBlog; ?></p>
                <label class="label link-1">Blogs</label>
            </a>
        </li>

        <li class="column" id="contenedor_tab_2">
            <a href="javascript:void(0)" onclick="cargarTab(2)" class="links-home link-2">
                <p class="tabs-home li-2"><?php echo $this->conWiki; ?></p>
                <label class="label link-2">Documentos</label>
            </a>
        </li>

        <li class="column" id="contenedor_tab_3">
            <a href="javascript:void(0)" onclick="cargarTab(3)" class="links-home link-3">
                <p class="tabs-home li-3"><?php echo count($this->groups); ?></p>
                <label class="label link-3">Grupos</label>
            </a>
        </li>
    </ul>
</div>

<div class="contenedores" id="contenedores">

    <div id="cont-1">
        <?php $iB = 0;
        foreach ($this->blogs as $itemBlog) {
            $iB++;
            if ($iB <= 5) { ?>
                <div class="columns columns--item">
                    <div class="column is-two-fifths">
                        <div class="tumb--tabs">
                            <a href="<?php echo $itemBlog->getHref(); ?>">
                                <span style="background-image: url(<?php echo $itemBlog->getPhotoUrl('thumb.profile'); ?>);"></span>
                            </a>
                        </div>
                    </div>
                    <div class="column" id="description">
                        <h3><?php echo $this->htmlLink($itemBlog->getHref(), $itemBlog->getTitle()); ?></h3>

                        <div class="sharing-btn">
                            <i class="icon-publicada img1"></i><?php if ($itemBlog->getLikesCount() > 0) { ?>
                            <span><?php echo $itemBlog->getLikesCount(); ?></span> <?php } ?>

                            <i class="icon-publicada img2"></i> <?php if ($itemBlog->getLikesCount() > 0) { ?>
                            <span><?php echo $itemBlog->getCommentsCount(); ?></span> <?php } ?>

                            <a href="/activity/index/share/type/blog/id/<?php echo $itemBlog->blog_id;?>/format/smoothbox" class="smoothbox">
                                <i class="icon-publicada img3"></i>
                            </a>
                        </div>
                    </div>
                </div>
            <?php }
        } ?>
        <div class="columns">
            <div class="column" id="contenedores--button">
                <a href="/blogs">Ver todos los blogs</a>
            </div>
        </div>
    </div>


    <div id="cont-2" style="display:none">
        <?php $iW = 0;
        foreach ($this->wikis as $documents) {
            $iW++;
            if ($iW <= 5) { ?>
                <div class="columns columns--item">
                    <div class="column is-two-fifths">
                        <div class="tumb--tabs">
                            <a href="<?php echo $documents->getHref(); ?>">
                                <span style="background-image: url(<?php echo $documents->getPhotoUrl('thumb.profile'); ?>);"></span>
                            </a>
                        </div>
                    </div>
                    <div class="column" id="description">
                        <h3><?php echo $this->htmlLink($documents->getHref(), $documents->getTitle()); ?></h3>

                        <div class="sharing-btn">
                            <i class="icon-publicada img1"></i>
                            <span><?php echo $documents->getLikesCount(); ?></span>

                            <i class="icon-publicada img2"></i>
                            <span><?php echo $documents->getCommentsCount(); ?></span>

                            <a href="/activity/index/share/type/ynwiki_page/id/<?php echo $documents->page_id; ?>/format/smoothbox" class="smoothbox">
                                <i class="icon-publicada img3"></i>
                            </a>
                        </div>
                    </div>
                </div>
            <?php }
        } ?>
        <div class="columns">
            <div class="column" id="contenedores--button">
                <a href="/wiki">Ver todos los documentos</a>
            </div>
        </div>
    </div>

    <div id="cont-3" style="display:none">
        <?php $iG = 0;
        foreach ($this->groups as $group) {
            $iG++;
            if ($iG <= 5) { ?>
                <div class="columns columns--item">
                    <div class="column is-two-fifths">
                        <div class="tumb--tabs">
                            <a href="/<?php echo $group['link']; ?>">
                                <span style="background-image: url(<?php echo $group['img']; ?>);"></span>
                            </a>
                        </div>
                    </div>
                    <div class="column" id="description">
                        <h3><a href="/<?php echo $group['link']; ?>"><?php echo $group['title']; ?></a></h3>
                        <p>
                            <?php echo $this->string()->truncate($this->string()->stripTags($group['description']), 90); ?>
                        </p>
                    </div>
                </div>
            <?php }
        } ?>
        <div class="columns">
            <div class="column" id="contenedores--button">
                <a href="/groups">Ver todos los grupos</a>
            </div>
        </div>
    </div>

</div>

<script>
    jQuery.noConflict();

    function cargarTab(idTab) {

        jQuery("#contenedor_tab_1").removeClass('active');
        jQuery("#contenedor_tab_2").removeClass('active');
        jQuery("#contenedor_tab_3").removeClass('active');
        jQuery("#contenedor_tab_" + idTab).addClass('active');


        document.getElementById("cont-1").style.display = "none";
        document.getElementById("cont-2").style.display = "none";
        document.getElementById("cont-3").style.display = "none";
        document.getElementById("cont-" + idTab).style.display = "block";

        //Cantidad

        var tabsHome = document.getElementsByClassName('tabs-home');
        var labelHome = document.getElementsByClassName('label');

        //Border Bottom
        var linksHome = document.getElementsByClassName('links-home');
        for (var i = 0; i < linksHome.length; i++) {
            linksHome[i].style.borderBottom = 'none';
        }
        //document.getElementsByClassName('link-' + idTab)[0].style.borderBottom = 'thin solid #74b71c';
        //console.log(document.getElementsByClassName('link-' + idTab)[0]);
    }
</script>