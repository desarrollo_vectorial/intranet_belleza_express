<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Core_Widget_BlogWikiGroupController extends Engine_Content_Widget_Abstract{
  public function indexAction(){

    $tabBlog  = Engine_Api::_()->getDbtable('blogs',  'blog');
    $tabWiki  = Engine_Api::_()->getDbtable('pages',  'ynwiki');
    $tableBanner = Engine_Api::_()->getDbtable('files', 'storage');

      $select  = $tableBanner->select()->where('type = ?', 'thumb.normal')->where('parent_type = ?','group')->order('creation_date DESC');
      $results = $tableBanner->fetchAll($select)->toArray();


     // echo "<pre>"; print_r($results); echo "</pre>";
      //exit;
    //$tabGroup = Engine_Api::_()->getDbtable('groups', 'group');
   
    // Get blogs
    $selBlog = $tabBlog->select()->where(" search =?", 1)->order('blog_id DESC');

    $this->view->blogs   = $blogs = $tabBlog->fetchAll($selBlog);
    $this->view->conBlog = count($blogs);

    // Get wikis
    $selWiki = $tabWiki->select()->where(" search =?", 1)->order('page_id DESC');
    $this->view->wikis   = $wikis = $tabWiki->fetchAll($selWiki);
    $this->view->conWiki = count($wikis);

    // Get groups
    $tabGroup  = Engine_Db_Table::getDefaultAdapter()->select()->from('engine4_group_groups')->where(" search =?", 1)->order("view_count DESC");
    $groups = $tabGroup->query()->fetchAll();
    $this->view->groups = array();

    foreach ($groups as $key => $group) {
      $title = $this->limpiarString($group['title']);
      foreach ($results as $result){
          if($result['parent_id']==$group['group_id']){
              $img=null;
              $img = $result['storage_path'];
          }

      }
      array_push($this->view->groups, array(
        "group_id"    => $group['group_id'],
        "title"       => $group['title'],
        "description" => $group['description'],
        "link"        => "group/".$group['group_id']."/".$title,
        "img" => $img,
        /*"like"=>$group->getLikesCount(),*/
        /*"comment"=>$group->getCommentsCount(),*/
      ));
    }

    $this->view->conGroup = count($groups);
  }

  //Inicio Funcion que limpia una url
      public function limpiarString($texto){
          $textoLimpio = strtolower($texto);

          //Quitando tildes
          $no_permitidas = array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹","Ñ","ñ");
          $permitidas    = array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E","N","n");
          $caracteres    = array("%", "-", "#", "&", "!", "¡", "(", ")", "=", "@", "|", "*", "^", "{", "}", "[", "]");
          $textoLimpio   = str_replace($no_permitidas, $permitidas, $textoLimpio);
          $textoLimpio   = str_replace($caracteres, "", $textoLimpio);
          $textoLimpio   = str_replace(' ', '_', $textoLimpio);

          return $textoLimpio;
      }
  //Fin Funcion que limpia una url

}