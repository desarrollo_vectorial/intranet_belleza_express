<style type="text/css">
	.layout_core_menu_home_horizontal{ position:inherit; display:inline-block; }
	.menuSubSecciones{ position:relative; display:flex; justify-content:center; /*flex-wrap:wrap;*/ vertical-align:middle; text-transform:uppercase; float:left; }
	.menuSubSecciones a>div{ width:140px; height:40px; }
	.menuSubSecciones .col{ background-color:#454545; cursor:pointer; text-align:center; position:relative; vertical-align:middle; border-right:1px solid #595959; }
	.menuSubSecciones .active{ background-color: #595959; cursor:pointer; text-align:center; position:relative; vertical-align:middle; border-right:1px solid #595959; }
	.menuSubSecciones .col:hover{ background-color: #595959; }
	.menuSubSecciones span{ position:absolute; top:10px; right:0; left:0px; margin:auto; color:#fff; line-height:1; text-align:center; text-transform:uppercase; font-size: 12px !important; padding:5px; font-weight:normal;	}
</style>

<div class="menuSubSecciones">
	<?php foreach ($this->elmenuSubSecciones as $key => $elMenuSubSeccion){ ?>
		<a href="<?php echo $elMenuSubSeccion->getUri(); ?>" >
			<div <?php if($elMenuSubSeccion->getUri() == $_SERVER['REQUEST_URI']){ echo ' class="active"'; }else{ echo ' class="col"'; } ?>>
				<!--<img src="<?php //echo $elMenuSubSeccion->icon; ?>" />-->
				<span style="font-size: 11px;"><?php echo $elMenuSubSeccion->label; ?></span>
			</div>
		</a>
	<?php } ?>
</div>
