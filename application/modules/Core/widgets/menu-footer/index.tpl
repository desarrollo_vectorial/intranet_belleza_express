<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
    <div class="footer_left" style="float: left">

    </div>
    <div id="general-footer" class="footer_center">
        <div class="container">
            <div class="row">
                <div class="col-4 align-self-center">
                    <img src="/public/belleza_express/logo--belleza-express-footer.png" alt="Belleza Express">
                </div>
                <div class="col-8 align-self-center">
                    <div class="row footer-enlaces">
                        <div class="col">
                            <a href="/profile/me">MI PERFIL</a>
                        </div>
                        <div class="col">
                            <a href="/">CONTÁCTENOS</a>
                        </div>
                        <div class="col">
                            <a href="/">ESTADÍSTICAS</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer_right" style="float: right"></div>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110270475-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	
	  gtag('config', 'UA-110270475-1');
	</script>


<!--
<?php echo $this->translate('Copyright &copy;%s', date('Y')) ?>
<?php foreach ($this->navigation as $item):
    $attribs = array_diff_key(array_filter($item->toArray()), array_flip(array(
        'reset_params', 'route', 'module', 'controller', 'action', 'type',
        'visible', 'label', 'href'
    )));
    ?>
    &nbsp;-&nbsp; <?php echo $this->htmlLink($item->getHref(), $this->translate($item->getLabel()), $attribs) ?>
<?php endforeach; ?>

<?php if (1 !== count($this->languageNameList)): ?>
    &nbsp;-&nbsp;
    <form method="post"
          action="<?php echo $this->url(array('controller' => 'utility', 'action' => 'locale'), 'default', true) ?>"
          style="display:inline-block">
        <?php $selectedLanguage = $this->translate()->getLocale() ?>
        <?php echo $this->formSelect('language', $selectedLanguage, array('onchange' => '$(this).getParent(\'form\').submit();'), $this->languageNameList) ?>
        <?php echo $this->formHidden('return', $this->url()) ?>
    </form>
<?php endif; ?>

<?php if (!empty($this->affiliateCode)): ?>
    <div class="affiliate_banner">
        <?php
        echo $this->translate('Powered by %1$s',
            $this->htmlLink('http://www.socialengine.com/?source=v4&aff=' . urlencode($this->affiliateCode),
                $this->translate('SocialEngine Community Software'),
                array('target' => '_blank')))
        ?>
    </div>
<?php endif; ?>
-->
