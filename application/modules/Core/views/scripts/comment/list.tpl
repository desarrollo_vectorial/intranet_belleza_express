<style type="text/css" media="screen">
    .comments{
        width: 100% !important
    }

    .comments .comments_options span{
        color       : #a7a5a5 !important;
        font-size   : 16px;
        font-weight : 400;
        background-color: white !important;
        padding     : 10px;
        border-top  : 1px solid #f0f0f0;
        border-left : 1px solid #f0f0f0;
        border-right: 1px solid #f0f0f0;
        border-right: 2px solid #FFF;
    }

    .comments .comments_options a{
        color       : #979796 !important;
        font-size   : 16px;
        font-weight : 400;
        padding     : 10px;
    }

    .comments ul{
        background-color: white !important;
        padding         : 10px;
        border          : 1px solid #f0f0f0;
    }

    .comments ul li .comments_author_photo{
        background-image: none;
    }

    .comments ul li .comments_author_photo .item_photo_user{
        width : 50px !important;
        margin-left : 20px;
        margin-right: 20px;
    }

    .comments .comments_info{ background-color: white !important; }
    .comments .comments_info .comments_author a{ color: #4c8ac5; font-weight: normal; font-size: 16px; }
    .comments_info .comments_date a{ color: #8b8b8a; }
    .comments .comments_likes{ background-color: white; }
    .comments .comments_likes a{ color: #4c8ac5; font-size: 14px; background-color: white; }
    .comments_info .comments_date .icon-publicada{
        background-image: url(public/admin/split-articulo.png);
        display         : inline-block;
        background-size : cover;
        width           : 20px;
        height          : 20px;
    }
    .comments_info .comments_date .img1{ background-position: 0 0; }
    .comments_info .comments_date .img2{ background-position: 59px 0; }
    .comments_info .comments_date .img3{ background-position: 40px 0; }
    .comments_info .comments_date .img4{ background-position: 100px 0; }
    .comments_info .comments_date .img5{ background-position: 140px 0; }

</style>

<script type="text/javascript">
    var CommentLikesTooltips;
    en4.core.runonce.add(function() {
        // Scroll to comment
        if( window.location.hash != '' ) {
            var hel = $(window.location.hash);
            if( hel ) {
                window.scrollTo(hel);
            }
        }

        // Add hover event to get likes
        $$('.comments_comment_likes').addEvent('mouseover', function(event) {
            var el = $(event.target);
            if( !el.retrieve('tip-loaded', false) ) {
                el.store('tip-loaded', true);
                el.store('tip:title', '<?php echo $this->translate('Loading...') ?>');
                el.store('tip:text', '');
                var id = el.get('id').match(/\d+/)[0];
                // Load the likes
                var url = '<?php echo $this->url(array('module' => 'core', 'controller' => 'comment', 'action' => 'get-likes'), 'default', true) ?>';
                var req = new Request.JSON({
                    url : url,
                    data : {
                        format : 'json',
                        type   : 'core_comment',
                        id     : id
                    }, onComplete : function(responseJSON) {
                        el.store('tip:title', responseJSON.body);
                        el.store('tip:text', '');
                        CommentLikesTooltips.elementEnter(event, el); // Force it to update the text
                    }
                });
                req.send();
            }
        });

        // Add tooltips
        CommentLikesTooltips = new Tips($$('.comments_comment_likes'), {
            fixed : true,
            className : 'comments_comment_likes_tips',
            offset : {
                'x' : 48,
                'y' : 16
            }
        });
        
        // Enable links
        $$('.comments_body').enableLinks();
    });
</script>

    <?php $this->headTranslate(array('Esta seguro que desea eliminar esto?',)); ?>

    <?php if( !$this->page ): ?>
        <div class='comments' id="comments">
    <?php endif; ?>
    
    <div class='comments_options'>
        <span><?php echo $this->translate(array('%s comentario', '%s comentarios', $this->comments->getTotalItemCount()), $this->locale()->toNumber($this->comments->getTotalItemCount())) ?></span>

        <?php if( isset($this->form) ): ?>
            - <a href='javascript:void(0);' onclick="$('comment-form').style.display = '';$('comment-form').body.focus();"><?php echo $this->translate('Publicar') ?></a>
        <?php endif; ?>

        <?php if( $this->viewer()->getIdentity() && $this->canComment ): ?>
            <?php if( $this->subject()->likes()->isLike($this->viewer()) ): ?>
                - <a href="javascript:void(0);" onclick="en4.core.comments.unlike('<?php echo $this->subject()->getType()?>', '<?php echo $this->subject()->getIdentity() ?>')"><?php echo $this->translate('Unlike This') ?></a>
            <?php else: ?>
                - <a href="javascript:void(0);" onclick="en4.core.comments.like('<?php echo $this->subject()->getType()?>', '<?php echo $this->subject()->getIdentity() ?>')"><?php echo $this->translate('Like This') ?></a>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <ul>

        <?php if( $this->likes->getTotalItemCount() > 0 ): // LIKES ------------- ?>
            <li>
            <?php if( $this->viewAllLikes || $this->likes->getTotalItemCount() <= 3 ): ?>
                <?php $this->likes->setItemCountPerPage($this->likes->getTotalItemCount()) ?>
                <div> </div>
                <div class="comments_likes">
                    <?php echo $this->translate(array('%s le gusta esto', '%s le gusta esto', $this->likes->getTotalItemCount()), $this->fluentList($this->subject()->likes()->getAllLikesUsers())) ?>
                </div>
            <?php else: ?>
                <div> </div>
                <div class="comments_likes">
                    <?php echo $this->htmlLink('javascript:void(0);', $this->translate(array('%s personas les gusta esto', '%s personas les gusta esto', $this->likes->getTotalItemCount()), $this->locale()->toNumber($this->likes->getTotalItemCount())), array('onclick' => 'en4.core.comments.showLikes("'.$this->subject()->getType().'", "'.$this->subject()->getIdentity().'");')); ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>


        <?php if( $this->comments->getTotalItemCount() > 0 ): // COMMENTS ------- ?>

            <?php if( $this->page && $this->comments->getCurrentPageNumber() > 1 ): ?>
                <li>
                    <div> </div>
                    <div class="comments_viewall">
                        <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Ver comentarios anteriores'), array('onclick' => 'en4.core.comments.loadComments("'.$this->subject()->getType().'", "'.$this->subject()->getIdentity().'", "'.($this->page - 1).'")')) ?>
                    </div>
                </li>
            <?php endif; ?>

            <?php if( !$this->page && $this->comments->getCurrentPageNumber() < $this->comments->count() ): ?>
                <li>
                    <div> </div>
                    <div class="comments_viewall">
                        <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Ver mas comentarios'), array('onclick' => 'en4.core.comments.loadComments("'.$this->subject()->getType().'", "'.$this->subject()->getIdentity().'", "'.($this->comments->getCurrentPageNumber()).'")')) ?>
                    </div>
                </li>
            <?php endif; ?>


            <?php // Iterate over the comments backwards (or forwards!)
            $comments = $this->comments->getIterator();
            if( $this->page ):
                $i = 0;
                $l = count($comments) - 1;
                $d = 1;
                $e = $l + 1;
            else:
                $i = count($comments) - 1;
                $l = count($comments);
                $d = -1;
                $e = -1;
            endif;

            for( ; $i != $e; $i += $d ):
                $comment    = $comments[$i];
                $poster     = $this->item($comment->poster_type, $comment->poster_id);
                $canDelete  = ( $this->canDelete || $poster->isSelf($this->viewer()) ); ?>

                <li id="comment-<?php echo $comment->comment_id ?>">
                    <div class="comments_author_photo">
                        <?php echo $this->htmlLink($poster->getHref(), $this->itemPhoto($poster, 'thumb.icon', $poster->getTitle())); ?>
                    </div>
                    <div class="comments_info">
                        <span class='comments_author'><?php echo $this->htmlLink($poster->getHref(), $poster->getTitle()); ?></span>
                        <span class="comments_body"><?php echo $this->viewMore($comment->body); ?></span>

                        <div class="comments_date">
                            <i class="icon-publicada img1"></i>
                            <?php echo $this->timestamp($comment->creation_date); ?>

                            <?php if( $canDelete ): ?>
                                <i class="icon-publicada img3"></i>
                                <a href="javascript:void(0);" onclick="en4.core.comments.deleteComment('<?php echo $this->subject()->getType()?>', '<?php echo $this->subject()->getIdentity() ?>', '<?php echo $comment->comment_id ?>')">Eliminar</a>
                            <?php endif; ?>

                            <?php if( $this->canComment ):
                                $isLiked = $comment->likes()->isLike($this->viewer()); ?>
                                <i class="icon-publicada img2"></i>
                                <?php if( !$isLiked ): ?>
                                    <a href="javascript:void(0)" onclick="en4.core.comments.like(<?php echo sprintf("'%s', %d, %d", $this->subject()->getType(), $this->subject()->getIdentity(), $comment->getIdentity()) ?>)">Me gusta</a>
                                <?php else: ?>
                                    <a href="javascript:void(0)" onclick="en4.core.comments.unlike(<?php echo sprintf("'%s', %d, %d", $this->subject()->getType(), $this->subject()->getIdentity(), $comment->getIdentity()) ?>)">Ya no me gusta</a>
                                <?php endif ?>
                            <?php endif ?>

                            <?php if( $comment->likes()->getLikeCount() > 0 ): ?>
                                -
                                <a href="javascript:void(0);" id="comments_comment_likes_<?php echo $comment->comment_id ?>" class="comments_comment_likes" title="<?php echo $this->translate('Loading...') ?>">
                                    <?php echo $this->translate(array('%s les gusta', '%s le gusta', $comment->likes()->getLikeCount()), $this->locale()->toNumber($comment->likes()->getLikeCount())) ?>
                                </a>
                            <?php endif ?>

                        </div>
                    </div>
                </li>
            <?php endfor; ?>

            <?php if( $this->page && $this->comments->getCurrentPageNumber() < $this->comments->count() ): ?>
                <li>
                    <div> </div>
                    <div class="comments_viewall">
                        <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Ver comentarios posteriores'), array('onclick' => 'en4.core.comments.loadComments("'.$this->subject()->getType().'", "'.$this->subject()->getIdentity().'", "'.($this->page + 1).'")')) ?>
                    </div>
                </li>
            <?php endif; ?>

        <?php endif; ?>

    </ul>

    <script type="text/javascript">
    en4.core.runonce.add(function(){
        $($('comment-form').body).autogrow();
        en4.core.comments.attachCreateComment($('comment-form'));
    });
    </script>
    <?php if( isset($this->form) ) echo $this->form->setAttribs(array('id' => 'comment-form', 'style' => 'display:none;'))->render() ?>
    <?php if( !$this->page ): ?>
    </div>
    <?php endif; ?>
