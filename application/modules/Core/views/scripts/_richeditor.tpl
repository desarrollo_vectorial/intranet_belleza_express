<?php $this ->headScript()->appendFile($this->layout()->staticBaseUrl . 'externals/ckeditor_4/ckeditor.js'); ?>

<script type="text/javascript">
  window.onload = function(){
  	
	jQuery('.richtext').each(function() {
	    CKEDITOR.replace( this , {
	      filebrowserBrowseUrl: '/externals/ckfinder/ckfinder.html',
	      filebrowserUploadUrl: '/externals/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
	    });  	
	});
	
  };
</script>