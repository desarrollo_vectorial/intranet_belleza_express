<style>
.search_result{ 
	margin-bottom: 10px;
	width: 100%;
	border-bottom: 1px dotted #DDD;
	padding-bottom: 15px;	
}
.search_result .photo{ display: inline-block; margin-left: 10px; width: 50px;}
.search_result .info{ display: inline-block; margin-left: 15px; width: 800px; top:-10px !important; color: #a7a5a5 !important; }
.search_result .info a{ color: #a7a5a5 !important; }
.search_result .info a:visited{ font-weight:400; }
.search_result .info .stats{ color: #a7a5a5 !important; }
</style>
<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>

<div class="mi-contenedor">

<?php if( empty($this->paginator) ): ?>
  <div class="tip">
    <span>
      <?php echo $this->translate('Please enter a search query.') ?>
    </span>
  </div>
<?php elseif( $this->paginator->getTotalItemCount() <= 0 ): ?>
  <div class="tip">
    <span>
      <?php echo $this->translate('No results were found.') ?>
    </span>
  </div>
<?php else: ?>
  
  <p style="float:right; color:#174087; font-size:16px;"><b><?php echo $this->translate(array('%s result found', '%s results found', $this->paginator->getTotalItemCount()), $this->locale()->toNumber($this->paginator->getTotalItemCount())); ?></b></p>
  <br><br>
  <?php 
  foreach( $this->paginator as $item ){
    $item = $this->item($item->type, $item->id);
    if( !$item ) continue; ?>
    <div class="search_result">
      
      <div class="photo">
        <?php echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, 'thumb.icon')) ?>
      </div>
      <div class="info">

        <div class="title">
          <?php if( '' != $this->query ): ?>
            <?php echo $this->htmlLink($item->getHref(), $this->highlightText($item->getTitle(), $this->query), array('class' => 'search_title')) ?>
          <?php else: ?>
            <?php echo $this->htmlLink($item->getHref(), $item->getTitle(), array('class' => 'search_title')) ?>
          <?php endif; ?>
        </div>

        <div class="stats">
          <?php if( '' != $this->query ): ?>
            <?php echo $this->highlightText($this->viewMore($item->getDescription()), $this->query); ?>
          <?php else: ?>
            <?php echo $this->viewMore($item->getDescription()); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  <?php } ?>

  <br />

  <div>
  <?php echo $this->paginationControl($this->paginator, null, null, array(
    'query' => array(
    'query' => $this->query,
    'type' => $this->type,
  ),
  )); ?>
  </div>

  
  

<?php endif; ?>

</div>