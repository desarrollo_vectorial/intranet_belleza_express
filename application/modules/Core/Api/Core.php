<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Core_Api_Core extends Core_Api_Abstract
{
  /**
   * @var Core_Model_Item_Abstract|mixed The object that represents the subject of the page
   */
  protected $_subject;

  /**
   * Set the object that represents the subject of the page
   *
   * @param Core_Model_Item_Abstract|mixed $subject
   * @return Core_Api_Core
   */
  public function setSubject($subject)
  {
    if( null !== $this->_subject ) {
      throw new Core_Model_Exception("The subject may not be set twice");
    }

    if( !($subject instanceof Core_Model_Item_Abstract) ) {
      throw new Core_Model_Exception("The subject must be an instance of Core_Model_Item_Abstract");
    }

    $this->_subject = $subject;
    return $this;
  }

  /**
   * Get the previously set subject of the page
   *
   * @return Core_Model_Item_Abstract|null
   */
  public function getSubject($type = null)
  {
    if( null === $this->_subject ) {
      throw new Core_Model_Exception("getSubject was called without first setting a subject.  Use hasSubject to check");
    } else if( is_string($type) && $type !== $this->_subject->getType() ) {
      throw new Core_Model_Exception("getSubject was given a type other than the set subject");
    } else if( is_array($type) && !in_array($this->_subject->getType(), $type) ) {
      throw new Core_Model_Exception("getSubject was given a type other than the set subject");
    }

    return $this->_subject;
  }

  /**
   * Checks if a subject has been set
   *
   * @return bool
   */
  public function hasSubject($type = null)
  {
    if( null === $this->_subject ) {
      return false;
    } else if( null === $type ) {
      return true;
    } else {
      return ( $type === $this->_subject->getType() );
    }
  }

  public function clearSubject()
  {
    $this->_subject = null;
    return $this;
  }

  public function getCaptchaOptions(array $params = array())
  {
    $spamSettings = Engine_Api::_()->getApi('settings', 'core')->core_spam;
    if( (empty($spamSettings['recaptchaenabled']) ||
        empty($spamSettings['recaptchapublic']) ||
        empty($spamSettings['recaptchaprivate'])) ) {
      // Image captcha
      return array_merge(array(
        'label' => 'Human Verification',
        'description' => 'Please type the characters you see in the image.',
        'captcha' => 'image',
        'required' => true,
        'captchaOptions' => array(
          'wordLen' => 6,
          'fontSize' => '30',
          'timeout' => 300,
          'imgDir' => APPLICATION_PATH . '/public/temporary/',
          'imgUrl' => Zend_Registry::get('Zend_View')->baseUrl() . '/public/temporary',
          'font' => APPLICATION_PATH . '/application/modules/Core/externals/fonts/arial.ttf',
        ),
      ), $params);
    } else {
      // Recaptcha
      return array_merge(array(
        'label' => 'Human Verification',
        'description' => 'Please type the characters you see in the image.',
        'captcha' => 'reCaptcha',
        'required' => true,
        'captchaOptions' => array(
          'privkey' => $spamSettings['recaptchaprivate'],
          'pubkey' => $spamSettings['recaptchapublic'],
          'theme' => 'white',
          'lang' => Zend_Registry::get('Locale')->getLanguage(),
          'tabindex' => (isset($params['tabindex']) ? $params['tabindex'] : null ),
          'ssl' => constant('_ENGINE_SSL')   // Fixed Captcha does not work well when ssl is enabled on website
        ),
      ), $params);
    }
  }
    
    /** TOKEN */    
    public $tokenKey = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9';
    
    public function generateTokenUser()
    {
        $user = Engine_Api::_()->user()->getViewer();
        $dataUser = $user->toArray();
        
        if( !empty($dataUser['user_id']) )
        {
            $token = array(
                'iat'  => time(),
                'exp'  => time() + (60 * 60 * 24),//expire en 24 horas
                //'jti'  => base64_encode(mcrypt_create_iv(32)),
                //'nbf'  => time() + 10,
                //'iss'  => $_SERVER['SERVER_NAME'],
                'data' => array(
                    'user_id' => $dataUser['user_id'],
                    'email' => $dataUser['email'],
                    'username' => $dataUser['username']
                )
            );
            //se retorna el token
            return $this->JWT_Encode($token, $this->tokenKey);
        }
        
        return null;
    }
    
    public function getUserByToken($jwt=null, $request)
    {
        if(empty($jwt))
        {
            $jwt = $request->getHeader('X-Auth-Token');
        }
        
        $infoToken = $this->JWT_Decode($jwt, $this->tokenKey);
        return $infoToken;
    }
    
    public function validateToken( $request )
    {
        $token = $request->getHeader('X-Auth-Token');
        $usuario = $this->getUserByToken( $token );
        //echo "<pre>"; print_r($usuario); 
        if( is_object($usuario) )
        {
            return true;
        }
        else
        {
            //Error (Token no valido o vencido)
            header('HTTP/1.1 401');
            echo json_encode(array('status' => false, 'message' => 'invalid token', 'data' => $usuario));
            exit;
        }
    }
    
    private function JWT_Encode($token, $key)
    {
        try
        {
            return Firebase_JWT_JWT::encode($token, $key);
        }
        catch(Exception $e)
        {
            return null;
        }
    }
    
    private function JWT_Decode($jwt, $key)
    {
        try
        {
            return Firebase_JWT_JWT::decode($jwt, $key, array('HS256'));
        }
        catch(Exception $e)
        {
            return $e->getMessage();
        }
    }
    
    public function log($controller)
    {
        $usuario = $this->getUserByToken(null, $controller->getRequest());
        
        $log = array();
        $log['created'] = date('Y-m-d H:i:s');
        $log['module'] = $controller->getParam('module');
        $log['controller'] = $controller->getParam('controller');
        $log['action'] = $controller->getParam('action');
        $log['user_id'] = $usuario->data->user_id;
        $log['ip'] = $_SERVER["REMOTE_ADDR"];
        $log['detalle'] = json_encode(file_get_contents('php://input'));
        $log['method'] = $controller->getRequest()->getMethod();
        
        $logTable = Engine_Api::_()->getDbtable('logs', 'core');
        $logRow = $logTable->createRow();
        $logRow->setFromArray( $log );
        $logRow->save();
    }

  public function getDateFormat($date, $format)
  {
    $monthsFull = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
    $monthsShort = array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sept', 'Oct', 'Nov', 'Dic');
    switch ($format) {
      case 'mmm-d':
        $day = date("d", strtotime($date));
        $month = date("n", strtotime($date));
        return $monthsFull[ $month - 1 ] . " " . $day;
        break;

      default:
        # code...
        break;
    }
  }

  /**
  * Libreria para enviar Mails por SMTP
  *
  */
  public function PHPMailer(){
    require_once APPLICATION_PATH_LIB . DS . 'PHPMailer' . DS . 'class.phpmailer.php';
    $mail = new PHPMailer();

    $mail->IsSMTP();
    //$mail->SMTPAuth = true;
    $mail->Host     = "172.26.6.88"; // SMTP a utilizar. Por ej. smtp.elserver.com
    $mail->Username = "comunica@centelsa.com.co"; // Correo completo a utilizar
    $mail->Password = "Julio2017"; // Contraseña
    $mail->Port     = 25; // Puerto a utilizar
    $mail->From     = "comunica@centelsa.com.co"; // Desde donde enviamos (Para mostrar)
    $mail->FromName = "Administrador Intranet";
    $mail->IsHTML(true); // El correo se envía como HTML
    $mail->CharSet  = "utf-8";
    $mail->Sender   = "comunica@centelsa.com.co"; // Desde donde enviamos (Para mostrar)
    //$mail->SMTPDebug = false;
    //$mail->AddCC("stephanie.sanchez@centelsa.com.co");
    //$mail->AddCC("jlopez@vectorial.co"); // Copia
    //$mail->AddBCC("jlopez@vectorial.co"); // Copia oculta

    //$mail->AddAttachment("imagenes/imagen.jpg", "imagen.jpg");

     

    return $mail;

    
  }

  public function PHPMailerSendEmail($subject, $toEmail, $body, $altBody=''){
    $mailSMTP          = $this->PHPMailer();
    $mailSMTP->Subject = $subject; // Este es el titulo del email.
    $mailSMTP->AddAddress($toEmail); // Esta es la dirección a donde enviamos
    $mailSMTP->Body    = $body; // Mensaje a enviar
    $mailSMTP->AltBody = $altBody; // Texto sin html
    $exito = $mailSMTP->send();

    if($exito){ 
      echo "El correo fue enviado correctamente."; 
    }else{ 
      echo "Hubo un problema. Contacta a un administrador.<br>"; 
      echo 'Mailer Error: ' . $mailSMTP->ErrorInfo;
    }


    return $exito;
  }



  public function PHPMailerSendEmailAttach($subject, $toEmail, $body, $altBody='', $attach){
    $mailSMTP          = $this->PHPMailer();
    $mailSMTP->Subject = $subject; // Este es el titulo del email.
    $mailSMTP->AddAddress($toEmail); // Esta es la dirección a donde enviamos
    $mailSMTP->Body    = $body; // Mensaje a enviar
    $mailSMTP->AltBody = $altBody; // Texto sin html
    $mailSMTP->addAttachment($path, $name, $encoding, $type);


    $exito = $mailSMTP->send();

    if($exito){ 
      echo "El correo fue enviado correctamente."; 
    }else{ 
      echo "Hubo un problema. Contacta a un administrador.<br>"; 
      echo 'Mailer Error: ' . $mailSMTP->ErrorInfo;
    }


    return $exito;
  }



  /**
  * Libreria para exportar vistas a PDF
  *
  */
  public function HTML2PDF($orientation = 'P', $format = 'A4', $langue='es', $unicode=true, $encoding='UTF-8', $marges = array(5, 5, 5, 8)) {
    return new HTML2PDF($orientation, $format, $langue, $unicode, $encodin, $marges);
  }


    /**
     * Flash Messages - mostrar mensages Warning y Success
     * level - warn ó success.
     */
    public function setFlashMessage($data)
    {
        $_SESSION['flash_message'] = $data;
    }

    public function getFlashMessage()
    {
        return $_SESSION['flash_message'];
    }

    public function unsetFlashMessage()
    {
        unset($_SESSION['flash_message']);
    }

    public function existsFlashMessage()
    {
        return !empty($_SESSION['flash_message']);
    }







    public function ldapSincro($codigoCIN){
        $ldap_sincro = new Ldap_Conexion();
        return $ldap_sincro->__getUserInfo($codigoCIN);
    }

    public function sincronizarLDAP($username){
        $ldap_sincro = new Ldap_Conexion();
        return $ldap_sincro->sincronizarLDAP($username);
    }

    public function crearUsuarioWithLdap($username){
        $ldap_sincro = new Ldap_Conexion();
        return $ldap_sincro->crearUsuarioWithLdap($username);
    }


}
