<?php
if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
    list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':', base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
} else {
    $_SERVER['PHP_AUTH_USER'] = '';
    $_SERVER['PHP_AUTH_PW'] = '';
}

/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Coremotovalle-portal.vectorial.co
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: default.tpl 10227 2014-05-16 22:43:27Z andres $
 * @author     John
 */
?>
<?php echo $this->doctype()->__toString() ?>
<?php $locale = $this->locale()->getLocale()->__toString();
$orientation = ($this->layout()->orientation == 'right-to-left' ? 'rtl' : 'ltr'); ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $locale ?>" lang="<?php echo $locale ?>"
      dir="<?php echo $orientation ?>">
<head>
    <base href="<?php echo rtrim('//' . $_SERVER['HTTP_HOST'] . $this->baseUrl(), '/') . '/' ?>"/>


    <?php // ALLOW HOOKS INTO META ?>
    <?php echo $this->hooks('onRenderLayoutDefault', $this) ?>


    <?php // TITLE/META ?>
    <?php
    $counter = (int)$this->layout()->counter;
    $staticBaseUrl = $this->layout()->staticBaseUrl;
    $headIncludes = $this->layout()->headIncludes;

    $request = Zend_Controller_Front::getInstance()->getRequest();
    $this->headTitle()->setSeparator(' - ');
    $pageTitleKey = 'pagetitle-' . $request->getModuleName() . '-' . $request->getActionName() . '-' . $request->getControllerName();
    $pageTitle = $this->translate($pageTitleKey);
    if ($pageTitle && $pageTitle != $pageTitleKey) {
        $this->headTitle($pageTitle, Zend_View_Helper_Placeholder_Container_Abstract::PREPEND);
    }
    $this->headTitle($this->translate($this->layout()->siteinfo['title']), Zend_View_Helper_Placeholder_Container_Abstract::PREPEND);
    $this->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset=UTF-8')
        ->appendHttpEquiv('Content-Language', $this->locale()->getLocale()->__toString());

    // Make description and keywords
    $description = '';
    $keywords = '';
    $description .= ' ' . $this->layout()->siteinfo['description'];
    $keywords = $this->layout()->siteinfo['keywords'];

    if ($this->subject() && $this->subject()->getIdentity()) {
        $this->headTitle($this->subject()->getTitle());

        $description .= ' ' . $this->subject()->getDescription();
        // Remove the white space from left and right side
        $keywords = trim($keywords);
        if (!empty($keywords) && (strrpos($keywords, ',') !== (strlen($keywords) - 1))) {
            $keywords .= ',';
        }
        $keywords .= $this->subject()->getKeywords(',');
    }

    $keywords = trim($keywords, ',');

    $this->headMeta()->appendName('description', trim($description));
    $this->headMeta()->appendName('keywords', trim($keywords));

    // Get body identity
    if (isset($this->layout()->siteinfo['identity'])) {
        $identity = $this->layout()->siteinfo['identity'];
    } else {
        $identity = $request->getModuleName() . '-' . $request->getControllerName() . '-' . $request->getActionName();
    }
    ?>
    <?php echo $this->headTitle()->toString() . "\n"; ?>
    <?php echo $this->headMeta()->toString() . "\n"; ?>


    <?php // LINK/STYLES ?>
    <?php
    header('Pragma: no-cache');
    header("Content-Type: text/html;charset=UTF-8");
    /*exit($this->layout);
    $this->headLink(array('rel'=> 'favicon', 'href' => ( isset($this->layout()->favicon) ? $staticBaseUrl . $this->layout()->favicon : '/favicon.ico' ), 'type' => 'image/x-icon'),'PREPEND');*/ ?>

    <link rel="icon" href="/application/modules/Core/layouts/scripts/favicon.ico" type="image/x-icon"/>

    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">

    <?php
    $themes = array();
    if (!empty($this->layout()->themes)) {
        $themes = $this->layout()->themes;
    } else {
        $themes = array('default');
    }
    foreach ($themes as $theme) {
        if (APPLICATION_ENV != 'development') {
            // Estilos Nuevo Formato
            $this->headLink()->prependStylesheet($staticBaseUrl . 'application/themes/' . $theme . '/main-primary.css');
            $this->headLink()->prependStylesheet($staticBaseUrl . 'application/css.php?request=application/themes/' . $theme . '/theme.css');
            $this->headLink()->prependStylesheet($staticBaseUrl . 'application/themes/' . $theme . '/external-scss/bootstrap4/bootstrap-grid.css');
        } else {
            $this->headLink()->prependStylesheet($staticBaseUrl . 'application/themes/' . $theme . '/main-primary.css');
            $this->headLink()->prependStylesheet(rtrim($this->baseUrl(), '/') . '/application/css.php?request=application/themes/' . $theme . '/theme.css');
            $this->headLink()->prependStylesheet($staticBaseUrl . 'application/themes/' . $theme . '/external-scss/bootstrap4/bootstrap-grid.css');
        }
    }

    // Agregar Todos los Estilos CSS
    $this->headLink()
        ->prependStylesheet('https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i')
        ->prependStylesheet('http://fonts.googleapis.com/icon?family=Material+Icons')
        ->prependStylesheet($staticBaseUrl . 'externals/jquery-flex-slider/flexslider.css')
        ->prependStylesheet($staticBaseUrl . 'externals/jquery-fullcalendar/fullcalendar.css')
        ->prependStylesheet($staticBaseUrl . 'externals/jquery-fullcalendar/fullcalendar.print.css', 'print')
        ->prependStylesheet($staticBaseUrl . 'externals/angular-wizard/angular-wizard.min.css')
        ->prependStylesheet($staticBaseUrl . 'externals/livepreview/css/livepreview.css')//Css live preview
        ->prependStylesheet($staticBaseUrl . 'externals/chosen/chosen.css')
        ->prependStylesheet($staticBaseUrl . 'externals/jquery_ui/css/themes/smoothness/jquery-ui.min.css')
        ->prependStylesheet($staticBaseUrl . 'application/modules/Chat/externals/syles/main.css'); //Chat

    // Process
    foreach ($this->headLink()->getContainer() as $dat) {
        if (!empty($dat->href)) {
            if (false === strpos($dat->href, '?')) {
                $dat->href .= '?c=' . $counter;
            } else {
                $dat->href .= '&c=' . $counter;
            }
        }
    }
    ?>
    <?php echo $this->headLink()->toString() . "\n"; ?>
    <?php echo $this->headStyle()->toString() . "\n"; ?>

    <?php // TRANSLATE ?>
    <?php $this->headScript()->prependScript($this->headTranslate()->toString()) ?>

    <?php // SCRIPTS ?>

    <script type="text/javascript">if (window.location.hash == '#_=_') window.location.hash = '';</script>
    <script type="text/javascript">
        <?php echo $this->headScript()->captureStart(Zend_View_Helper_Placeholder_Container_Abstract::PREPEND) ?>

        Date.setServerOffset('<?php echo date('D, j M Y G:i:s O', time()) ?>');

        en4.orientation = '<?php echo $orientation ?>';
        en4.core.environment = '<?php echo APPLICATION_ENV ?>';
        en4.core.language.setLocale('<?php echo $this->locale()->getLocale()->__toString() ?>');
        en4.core.setBaseUrl('<?php echo $this->url(array(), 'default', true) ?>');
        en4.core.staticBaseUrl = '<?php echo $this->escape($staticBaseUrl) ?>';
        en4.core.loader = new Element('img', {src: en4.core.staticBaseUrl + 'application/modules/Core/externals/images/loading.gif'});

        <?php if( $this->subject() ): ?>
        en4.core.subject = {
            type: '<?php echo $this->subject()->getType(); ?>',
            id: <?php echo $this->subject()->getIdentity(); ?>,
            guid: '<?php echo $this->subject()->getGuid(); ?>'
        };
        <?php endif; ?>
        <?php if( $this->viewer()->getIdentity() ): ?>
        en4.user.viewer = {
            type: '<?php echo $this->viewer()->getType(); ?>',
            id: <?php echo $this->viewer()->getIdentity(); ?>,
            guid: '<?php echo $this->viewer()->getGuid(); ?>'
        };
        <?php endif; ?>
        if ( <?php echo(Engine_Api::_()->getDbtable('settings', 'core')->core_dloader_enabled ? 'true' : 'false') ?> ) {
            en4.core.runonce.add(function () {
                en4.core.dloader.attach();
            });
        }

        <?php echo $this->headScript()->captureEnd(Zend_View_Helper_Placeholder_Container_Abstract::PREPEND) ?>
    </script>

    <?php
    $this->headScript()
        // Angular
        ->prependFile($staticBaseUrl . 'externals/angular-wizard/angular-wizard.min.js')
        ->prependFile($staticBaseUrl . 'externals/angular-pagination/dirPagination.js')
        ->prependFile($staticBaseUrl . 'externals/angular-csv-reader/angular-csv-import.min.js')
        ->prependFile($staticBaseUrl . 'externals/angular/angular-locale_es-co.js')
        ->prependFile($staticBaseUrl . 'externals/angular/angular-1-2-0.js')
        // jQuery
        ->prependFile($staticBaseUrl . 'externals/jquery-fullcalendar/lang/es.js')
        ->prependFile($staticBaseUrl . 'externals/jquery-fullcalendar/fullcalendar.min.js')
        ->prependFile($staticBaseUrl . 'externals/jquery-fullcalendar/lib/moment.min.js')
        ->prependFile($staticBaseUrl . 'externals/jquery-flex-slider/jquery.flexslider.js')
        ->prependFile($staticBaseUrl . 'externals/chosen/chosen.jquery.js')
        ->prependFile($staticBaseUrl . 'externals/chosen/docsupport/prism.js')
        ->prependFile($staticBaseUrl . 'externals/jquery_ui/js/jquery-ui-1.10.3.custom.min.js')
        ->prependFile($staticBaseUrl . 'application/modules/Photoviewer/externals/scripts/PhotoViewer.min.js')
        ->prependFile($staticBaseUrl . 'application/modules/Photoviewer/externals/scripts/jquery.mousewheel.js')
        ->prependFile($staticBaseUrl . 'application/modules/Photoviewer/externals/scripts/jquery.jscrollpane.min.js')
        ->prependFile($staticBaseUrl . 'externals/jQuery/jquery-1.11.3.min.js')
        // Mootools
        ->prependFile($staticBaseUrl . 'externals/smoothbox/smoothbox4.js')
        ->prependFile($staticBaseUrl . 'application/modules/User/externals/scripts/core.js')
        ->prependFile($staticBaseUrl . 'application/modules/Core/externals/scripts/core.js')
        ->prependFile($staticBaseUrl . 'externals/chootools/chootools.js')
        ->prependFile($staticBaseUrl . 'externals/mootools/mootools-more-1.4.0.1-full-compat-' . (APPLICATION_ENV == 'development' ? 'nc' : 'yc') . '.js')
        ->prependFile($staticBaseUrl . 'externals/mootools/mootools-core-1.4.5-full-compat-' . (APPLICATION_ENV == 'development' ? 'nc' : 'yc') . '.js')
        // Chat
        ->prependFile($staticBaseUrl . 'application/modules/Chat/externals/scripts/socket.io.js')
        ->prependFile($staticBaseUrl . 'application/modules/Chat/externals/scripts/client.js');


    // Process
    foreach ($this->headScript()->getContainer() as $dat) {
        if (!empty($dat->attributes['src'])) {
            if (false === strpos($dat->attributes['src'], '?')) {
                $dat->attributes['src'] .= '?c=' . $counter;
            } else {
                $dat->attributes['src'] .= '&c=' . $counter;
            }
        }
    }
    ?>
    <?php echo $this->headScript()->toString() . "\n"; ?>

    <?php echo $headIncludes ?>

    <!-- JQUERY NO CONFLICT -->
    <script type="text/javascript">
        jQuery.noConflict();
        $slideshow_time = 3;
        var j = jQuery.noConflict();
        j(window).ready(function () {
            PhotoViewer.options.slideshow_time = $slideshow_time;
            PhotoViewer.bindPhotoViewer();
            window.wpViewerTimer = setInterval(function () {
                PhotoViewer.bindPhotoViewer();
            }, 3000);
        });
    </script>
    <!-- JQUERY NO CONFLICT -->

    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106483224-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments)
        };
        gtag('js', new Date());

        gtag('config', 'UA-106483224-1');
    </script>
</head>
<body id="global_page_<?php echo $identity ?>">
<script type="javascript/text">
    if(DetectIpad()){
      $$('a.album_main_upload').setStyle('display', 'none');
      $$('a.album_quick_upload').setStyle('display', 'none');
      $$('a.icon_photos_new').setStyle('display', 'none');
    }

</script>

<script type="text/javascript">
    /*jQuery(document).ready(function() {
      $(".vistaprevia").livePreview();
    });*/
</script>


<div id="global_header">
    <?php echo $this->content('header') ?>
</div>
<div id='global_wrapper'>
    <div id='global_content'>
        <?php echo $this->content('global-user', 'before') ?>
        <?php echo $this->layout()->content ?>
        <?php //echo $this->content('global-user', 'after') ?>
    </div>
</div>
<div id="global_footer">
    <?php echo $this->content('footer') ?>
</div>
<div id="janrainEngageShare" style="display:none">Share</div>

<?php echo $this->content()->renderWidget('chat.show'); // Widget Chat vs ?>

</body>
</html>
