<?php

return array(
	'package' => array(
		'type' 		  => 'module',
		'name' 		  => 'miscellaneous',
		'version' 	  => '1.0.0',
		'path' 		  => 'application/modules/Miscellaneous',
		'title' 	  => 'Módulos varios',
		'description' => 'Módulos personalizados de cada cliente',
		'author' 	  => 'vectorial@vectorial.co',
		'callback' 	  => array(
			'class'   => 'Engine_Package_Installer_Module',
		),
		'actions' => array(
			0 => 'install',
			1 => 'upgrade',
			2 => 'refresh',
			3 => 'enable',
			4 => 'disable',
		),
		'directories' => array(
			0 => 'application/modules/Miscellaneous',
		),
	),
	'routes' => array(
        'miscellaneous_documentos' => array(
            'route'    => 'documentos/:action',
            'defaults' => array(
                'module' 	 => 'miscellaneous',
                'controller' => 'documents',
                'action' 	 => 'index',
            ),
            'reqs' => array('action' => '(index|upload)'),
        ),	
		'miscellaneous_informesincauca' => array(
			'route'        => 'informesincauca/:action',
			'defaults'     => array(
				'module'     => 'miscellaneous',
				'controller' => 'informesincauca',
				'action'     => 'index',
			),
			'reqs' => array('action' => '(index|delete|upload)', ),
		),
		'miscellaneous_informesmanufactura' => array(
			'route'        => 'informesmanufactura/:action',
			'defaults'     => array(
				'module'     => 'miscellaneous',
				'controller' => 'informesmanufactura',
				'action'     => 'index',
			),
			'reqs' => array('action' => '(index|delete|upload)', ),
		),
        
	),
	'items' => array('miscellaneous_informes'),
);