<?php
$this->headLink()
    ->prependStylesheet($this->layout()->staticBaseUrl . 'application/modules/Miscellaneous/externals/styles/documents.css')
    ->appendStylesheet($this->baseUrl().'/externals/datetimepicker/jquery.datetimepicker.css');

$this ->headScript()
    ->appendFile($this->baseUrl().'/externals/datetimepicker/php-date-formatter.min.js')
    ->appendFile($this->baseUrl().'/externals/datetimepicker/jquery.mousewheel.js')
    ->appendFile($this->baseUrl().'/externals/datetimepicker/jquery.datetimepicker.js');
?>

<div class="generic--breadcrumbs">
    <a href="/">Inicio > </a>
    <?php echo $this->htmlLink(array('module' => 'contents', 'action'=>'view', 'route'=>'contents_index_view', 'page_section' => 'sistemas_de_gestion', 'page_url' => 'calidad-e-inocuidad-incauca'), 'Sistemas de gestión >'); ?>
    <a class="activo" href="<?php echo $this->url(); ?>">Documentos</a>
</div>

<div class="layout_left">
    <div class="side-left-box">
        <div class="side-left-box--titulo">
            <span><strong>Buscar</strong> documentos</span>
        </div>

        <?php if ($this->form): ?>
            <?php echo $this->form->render($this) ?>
        <?php endif ?>
    </div>
    
    <?php if ($this->showAdmin): ?>
	    <div class="side-left-box">
	        <div class="side-left-box--titulo">
	            <span><strong>Actualizar</strong> documentos</span>
	        </div>
			<div class="descripcion-upload">
				Por favor ingrese el archivo a importar con las siguientes condiciones:
				<ul>
					<li>* Debe ser formato Excel 97, extensión XLS</li>
					<li>* Debe contener las siguientes columnas en su orden:Nombre, código, empresa, proceso, tipo de documento y enlace</li>
				</ul>
			</div>	
	        <?php if ($this->form): ?>
	            <?php echo $this->formUpload->render($this) ?>
	        <?php endif ?>
	    </div>  
    <?php endif; ?>  
</div>

<!-- Digita la palabra clave o código y haz clic en el botón Buscar. -->

<div class="layout_middle">
	<div class="documents-table-container">
		<?php if($this->docsSaved): ?>
			<div class="mensaje">Proceso finalizado.<br /><br />Se han ingresado <strong><?php echo $this->docsSaved; ?></strong> documentos.</div>
		<?php endif; ?>
		<?php if($this->documentos): ?>
			<?php if(count($this->documentos) > 0): ?>
			Los únicos documentos vigentes para consulta son los que se encuentran en MiCámara. No descargues procedimientos o formularios porque son susceptibles de actualizarse.
			<table>
				<tr>
					<th>Nombre</th>
					<th>Código</th>
					<th>Empresa</th>
					<th>Proceso</th>
					<th>Tipo</th>
					<th>Enlace</th>
				</tr>
				<tbody>
					<?php foreach($this->documentos as $doc): ?>
					<tr>
						<td><?php echo $doc->nombre; ?></td>
						<td><?php echo $doc->codigo; ?></td>
						<td><?php echo $doc->empresa; ?></td>
						<td><?php echo $doc->proceso; ?></td>
						<td><?php echo $doc->tipo; ?></td>
						<td><a href="<?php echo addslashes($doc->url); ?>" target="_blank">VER</a></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
	
			<?php echo $this->paginationControl($this->documentos, null, null, array(
				'pageAsQuery' => true,
				'query' => $this->formValues,
			)); ?>
			
			<?php else: ?>
				No se encontraron documentos que coincidan con la búsqueda
			<?php endif; ?>
			
			
		
		<?php endif; ?>
	</div>
</div>