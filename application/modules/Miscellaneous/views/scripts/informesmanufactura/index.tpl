<?php
$this->headLink()
    ->prependStylesheet($this->layout()->staticBaseUrl . 'application/modules/Miscellaneous/externals/styles/documents.css')
    ->appendStylesheet($this->baseUrl().'/externals/datetimepicker/jquery.datetimepicker.css');

$this ->headScript()
    ->appendFile($this->baseUrl().'/externals/datetimepicker/php-date-formatter.min.js')
    ->appendFile($this->baseUrl().'/externals/datetimepicker/jquery.mousewheel.js')
    ->appendFile($this->baseUrl().'/externals/datetimepicker/jquery.datetimepicker.js');
?>

<div class="generic--breadcrumbs">
    <a href="/">Inicio > </a>
    <a class="activo" href="<?php echo $this->url(); ?>">Informes de manufactura</a>
</div>

<div class="layout_left">

    <div class="side-left-box">
        <div class="side-left-box--titulo">
            <span><strong>Buscar</strong> informes</span>
        </div>

        <?php if ($this->form): ?>
            <?php echo $this->form->render($this) ?>
        <?php endif ?>
    </div>

    <?php if ($this->showAdmin): ?>

        <div class="side-left-box layout_informe_search">
            <div class="side-left-box--titulo">
                <span><strong>Subir</strong> informe</span>
            </div>

            <?php
            echo $this->form_upload->render($this);
            ?>
        </div>

    <?php endif; ?>

</div>

<div class="layout_middle">
    <div class="documents-table-container">
        <?php if ($this->informes): ?>
            <?php if (count($this->informes) > 0): ?>
                <form method="post"
                      action="<?php echo $this->url(array('action' => 'delete')) . '?' . $this->urlParams ?>">
                    <table>
                        <tr>
                            <?php if ($this->showAdmin): ?>
                                <th></th>
                            <?php endif; ?>
                            <th>Tipo de informe</th>
                            <th>Descripción</th>
                            <th>Fecha</th>
                            <th>Documento</th>
                        </tr>
                        <tbody>
                        <?php foreach ($this->informes as $doc): ?>
                            <?php $file = Engine_Api::_()->getItem('storage_file', $doc->file_id); ?>
                            <tr>
                                <?php if ($this->showAdmin): ?>
                                    <!--<td><?php echo $this->htmlLink(array('action' => 'delete', 'informe_id' => $doc->informe_id, 'route' => 'miscellaneous_informesincauca', 'reset' => true,), $this->translate('delete'), array('class' => 'delete-link smoothbox')); ?></td>-->
                                    <td><input type="checkbox" name="informe_id[]"
                                               value="<?php echo $doc->informe_id; ?>"/></td>
                                <?php endif; ?>
                                <td><?php echo $doc->tipo_informe; ?></td>
                                <td><?php echo $doc->descripcion; ?></td>
                                <td>
                                    <?php $month = $this->translate(date("F", strtotime($doc->fecha))); ?>
                                    <?php echo $month . ' ' . date("d, Y", strtotime($doc->fecha)); ?>
                                </td>
                                <td><a href="<?php echo $file->storage_path ?>" target="_blank">VER</a></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php if ($this->showAdmin): ?>
                    	<input type="submit" name="submit" value="Eliminar informes"/>
                    <?php endif; ?>
                </form>

                <?php echo $this->paginationControl($this->informes, null, null, array(
                    'pageAsQuery' => true,
                    'query' => $this->formValues,
                )); ?>

            <?php else: ?>
                No se encontraron documentos que coincidan con la búsqueda
            <?php endif; ?>


        <?php endif; ?>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $.datetimepicker.setLocale('es');

        $('#fecha_desde').datetimepicker({
            lang: 'es',
            format: 'Y-m-d',
            formatDate: 'Y-m-d',
        });

        $('#fecha_hasta').datetimepicker({
            lang: 'es',
            format: 'Y-m-d',
            formatDate: 'Y-m-d',
        });

        $('#fecha').datetimepicker({
            lang: 'es',
            format: 'Y-m-d',
            formatDate: 'Y-m-d',
        });

    });
</script>
