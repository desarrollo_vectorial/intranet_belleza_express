<?php
class Miscellaneous_Model_Informe extends Core_Model_Item_Abstract
{

    public function guardarInforme($file){
		if ($file instanceof Zend_Form_Element_File) {
			$file   = $file->getFileName();
		} else if ($file instanceof Core_Model_Item_Abstract && !empty($file->file_id)) {
			$tmpRow = Engine_Api::_()->getItem('storage_file', $file->file_id);
			$file   = $tmpRow->name;
		} else if (is_array($file) && !empty($file['tmp_name'])) {
			$file   = $file['tmp_name'];
		} else if (is_string($file) && file_exists($file)) {
			$file   = $file;
		} else {
			throw new Event_Model_Exception('invalid argument passed to guardarInforme');
		}

		$name = basename($file);
		$ext  = pathinfo($name, PATHINFO_EXTENSION);

		$params = array(
			'parent_id'   => $this->informe_id,
			'parent_type' => 'InformesCalidad'
		);

		// Save
		$storage = Engine_Api::_()->storage();

		// Store
		try {
			$iMain = $storage->create($file, $params);
		} catch (Exception $e) {
			// Throw
			if ($e->getCode() == Storage_Model_DbTable_Files::SPACE_LIMIT_REACHED_CODE) {
				//throw new Album_Model_Exception($e->getMessage(), $e->getCode());
			} else {
				throw $e;
			}
		}

		// Update row
		$this->file_id = $iMain->file_id;
		$this->save();

	}

}