<?php

class Miscellaneous_InformesmanufacturaController extends Core_Controller_Action_Standard
{
	private $Tipos = array('Manufactura' => 'Manufactura', 'Producción' => 'Producción', 'Ambientales' => 'Ambientales');
	private $ModuloInforme = 'informes_manufactura';

    public function init() {
        $viewer = Engine_Api::_()->user()->getViewer();
        $this->view->identity = $viewer->getIdentity();
    }

	public function indexAction() {		 
		$viewer = Engine_Api::_()->user()->getViewer();
		$this->view->showAdmin = $viewer->isAdmin();		
				
		$docsTable = Engine_Api::_()->getDbtable('informes', 'miscellaneous');

		/* Construir el formulario del buscador de documentos */
        $this->view->form = $form = new Miscellaneous_Form_Informesmanufactura_Search();
        $form->setAction($this->view->url());

		/* Construir el formulario del buscador de documentos */
		$this->uploadAction();
		
		/* Select values */
		$form->tipo_informe->setMultiOptions( array_merge(array(0 => 'Tipo de informe'), $this->Tipos) );
		
		$params 				= array();
		$params['tipo_informe'] = $this->_getParam('tipo_informe', 0);
		$params['descripcion']  = $this->_getParam('descripcion', '');
		$params['fecha_desde'] 	= $this->_getParam('fecha_desde', '');
		$params['fecha_hasta'] 	= $this->_getParam('fecha_hasta', '');
		
		$this->view->urlParams = http_build_query($params);
		
		//Populate Search Form
		$form -> populate(array(
			'tipo_informe'  => $params['tipo_informe'],
			'descripcion'   => $params['descripcion'],
			'fecha_desde' 	=> $params['fecha_desde'],
			'fecha_hasta' 	=> $params['fecha_hasta']
		));
		
		/* Buscar los documentos que coincidan con la búsqueda */
		$this->view->formValues = $form->getValues();
    	$values = $form->getValues();

		if(isset($values['page'])) {
			unset($values['page']);	
		}								
						
		if(array_filter($values)) {
		    $this->view->informes = $paginator = $this->_getInformesPaginator($values);
		    $paginator->setItemCountPerPage(20);
		    $paginator->setCurrentPageNumber($this->_getParam('page', 1));							
		} else {
			$this->view->informes = null;
		}
				
	}	

	public function deleteAction(){
		
		if ($this->getRequest()->isPost() && $informes_id = $this->getRequest()->getPost('informe_id')) {
			
			$viewer = Engine_Api::_() -> user() -> getViewer();
			
			
			foreach($informes_id as $informe_id) {
				$informe = Engine_Api::_() -> getItem('miscellaneous_informes', $this -> _getParam('informe_id'));
		
				if (!$informe)
				{
					return $this -> _helper -> requireAuth -> forward();
				}
		
				if($file = Engine_Api::_() -> getItem('storage_file', $informe -> file_id)) {
					$file -> delete();			
				}
				$informe -> delete();				
			}
		}
		
		$params 				= array();
		$params['tipo_informe'] = $this->_getParam('tipo_informe', '');
		$params['descripcion']  = $this->_getParam('descripcion', '');
		$params['fecha_desde'] 	= $this->_getParam('fecha_desde', '');
		$params['fecha_hasta'] 	= $this->_getParam('fecha_hasta', '');
		
		$urlParams = http_build_query($params);
		
		return $this->_redirect('informesmanufactura/index?'.$urlParams);
	}

	public function uploadAction() {
        $this->view->form_upload = $form_upload = new Miscellaneous_Form_Informesmanufactura_Upload();
        $form_upload->setAction('/informesmanufactura/upload');
		$form_upload->tipo_informe->setMultiOptions( array_merge(array(0 => 'Tipo de informe'), $this->Tipos) );

        if ($this->getRequest()->isPost()) {
            if ($form_upload->isValid($this->getRequest()->getPost())) {
            	
				if(!$_FILES['document_file']['error']) {

					$table = Engine_Api::_()->getDbtable('informes', 'miscellaneous');
					$db 	= $table -> getAdapter();
						
					$db -> beginTransaction();
					try{
		                $values = array_merge($form_upload->getValues(), array('modulo' => $this->ModuloInforme));

		                $data = $table->createRow();
		                $data->setFromArray($values);
						$data->save();
						
		      			if (!empty($values['document_file'])) {
		                  $data->guardarInforme($form_upload->document_file);
		      			}
						
						$db->commit();
												
      				}catch(Exception $e){
						$db->rollBack();
						throw ($e);
					}
					$params = $form_upload->getValues();
					$params['descripcion'] = $params['descripcion'];
					$params['fecha_desde'] = $params['fecha'];
					$params['fecha_hasta'] = $params['fecha'];
					
					$urlParams = http_build_query($params);
					
					return $this->_redirect('informesmanufactura/index?'.$urlParams);
				}
            }
        }		
	}

	private function _getInformesPaginator($params = array()) {
		$paginator = Zend_Paginator::factory($this -> _getInformesSelect($params));
		
		if (!empty($params['page'])) {
			$paginator -> setCurrentPageNumber($params['page']);
		}
		if (!empty($params['limit'])) {
			$paginator -> setItemCountPerPage($params['limit']);
		}
		return $paginator;
	}

	private function _getInformesSelect($values = array()) {
		$table = Engine_Api::_() -> getDbtable('informes', 'miscellaneous');

		$where = array();

		if (isset($values['tipo_informe']) && $values['tipo_informe']) {
			$where[] = "tipo_informe ='" . $values['tipo_informe'] . "'";
		}
		if (isset($values['descripcion']) && $values['descripcion']) {
			$where[] = "descripcion LIKE '%" . $values['descripcion'] . "%'";
		}
		if (isset($values['fecha_desde']) && $values['fecha_desde'] && isset($values['fecha_hasta']) && $values['fecha_hasta']) {
			$where[] = "fecha BETWEEN '".$values['fecha_desde']."' AND '".$values['fecha_hasta']."'";
		} else {
			if (isset($values['fecha_desde']) && $values['fecha_desde']) {
				$where[] = "fecha >='" . $values['fecha_desde'] . "'";
			} else {
				if (isset($values['fecha_hasta']) && $values['fecha_hasta']) {
					$where[] = "fecha <='" . $values['fecha_hasta'] . "'";
				}
			}
		}
		
		$where[] = "modulo ='" . $this->ModuloInforme . "'";

		$conditionString = implode(" AND ", $where);

		$select = $table -> select() -> where($conditionString) -> order('fecha desc');
		return $select;
	}	
}

