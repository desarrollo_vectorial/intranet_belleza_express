<?php

class Miscellaneous_DocumentsController extends Core_Controller_Action_Standard
{
    public function init() {
        $viewer = Engine_Api::_()->user()->getViewer();
        $this->view->identity = $viewer->getIdentity();
    }

		
	public function indexAction() {
		 $viewer = Engine_Api::_()->user()->getViewer();
		 $this->view->showAdmin = $viewer->isAdmin();		
				
		$docsTable = Engine_Api::_()->getDbtable('documentos', 'miscellaneous');
		
		$Empresas = array();
		$tiposDocumento = array();
		$Procesos = array();
		
		/* Obtener los tipos de documento */
		$select = $docsTable->select()->group('tipo');
		$res_tiposDocumento = $docsTable->fetchAll($select);
		foreach($res_tiposDocumento as $tipo) {
			$tiposDocumento[$tipo->tipo] = $tipo->tipo;
		}
		
		/* Obtener las empresas */
		$select = $docsTable->select()->group('empresa');
		$res_Empresas = $docsTable->fetchAll($select);
		foreach($res_Empresas as $empresa) {
			$Empresas[$empresa->empresa] = $empresa->empresa;
		}
		
		/* Obtener los procesos */
		$select = $docsTable->select()->group('proceso');
		$res_Procesos = $docsTable->fetchAll($select);
		foreach($res_Procesos as $proceso) {
			$Procesos[$proceso->proceso] = $proceso->proceso;
		}

		/* Construir el formulario del buscador de documentos */
        $this->view->form = $form = new Miscellaneous_Form_Documents_Search();
		
		/* Construir el formulario para cargar los documentos */
		$this->uploadAction();
		$this->view->docsSaved = $this->_getParam('docs_saved', 0); //Contiene la información de los documentos importados después de ejecutarse uploadAction
		
        $form->setAction($this->view->url());
		
		$form->empresa->setMultiOptions( array_merge(array(0 => 'Empresa'), $Empresas) );
		$form->tipo->setMultiOptions( array_merge(array(0 => 'Tipo'), $tiposDocumento) );
		$form->proceso->setMultiOptions( array_merge(array(0 => 'Proceso'), $Procesos) );
		
		$params 			= array();
		$params['nombre'] 	= $this->_getParam('nombre', '');
		$params['codigo'] 	= $this->_getParam('codigo', '');
		$params['empresa'] 	= $this->_getParam('empresa', 0);
		$params['tipo'] 	= $this->_getParam('tipo', 0);
		$params['proceso'] 	= $this->_getParam('proceso', 0);
		
		//Populate Search Form
		$form -> populate(array(
			'nombre' 	=> $params['nombre'],
			'codigo' 	=> $params['codigo'],
			'empresa'  	=> $params['empresa'],
			'tipo'  	=> $params['tipo'],
			'proceso'  	=> $params['proceso'],
			'page'   	=> $this->_getParam('page', 1)
		));
		
		
		/* Buscar los documentos que coincidan con la búsqueda */
		$this->view->formValues = $form->getValues();
    	$values = $form->getValues();

		if(isset($values['page'])) {
			unset($values['page']);	
		}								
						
		if(array_filter($values)) {
		    $this->view->documentos = $paginator = self::_getDocumentosPaginator($values);
		    $paginator->setItemCountPerPage(20);
		    $paginator->setCurrentPageNumber($this->_getParam('page', 1));							
		} else {
			$this->view->documentos = null;
		}

		//$this->_helper->content->setEnabled();
	}
	
	public function uploadAction() {
        $this->view->formUpload = $form_upload = new Miscellaneous_Form_Documents_Upload();
        $form_upload->setAction($this->view->url());

        if ($this->getRequest()->isPost()) {
            if ($form_upload->isValid($this->getRequest()->getPost())) {
            	
				if(!$_FILES['document_file']['error']) {
					require_once 'application' . DS . 'modules' . DS . 'Miscellaneous' . DS. 'externals' . DS . 'excel_reader2.php';		
					$data = new Spreadsheet_Excel_Reader($_FILES['document_file']['tmp_name']);
					
					$documents = $data->sheets[0]['cells'];
					unset($documents[1]);
			
					$table = Engine_Api::_()->getDbtable('documentos', 'miscellaneous');
					$db 	= $table -> getAdapter();
					
					$db->query('TRUNCATE TABLE engine4_miscellaneous_documentos');
					
					$docsSaved = 0;
					foreach($documents as $documentRow) {
						
						$db -> beginTransaction();
						try{
							$data = $table -> createRow();
							$data->nombre = utf8_encode($documentRow[1]);
							$data->codigo = utf8_encode($documentRow[2]);
							$data->empresa = utf8_encode($documentRow[3]);
							$data->proceso = utf8_encode($documentRow[4]);
							$data->tipo = utf8_encode($documentRow[5]);
							$data->url = utf8_encode($documentRow[6]);
							$data -> save();
							$db -> commit();
							
							$docsSaved++;
							
						}catch(Exception $e){
							$db -> rollBack();
							throw ($e);
						}
						
					}	
					return $this->_redirect('documentos?docs_saved='.$docsSaved);
				}
            }
        }
	}

	private function _getDocumentosPaginator($params = array()) {
		$paginator = Zend_Paginator::factory($this -> _getDocumentosSelect($params));
		if (!empty($params['page'])) {
			$paginator -> setCurrentPageNumber($params['page']);
		}
		if (!empty($params['limit'])) {
			$paginator -> setItemCountPerPage($params['limit']);
		}
		return $paginator;
	}

	private function _getDocumentosSelect($values = array()) {
		$table = Engine_Api::_() -> getDbtable('documentos', 'miscellaneous');

		$where = array();

		if (isset($values['nombre']) && $values['nombre']) {
			$where[] = "nombre like '%" . $values['nombre'] . "%'";
		}
		if (isset($values['codigo']) && $values['codigo']) {
			$where[] = "codigo = '" . $values['codigo'] . "'";
		}
		if (isset($values['empresa']) && $values['empresa']) {
			$where[] = "empresa ='" . $values['empresa'] . "'";
		}
		if (isset($values['tipo']) && $values['tipo']) {
			$where[] = "tipo ='" . $values['tipo'] . "'";
		}
		if (isset($values['proceso']) && $values['proceso']) {
			$where[] = "proceso ='" . $values['proceso'] . "'";
		}

		$conditionString = implode(" AND ", $where);

		$select = $table -> select() -> where($conditionString) -> order('nombre');
		return $select;
	}
}

