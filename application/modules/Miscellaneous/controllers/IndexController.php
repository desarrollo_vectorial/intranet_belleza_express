<?php

class Miscellaneous_IndexController extends Core_Controller_Action_Standard
{
    public function init() {
        $viewer = Engine_Api::_()->user()->getViewer();
        $this->view->identity = $viewer->getIdentity();
    }
}

