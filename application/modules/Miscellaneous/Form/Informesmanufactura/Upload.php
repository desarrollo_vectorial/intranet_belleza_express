<?php
class Miscellaneous_Form_Informesmanufactura_Upload extends Engine_Form
{
    public $_error = array();

    public function init()
    {
        $this
            ->setAttribs(array(
                'class' => 'side-form',
        ));

        $this->addElement('Select', 'tipo_informe', array(
            'label' => 'Tipo',
            'multiOptions' => array('' => 'NINGUNO'),
            'allowEmpty' => false,
            'required' => true,
        ));
        $this->addElement('Text', 'descripcion', array(
            'label' => 'Descripción',
            'placeholder' => 'Descripción',
            'allowEmpty' => false,
            'required' => true,
        ));		
        $this->addElement('Text', 'fecha', array(
            'label' => 'Fecha',
            'placeholder' => 'Fecha',
            'allowEmpty' => false,
            'required' => true,
        ));
        $this->addElement('File', 'document_file', array(
            'label' => 'Informe',
            'allowEmpty' => false,
            'required' => true,            
        ));		
         $this->addElement('Button', 'submit', array(
            'label' => 'Guardar',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array('ViewHelper'),
        ));
    }
}