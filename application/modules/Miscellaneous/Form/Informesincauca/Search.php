<?php
class Miscellaneous_Form_Informesincauca_Search extends Engine_Form
{
    public $_error = array();

    public function init()
    {
        $this->setTitle('')
            ->setDescription('')
            ->setAttrib('name', 'search_docs');

	    $this
	      ->setAttribs(array(
	        'class' => 'side-form',
	      ))
	       ->setMethod('GET')
	            ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array('page' => null)));
	
        $this->addElement('Select', 'tipo_informe', array(
            'label' => 'Tipo',
            'multiOptions' => array('' => 'NINGUNO'),
            'allowEmpty' => true,
            'required' => false,
        ));
        $this->addElement('Select', 'empresa', array(
            'label' => 'Empresa',
            'multiOptions' => array('' => 'NINGUNO'),
            'allowEmpty' => true,
            'required' => false,
        ));
        $this->addElement('Text', 'fecha_desde', array(
            'label' => 'Desde',
            'placeholder' => 'Desde',
            'allowEmpty' => true,
            'required' => false,
        ));
        $this->addElement('Text', 'fecha_hasta', array(
            'label' => 'Hasta',
            'placeholder' => 'Hasta',
            'allowEmpty' => true,
            'required' => false,
        ));
         $this->addElement('Button', 'submit', array(
            'label' => 'Buscar',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array('ViewHelper'),
        ));

    }
}