<?php
class Miscellaneous_Form_Documents_Search extends Engine_Form
{
    public $_error = array();

    public function init()
    {
        $this->setTitle('')
            ->setDescription('')
            ->setAttrib('name', 'search_docs');

	    $this
	      ->setAttribs(array(
	        'class' => 'side-form',
	      ))
	       ->setMethod('GET')
	            ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array('page' => null)));
	
        $this->addElement('Text', 'nombre', array(
            'label' => 'Palabra clave',
            'placeholder' => 'Palabra clave',
            'allowEmpty' => true,
            'required' => false,
            'maxlength ' => 255,
        ));
        $this->addElement('Text', 'codigo', array(
            'label' => 'Código',
            'placeholder' => 'Código',
            'allowEmpty' => true,
            'required' => false,
            'maxlength ' => 255,
        ));
        $this->addElement('Select', 'empresa', array(
            'label' => 'Empresa',
            'multiOptions' => array('' => 'NINGUNO'),
            'allowEmpty' => true,
            'required' => false,
        ));
        $this->addElement('Select', 'proceso', array(
            'label' => 'Proceso',
            'multiOptions' => array('' => 'NINGUNO'),
            'allowEmpty' => true,
            'required' => false,
        ));
        $this->addElement('Select', 'tipo', array(
            'label' => 'Tipo de documento',
            'multiOptions' => array('' => 'NINGUNO'),
            'allowEmpty' => true,
            'required' => false,
        ));	
         $this->addElement('Button', 'submit', array(
            'label' => 'Buscar',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array('ViewHelper'),
        ));

    }
}