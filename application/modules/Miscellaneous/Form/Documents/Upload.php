<?php
class Miscellaneous_Form_Documents_Upload extends Engine_Form
{
    public $_error = array();

    public function init()
    {
        $this
            ->setAttribs(array(
                'class' => 'side-form',
        ));
    	
        $this->addElement('File', 'document_file', array(
            'label' => 'Archivo',
            'allowEmpty' => false,
            'required' => true,            
        ));

         $this->addElement('Button', 'submit', array(
            'label' => 'Guardar',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array('ViewHelper'),
        ));

        $this->addElement('Cancel', 'cancel', array(
            'label' => 'Cancelar',
            'link' => true,
            'prependText' => ' or ',
            'decorators' => array('ViewHelper'),
        ));
    }
}