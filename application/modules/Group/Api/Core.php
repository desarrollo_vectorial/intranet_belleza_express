<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Core.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Group_Api_Core extends Core_Api_Abstract{
  
  //Función que obtiene las estadisticas de los grupos por categoria
	public function getEstadisticas(){
    $tabCatego =  Engine_Api::_()->getDbtable('categories', 'group');
    $tabVideo  =  Engine_Api::_()->getDbtable('groups', 'group');
    $selecEst  =  $tabCatego
                  ->select()
                  ->setIntegrityCheck(false)
                  ->from(array("c" => $tabCatego->info('name')), array("category_id", "title"))
                  ->join(array("v" => $tabVideo->info('name')), 'c.category_id = v.category_id', array())
                  ->group('v.category_id')
                  ->limit(4);
    $listado   = $tabCatego->fetchAll($selecEst);
    $elListaTop = array();

    // Guardando el listado del top 4 del categorias de los videos
    foreach ($listado as $key => $estadistica) {
      $cant = $this->getCountCategory($estadistica->category_id);

      array_push($elListaTop, array(
          'category_id' => $estadistica->category_id,
          'title' 			=> $estadistica->title,
          'count_group' => $cant
        )
      );
    }
    return $elListaTop;
  }

  //Función que cuenta la cantidad de grupos ligados a la categoria
  public function getCountCategory($category_id) {
    $table  = Engine_Api::_()->getDbTable('categories', 'group');
    $rName = $table->info('name');
    $select = $table->select()
                    ->from($rName)
                    ->where($rName.'.category_id = ?', $category_id);
    $row = $table->fetchAll($select);
    return count($row);
  }


}
