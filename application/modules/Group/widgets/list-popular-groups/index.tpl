<style type="text/css" media="screen">
  .layout_group_list_popular_groups{ background-color:#ab1607 !important; margin-bottom:15px; margin-top:10px; padding-bottom: 10px; }
  .layout_group_list_popular_groups .titulo{ padding:20px; display:block; position:relative; text-align:right; }
  .layout_group_list_popular_groups .titulo .most{ color:white; display:inline-block; top:10px; font-size:34px; line-height:30px; width:230px; vertical-align: middle; }
  .layout_group_list_popular_groups  ul{ background-color: none !important; display: block; padding-bottom: 20px; }
  .layout_group_list_popular_groups  ul>li{ margin-bottom: 10px; }
  .layout_group_list_popular_groups  ul>li .photo{ display: inline-block; margin-left: 10px; width: 50px; }
  .layout_group_list_popular_groups  ul>li .info{ display: inline-block; margin-left: 15px; width: 200px; top:-10px !important; color: white !important; }
  .layout_group_list_popular_groups  ul>li .info a{ color: white !important; }
  .layout_group_list_popular_groups  ul>li .info .stats{ color: white !important; }
  .layout_group_list_popular_groups .icon-publicada{
      background-image: url(public/admin/split-articulo.png);
      display         : inline-block;
      background-size : cover; 
      width           : 20px;
      height          : 20px;
    }
    .layout_group_list_popular_groups .img1{ background-position: 0 0; }
    .layout_group_list_popular_groups .img2{ background-position: 100px 0; }
    .layout_group_list_popular_groups .img3{ background-position: 20px 0; }
</style>

<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>

<div class="titulo">
  <div class="most">
    <p style="font-size: 30px !important;">Grupos</p>
    <b>Populares</b>
  </div> 
</div>

<ul>
  <?php foreach( $this->paginator as $item ): ?>
    <li>
      <div class="photo">
        <?php echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, 'thumb.icon'), array('class' => 'thumb')) ?>
      </div>
      <div class="info">
        <div class="title">
          <?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?>
        </div>
        <div class="stats">
          <?php echo $this->timestamp(strtotime($item->creation_date)) ?> - 
          
          <?php if( $this->popularType == 'view' ): ?>
            <?php echo $this->translate(array('%s view', '%s views', $item->view_count), $this->locale()->toNumber($item->view_count)) ?>
          <?php else /*if( $this->popularType == 'member' )*/: ?>
            <?php echo $this->translate(array('%s member', '%s members', $item->member_count), $this->locale()->toNumber($item->member_count)) ?>
          <?php endif; ?><br>

          <?php echo $this->translate($this->htmlLink($item->getOwner()->getHref(), $item->getOwner()->getTitle())) ?>
          
        </div>
      </div>
      <?php /*
        $desc = trim($this->string()->truncate($this->string()->stripTags($item->description), 300));
        if( !empty($desc) ): ?>
        <div class="description">
          <?php echo $desc ?>
        </div>
      <?php endif; */ ?>
    </li>
  <?php endforeach; ?>
</ul>
