<!--
<div class="headline">
  <h2>
    <?php /*echo $this->translate('Groups') ?>
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render(); */
    ?>
  </div>
</div>-->

<style>
  .layout_group_browse_menu{
  display      : block;
  position     : relative;
  width        : 90%;
  margin-top   : 20px;
}
.layout_group_browse_menu .row{
  display         : flex;
  justify-content : center;
  width           : 900px;
  margin-left     : -27px;
}
.layout_group_browse_menu .row .col{
  width       : 280px;
  height      : 60px;
  cursor      : pointer;
  text-align  : center;
  margin      : 0 auto;
  position    : relative;
  border-right: 1px solid #8b8b8a;
}
.layout_group_browse_menu .row .col img{
  position      : relative;
  display       : inline-block;
  margin        : 0 auto;
  padding-right : 10px; 
  top           : 5px;
  width         : 40px;
  height        : 40px;
  padding-left  : 10px;
  -webkit-filter: grayscale(100%); 
  filter        : grayscale(100%);
}
.layout_group_browse_menu .row .col span{
  position        : relative;
  display         : inline-block;
  margin          : 0 auto;
  color           : #a6a6a6;
  width           : 120px;
  line-height     : 1;
  text-align      : left;
  padding         : 5px;
  font-size       : 22px;
  font-weight     : normal;
}
.layout_group_browse_menu .activo img, .activo span{
  color: #ab1607 !important;
  -webkit-filter: grayscale(0%) !important; 
  filter        : grayscale(0%) !important;
}
.layout_group_browse_menu .activo{
  background-color: white;
  border-top      : 0.01em solid #d1d1d1;
  border-left     : 0.01em solid #d1d1d1;
  border-right    : 0.01em solid #d1d1d1;
}
</style>


<div class="row">
  <?php 
  foreach ($this->navigation as $key => $item) : ?>
      <a href="groups/<?php echo $item['action']; ?>" <?php if($item['active']) echo 'class="activo"'; ?>>
        <div class="col">
          <img src="<?php echo $item['icon']; ?>" />
          <span><?php echo $item['label']; ?></span>
        </div>
      </a>
  <?php endforeach; ?>
</div>