<style>
.layout_group_browse_search{ background-color: #a7a5a5 !important; margin-bottom: 15px; }
.layout_group_browse_search .titulo{ padding: 20px; display: block; position: relative; text-align: right; }
.layout_group_browse_search .titulo .most{ color: white; display: inline-block; top: 10px; font-size: 36px; line-height: 30px; width: 170px; vertical-align: middle; }
.layout_group_browse_search .titulo .most:before{ content: ''; display: block; position: absolute; background-image: url(../application/modules/Article/externals/images/search-white.png); width:50px; height: 50px; left: 40px; top: 30px; }
.layout_group_browse_search div#filter_form, .filters, form#filter_form div.form-elements{ background-color: #a7a5a5 !important; margin-bottom: 15px; margin-top: 10px; position: relative; }
.layout_group_browse_search .form-elements .form_titulo{ width: 250px; height: 25px; position: relative; display: block; margin: 0 auto; color: white; border-left: 2px solid #ab1607; border-bottom: none; border-top: none; border-right: none; background-color: #949494; }
.layout_group_browse_search .form-elements .form_titulo::-webkit-input-placeholder{ color:white; }
.layout_group_browse_search .form-elements .form_titulo:-moz-placeholder{ color:white; }
.layout_group_browse_search .form-elements .form_titulo::-moz-placeholder{ color:white; }
.layout_group_browse_search .form-elements .form_titulo:-ms-input-placeholder{ color:white; }
.layout_group_browse_search .form-elements .form_select{ width: 260px; height: 30px; position: relative; display: block; margin: 0 auto; color: white; border-left: 2px solid #ab1607; border-bottom: none; border-top: none; border-right: none; background-color: #949494; -webkit-border-radius: none; -moz-border-radius: none; border-radius: none; }
.layout_group_browse_search .form-elements .form_boton{ position: relative; display: block; width: 90px; height: 25px; margin: 0 auto; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }
.layout_group_browse_search .form-elements label{ color: white; padding: 10px; padding-top: 30px; }
</style>

<div class="titulo">
	<div class="most">
		<p style="font-size: 30px !important; color:#ab1607;">Buscar</p>
		<b>Grupo</b>
	</div>	
</div>

<?php if( $this->form ): ?>
  <?php echo $this->form->render($this) ?>
<?php endif ?>
