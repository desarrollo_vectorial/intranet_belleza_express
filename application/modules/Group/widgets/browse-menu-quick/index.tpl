<style type="text/css" media="screen">
	.layout_group_browse_menu_quick{ background-color:#ab1607 !important; padding:25px !important; margin-top:-15px; }
	.layout_group_browse_menu_quick .quicklinks { background: none !important; padding:0 !important; }
	.layout_group_browse_menu_quick .quicklinks ul{ padding:0 !important; }
	.layout_group_browse_menu_quick .quicklinks ul li{ padding:0 !important; height: 70px; margin-left: 20px; }
	.layout_group_browse_menu_quick .quicklinks ul li a.buttonlink{
		padding       : 0px !important; 
		font-size     : 22px; 
		background    : none !important;
	    display       : inline-block !important;
	    margin-left   : 20px;
	    width         : 150px;
	    color         : white;
	    text-transform: capitalize;
	}
	.layout_group_browse_menu_quick .quicklinks li:before{
		content 		:'';
		background-image: url(../public/admin/grupo.png);
		display         : inline-block;
		background-size : cover; 
		width           : 50px;
		height          : 40px;
	}
</style>

<?php if( count($this->quickNavigation) > 0 ): ?>
  <div class="quicklinks">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->quickNavigation)
        ->render();
    ?>
  </div>
<?php endif; ?>
