<style> 
  .mi-contenedor-options{ background-color:#717171; padding:20px; border: none !important; } 
  .mi-contenedor-options a{ color: white; } 
</style>

<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author		 John
 */
?>

<div class="mi-contenedor-options">
  <div id='profile_options'>
    <?php // This is rendered by application/modules/core/views/scripts/_navIcons.tpl
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->setPartial(array('_navIcons.tpl', 'core'))
        ->render()
    ?>
  </div>
</div>