<style type="text/css" media="screen">
	.layout_group_profile_photo { background-color: #616161 !important; display : block; position: relative; margin: 0 auto; text-align: center; border: none !important; }
	.layout_group_profile_photo img{ padding-top: 10px; padding-bottom: 10px; margin: 0 auto; width: 300px; }
	img.item_photo_group { border: none; max-width:275px !important; }
</style>


<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author		 John
 */
?>

<?php echo $this->itemPhoto($this->subject(), 'thumb.profile') ?>
