<style type="text/css" media="screen">
  .layout_group_list_recent_groups{ background-color:#ab1607 !important; margin-bottom:15px; margin-top:10px; padding-bottom: 10px; }
  .layout_group_list_recent_groups .titulo{ padding:20px; display:block; position:relative; text-align:right; }
  .layout_group_list_recent_groups .titulo .most{ color:white; display:inline-block; top:10px; font-size:34px; line-height:30px; width:220px; vertical-align: middle; }
  .layout_group_list_recent_groups  ul{ background-color: none !important; display: block; padding-bottom: 20px; }
  .layout_group_list_recent_groups  ul>li{ margin-bottom: 10px; }
  .layout_group_list_recent_groups  ul>li .photo{ display: inline-block; margin-left: 10px; width: 50px; }
  .layout_group_list_recent_groups  ul>li .info{ display: inline-block; margin-left: 15px; width: 200px; top:-10px !important; color: white !important; }
  .layout_group_list_recent_groups  ul>li .info a{ color: white !important; }
  .layout_group_list_recent_groups  ul>li .info .stats{ color: white !important; }
</style>

<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>

<div class="titulo">
  <div class="most">
    <p style="font-size: 30px !important;">Grupos</p>
    <b>Recientes</b>
  </div> 
</div>

<ul>
  <?php foreach( $this->paginator as $item ): ?>
    <li>
      <div class="photo">
        <?php echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, 'thumb.icon'), array('class' => 'thumb photo')) ?>
      </div>
      <div class="info">
        <div class="title">
          <h3><?php echo $this->htmlLink($item->getHref(), $item->getTitle()); ?></h3>
        </div>
        <div class="stats">
          <?php echo $this->timestamp(strtotime($item->{$this->recentCol})) ?>
          <!-- - <?php /* echo $this->translate('led by %1$s', $this->htmlLink($item->getOwner()->getHref(), $item->getOwner()->getTitle())) */ ?> -->
        </div>
      </div>      
    </li>
  <?php endforeach; ?>
</ul>
