<?php

class Group_Widget_EstadisticasController extends Engine_Content_Widget_Abstract{

    public function indexAction(){
		$this->view->estadisticas = Engine_Api::_()->group()->getEstadisticas();
    }
}
