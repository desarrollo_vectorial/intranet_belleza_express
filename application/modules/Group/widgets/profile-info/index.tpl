<style> 
  .mi-contenedor-info{ background-color:#FFFFFF; padding:20px; border:0.01em solid #d1d1d1; } 
  .mi-contenedor-info h2{ color: #9f291d; }
  .mi-contenedor-info .group_stats_title span{
    border-bottom:0.01em solid #d1d1d1; color:#848484; margin-bottom: 10px !important;
  }
  .mi-contenedor-info .group_stats_info, .group_stats_staff{
    font-size:10px !important;
  }
</style>

<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9987 2013-03-20 00:58:10Z john $
 * @author		 John
 */
?>

<div class="mi-contenedor-info">
  <h2><?php echo $this->translate("Group Info") ?></h2>

  <ul>
    <li class="group_stats_title">
      <span>
        <?php echo $this->group->getTitle() ?>
      </span>
      <?php if( !empty($this->group->category_id) && ($category = $this->group->getCategory()) instanceof Core_Model_Item_Abstract && !empty($category->title)): ?>
        <?php echo $this->htmlLink(array('route' => 'group_general', 'action' => 'browse', 'category_id' => $this->group->category_id), $this->translate((string)$category->title)) ?>
      <?php endif; ?>
    </li>
    <?php if( '' !== ($description = $this->group->description) ): ?>
      <li class="group_stats_description">
        <?php echo $this->viewMore($description, null, null, null, true) ?>
      </li>
    <?php endif; ?>
    <li class="group_stats_staff">
      <ul>
        <?php foreach( $this->staff as $info ): ?>
          <li>
            <?php echo $info['user']->__toString() ?>
            <?php if( $this->group->isOwner($info['user']) ): ?>
              (<?php echo ( !empty($info['membership']) && $info['membership']->title ? $info['membership']->title : $this->translate('owner') ) ?>)
            <?php else: ?>
              (<?php echo ( !empty($info['membership']) && $info['membership']->title ? $info['membership']->title : $this->translate('officer') ) ?>)
            <?php endif; ?>
          </li>
        <?php endforeach; ?>
      </ul>
    </li>
    <li class="group_stats_info">
      <ul>
        <li><?php echo $this->translate(array('%s total view', '%s total views', $this->group->view_count), $this->locale()->toNumber($this->group->view_count)) ?></li>
        <li><?php echo $this->translate(array('%s total member', '%s total members', $this->group->member_count), $this->locale()->toNumber($this->group->member_count)) ?></li>
        <li><?php echo $this->translate('Last updated %s', $this->timestamp($this->group->modified_date)) ?></li>
      </ul>
    </li>
  </ul>

  <script type="text/javascript">
    $$('.core_main_group').getParent().addClass('active');
  </script>
</div>