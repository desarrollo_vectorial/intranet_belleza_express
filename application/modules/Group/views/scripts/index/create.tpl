<style> .mi-contenedor{ background-color:white; padding:30px; width:900px; border:0.01em solid #d1d1d1; } </style>
<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: create.tpl 9987 2013-03-20 00:58:10Z john $
 * @author	   John
 */
?>


<div class="mi-contenedor">
	<?php echo $this->form->render($this) ?>
	<script type="text/javascript">
	  $$('.core_main_group').getParent().addClass('active');
	</script>
</div>