<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: browse.tpl 9785 2012-09-25 08:34:18Z pamela $
 * @author	   John
 */
?>

<style>
  .mi-contenedor .groups_browse .groups_info .ver-mas a{
    background-color: #ab1607; 
    display: block;
    float: right;
    width: 100px;
    color: white;
    font-size : 16px;
    text-align: center;
    padding   : 5px;
    -webkit-border-radius: 3px;
    -moz-border-radius   : 3px;
    border-radius        : 3px;
  }
  .mi-contenedor .groups_browse .groups_photo{ width : 250px; height: 150px; }
  .mi-contenedor .groups_browse .groups_photo img{ width : 240px !important; height: 150px !important; max-width: 240px !important; max-height:150px !important; }
  .mis-tabs #main_tabs>li{ height: 50px !important; width: 125px !important; }
</style>

<div class="mi-contenedor">
  

  <?php if( count($this->paginator) > 0 ): ?>

  <ul class='groups_browse'>
    <?php foreach( $this->paginator as $group ): ?>
      <li>
        <div class="groups_photo">
          <?php echo $this->htmlLink($group->getHref(), $this->itemPhoto($group, '')) ?>
        </div>
        <div class="groups_options">
        </div>
        <div class="groups_info">
          <div class="groups_title">
            <h3><?php echo $this->htmlLink($group->getHref(), $group->getTitle()) ?></h3>
          </div>
          <div class="groups_members">
            <?php echo $this->translate(array('%s member', '%s members', $group->membership()->getMemberCount()),$this->locale()->toNumber($group->membership()->getMemberCount())) ?>
            <?php echo $this->translate('led by');?> <?php echo $this->htmlLink($group->getOwner()->getHref(), $group->getOwner()->getTitle()) ?>
          </div>
          <div class="groups_desc">
            <?php echo $this->viewMore($group->getDescription()) ?>
          </div>

          <div class="ver-mas"><?php echo $this->htmlLink($group->getHref(), 'Ver más'); ?></div>
        </div>
      </li>
    <?php endforeach; ?>
  </ul>

  <?php elseif( preg_match("/category_id=/", $_SERVER['REQUEST_URI'] )): ?>
  <div class="tip">
      <span>
      <?php echo $this->translate('_group_Nobody has created a group with that criteria.');?>
      <?php if( $this->canCreate ): ?>
        <?php echo $this->translate('_group_Why don\'t you %1$screate one%2$s?',
          '<a href="'.$this->url(array('action' => 'create'), 'group_general').'">', '</a>') ?>
      <?php endif; ?>
      </span>
  </div> 
      
  <?php else: ?>
    <div class="tip">
      <span>
      <?php echo $this->translate('_group_There are no groups yet.') ?>
      <?php if( $this->canCreate): ?>
        <?php echo $this->translate('_group_Why don\'t you %1$screate one%2$s?',
          '<a href="'.$this->url(array('action' => 'create'), 'group_general').'">', '</a>') ?>
      <?php endif; ?>
      </span>
    </div>
  <?php endif; ?>

  <?php echo $this->paginationControl($this->paginator, null, null, array(
    'query' => $this->formValues
  )); ?>


</div>