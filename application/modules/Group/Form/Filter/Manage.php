<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Manage.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Group_Form_Filter_Manage extends Engine_Form
{
  public function init()
  {
    $this->clearDecorators()
      ->addDecorators(array(
        'FormElements',
        array('HtmlTag', array('tag' => 'dl')),
        'Form',
      ))
      ->setMethod('get')
      //->setAttrib('onchange', 'this.submit()')
      ->setAttrib('class', 'filters form-elements')
      ;

    $this->addElement('Text', 'text', array(
      'placeholder' => 'Buscar Grupos:',
      'decorators' => array(
        'ViewHelper',
        array('HtmlTag', array('tag' => 'dd')),
        array('Label', array('tag' => 'dt', 'placement' => 'PREPEND'))
      ),
      'onchange' => '$(this).getParent("form").submit();',
      'class'       => 'form_titulo'
    ));

    $this->addElement('Select', 'view', array(
      //'label' => 'View:',
      'multiOptions' => array(
        '' => 'All My Groups',
        '2' => 'Only Groups I Lead',
      ),
      'decorators' => array(
        'ViewHelper',
        array('HtmlTag', array('tag' => 'dd')),
        array('Label', array('tag' => 'dt', 'placement' => 'PREPEND'))
      ),
      'onchange' => '$(this).getParent("form").submit();',
      'class'    => 'form_select'
    ));

    $this->addElement('Button', 'submit', array(
      'order'  => 104,
      'label'  => 'Buscar',
      'type'   => 'submit',
      'class'  => 'form_boton',
      'ignore' => true,
    ));

  }
}