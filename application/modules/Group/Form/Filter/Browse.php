<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Browse.php 9826 2012-11-21 02:56:50Z richard $
 * @author     John
 */

/**
 * @category   Application_Extensions
 * @package    Group
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Group_Form_Filter_Browse extends Engine_Form
{
  public function init()
  {
    $this->clearDecorators()
      ->addDecorators(array(
        'FormElements',
        array('HtmlTag', array('tag' => 'dl')),
        'Form',
      ))
      ->setMethod('get')
      ->setAttrib('class', 'filters form-elements')
      //->setAttrib('onchange', 'this.submit()')
      ;
    
    $this->addElement('Text', 'search_text', array(
      'placeholder' => 'Buscar Grupos:',
      'class'       => 'form_titulo'
    ));
    
    $this->addElement('Select', 'category_id', array(
      //'label' => 'Category:',
      'multiOptions' => array(
        '' => 'All Categories',
      ),
      'decorators' => array(
        'ViewHelper',
        array('HtmlTag', array('tag' => 'dd')),
        array('Label', array('tag' => 'dt', 'placement' => 'PREPEND'))
      ),
      'onchange' => '$(this).getParent("form").submit();',
      'class'    => 'form_select'
    ));

    $this->addElement('Select', 'view', array(
      //'label' => 'View:',
      'multiOptions' => array(
        '' => 'Everyone\'s Groups',
        '1' => 'Only My Friends\' Groups',
      ),
      'decorators' => array(
        'ViewHelper',
        array('HtmlTag', array('tag' => 'dd')),
        array('Label', array('tag' => 'dt', 'placement' => 'PREPEND'))
      ),
      'onchange' => '$(this).getParent("form").submit();',
      'class'    => 'form_select'
    ));

    $this->addElement('Select', 'order', array(
      //'label' => 'List By:',
      'multiOptions' => array(
        'creation_date DESC' => 'Recently Created',
        'member_count DESC' => 'Most Popular',
      ),
      'decorators' => array(
        'ViewHelper',
        array('HtmlTag', array('tag' => 'dd')),
        array('Label', array('tag' => 'dt', 'placement' => 'PREPEND'))
      ),
      'value' => 'creation_date DESC',
      'onchange' => '$(this).getParent("form").submit();',
      'class'    => 'form_select'
    ));

    $this->addElement('Button', 'submit', array(
      'order'  => 104,
      'label'  => 'Buscar',
      'type'   => 'submit',
      'class'  => 'form_boton',
      'ignore' => true,
    ));
  }
}