<?php
class Estadistica_IndexController extends Core_Controller_Action_Standard{

    public function init(){
        // Validar que el usuario debe estar logueado y ser administrador para poder entrar al administrador del módulo
        // Aplica para todas las vistas de este controlador
        $viewer = Engine_Api::_()->user()->getViewer();
        if ($viewer->isModSuperAdmin() == 0) {
        $this->_redirect('/');
        }
    }

    public function indexAction(){
        $this->_helper->content->setEnabled();

        //Sacando el reporte
        if ($this->getRequest()->isPost()) {

            if ($_POST['fecha_inicio'] != '' && $_POST['limite_inscripcion'] != '') {

                // Estadisticas por Usuario
                try {
                    $usuarios_activos = Engine_Api::_()->estadistica()->getTotalUsersEnabled();
                    $startDate        = $_POST['fecha_inicio'] . ' 00:00:00';
                    $endDate          = $_POST['limite_inscripcion'] . ' 23:59:59';
                    $estPorUsuario    = Engine_Api::_()->estadistica()->getEstadisticasPorUsuario($startDate, $endDate);
                    $titulos          = array(
                        'nombre'        => 'Nombre',
                        'comentarios'   => 'Comentarios',
                        'likes'         => 'Me Gusta',
                        'publicaciones' => 'Publicaciones',
                        'compartidos'   => 'Compartidos',
                        'album_photo'   => 'Fotos subidas',
                        'total'         => 'Total de interacciones',
                    );

                    // aqui le decimos al navegador que vamos a mandar un archivo del tipo CSV
                    header("Content-Description: File Transfer");
                    header("Content-Type: application/force-download");
                    header("Content-Disposition: attachment; filename=estadisticas_" . date("Y-m-d") . ".csv");

                    $handle = fopen('php://temp', 'r+');
                    $part   = '';
                    fputcsv($handle, $titulos, ';', '"');                    
                    foreach ($estPorUsuario as $lines) {
                        $array_line = array();
                        $total      = 0;
                        foreach ($lines as $line) {
                            foreach ($line as $key => $lin) {
                                if ($key != 'nombre') {
                                    $total = $total + $lin;
                                }
                                $array_line[] = $lin;
                            }
                        }
                        $array_line[] = $total;
                        fputcsv($handle, $array_line, ';', '"');
                    }
                    fputcsv($handle, array('Total de usuarios activos', $usuarios_activos), ';', '"');
                    rewind($handle);
                    while (!feof($handle)) {
                        $contents .= fread($handle, 8192);
                    }
                    fclose($handle);
                    echo $contents;
                } catch (Exception $ex) {
                    print_r($ex->getMessage());
                }
                exit();
            } else {
                // Flash messages
                Engine_Api::_()->core()->setFlashMessage(array(
                    'title'   => 'Datos incompletos',
                    'message' => 'La fecha de inicio y la final debe ir diligenciada',
                    'level'   => 'error',
                ));
            }
        }
        // Revisar si hay Mensajes de confirmacion por mostrar al usuario
        if (Engine_Api::_()->core()->existsFlashMessage()) {
            $this->view->popupMessage = Engine_Api::_()->core()->getFlashMessage();
            Engine_Api::_()->core()->unsetFlashMessage();
        };
    }

    public function publicacionesAction(){
        $this->_helper->content->setEnabled();

        //Sacando el reporte
        if ($this->getRequest()->isPost()) {
            if ($_POST['fecha_inicio'] != '' && $_POST['limite_inscripcion'] != '') {
                // Estadisticas por Usuario
                try {
                    $startDate       = $_POST['fecha_inicio'] . ' 00:00:00';
                    $endDate         = $_POST['limite_inscripcion'] . ' 23:59:59';
                    $totNews         = Engine_Api::_()->estadistica()->getTotalNews($startDate, $endDate);
                    $totBlogs        = Engine_Api::_()->estadistica()->getTotalBlogs($startDate, $endDate);
                    $totWikis        = Engine_Api::_()->estadistica()->getTotalWikis($startDate, $endDate);
                    $totVideos       = Engine_Api::_()->estadistica()->getTotalVideos($startDate, $endDate);
                    $totClasificados = Engine_Api::_()->estadistica()->getTotalClasificados($startDate, $endDate);
                    $totEventos      = Engine_Api::_()->estadistica()->getTotalEventos($startDate, $endDate);
                    $totAlbums       = Engine_Api::_()->estadistica()->getTotalAlbumes($startDate, $endDate);
                    //$totGrupos       = Engine_Api::_()->estadistica()->getTotalGrupos($startDate, $endDate);

                    // aqui le decimos al navegador que vamos a mandar un archivo del tipo CSV
                    header("Content-Description: File Transfer");
                    header("Content-Type: application/force-download");
                    header("Content-Disposition: attachment; filename=estadisticas_publicacion" . date("Y-m-d") . ".csv");

                    $handle = fopen('php://temp', 'r+');
                    $part   = '';
                    fputcsv($handle, array('publicaciones', 'Cantidad'),';', '"');
                    fputcsv($handle, array('Noticias',      $totNews),  ';', '"');
                    fputcsv($handle, array('Blogs',         $totBlogs), ';', '"');
                    fputcsv($handle, array('Wikis',         $totWikis), ';', '"');
                    fputcsv($handle, array('Videos',        $totVideos),';', '"');
                    fputcsv($handle, array('Clasificados',  $totClasificados),';', '"');
                    fputcsv($handle, array('Eventos',       $totEventos),';', '"');
                    fputcsv($handle, array('Albumes',       $totAlbums),';', '"');
                    //fputcsv($handle, array('Grupos',        $totGrupos),';', '"');

                    rewind($handle);
                    while (!feof($handle)) {
                        $contents .= fread($handle, 8192);
                    }
                    fclose($handle);
                    echo $contents;
                } catch (Exception $ex) {
                    print_r($ex->getMessage());
                }
                exit();
            } else {
                // Flash messages
                Engine_Api::_()->core()->setFlashMessage(array(
                    'title'   => 'Datos incompletos',
                    'message' => 'La fecha de inicio y la final debe ir diligenciada',
                    'level'   => 'error',
                ));
            }
        }

        // Revisar si hay Mensajes de confirmacion por mostrar al usuario
        if (Engine_Api::_()->core()->existsFlashMessage()) {
            $this->view->popupMessage = Engine_Api::_()->core()->getFlashMessage();
            Engine_Api::_()->core()->unsetFlashMessage();
        };

    }

    public function datospublicacionAction(){
        $this->_helper->content->setEnabled();

        //Sacando el reporte
        if ($this->getRequest()->isPost()) {
            if ($_POST['fecha_inicio'] != '' && $_POST['limite_inscripcion'] != '' && $_POST['selTipo'] != '') {
                // Estadisticas por Usuario
                try {
                    $startDate   = $_POST['fecha_inicio'] . ' 00:00:00';
                    $endDate     = $_POST['limite_inscripcion'] . ' 23:59:59';
                    $tipoReporte = $_POST['selTipo'];


                    //Sacando las noticias
                    if($tipoReporte == 1){
                        $startDate        = $_POST['fecha_inicio'] . ' 00:00:00';
                        $endDate          = $_POST['limite_inscripcion'] . ' 23:59:59';
                        $getNoticias      = Engine_Api::_()->estadistica()->getNoticias($startDate, $endDate);
                        $titulos          = array(
                            'noticias'    => 'Noticias',
                            'comentarios' => 'Comentarios',
                            'likes'       => 'Me Gusta',
                            'compartir'   => 'Compartir',
                        );


                        // aqui le decimos al navegador que vamos a mandar un archivo del tipo CSV
                        header("Content-Description: File Transfer");
                        header("Content-Type: application/force-download");
                        header("Content-Disposition: attachment; filename=estadisticas_noticias_" . date("Y-m-d") . ".csv");

                        $handle = fopen('php://temp', 'r+');
                        $part   = '';
                        fputcsv($handle, $titulos, ';', '"');

                        foreach ($getNoticias as $key => $new) {
                            fputcsv($handle, array($new['noticias'], $new['comentarios']['comentarios'], $new['likes']['likes'], $new['share']['share']),';', '"');
                        }

                        rewind($handle);
                        while (!feof($handle)) {
                            $contents .= fread($handle, 8192);
                        }
                        fclose($handle);
                        echo $contents;


                    //Sacando los blogs
                    }elseif($tipoReporte == 2){

                        $startDate     = $_POST['fecha_inicio'] . ' 00:00:00';
                        $endDate       = $_POST['limite_inscripcion'] . ' 23:59:59';
                        $getBlogs      = Engine_Api::_()->estadistica()->getBlogs($startDate, $endDate);
                        $titulos          = array(
                            'blog'        => 'Blogs',
                            'comentarios' => 'Comentarios',
                            'likes'       => 'Me Gusta',
                            'compartir'   => 'Compartir'
                        );

                        // aqui le decimos al navegador que vamos a mandar un archivo del tipo CSV
                        header("Content-Description: File Transfer");
                        header("Content-Type: application/force-download");
                        header("Content-Disposition: attachment; filename=estadisticas_blogs_" . date("Y-m-d") . ".csv");

                        $handle = fopen('php://temp', 'r+');
                        $part   = '';
                        fputcsv($handle, $titulos, ';', '"');



                        foreach ($getBlogs as $key => $blog) {
                            fputcsv($handle, array($blog['blog'], $blog['comentarios']['comentarios'], $blog['likes']['likes'], $blog['share']['share']),';', '"');
                        }

                        rewind($handle);
                        while (!feof($handle)) {
                            $contents .= fread($handle, 8192);
                        }
                        fclose($handle);
                        echo $contents;

                    //Sacando las wikis
                    }elseif($tipoReporte == 3){

                        $startDate     = $_POST['fecha_inicio'] . ' 00:00:00';
                        $endDate       = $_POST['limite_inscripcion'] . ' 23:59:59';
                        $getWikis      = Engine_Api::_()->estadistica()->getWikis($startDate, $endDate);
                        $titulos          = array(
                            'wiki'        => 'Wikis',
                            'comentarios' => 'Comentarios',
                            'likes'       => 'Me Gusta',
                            'compartir'   => 'Compartir'
                        );

                        // aqui le decimos al navegador que vamos a mandar un archivo del tipo CSV
                        header("Content-Description: File Transfer");
                        header("Content-Type: application/force-download");
                        header("Content-Disposition: attachment; filename=estadisticas_wikis_" . date("Y-m-d") . ".csv");

                        $handle = fopen('php://temp', 'r+');
                        $part   = '';
                        fputcsv($handle, $titulos, ';', '"');



                        foreach ($getWikis as $key => $wiki) {
                            fputcsv($handle, array($wiki['wiki'], $wiki['comentarios']['comentarios'], $wiki['likes']['likes'], $wiki['share']['share']),';', '"');
                        }

                        rewind($handle);
                        while (!feof($handle)) {
                            $contents .= fread($handle, 8192);
                        }
                        fclose($handle);
                        echo $contents;

                    //Sacando los videos
                    }elseif($tipoReporte == 4){

                        $startDate     = $_POST['fecha_inicio'] . ' 00:00:00';
                        $endDate       = $_POST['limite_inscripcion'] . ' 23:59:59';
                        $getVideo      = Engine_Api::_()->estadistica()->getVideo($startDate, $endDate);
                        $titulos          = array(
                            'video'       => 'videos',
                            'comentarios' => 'Comentarios',
                            'likes'       => 'Me Gusta',
                            'compartir'   => 'Compartir'
                        );

                        // aqui le decimos al navegador que vamos a mandar un archivo del tipo CSV
                        header("Content-Description: File Transfer");
                        header("Content-Type: application/force-download");
                        header("Content-Disposition: attachment; filename=estadisticas_videos_" . date("Y-m-d") . ".csv");

                        $handle = fopen('php://temp', 'r+');
                        $part   = '';
                        fputcsv($handle, $titulos, ';', '"');



                        foreach ($getVideo as $key => $video) {
                            fputcsv($handle, array($video['video'], $video['comentarios']['comentarios'], $video['likes']['likes'], $video['share']['share']),';', '"');
                        }

                        rewind($handle);
                        while (!feof($handle)) {
                            $contents .= fread($handle, 8192);
                        }
                        fclose($handle);
                        echo $contents;

                    //Sacando los Clasificados
                    }elseif($tipoReporte == 5){
                        
                        $startDate        = $_POST['fecha_inicio'] . ' 00:00:00';
                        $endDate          = $_POST['limite_inscripcion'] . ' 23:59:59';
                        $getClasificado   = Engine_Api::_()->estadistica()->getClasificado($startDate, $endDate);
                        $titulos          = array(
                            'classified'  => 'classified',
                            'comentarios' => 'Comentarios',
                            'likes'       => 'Me Gusta',
                            'compartir'   => 'Compartir'
                        );

                        // aqui le decimos al navegador que vamos a mandar un archivo del tipo CSV
                        header("Content-Description: File Transfer");
                        header("Content-Type: application/force-download");
                        header("Content-Disposition: attachment; filename=estadisticas_clasificado_" . date("Y-m-d") . ".csv");

                        $handle = fopen('php://temp', 'r+');
                        $part   = '';
                        fputcsv($handle, $titulos, ';', '"');



                        foreach ($getClasificado as $key => $clasificado) {
                            fputcsv($handle, array($clasificado['classified'], $clasificado['comentarios']['comentarios'], $clasificado['likes']['likes'], $clasificado['share']['share']),';', '"');
                        }

                        rewind($handle);
                        while (!feof($handle)) {
                            $contents .= fread($handle, 8192);
                        }
                        fclose($handle);
                        echo $contents;


                    //Sacando los Eventos
                    }elseif($tipoReporte == 6){
                        
                        $startDate  = $_POST['fecha_inicio'] . ' 00:00:00';
                        $endDate    = $_POST['limite_inscripcion'] . ' 23:59:59';
                        $getEventos = Engine_Api::_()->estadistica()->getEvento($startDate, $endDate);
                        $titulos    = array(
                            'event'       => 'events',
                            'comentarios' => 'Comentarios',
                            'likes'       => 'Me Gusta',
                            'compartir'   => 'Compartir'
                        );

                        // aqui le decimos al navegador que vamos a mandar un archivo del tipo CSV
                        header("Content-Description: File Transfer");
                        header("Content-Type: application/force-download");
                        header("Content-Disposition: attachment; filename=estadisticas_eventos_" . date("Y-m-d") . ".csv");

                        $handle = fopen('php://temp', 'r+');
                        $part   = '';
                        fputcsv($handle, $titulos, ';', '"');



                        foreach ($getEventos as $key => $evento) {
                            fputcsv($handle, array($evento['event'], $evento['comentarios']['comentarios'], $evento['likes']['likes'], $evento['share']['share']),';', '"');
                        }

                        rewind($handle);
                        while (!feof($handle)) {
                            $contents .= fread($handle, 8192);
                        }
                        fclose($handle);
                        echo $contents;

                    //Sacando los Albumes
                    }elseif($tipoReporte == 7){
                        
                        $startDate  = $_POST['fecha_inicio'] . ' 00:00:00';
                        $endDate    = $_POST['limite_inscripcion'] . ' 23:59:59';
                        $getAlbums  = Engine_Api::_()->estadistica()->getAlbum($startDate, $endDate);
                        $titulos    = array(
                            'album'       => 'albums',
                            'comentarios' => 'Comentarios',
                            'likes'       => 'Me Gusta',
                            'compartir'   => 'Compartir'
                        );

                        // aqui le decimos al navegador que vamos a mandar un archivo del tipo CSV
                        header("Content-Description: File Transfer");
                        header("Content-Type: application/force-download");
                        header("Content-Disposition: attachment; filename=estadisticas_albumes_" . date("Y-m-d") . ".csv");

                        $handle = fopen('php://temp', 'r+');
                        $part   = '';
                        fputcsv($handle, $titulos, ';', '"');



                        foreach ($getAlbums as $key => $album) {
                            fputcsv($handle, array($album['album'], $album['comentarios']['comentarios'], $album['likes']['likes'], $album['share']['share']),';', '"');
                        }

                        rewind($handle);
                        while (!feof($handle)) {
                            $contents .= fread($handle, 8192);
                        }
                        fclose($handle);
                        echo $contents;

                    
                    }

                    

                    
                } catch (Exception $ex) {
                    print_r($ex->getMessage());
                }
                exit();
            } else {
                // Flash messages
                Engine_Api::_()->core()->setFlashMessage(array(
                    'title'   => 'Datos incompletos',
                    'message' => 'La fecha de inicio y la final debe ir diligenciada',
                    'level'   => 'error',
                ));
            }
        }

        // Revisar si hay Mensajes de confirmacion por mostrar al usuario
        if (Engine_Api::_()->core()->existsFlashMessage()) {
            $this->view->popupMessage = Engine_Api::_()->core()->getFlashMessage();
            Engine_Api::_()->core()->unsetFlashMessage();
        };

    }

}