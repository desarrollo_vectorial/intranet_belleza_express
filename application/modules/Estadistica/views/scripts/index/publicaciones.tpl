<?php
$this->headLink()
    ->appendStylesheet($this->baseUrl().'/application/modules/Encuestas/externals/styles/materialize.min.css')
    ->appendStylesheet($this->baseUrl().'/externals/jquery_ui/css/ui-lightness/jquery-ui-1.10.3.custom.min.css')
    ->appendStylesheet($this->baseUrl().'/externals/datetimepicker/jquery.datetimepicker.css');


$this->headScript()->appendFile($this->baseUrl().'/externals/jquery_ui/js/jquery-ui-1.10.3.custom.js')
    ->appendFile($this->baseUrl().'/externals/datetimepicker/php-date-formatter.min.js')
    ->appendFile($this->baseUrl().'/externals/datetimepicker/jquery.mousewheel.js')
    ->appendFile($this->baseUrl().'/externals/datetimepicker/jquery.datetimepicker.js');
?>

<div class="form-general estadisticas--container">
    <h4>Generar Reporte de cantidad de publicaciones</h4>

    <form id="exportar_csv" enctype="multipart/form-data" class="global_form" action="/estadistica/publicaciones/" method="post">

        <div class="row">
            <div class="col">
                <label for="fecha_inicio" class="required">Fecha de Inicio</label>
                <input type="text" id="fecha_inicio" name="fecha_inicio">
            </div>
            <div class="col">
                <label for="limite_inscripcion" class="optional">Fecha límite</label>
                <input type="text" id="limite_inscripcion" name="limite_inscripcion">
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <button name="submit" id="submit" type="submit" class="waves-effect waves-light right btn red darken-4">EXPORTAR (.CSV)</button>
            </div>
        </div>

    </form>
    

    <!-- FLASH MESSAGES -->
    <script type="text/javascript">
        <?php if (!empty($this->popupMessage)): ?>
        <?php Engine_Api::_()->core()->unsetFlashMessage();?>
            var myPopUp = new PopUpWindow('<?php echo $this->popupMessage["title"]; ?>', <?php echo json_encode($this->popupMessage); ?>);
            myPopUp.open();
            setTimeout(function(){
                myPopUp.close();
            }, 5000);
        <?php endif;?>
    </script>
    <!-- FLASH MESSAGES -->
</div>


<script type="text/javascript">
    jQuery(document).ready(function($) {
        $.datetimepicker.setLocale('es');

        $('#fecha_inicio').datetimepicker({
            lang: 'es',
            format: 'Y-m-d',
            formatDate: 'Y-m-d',
            mask: '9999-19-39'
        });
        $('#limite_inscripcion').datetimepicker({
            lang: 'es',
            format: 'Y-m-d',
            formatDate: 'Y-m-d',
            mask: '9999-19-39'
        });
    });
</script>