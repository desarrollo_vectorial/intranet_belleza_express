<?php
return array(
	// ------- where I am
	array(
		'title'       => 'Donde estoy - Estadisticas',
		'description' => 'Muestra en que modulo se encuestra actualmente',
		'category'    => 'Estadistica',
		'type'        => 'widget',
		'name'        => 'estadistica.where-i-am'
	),

	// ------- menu
	array(
		'title'       => 'Menu del modulo - Estadisticas',
		'description' => 'Muestra el menu del modulo.',
		'category'    => 'Estadistica',
		'type'        => 'widget',
		'name'        => 'estadistica.menu-browse'
	),
);