<?php
	return array (
		'package' => array (
			'type' => 'module',
			'name' => 'estadistica',
			'version' => '1.0.0',
			'path' => 'application/modules/Estadistica',
			'title' => 'Estadistica',
			'description' => 'Modulo de estadistica.',
			'author' => 'Javier López',
			'callback' => array (
				'class' => 'Engine_Package_Installer_Module',
			),
			'actions' => array (
				0 => 'install',
				1 => 'upgrade',
				2 => 'refresh',
				3 => 'enable',
				4 => 'disable',
			),
			'directories' => array (
				0 => 'application/modules/Estadistica',
			),
			'files' => array (
				0 => 'application/languages/en/estadistica.csv',
			),
		),
		'routes' => array(
			'estadistica_index_index' => array(
				'route'    => 'estadistica/',
				'defaults' => array(
					'module' 	 => 'estadistica',
					'controller' => 'index',
					'action' 	 => 'index',
				),
			),

			'estadistica_index_publicaciones' => array(
				'route'   => 'estadistica/publicaciones',
				'defaults' => array(
					'module' 	 => 'estadistica',
					'controller' => 'index',
					'action'  	 => 'publicaciones',
				),
			),

			'estadistica_index_datospublicacion' => array(
				'route'   => 'estadistica/datospublicacion',
				'defaults' => array(
					'module' 	 => 'estadistica',
					'controller' => 'index',
					'action'  	 => 'datospublicacion',
				),
			),


		)
	);
?>