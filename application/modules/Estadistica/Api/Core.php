<?php

class Estadistica_Api_Core extends Core_Api_Abstract{

    //Funciones para el reporte de interaccion //

    //Obteniendo las estadisticas por usuario
    public function getEstadisticasPorUsuario($startDate, $endDate){
        $returnObj  = array();
        $tableUsers = Engine_Api::_()->getDbtable('users', 'user');
        $listaUsers = $tableUsers->fetchAll();

        foreach ($listaUsers as $key => $user) {
            $comentarios    = $this->getEstadisticasNoticias($user->user_id, $startDate, $endDate);
            $likes          = $this->getEstadisticasLikes($user->user_id, $startDate, $endDate);
            $publicaciones  = $this->getPublicaciones($user->user_id, $startDate, $endDate);
            $compartidos    = $this->getCompartidos($user->user_id, $startDate, $endDate);
            $album_photo    = $this->getAlbumFotos($user->user_id, $startDate, $endDate);
            if ($comentarios['comentarios'] != 0 || $likes['likes'] != 0  || $publicaciones['publicaciones'] != 0 || $compartidos['compartidos'] != 0 || $album_photo['album_photo'] != 0) {
                $returnObj[$user->user_id] = array(
                    'userData'      => array( 'nombre' => $user->displayname, ),
                    'comentarios'   => $comentarios,
                    'likes'         => $likes,
                    'publicaciones' => $publicaciones,
                    'compartidos'   => $compartidos,
                    'album_photo'   => $album_photo,
                );
            }
        }

        return $returnObj;
    }

    //Obteniendo las estadisticas por noticias por usuario
    public function getEstadisticasNoticias($user_id, $startDate, $endDate){
        $returnObj = array();
        $tableComments = Engine_Api::_()->getDbtable('comments', 'core');
        $array_resource_type = array(
            'advgroup_album',
            'album',
            'album_photo',
            'article',
            'article_photo',
            'blog',
            'classified',
            'ynwiki_page',
        );
        $select = $tableComments->select()
            ->from('engine4_core_comments', array('TotalComentarios' => new Zend_Db_Expr('COUNT(*)')))
            ->where('resource_type IN (?)', $array_resource_type)
            ->where('poster_type = ?', 'user')
            ->where('poster_id = ?', $user_id)
            ->where("creation_date BETWEEN '" . $startDate . "' AND '" . $endDate . "' ")
        ;
        $resultComentarios = $tableComments->fetchRow($select);
        $comentarios_core = $resultComentarios['TotalComentarios'];

        $tableComments = Engine_Api::_()->getDbtable('comments', 'activity');
        $select = $tableComments->select()
            ->from('engine4_activity_comments', array('TotalComentarios' => new Zend_Db_Expr('COUNT(*)')))
            ->where('poster_type = ?', 'user')
            ->where('poster_id = ?', $user_id)
            ->where("creation_date BETWEEN '" . $startDate . "' AND '" . $endDate . "' ")
        ;
        $resultComentarios = $tableComments->fetchRow($select);
        $comentarios_activity = $resultComentarios['TotalComentarios'];

        $returnObj['comentarios'] = $comentarios_core + $comentarios_activity;

        return $returnObj;
    }

    //Obteniendo la cantidad de estadisticas por like por usuario
    public function getEstadisticasLikes($user_id, $startDate, $endDate){
        $returnObj = array();
        $tableLikes = Engine_Api::_()->getDbtable('likes', 'core');
        $array_resource_type = array(
            'activity_comment',
            'advgroup_album',
            'album',
            'album_photo',
            'article',
            'article_photo',
            'blog',
            'classified',
            'core_comment',
            'video',
            'ynwiki_page',
        );
        $select = $tableLikes->select()
            ->from('engine4_core_likes', array('TotalLikes' => new Zend_Db_Expr('COUNT(*)')))
            ->where('resource_type IN (?)', $array_resource_type)
            ->where('poster_id = ?', $user_id)
            ->where('poster_type = ?', 'user')
            ->where("fecha_registro BETWEEN '" . $startDate . "' AND '" . $endDate . "' ")
        ;
        $resultLikes = $tableLikes->fetchRow($select);

        $returnObj['likes'] = $resultLikes['TotalLikes'];

        return $returnObj;
    }

    //Obteniendo la cantidad de publicaciones por usuarios
    public function getPublicaciones($user_id, $startDate, $endDate){
        $array_resource_type = array(
            'article_new',
            'blog_new',
            'classified_new',
            'ynwiki_new',
            'status',
            'post',
            'advgroup_wiki_create',
            'advgroup_topic_create',
            'advgroup_poll_new',
            'event_create',
            'event_topic_create',
        );
        $returnObj = array();
        $table = Engine_Api::_()->getDbtable('actions', 'activity');
        $select = $table->select()
            ->from('engine4_activity_actions', array('count(*) as type_count'))
            ->where('type IN (?)', $array_resource_type)
            ->where('subject_id = ?', $user_id)
            ->where('subject_type = ?', 'user')
            ->where("date BETWEEN '" . $startDate . "' AND '" . $endDate . "' ")
        ;
        $result = $table->fetchAll($select);
        foreach ($result as $value) {
            $returnObj['publicaciones'] = $value['type_count'];
        }
        return $returnObj;
    }

    //Obteniendo la cantidad de compartidos por usuario
    public function getCompartidos($user_id, $startDate, $endDate){
        $returnObj = array();
        $table  = Engine_Api::_()->getDbtable('actions', 'activity');
        $select = $table->select()
            ->from('engine4_activity_actions', array('count(*) as type_count'))
            ->where('type = ?', 'share')
            ->where('subject_id = ?', $user_id)
            ->where("date BETWEEN '" . $startDate . "' AND '" . $endDate . "' ")
        ;
        $result = $table->fetchAll($select);
        foreach ($result as $value) {
            $returnObj['compartidos'] = $value['type_count'];
        }
        return $returnObj;
    }

    //Obteniendo el total de fotos de albums por usuario
    public function getAlbumFotos($user_id, $startDate, $endDate){
        $array_resource_type = array(
            'album_photo_new',
            'profile_photo_update',
            'event_photo_upload',
            'advgroup_photo_upload'
            );
        $returnObj = array();
        $table = Engine_Api::_()->getDbtable('actions', 'activity');
        $select = $table->select()
            ->from('engine4_activity_actions', array('attachment_count'))
            ->where('type IN (?)', $array_resource_type)
            ->where('subject_id = ?', $user_id)
            ->where('subject_type = ?', 'user')
            ->where("date BETWEEN '" . $startDate . "' AND '" . $endDate . "' ")
        ;
        $result = $table->fetchAll($select);
        $fotos = 0;
        foreach ($result as $value) {
            $fotos = $fotos + $value['attachment_count'];
        }
        $returnObj['album_photo'] = $fotos_perfil + $fotos;
        return $returnObj;
    }

    //Obteniendo el total de usuarios
    public function getTotalUsersEnabled(){
        $table = Engine_Api::_()->getDbtable('users', 'user');
        $select = $table->select()
                        ->from('engine4_users', array('TotalUsuarios' => new Zend_Db_Expr('COUNT(*)')))
                        ->where('enabled = ?', 1);
        $result = $table->fetchRow($select);
        return $result['TotalUsuarios'];
    }




    //Funciones para el reporte de publicaciones 
    
    //Obteniendo la cantidad de noticias por fecha
    public function getTotalNews($startDate, $endDate){
        $table      = Engine_Api::_()->getDbtable('articles', 'article');
        $select     = $table->select()
                            ->from('engine4_article_articles', array('count(*) as type_count'))
                            ->where("creation_date BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
        $result     = $table->fetchRow($select);
        return $result['type_count'];
    }

    //Obteniendo la cantidad de blogs por fecha
    public function getTotalBlogs($startDate, $endDate){
        $array_type = array('blog_new');
        $table      = Engine_Api::_()->getDbtable('blogs', 'blog');
        $select     = $table->select()
                            ->from('engine4_blog_blogs', array('count(*) as type_count'))
                            ->where("creation_date BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
        $result     = $table->fetchRow($select);
        return $result['type_count'];
    }

    //Obteniendo la cantidad de wikis por fecha
    public function getTotalWikis($startDate, $endDate){
        $table      = Engine_Api::_()->getDbtable('pages', 'ynwiki');
        $select     = $table->select()
                            ->from('engine4_ynwiki_pages', array('count(*) as type_count'))
                            ->where("creation_date BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
        
        $result     = $table->fetchRow($select);
        return $result['type_count'];
    }

    //Obteniendo la cantidad de videos por fecha
    public function getTotalVideos($startDate, $endDate){
        $table      = Engine_Api::_()->getDbtable('videos', 'video');
        $select     = $table->select()
                            ->from('engine4_video_videos', array('count(*) as type_count'))
                            ->where("creation_date BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
        $result     = $table->fetchRow($select);
        return $result['type_count'];
    }

    //Obteniendo la cantidad de clasificados por fecha
    public function getTotalClasificados($startDate, $endDate){
        $table      = Engine_Api::_()->getDbtable('classifieds', 'classified');
        $select     = $table->select()
                            ->from('engine4_classified_classifieds', array('count(*) as type_count'))
                            ->where("creation_date BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
        $result     = $table->fetchRow($select);
        return $result['type_count'];
    }

    //Obteniendo la cantidad de eventos por fecha
    public function getTotalEventos($startDate, $endDate){
        $table      = Engine_Api::_()->getDbtable('events', 'event');
        $select     = $table->select()
                            ->from('engine4_event_events', array('count(*) as type_count'))
                            ->where("creation_date BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
        $result     = $table->fetchRow($select);
        return $result['type_count'];
    }

    //Obteniendo la cantidad de albums por fecha
    public function getTotalAlbumes($startDate, $endDate){
        $table      = Engine_Api::_()->getDbtable('albums', 'album');
        $select     = $table->select()
                            ->from('engine4_album_albums', array('count(*) as type_count'))
                            ->where("type IS NULL AND search = 1 OR search = 1 AND type NOT IN ('profile', 'wall', 'message', 'blog')")
                            ->where("creation_date BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
        $result     = $table->fetchRow($select);
        return $result['type_count'];
    }

    //Obteniendo la cantidad de grupos por fecha
    public function getTotalGrupos($startDate, $endDate){
        $table      = Engine_Api::_()->getDbtable('groups', 'group');
        $select     = $table->select()
                            ->from('engine4_group_groups', array('count(*) as type_count'))
                            ->where("creation_date BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
        $result     = $table->fetchRow($select);
        return $result['type_count'];
    }




    //Funciones para el reporte de datos de las publicaciones

    //Obteniendo las estadisticas por noticias
    public function getNoticias($startDate, $endDate){
        $returnObj = array();
        $tableNews = Engine_Api::_()->getDbtable('articles', 'article');
        $selNews   = $tableNews->select()->where("creation_date >= ?", $startDate)->where("creation_date <= ?", $endDate);
        $listaNews = $tableNews->fetchAll($selNews);

        foreach ($listaNews as $key => $article) {
            $comentarios    = $this->getComentariosTipo($article->article_id, $startDate, $endDate, 'article');
            $likes          = $this->getLikesTipo      ($article->article_id, $startDate, $endDate, 'article');
            $compartidos    = $this->getCompartidosTipo($article->article_id, $startDate, $endDate, 'article');
            if ($comentarios['comentarios'] != 0 || $likes['likes'] != 0 || $compartidos['compartidos'] != 0){
                $returnObj[$article->article_id] = array(
                    'noticias'    => $article->title,
                    'comentarios' => $comentarios,
                    'likes'       => $likes,
                    'share'       => $compartidos
                );
            }
        }

        return $returnObj;
    }

    //Obteniendo las estadisticas por blogs
    public function getBlogs($startDate, $endDate){
        $returnObj  = array();
        $tableBlogs = Engine_Api::_()->getDbtable('blogs', 'blog');
        $selBlogs   = $tableBlogs->select()->where("creation_date >= ?", $startDate)->where("creation_date <= ?", $endDate);
        $listaBlogs = $tableBlogs->fetchAll($selBlogs);

        foreach ($listaBlogs as $key => $blog) {
            $comentarios    = $this->getComentariosTipo($blog->blog_id, $startDate, $endDate, 'blog');
            $likes          = $this->getLikesTipo      ($blog->blog_id, $startDate, $endDate, 'blog');
            $compartidos    = $this->getCompartidosTipo($blog->blog_id, $startDate, $endDate, 'blog');
            $returnObj[$blog->blog_id] = array(
                'blog'        => $blog->title,
                'comentarios' => $comentarios,
                'likes'       => $likes,
                'share'       => $compartidos
            );
        }
        return $returnObj;
    }

    //Obteniendo las estadisticas por wikis
    public function getWikis($startDate, $endDate){
        $returnObj  = array();
        $tableWikis = Engine_Api::_()->getDbtable('pages', 'ynwiki');
        $selWikis   = $tableWikis->select()->where("creation_date >= ?", $startDate)->where("creation_date <= ?", $endDate);
        $listaWikis = $tableWikis->fetchAll($selWikis);
        

        foreach ($listaWikis as $key => $wiki) {
            $comentarios    = $this->getComentariosTipo($wiki->page_id, $startDate, $endDate, 'ynwiki_page');
            $likes          = $this->getLikesTipo      ($wiki->page_id, $startDate, $endDate, 'ynwiki_page');
            $compartidos    = $this->getCompartidosTipo($wiki->page_id, $startDate, $endDate, 'wiki');
            $returnObj[$wiki->page_id] = array(
                'wiki'        => $wiki->title,
                'comentarios' => $comentarios,
                'likes'       => $likes,
                'share'       => $compartidos
            );
        }
        return $returnObj;
    }

    //Obteniendo las estadisticas por video
    public function getVideo($startDate, $endDate){
        $returnObj   = array();
        $tablevideos = Engine_Api::_()->getDbtable('videos', 'video');
        $selVideos   = $tablevideos->select()->where("creation_date >= ?", $startDate)->where("creation_date <= ?", $endDate);
        $listavideos = $tablevideos->fetchAll($selVideos);
 

        foreach ($listavideos as $key => $video) {
            $comentarios    = $this->getComentariosTipo($video->video_id, $startDate, $endDate, 'video');
            $likes          = $this->getLikesTipo($video->video_id, $startDate, $endDate, 'video');
            $compartidos    = $this->getCompartidosTipo($video->video_id, $startDate, $endDate, 'videos');
            
            array_push($returnObj, array(
                    'video_id'    => $video_id,
                    'video'       => $video->title,
                    'comentarios' => $comentarios,
                    'likes'       => $likes,
                    'share'       => $compartidos
                )
            );
        }

        return $returnObj;
    }


    //Obteniendo las estadisticas por clasificado
    public function getClasificado($startDate, $endDate){
        $returnObj        = array();
        $tableclassifieds = Engine_Api::_()->getDbtable('classifieds', 'classified');
        $selclassifieds   = $tableclassifieds->select()->where("creation_date >= ?", $startDate)->where("creation_date <= ?", $endDate);
        $listaclassifieds = $tableclassifieds->fetchAll($selclassifieds);
 

        foreach ($listaclassifieds as $key => $classified) {
            $comentarios = $this->getComentariosTipo($classified->classified_id, $startDate, $endDate, 'classified');
            $likes       = $this->getLikesTipo($classified->classified_id, $startDate, $endDate, 'classified');
            $compartidos = $this->getCompartidosTipo($classified->classified_id, $startDate, $endDate, 'classifieds');
            
            array_push($returnObj, array(
                    'classified_id' => $classified_id,
                    'classified'    => $classified->title,
                    'comentarios'   => $comentarios,
                    'likes'         => $likes,
                    'share'         => $compartidos
                )
            );
        }

        return $returnObj;
    }


    //Obteniendo las estadisticas por eventos
    public function getEvento($startDate, $endDate){
        $returnObj        = array();
        $tableevents = Engine_Api::_()->getDbtable('events', 'event');
        $selevents   = $tableevents->select()->where("creation_date >= ?", $startDate)->where("creation_date <= ?", $endDate);
        $listaevents = $tableevents->fetchAll($selevents);
 

        foreach ($listaevents as $key => $event) {
            $comentarios    = $this->getComentariosTipo($event->event_id, $startDate, $endDate, 'event');
            $likes          = $this->getLikesTipo($event->event_id, $startDate, $endDate, 'event');
            //$compartidos  = $this->getCompartidosTipo($event->event_id, $startDate, $endDate, 'event');
            $compartidos    = 0;
            
            array_push($returnObj, array(
                    'event_id'    => $event_id,
                    'event'       => $event->title,
                    'comentarios' => $comentarios,
                    'likes'       => $likes,
                    'share'       => $compartidos
                )
            );
        }

        return $returnObj;
    }


    //Obteniendo las estadisticas por albumes
    public function getAlbum($startDate, $endDate){
        $returnObj   = array();
        $tablealbums = Engine_Api::_()->getDbtable('albums', 'album');
        $selalbums   = $tablealbums->select()->where("creation_date >= ?", $startDate)->where("creation_date <= ?", $endDate)->where("type IS NULL AND search = 1 OR search = 1 AND type NOT IN ('profile', 'wall', 'message', 'blog')");
        $listaalbums = $tablealbums->fetchAll($selalbums);
 

        foreach ($listaalbums as $key => $album) {
            $comentarios    = $this->getComentariosTipo($album->album_id, $startDate, $endDate, 'album');
            $likes          = $this->getLikesTipo($album->album_id, $startDate, $endDate, 'album');
            $compartidos  = $this->getCompartidosTipo($album->album_id, $startDate, $endDate, 'albums');
            
            array_push($returnObj, array(
                    'album_id'    => $album_id,
                    'album'       => $album->title,
                    'comentarios' => $comentarios,
                    'likes'       => $likes,
                    'share'       => $compartidos
                )
            );
        }

        return $returnObj;
    }








    //Obteniendo la cantidad de comentarios por tipo
    public function getComentariosTipo($tipo_id, $startDate, $endDate, $tipo){
        $returnObj           = array();
        $tableComments       = Engine_Api::_()->getDbtable('comments', 'core');
        $array_resource_type = array($tipo);
        $select              = $tableComments   ->select()
                                                ->from('engine4_core_comments', array('TotalComentarios' => new Zend_Db_Expr('COUNT(*)')))
                                                ->where('resource_type IN (?)', $array_resource_type)
                                                ->where('resource_id = ?', $tipo_id)
                                                ->where("creation_date BETWEEN '" . $startDate . "' AND '" . $endDate . "' ");
        $resultComentarios = $tableComments->fetchRow($select);
        $comentarios_core  = $resultComentarios['TotalComentarios'];

        $tableComments     = Engine_Api::_()->getDbtable('comments', 'activity');
        $select            = $tableComments ->select()
                                            ->from('engine4_activity_comments', array('TotalComentarios' => new Zend_Db_Expr('COUNT(*)')))
                                            ->where('resource_id = ?', $tipo_id)
                                            ->where("creation_date BETWEEN '" . $startDate . "' AND '" . $endDate . "' ");

        $resultComentarios    = $tableComments->fetchRow($select);
        $comentarios_activity = $resultComentarios['TotalComentarios'];

        $returnObj['comentarios'] = $comentarios_core + $comentarios_activity;

        return $returnObj;
    }


    //Obteniendo la cantidad de likes por tipo
    public function getLikesTipo($tipo_id, $startDate, $endDate, $tipo){
        $returnObj           = array();
        $tableLikes          = Engine_Api::_()->getDbtable('likes', 'core');
        $array_resource_type = array($tipo);
        $select = $tableLikes   ->select()
                                ->from('engine4_core_likes', array('TotalLikes' => new Zend_Db_Expr('COUNT(*)')))
                                ->where('resource_type IN (?)', $array_resource_type)
                                ->where('resource_id = ?', $tipo_id)
                                ->where("fecha_registro BETWEEN '" . $startDate . "' AND '" . $endDate . "' ");
        $resultLikes        = $tableLikes->fetchRow($select);
        $returnObj['likes'] = $resultLikes['TotalLikes'];
        return $returnObj;
    }


    //Obteniendo la cantidad de compartidos por tipo
    public function getCompartidosTipo($tipo_id, $startDate, $endDate, $tipo){
       
        $returnObj  = array();
        $tableShare = Engine_Api::_()->getDbtable('actions', 'activity');
        
        $select     = $tableShare   ->select()
                                    ->from('engine4_activity_actions', array('TotalShare' => new Zend_Db_Expr('COUNT(*)')))
                                    ->where("type  = 'share'")
                                    ->where("date BETWEEN '" . $startDate . "' AND '" . $endDate . "' ");
        
        if($tipo == 'article'){
            $select->where("SUBSTRING_INDEX(SUBSTRING_INDEX(params,'\\\\', 3), '/',-1) = 'article' ");
            $select->where("SUBSTRING_INDEX(SUBSTRING_INDEX(params,'\\\\', 5), '/',-1) = '".$tipo_id."' ");
        }

        if($tipo == 'blogs'){
            $select->where("SUBSTRING_INDEX(SUBSTRING_INDEX(params,'\\\\', 3), '/',-1) = 'blogs' ");
            $select->where("SUBSTRING_INDEX(SUBSTRING_INDEX(params,'\\\\', 5), '/',-1) = '".$tipo_id."' ");
        }

        if($tipo == 'wiki'){
            $select->where("SUBSTRING_INDEX(SUBSTRING_INDEX(params,'\\\\', 3), '/',-1) = 'wiki' ");
            $select->where("SUBSTRING_INDEX(SUBSTRING_INDEX(params,'\\\\', 6), '/',-1) = '".$tipo_id."' ");
        }

        if($tipo == 'videos'){
            $select->where("SUBSTRING_INDEX(SUBSTRING_INDEX(params,'\\\\', 3), '/',-1) = 'videos' ");
            $select->where("SUBSTRING_INDEX(SUBSTRING_INDEX(params,'\\\\', 5), '/',-1) = '".$tipo_id."' ");
        }

        if($tipo == 'classifieds'){
            $select->where("SUBSTRING_INDEX(SUBSTRING_INDEX(params,'\\\\', 3), '/',-1) = 'classifieds' ");
            $select->where("SUBSTRING_INDEX(SUBSTRING_INDEX(params,'\\\\', 5), '/',-1) = '".$tipo_id."' ");
        }

        if($tipo == 'albums'){
            $select->where("SUBSTRING_INDEX(SUBSTRING_INDEX(params,'\\\\', 3), '/',-1) = 'albums' ");
            $select->where("SUBSTRING_INDEX(SUBSTRING_INDEX(params,'\\\\', 7), '/',-1) = '".$tipo_id."' ");
        }

        $resultShare        = $tableShare->fetchRow($select);
        $returnObj['share'] = $resultShare['TotalShare'];
        return $returnObj;

    }


    

}
