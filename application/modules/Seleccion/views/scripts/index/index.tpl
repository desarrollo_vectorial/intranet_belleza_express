<div class="seleccion_bg">
	<div class="seleccion_info">
		<h1><strong>BIENVENIDO A NUESTRA</strong></h1>
		<h1>PLATAFORMA DE SELECCIÓN</h1>
		<p class="borders_parragraph" style="text-align: justify; font-size: 16px">
			¿Estás buscando talento humano para una vacante en tu área? 
			Realiza tu solicitud a través de la nueva herramienta <strong>Ikano</strong> 
			para que el área de Selección te brinde la asesoría y 
			acompañamiento en el proceso.
			<br /><br />
			Si tienes alguna duda descarga el manual o dirígete a 
			la persona responsable de Selección según tu sede.
		</p>
	</div>
</div>
<div class="postula_vacante_cont">
	<a href="/seleccion?codCIN=<?php echo $this->codigoCin; ?>">
		<div class="postula_vacante" style="width: 260px;">
			REALIZA TU SOLICITUD
		</div>
	</a>
	<a href="/public/pdfs/manual_seleccion.pdf" target="_blank" download="manual_seleccion">
		<div class="descargar_manual">
			<strong>Descargar</strong> manual
		</div>
	</a>
</div>