<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'seleccion',
    'version' => '4.0.0',
    'path' => 'application/modules/Seleccion',
    'title' => 'Selección',
    'description' => 'Selección - Ikano',
    'author' => 'Vectorial Studios Ltda',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Seleccion',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/seleccion.csv',
    ),
  ),
); ?>