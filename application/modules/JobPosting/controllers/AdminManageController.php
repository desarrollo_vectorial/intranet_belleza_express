<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminManageController
 *
 * @author dihinca
 */
class JobPosting_AdminManageController extends Core_Controller_Action_Admin
{
  public function indexAction()
  {
      $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('jobs_admin_main', array(), 'core_admin_main_plugins_job-posting');     
	
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        
    if ($this->getRequest()->isPost()) {
    $values = $this->getRequest()->getPost();
      foreach ($values as $key => $value) {
        if ($key == 'delete_' . $value) {
            $db->query("DELETE FROM engine4_jobposting_conocimientos WHERE conocimiento_id = ".$value.";");
        }
      }
    }
	
    $page=$this->_getParam('page',1);

    $table = Engine_Api::_()->getDbtable('Conocimientos', 'JobPosting');
    $select = $table->select();
    
    // Make paginator
    $this->view->paginator = $paginator = Zend_Paginator::factory($select);
    $this->view->paginator = $paginator->setCurrentPageNumber( $page );
  }
  
  public function createAction()
  {
    $_module = $this->module;
    // In smoothbox
    $this->_helper->layout->setLayout('admin-simple');

    // Generate and assign form
    $form = $this->view->form = new JobPosting_Form_Create();
    $form->setAction($this->view->url());
	
    // Check post
    if( !$this->getRequest()->isPost() ) {
		$form->setDefaultValues();
      return;
    }
	
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }
	
	
    
    // Process
    $values = $form->getValues();
	
    $table = $this->_helper->api()->getDbtable('conocimientos', 'JobPosting');
	
    $db = $table->getAdapter();
    $db->beginTransaction();

    try {
	  $indicator = $table->createRow();
	  $indicator -> conocimiento_conocimiento = $values['conocimiento_conocimiento'];
	  $indicator -> save();
      
      $db->commit();
    } catch( Exception $e ) {
      $db->rollBack();
      throw $e;
    }
    
    return $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => 10,
      'parentRefresh' => 10,
      'messages' => array('')
    ));
  }  
  public function editAction()
  {
    $id = $this->_getParam('id');
    $this->view->conocimiento_id = $id;
	$link_id = $id;
	
	
    
    $conocimientoTable = Engine_Api::_()->getDbtable('Conocimientos', 'JobPosting');
    $conocimiento = $conocimientoTable->find($id)->current();
	
    // In smoothbox
    $this->_helper->layout->setLayout('admin-simple');

    // Generate and assign form
    $form = $this->view->form = new JobPosting_Form_Create();
    $form->setAction($this->view->url());
    $form->conocimiento_id->setValue($id);
    $form->conocimiento_conocimiento->setValue($conocimiento->conocimiento_conocimiento);
    $form->submit->setLabel('Edit conocimiento');
    
    // Check post
    if( !$this->getRequest()->isPost() ) {
      $this->renderScript('admin-manage/create.tpl');
      return;
    }
    
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      $this->renderScript('admin-manage/create.tpl');
      return;
    }
    // Process
    $values = $form->getValues();
	
    $db = $conocimientoTable->getAdapter();
    $db->beginTransaction();

    try {


      $conocimiento->conocimiento_conocimiento = $values['conocimiento_conocimiento'];

      $conocimiento -> save();
      
      $db->commit();

    } catch( Exception $e ) {
      $db->rollBack();
      throw $e;
    }
    
    return $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => 10,
      'parentRefresh' => 10,
      'messages' => array('')
    ));
  }
  
  public function deleteAction()
  {
    $id = $this->_getParam('id');
	 
    $conocimientoTable = Engine_Api::_()->getDbtable('Conocimientos', 'JobPosting');
    $conocimiento = $conocimientoTable->find($id)->current();
	
    $conocimiento -> delete();
    
    return $this->_redirect('/admin/job-posting/manage/');
  }

	public function levelsAction()
	{
		
		$id = $this->_getParam('level_id');
                
	
		$level = (null != $id) ? Engine_Api::_()->getItem('authorization_level', $id) : Engine_Api::_()->getItemTable('authorization_level')->getDefaultLevel();
	
		if(!$level instanceof Authorization_Model_Level) throw new Engine_Exception('Missing Level');
		
		$id = $level->level_id;
		
		$this->view->form = $form = new JobPosting_Form_Levels(array('public' => ( in_array($level->type, array('public')) ), 'moderator' => ( in_array($level->type, array('admin', 'moderator')))));
	
		$form->level_id->setValue($id);
			
		$permissions = Engine_Api::_()->getDbtable('permissions', 'authorization');
	
		if( !$this->getRequest()->isPost() ) {
		  $form->populate($permissions->getAllowed('JobPosting', $id, array_keys($form->getValues())));
		  return;
		}
	
		if( !$form->isValid($this->getRequest()->getPost()) ) return;
		
		$values = $form->getValues();
	
		$db = $permissions->getAdapter();
		$db->beginTransaction();
		try	{
		  $permissions->setAllowed('JobPosting', $id, $values);
		  $db->commit();
		  $form->addNotice('Your changes have been saved.');
		}
		catch( Exception $e ) {
		  $db->rollBack();
		  throw $e;
		}
	}
}

?>
