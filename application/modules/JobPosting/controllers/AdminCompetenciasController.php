<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminManageController
 *
 * @author dihinca
 */
class JobPosting_AdminCompetenciasController extends Core_Controller_Action_Admin
{
  public function indexAction()
  {
      $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('jobs_admin_main', array(), 'core_admin_main_plugins_job-posting-competencias');     
	
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        
    if ($this->getRequest()->isPost()) {
    $values = $this->getRequest()->getPost();
      foreach ($values as $key => $value) {
        if ($key == 'delete_' . $value) {
            $db->query("DELETE FROM engine4_jobposting_competencias WHERE competencias_id = ".$value.";");
        }
      }
    }
	
    $page=$this->_getParam('page',1);

    $table = Engine_Api::_()->getDbtable('Competencias', 'JobPosting');
    $select = $table->select();
    
    // Make paginator
    $this->view->paginator = $paginator = Zend_Paginator::factory($select);
    $this->view->paginator = $paginator->setCurrentPageNumber( $page );
  }
  
  public function createAction()
  {
    $_module = $this->module;
    // In smoothbox
    $this->_helper->layout->setLayout('admin-simple');

    // Generate and assign form
    $form = $this->view->form = new JobPosting_Form_Competencias();
    $form->setAction($this->view->url());
	
    // Check post
    if( !$this->getRequest()->isPost() ) {
		$form->setDefaultValues();
      return;
    }
	
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }
	
	
    
    // Process
    $values = $form->getValues();
	
    $table = $this->_helper->api()->getDbtable('competencias', 'JobPosting');
	
    $db = $table->getAdapter();
    $db->beginTransaction();

    try {
	  $indicator = $table->createRow();
	  $indicator -> competencias_competencia = $values['competencias'];
	  $indicator -> save();
      
      $db->commit();
    } catch( Exception $e ) {
      $db->rollBack();
      throw $e;
    }
    
    return $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => 10,
      'parentRefresh' => 10,
      'messages' => array('')
    ));
  }  
  public function editAction()
  {
    $id = $this->_getParam('id');
    $this->view->competencias_id = $id;
	
	
    
    $conocimientoTable = Engine_Api::_()->getDbtable('Competencias', 'JobPosting');
    $conocimiento = $conocimientoTable->find($id)->current();
	
    // In smoothbox
    $this->_helper->layout->setLayout('admin-simple');

    // Generate and assign form
    $form = $this->view->form = new JobPosting_Form_Competencias();
    $form->setAction($this->view->url());
    $form->competencias_id->setValue($id);
    $form->competencias_competencia->setValue($conocimiento->competencias_competencia);
    $form->submit->setLabel('Edit competencia');
    
    // Check post
    if( !$this->getRequest()->isPost() ) {
      $this->renderScript('admin-competencias/create.tpl');
      return;
    }
    
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      $this->renderScript('admin-competencias/create.tpl');
      return;
    }
    // Process
    $values = $form->getValues();
	
    $db = $conocimientoTable->getAdapter();
    $db->beginTransaction();

    try {


      $conocimiento->competencias_competencia = $values['competencias_competencia'];

      $conocimiento -> save();
      
      $db->commit();

    } catch( Exception $e ) {
      $db->rollBack();
      throw $e;
    }
    
    return $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => 10,
      'parentRefresh' => 10,
      'messages' => array('')
    ));
  }
  
  public function deleteAction()
  {
    $id = $this->_getParam('id');
	 
    $conocimientoTable = Engine_Api::_()->getDbtable('Competencias', 'JobPosting');
    $conocimiento = $conocimientoTable->find($id)->current();
	
    $conocimiento -> delete();
    
    return $this->_redirect('/admin/job-posting/competencias/');
  }
}

?>
