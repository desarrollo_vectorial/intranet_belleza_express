<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminManageController
 *
 * @author dihinca
 */
class JobPosting_AdminNivelesController extends Core_Controller_Action_Admin
{
  public function indexAction()
  {
      $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('jobs_admin_main', array(), 'core_admin_main_plugins_job-posting-niveles');     
	
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        
    if ($this->getRequest()->isPost()) {
    $values = $this->getRequest()->getPost();
      foreach ($values as $key => $value) {
        if ($key == 'delete_' . $value) {
            $db->query("DELETE FROM engine4_jobposting_niveles WHERE niveles_id = ".$value.";");
        }
      }
    }
	
    $page=$this->_getParam('page',1);

    $table = Engine_Api::_()->getDbtable('Niveles', 'JobPosting');
    $select = $table->select();
    
    // Make paginator
    $this->view->paginator = $paginator = Zend_Paginator::factory($select);
    $this->view->paginator = $paginator->setCurrentPageNumber( $page );
  }
  
  public function createAction()
  {
    $_module = $this->module;
    // In smoothbox
    $this->_helper->layout->setLayout('admin-simple');

    // Generate and assign form
    $form = $this->view->form = new JobPosting_Form_Niveles();
    $form->setAction($this->view->url());
	
    // Check post
    if( !$this->getRequest()->isPost() ) {
		$form->setDefaultValues();
      return;
    }
	
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }
	
	
    
    // Process
    $values = $form->getValues();
	
    $table = $this->_helper->api()->getDbtable('Niveles', 'JobPosting');
	
    $db = $table->getAdapter();
    $db->beginTransaction();

    try {
	  $indicator = $table->createRow();
	  $indicator -> niveles_nivel = $values['niveles_nivel'];
	  $indicator -> save();
      
      $db->commit();
    } catch( Exception $e ) {
      $db->rollBack();
      throw $e;
    }
    
    return $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => 10,
      'parentRefresh' => 10,
      'messages' => array('')
    ));
  }  
  public function editAction()
  {
    $id = $this->_getParam('id');
    $this->view->niveles_id = $id;
	
	
    
    $conocimientoTable = Engine_Api::_()->getDbtable('Niveles', 'JobPosting');
    $conocimiento = $conocimientoTable->find($id)->current();
	
    // In smoothbox
    $this->_helper->layout->setLayout('admin-simple');

    // Generate and assign form
    $form = $this->view->form = new JobPosting_Form_Niveles();
    $form->setAction($this->view->url());
    $form->niveles_id->setValue($id);
    $form->niveles_nivel->setValue($conocimiento->niveles_nivel);
    $form->submit->setLabel('Edit nivel');
    
    // Check post
    if( !$this->getRequest()->isPost() ) {
        $this->renderScript('admin-niveles/create.tpl');
      return;
    }
    
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      $this->renderScript('admin-niveles/create.tpl');
      return;
    }
    // Process
    $values = $form->getValues();
	
    $db = $conocimientoTable->getAdapter();
    $db->beginTransaction();

    try {


      $conocimiento->niveles_nivel = $values['niveles_nivel'];

      $conocimiento -> save();
      
      $db->commit();

    } catch( Exception $e ) {
      $db->rollBack();
      throw $e;
    }
    
    return $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => 10,
      'parentRefresh' => 10,
      'messages' => array('')
    ));
  }
  
  public function deleteAction()
  {
    $id = $this->_getParam('id');
	 
    $conocimientoTable = Engine_Api::_()->getDbtable('Niveles', 'JobPosting');
    $conocimiento = $conocimientoTable->find($id)->current();
	
    $conocimiento -> delete();
    
    return $this->_redirect('/admin/job-posting/niveles/');
  }
}

?>
