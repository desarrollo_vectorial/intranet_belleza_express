<?php

class JobPosting_IndexController extends Core_Controller_Action_Standard
{
  public function indexAction()
  {
    $viewer = Engine_Api::_()->user()->getViewer();    
    
    $canAdmin = $canCreate = Engine_Api::_()->authorization()->isAllowed('JobPosting', $viewer, 'creates');
    if($canAdmin)
    {
        return $this ->_redirect('jobposting/adminbrowse');
    }
    else
    {
        return $this ->_redirect('jobposting/userbrowse');
    }
  }
  
  public function adminbrowseAction()
  {
      $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('jobs_admin_browse', array(), 'core_public_admin_job-posting');     
      
      
	
        $page=$this->_getParam('page',1);

        $table = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $select = $table->select();
        
        $values = $this->getRequest()->getParams();
        
        $this->view->formValues = $values;
        
        if(($this->getRequest()->isPost()) || ($values['filterpost']))
        {
            unset($values['module']);
            unset($values['controller']);
            unset($values['action']);
            unset($values['rewrite']);
            $values['filterpost'] = true;
            $this->view->formValues =  $values;
            if($values['palabras']!='')
            {
                $palabrasArray = explode(' ', $values['palabras']);
                
                $arrayfields = array('jobs_nombre', 'jobs_mision', 'jobs_formacion', 'jobs_experiencia', 'jobs_lugar');
                $arrayquerys = array();
                
                foreach($palabrasArray as $palabra)
                {
                    foreach($arrayfields as $v)
                    {
                        $arrayquerys[] = "(".$v." LIKE '%".$palabra."%')";
                    }
                }
                
                $select->where(join(' OR ', $arrayquerys));
            }
            if($values['cargo']!='')
            {
                $select->where("(jobs_nombre LIKE '%".$values['cargo']."%')");
            }
            if($values['sede']!='')
            {
                $select->where("(jobs_lugar LIKE '%".$values['sede']."%')");
            }
        }

        // Make paginator
        $this->view->paginator = $paginator = Zend_Paginator::factory($select);
        $this->view->paginator = $paginator->setCurrentPageNumber( $page );
  }
  
  public function userbrowseAction()
  {
        $translate = Zend_Registry::get('Zend_Translate');
      
        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('job_user_browse', array(), 'core_public_user_job-posting');     
      
        $table = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
	
        $arraycompetencias = array();
        
        $db = $table->getAdapter();
        $viewer = Engine_Api::_()->user()->getViewer();    
      
        /*$query = "	SELECT 
        			engine4_user_fields_values.privacy
            		FROM 
            		engine4_user_fields_meta, 
            		engine4_user_fields_options, 
            		engine4_user_fields_values
					WHERE 
					engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id 
					AND engine4_user_fields_meta.type='multi_checkbox' 
					AND engine4_user_fields_meta.label IN ('Competencias', 'Conocimientos') 
					AND engine4_user_fields_values.field_id=engine4_user_fields_meta.field_id 
					AND engine4_user_fields_values.value=engine4_user_fields_options.option_id 
					AND engine4_user_fields_values.item_id=".$viewer->user_id;
        echo $query;
        $hojadevida = $db->fetchAll($query);
        
        $this->view->errorjobs = '';
        
        if(count($hojadevida)<1)
        {
            $this->view->errorjobs = $translate->translate('Lo invitamos a actualizar su perfil para poder postularse a las vacantes');
        }*/
      
        $query = "		SELECT 
        				engine4_user_fields_options.option_id, 
        				engine4_user_fields_options.label 
						FROM 
						engine4_user_fields_meta, 
						engine4_user_fields_options 
						WHERE 
						engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id 
						AND engine4_user_fields_meta.type='multi_checkbox' 
						AND engine4_user_fields_meta.label='Competencias'";
        
        $competencias = $db->fetchAll($query);
      
        $arraycompetencias = array();
      
        foreach($competencias as $r)
        {
            $arraycompetencias[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> competencias = $arraycompetencias;
      
   /*     $table = Engine_Api::_()->getDbtable('Conocimientos', 'JobPosting');
	$select = $table->select();
	$result = $table->fetchAll($select);
        $arrayconocimientos = array();
    */
      
        $query = "SELECT 
        			engine4_user_fields_options.option_id, 
        			engine4_user_fields_options.label 
					FROM 
					engine4_user_fields_meta, 
					engine4_user_fields_options 
					WHERE 
					engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id 
					AND engine4_user_fields_meta.type='multi_checkbox' 
					AND engine4_user_fields_meta.label='Conocimientos'";
        
        $conocimientos = $db->fetchAll($query);
      
        foreach($conocimientos as $r)
        {
            $arrayconocimientos[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> conocimientos = $arrayconocimientos;
        
        $viewer = Engine_Api::_()->user()->getViewer();    
        
        $userJobTable = Engine_Api::_()->getDbtable('Job_Usuario', 'JobPosting');
        
        $table = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $select = $table->select()
                ->where("jobs_estado='abierta'");
        
        $values = $this->getRequest()->getParams();
        
        $this->view->formValues = $values;
        
        if(($this->getRequest()->isPost()) || ($values['filterpost']))
        {
            unset($values['module']);
            unset($values['controller']);
            unset($values['action']);
            unset($values['rewrite']);
            $values['filterpost'] = true;
            $this->view->formValues =  $values;
            if(isset($values['competencias']))
            {
                $tabledos = $this->_helper->api()->getDbtable('job_Competencia', 'JobPosting');
                $select->from($table->info('name'))
                        ->join($tabledos->info('name'), $tabledos->info('name').'.job_competencia_job='.$table->info('name').'.jobs_id', null)
                        ->where($tabledos->info('name').'.job_competencia_competencia IN (?)', $values['competencias']);
            }
            if(isset($values['conocimientos']))
            {
                $tabledos = $this->_helper->api()->getDbtable('job_Conocimiento', 'JobPosting');
                $select->from($table->info('name'))
                        ->join($tabledos->info('name'), $tabledos->info('name').'.job_conocimiento_job='.$table->info('name').'.jobs_id', null)
                        ->where($tabledos->info('name').'.job_conocimiento_conocimiento IN (?)', $values['conocimientos']);
            }
            if($values['palabras']!='')
            {
                $palabrasArray = explode(' ', $values['palabras']);
                
                $arrayfields = array('jobs_nombre', 'jobs_mision', 'jobs_formacion', 'jobs_experiencia', 'jobs_lugar');
                $arrayquerys = array();
                
                foreach($palabrasArray as $palabra)
                {
                    foreach($arrayfields as $v)
                    {
                        $arrayquerys[] = "(".$v." LIKE '%".$palabra."%')";
                    }
                }
                
                $select->where(join(' OR ', $arrayquerys));
            }
            if($values['cargo']!='')
            {
                $select->where("(jobs_nombre LIKE '%".$values['cargo']."%')");
            }
            if($values['sede']!='')
            {
                $select->where("(jobs_lugar LIKE '%".$values['sede']."%')");
            }
        }
        
        $page = $this->_getParam('page', 1);
        
        $this->view->paginator = $paginator = Zend_Paginator::factory($select);
        $this->view->paginator = $paginator->setCurrentPageNumber( $page );
  }
  
  public function newjobAction()
  {
      $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('jobs_admin_browse', array(), 'core_public_new_job-posting');
      
/*        $table = Engine_Api::_()->getDbtable('Competencias', 'JobPosting');
	$select = $table->select();
	$result = $table->fetchAll($select);*/
      
	
        $table = $this->_helper->api()->getDbtable('jobs', 'JobPosting');

        $db = $table->getAdapter();
      
        /*$query = "SELECT engine4_user_fields_options.option_id, engine4_user_fields_options.label 
            FROM engine4_user_fields_meta, engine4_user_fields_options 
            WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='multi_checkbox' AND engine4_user_fields_meta.label='Competencias'";
        
        $competencias = $db->fetchAll($query);
      
        $arraycompetencias = array();
      
        foreach($competencias as $r)
        {
            $arraycompetencias[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> competencias = $arraycompetencias;*/
      
   /*     $table = Engine_Api::_()->getDbtable('Conocimientos', 'JobPosting');
	$select = $table->select();
	$result = $table->fetchAll($select);
        $arrayconocimientos = array();
    */
      
        /*$query = "SELECT engine4_user_fields_options.option_id, engine4_user_fields_options.label 
            FROM engine4_user_fields_meta, engine4_user_fields_options 
            WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='multi_checkbox' AND engine4_user_fields_meta.label='Conocimientos'";
        
        $conocimientos = $db->fetchAll($query);
      
        foreach($conocimientos as $r)
        {
            $arrayconocimientos[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> conocimientos = $arrayconocimientos;
      */
        $query = "SELECT engine4_user_fields_options.option_id, 
        				engine4_user_fields_options.label 
            			FROM 
            			engine4_user_fields_meta, 
            			engine4_user_fields_options 
				WHERE 
						engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id 
						AND engine4_user_fields_meta.type='select' 
						AND engine4_user_fields_meta.label='Nivel academico'";
        
        $niveles = $db->fetchAll($query);
      
        $arrayniveles = array();
      
        foreach($niveles as $r)
        {
            $arrayniveles[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> niveles = $arrayniveles;
        
        if($this->getRequest()->isPost())
        {
            $values = $this -> getRequest()->getParams();
	
            $table = $this->_helper->api()->getDbtable('jobs', 'JobPosting');

            $db = $table->getAdapter();
            $db->beginTransaction();

            try {
                  $job = $table->createRow();
                  $job -> jobs_nombre = $values['cargo'];
                  $job -> jobs_mision = $values['descripcion'];
                  $job -> jobs_formacion = $values['formacion'];
                  $job -> jobs_experiencia = $values['experiencia'];
                  $job -> jobs_nivel = $values['nivel'];
                  $job -> jobs_lugar = $values['ubicacion'];
                  $job -> jobs_fecha = date('Y-m-d H:i:s');
                  $job -> jobs_conocimientos = $values['conocimientos'];
                  $job -> jobs_competencias = $values['competencias'];
                  
                  if($values['term']=='publicar')
                  {
                    $job -> jobs_estado = 'abierta';
                    $job -> jobs_borrador = 'no';
                  }
                  else
                  {
                    $job -> jobs_estado = 'archivada';
                    $job -> jobs_borrador = 'si';
                  }
                  
                  
                    $viewer = Engine_Api::_()->user()->getViewer();    
                  
                  $job->owner_id = $viewer->user_id;
                  
                  $job -> save();
                  
                  
                  $job_id = $db->lastInsertId();
                  
                  
	
                  /*  $tabledos = $this->_helper->api()->getDbtable('job_Competencia', 'JobPosting');
                    
                    foreach($values['competencias'] as $v)
                    {
                        $jobcompetencia = $tabledos->createRow();
                        $jobcompetencia -> job_competencia_job = $job_id;
                        $jobcompetencia -> job_competencia_competencia = $v;
                        $jobcompetencia -> save();
                    }
	
                    $tabletres = $this->_helper->api()->getDbtable('job_Conocimiento', 'JobPosting');
                    
                    foreach($values['conocimientos'] as $v)
                    {
                        $jobconocimiento = $tabletres->createRow();
                        $jobconocimiento -> job_conocimiento_job = $job_id;
                        $jobconocimiento -> job_conocimiento_conocimiento = $v;
                        $jobconocimiento -> save();
                    }*/

              $db->commit();
            } catch( Exception $e ) {
              $db->rollBack();
              throw $e;
            }
    
            return $this ->_redirect('jobposting/browse');
        }
  }
  
  public function stateAction()
  {
        $id = $this->_getParam('id');

        $jobTable = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $job = $jobTable->find($id)->current();
	
        $db = $jobTable->getAdapter();
        $db->beginTransaction();
        
        try
        {
            if(($job->jobs_estado=='abierta'))
            {
                $job->jobs_estado = 'archivada';
                $job->jobs_borrador = 'no';

                $job->save();
                
                $db->commit();
            }
            else
            {
                $job->jobs_estado = 'abierta';
                $job->jobs_borrador = 'no';

                $job->save();
                
                $db->commit();
            }

        } catch( Exception $e ) {
          $db->rollBack();
          throw $e;
        }
    
        return $this ->_redirect('jobposting/browse');
  }
  
  public function editAction()
  {
        $this->view->id = $id = $this->_getParam('id');

        $jobTable = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $job = $jobTable->find($id)->current();
        
        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('jobs_admin_browse', array(), 'core_public_new_job-posting');
        
        $this->view->cargo = $job->jobs_nombre;
        $this->view->formacion = $job->jobs_formacion;
        $this->view->descripcion = $job->jobs_mision;
        $this->view->experiencia = $job->jobs_experiencia;
        $this->view->nivel = $job->jobs_nivel;
        $this->view->ubicacion = $job->jobs_lugar;
        
        $this->view->competencias = $job->jobs_competencias;
        $this->view->conocimientos = $job->jobs_conocimientos;
        
        $table = Engine_Api::_()->getDbtable('Job_Competencia', 'JobPosting');
	/*$select = $table->select()->where('job_competencia_job='.$id);
	$result = $table->fetchAll($select);
        $array = array();
      
        foreach($result as $r)
        {
            $array[] = $r->job_competencia_competencia;
        }
        
        $this->view->selectedcompetencias = $array;
        */
        /*$table = Engine_Api::_()->getDbtable('Job_Conocimiento', 'JobPosting');
		$select = $table->select()->where('job_conocimiento_job='.$id);
		$result = $table->fetchAll($select);
        $array = array();
      
        foreach($result as $r)
        {
            $array[] = $r->job_conocimiento_conocimiento;
        }
        
        $this->view->selectedconocimientos = $array;*/
        
        $db = $table->getAdapter();
      /*
        $query = "SELECT engine4_user_fields_options.option_id, engine4_user_fields_options.label 
            FROM engine4_user_fields_meta, engine4_user_fields_options 
            WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='multi_checkbox' AND engine4_user_fields_meta.label='Competencias'";
        
        $competencias = $db->fetchAll($query);
      
        $arraycompetencias = array();
      
        foreach($competencias as $r)
        {
            $arraycompetencias[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> competencias = $arraycompetencias;*/
      
   /*     $table = Engine_Api::_()->getDbtable('Conocimientos', 'JobPosting');
	$select = $table->select();
	$result = $table->fetchAll($select);
        $arrayconocimientos = array();
    */
    /*
      
        $query = "SELECT engine4_user_fields_options.option_id, engine4_user_fields_options.label 
            FROM engine4_user_fields_meta, engine4_user_fields_options 
            WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='multi_checkbox' AND engine4_user_fields_meta.label='Conocimientos'";
        
        $conocimientos = $db->fetchAll($query);
      
        foreach($conocimientos as $r)
        {
            $arrayconocimientos[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> conocimientos = $arrayconocimientos;*/
      
        $query = "SELECT 
        			engine4_user_fields_options.option_id, 
        			engine4_user_fields_options.label 
            FROM 
            		engine4_user_fields_meta, 
            		engine4_user_fields_options 
            WHERE 	engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id 
            		AND engine4_user_fields_meta.type='select' 
            		AND engine4_user_fields_meta.label='Nivel academico'";
        
        $niveles = $db->fetchAll($query);
      
        $arrayniveles = array();
      
        foreach($niveles as $r)
        {
            $arrayniveles[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> niveles = $arrayniveles;
        
        $this->renderScript('index/newjob.tpl');
        
        if($this->getRequest()->isPost())
        {
            
        
            /*$table = Engine_Api::_()->getDbtable('Job_Competencia', 'JobPosting');
            $select = $table->select()->where('job_competencia_job='.$id);
            $result = $table->fetchAll($select);
            
            foreach($result as $v)
            {
                $v-> delete();
            }
            
        
            $table = Engine_Api::_()->getDbtable('Job_Conocimiento', 'JobPosting');
            $select = $table->select()->where('job_conocimiento_job='.$id);
            $result = $table->fetchAll($select);
            
            foreach($result as $v)
            {
                $v-> delete();
            }*/
            
            $values = $this -> getRequest()->getParams();
	
            $table = $this->_helper->api()->getDbtable('jobs', 'JobPosting');

            $db = $table->getAdapter();
            $db->beginTransaction();

            try {
                  $job -> jobs_nombre = $values['cargo'];
                  $job -> jobs_mision = $values['descripcion'];
                  $job -> jobs_formacion = $values['formacion'];
                  $job -> jobs_experiencia = $values['experiencia'];
                  $job -> jobs_nivel = $values['nivel'];
                  $job -> jobs_lugar = $values['ubicacion'];
                  $job -> jobs_fecha = date('Y-m-d H:i:s');
                  $job -> jobs_conocimientos = $values['conocimientos'];
                  $job -> jobs_competencias = $values['competencias'];
                  if($values['term']=='publicar')
                  {
                    $job -> jobs_estado = 'abierta';
                    $job -> jobs_borrador = 'no';
                  }
                  else
                  {
                    $job -> jobs_estado = 'archivada';
                    $job -> jobs_borrador = 'si';
                  }
                  $job -> save();
                  
                  
                  $job_id = $id;
                  
                  
	
                    $tabledos = $this->_helper->api()->getDbtable('job_Competencia', 'JobPosting');
                    
                    foreach($values['competencias'] as $v)
                    {
                        $jobcompetencia = $tabledos->createRow();
                        $jobcompetencia -> job_competencia_job = $job_id;
                        $jobcompetencia -> job_competencia_competencia = $v;
                        $jobcompetencia -> save();
                    }
	
                    $tabletres = $this->_helper->api()->getDbtable('job_Conocimiento', 'JobPosting');
                    
                    foreach($values['conocimientos'] as $v)
                    {
                        $jobconocimiento = $tabletres->createRow();
                        $jobconocimiento -> job_conocimiento_job = $job_id;
                        $jobconocimiento -> job_conocimiento_conocimiento = $v;
                        $jobconocimiento -> save();
                    }

                  $db->commit();
    
                return $this ->_redirect('jobposting/browse');
            } catch( Exception $e ) {
              $db->rollBack();
              throw $e;
            }
        }
        
  }
  
  public function delAction()
  {
        $this->view->id = $id = $this->_getParam('id');

        $jobTable = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $job = $jobTable->find($id)->current();

        
            $table = Engine_Api::_()->getDbtable('Job_Competencia', 'JobPosting');
            $select = $table->select()->where('job_competencia_job='.$id);
            $result = $table->fetchAll($select);
            
            foreach($result as $v)
            {
                $v-> delete();
            }
        
            $table = Engine_Api::_()->getDbtable('Job_Conocimiento', 'JobPosting');
            $select = $table->select()->where('job_conocimiento_job='.$id);
            $result = $table->fetchAll($select);
            
            foreach($result as $v)
            {
                $v-> delete();
            }
        
        $job -> delete();
        
        return $this ->_redirect('jobposting/browse');
  }
  
  public function aplicarAction()
  {
      $id = $this -> _getParam('id');
      if(!$this->getRequest()->isPost())
      {
      
        $table = Engine_Api::_()->getDbtable('Jobs', 'JobPosting'); 
        $jobO = $table->find($id)->current();
        
        $db = $table->getAdapter();
        $viewer = Engine_Api::_()->user()->getViewer();    
        
        $table = Engine_Api::_()->getDbtable('Job_Competencia', 'JobPosting');
	$select = $table->select()->where('job_competencia_job='.$id);
	$result = $table->fetchAll($select);
        $array = array();
      
        $this -> view -> paso = true;
        
       /* foreach($result as $r)
        {
         //   $array[] = $r->job_competencia_competencia;
            $query = "SELECT engine4_user_fields_values.privacy
                FROM engine4_user_fields_meta, engine4_user_fields_options, engine4_user_fields_values
                WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='multi_checkbox' 
                AND engine4_user_fields_meta.label IN ('Competencias') AND engine4_user_fields_values.field_id=engine4_user_fields_meta.field_id 
                AND engine4_user_fields_values.value=engine4_user_fields_options.option_id AND engine4_user_fields_values.item_id=".$viewer->user_id." AND engine4_user_fields_values.value=".$r->job_competencia_competencia;

            $hojadevidacompetencias = $db->fetchAll($query);
            if(count($hojadevidacompetencias)<=0)
            {
                $this -> view -> paso = false;
                return;
            }
        }*/
        
        $table = Engine_Api::_()->getDbtable('Job_Conocimiento', 'JobPosting');
	$select = $table->select()->where('job_conocimiento_job='.$id);
	$result = $table->fetchAll($select);
        $array = array();
        
        /*foreach($result as $r)
        {
         //   $array[] = $r->job_competencia_competencia;
            $query = "SELECT engine4_user_fields_values.privacy
                FROM engine4_user_fields_meta, engine4_user_fields_options, engine4_user_fields_values
                WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='multi_checkbox' 
                AND engine4_user_fields_meta.label IN ('Conocimientos') AND engine4_user_fields_values.field_id=engine4_user_fields_meta.field_id 
                AND engine4_user_fields_values.value=engine4_user_fields_options.option_id AND engine4_user_fields_values.item_id=".$viewer->user_id." AND engine4_user_fields_values.value=".$r->job_conocimiento_conocimiento;

            $hojadevidaconocimientos = $db->fetchAll($query);
            if(count($hojadevidaconocimientos)<=0)
            {
                $this -> view -> paso = false;
                return;
            }
        }*/
        
        
         //   $array[] = $r->job_competencia_competencia;
        /*$query = "SELECT engine4_user_fields_values.privacy
            FROM engine4_user_fields_meta, engine4_user_fields_options, engine4_user_fields_values
            WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='select' 
            AND engine4_user_fields_meta.label IN ('Nivel academico') AND engine4_user_fields_values.field_id=engine4_user_fields_meta.field_id 
            AND engine4_user_fields_values.value=engine4_user_fields_options.option_id AND engine4_user_fields_values.item_id=".$viewer->user_id." AND engine4_user_fields_values.value=".$jobO->jobs_nivel;

        $hojadevidaconocimientos = $db->fetchAll($query);
        if(count($hojadevidaconocimientos)<=0)
        {
            $this -> view -> paso = false;
            return;
        }*/
        
        
        $table = Engine_Api::_()->getDbtable('Job_Usuario', 'JobPosting');      
        $viewer = Engine_Api::_()->user()->getViewer();    



        $id = $this->_getParam('id');
      
        $jobTable = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $job = $jobTable->find($id)->current();
      
        try 
        {
              $db = $table->getAdapter();
              $db->beginTransaction();
              $apli = $table->createRow();

              $apli->usuarios_job = $this->_getParam('id');
              $apli->usuarios_usuario = $viewer->user_id;
              $apli->usuarios_estado = 'postulado';
              $apli->usuarios_fecha = date('Y-m-d H:i:s');
              $apli->owner_id = $viewer->user_id;

              $apli->save();

                  $postulaciones = $job->jobs_postulaciones;
                  $job->jobs_postulaciones = $postulaciones+1;
                  $job->save();
                  
                  

                    $recipient = $viewer->email;
                  
                  $mailParams = array();
                  
                  $mailParams['queue'] = false;
                  $mailParams['job_cargo'] = $job->jobs_nombre;
                  
                  Engine_Api::_()->getApi('mail', 'core')->sendSystem(
                    $recipient,
                    'job-posting_aplicar',
                    $mailParams
                    );
                  
              $db->commit();

        } 
        catch( Exception $e ) {
                $db->rollBack();
                throw $e;
              }
          
          return;
      }
    
    return $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => 10,
      'parentRefresh' => 10,
      'messages' => array('')
    ));
  }
  
  public function aplicarDosAction()
  {
      $table = Engine_Api::_()->getDbtable('Job_Usuario', 'JobPosting');      
      $viewer = Engine_Api::_()->user()->getViewer();    

      $id = $this->_getParam('id');
      
        $jobTable = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $job = $jobTable->find($id)->current();
      
      try 
      {
            $db = $table->getAdapter();
            $db->beginTransaction();
            $apli = $table->createRow();
            
            $apli->usuarios_job = $this->_getParam('id');
            $apli->usuarios_usuario = $viewer->user_id;
            $apli->usuarios_estado = 'postulado';
            $apli->usuarios_fecha = date('Y-m-d H:i:s');
            $apli->owner_id = $viewer->user_id;
            
            $apli->save();
            
            
            
                $postulaciones = $job->jobs_postulaciones;
                $job->jobs_postulaciones = $postulaciones+1;
                $job->save();
            
            $db->commit();
            die('Su postulacion ha sido recibida');
                  
      } 
      catch( Exception $e ) {
              $db->rollBack();
              throw $e;
            }
  }
  
  public function popularityAction()
  {
        $translate = Zend_Registry::get('Zend_Translate');
        
        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('job_user_browse', array(), 'core_public_user_job-posting');     
      
        $table = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
	
        $arraycompetencias = array();
        
        $db = $table->getAdapter();
        $viewer = Engine_Api::_()->user()->getViewer();    
      
        $query = "SELECT engine4_user_fields_values.privacy
            FROM engine4_user_fields_meta, engine4_user_fields_options, engine4_user_fields_values
            WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='multi_checkbox' AND engine4_user_fields_meta.label IN ('Competencias', 'Conocimientos') AND engine4_user_fields_values.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_values.value=engine4_user_fields_options.option_id AND engine4_user_fields_values.item_id=".$viewer->user_id;
        
        $hojadevida = $db->fetchAll($query);
        
        $this->view->errorjobs = '';
        
        if(count($hojadevida)<1)
        {
            $this->view->errorjobs = $translate->translate('Lo invitamos a actualizar su perfil para poder postularse a las vacantes');
        }
      
        $query = "SELECT engine4_user_fields_options.option_id, engine4_user_fields_options.label 
            FROM engine4_user_fields_meta, engine4_user_fields_options 
            WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='multi_checkbox' AND engine4_user_fields_meta.label='Competencias'";
        
        $competencias = $db->fetchAll($query);
      
        $arraycompetencias = array();
      
        foreach($competencias as $r)
        {
            $arraycompetencias[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> competencias = $arraycompetencias;
      
   /*     $table = Engine_Api::_()->getDbtable('Conocimientos', 'JobPosting');
	$select = $table->select();
	$result = $table->fetchAll($select);
        $arrayconocimientos = array();
    */
      
        $query = "SELECT engine4_user_fields_options.option_id, engine4_user_fields_options.label 
            FROM engine4_user_fields_meta, engine4_user_fields_options 
            WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='multi_checkbox' AND engine4_user_fields_meta.label='Conocimientos'";
        
        $conocimientos = $db->fetchAll($query);
      
        foreach($conocimientos as $r)
        {
            $arrayconocimientos[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> conocimientos = $arrayconocimientos;
        
        $viewer = Engine_Api::_()->user()->getViewer();    
        
        $userJobTable = Engine_Api::_()->getDbtable('Job_Usuario', 'JobPosting');
        
        $table = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $select = $table->select()
                ->where("jobs_estado='abierta'");
        
        $values = $this->getRequest()->getParams();
        
        $this->view->formValues = $values;
        
        if(($this->getRequest()->isPost()) || ($values['filterpost']))
        {
            unset($values['module']);
            unset($values['controller']);
            unset($values['action']);
            unset($values['rewrite']);
            $values['filterpost'] = true;
            $this->view->formValues =  $values;
            if(isset($values['competencias']))
            {
                $tabledos = $this->_helper->api()->getDbtable('job_Competencia', 'JobPosting');
                $select->from($table->info('name'))
                        ->join($tabledos->info('name'), $tabledos->info('name').'.job_competencia_job='.$table->info('name').'.jobs_id', null)
                        ->where($tabledos->info('name').'.job_competencia_competencia IN (?)', $values['competencias']);
            }
            if(isset($values['conocimientos']))
            {
                $tabledos = $this->_helper->api()->getDbtable('job_Conocimiento', 'JobPosting');
                $select->from($table->info('name'))
                        ->join($tabledos->info('name'), $tabledos->info('name').'.job_conocimiento_job='.$table->info('name').'.jobs_id', null)
                        ->where($tabledos->info('name').'.job_conocimiento_conocimiento IN (?)', $values['conocimientos']);
            }
            if($values['palabras']!='')
            {
                $palabrasArray = explode(' ', $values['palabras']);
                
                $arrayfields = array('jobs_nombre', 'jobs_mision', 'jobs_formacion', 'jobs_experiencia', 'jobs_lugar');
                $arrayquerys = array();
                
                foreach($palabrasArray as $palabra)
                {
                    foreach($arrayfields as $v)
                    {
                        $arrayquerys[] = "(".$v." LIKE '%".$palabra."%')";
                    }
                }
                
                $select->where(join(' OR ', $arrayquerys));
            }
            if($values['cargo']!='')
            {
                $select->where("(jobs_nombre LIKE '%".$values['cargo']."%')");
            }
            if($values['sede']!='')
            {
                $select->where("(jobs_lugar LIKE '%".$values['sede']."%')");
            }
        }
        
        $select->order('jobs_visitas DESC');
        
        $page = $this->_getParam('page', 1);
        
        $this->view->paginator = $paginator = Zend_Paginator::factory($select);
        $this->view->paginator = $paginator->setCurrentPageNumber( $page );
  }
  
  public function viewAction()
  {
        $this->view->id = $id = $this->_getParam('id');

        $jobTable = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $job = $jobTable->find($id)->current();
        //echo "<pre>".print_r($job,true)."</pre>";
        /*$table = Engine_Api::_()->getDbtable('Job_Competencia', 'JobPosting');
	$select = $table->select()->where('job_competencia_job='.$id);
	$result = $table->fetchAll($select);
        $array = array();
      
        foreach($result as $r)
        {
            $array[] = $r->job_competencia_competencia;
        }
        
        $this->view->selectedcompetencias = $array;*/
        
        $table = Engine_Api::_()->getDbtable('Job_Conocimiento', 'JobPosting');
	/*$select = $table->select()->where('job_conocimiento_job='.$id);
	$result = $table->fetchAll($select);
        $array = array();
      
        foreach($result as $r)
        {
            $array[] = $r->job_conocimiento_conocimiento;
        }
        
        $this->view->selectedconocimientos = $array;*/
        
        $db = $table->getAdapter();
      
        /*$query = "SELECT engine4_user_fields_options.option_id, engine4_user_fields_options.label 
            FROM engine4_user_fields_meta, engine4_user_fields_options 
            WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='multi_checkbox' AND engine4_user_fields_meta.label='Competencias'";
        
        $competencias = $db->fetchAll($query);
      
        $arraycompetencias = array();
      
        foreach($competencias as $r)
        {
            $arraycompetencias[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> competencias = $arraycompetencias;*/
      
   /*     $table = Engine_Api::_()->getDbtable('Conocimientos', 'JobPosting');
	$select = $table->select();
	$result = $table->fetchAll($select);
        $arrayconocimientos = array();
    */
      
        /*$query = "SELECT engine4_user_fields_options.option_id, engine4_user_fields_options.label 
            FROM engine4_user_fields_meta, engine4_user_fields_options 
            WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='multi_checkbox' AND engine4_user_fields_meta.label='Conocimientos'";
        
        $conocimientos = $db->fetchAll($query);
      
        foreach($conocimientos as $r)
        {
            $arrayconocimientos[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> conocimientos = $arrayconocimientos;*/
      
        $query = "SELECT engine4_user_fields_options.option_id, engine4_user_fields_options.label 
            FROM engine4_user_fields_meta, engine4_user_fields_options 
            WHERE engine4_user_fields_options.field_id=engine4_user_fields_meta.field_id AND engine4_user_fields_meta.type='select' AND engine4_user_fields_meta.label='Nivel academico'";
        
        $niveles = $db->fetchAll($query);
      
        $arrayniveles = array();
      
        foreach($niveles as $r)
        {
            $arrayniveles[$r['option_id']] = $r['label'];
        }
        
        $this -> view -> niveles = $arrayniveles;
        
        $viewer = Engine_Api::_()->user()->getViewer();    

        $canAdmin = $canCreate = Engine_Api::_()->authorization()->isAllowed('JobPosting', $viewer, 'creates');
        if(!$canAdmin)
        {
            $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('job_user_browse', array(), 'core_public_user_job-posting');     
            
            try 
            {
                $db = $table->getAdapter();
                $db->beginTransaction();
            
                $visitas = $job->jobs_visitas;
                $job->jobs_visitas = $visitas+1;
                $job->save();
                
                $db->commit();
        
                $this -> view -> job = $job;
                  
            } 
            catch( Exception $e ) {
                    $db->rollBack();
                    throw $e;
                  }
                
        }
        else
        {
            $this -> view -> job = $job;
            $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('jobs_admin_browse', array(), 'core_public_aspirantes_job-posting');     
            
            $table = Engine_Api::_()->getDbtable('Job_Usuario', 'JobPosting');
            $select = $table->select()
                    ->where('usuarios_job='.$id);

            $result = $table->fetchAll($select);
            
            $this -> view -> aspirantes = $result;
            
            $this->renderScript('index/viewadmin.tpl');
        }
  }
  
  public function exportjobAction()
  {
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("content-disposition: attachment;filename=jobs.xlsx");
      
        $table = Engine_Api::_()->getDbtable('Competencias', 'JobPosting');
	$select = $table->select();
	$result = $table->fetchAll($select);
        $arraycompetencias = array();
      
        foreach($result as $r)
        {
            $arraycompetencias[$r->getIdentity()] = $r->competencias_competencia;
        }
      
		$table = Engine_Api::_()->getDbtable('Conocimientos', 'JobPosting');
		$select = $table->select();
		$result = $table->fetchAll($select);
        $arrayconocimientos = array();
      
        foreach($result as $r)
        {
            $arrayconocimientos[$r->getIdentity()] = $r->conocimiento_conocimiento;
        }
      
        $tablen = Engine_Api::_()->getDbtable('Niveles', 'JobPosting');
	$select = $tablen->select();
	$niveles = $tablen->fetchAll($select);
        $arrayniveles = array();
      
        foreach($niveles as $r)
        {
            $arrayniveles[$r->getIdentity()] = $r->niveles_nivel;
        }


        $table = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $select = $table->select();
        
        $jobs = $table->fetchAll($select);
        $table = '<table>
                <thead>
                    <tr>
                        <th>
                            Cargo
                        </th>
                        <th>
                            Descripci&oacute;n
                        </th>
                        <th>
                            Formaci&oacute;n
                        </th>
                        <th>
                            Experiencia
                        </th>
                        <th>
                            Nivel
                        </th>
                        <th>
                            Area
                        </th>
                        <th>
                            Estado
                        </th>
                        <th>
                            Competencias requeridas
                        </th>
                        <th>
                            Conocimientos previos requeridos
                        </th>
                        <th>
                            Fecha creaci&oacute;n
                        </th>
                        <th>
                            Cantidad de visitas
                        </th>
                        <th>
                            Cantidad de postulaciones
                        </th>
                        <th>
                            Creado por
                        </th>
                    </tr>
                </thead><tbody>';
        
        foreach($jobs as $v)
        {
            $tableJC = Engine_Api::_()->getDbtable('Job_Competencia', 'JobPosting');
            $select = $tableJC->select()->where('job_competencia_job='.$v->jobs_id);
            $competencias = $tableJC->fetchAll($select);
            $array = array();

            foreach($competencias as $r)
            {
                $array[] = $arraycompetencias[$r->job_competencia_competencia];
            }

            $competencias = join(', ', $array);
            
            $tableJC = Engine_Api::_()->getDbtable('Job_Conocimiento', 'JobPosting');
            $select = $tableJC->select()->where('job_conocimiento_job='.$v->jobs_id);
            $conocimientos = $tableJC->fetchAll($select);
            $array = array();

            foreach($conocimientos as $r)
            {
                $array[] = $arrayconocimientos[$r->job_conocimiento_conocimiento];
            }

            $conocimientos = join(', ', $array);
            
            $table .= '<tr>
                    <td>
                        '.$v->jobs_nombre.'
                    </td>
                    <td>
                        '.$v->jobs_mision.'
                    </td>
                    <td>
                        '.$v->jobs_formacion.'
                    </td>
                    <td>
                        '.$v->jobs_experiencia.'
                    </td>
                    <td>
                        '.$arrayniveles[$v->jobs_nivel].'
                    </td>
                    <td>
                        '.$v->jobs_lugar.'
                    </td>
                    <td>
                        '.$v->jobs_estado.'
                    </td>   
                    <td>
                        '.$competencias.'
                    </td>
                    <td>
                        '.$conocimientos.'
                    </td>
                    <td>
                        '.$v->jobs_fecha.'
                    </td>
                    <td>
                        '.$v->jobs_visitas.'
                    </td>
                    <td>
                        '.$v->jobs_postulaciones.'
                    </td>
                    <td>
                        '.$v->getOwner()->getTitle().'
                    </td>
                </tr>';
        }
        
        $table .= '</tbody></table>';
        
        echo $table;
        die();
  }
  
  public function preseleccionarAction()
  {
        $job = $this ->_getParam('job');
        $user = $this ->_getParam('owner');
            
        $table = Engine_Api::_()->getDbtable('Job_Usuario', 'JobPosting');
        $select = $table->select()
                ->where('usuarios_job='.$job.' AND usuarios_usuario='.$user);

        $result = $table->fetchAll($select);
        $db = $table->getAdapter();
            
        try
        {
            $db->beginTransaction();
        
        foreach($result as $v)
        {
            $v->usuarios_estado = 'preseleccionado';
            $v-> save();
            
            
                    $recipient = $v->getOwner()->email;
                  
                  $mailParams = array();
                  
                  $mailParams['queue'] = false;
                  $mailParams['job_cargo'] = $job->jobs_nombre;
                  
                  Engine_Api::_()->getApi('mail', 'core')->sendSystem(
                    $recipient,
                    'job-posting_preseleccionar',
                    $mailParams
                    );
            
        }

              $db->commit();
            } catch( Exception $e ) {
              $db->rollBack();
              throw $e;
            }
            
            return $this ->_redirect('jobposting/view/id/'.$job);
  }
  

  
  public function descartarAction()
  {
        $job = $this ->_getParam('job');
        $user = $this ->_getParam('owner');
            
        $table = Engine_Api::_()->getDbtable('Job_Usuario', 'JobPosting');
        $select = $table->select()
                ->where('usuarios_job='.$job.' AND usuarios_usuario='.$user);

        $result = $table->fetchAll($select);
        

            $db = $table->getAdapter();
            $db->beginTransaction();
        try
        {
            
        
        foreach($result as $v)
        {
            $v->usuarios_estado = 'rechazado';
            $v-> save();
            
            $recipient = $v->getOwner()->email;
                  
                  $mailParams = array();
                  
                  $mailParams['queue'] = false;
                  $mailParams['job_cargo'] = $job->jobs_nombre;
                  
                  Engine_Api::_()->getApi('mail', 'core')->sendSystem(
                    $recipient,
                    'job-posting_descartar',
                    $mailParams
                    );
        }

              $db->commit();
            } catch( Exception $e ) {
              $db->rollBack();
              throw $e;
            }
            
            return $this ->_redirect('jobposting/view/id/'.$job);
  }
  
  public function exportaplyAction()
  {
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("content-disposition: attachment;filename=postulaciones.xlsx");
        
        $job = $this ->_getParam('id');
            
        $table = Engine_Api::_()->getDbtable('Job_Usuario', 'JobPosting');
        $select = $table->select()
                ->where('usuarios_job='.$job);

        $result = $table->fetchAll($select);
        
        $cadena = '<table>
            <thead>
                <tr>
                    <th>
                        Vacante
                    </th>
                    <th>
                        Usuario
                    </th>
                    <th>
                        Fecha
                    </th>
                    <th>
                        Estado
                    </th>
                </tr>
            </thead>
            <tbody>';
        
        foreach($result as $v)
        {

            $jobTable = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
            $jobO = $jobTable->find($job)->current();
            
            $cadena .= '<tr>
                    <td>
                        '.$jobO->jobs_nombre.'
                    </td>
                    <td>
                        '.$v->getOwner()->getTitle().'
                    </td>
                    <td>
                        '.$v->usuarios_fecha.'
                    </td>
                    <td>
                        '.$v->usuarios_estado.'
                    </td>
                </tr>';
        }
        
        $cadena .= '</tbody></table>';
        
        echo $cadena;
        
        die();        
  }
  
  public function cerrarAction()
  {
        $this->view->id = $id = $this->_getParam('id');

        $jobTable = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $job = $jobTable->find($id)->current();
        
        $job -> jobs_estado = 'cerrada';
        $job -> save();
            
        $table = Engine_Api::_()->getDbtable('Job_Usuario', 'JobPosting');
        $select = $table->select()
                ->where('usuarios_job='.$id." AND usuarios_estado!='preseleccionado'");

        $result = $table->fetchAll($select);

        $db = $table->getAdapter();
        $db->beginTransaction();        
        try
        {
            foreach($result as $v)
            {
                $v->usuarios_estado = 'rechazado';
                $v->save();
                
                $recipient = $v->getOwner()->email;
                  
                  $mailParams = array();
                  
                  $mailParams['queue'] = false;
                  $mailParams['job_cargo'] = $job->jobs_nombre;
                  
                  Engine_Api::_()->getApi('mail', 'core')->sendSystem(
                    $recipient,
                    'job-posting_descartar',
                    $mailParams
                    );
            }
            $db-> commit();
        }
        catch(Exception $e)
        {
              $db->rollBack();
              throw $e;
        }
        
        return $this ->_redirect('jobposting/view/id/'.$id);
  }
  
  public function statsAction()
  {      
        $table = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        
        $db = $table->getAdapter();
        $viewer = Engine_Api::_()->user()->getViewer();    
      
        $query = "SELECT COUNT(*) AS cantidad FROM engine4_jobposting_jobs";
        
        $cantidadvacantes = $db->fetchAll($query);
      
        $query = "SELECT SUM(jobs_visitas) AS cantidad FROM engine4_jobposting_jobs";
        
        $cantidadvisitas = $db->fetchAll($query);
      
        $query = "SELECT SUM(jobs_postulaciones) AS cantidad FROM engine4_jobposting_jobs";
        
        $cantidadpostulaciones = $db->fetchAll($query);
        
        $this -> view -> vacantes = $cantidadvacantes[0]["cantidad"];
        $this -> view -> cantidadvisitas = $cantidadvisitas[0]["cantidad"];
        $this -> view -> cantidadpostulaciones = $cantidadpostulaciones[0]["cantidad"];
        
      $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('jobs_admin_browse', array(), 'core_public_stats_job-posting');     
      
      
	
        $page=$this->_getParam('page',1);

        $table = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $select = $table->select();
        
        $values = $this->getRequest()->getParams();
        
        $this->view->formValues = $values;
        
        if(($this->getRequest()->isPost()) || ($values['filterpost']))
        {
            unset($values['module']);
            unset($values['controller']);
            unset($values['action']);
            unset($values['rewrite']);
            $values['filterpost'] = true;
            $this->view->formValues =  $values;
            if($values['palabras']!='')
            {
                $palabrasArray = explode(' ', $values['palabras']);
                
                $arrayfields = array('jobs_nombre', 'jobs_mision', 'jobs_formacion', 'jobs_experiencia', 'jobs_lugar');
                $arrayquerys = array();
                
                foreach($palabrasArray as $palabra)
                {
                    foreach($arrayfields as $v)
                    {
                        $arrayquerys[] = "(".$v." LIKE '%".$palabra."%')";
                    }
                }
                
                $select->where(join(' OR ', $arrayquerys));
            }
            if($values['cargo']!='')
            {
                $select->where("(jobs_nombre LIKE '%".$values['cargo']."%')");
            }
            if($values['sede']!='')
            {
                $select->where("(jobs_lugar LIKE '%".$values['sede']."%')");
            }
        }

        // Make paginator
        $this->view->paginator = $paginator = Zend_Paginator::factory($select);
        $this->view->paginator = $paginator->setCurrentPageNumber( $page );
  }
  
  public function exportstatsAction()
  {      
        $table = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        
        $db = $table->getAdapter();
        $viewer = Engine_Api::_()->user()->getViewer();    
      
        $query = "SELECT COUNT(*) AS cantidad FROM engine4_jobposting_jobs";
        
        $cantidadvacantes = $db->fetchAll($query);
      
        $query = "SELECT SUM(jobs_visitas) AS cantidad FROM engine4_jobposting_jobs";
        
        $cantidadvisitas = $db->fetchAll($query);
      
        $query = "SELECT SUM(jobs_postulaciones) AS cantidad FROM engine4_jobposting_jobs";
        
        $cantidadpostulaciones = $db->fetchAll($query);
      
      
	
        $page=$this->_getParam('page',1);

        $table = Engine_Api::_()->getDbtable('Jobs', 'JobPosting');
        $select = $table->select();
        
        $result = $table->fetchAll($select);
        
        
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("content-disposition: attachment;filename=jobs.xlsx");
        
        
        $table = '<table>
                <tbody>
                    <tr>
                        <td>
                            Vacantes publicadas: '.$cantidadvacantes[0]['cantidad'].'
                        </td>
                        <td>
                            Cantidad de visitas: '.$cantidadvisitas[0]['cantidad'].'
                        </td>
                        <td>
                            Cantidad de postulaciones: '.$cantidadvisitas[0]['cantidad'].'
                        </td>
                    </tr>
                    <tr>
                        <th>
                        </th>
                        <th>
                        </th>
                        <th>
                        </th>
                    </tr>
                    <tr>
                        <th>
                            Vacante
                        </th>
                        <th>
                            Visitas
                        </th>
                        <th>
                            Postulaciones
                        </th>
                    </tr>';
        
        foreach($result as $v)
        {
            $table .= '
                    <tr>
                        <td>
                            '.$v->jobs_nombre.'
                        </td>
                        <td>
                            '.$v->jobs_visitas.'
                        </td>
                        <td>
                            '.$v->jobs_postulaciones.'
                        </td>
                    </tr>';
        }
        
        $table .= '</tbody></table>';
        
         echo $table;
        
        die();
  }
  
}
