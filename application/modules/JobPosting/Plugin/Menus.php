<?php

class JobPosting_Plugin_Menus
{
  public function onMenuInitialize_CoreMiniProfileJp($row)
  {
    $viewer = Engine_Api::_()->user()->getViewer();
    if( $viewer->getIdentity() ) {
      return array(
        'label' => $row->label,
        'uri' => $viewer->getHref(),
      );
    }
    
    return false;
  }
}
