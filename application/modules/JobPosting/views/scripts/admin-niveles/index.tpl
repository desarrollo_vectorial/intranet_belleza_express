<?php if( count($this->navigation) ): ?>
  <div class='tabs'>
    <?php
      // Render the menu
      //->setUlClass()
      echo $this->navigation()->menu()->setContainer($this->navigation)->render()
    ?>
  </div>
<?php endif; ?>

<script type="text/javascript">

function multiDelete()
{
  return confirm("<?php echo $this->translate("Are you sure you want to delete the selected levels?") ?>");
}

function selectAll()
{
  var i;
  var multidelete_form = $('multidelete_form');
  var inputs = multidelete_form.elements;
  for (i = 1; i < inputs.length; i++) {
    if (!inputs[i].disabled) {
      inputs[i].checked = inputs[0].checked;
    }
  }
}
</script>

                <h2>
  <?php echo $this->translate('Listado de niveles') ?>
</h2>

          <?php echo $this->htmlLink(
                array('route' => 'admin_default', 'module' => 'job-posting', 'controller' => 'niveles', 'action' => 'create',),
                $this->translate("New"),
                array('class' => 'smoothbox buttonlink')) ?>
<br />
<br />
<?php if( count($this->paginator) ): ?>
<form id='multidelete_form' method="post" action="<?php echo $this->url();?>" onSubmit="return multiDelete()">
<table class='admin_table'>
  <thead>
    <tr>
      <th class='admin_table_short'><input onclick='selectAll();' type='checkbox' class='checkbox' /></th>
      <th>ID</th>
      <th><?php echo $this->translate("Nivel") ?></th>
      <th><?php echo $this->translate("Actions") ?></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($this->paginator as $item): ?>
      <tr>
        <td>
            <input type='checkbox' class='checkbox' name='delete_<?php echo $item->getIdentity(); ?>' value="<?php echo $item->getIdentity(); ?>" />
        </td>
        <td>
            <?php echo $item->niveles_id; ?>
        </td>
        <td>
            <?php echo $item->niveles_nivel; ?>
        </td>
        <td>
          <?php echo $this->htmlLink(
                array('route' => 'admin_default', 'module' => 'job-posting', 'controller' => 'niveles', 'action' => 'edit', 'id' => $item->getIdentity()),
                $this->translate("edit"),
                array('class' => 'smoothbox')) ?>
                |
          <?php echo $this->htmlLink(
                array('route' => 'admin_default', 'module' => 'job-posting', 'controller' => 'niveles', 'action' => 'delete', 'id' => $item->getIdentity()),
                $this->translate("delete"),
                array('class' => '', 'onclick' => "return confirm('".$this->translate('Are you sure').'?'."');")) ?>
        </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<br />

<div class='buttons'>
  <button type='submit'><?php echo $this->translate("Delete Selected") ?></button>
</div>
<br/>
<div>
  <?php echo $this->paginationControl($this->paginator); ?>
</div>

<?php else: ?>
  <div class="tip">
    <span>
      <?php echo $this->translate("No hay niveles creados.") ?>
    </span>
  </div>
<?php endif; ?>