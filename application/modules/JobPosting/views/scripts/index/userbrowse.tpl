<div class="layout_page_user_index_home user_jobposting">
    <h1>&nbsp;</h1><!-- <h1><?php echo $this->translate('Job Posting Colombina'); ?></h1>-->
    <div class="generic_layout_container layout_main">
        <div class="generic_layout_container layout_left">
            <div class="search">
                <form method="post" action="<?php echo $this -> url(); ?>">
                    <fieldset>
                        <legend>
                            <?php echo $this -> translate('Buscar empleo'); ?>
                        </legend>
                        <label for="palabras">
                            <?php echo $this -> translate('Palabras claves'); ?>
                        </label>
                        <input type="text" name="palabras" id="palabras" value="<?php echo $this->formValues['palabras']; ?>" />
                        <label for="cargo">
                            <?php echo $this -> translate('Cargo laboral'); ?>
                        </label>
                        <input type="text" name="cargo" id="cargo" value="<?php echo $this->formValues['cargo']; ?>" />
                        <label for="sede">
                            <?php echo $this -> translate('Sede'); ?>
                        </label>
                        <input type="text" name="sede" id="sede" value="<?php echo $this->formValues['sede']; ?>" />
                    </fieldset>
                    <a href="#" id="mostrar_busqueda_avanzada"><?php echo $this -> translate('Busqueda avanzada'); ?></a>
                    <a href="#" id="ocultar_busqueda_avanzada"><?php echo $this -> translate('Busqueda avanzada'); ?></a>
                    <div id="busqueda_avanzada">
                        <!-- <label for="competencias">
                            <?php echo $this -> translate('Competencias'); ?>
                        </label>
                        <div class="jp_checkbox_group">
                            <?php
                           /*
                                foreach($this->competencias as $k => $v)
                                {
                                    $checked = '';
                                    if(in_array($k, $this->formValues['competencias']))
                                    {
                                        $checked = 'checked="checked"';
                                    }
                            ?>
                            <input type="checkbox" name="competencias[]" id="competencias" value="<?php echo $k ?>" <?php echo $checked; ?> />&nbsp;<?php echo $v; ?>
                            <br />
                            <?php
                                }*/
                            ?>
                        </div>
                        <br />
                        <label for="conocimientos">
                            <?php echo $this -> translate('Conocimientos'); ?>
                        </label>
                        <div class="jp_checkbox_group">
                            <?php
                            /*
                                foreach($this->conocimientos as $k => $v)
                                {
                                    $checked = '';
                                    if(in_array($k, $this->formValues['conocimientos']))
                                    {
                                        $checked = 'checked="checked"';
                                    }
                            ?>
                            <input type="checkbox" name="conocimientos[]" id="conocimientos" value="<?php echo $k ?>" <?php echo $checked; ?> />&nbsp;<?php echo $v; ?>
                            <br />
                            <?php
                                }*/
                            ?>
                        </div>
                        <br />-->
                    </div>
                    <input type="submit" value="<?php echo $this -> translate('Buscar'); ?>" />
                </form>
            </div>
			
			<div style="text-align:center;">
				<a href="/public/admin/politica_job_posting.pdf" target="NEW">Política de Job Posting</a>
			</div>
        </div>
        <div class="generic_layout_container layout_right">
            <div id="banner">
                <?php echo $this -> translate('Banner capacitaciones'); ?>
            </div>
        </div>
        <div class="generic_layout_container layout_middle">
            <span class="error"><?php echo $this -> errorjobs; ?></span>
            <?php 
            //echo "<pre>".print_r($this->navigation,true)."</pre>";
            if( count($this->navigation) ): ?>
                <div class='tabs'>
                  <?php
                    // Render the menu
                    //->setUlClass()
                    echo $this->navigation()->menu()->setContainer($this->navigation)->render()
                  ?>
                </div>
            <?php endif; ?>
            <?php if( count($this->paginator) ): ?>
                <br/>
                <ul class="job_list">
                
                <li class="current"><?php echo $this -> translate('Resultados'); ?></li><li style="display:none;"><?php 
                                echo $this->htmlLink
                                (
                                    array
                                    (
                                        'route' =>  'jobposting-popularity'
                                    ),
                                    $this->translate('Destacados')
                                );
                            ?></li></ul>
                <?php foreach ($this->paginator as $item): ?>
                    <div class="job_item">
                        <h3>
                            <?php 
                                echo $this->htmlLink
                                (
                                    array
                                    (
                                        'route' =>  'jobposting-view', 'id' => $item->getIdentity()
                                    ),
                                    $item->jobs_nombre,
                                    array
                                    (
                                        'class' => 'option_view'
                                    )
                                );
                            ?>
                        </h3>
                        <span class="lugar">
                            <?php echo $item->jobs_lugar; ?>
                        </span>
                        <span class="fecha">
                            <?php echo strftime('%e/%b/%Y', strtotime($item->jobs_fecha)); ?>
                        </span>
                        <span class="descripcion">
                            <?php echo $item->jobs_mision; ?>
                        </span>
                        <div class="published_visitas">
                            <span class="published">
                                <?php echo $this->translate('Posted by %s', $item->getOwner()->toString()); ?>
                            </span>
                            <span class="visitas">
                                <?php echo $item->jobs_visitas.' '.$this->translate('Visitas'); ?>
                            </span>
                            <?php
                                if($item->getAplicar())
                                {
                                    echo $this->htmlLink
                                    (
                                        array
                                        (
                                            'route' =>  'jobposting-aplicar', 'id' => $item->getIdentity()
                                        ),
                                        '<input type="submit" value="'.$this -> translate('Aplicar').'" />',
                                        array
                                        (
                                            'class' => 'smoothbox'
                                        )
                                    );
                                }
                                else
                                {
                                    echo '<input type="submit" value="'.$this -> translate('Ya aplicaste a esta vacante').'" />';
                                }
                            ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <div>
                <?php echo $this->paginationControl($this->paginator, null, null, array('query' => $this->formValues)); ?>
            </div>
            <?php else: ?>
              <div class="tip">
                <span>
                  <?php echo $this->translate("No hay vacantes disponibles.") ?>
                </span>
              </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<script>
    $('mostrar_busqueda_avanzada').addEvent
    (
            'click',
            function()
            {
                $('ocultar_busqueda_avanzada').show();
                $('mostrar_busqueda_avanzada').hide();
                $('busqueda_avanzada').show();
                
                return false;
            }
    );
    
    $('ocultar_busqueda_avanzada').addEvent
    (
            'click',
            function()
            {
                $('ocultar_busqueda_avanzada').hide();
                $('mostrar_busqueda_avanzada').show();
                $('busqueda_avanzada').hide();
                
                return false;
            }
    );
    
</script>