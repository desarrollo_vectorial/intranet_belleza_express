<?php
    $class = 'error';
    $titulo = '¡No fue posible aplicar a esta vacante!';
	$mensaje = 'Ha ocurrido un error. Es posible que la información de su hoja de vida no esté completa, por favor vaya a su perfil y actualícela para poder aplicar nuevamente.';
	$mensaje .= '<br>';
	$mensaje .= '<br>';
	$mensaje .= '<a href="members/edit/profile" target="NEW">!Actualizar mi perfil ahora¡</a>';
	
    if($this->paso)
    {
        $class = 'success';
		$titulo = 'Gracias por inscribirte.';
        $mensaje = 'Ha aplicado exitosamente a la oferta de empleo. A partir de este momento la informacion sobre el proceso de seleccion, sera enviada a su correo electronico.';
    }
?>
<style type="text/css">
    .wrapper_modal
    {
        float: left;
        position: relative;
        width: 527px;
        min-height: 1px;
        height: auto;
    }

    .wrapper_modal .header_modal
    {
        float: left;
        position: relative;
        width: 96%;
        min-height: 1px;
        height: auto;
        padding: 2%;
        font-size: 1.5em;
        font-weight: bolder;
        color: #999996;
    }

    .wrapper_modal .aplica_error
    {
        float: left;
        position: relative;
        width: 68%;
        min-height: 1px;
        height: auto;
        padding: 2% 2% 2% 30%;
        font-size: 1em;
        font-weight: normal;
        color: #999996;
        background: url(/application/modules/JobPosting/externals/images/modal_error.jpg) no-repeat scroll left center;
    }

    .wrapper_modal .aplica_success
    {
        float: left;
        position: relative;
        width: 68%;
        min-height: 1px;
        height: auto;
        padding: 2% 2% 2% 30%;
        font-size: 1em;
        font-weight: normal;
        color: #999996;
        background: url(/application/modules/JobPosting/externals/images/modal_ok.jpg) no-repeat scroll left center;
    }

    .wrapper_modal .aplica_error input[type="submit"], .wrapper_modal .aplica_success input[type="submit"]
    {
        background: url("/application/modules/JobPosting/externals/images/boton.jpg") no-repeat scroll left top transparent;
        border: 0 none;
        color: #FFFFFF;
        cursor: pointer;
        float: right;
        margin: 10px 5px;
        padding: 12px 30px;
        position: relative;
        text-align: center;
        text-decoration: none;
        width: 111px;
    }
</style>
<div class="wrapper_modal">
    <div class="header_modal">
        <?php echo $this -> translate($titulo); ?>
    </div>
    <div class="aplica_<?php echo $class; ?>">
        <?php echo $this -> translate($mensaje); ?>
        <form method="post" action=<?php echo $this -> url(); ?>">
              <input type="hidden" name="estado" value="<?php echo $this->paso; ?>" />
              <input type="submit" value="<?php echo $this -> translate('Aceptar'); ?>" />
        </form>
    </div>
</div>