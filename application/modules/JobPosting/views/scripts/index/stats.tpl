<div class="admin_jobposting">
    <h1>&nbsp;</h1><!-- <h1><?php echo $this->translate('Job Posting Colombina'); ?></h1>-->
    <div id="left_column_admin">
        <div id="admin_menu_jobposting">
            <?php if( count($this->navigation) ): ?>
                <div class='tabs'>
                  <?php
                    // Render the menu
                    //->setUlClass()
                    echo $this->navigation()->menu()->setContainer($this->navigation)->render()
                  ?>
                </div>
            <?php endif; ?>
        </div>
            <div class="search">
                <form method="post" action="<?php echo $this -> url(); ?>">
                    <fieldset>
                        <legend>
                            <?php echo $this -> translate('Buscar'); ?>
                        </legend>
                        <label for="palabras">
                            <?php echo $this -> translate('Palabras claves'); ?>
                        </label>
                        <input type="text" name="palabras" id="palabras" value="<?php echo $this->formValues['palabras']; ?>" />
                        <label for="cargo">
                            <?php echo $this -> translate('Cargo laboral'); ?>
                        </label>
                        <input type="text" name="cargo" id="cargo" value="<?php echo $this->formValues['cargo']; ?>" />
                        <label for="sede">
                            <?php echo $this -> translate('Sede'); ?>
                        </label>
                        <input type="text" name="sede" id="sede" value="<?php echo $this->formValues['sede']; ?>" />
                    </fieldset>
                    <input type="submit" value="<?php echo $this -> translate('Buscar'); ?>" />
                </form>
            </div>
    </div>
    <div id="right_column_admin">
        <div class="border">
            <div class="treintaytres">
                <span><?php echo $this -> vacantes; ?></span>
                <?php echo $this -> translate('Vacantes publicadas'); ?>
            </div>
            <div class="treintaytres">
                <span><?php echo $this -> cantidadvisitas; ?></span>
                <?php echo $this -> translate('Total visitas'); ?>
            </div>
            <div class="treintaytres">
                <span><?php echo $this -> cantidadpostulaciones; ?></span>
                <?php echo $this -> translate('Total de aspirantes'); ?>
            </div>
            <?php if( count($this->paginator) ): ?>
                <?php foreach ($this->paginator as $item): ?>
                    <div class="job_item">
                        <h3>
                            <?php 
                                echo $this->htmlLink
                                (
                                    array
                                    (
                                        'route' =>  'jobposting-view', 'id' => $item->getIdentity()
                                    ),
                                    $item->jobs_nombre,
                                    array
                                    (
                                        'class' => 'option_view'
                                    )
                                );
                            ?>
                        </h3>
                        <span class="lugar">
                            <?php echo $item->jobs_lugar; ?>
                        </span>
                        <span class="fecha">
                            <?php echo strftime('%e/%b/%Y', strtotime($item->jobs_fecha)); ?>
                        </span>
                        <span class="descripcion">
                            <?php echo $item->jobs_mision; ?>
                        </span>
                        <div class="published_visitas">
                            <span class="published">
                                <?php echo $this->translate('Posted by %s', $item->getOwner()->toString()); ?>
                            </span>
                            <span class="visitas">
                                <?php echo $item->jobs_visitas.' '.$this->translate('Visitas'); ?>
                            </span>
                            <span class="visitas">
                                <?php echo $item->jobs_postulaciones.' '.$this->translate('Postulaciones'); ?>
                            </span>
                        </div>
                    </div>
                <?php endforeach; ?>
            <div>
                <?php echo $this->paginationControl($this->paginator, null, null, array('query' => $this->formValues)); ?>
            </div>
            <?php else: ?>
              <div class="tip">
                <span>
                  <?php echo $this->translate("No hay vacantes disponibles.") ?>
                </span>
              </div>
            <?php endif; ?>
            <br />
            <?php
                echo $this->htmlLink
                (
                    array
                    (
                        'route' =>  'jobposting-exports-stats'
                    ),
                    $this -> translate('Exportar a excel'),
                    array
                    (
                        'class' => 'enlace_boton_link',
                        'style' =>  'padding: 12px 10px; width: 95px; margin: 30px;'
                    )
                );
            ?>
        </div>
    </div>
</div>