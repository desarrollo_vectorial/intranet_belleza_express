<div class="admin_jobposting">
    <h1>&nbsp;</h1><!-- <h1><?php echo $this->translate('Job Posting Colombina'); ?></h1>-->
    <div id="left_column_admin">
        <div id="admin_menu_jobposting">
            <?php if( count($this->navigation) ): ?>
                <div class='tabs'>
                  <?php
                    // Render the menu
                    //->setUlClass()
                    echo $this->navigation()->menu()->setContainer($this->navigation)->render()
                  ?>
                </div>
            <?php endif; ?>
        </div>
            <div class="search">
                <form method="post" action="/jobposting/adminbrowse">
                    <fieldset>
                        <legend>
                            <?php echo $this -> translate('Buscar'); ?>
                        </legend>
                        <label for="palabras">
                            <?php echo $this -> translate('Palabras claves'); ?>
                        </label>
                        <input type="text" name="palabras" id="palabras" value="<?php echo $this->formValues['palabras']; ?>" />
                        <label for="cargo">
                            <?php echo $this -> translate('Cargo laboral'); ?>
                        </label>
                        <input type="text" name="cargo" id="cargo" value="<?php echo $this->formValues['cargo']; ?>" />
                        <label for="sede">
                            <?php echo $this -> translate('Sede'); ?>
                        </label>
                        <input type="text" name="sede" id="sede" value="<?php echo $this->formValues['sede']; ?>" />
                    </fieldset>
                    <input type="submit" value="<?php echo $this -> translate('Buscar'); ?>" />
                </form>
            </div>
    </div>
    <div id="right_column_admin">
        <div id="frame_view_job">
            <?php
                $item = $this->job;
            ?>
            <div class="left">
                <h2>
                    <?php echo $this -> translate('Cargo'); ?>
                </h2>
                <span class="red">
                    <?php echo $item->jobs_nombre; ?>
                </span>
                <h2>
                    <?php echo $this -> translate('Perfil'); ?>
                </h2>
                <span>
                    <?php echo $item->jobs_mision; ?>
                </span>
                <h2>
                    <?php echo $this -> translate('Formacion academica'); ?>
                </h2>
                <span>
                    <?php echo $item->jobs_formacion; ?>
                </span>
                <h2>
                    <?php echo $this -> translate('Experiencia requerida'); ?>
                </h2>
                <span>
                    <?php echo $item->jobs_experiencia; ?>
                </span>
            </div>
            <div class="right w258">
                <h2><?php echo $this -> translate('Competencias'); ?></h2>
                <div class="jp_checkbox_group">
                    <!--
                    <?php
                        foreach($this->competencias as $k => $v)
                        {
                            $checked = '';
                            if(in_array($k, $this->selectedcompetencias))
                            {
                                $checked = 'checked="checked"';
                            }
                    ?>
                    <input type="checkbox" disabled="disabled" readonly="readonly" name="competencias[]" id="competencias" value="<?php echo $k ?>" <?php echo $checked; ?> />&nbsp;<?php echo $v; ?>
                    <br />
                    <?php
                        }
                    ?>-->
                    <?php echo $item->jobs_competencias;?>
                </div>
                <h2><?php echo $this -> translate('Conocimientos'); ?></h2>
                <div class="jp_checkbox_group">
                    <!--
                    <?php
                        foreach($this->conocimientos as $k => $v)
                        {
                            $checked = '';
                            if(in_array($k, $this->selectedconocimientos))
                            {
                                $checked = 'checked="checked"';
                            }
                    ?>
                    <input type="checkbox" disabled="disabled" readonly="readonly" name="conocimientos[]" id="conocimientos" value="<?php echo $k ?>" <?php echo $checked; ?> />&nbsp;<?php echo $v; ?>
                    <br />
                    <?php
                        }
                    ?>-->
                    <?php echo $item->jobs_conocimientos;?>
                </div>
                <h2><?php echo $this -> translate('Nivel'); ?></h2>
                <span>
                    <?php
                        foreach($this->niveles as $k => $v)
                        {
                            if($k==$item->jobs_nivel)
                            {
                                echo $v;
                            }
                        }
                    ?>
                </span>
                <h2><?php echo $this -> translate('Area'); ?></h2>
                <span>
                    <?php echo $item->jobs_lugar; ?>
                </span>
            </div>
        </div>
        <div id="frame_view_job_extra">
            <div class="header">
                <span class="published">
                    <?php echo $this->translate('Posted by %s', $item->getOwner()->toString()); ?>
                </span>
                <span class="published_date">
                    <?php echo strftime('%e/%b/%Y', strtotime($item->jobs_fecha)); ?>
                </span>
                <span class="published_visitas">
                    <?php echo $this->translate('%s visitas', $item->jobs_visitas); ?>
                </span>
                <span class="published_aply">
                    <?php echo $this->translate('%s postulados', $item->jobs_postulaciones); ?>
                </span>
                <span class="published_rejected">
                    <?php echo $this->translate('%s descartados', $item->descartados()); ?>
                </span>
            </div>
            <div class="aspirantes">
                <span><?php echo $this -> translate('%s aspirantes', $item->jobs_postulaciones); ?></span>
                <div class="aspirantes_container">
                        <?php
                            foreach($this -> aspirantes as $v)
                            {
                        ?>
                            <div class="left">
                                <div class="profile_photo">
                                    <?php
                                        echo $this->htmlLink($this->user($v->owner_id)->getHref(), $this->itemPhoto($this->user($v->owner_id), 'thumb.icon'));
                                    ?>
                                </div>
                                <h3>
                                    <?php 
                                        echo $v->getOwner()->toString();
                                    ?>
                                </h3>
                                <span class="fecha">
                                    <?php echo strftime('%e/%b/%Y', strtotime($v->usuarios_fecha)); ?>
                                </span>
                            </div>
                            <div class="right">
                                <?php
                                    if($v->usuarios_estado=='postulado')
                                    {
                                        echo $this->htmlLink
                                        (
                                            array
                                            (
                                                'route' =>  'jobposting-preseleccionar',
                                                'job'   =>  $v->usuarios_job,
                                                'owner'   =>  $v->owner_id
                                            ),
                                            'Preseleccionar',
                                            array
                                            (
                                                'class' => 'enlace_boton_link',
                                                'style' => 'width: 81px; padding: 12px 15px;',
                                                'onclick' => "return confirm('".$this -> translate('Are you sure').'?'."');"
                                            )
                                        );
                                        echo $this->htmlLink
                                        (
                                            array
                                            (
                                                'route' =>  'jobposting-descartar',
                                                'job'   =>  $v->usuarios_job,
                                                'owner'   =>  $v->owner_id
                                            ),
                                            'Descartar',
                                            array
                                            (
                                                'class' => 'enlace_boton_link_red',
                                                'onclick' => "return confirm('".$this -> translate('Are you sure').'?'."');"
                                            )
                                        );
                                    }
                                    else if($v->usuarios_estado=='rechazado')
                                    {
                                        echo '<b>'.$this->translate('esta postulacion ya fue descartada').'</b>';
                                    }
                                    else
                                    {
                                        echo '<b>'.$this->translate('esta postulacion ya fue preseleccionada').'</b>';
                                    }
                                ?>
                            </div>
                            <div class="complete">
                                <span>
                                    <a class="contactar" href="mailto:<?php echo $this->user($v->owner_id)->email; ?>"><?php echo $this -> translate('Contactar'); ?></a>
                                    <?php echo $this->htmlLink($this->user($v->owner_id)->getHref(), $this -> translate('Ver hoja de vida'), array('class' => 'hoja')); ?>
                                </span>
                            </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
            <br />
            <?php
                if($item->jobs_estado=='abierta')
                {
                    echo $this->htmlLink
                    (
                        array
                        (
                            'route' =>  'jobposting-cerrar',
                            'id' => $item->jobs_id
                        ),
                        $this -> translate('Finalizar busqueda de vacante'),
                        array
                        (
                            'class' => 'enlace_boton_link_largo',
                            'onclick' => "return confirm('".$this -> translate('Are you sure').'?'."');"
                        )
                    );
                }
            ?>
            <?php
                    echo $this->htmlLink
                    (
                        array
                        (
                            'route' =>  'jobposting-exports-aply',
                            'id' => $item->jobs_id
                        ),
                        $this -> translate('Exportar a excel'),
                        array
                        (
                            'class' => 'enlace_boton_link',
                            'style' =>  'padding: 12px 10px; width: 95px;'
                        )
                    );
            ?>
    </div>
</div>