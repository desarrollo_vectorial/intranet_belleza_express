<div class="admin_jobposting">
    <h1>&nbsp;</h1><!-- <h1><?php echo $this->translate('Job Posting Colombina'); ?></h1>-->
    <div id="left_column_admin">
        <div id="admin_menu_jobposting">
            <?php if( count($this->navigation) ): ?>
                <div class='tabs'>
                  <?php
                    // Render the menu
                    //->setUlClass()
                    echo $this->navigation()->menu()->setContainer($this->navigation)->render()
                  ?>
                </div>
            <?php endif; ?>
        </div>
            <div class="search">
                <form method="post" action="/jobposting/adminbrowse">
                    <fieldset>
                        <legend>
                            <?php echo $this -> translate('Buscar'); ?>
                        </legend>
                        <label for="palabras">
                            <?php echo $this -> translate('Palabras claves'); ?>
                        </label>
                        <input type="text" name="palabras" id="palabras" value="<?php echo $this->formValues['palabras']; ?>" />
                        <label for="cargo">
                            <?php echo $this -> translate('Cargo laboral'); ?>
                        </label>
                        <input type="text" name="cargo" id="cargo" value="<?php echo $this->formValues['cargo']; ?>" />
                        <label for="sede">
                            <?php echo $this -> translate('Sede'); ?>
                        </label>
                        <input type="text" name="sede" id="sede" value="<?php echo $this->formValues['sede']; ?>" />
                    </fieldset>
                    <input type="submit" value="<?php echo $this -> translate('Buscar'); ?>" />
                </form>
            </div>
    </div>
    <div id="right_column_admin">
        <?php if( count($this->paginator) ): ?>
            <table class="admin_list_table">
                <thead>
                    <tr>
                        <th>
                            <?php echo $this -> translate('Cargo'); ?>
                        </th>
                        <th>
                            <?php echo $this -> translate('Perfil y funciones del aspirante'); ?>
                        </th>
                        <th>
                            <?php echo $this -> translate('Fecha'); ?>
                        </th>
                        <th>
                            <?php echo $this -> translate('Estado'); ?>
                        </th>
                    </tr>
                    <tr class="sep">
                        <td colspan="4">&nbsp;</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->paginator as $item): ?>
                        <tr>
                            <td class="nombrecargo">
                                <?php echo $item->jobs_nombre; ?>
                                <br />
                                <span>
                                    <?php echo $item->jobs_lugar; ?>
                                </span>
                                <br />
                                <div class="options">
                                    <?php
                                        echo $this->htmlLink
                                        (
                                            array
                                            (
                                                'route' =>  'jobposting-edit', 'id' => $item->getIdentity()
                                            ),
                                            $this -> translate('Editar'),
                                            array
                                            (
                                                'class' => 'option_edit'
                                            )
                                        );
                                        echo $this->htmlLink
                                        (
                                            array
                                            (
                                                'route' =>  'jobposting-view', 'id' => $item->getIdentity()
                                            ),
                                            $this -> translate('Ver'),
                                            array
                                            (
                                                'class' => 'option_view'
                                            )
                                        );
                                        echo $this->htmlLink
                                        (
                                            array
                                            (
                                                'route' =>  'jobposting-del', 'id' => $item->getIdentity()
                                            ),
                                            $this -> translate('Borrar'),
                                            array
                                            (
                                                'class' => 'option_del',
                                                'onclick' => "return confirm('".$this->translate('Are you sure').'?'."');"
                                            )
                                        );
                                    ?>
                                </div>
                            </td>
                            <td>
                                <?php echo $item->jobs_mision; ?>
                            </td>
                            <td>
                                <?php echo strftime('%e/%b/%Y', strtotime($item->jobs_fecha)); ?>
                            </td>
                            <td>
                                <?php
                                    $estado = 'Desconocido';
                                    $estadob = 'Desconocido';
                                    $class = '';
                                    if($item->jobs_estado=='abierta')
                                    {
                                        $estado = $this-> translate('Publicada');
                                        $estadob = $this-> translate('Cerrar');
                                    }
                                    else if(($item->jobs_estado=='archivada'))
                                    {
                                        $estado = $this -> translate('Archivada'); 
                                        $estadob = $this-> translate('Publicar');
                                        $class = '_rojo';
                                    }
                                    else if(($item->jobs_estado=='cerrada'))
                                    {
                                        $estado = $this -> translate('Cerrada'); 
                                        $estadob = $this-> translate('Publicar');
                                        $class = '_rojo';
                                    }
                                    /*else if(($item->jobs_estado=='archivada') && ($item->jobs_borrador=='no'))
                                    {
                                        $estado = $this -> translate('Cerrada'); 
                                        $estadob = $this-> translate('Publicar');
                                        $class = '_rojo';
                                    }*/
                                    
                                    echo '<a class="enlace_boton'.$class.'">'.$estado.'</a>';
                                    
                                    if(($item->jobs_estado!='cerrada'))
                                    {
                                        echo $this->htmlLink
                                        (
                                            array
                                            (
                                                'route' =>  'jobposting-state', 'id' => $item->getIdentity()
                                            ),
                                            $estadob,
                                            array
                                            (
                                                'class' => 'enlace_boton_link',
                                                'onclick' => "return confirm('".$this->translate('Are you sure').'?'."');"
                                            )
                                        );
                                    }
                                    else
                                    {
                                        echo '<br />';
                                        echo '<br />';
                                        echo '<br />';
                                        echo '<br />';
                                    }
                                ?>
                            </td>
                        </tr>   
                        <tr class="sep">
                            <td colspan="4">&nbsp;</td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div style="text-align: center;">
                <?php echo $this->paginationControl($this->paginator, null, null, array('query' => $this->formValues)); ?>
                <br />
                <?php
                    echo $this->htmlLink
                    (
                        array
                        (
                            'route' =>  'jobposting-exports-job'
                        ),
                        $this -> translate('Exportar a excel'),
                        array
                        (
                            'class' => 'enlace_boton_link',
                            'style' =>  'padding: 12px 10px; width: 95px; float: none;'
                        )
                    );
                ?>
            </div>
        <?php else: ?>
          <div class="tip">
            <span>
              <?php echo $this->translate("No hay vacantes disponibles.") ?>
            </span>
          </div>
        <?php endif; ?>
    </div>
</div>