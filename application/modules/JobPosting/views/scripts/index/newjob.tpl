<div class="admin_jobposting">
    <h1>&nbsp;</h1><!-- <h1><?php echo $this->translate('Job Posting Colombina'); ?></h1>-->
    <div id="left_column_admin">
        <div id="admin_menu_jobposting">
            <?php if( count($this->navigation) ): ?>
                <div class='tabs'>
                  <?php
                    // Render the menu
                    //->setUlClass()
                    echo $this->navigation()->menu()->setContainer($this->navigation)->render()
                  ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div id="right_column_admin">
        <h2><?php if($this->id==0){ echo $this -> translate('Crear nueva vacante'); } else { echo $this -> translate('Editar vacante'); } ?></h2>
        <form method="post" action="<?php echo $this->url(); ?>" id="forma_jp">
            <input type="hidden" name="id" id="id" value="<?php echo $this->id; ?>" />
            <fieldset>
                <div class="w392">
                    <label for="cargo">
                        <?php echo $this -> translate('Cargo'); ?>
                    </label>
                    <input type="text" name="cargo" id="cargo" value="<?php echo $this -> cargo; ?>" class="required" />
                </div>
                <div class="w258">
                    <label for="formacion">
                        <?php echo $this -> translate('Formacion academica'); ?>
                    </label>
                    <input type="text" name="formacion" id="formacion" value="<?php echo $this -> formacion; ?>" class="required" />
                </div>
                <div class="w392">
                    <label for="descripcion">
                        <?php echo $this -> translate('Descripcion'); ?>
                    </label>
                    <textarea name="descripcion" id="descripcion" class="required"><?php echo $this -> descripcion; ?></textarea>
                </div>
                <div class="w258">
                    <label for="experiencia">
                        <?php echo $this -> translate('Experiencia requerida'); ?>
                    </label>
                    <textarea name="experiencia" id="experiencia" class="required"><?php echo $this -> experiencia; ?></textarea>
                </div>
                <div class="w392">
                    <label for="competencias">
                        <?php echo $this -> translate('Competencias'); ?>
                    </label>
                    <div class="jp_checkbox_group">
                        <!--
                        <?php
                            foreach($this->competencias as $k => $v)
                            {
                                $checked = '';
                                if(in_array($k, $this->selectedcompetencias))
                                {
                                    $checked = 'checked="checked"';
                                }
                        ?>
                        <input type="checkbox" name="competencias[]" id="competencias" value="<?php echo $k ?>" <?php echo $checked; ?> />&nbsp;<?php echo $v; ?>
                        <br />
                        <?php
                            }
                        ?>-->
                        <textarea name="competencias" id="competencias" class="required"><?php echo $this -> competencias; ?></textarea>
                    </div>
                </div>
                <div class="w258">
                    <label for="nivel">
                        <?php echo $this -> translate('Nivel'); ?>
                    </label>
                    <select name="nivel" id="nivel">
                        <?php
                            foreach($this->niveles as $k => $v)
                            {
                                $selected = '';
                                if($k==$this->nivel)
                                {
                                    $selected = 'selected="selected"';
                                }
                        ?>
                        <option value="<?php echo $k; ?>" <?php echo $selected; ?>>
                            <?php echo $v; ?>
                        </option>
                        <br />
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="w392">
                    <label for="conocimientos">
                        <?php echo $this -> translate('Conocimientos'); ?>
                    </label>
                    <div class="jp_checkbox_group">
                        <!--<?php
                            foreach($this->conocimientos as $k => $v)
                            {
                                $checked = '';
                                if(in_array($k, $this->selectedconocimientos))
                                {
                                    $checked = 'checked="checked"';
                                }
                        ?>
                        <input type="checkbox" name="conocimientos[]" id="conocimientos" value="<?php echo $k ?>" <?php echo $checked; ?> />&nbsp;<?php echo $v; ?>
                        <br />
                        <?php
                            }
                        ?>-->
                        <textarea name="conocimientos" id="conocimientos" class="required"><?php echo $this -> conocimientos; ?></textarea>
                    </div>
                </div>
                <div class="w258">
                    <label for="ubicacion">
                        <?php echo $this -> translate('Area y ubicacion'); ?>
                    </label>
                    <input type="text" name="ubicacion" id="ubicacion" value="<?php echo $this -> ubicacion; ?>" class="required" />
                </div>
                <div class="w258">
                    <input type="hidden" name="term" id="term" value="publicar" />
                    <input type="submit" name="enviar" id="publicar" value="<?php echo $this -> translate('Publicar'); ?>" />
                    <input type="submit" name="enviar" id="archivar" value="<?php echo $this -> translate('Archivar'); ?>" />
                </div>
            </fieldset>
        </form>
        <script>
        window.addEvent('domready', function() {
            MooTools.lang.setLanguage("es-ES");
            validate = new Form.Validator.Inline("forma_jp");
            $('publicar').addEvent('click', function() {
                $('term').value = 'publicar'; 
                validate.validate();
            });
            $('archivar').addEvent('click', function() {
                $('term').value = 'archivar'; 
                validate.validate();
            });
        });
        </script>
    </div>
</div>