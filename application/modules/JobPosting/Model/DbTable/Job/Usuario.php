<?php

class JobPosting_Model_DbTable_Job_Usuario extends Engine_Db_Table
{
  protected $_rowClass = 'JobPosting_Model_Job_Usuario';
  
  
  public function getJobUsuarioPaginator($params = array(), $customParams = null)
  {
    $paginator = Zend_Paginator::factory($this->getJobsSelect($params, $customParams));
    if( !empty($params['page']) ) {
      $paginator->setCurrentPageNumber($params['page']);
    }
    if( !empty($params['limit']) ) {
      $paginator->setItemCountPerPage($params['limit']);
    }
    return $paginator;
  }
  
  public function getJobUsuarioSelect($params = array(), $customParams = null)
  {
    $tableName = $this->info('name');

    $select = $this->select()
        ->from($this)
        ->order(!empty($params['orderby']) ? $tableName . '.' . $params['orderby'] . ' ASC'
                  : $tableName . '.link_id ASC' );
	/*echo $select;*/
	return $select;
  }
  
}