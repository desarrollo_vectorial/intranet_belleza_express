<?php

class JobPosting_Model_DbTable_Niveles extends Engine_Db_Table
{
  protected $_rowClass = 'JobPosting_Model_Niveles';
  
  
  public function getNivelesPaginator($params = array(), $customParams = null)
  {
    $paginator = Zend_Paginator::factory($this->getConocimientosSelect($params, $customParams));
    if( !empty($params['page']) ) {
      $paginator->setCurrentPageNumber($params['page']);
    }
    if( !empty($params['limit']) ) {
      $paginator->setItemCountPerPage($params['limit']);
    }
    return $paginator;
  }
  
  public function getNivelesSelect($params = array(), $customParams = null)
  {
    $tableName = $this->info('name');

    $select = $this->select()
        ->from($this)
        ->order(!empty($params['orderby']) ? $tableName . '.' . $params['orderby'] . ' ASC'
                  : $tableName . '.link_id ASC' );
	/*echo $select;*/
	return $select;
  }
  
}