<?php

class JobPosting_Model_Jobs extends Core_Model_Item_Abstract
{
    public function getAplicar()
    {
        $viewer = Engine_Api::_()->user()->getViewer();    
        
        $table = Engine_Api::_()->getDbtable('Job_Usuario', 'JobPosting');
	$select = $table->select()
                ->where('usuarios_job='.$this->jobs_id.' AND usuarios_usuario='.$viewer->user_id);
        
	$result = $table->fetchAll($select);
        
        if(count($result)>0)
        {
            return false;
        }
        
        return true;
    }
    
    public function descartados()
    {
        $table = Engine_Api::_()->getDbtable('Job_Usuario', 'JobPosting');
	$select = $table->select()
                ->where('usuarios_job='.$this->jobs_id." AND usuarios_estado='rechazado' " );
        
	$result = $table->fetchAll($select);
        
        return count($result);
    }
}