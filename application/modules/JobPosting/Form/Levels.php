<?php

class JobPosting_Form_Levels extends Authorization_Form_Admin_Level_Abstract
{
	public function init()
	{
		$this
			->setTitle('Member Level Settings');

		$this->loadDefaultDecorators();
		$this->getDecorator('Description')->setOptions(array('tag' => 'h4', 'placement' => 'PREPEND'));

		// prepare user levels
		$table = Engine_Api::_()->getDbtable('levels', 'authorization');
		$select = $table->select();
		$user_levels = $table->fetchAll($select);
    
		foreach ($user_levels as $user_level)
			$levels[$user_level->level_id]= $user_level->getTitle();
    
		// category field
		$this->addElement('Select', 'level_id', array(
			'label' => 'Select Member Level',
			'multiOptions' => $levels,
			'onchange' => 'javascript:changeLevel(this.value);',
			'ignore' => true,
		));
        
		$this->addElement('Radio', 'creates', array(
			'label' => 'Allow Admin',
			'description' => 'Do you want to let users admin the module?.',
			'multiOptions' => array(
				1 => 'Yes.',
				0 => 'No.'
			)
		));
	  
		$this->addElement('Button', 'submit', array(
			'label' => 'Save Settings',
			'type' => 'submit',
			'order' => 1000
		));
	}
}