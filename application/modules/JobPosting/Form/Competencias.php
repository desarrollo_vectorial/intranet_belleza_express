<?php
class JobPosting_Form_Competencias extends Engine_Form
{
  protected $_field;

  public function init()
  {
    $this->setMethod('post');

    $id = new Zend_Form_Element_Hidden('competencias_id');
	
    $competencia = new Zend_Form_Element_Text('competencias_competencia');
    $competencia->setLabel('Competencia')
      ->addValidator('NotEmpty')
      ->setRequired(true)
      ->setAttrib('class', 'text');


    $this->addElements(array(
      $id,
	  $competencia,
    ));
    // Buttons
    $this->addElement('Button', 'submit', array(
      'label' => 'Add Competencia',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => Zend_Registry::get('Zend_Translate')->_(' or '),
      'href' => '',
      'onClick'=> 'javascript:parent.Smoothbox.close();',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    $button_group = $this->getDisplayGroup('buttons');
  }
  
  public function setDefaultValues()
  {
	  $this->competencias_id->setValue(0);
  }
}