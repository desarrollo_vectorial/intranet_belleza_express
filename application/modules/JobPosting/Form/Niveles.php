<?php
class JobPosting_Form_Niveles extends Engine_Form
{
  protected $_field;

  public function init()
  {
    $this->setMethod('post');

    $id = new Zend_Form_Element_Hidden('niveles_id');
	
    $nivel = new Zend_Form_Element_Text('niveles_nivel');
    $nivel->setLabel('Nivel')
      ->addValidator('NotEmpty')
      ->setRequired(true)
      ->setAttrib('class', 'text');


    $this->addElements(array(
      $id,
	  $nivel,
    ));
    // Buttons
    $this->addElement('Button', 'submit', array(
      'label' => 'Add Nivel',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => Zend_Registry::get('Zend_Translate')->_(' or '),
      'href' => '',
      'onClick'=> 'javascript:parent.Smoothbox.close();',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    $button_group = $this->getDisplayGroup('buttons');
  }
  
  public function setDefaultValues()
  {
	  $this->niveles_id->setValue(0);
  }
}