<?php
class JobPosting_Form_Createb extends Engine_Form
{
  protected $_field;

  public function init()
  {
    $this->setMethod('post');

    $id = new Zend_Form_Element_Hidden('conocimiento_id');
	
    $conocimiento = new Zend_Form_Element_Text('conocimiento_conocimiento');
    $conocimiento->setLabel('Conocimiento especifico')
      ->addValidator('NotEmpty')
      ->setRequired(true)
      ->setAttrib('class', 'text');

        $title = new Zend_Form_Element_Select('capacitaciones_estado');
        $title->setLabel('Estado')
        ->setMultiOptions(array('1' => 'Activa', '2' => 'Aplazada', '3' => 'Cancelado'))
        ->setRequired(true)->addValidator('NotEmpty', true);
        $form->addElement($title);

    $this->addElements(array(
      $id,
	  $conocimiento,
    ));
    // Buttons
    $this->addElement('Button', 'submit', array(
      'label' => 'Add Conocimiento',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => Zend_Registry::get('Zend_Translate')->_(' or '),
      'href' => '',
      'onClick'=> 'javascript:parent.Smoothbox.close();',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    $button_group = $this->getDisplayGroup('buttons');
  }
  
  public function setDefaultValues()
  {
	  $this->conocimiento_id->setValue(0);
  }
}