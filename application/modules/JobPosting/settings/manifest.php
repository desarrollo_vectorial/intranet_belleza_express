<?php return array (
  'package' =>
  array (
    'type' => 'module',
    'name' => 'job-posting',
    'version' => '4.1.8',
    'path' => 'application/modules/JobPosting',
    'title' => 'Job Posting',
    'description' => 'Modulo para la publicacion de vacantes, y su respectiva administracion',
    'author' => 'dhincapie@weabers.com',
    'callback' =>
    array (
      'path'  => 'application/modules/JobPosting/settings/JobPostingInstall.php',
      'class' => 'JobPosting_Installer',
    ),
    'actions' =>
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' =>
    array (
      0 => 'application/modules/JobPosting',
    ),
    'files' =>
    array (
      0 => 'application/languages/en/job-posting.csv',
    ),
     ),
    'items' => array
    (
        'conocimientos',
        'competencias',
        'niveles',
        'jobs',
    ),
  'routes' => array(
    'jobposting_main_home' => array(
      'route' => 'jobposting',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
      )
    ),
    'jobposting_main_browse' => array(
      'route' => 'jobposting/browse',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
      )
    ),
    'jobposting_admin_browse' => array(
      'route' => 'jobposting/adminbrowse/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'adminbrowse'
      )
    ),
    'jobposting_user_browse' => array(
      'route' => 'jobposting/userbrowse/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'userbrowse'
      )
    ),
    'jobposting-stats' => array(
      'route' => 'jobposting/stats',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'stats'
      )
    ),
    'jobposting-aspirantes' => array(
      'route' => 'jobposting/aspirantes',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'adminbrowse'
      )
    ),
    'jobposting-new' => array(
      'route' => 'jobposting/new',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'newjob'
      )
    ),
    'jobposting-state' => array(
      'route' => 'jobposting/state/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'state'
      )
    ),
    'jobposting-edit' => array(
      'route' => 'jobposting/edit/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'edit'
      )
    ),
    'jobposting-del' => array(
      'route' => 'jobposting/del/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'del'
      )
    ),
    'jobposting-view' => array(
      'route' => 'jobposting/view/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'view'
      )
    ),
    'jobposting-aplicar' => array(
      'route' => 'jobposting/aplicar/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'aplicar'
      )
    ),
    'jobposting-popularity' => array(
      'route' => 'jobposting/popularity/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'popularity'
      )
    ),
    'jobposting-exports-job' => array(
      'route' => 'jobposting/exportjob/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'exportjob'
      )
    ),
    'jobposting-exports-stats' => array(
      'route' => 'jobposting/exportstats',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'exportstats'
      )
    ),
    'jobposting-exports-aply' => array(
      'route' => 'jobposting/exportaply/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'exportaply'
      )
    ),
    'jobposting-preseleccionar' => array(
      'route' => 'jobposting/preseleccionar/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'preseleccionar'
      )
    ),
    'jobposting-descartar' => array(
      'route' => 'jobposting/descartar/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'descartar'
      )
    ),
    'jobposting-cerrar' => array(
      'route' => 'jobposting/cerrar/*',
      'defaults' => array(
        'module' => 'job-posting',
        'controller' => 'index',
        'action' => 'cerrar'
      )
    )
  )
); ?>
