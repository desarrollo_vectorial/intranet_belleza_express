<?php

class JobPosting_Installer extends Engine_Package_Installer_Module
{
    public function onInstall()
    {
        $this->__createCustomFields();
        $this->__createMailTemplates();
        parent::onInstall();
    }
    
    protected function __createMailTemplates()
    {
        $db = $this->getDb();

        // profile page
        $template_id = $db->select()
                ->from('engine4_core_mailtemplates', 'mailtemplate_id')
                ->where('module = ?', 'job-posting')
                ->where('type = ?', 'job-posting_aplicar')
                ->limit(1)
                ->query()
                ->fetchColumn();
        
        if(!$template_id)
        {
            $db->insert('engine4_core_mailtemplates', array
                (
                    'type'      =>  'job-posting_aplicar',
                    'module'    =>  'job-posting',
                    'vars'     =>  '[job_cargo]',
                )
            );
        }

        // profile page
        $template_id = $db->select()
                ->from('engine4_core_mailtemplates', 'mailtemplate_id')
                ->where('module = ?', 'job-posting')
                ->where('type = ?', 'job-posting_preseleccionar')
                ->limit(1)
                ->query()
                ->fetchColumn();
        
        if(!$template_id)
        {
            $db->insert('engine4_core_mailtemplates', array
                (
                    'type'      =>  'job-posting_preseleccionar',
                    'module'    =>  'job-posting',
                    'vars'     =>  '[job_cargo]',
                )
            );
        }

        // profile page
        $template_id = $db->select()
                ->from('engine4_core_mailtemplates', 'mailtemplate_id')
                ->where('module = ?', 'job-posting')
                ->where('type = ?', 'job-posting_descartar')
                ->limit(1)
                ->query()
                ->fetchColumn();
        
        if(!$template_id)
        {
            $db->insert('engine4_core_mailtemplates', array
                (
                    'type'      =>  'job-posting_descartar',
                    'module'    =>  'job-posting',
                    'vars'     =>  '[job_cargo]',
                )
            );
        }
    }
    
    protected function __createCustomFields()
    {
        $db = $this->getDb();

        // profile page
        $conocimiento_id = $db->select()
                ->from('engine4_user_fields_meta', 'field_id')
                ->where('label = ?', 'Conocimientos')
                ->where('type = ?', 'multi_checkbox')
                ->limit(1)
                ->query()
                ->fetchColumn();
        
        if(!$conocimiento_id)
        {
            $db->insert('engine4_user_fields_meta', array
                (
                    'type'      =>  'multi_checkbox',
                    'label'    =>  'Conocimientos',
                    'display'     =>  '1',
                )
            );
            
           $conocimiento_id = $db->select()
            ->from('engine4_user_fields_meta', 'field_id')
            ->where('label = ?', 'Conocimientos')
            ->where('type = ?', 'multi_checkbox')
            ->limit(1)
            ->query()
            ->fetchColumn();
           
            $db->insert('engine4_user_fields_maps', array
                (
                    'field_id'  =>  1,
                    'option_id'  =>  1,
                    'child_id'  => $conocimiento_id,
                    'order'     =>  9999
                )
            );
        }

        // profile page
        $competencia_id = $db->select()
                ->from('engine4_user_fields_meta', 'field_id')
                ->where('label = ?', 'Competencias')
                ->where('type = ?', 'multi_checkbox')
                ->limit(1)
                ->query()
                ->fetchColumn();
        
        if(!$competencia_id)
        {
            $db->insert('engine4_user_fields_meta', array
                (
                    'type'      =>  'multi_checkbox',
                    'label'    =>  'Competencias',
                    'display'     =>  '1',
                )
            );
            
           $competencia_id = $db->select()
            ->from('engine4_user_fields_meta', 'field_id')
            ->where('label = ?', 'Competencias')
            ->where('type = ?', 'multi_checkbox')
            ->limit(1)
            ->query()
            ->fetchColumn();
           
            $db->insert('engine4_user_fields_maps', array
                (
                    'field_id'  =>  1,
                    'option_id'  =>  1,
                    'child_id'  => $competencia_id,
                    'order'     =>  9999
                )
            );
        }

        // profile page
        $nivel_id = $db->select()
                ->from('engine4_user_fields_meta', 'field_id')
                ->where('label = ?', 'Nivel academico')
                ->where('type = ?', 'select')
                ->limit(1)
                ->query()
                ->fetchColumn();
        
        if(!$nivel_id)
        {
            $db->insert('engine4_user_fields_meta', array
                (
                    'type'      =>  'select',
                    'label'    =>  'Nivel academico',
                    'display'     =>  '1',
                )
            );
            
           $nivel_id = $db->select()
            ->from('engine4_user_fields_meta', 'field_id')
            ->where('label = ?', 'Nivel academico')
            ->where('type = ?', 'select')
            ->limit(1)
            ->query()
            ->fetchColumn();
           
            $db->insert('engine4_user_fields_maps', array
                (
                    'field_id'  =>  1,
                    'option_id'  =>  1,
                    'child_id'  => $nivel_id,
                    'order'     =>  9999
                )
            );
        }
        
    }
}
