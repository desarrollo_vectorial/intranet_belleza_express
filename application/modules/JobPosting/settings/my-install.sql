INSERT IGNORE INTO `engine4_core_modules` (`name`, `title`, `description`, `version`, `enabled`, `type`) VALUES  ('job-posting', 'Job Posting', 'Modulo para la publicacion de vacantes, y su respectiva administracion', '4.1.8', 1, 'extra');

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`) VALUES
('core_mini_profile_jp', 'job-posting', 'Hoja de vida', 'JobPosting_Plugin_Menus', '', 'job_user_browse', NULL, 1, 0, 2),
('core_public_user_job-posting', 'job-posting', 'Nuevas vacantes', NULL, '{"route":"jobposting_user_browse"}', 'job_user_browse', NULL, 1, 0, 1),
('core_public_stats_job-posting', 'job-posting', 'estadisticas', NULL, '{"route":"jobposting-stats"}', 'jobs_admin_browse', NULL, 1, 0, 4),
('core_public_new_job-posting', 'job-posting', 'nuevo empleo', NULL, '{"route":"jobposting-new"}', 'jobs_admin_browse', NULL, 1, 0, 1),
('core_public_admin_job-posting', 'job-posting', 'publicados', NULL, '{"route":"jobposting_admin_browse"}', 'jobs_admin_browse', NULL, 1, 0, 2),
('core_admin_main_plugins_job-posting', 'job-posting', 'Job Posting', '', '{"route":"admin_default","module":"job-posting","controller":"manage","action":"levels"}', 'core_admin_main_plugins', '1', 1, 0, 999),
('core_main_jobposting', 'job-posting', 'Vacantes', '', '{"route":"jobposting_main_browse"}', 'core_main', '', 1, 0, 999);


DROP TABLE IF EXISTS `engine4_jobposting_jobs`;
CREATE TABLE IF NOT EXISTS `engine4_jobposting_jobs` (
  `jobs_id` int(11) NOT NULL auto_increment,
  `jobs_nombre` varchar(255) NOT NULL,
  `jobs_mision` text,
  `jobs_formacion` text,
  `jobs_experiencia` text,
  `jobs_nivel` int(11) NOT NULL,
  `jobs_lugar` varchar(255) NOT NULL,
  `jobs_estado` enum('abierta','archivada','cerrada') default NULL,
  `jobs_borrador` enum('si','no') default NULL,
  `jobs_fecha` datetime NOT NULL,
  `jobs_visitas` int(11) NOT NULL default '0',
  `jobs_postulaciones` int(11) NOT NULL default '0',
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY  (`jobs_id`),
  KEY `jobs_nivel` (`jobs_nivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `engine4_jobposting_job_competencia`
--

DROP TABLE IF EXISTS `engine4_jobposting_job_competencia`;
CREATE TABLE IF NOT EXISTS `engine4_jobposting_job_competencia` (
  `competencia_id` int(11) NOT NULL auto_increment,
  `job_competencia_job` int(11) NOT NULL,
  `job_competencia_competencia` int(11) NOT NULL,
  PRIMARY KEY  (`competencia_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `engine4_jobposting_job_conocimiento`
--

DROP TABLE IF EXISTS `engine4_jobposting_job_conocimiento`;
CREATE TABLE IF NOT EXISTS `engine4_jobposting_job_conocimiento` (
  `conocimiento_id` int(11) NOT NULL auto_increment,
  `job_conocimiento_job` int(11) NOT NULL,
  `job_conocimiento_conocimiento` int(11) NOT NULL,
  PRIMARY KEY  (`conocimiento_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `engine4_jobposting_job_usuario`
--

DROP TABLE IF EXISTS `engine4_jobposting_job_usuario`;
CREATE TABLE IF NOT EXISTS `engine4_jobposting_job_usuario` (
  `usuario_id` int(11) NOT NULL auto_increment,
  `usuarios_job` int(11) NOT NULL,
  `usuarios_usuario` int(11) NOT NULL,
  `usuarios_estado` enum('postulado','rechazado','preseleccionado','seleccionado') NOT NULL,
  `usuarios_fecha` datetime NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY  (`usuario_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;


--
-- AJUSTES JOB POSTING
--

ALTER TABLE `engine4_jobposting_jobs` ADD `jobs_competencias` TEXT NULL AFTER `jobs_nivel` ,
ADD `jobs_conocimientos` TEXT NULL AFTER `jobs_competencias` 